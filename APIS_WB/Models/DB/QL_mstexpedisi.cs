//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstexpedisi
    {
        public string cmpcode { get; set; }
        public int expoid { get; set; }
        public string expcode { get; set; }
        public string expname { get; set; }
        public int expcityoid { get; set; }
        public string expaddr { get; set; }
        public string expemail { get; set; }
        public string expphone1 { get; set; }
        public string expphone2 { get; set; }
        public string expres1 { get; set; }
        public string expres2 { get; set; }
        public string expres3 { get; set; }
        public string activeflag { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
