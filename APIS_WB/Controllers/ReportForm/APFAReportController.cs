﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class APFAReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public APFAReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnapassetmst apm ON apm.rabmstoid=rm.rabmstoid WHERE apm.apassettype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                //if (DDLStatus != null)
                //{
                //    if (DDLStatus.Count() > 0)
                //    {
                //        string stsval = "";
                //        for (int i = 0; i < DDLStatus.Count(); i++)
                //        {
                //            stsval += "'" + DDLStatus[i] + "',";
                //        }
                //        sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                //    }
                //}

                sSql += " ORDER BY rm.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, apm.apassetmstoid AS [apmstoid], (CASE apm.apassetno WHEN '' THEN CONVERT(VARCHAR(10), apm.apassetmstoid) ELSE apm.apassetno END) AS [Draft No.], apm.apassetno AS [AP No.], apm.apassetdate AS [AP Date], CURRENT_TIMESTAMP [AP Date Take Giro], s.suppname [Suppname] FROM QL_trnapassetmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid WHERE apm.cmpcode='" + CompnyCode + "' ";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND apm.apassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY apm.apassetno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.suppcode [Kode], c.suppname [Nama], c.suppaddr [Alamat] FROM QL_mstsupp c WHERE c.suppoid IN (SELECT apm.suppoid FROM QL_trnapassetmst apm WHERE apm.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND apm.apassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                //if (TextNomor != "")
                //{
                //    string[] arr = TextNomor.Split(';'); string datafilter = "";
                //    for (int i = 0; i < arr.Count(); i++)
                //    {
                //        datafilter += "'" + arr[i] + "',";
                //    }
                //    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                //}

                sSql += ") ORDER BY c.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomor, string TextNomor, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnapassetdtl rd ON rd.apassetrefoid=m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.apassetunitoid INNER JOIN QL_trnapassetmst apm ON apm.apassetmstoid=rd.apassetmstoid WHERE apm.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND apm.apassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                //if (TextCust != "")
                //{
                //    string[] arr = TextCust.Split(';'); string datafilter = "";
                //    for (int i = 0; i < arr.Count(); i++)
                //    {
                //        datafilter += "'" + arr[i] + "',";
                //    }
                //    sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                //}

                //if (TextNomor != "")
                //{
                //    string[] arr = TextNomor.Split(';'); string datafilter = "";
                //    for (int i = 0; i < arr.Count(); i++)
                //    {
                //        datafilter += "'" + arr[i] + "',";
                //    }
                //    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                //}
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptAPAsset_ReportAPNo.rpt";
                rptname = "FB_SUMMARY";
            }
            else if (DDLType == "Summary2")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptAP_ReportPPNMXls.rpt";
                }
                else
                {
                    rptfile = "rptAP_ReportPPNMPdf.rpt";
                }
                rptname = "PPNMASUKAN_SUMMARY";
            }
            else
            {
                rptfile = "rptAPAsset_ReportDtlAPNo.rpt";
                rptname = "FB_DETAIL";
            }

            var Slct = "";
            var Join = "";
            if (DDLType == "Summary2")
            {
                sSql = "SELECT apm.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=apm.cmpcode) [BU Name], apm.apassetmstoid, apm.apassetno, apm.apassetdate, fa.fabelicode+'-'+fa.fakturno fakturno, s.suppname, s.suppnpwpno suppnpwp, CASE WHEN apm.apassettaxtype='TAX' THEN 'PPN' ELSE 'NON PPN' END keterangan, apm.apassettotalamt DPP, apd2.apassetdtl2amt PPN FROM QL_trnapassetmst apm INNER JOIN QL_mstsupp s ON s.suppoid = apm.suppoid INNER JOIN QL_m05GN g ON g.gnoid = apm.apassetpaytypeoid AND g.gngroup = 'PAYMENT TERM' INNER JOIN QL_trnpoassetmst pom ON apm.cmpcode = pom.cmpcode AND pom.poassetmstoid = apm.poassetmstoid INNER JOIN QL_mstcurr c ON c.curroid = apm.curroid INNER JOIN QL_trnapassetdtl2 apd2 ON apd2.apassetmstoid = apm.apassetmstoid INNER JOIN QL_mstfabeli fa ON fa.fabelioid = apd2.fabelioid LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid = apm.rabmstoid WHERE apm.cmpcode='" + CompnyCode + "'";
            }
            else
            {
                if (DDLType == "Detail")
                {
                    Slct += ", isnull(apassetdtlres2, '') [No. Invoice],apd.apassetdtloid [Dtl Oid], apd.apassetdtlseq [Seq], apd.apassetrefoid [Mat Oid], m.itemcode [Mat Code] , m.itemdesc [Mat Longdesc], apassetqty [Qty] , g2.gndesc [Unit], apassetprice [Price], apassetdtlamt [Detail Amount], apassetdtldisctype [Disc Type], apassetdtldiscvalue [Disc Value], apassetdtldiscamt [Disc Amount], apassetdtlnetto [Dtl Netto],apassetdtlres1 [Faktur No.],apassetdtlres2 [Faktur Pajak No.], apassetdtltaxamt [Detail Tax Amount], mrassetno [MR No.] , CONVERT(VARCHAR(20), pom.poassetmstoid) [PO Draft No.], pom.poassetmstoid [PO ID], poassettype [PO Type], poassetdate [PO Date], poassetno [PO No.], poassetmstnote [PO Header Note], poassetmststatus [PO Status], pom.approvaluser AS [App User PO], pom.approvaldatetime AS [App Date PO], '' [No. Cash/Bank], CURRENT_TIMESTAMP [Tgl JT], '' [COA] ";

                    Join = " INNER JOIN QL_trnapassetdtl apd ON apm.cmpcode=apd.cmpcode AND apm.apassetmstoid=apd.apassetmstoid INNER JOIN QL_mstitem m ON m.itemoid = apd.apassetrefoid INNER JOIN QL_m05GN g2 ON g2.gnoid = apd.apassetunitoid INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode=apd.cmpcode AND mrm.mrassetmstoid=apd.mrassetmstoid ";
                }

                sSql = "SELECT apm.apassetmstoid AS [apmstoid], (CASE apm.apassetno WHEN '' THEN CONVERT(VARCHAR(10), apm.apassetmstoid) ELSE apm.apassetno END) AS [Draft No.], apm.apassetno AS [AP No.], apm.apassetdate AS [AP Date], '' projectname, CURRENT_TIMESTAMP [AP Date Take Giro], apm.apassetmststatus AS [Status], apm.cmpcode AS [CmpCode], apm.suppoid [Suppoid], s.suppcode [Suppcode], s.suppname [Suppname], '' [Supptype], g.gndesc [Payment Type], c.currcode [Currency], '' [Rate IDR], '' [Rate USD], (SELECT CAST(r.rate2res1 AS real) FROM QL_mstrate2 r WHERE r.rate2oid=apm.rate2oid) [Rate Montly IDR], (SELECT CAST(r.rate2res2 AS real) FROM QL_mstrate2 r WHERE r.rate2oid=apm.rate2oid) [Rate Monthly USD] , apm.apassettotalamt [AP Total Amt], 0.00 [AP Total Amt IDR], 0.00 [AP Total Amt USD], apm.apassettotaldisc [AP Total Disc], 0.00 [AP Total Disc IDR], 0.00 [AP Total Disc USD], 0.00 [AP Total Tax], apm.apassettotaltax [AP Total Tax IDR], 0.00 [AP Total Tax USD], apm.apassetgrandtotal [AP Grand Total], 0.00 [AP Grand Total IDR], 0.00 [AP Grand Total USD], '" + CompnyName + "' AS [BU Name], apm.apassetmstnote AS [Header Note], apm.createuser [Create User], apm.createtime [Create Time], '' [App User], CURRENT_TIMESTAMP [App Datetime], 0.00 [Grand Total Supplier],1 [Tax], apm.apassetduedate [Tgl JT], CASE WHEN s.supppengakuan='TT' THEN 'CHECK TT' ELSE 'PENERIMAAN' END [Pengakuan JT] " + Slct + " FROM QL_trnapassetmst apm " + Join + " INNER JOIN QL_mstsupp s ON s.suppoid = apm.suppoid INNER JOIN QL_m05GN g ON g.gnoid = apm.apassetpaytypeoid AND g.gngroup = 'PAYMENT TERM' INNER JOIN QL_trnpoassetmst pom ON apm.cmpcode=pom.cmpcode AND pom.poassetmstoid=apm.poassetmstoid INNER JOIN QL_mstcurr c ON c.curroid = apm.curroid WHERE apm.cmpcode='" + CompnyCode + "' ";
            }

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND apm.apassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorPO != "")
            {
                string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (DDLType == "Summary" || DDLType == "Summary2")
            {
                sSql += " ORDER BY apm.apassetno";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else if (DDLType == "Summary2")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else if (DDLType == "Summary2")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}