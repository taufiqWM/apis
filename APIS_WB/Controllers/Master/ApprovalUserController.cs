﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class ApprovalUserController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public ApprovalUserController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listapprovalstructure
        {
            public int asoid { get; set; }
            public int stoid_req { get; set; }
            public string req { get; set; }
            public int stoid_app { get; set; }
            public string app { get; set; }
            public int mnoid { get; set; }
            public string mnname { get; set; }
            public string asflag { get; set; }
        }

        // GET/POST : ApprovalStructure
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = " SELECT a.cmpcode, a.asoid, isnull((select x.usname from QL_m01US x where x.usoid=a.stoid_req),'') req, isnull((select x.usname from QL_m01US x where x.usoid=a.stoid_app),'') app, a.mnoid, m.mnname, a.asflag FROM QL_m08AS2 a INNER JOIN QL_m04MN m ON a.cmpcode=m.cmpcode AND a.mnoid=m.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by a.asoid desc ";
            List<listapprovalstructure> approvalstruc = db.Database.SqlQuery<listapprovalstructure>(sSql).ToList();
            return View(approvalstruc);
        }

        // GET: ApprovalStructure/Edit/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m08AS2 QL_m08AS2;
            string action = "Create";
            if (id == null)
            {
                QL_m08AS2 = new QL_m08AS2();
                QL_m08AS2.cmpcode = Session["CompnyCode"].ToString();
                //QL_m08AS2.crtuser = Session["UserID"].ToString();
                //QL_m08AS2.crttime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                QL_m08AS2 = db.QL_m08AS2.Find(Session["CompnyCode"].ToString(), id);
            }

            if (QL_m08AS2 == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(QL_m08AS2);
            return View(QL_m08AS2);
        }

        // POST: ApprovalStructure/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m08AS2 QL_m08AS2, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (QL_m08AS2.stoid_req == QL_m08AS2.stoid_app)
                ModelState.AddModelError("", "Request dan Approval tidak boleh sama !!");
            if (db.QL_m08AS2.Where(w => w.stoid_req == QL_m08AS2.stoid_req & w.stoid_app == QL_m08AS2.stoid_app & w.mnoid == QL_m08AS2.mnoid & w.asoid != QL_m08AS2.asoid).Count() > 0)
                ModelState.AddModelError("", "Data dengan kombinasi ini sudah ada !!");


            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_m08AS2");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            QL_m08AS2.cmpcode = Session["CompnyCode"].ToString();
                            QL_m08AS2.asoid = mstoid;
                            QL_m08AS2.crttime = servertime;
                            QL_m08AS2.crtuser = Session["UserID"].ToString();
                            QL_m08AS2.updtime = servertime;
                            QL_m08AS2.upduser = Session["UserID"].ToString();
                            db.QL_m08AS2.Add(QL_m08AS2);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m08AS2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            QL_m08AS2.updtime = servertime;
                            QL_m08AS2.upduser = Session["UserID"].ToString();
                            db.Entry(QL_m08AS2).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(QL_m08AS2);
            return View(QL_m08AS2);
        }

        private void setViewBag(QL_m08AS2 QL_m08AS2)
        {
            sSql = " Select * from QL_m01US where usflag='ACTIVE'";
            ViewBag.stoid_req = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql), "usoid", "usname", QL_m08AS2.stoid_req);
            sSql = " Select * from QL_m01US where usflag='ACTIVE'";
            ViewBag.stoid_app = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql), "usoid", "usname", QL_m08AS2.stoid_app);
            sSql = " Select * from QL_m04MN where appflag=2 AND mnflag='ACTIVE'";
            ViewBag.mnoid = new SelectList(db.Database.SqlQuery<QL_m04MN>(sSql), "mnoid", "mnname", QL_m08AS2.mnoid);
        }


        // POST: ApprovalStructure/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_m08AS2 QL_m08AS2 = db.QL_m08AS2.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_m08AS2.Remove(QL_m08AS2);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
