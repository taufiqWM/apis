﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SJJasaReportController : Controller
    {
        // GET: SJJasaReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string sWhere = "";

        public SJJasaReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT shm.custoid FROM QL_trnshipmentitemmst shm WHERE shm.cmpcode='" + CompnyCode + "' AND shm.shipmentitemtype='Jasa'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND shm.shipmentitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSJData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, shm.shipmentitemmstoid [No. Draft], shm.shipmentitemno AS [No. Surat Jalan], CONVERT(varchar(10), shm.shipmentitemdate, 103) [Tgl Surat Jalan], (SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = som.rabmstoid) [Nama Project], som.soitemno [No. Sales Order], shm.shipmentitemmstnote [Note] FROM QL_trnshipmentitemmst shm INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = shm.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=shm.custoid WHERE shm.cmpcode='" + CompnyCode + "' AND shm.shipmentitemtype='Jasa'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND shm.shipmentitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (custcode != "")
                {
                    string[] arr = custcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY shm.shipmentitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSJ");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string sjno, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq , m.itemcode [Kode Barang], m.itemdesc [Nama Barang], g.gndesc [Satuan] FROM QL_mstitem m INNER JOIN QL_trnshipmentitemdtl shd ON shd.itemoid=m.itemoid INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=shd.cmpcode AND shm.shipmentitemmstoid=shd.shipmentitemmstoid INNER JOIN QL_mstcust c ON c.custoid=shm.custoid INNER JOIN QL_m05GN g ON g.gnoid=shd.shipmentitemunitoid WHERE shd.cmpcode='" + CompnyCode + "' AND shm.shipmentitemtype='Jasa'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND shm.shipmentitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (custcode != "")
                {
                    string[] arr = custcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (sjno != "")
                {
                    string[] arr = sjno.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            sWhere = "WHERE shm.cmpcode='" + CompnyCode + "' AND shm.shipmentitemtype='Jasa'";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptSJJasaSumPdf.rpt";
                else
                    rptfile = "rptSJJasaSumPdf.rpt";
                rptname = "SJ_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptSJJasaDtlPdf.rpt";
                else
                {
                    rptfile = "rptSJJasaDtlPdf.rpt";
                }
                rptname = "SJ_DETAIL";
            }

            var Dtl = "";
            var Join = "";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sWhere += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sWhere += " AND shm.shipmentitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhere += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (DDLType == "Detail")
            {
                //SELECT DETAIL
                Dtl += " , shipmentitemdtlseq [No.], itemcode [Code], itemdesc [Material], ISNULL([SO No.],'') AS [SO No.], shd.shipmentitemqty [Qty], g2.gndesc [Unit], g3.gndesc [Warehouse], shipmentitemdtlnote [Detail Note] ,ISNULL(custdtl2picitem,'') [Cust Ship To. Name], ISNULL(custdtl2loc,'') [Cust Ship To. Loc], ISNULL(custdtl2addr,'') [Cust Ship To. Address], ISNULL([City],'') [Cust Ship To. City] ";
                //JOIN DTL
                Join += " INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=shm.cmpcode AND shd.shipmentitemmstoid=shm.shipmentitemmstoid  INNER JOIN QL_mstitem m ON m.itemoid=shd.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid=shipmentitemunitoid INNER JOIN QL_m05GN g3 ON g3.gnoid=shipmentitemwhoid ";
                Join += " INNER JOIN (SELECT som.soitemmstoid, som.soitemno [SO No.] FROM QL_trnsoitemmst som ) AS tblSO ON tblSO.soitemmstoid=shm.soitemmstoid  LEFT JOIN (SELECT custoid, custdtl2oid, custdtl2loc, custdtl2picitem, custdtl2addr, custdtl2cityoid, g4.gndesc [City] FROM QL_mstcustdtl2 cdtl INNER JOIN QL_m05GN g4 ON g4.gnoid=custdtl2cityoid AND g4.gngroup='KOTA')tblCustDtl ON c.custoid=tblCustDtl.custoid --AND dom.custdtl2oid=tblCustDtl.custdtloid ";
            }

            sSql = " SELECT '"+ CompnyName + "' AS [Business Unit], shm.cmpcode [CMPCODE], CONVERT(VARCHAR(20), shm.shipmentitemmstoid) [Draft No.], shm.shipmentitemmstoid [ID], shipmentitemdate [Date], shipmentitemno [Shipment No.], custname [Customer], custcode [Customer Code], shipmentitemmstnote [Header Note], shipmentitemmststatus [Status], shm.createuser [Create User], shm.createtime [Create Date], driveroid [Driver Name], ISNULL(v.vhcdesc,'') [Vehicle], 'FG' AS [Shipment Type], tbl.shipmentitemqty AS Qty " + Dtl + " FROM QL_trnshipmentitemmst shm INNER JOIN QL_mstcust c ON c.custoid=shm.custoid INNER JOIN ( select cmpcode, shipmentitemmstoid, SUM(shipmentitemqty) AS shipmentitemqty from QL_trnshipmentitemdtl group by cmpcode, shipmentitemmstoid) AS tbl ON tbl.cmpcode = shm.cmpcode AND tbl.shipmentitemmstoid = shm.shipmentitemmstoid LEFT JOIN QL_mstvehicle v ON shm.armadaoid=v.vhcoid " + Join + " " + sWhere + " ";

            if (DDLType == "Detail")
            {
                sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetterSmall;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetterSmall;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}