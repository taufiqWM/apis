//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstpartnerdtl1
    {
        public string cmpcode { get; set; }
        public int partneroid { get; set; }
        public int partnerdtl1seq { get; set; }
        public int partnerdtl1oid { get; set; }
        public string partnerdtl1addr { get; set; }
        public int partnercityoid { get; set; }
        public string partnerdtl1phone { get; set; }
        public string partnerdtl1status { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
