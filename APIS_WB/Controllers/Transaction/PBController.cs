﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace APIS_WB.Controllers.Transaction
{
    public class PBController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public PBController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listpo
        {
            public int poitemmstoid { get; set; }
            public string poitemno { get; set; }
            public DateTime poitemdate { get; set; }
            public string projectname { get; set; }
            public string rabno { get; set; }
            public string poitemmstnote { get; set; }
            public int curroid { get; set; }
            public int rabmstoid { get; set; }
            public int rabmstoid_awal { get; set; }
            public string departemen { get; set; }
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
            public string phone { get; set; }
        }

        public class listsales
        {
            public int suppsalesoid { get; set; }
            public string picname { get; set; }
            public string phone { get; set; }
            public string jabatan { get; set; }
            public string produk { get; set; }
        }

        public class listpbdtl2
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "BPB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mritemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmritemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND mritemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }
        private void InitDDL(QL_trnmritemmst tbl)
        {
            var whoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 mritemwhoid from QL_trnmritemdtl where mritemmstoid = '" + tbl.mritemmstoid + "'),0) AS whoid").FirstOrDefault();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var mrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", whoid);
            ViewBag.mritemwhoid = mrwhoid;
            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        private void FillAdditionalField(QL_trnmritemmst tblmst)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname from QL_mstsupp where suppoid = '" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.pono = db.Database.SqlQuery<string>("SELECT poitemno from QL_trnpoitemmst where poitemmstoid = '" + tblmst.pomstoid + "'").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
        }

        private int GenPID(DateTime tanggal)
        {
            string sNo = tanggal.ToString("yy") + "-" + tanggal.ToString("MM") + "-";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno, "+ formatCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND refno LIKE '" + sNo + "%'";
            var counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            return counter;
        }

        private int GenPIDNew()
        {
            var str = ClassFunction.GetServerTime().ToString("yy") + "-"; int counter = 1;
            sSql = "SELECT COUNT(-1) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'' ) LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "'";
            var cek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (cek > 0)
            {
                sSql = "SELECT ISNULL((SELECT TOP 1 CAST(REPLACE(refno,'" + str + "','') AS INTEGER) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'')<>'' AND ISNULL(refno,'') LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "' ORDER BY CAST(REPLACE(refno,'" + str + "','') AS INTEGER) DESC),0) + 1 AS IDNEW";
                counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            return counter;
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPOData(int suppoid)
        {
            List<listpo> tbl = new List<listpo>();

            sSql = "SELECT pm.poitemmstoid, pm.poitemno, r.projectname, r.rabno, pm.poitemdate,  pm.poitemmstnote, pm.curroid, pm.rabmstoid, pm.rabmstoid_awal, de.groupdesc departemen FROM QL_trnpoitemmst pm INNER JOIN QL_trnrabmst r ON pm.rabmstoid = r.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=r.deptoid WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.suppoid= " + suppoid + " AND pm.poitemtype NOT IN ('Jasa') AND poitemmststatus='Approved' ORDER BY poitemdate DESC";
            tbl = db.Database.SqlQuery<listpo>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            List<listsupp> tbl = new List<listsupp>();

            sSql = "SELECT s.suppoid, s.suppname, s.suppaddr, s.suppcode, s.supppaymentoid FROM QL_mstsupp s WHERE activeflag='ACTIVE' AND s.suppoid IN (SELECT suppoid FROM QL_trnpoitemmst where poitemmststatus = 'Approved' AND poitemtype NOT IN ('Jasa')) ORDER BY suppname";
            tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMRDtl2(int itemoid, int podtloid)
        {
            List<listpbdtl2> tbl = new List<listpbdtl2>();

            sSql = "SELECT i.itemcode, i.itemdesc, mrd.refno FROM QL_trnmritemdtl mrd INNER JOIN QL_mstitem i ON i.itemoid=mrd.itemoid WHERE mrd.cmpcode='"+ CompnyCode +"' AND mrd.itemoid="+ itemoid +" AND mrd.podtloid="+ podtloid +" AND i.activeflag='ACTIVE' ORDER BY mrd.refno";
            tbl = db.Database.SqlQuery<listpbdtl2>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class mritemdtl : QL_trnmritemdtl
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string poitemno { get; set; }
            public string poitemdate { get; set; }
            public decimal poitemqty { get; set; }
            public decimal poitemprice { get; set; }
            public decimal qtyos { get; set; }
            public decimal mritemamt { get; set; }
            public string mritemunit { get; set; }
            public string mritemwh { get; set; }
            public string pidtype { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail(int mstoid, int whoid, int mritemmstoid)
        {
            List<mritemdtl> tbl_ori = new List<mritemdtl>();

            sSql = "select 0 mritemdtlseq, 0 mritemdtloid, pd.poitemdtloid podtloid, i.itemoid, i.itemcode, i.itemdesc, '' refno, pm.poitemno, convert(varchar(10),pm.poitemdate, 103) as poitemdate, pd.poitemqty, (pd.poitemqty-isnull((select sum(mritemqty) from QL_trnmritemdtl md where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid and md.mritemmstoid<>"+ mritemmstoid + "),0)) as qtyos, (((pd.poitemqty * pd.poitemprice) - pd.poitemdtldiscamt) / pd.poitemqty) poitemprice, 0.0 mritemqty, pd.poitemunitoid mritemunitoid ,g.gndesc as mritemunit, '' mritemdtlnote, 0.0 mritemvalue, " + whoid + " as mritemwhoid, (select gndesc from ql_m05GN where gnoid=" + whoid + ") as mritemwh, i.pidtype, '' serialnumber FROM ql_trnpoitemdtl pd INNER JOIN ql_trnpoitemmst pm on pm.cmpcode=pd.cmpcode and pd.poitemmstoid=pm.poitemmstoid INNER JOIN QL_mstitem i on i.cmpcode=pd.cmpcode and pd.itemoid=i.itemoid INNER JOIN QL_m05GN g ON g.gnoid=pd.poitemunitoid WHERE i.activeflag='ACTIVE' AND pm.poitemtype NOT IN ('Jasa') AND pd.poitemmstoid=" + mstoid + " /*AND pd.poitemdtlstatus=''*/ AND (pd.poitemqty-isnull((select sum(mritemqty) from QL_trnmritemdtl md where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid and md.mritemmstoid<>" + mritemmstoid + "),0))>0";
            tbl_ori = db.Database.SqlQuery<mritemdtl>(sSql).ToList();

            var tbl = new List<mritemdtl>();
            foreach (var item in tbl_ori)
            {
                if (item.pidtype == "PID")
                {
                    var maxloop = (int)item.qtyos;
                    for (int i = 0; i < maxloop; i++)
                    {
                        item.mritemqty = 1;
                        tbl.Add(item);
                    }
                }
                else
                {
                    item.mritemqty = item.qtyos;
                    tbl.Add(item);
                }
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<mritemdtl>("select rd.mritemdtlseq mritemdtlseq, rd.mritemdtloid mritemdtloid, rd.podtloid, rd.itemoid, i.itemcode, i.itemdesc, rd.refno, pm.poitemno, convert(varchar(10),pm.poitemdate, 103) as poitemdate, pd.poitemqty,  (pd.poitemqty - isnull((select sum(mritemqty) from QL_trnmritemdtl md where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid AND md.mritemmstoid<>rd.mritemmstoid),0.0) + isnull((select sum(pretitemqty) from QL_trnpretitemdtl apd inner join QL_trnmritemdtl md on md.mritemdtloid=apd.mritemdtloid where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid AND md.mritemmstoid<>rd.mritemmstoid),0.0) + isnull((select sum(apretitemqty) from QL_trnapretitemdtl retd inner join QL_trnapitemdtl apd on apd.apitemdtloid=retd.apitemdtloid inner join QL_trnmritemdtl md on md.mritemdtloid=apd.mritemdtloid where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid AND md.mritemmstoid<>rd.mritemmstoid),0.0)) as qtyos, (((pd.poitemqty * pd.poitemprice) - pd.poitemdtldiscamt) / pd.poitemqty) poitemprice, rd.mritemqty, rd.mritemunitoid, g.gndesc as mritemunit, rd.mritemdtlnote mritemdtlnote, rd.mritemvalue, rd.mritemwhoid, g2.gndesc as mritemwh, i.pidtype, ISNULL(rd.serialnumber,'') serialnumber FROM QL_trnmritemdtl rd inner join ql_trnpoitemdtl pd on rd.podtloid=pd.poitemdtloid inner join ql_trnpoitemmst pm on pd.poitemmstoid=pm.poitemmstoid INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.mritemunitoid inner join QL_m05GN g2 on g2.gnoid=rd.mritemwhoid WHERE rd.cmpcode='" + CompnyCode + "' AND pm.poitemtype NOT IN ('Jasa') AND rd.mritemmstoid=" + id + " ORDER BY rd.mritemdtlseq").ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        // GET/POST: poitem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No PB",["Supplier"] = "Supplier",["Project"] = "Project",["NomorPO"] = "No PO" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select pr.mritemmstoid, pr.mritemno, pr.mritemtype, pr.mritemdate, c.suppname, ISNULL(rm.projectname,'') projectname, ISNULL((SELECT c.custname FROM QL_mstcust c WHERE ISNULL(c.custoid,0)=ISNULL(rm.custoid,0)),'') custname, pom.poitemmstoid, pom.poitemno, pr.mritemmststatus FROM QL_trnmritemmst pr INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid=pr.pomstoid INNER JOIN QL_mstsupp c ON c.suppoid=pr.suppoid LEFT JOIN QL_trnrabmst rm ON ISNULL(pr.rabmstoid,0)=ISNULL(rm.rabmstoid,0) WHERE pr.mritemtype NOT IN ('Jasa') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.mritemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.mritemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.mritemmstoid LIKE'%"+ param.filtertext +"%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.mritemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "NomorPO") sSql += " AND t.poitemno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.mritemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.mritemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.mritemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.mritemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmritemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnmritemmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.mritemno = generateNo(ClassFunction.GetServerTime());
                tblmst.mritemmstoid = ClassFunction.GenerateID("QL_trnmritemmst");
                tblmst.rabmstoid = 0;
                tblmst.mritemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.upduser = Session["UserID"].ToString();

                ViewBag.mritemdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.tglsjsupp = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnmritemmst.Find(Session["CompnyCode"].ToString(), id);

                ViewBag.mritemdate = tblmst.mritemdate.ToString("dd/MM/yyyy");
                ViewBag.tglsjsupp = tblmst.tglsjsupp.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tblmst.updtime.ToString("dd/MM/yyyy");
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmritemmst tblmst, List<mritemdtl> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed"; var isdoubleinput = false; var isdoublepost = false;
            var servertime = ClassFunction.GetServerTime();

            //is Input Valid 
            if (string.IsNullOrEmpty(tblmst.mritemno))
            {
                tblmst.mritemno = generateNo(ClassFunction.GetServerTime());
            }
            if (action == "Create")
            {
                tblmst.mritemno = generateNo(ClassFunction.GetServerTime());
            }
            if (string.IsNullOrEmpty(tblmst.mritemtype))
                tblmst.mritemtype = "";
            if (string.IsNullOrEmpty(tblmst.mritemno))
                tblmst.mritemno = "";
            if (string.IsNullOrEmpty(tblmst.nosjsupp))
                tblmst.nosjsupp = "";
            if (string.IsNullOrEmpty(tblmst.mritemmstnote))
                tblmst.mritemmstnote = "";
            if (string.IsNullOrEmpty(tblmst.mritemmstres1))
                tblmst.mritemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.mritemmstres2))
                tblmst.mritemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.mritemmstres3))
                tblmst.mritemmstres3 = "";
            if (string.IsNullOrEmpty(tblmst.curroid.ToString()))
                tblmst.curroid = 1;
            if (string.IsNullOrEmpty(tblmst.nosjsupp))
                tblmst.nosjsupp = "";

            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tblmst.mritemmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    msg += "- " + cRate.GetRateMonthlyLastError + "<br>";
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            sSql = "SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tblmst.suppoid + "";
            string suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (dtDtl == null)
                msg += "- Please fill detail data!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br>";
            else
            {
                if (dtDtl != null)
                {
                    if (dtDtl.Count > 0)
                    {
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            decimal dQty = 0;
                            if (dtDtl[i].pidtype == "PID")
                            {
                                var a = dtDtl.Where(x => x.itemoid == dtDtl[i].itemoid && x.podtloid == dtDtl[i].podtloid).GroupBy(x => x.itemoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.mritemqty) }).ToList();
                                for (int j = 0; j < a.Count(); j++)
                                {
                                    dQty = a[j].Qty;
                                }
                            }
                            else
                            {
                                dQty = dtDtl[i].mritemqty;
                            }

                            sSql = "select (pd.poitemqty - isnull((select sum(mritemqty) from QL_trnmritemdtl md where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid AND md.mritemmstoid<>" + tblmst.mritemmstoid + "),0.0) + isnull((select sum(pretitemqty) from QL_trnpretitemdtl apd inner join QL_trnmritemdtl md on md.mritemdtloid=apd.mritemdtloid where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid AND md.mritemmstoid<>" + tblmst.mritemmstoid + "),0.0) + isnull((select sum(apretitemqty) from QL_trnapretitemdtl retd inner join QL_trnapitemdtl apd on apd.apitemdtloid=retd.apitemdtloid inner join QL_trnmritemdtl md on md.mritemdtloid=apd.mritemdtloid where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid AND md.mritemmstoid<>" + tblmst.mritemmstoid + "),0.0)) as qtyos FROM ql_trnpoitemdtl pd inner join ql_trnpoitemmst pm on pd.poitemmstoid=pm.poitemmstoid INNER JOIN QL_mstitem i ON i.itemoid=pd.itemoid WHERE pd.cmpcode='" + CompnyCode + "' AND pm.poitemtype NOT IN ('Jasa') AND pd.poitemdtloid=" + dtDtl[i].podtloid + "";
                            var dQtyOS = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (dQtyOS != dtDtl[i].qtyos)
                            {
                                dtDtl[i].qtyos = dQtyOS;
                            }
                            //Cek Qty MR > OS
                            if (dQty > dtDtl[i].qtyos)
                            {
                                msg += "- " + dtDtl[i].itemdesc + ", Qty Terima > Qty OS!<br>";
                            }
                            if (string.IsNullOrEmpty(dtDtl[i].serialnumber))
                                dtDtl[i].serialnumber = "";
                            if (string.IsNullOrEmpty(dtDtl[i].mritemdtlnote))
                                dtDtl[i].mritemdtlnote = "";

                            sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                            var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                            if (tblmst.mritemmststatus == "Post")
                            {
                                if (type != "Ongkir")
                                {
                                    //Stock Value
                                    decimal sValue = dtDtl[i].poitemprice * cRate.GetRateMonthlyIDRValue;
                                    decimal sAvgValue = 0;
                                    sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dQty, sValue, "IN");
                                    if (dtDtl[i].pidtype == "PID")
                                    {
                                        dtDtl[i].mritemvalue = Math.Round((sAvgValue > 0 ? (sAvgValue / dQty) : sAvgValue), 4);
                                        dtDtl[i].mritemvalueidr = Math.Round((sAvgValue > 0 ? (sAvgValue / dQty) : sAvgValue), 4);
                                    }
                                    else
                                    {
                                        dtDtl[i].mritemvalue = Math.Round(sAvgValue, 4);
                                        dtDtl[i].mritemvalueidr = Math.Round(sAvgValue, 4);
                                    }
                                    dtDtl[i].mritemamt = dtDtl[i].poitemprice * dtDtl[i].mritemqty;
                                }
                                else
                                {
                                    dtDtl[i].mritemvalue = 0;
                                    dtDtl[i].mritemvalueidr = 0;
                                    dtDtl[i].mritemamt = 0;
                                }
                            }
                            else
                            {
                                dtDtl[i].mritemvalue = 0;
                                dtDtl[i].mritemvalueidr = 0;
                                dtDtl[i].mritemamt = 0;
                            }
                        }
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.mritemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                tblmst.mritemmststatus = "In Process";
            }
            if (tblmst.mritemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                    tblmst.mritemmststatus = "In Process";
                }
            }

            if (tblmst.mritemmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                    msg += "- " + ClassFunction.GetInterfaceWarning("VAR_STOCK") + "<br>";
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", CompnyCode))
                    msg += "- " + ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED") + "<br>";
            }

            if (msg == "")
            {
                decimal dSumMR = 0; decimal dSumMRIDR = 0;
                tblmst.rate2oid = rate2oid;

                int PIDNew = 0;
                if (tblmst.mritemmststatus == "Post")
                {
                    PIDNew = GenPIDNew();
                }

                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", CompnyCode));

                var mstoid = ClassFunction.GenerateID("QL_trnmritemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmritemdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");

                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.Database.ExecuteSqlCommand($"UPDATE QL_trnmritemmst SET createtime='{tblmst.createtime}' WHERE createtime='{tblmst.createtime}'") > 0) isdoubleinput = true;
                            else
                            {
                                //Insert
                                tblmst.mritemmstoid = mstoid;
                                tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                tblmst.createtime = servertime;
                                tblmst.createuser = Session["UserID"].ToString();
                                tblmst.updtime = servertime;
                                tblmst.upduser = Session["UserID"].ToString();
                                db.QL_trnmritemmst.Add(tblmst);
                                db.SaveChanges();

                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnmritemmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else if (action == "Edit")
                        {
                            if (db.QL_trnmritemmst.AsNoTracking().FirstOrDefault(t => t.cmpcode == tblmst.cmpcode && t.mritemmstoid == tblmst.mritemmstoid).mritemmststatus.ToUpper() == "IN PROCESS")
                            {
                                //Update
                                tblmst.updtime = servertime;
                                tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                tblmst.upduser = Session["UserID"].ToString();
                                db.Entry(tblmst).State = EntityState.Modified;
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid IN (SELECT podtloid FROM QL_trnmritemdtl WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + tblmst.mritemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid IN (SELECT mm.pomstoid FROM QL_trnmritemdtl md INNER JOIN QL_trnmritemmst mm ON mm.mritemmstoid = md.mritemmstoid WHERE mm.cmpcode='" + CompnyCode + "' AND mm.mritemmstoid=" + tblmst.mritemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                var trndtl = db.QL_trnmritemdtl.Where(b => b.mritemmstoid == tblmst.mritemmstoid && b.cmpcode == tblmst.cmpcode);
                                db.QL_trnmritemdtl.RemoveRange(trndtl);
                                db.SaveChanges();
                            }
                            else if (tblmst.mritemmststatus.ToUpper() == "POST") isdoublepost = true;
                        }

                        if (!isdoubleinput && !isdoublepost)
                        {
                            var tbldtl = new List<QL_trnmritemdtl>();
                            foreach (var item in dtDtl)
                            {
                                sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + item.itemoid + "";
                                var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                                var new_dtl = (QL_trnmritemdtl)ClassFunction.MappingTable(new QL_trnmritemdtl(), item);
                                if (string.IsNullOrEmpty(item.mritemdtlstatus)) new_dtl.mritemdtlstatus = "";
                                if (string.IsNullOrEmpty(item.mritemdtlnote)) new_dtl.mritemdtlnote = "";
                                new_dtl.cmpcode = tblmst.cmpcode;
                                new_dtl.mritemdtloid = dtloid++;
                                new_dtl.mritemmstoid = tblmst.mritemmstoid;
                                new_dtl.mritembonusqty = 0;
                                new_dtl.mritemvalueusd = 1;
                                new_dtl.mritemdtlres1 = "";
                                new_dtl.mritemdtlres2 = "";
                                new_dtl.mritemdtlres3 = "";
                                new_dtl.refno = (item.pidtype == "PID" ? ClassFunction.GetServerTime().ToString("yy") + "-" + PIDNew.ToString() : "");
                                new_dtl.serialnumber = item.serialnumber ?? "";
                                new_dtl.upduser = tblmst.upduser;
                                new_dtl.updtime = tblmst.updtime;
                                tbldtl.Add(new_dtl);

                                if (tblmst.mritemmststatus == "Post")
                                {
                                    PIDNew = (item.pidtype == "PID" ? (PIDNew + 1) : PIDNew);
                                }

                                if (tblmst.mritemmststatus == "Post")
                                {
                                    if (type != "Ongkir")
                                    {
                                        // Insert QL_conmat
                                        db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "PBFG", "QL_trnmritemdtl", tblmst.mritemmstoid, item.itemoid, "FINISH GOOD", item.mritemwhoid, item.mritemqty, "Penerimaan Barang", tblmst.mritemno + " | " + suppname, Session["UserID"].ToString(), new_dtl.refno, item.mritemvalue, 0, 0, null, new_dtl.mritemdtloid, tblmst.rabmstoid_awal, item.mritemamt, new_dtl.serialnumber));

                                        dSumMR += item.mritemamt;
                                        dSumMRIDR += item.mritemamt;
                                    }
                                }

                                decimal dQty = 0;
                                if (item.pidtype == "PID")
                                {
                                    var a = dtDtl.Where(x => x.itemoid == item.itemoid && x.podtloid == item.podtloid).GroupBy(x => x.itemoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.mritemqty) }).ToList();
                                    for (int j = 0; j < a.Count(); j++)
                                    {
                                        dQty = a[j].Qty;
                                    }
                                }
                                else
                                {
                                    dQty = item.mritemqty;
                                }

                                if (dQty >= item.qtyos)
                                {
                                    sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid=" + item.podtloid;
                                    db.Database.ExecuteSqlCommand(sSql);

                                    sSql = "UPDATE QL_trnrabmst SET statusbeli='PB PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tblmst.rabmstoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);

                                    sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND poitemtype NOT IN ('Jasa') AND poitemmstoid=" + tblmst.pomstoid + " AND (SELECT COUNT(*) FROM QL_trnpoitemdtl pd WHERE pd.cmpcode='" + CompnyCode + "' AND pd.poitemdtlstatus='' AND pd.poitemmstoid=" + tblmst.pomstoid + " AND pd.poitemdtloid<>" + item.podtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);

                                    sSql = "UPDATE QL_trnrabmst SET statusbeli='PB' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tblmst.rabmstoid + " AND (SELECT COUNT(*) FROM QL_trnpoitemdtl pd WHERE pd.cmpcode='" + CompnyCode + "' AND pd.poitemdtlstatus='' AND pd.poitemmstoid=" + tblmst.pomstoid + " AND pd.poitemdtloid<>" + item.podtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            db.QL_trnmritemdtl.AddRange(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnmritemdtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tblmst.mritemmststatus == "Post")
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.mritemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                var glseq = 1;
                                // Insert QL_trngldtl
                                // D : Persediaan Barang 
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidStock, "D", dSumMR, tblmst.mritemno, tblmst.mritemno + " | " + suppname + " | " + tblmst.mritemmstnote, "Post", Session["UserID"].ToString(), servertime, dSumMRIDR, 0, "QL_trnmritemmst " + tblmst.mritemmstoid, null, null, null, 0));
                                db.SaveChanges();
                                // C : Penerimaan Barang
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "C", dSumMR, tblmst.mritemno, tblmst.mritemno + " | " + suppname + " | " + tblmst.mritemmstnote, "Post", Session["UserID"].ToString(), servertime, dSumMRIDR, 0, "QL_trnmritemmst " + tblmst.mritemmstoid, null, null, null, 0));
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }//END double input post

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmritemmst tblmst = db.QL_trnmritemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid IN (SELECT podtloid FROM QL_trnmritemdtl WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND poitemtype NOT IN ('Jasa') AND poitemmstoid IN (SELECT pomstoid FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.mritemmstoid = mrd.mritemmstoid WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mritemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusbeli='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tblmst.rabmstoid + " ";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusbeli='PB PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tblmst.rabmstoid + " AND (SELECT COUNT(*) FROM QL_trnpoitemdtl pd WHERE pd.cmpcode='" + CompnyCode + "' AND pd.poitemdtlstatus='Complete' AND pd.poitemmstoid=" + tblmst.pomstoid + ")>0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //sSql = "UPDATE QL_trnrabmst SET statusbeli='PB PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnpoitemmst pm INNER JOIN QL_trnmritemmst rm ON rm.pomstoid = pm.poitemmstoid AND rm.mritemtype NOT IN ('Jasa') WHERE pm.cmpcode='" + CompnyCode + "' AND rm.mritemmstoid=" + id + " AND pm.poitemtype NOT IN ('Jasa'))";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        //sSql = "UPDATE QL_trnrabmst SET statusbeli='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnpoitemmst pm INNER JOIN QL_trnmritemmst rm ON rm.pomstoid = pm.poitemmstoid AND rm.mritemtype NOT IN ('Jasa') WHERE pm.cmpcode='" + CompnyCode + "' AND rm.mritemmstoid=" + id + " AND pm.poitemtype NOT IN ('Jasa')) AND (SELECT COUNT(*) FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = pod.poitemmstoid AND pom.poitemtype NOT IN ('Jasa') WHERE pom.cmpcode='" + CompnyCode + "' AND poitemdtlstatus='' AND pom.rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnmritemmst mm INNER JOIN QL_trnpoitemmst pm ON pm.poitemmstoid = mm.pomstoid AND pm.poitemtype NOT IN ('Jasa') where mm.mritemmstoid <> " + id + " AND mm.mritemtype NOT IN ('Jasa')))=0";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        var trndtl = db.QL_trnmritemdtl.Where(a => a.mritemmstoid == id);
                        db.QL_trnmritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        //var trndtl2 = db.QL_trnmritemdtl2.Where(a => a.mritemmstoid == id);
                        //db.QL_trnmritemdtl2.RemoveRange(trndtl2);
                        //db.SaveChanges();

                        db.QL_trnmritemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: MR/Revise
        [HttpPost, ActionName("Revise")]
        [ValidateAntiForgeryToken]
        public ActionResult ReviseConfirmed(int? id, string nosjsupp, string tglsjsupp, string reqoid, string note)
        {
            var tglsj = DateTime.Parse(ClassFunction.toDate(tglsjsupp));

            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmritemmst tblmst = db.QL_trnmritemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmritemmst SET nosjsupp='"+ nosjsupp + "', tglsjsupp = '"+ tglsj + "', mritemmstres3 = '"+ reqoid +"', mritemmstnote = '"+ note +"', updtime = '"+ servertime +"', upduser = '"+ Session["UserID"].ToString() + "' WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();                       

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT mritemno FROM QL_trnmritemmst WHERE mritemmstoid=" + id + "";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPBItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptPBItem_"+ sNo +".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}