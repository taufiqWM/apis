//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_mstjasa
    {
        public string cmpcode { get; set; }
        public int jasaoid { get; set; }
        public string jasacode { get; set; }
        public string jasadesc { get; set; }
        public int acctgoid { get; set; }
        public int jasaunitoid { get; set; }
        public string jasanote { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public string activeflag { get; set; }
        public string jasatype { get; set; }
        public Nullable<int> rabdtloid4_draft { get; set; }
        public Nullable<int> rabmstoid_draft { get; set; }
    }
}
