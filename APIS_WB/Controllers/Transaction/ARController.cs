﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class ARController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
        private string sSql = "";

        public ARController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class viewindex
        {
            public string cmpcode { get; set; }
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public DateTime assetdate { get; set; }
            public string reftype { get; set; }
            public string refcode { get; set; }
            public string reflongdesc { get; set; }
            public string assetmstnote { get; set; }
            public string deptname { get; set; }
            public string divname { get; set; }
            public string revaluser { get; set; }
        }

        public class assetmst
        {
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public string refcode { get; set; }
            public string reflongdesc { get; set; }
            public decimal assetvalue { get; set; }
            public decimal assetvalueusd { get; set; }
            public int assetacctgoid { get; set; }
            public int accumdepacctgoid { get; set; }
            public int assetdepmonth { get; set; }
            public int depacctgoid { get; set; }
            public string startperiod { get; set; }
            public int startseq { get; set; }

        }

        public class assetdtl
        {
            public int assetdtlseq { get; set; }
            public int assetmonth { get; set; }
            public int assetyear { get; set; }
            public string assetperiod { get; set; }
            public string assetdtlres1 { get; set; }
            public decimal assetperiodvalue { get; set; }
            public decimal assetperiodvalueidr { get; set; }
            public decimal assetperiodvalueusd { get; set; }
            public decimal assetperioddep { get; set; }
            public decimal assetperioddepidr { get; set; }
            public decimal assetperioddepusd { get; set; }
            public int periodacctgoid { get; set; }
            public int perioddepacctgoid { get; set; }
            public string assetdtlnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetAssetData(int deptoid)
        {
            List<assetmst> tbl = new List<assetmst>();
            sSql = "SELECT assetmstoid, assetno, reftype, refoid, refcode, reflongdesc, assetvalue, assetvalueidr, assetvalueusd, assetacctgoid, accumdepacctgoid, assetdepmonth, depacctgoid, startperiod, startseq FROM ( SELECT am.assetmstoid, assetno, am.reftype, am.refoid, (SELECT itemcode FROM QL_mstitem m WHERE itemoid = am.refoid) AS refcode, (SELECT itemdesc FROM QL_mstitem m WHERE itemoid = am.refoid) AS reflongdesc, (assetpurchase - ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus <> ''), 0.0)) AS assetvalue, (assetpurchaseidr - ISNULL((SELECT SUM(assetperiodvalueidr) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus <> ''), 0.0)) AS assetvalueidr, (assetpurchaseusd - ISNULL((SELECT SUM(assetperiodvalueusd) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus <> ''), 0.0)) AS assetvalueusd, assetacctgoid, accumdepacctgoid, (assetdepmonth - ISNULL((SELECT COUNT(*) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus <> ''), 0.0)) assetdepmonth, depacctgoid, (SELECT MIN(ad.assetperiod) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus = '') startperiod, (SELECT MIN(ad.assetdtlseq) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus = '') startseq FROM QL_assetmst am WHERE am.cmpcode = '"+ CompnyCode +"' AND am.deptoid = " + deptoid + " AND ISNULL(assetmstres1, '')<> 'Asset In Progress' AND assetmststatus = 'Post') AS QL_assetmst  ORDER BY assetno";

            tbl = db.Database.SqlQuery<assetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<assetdtl> dtDtl)
        {
            Session["QL_assetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_assetdtl"] == null)
            {
                Session["QL_assetdtl"] = new List<assetdtl>();
            }

            List<assetdtl> dataDtl = (List<assetdtl>)Session["QL_assetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET: FixedAssetsRevaluation
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "normal";

            sSql = "SELECT * FROM (";
            sSql += "SELECT am.cmpcode, assetmstoid, assetno, revaltime AS assetdate, reftype, (SELECT itemcode FROM QL_mstitem m WHERE itemoid=refoid) AS refcode, (SELECT itemdesc FROM QL_mstitem m WHERE itemoid=refoid) AS reflongdesc, assetmstnote, deptname, revaluser FROM QL_assetmst am INNER JOIN QL_mstdept de ON de.cmpcode=am.cmpcode AND de.deptoid=am.deptoid WHERE am.cmpcode = '"+ CompnyCode + "' AND revaluser<>'' ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND am.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " AND am.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND revaltime>=CAST('" + modfil.filterperiodfrom + " 00:00:00' AS DATETIME) AND revaltime<=CAST('" + modfil.filterperiodto + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }

            sSql += ") AS QL_assetmst ORDER BY CAST(assetdate AS DATETIME) DESC, assetmstoid DESC";

            List<viewindex> dt = db.Database.SqlQuery<viewindex>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: AssetsInProgressClosing/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_assetmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_assetmst();
                tbl.assetdate = ClassFunction.GetServerTime();
                tbl.revaltime = ClassFunction.GetServerTime();
                tbl.revaluser = Session["UserID"].ToString();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.assetmststatus = "Post";

                Session["QL_assetdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_assetmst.Find(CompnyCode, id);
                FillAdditionalField(tbl);

                sSql = "SELECT assetdtlseq, CONVERT(VARCHAR(2), assetmonth) AS assetmonth, assetyear, assetperiod, assetdtlres1, assetperiodvalue, assetperioddep, periodacctgoid, perioddepacctgoid, assetdtlstatus, assetdtlnote FROM QL_assetdtl ad WHERE ad.cmpcode='" + CompnyCode + "' AND assetmstoid=" + id + " AND revalflag<>'' ORDER BY assetdtlseq";
                var tbldtl = db.Database.SqlQuery<assetdtl>(sSql).ToList();

                Session["QL_assetdtl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_assetmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            //is Input Valid
            try
            {
                tbl.assetdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("assetdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tbl.cmpcode))
                tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.reftype))
                tbl.reftype = "Asset";
            if (string.IsNullOrEmpty(tbl.assetmstnote))
                tbl.assetmstnote = "";
            if (string.IsNullOrEmpty(tbl.assetmstres1))
                tbl.assetmstres1 = "";
            if (string.IsNullOrEmpty(tbl.assetmstres2))
                tbl.assetmstres2 = "";
            if (string.IsNullOrEmpty(tbl.assetmstres3))
                tbl.assetmstres3 = "";           

            //is Input Detail Valid
            List<assetdtl> dtDtl = (List<assetdtl>)Session["QL_assetdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].assetdtlnote))
                            dtDtl[i].assetdtlnote = "";
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.assetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
            }
            if (tbl.assetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                }
            }

            var rate2oid = 0;
            var cRate = new ClassRate();

            if (ModelState.IsValid)
            {          
                var assetdtloid = ClassFunction.GenerateID("QL_assetdtl");            
                var assetmstoid = ClassFunction.GenerateID("QL_assetmst");

                tbl.rateoid = 1;
                tbl.rate2oid = rate2oid;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Update
                            sSql = "UPDATE QL_assetmst SET revaluser='" + Session["UserID"].ToString() + "', revaltime='"+ ClassFunction.GetServerTime() +"', assetmstres1 = 'Asset Revaluation " + sPeriod +"' WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + tbl.assetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_assetdtl SET assetdtlstatus='Complete', revalflag = 'REVAL' WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + tbl.assetmstoid + " ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            QL_assetmst tblass;
                            tblass = new QL_assetmst();
                            tblass.cmpcode = tbl.cmpcode;
                            tblass.assetmstoid = assetmstoid;
                            tblass.periodacctg = sPeriod;
                            tblass.assetno = "";

                            tblass.assetdate = tbl.assetdate;
                            tblass.reftype = "Asset";
                            tblass.refoid = tbl.refoid;
                            tblass.assetacctgoid = tbl.assetacctgoid;
                            tblass.assetpurchase = tbl.assetvalue;
                            tblass.assetpurchaseidr = tbl.assetvalue;
                            tblass.assetpurchaseusd = 1;
                            tblass.assetvalue = tbl.assetvalue;
                            tblass.assetvalueidr = tbl.assetvalue;
                            tblass.assetvalueusd = 1;
                            tblass.assetaccumdep = 0;
                            tblass.assetaccumdepidr = 0;
                            tblass.assetaccumdepusd = 0;
                            tblass.accumdepacctgoid = tbl.accumdepacctgoid;
                            tblass.assetdepmonth = tbl.assetdepmonth;
                            tblass.assetdepvalue = tbl.assetdepvalue;
                            tblass.assetdepvalueidr = tbl.assetdepvalue;
                            tblass.assetdepvalueusd = 1;
                            tblass.depacctgoid = tbl.depacctgoid;
                            tblass.assetmststatus = "Post";
                            tblass.assetmstres1 = "Asset In Progress";
                            tblass.assetmstres2 = tbl.assetmstoid.ToString();
                            tblass.assetmstres3 = "";
                            tblass.assetmstnote = "";
                            tblass.createuser = tbl.createuser;
                            tblass.createtime = tbl.createtime;
                            tblass.upduser = Session["UserID"].ToString();
                            tblass.updtime = ClassFunction.GetServerTime();
                            tblass.deptoid = tbl.deptoid;
                            tblass.curroid = tbl.curroid;
                            tblass.rateoid = tbl.rateoid;
                            tblass.rate2oid = tbl.rate2oid;
                            tblass.assetdepdate = tbl.assetdate;
                            tblass.inprogacctgoid = 0;
                            tblass.closeuser = "";
                            tblass.closetime = new DateTime(1900, 01, 01);
                            tblass.assetmstflag = "";
                            tblass.solduser = "";
                            tblass.soldtime = new DateTime(1900, 01, 01);
                            tblass.shipmentuser = "";
                            tblass.shipmenttime = new DateTime(1900, 01, 01);
                            tblass.disposeuser = "";
                            tblass.disposetime = new DateTime(1900, 01, 01);
                            tblass.shipmentamt = 0;
                            tblass.shipmentamtidr = 0;
                            tblass.shipmentamtusd = 0;
                            tblass.revaluser = "";
                            tblass.revaltime = new DateTime(1900, 01, 01);
                            tblass.disposeacctgoid = 0;
                            tblass.mtrwhoid = 0;

                            db.QL_assetmst.Add(tblass);
                            db.SaveChanges();

                            QL_assetdtl dtlass;
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                dtlass = new QL_assetdtl();
                                dtlass.cmpcode = tbl.cmpcode;
                                dtlass.assetdtloid = assetdtloid++;
                                dtlass.assetmstoid = tblass.assetmstoid;
                                dtlass.assetdtlseq = i + 1;
                                dtlass.assetmonth = dtDtl[i].assetmonth;
                                dtlass.assetyear = dtDtl[i].assetyear;
                                dtlass.assetperiod = dtDtl[i].assetperiod;
                                dtlass.assetperiodvalue = dtDtl[i].assetperiodvalue;
                                dtlass.assetperiodvalueidr = dtDtl[i].assetperiodvalue;
                                dtlass.assetperiodvalueusd = 1;
                                dtlass.periodacctgoid = dtDtl[i].periodacctgoid;
                                dtlass.assetperioddep = dtDtl[i].assetperioddep;
                                dtlass.assetperioddepidr = dtDtl[i].assetperioddep;
                                dtlass.assetperioddepusd = 1;
                                dtlass.perioddepacctgoid = dtDtl[i].perioddepacctgoid;
                                dtlass.assetdtlstatus = "";
                                dtlass.assetdtlres1 = "";
                                dtlass.assetdtlres2 = "";
                                dtlass.assetdtlres3 = "";
                                dtlass.assetdtlnote = "";
                                dtlass.postuser = "";
                                dtlass.postdate = new DateTime(1900, 01, 01);
                                dtlass.upduser = Session["UserID"].ToString();
                                dtlass.updtime = ClassFunction.GetServerTime();
                                dtlass.revalflag = "";

                                db.QL_assetdtl.Add(dtlass);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (assetdtloid - 1) + " WHERE tablename='QL_assetdtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + assetmstoid + " WHERE tablename='QL_assetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //nothing
                        }
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                       
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private void InitDDL(QL_assetmst tbl)
        {
            var cmp = CompnyCode;

            var assetacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, "VAR_ASSET")).ToList(), "acctgoid", "acctgdesc", tbl.assetacctgoid);
            ViewBag.assetacctgoid = assetacctgoid;

            var accumdepacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, "VAR_ASSET_ACCUM")).ToList(), "acctgoid", "acctgdesc", tbl.accumdepacctgoid);
            ViewBag.accumdepacctgoid = accumdepacctgoid;

            var depacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(cmp, "VAR_DEPRECIATION")).ToList(), "acctgoid", "acctgdesc", tbl.depacctgoid);
            ViewBag.depacctgoid = depacctgoid;

            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(GetQueryInitDDLDepartment()).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;
        }

        private string GetQueryInitDDLDepartment()
        {
            var result = "SELECT * FROM QL_mstdept WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            return result;
        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private void FillAdditionalField(QL_assetmst tbl)
        {
            ViewBag.startperiod = db.Database.SqlQuery<string>("SELECT (SELECT MIN(ad.assetperiod) FROM QL_assetdtl ad WHERE ad.cmpcode = am.cmpcode AND ad.assetmstoid = am.assetmstoid AND assetdtlstatus = '') FROM QL_assetmst am where am.assetmstoid = "+ tbl.assetmstoid +"").FirstOrDefault();
        }
    }
}