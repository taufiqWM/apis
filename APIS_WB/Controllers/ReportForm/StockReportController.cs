﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class StockReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public StockReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_conmat tbl)
        {
            sSql = "SELECT * FROM QL_m05GN g WHERE gngroup='GUDANG' ORDER BY g.gndesc";
            var DDLWH = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.mtrwhoid);
            ViewBag.DDLWH = DDLWH;
        }

        private void InitDDL1(QL_mstitem tbl1)
        {
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1code", "cat1shortdesc", tbl1.cat1code);
            ViewBag.cat1code = cat1code;

            sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1code='" + tbl1.cat1code + "' AND activeflag='ACTIVE'";
            var cat2code = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2code", "cat2shortdesc", tbl1.cat2code);
            ViewBag.cat2code = cat2code;

            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + CompnyCode + "' AND cat1code= '" + tbl1.cat1code + "'  AND cat2code= '" + tbl1.cat2code + "' AND  activeflag='ACTIVE'";
            var cat3code = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3code", "cat3shortdesc", tbl1.cat3code);
            ViewBag.cat3code = cat3code;

            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "' AND cat1code= '" + tbl1.cat1code + "'  AND cat2code= '" + tbl1.cat2code + "'   AND cat3code= '" + tbl1.cat3code + "' AND activeflag ='ACTIVE'";
            var cat4code = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4shortdesc", tbl1.cat4code);
            ViewBag.cat4code = cat4code;
        }

        [HttpPost]
        public ActionResult Getcat2(string[] cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE'";

            if (cat1 !=null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat3(string[] cat1, string[] cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            sSql = "SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE'";

            if(cat1 !=null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 !=null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }
            
            sSql += " ORDER BY cat3code";
            tbl = db.Database.SqlQuery<QL_mstcat3>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat4(string[] cat1, string[] cat2, string[] cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            sSql = "SELECT * FROM QL_mstcat4 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 != null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            if (cat3 != null)
            {
                if (cat3.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3.Count(); i++)
                    {
                        stsval3 += "'" + cat3[i] + "',";
                    }
                    sSql += " AND cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat4code";
            tbl = db.Database.SqlQuery<QL_mstcat4>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] cat1, string[] cat2, string[] cat3, string[] cat4)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "SELECT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_m05GN g ON g.gnoid=m.itemunitoid WHERE m.cmpcode='" + CompnyCode + "' ";

                if (cat1 !=null)
                {
                    if (cat1.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < cat1.Count(); i++)
                        {
                            stsval += "'" + cat1[i] + "',";
                        }
                        sSql += " AND cat1code IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (cat2 != null)
                {
                    if (cat2.Count() > 0)
                    {
                        string stsval1 = "";
                        for (int i = 0; i < cat2.Count(); i++)
                        {
                            stsval1 += "'" + cat2[i] + "',";
                        }
                        sSql += " AND cat2code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                    }
                } 
                
                if(cat3 != null)
                {
                    if (cat3.Count() > 0)
                    {
                        string stsval2 = "";
                        for (int i = 0; i < cat3.Count(); i++)
                        {
                            stsval2 += "'" + cat3[i] + "',";
                        }
                        sSql += " AND cat3code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                    }
                }

                if (cat4 != null)
                {
                    if (cat4.Count() > 0)
                    {
                        string stsval3 = "";
                        for (int i = 0; i < cat4.Count(); i++)
                        {
                            stsval3 += "'" + cat4[i] + "',";
                        }
                        sSql += " AND cat4code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                    }
                }
                
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_conmat tbl;
            tbl = new QL_conmat();
            if (tbl == null)
                return HttpNotFound();
            InitDDL(tbl);
            
            QL_mstitem tbl1;
            tbl1 = new QL_mstitem();
            if (tbl1 == null)
                return HttpNotFound();
            InitDDL1(tbl1);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLWH, string StartPeriod, string EndPeriod, string DDLPeriod, string TextMaterial, string reporttype, string[] cat1code, string[] cat2code, string[] cat3code, string[] cat4code, string DDLopQty, string DDLPid, string TextPid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptStockSumXls.rpt";
                }
                else
                {
                    rptfile = "rptStockSumPdf.rpt";
                }    
                rptname = "Stock_SUMMARY";
            }
            else if (DDLType == "Summary PID")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptStockSumPIDXls.rpt";
                }
                else
                {
                    rptfile = "rptStockSumPIDPdf.rpt";
                }
                rptname = "Stock_SUMMARY_PID";
            }
            else if (DDLType == "Draft")
            {
                rptfile = "rptStockDraftPdf.rpt";
                rptname = "Stock_Draft";
            }
            else
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptStockDtlXls.rpt";
                }
                else
                {
                    rptfile = "rptStockDtlPdf.rpt";
                }  
                rptname = "Stock_DETAIL";
            }

            if (DDLType == "Draft")
            {
                sSql = "SELECT '" + CompnyName + "' BU, cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, saldo, qtyin, qtyout, avgvalue, totalvalue, unit, refno, serialnumber, note, updtime, seq, t.cat1code, t.cat2code, t.cat3code, t.cat4code FROM ( ";
                sSql += " SELECT tm.cmpcode, i.itemoid, i.itemcode, i.itemdesc, td.transformdtl1whoid mtrwhoid, g.gndesc whdesc, 0.0 AS saldo, 0.0 AS qtyin, td.transformdtl1qty AS qtyout, 0.0 avgvalue, 0.0 totalvalue, g2.gndesc unit, td.refno, ISNULL(td.serialnumber,'') serialnumber, tm.transformno + ' | ' + ISNULL((SELECT TOP 1 som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=tm.rabmstoid),'') AS note, tm.transformdate updtime, 1 AS seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_trntransformmst tm INNER JOIN QL_trntransformdtl1 td ON td.transformmstoid = tm.transformmstoid INNER JOIN QL_mstitem i ON i.itemoid = td.transformdtl1refoid INNER JOIN QL_m05GN g ON g.gnoid = td.transformdtl1whoid INNER JOIN QL_m05GN g2 ON g2.gnoid = td.transformdtl1unitoid WHERE tm.transformmststatus = 'In Process'";
                sSql += " UNION ALL  SELECT tm.cmpcode, i.itemoid, i.itemcode, i.itemdesc, td.transformdtl2whoid mtrwhoid, g.gndesc whdesc, 0.0 AS saldo, td.transformdtl2qty AS qtyin, 0.0 AS qtyout, 0.0 avgvalue, 0.0 totalvalue, g2.gndesc unit, td.refno, ISNULL(td.serialnumber,'') serialnumber, tm.transformno + ' | ' + ISNULL((SELECT TOP 1 som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=tm.rabmstoid),'') AS note, tm.transformdate updtime, 2 AS seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_trntransformmst tm INNER JOIN QL_trntransformdtl2 td ON td.transformmstoid = tm.transformmstoid INNER JOIN QL_mstitem i ON i.itemoid = td.transformdtl2refoid INNER JOIN QL_m05GN g ON g.gnoid = td.transformdtl2whoid INNER JOIN QL_m05GN g2 ON g2.gnoid = td.transformdtl2unitoid WHERE tm.transformmststatus = 'In Process'";
                sSql += " UNION ALL  SELECT tm.cmpcode, i.itemoid, i.itemcode, i.itemdesc, td.shipmentitemwhoid mtrwhoid, g.gndesc whdesc, 0.0 AS saldo, 0.0 AS qtyin, td.shipmentitemqty AS qtyout, 0.0 avgvalue, 0.0 totalvalue, g2.gndesc unit, td.refno, ISNULL(td.serialnumber,'') serialnumber, tm.shipmentitemno + ' | ' + ISNULL((SELECT TOP 1 som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=tm.rabmstoid),'') AS note, tm.shipmentitemdate updtime, 3 AS seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_trnshipmentitemmst tm INNER JOIN QL_trnshipmentitemdtl td ON td.shipmentitemmstoid = tm.shipmentitemmstoid INNER JOIN QL_mstitem i ON i.itemoid = td.itemoid INNER JOIN QL_m05GN g ON g.gnoid = td.shipmentitemwhoid INNER JOIN QL_m05GN g2 ON g2.gnoid = td.shipmentitemunitoid WHERE tm.shipmentitemmststatus = 'In Process'";
                sSql += " UNION ALL  SELECT td.cmpcode, i.itemoid, i.itemcode, i.itemdesc, td.mtrwhoid, g.gndesc whdesc, 0.0 AS saldo, 0.0 AS qtyin, td.qtyout AS qtyout, 0.0 avgvalue, 0.0 totalvalue, g2.gndesc unit, td.refno, ISNULL(td.serialnumber,'') serialnumber, CONCAT('Booking Stock ', isnull((select CONCAT(rabno,' ', projectname) from ql_trnrabmst x where x.rabmstoid=td.formoid),'')) AS note, td.updtime updtime, 4 AS seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_matbooking td INNER JOIN QL_mstitem i ON i.itemoid = td.refoid INNER JOIN QL_m05GN g ON g.gnoid = td.mtrwhoid INNER JOIN QL_m05GN g2 ON g2.gnoid = i.itemunitoid WHERE td.flag = 'Post'";
                sSql += " )AS t WHERE t.cmpcode='" + CompnyCode + "'";
            }
            else //summary & detail
            {
                sSql = " DECLARE @startdate AS DATETIME; DECLARE @enddate AS DATETIME; SET @startdate='" + StartPeriod + " 00:00:00'; SET @enddate='" + EndPeriod + " 23:23:23'";
                if (DDLType == "Summary")
                {
                    sSql += " SELECT BU, cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, SUM(saldo) saldo, SUM(qtyin) qtyin, SUM(qtyout) qtyout, SUM(totalvalue) totalvalue, unit FROM (";
                }
                else if (DDLType == "Summary PID")
                {
                    sSql += " SELECT BU, cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, SUM(saldo) saldo, SUM(qtyin) qtyin, SUM(qtyout) qtyout, SUM(totalvalue) totalvalue, refno, serialnumber, unit  FROM (";
                }

                sSql += " SELECT '" + CompnyName + "' BU, cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, SUM(saldo) saldo, SUM(qtyin) qtyin, SUM(qtyout) qtyout, avgvalue, totalvalue, totalvalue_backup, unit, refno, serialnumber, note, updtime, seq, t.cat1code, t.cat2code, t.cat3code, t.cat4code FROM ( SELECT con.cmpcode, con.refoid itemoid, i.itemcode, i.itemdesc, con.mtrwhoid, g.gndesc whdesc, 0.0 saldo, con.qtyin, con.qtyout, (CASE WHEN con.valueidr > 0 AND (con.qtyin + con.qtyout) > 0 THEN (con.valueidr / (con.qtyin + con.qtyout)) ELSE 0.0 END) avgvalue, con.valueidr totalvalue, (CASE WHEN con.valueidr_backup > 0 AND (con.qtyin + con.qtyout) > 0 THEN (con.valueidr_backup / (con.qtyin + con.qtyout)) ELSE 0.0 END) avgvalue_backup, con.valueidr_backup totalvalue_backup, g2.gndesc unit, con.refno, ISNULL(con.serialnumber,'') serialnumber, con.note, con.updtime, 3 seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_conmat con INNER JOIN QL_m05GN g ON g.gnoid = con.mtrwhoid INNER JOIN QL_mstitem i ON i.itemoid = con.refoid INNER JOIN QL_m05GN g2 ON g2.gnoid = i.itemunitoid WHERE con.trndate >= @startdate AND con.trndate<=@enddate ";

                sSql += " UNION ALL SELECT cmpcode, itemoid, itemcode, itemdesc, mtrwhoid, whdesc, SUM(saldo) saldo, SUM(qtyin) qtyin, SUM(qtyout) qtyout, (CASE WHEN sa.totalvalue > 0 AND SUM(saldo) > 0 THEN (sa.totalvalue / SUM(saldo)) ELSE 0.0 END) avgvalue, (CASE WHEN sa.totalvalue > 0 AND SUM(saldo) > 0 THEN sa.totalvalue ELSE 0.0 END) totalvalue, (CASE WHEN sa.totalvalue_backup > 0 AND SUM(saldo) > 0 THEN (sa.totalvalue_backup / SUM(saldo)) ELSE 0.0 END) avgvalue_backup, (CASE WHEN sa.totalvalue_backup > 0 AND SUM(saldo) > 0 THEN sa.totalvalue_backup ELSE 0.0 END) totalvalue_backup, unit, refno, serialnumber, note, updtime, seq, sa.cat1code, sa.cat2code, sa.cat3code, sa.cat4code FROM( SELECT con.cmpcode, con.refoid itemoid, i.itemcode, i.itemdesc, con.mtrwhoid, g.gndesc whdesc, (con.qtyin - con.qtyout) saldo, 0.0 qtyin, 0.0 qtyout, 0.0 avgvalue, ISNULL((SELECT TOP 1 conx.valueidr FROM QL_conmat conx WHERE conx.refoid = con.refoid AND conx.trndate < @startdate ORDER BY conx.updtime DESC), 0.0) totalvalue, 0.0 avgvalue_backup, ISNULL((SELECT TOP 1 conx.valueidr_backup FROM QL_conmat conx WHERE conx.refoid = con.refoid AND conx.trndate < @startdate ORDER BY conx.updtime DESC),0.0) totalvalue_backup, g2.gndesc unit, '' refno, '' serialnumber, 'SALDO AWAL' note, '" + StartPeriod + "' updtime, 2 seq, i.cat1code, i.cat2code, i.cat3code, i.cat4code FROM QL_conmat con INNER JOIN QL_m05GN g ON g.gnoid = con.mtrwhoid INNER JOIN QL_mstitem i ON i.itemoid = con.refoid INNER JOIN QL_m05GN g2 ON g2.gnoid = i.itemunitoid WHERE con.trndate < @startdate) AS sa  GROUP BY sa.cmpcode, sa.itemoid, sa.itemcode, sa.itemdesc, sa.mtrwhoid, sa.whdesc, sa.avgvalue, sa.totalvalue, sa.avgvalue_backup, sa.totalvalue_backup, sa.unit, sa.refno, sa.serialnumber, sa.note, sa.updtime, sa.seq, sa.cat1code, sa.cat2code, sa.cat3code, sa.cat4code ";
                sSql += " ) AS t WHERE t.cmpcode='" + CompnyCode + "'";
            }  

            if (DDLWH != null)
            {
                if (DDLWH.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLWH.Count(); i++)
                    {
                        stsval += "'" + DDLWH[i] + "',";
                    }
                    sSql += " AND t.mtrwhoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextMaterial != "")
            {
                string[] arr = TextMaterial.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND t.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (cat1code !=null)
            {
                if (cat1code.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1code.Count(); i++)
                    {
                        stsval1 += "'" + cat1code[i] + "',";
                    }
                    sSql += " AND t.cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }
            
            if(cat2code != null)
            {
                if (cat2code.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2code.Count(); i++)
                    {
                        stsval2 += "'" + cat2code[i] + "',";
                    }
                    sSql += " AND t.cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }
            
            if(cat3code != null)
            {
                if (cat3code.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3code.Count(); i++)
                    {
                        stsval3 += "'" + cat3code[i] + "',";
                    }
                    sSql += " AND t.cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }
            
            if(cat4code != null)
            {
                if (cat4code.Count() > 0)
                {
                    string stsval4 = "";
                    for (int i = 0; i < cat4code.Count(); i++)
                    {
                        stsval4 += "'" + cat4code[i] + "',";
                    }
                    sSql += " AND t.cat4code IN (" + ClassFunction.Left(stsval4, stsval4.Length - 1) + ")";
                }
            }

            if (DDLType != "Summary")
            {
                if (TextPid != "")
                {
                    if (DDLPid == "PID")
                    {
                        sSql += " AND t.refno LIKE '%" + TextPid + "%' ";
                    }
                    else if (DDLPid == "SN")
                    {
                        sSql += " AND t.serialnumber LIKE '%" + TextPid + "%' ";
                    }
                }
            }

            if (DDLType == "Draft")
            {

            }
            else //summary & detail
            {
                sSql += " GROUP BY t.cmpcode, t.itemoid, t.itemcode, t.itemdesc, t.mtrwhoid, t.whdesc, t.avgvalue, t.totalvalue, t.avgvalue_backup, t.totalvalue_backup, t.unit, t.refno, t.serialnumber, t.note, t.updtime, t.seq, t.cat1code, t.cat2code, t.cat3code, t.cat4code";
                if (DDLType == "Summary")
                {
                    sSql += " ) AS t GROUP BY t.BU, t.cmpcode, t.itemoid, t.itemcode, t.itemdesc, t.mtrwhoid, t.whdesc, t.unit, t.cat1code, t.cat2code, t.cat3code, t.cat4code HAVING (SUM(saldo) + SUM(qtyin) - SUM(qtyout))" + DDLopQty + " 0 ";
                }
                else if (DDLType == "Summary PID")
                {
                    sSql += " ) AS t GROUP BY t.BU, t.cmpcode, t.itemoid, t.itemcode, t.itemdesc, t.mtrwhoid, t.whdesc, t.refno, t.serialnumber, t.unit, t.cat1code, t.cat2code, t.cat3code, t.cat4code HAVING (SUM(saldo) + SUM(qtyin) - SUM(qtyout))"+ DDLopQty +" 0 ";
                }
                if (DDLType == "Summary" || DDLType == "Summary PID")
                {
                    sSql += " ORDER BY t.itemdesc";
                }
                else
                {
                    sSql += " ORDER BY t.itemdesc, t.updtime, t.seq";
                }
            }                   

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}