﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class StockOpnameReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string sWhere = "";

        public StockOpnameReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_conmat tbl)
        {
            sSql = "SELECT * FROM QL_m05GN g WHERE gngroup='GUDANG' ORDER BY g.gndesc";
            var DDLWH = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.mtrwhoid);
            ViewBag.DDLWH = DDLWH;
        }


        [HttpPost]
        public ActionResult GetReturData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, adj.resfield1 [No. Draft], adj.stockadjno [No. Retur SJ], CONVERT(varchar(10),adj.stockadjdate, 103) [Tanggal], adj.stockadjmstnote [Note] FROM QL_trnstockadj adj WHERE adj.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND adj.stockadjstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }                
                sSql += " ORDER BY adj.stockadjno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRetur");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string returno, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode Barang], m.itemdesc [Nama Barang], g.gndesc [Satuan] FROM QL_mstitem m INNER JOIN QL_trnstockadj adj ON adj.refoid=m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=adj.stockadjunit WHERE adj.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND adj.stockadjstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }               
                if (returno != "")
                {
                    string[] arr = returno.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_conmat tbl;
            tbl = new QL_conmat();
            if (tbl == null)
                return HttpNotFound();            
            InitDDL(tbl);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string DDLWH, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Detail")            
            {
                if (reporttype == "XLS")
                    rptfile = "rptStockOpnameXls.rpt";
                else
                {
                    rptfile = "rptStockOpnamePdf.rpt";
                }
                rptname = "StockOpname_DETAIL";
            }

            if (StartPeriod != "" && EndPeriod != "")
            {
                sWhere += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sWhere += " AND adj.stockadjstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }            
            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (DDLWH != null)
            {
                if (DDLWH.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLWH.Count(); i++)
                    {
                        stsval += "'" + DDLWH[i] + "',";
                    }
                    sSql += " AND adj.mtrwhoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhere += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }          

            sSql = " SELECT adj.cmpcode, '" + CompnyName + "' [Business Unit], resfield1 [Draft No.], stockadjdate [Adj. Date], stockadjno [Adj. No.], stockadjmstnote [Header Note], stockadjstatus [Status], UPPER(adj.createuser) [Create User], adj.createtime [Create Datetime], UPPER(ISNULL(adj.approvaluser, '')) [Approval User], ISNULL(adj.approvaldatetime, CAST('01/01/1900' AS DATETIME)) [Approval Datetime], adj.mtrwhoid [WH ID], g1.gndesc [Warehouse], adj.refname [Type], itemcode [Code], itemdesc [Material], stockadjqty [Qty], stockadjqtybefore [Qty Before Adj], g2.gndesc [Unit], g3.gndesc [Reason], stockadjtype [Adj. Type], CASE WHEN adj.stockadjqty>0 THEN (adj.stockadjamtidr/adj.stockadjqty) ELSE 0 END [Value IDR], stockadjamtusd [Value USD],stockadjnote [Note], adj.refno [PID], ISNULL(adj.serialnumber,'') [serialnumber] FROM QL_trnstockadj adj INNER JOIN QL_m05gn g1 ON g1.gnoid=adj.mtrwhoid INNER JOIN QL_m05gn g2 ON g2.gnoid=stockadjunit LEFT JOIN QL_m05gn g3 ON g3.gnoid=reasonoid INNER JOIN QL_mstitem m ON m.itemoid = adj.refoid ";
            sSql += " Where adj.cmpcode = '" + CompnyCode + "' " + sWhere + "";
            sSql += " UNION ALL SELECT adj.cmpcode, '" + CompnyName + "' [Business Unit], adj.resfield1 [Draft No.], stockadjdate [Adj. Date], stockadjno [Adj. No.], stockadjmstnote [Header Note], adj.stockadjstatus [Status], UPPER(adj.createuser) [Create User], adj.createtime [Create Datetime], UPPER(ISNULL(adj.approvaluser, '')) [Approval User], ISNULL(adj.approvaldatetime, CAST('01/01/1900' AS DATETIME)) [Approval Datetime], adj.mtrwhoid [WH ID], g1.gndesc [Warehouse], adj.refname [Type], itemcode [Code], itemdesc [Material], b.stockadjqty [Qty], stockadjqtybefore [Qty Before Adj], g2.gndesc [Unit], g3.gndesc [Reason], stockadjtype [Adj. Type], CASE WHEN b.stockadjqty>0 THEN (b.stockadjamtidr/b.stockadjqty) ELSE 0 END [Value IDR], b.stockadjamtusd [Value USD],stockadjnote [Note], b.refno [PID], ISNULL(b.serialnumber,'') [serialnumber] FROM QL_trnstockadj adj INNER JOIN QL_trnstockadj2 b ON b.stockadjoid=adj.stockadjoid INNER JOIN QL_m05gn g1 ON g1.gnoid=adj.mtrwhoid INNER JOIN QL_m05gn g2 ON g2.gnoid=stockadjunit LEFT JOIN QL_m05gn g3 ON g3.gnoid=reasonoid INNER JOIN QL_mstitem m ON m.itemoid = adj.refoid ";
            sSql += " Where adj.cmpcode = '" + CompnyCode + "' " + sWhere + "";

            if (DDLType == "Detail")
            {
                sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}