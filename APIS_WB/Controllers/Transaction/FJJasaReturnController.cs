﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class FJJasaReturnController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public FJJasaReturnController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class arretitemmst
        {
            public string cmpcode { get; set; }
            public int arretitemmstoid { get; set; }
            public string arretitemno { get; set; }
            public DateTime arretitemdate { get; set; }
            public string aritemno { get; set; }
            public string custname { get; set; }
            public string arretitemmststatus { get; set; }
            public string arretitemmstnote { get; set; }
        }

        public class arretitemdtl
        {
            public int arretitemdtlseq { get; set; }
            public int aritemdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int arretitemunitoid { get; set; }
            public string arretitemunit { get; set; }
            public decimal arretitemqty { get; set; }
            public decimal arretitemprice { get; set; }
            public decimal arretitemdtlamt { get; set; }
            public decimal arretitemdtltaxvalue { get; set; }
            public decimal arretitemdtltaxamt { get; set; }
            public decimal arretitemdtlnetto { get; set; }
            public string arretitemdtlnote { get; set; }
            public decimal arretitemvalueidr { get; set; }
            public string arretitemdtlres2 { get; set; }
            public int acctgoid { get; set; }
        }

        public class aritem
        {
            public int aritemmstoid { get; set; }
            public string aritemno { get; set; }
            public DateTime aritemdate { get; set; }
            public string aritemdatestr { get; set; }
            public decimal aritemgrandtotal { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        public class Cust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custphone { get; set; }
        }

        public class trnardtl
        {
            public decimal arqty { get; set; }
            public decimal mrvalue { get; set; }
        }

        public class warehouse
        {
            public int sretitemwhoid { get; set; }
            public string sretitemwh { get; set; }
        }

        public class listmat
        {
            public int aritemdtloid { get; set; }
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int arretitemunitoid { get; set; }
            public string arretitemunit { get; set; }
            public decimal arretitemprice { get; set; }
            public decimal arretitemdtlamt { get; set; }
            public decimal arretitemdtltaxvalue { get; set; }
            public decimal arretitemdtltaxamt { get; set; }
            public decimal arretitemdtlnetto { get; set; }
            public Decimal arretitemqty { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + itemoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        private void InitDDL(QL_trnarretitemmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var sretitemwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.sretitemwhoid = sretitemwhoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        [HttpPost]
        public ActionResult GetDetailData(int aritemmstoid)
        {
            List<listmat> tbl = new List<listmat>();
            sSql = "SELECT aritemdtloid, 0 itemseq, apd.itemoid, jasacode itemcode, m.jasadesc itemdesc, m.acctgoid, ISNULL((SELECT refno FROM QL_trnshipmentitemdtl where shipmentitemdtloid = apd.shipmentitemdtloid AND shipmentitemmstoid = apd.shipmentitemmstoid ),'') AS refno, ISNULL((SELECT serialnumber FROM QL_trnshipmentitemdtl where shipmentitemdtloid = apd.shipmentitemdtloid AND shipmentitemmstoid = apd.shipmentitemmstoid ),'') AS serialnumber, aritemunitoid arretitemunitoid, g.gndesc AS arretitemunit, apd.aritemqty arretitemqty, apd.aritemprice AS arretitemprice, apd.aritemdtlamt AS arretitemdtlamt, apd.aritemdtltaxvalue AS arretitemdtltaxvalue, apd.aritemdtltaxamt AS arretitemdtltaxamt, apd.aritemdtlnetto AS arretitemdtlnetto FROM QL_trnaritemdtl apd INNER JOIN QL_mstjasa m ON m.jasaoid = apd.itemoid INNER JOIN QL_m05gn g ON gnoid = aritemunitoid WHERE apd.cmpcode = '" + CompnyCode + "' AND aritemmstoid = " + aritemmstoid + " AND ISNULL(aritemdtlres1, '')<> 'Complete' ORDER BY aritemdtlseq";

            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFJData(int custoid, string type, int curroid)
        {
            var sAdd = "";
            List<aritem> tbl = new List<aritem>();
            if (type == "Lunas")
            {
                sAdd = "AND (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conar where refoid = aritemmstoid AND reftype='QL_trnaritemmst' AND trnarstatus = 'POST') = 0";
            }
            else
            {
                sAdd = "AND (SELECT COUNT(-1) FROM QL_conar where refoid = aritemmstoid AND reftype='QL_trnaritemmst' AND payrefoid > 0) = 0";
            }
            sSql = "SELECT aritemmstoid, aritemno, aritemdate, (SELECT soitemno FROM QL_trnsoitemmst where soitemmstoid = apm.somstoid) soitemno, apm.aritemgrandtotal, rm.projectname, de.groupdesc departemen FROM QL_trnaritemmst apm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=apm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid where aritemmststatus IN ('Post','Closed') AND apm.curroid=" + curroid + " AND ISNULL(apm.aritemtype,'')='Jasa' AND apm.custoid = " + custoid + " AND ISNULL(aritemmstres1, '')<> 'Closed' " + sAdd + " ORDER BY aritemmstoid";

            tbl = db.Database.SqlQuery<aritem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustData(string type, int curroid)
        {
            var sAdd = "";
            List<Cust> tbl = new List<Cust>();
            if (type == "Lunas")
            {
                sAdd = "AND (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conar where refoid = aritemmstoid AND reftype='QL_trnaritemmst' AND trnarstatus = 'POST') = 0";
            }
            else
            {
                sAdd = "AND (SELECT COUNT(-1) FROM QL_conar where refoid = aritemmstoid AND reftype='QL_trnaritemmst' AND payrefoid > 0) = 0";
            }
            sSql = "SELECT DISTINCT custoid, custname, custcode, custaddr, custphone1 custphone FROM QL_mstcust where activeflag = 'ACTIVE' and custoid IN (SELECT custoid FROM QL_trnaritemmst apm where aritemmststatus IN ('Post','Closed') AND apm.curroid=" + curroid + " AND ISNULL(apm.aritemtype,'')='Jasa' AND ISNULL(aritemmstres1, '')<>'Closed' " + sAdd + ")  ORDER BY custcode, custname";

            tbl = db.Database.SqlQuery<Cust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<arretitemdtl> dtDtl)
        {
            Session["QL_trnarretitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnarretitemdtl"] == null)
            {
                Session["QL_trnarretitemdtl"] = new List<arretitemdtl>();
            }

            List<arretitemdtl> dataDtl = (List<arretitemdtl>)Session["QL_trnarretitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnarretitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid='" + tbl.custoid + "'").FirstOrDefault();

            ViewBag.aritemgrandtotal = db.Database.SqlQuery<decimal>("SELECT aritemgrandtotal FROM QL_trnaritemmst WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid='" + tbl.aritemmstoid + "'").FirstOrDefault();

            ViewBag.aritemno = db.Database.SqlQuery<string>("SELECT aritemno FROM QL_trnaritemmst WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid='" + tbl.aritemmstoid + "'").FirstOrDefault();

            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid='" + tbl.aritemmstoid + "'").FirstOrDefault();

            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid='" + tbl.aritemmstoid + "'").FirstOrDefault();
        }

        // GET: sretitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT apm.cmpcode, apm.arretitemmstoid, apm.arretitemno, apm.arretitemdate, apm.aritemmstoid, ISNULL((SELECT aritemno FROM QL_trnaritemmst where aritemmstoid = apm.aritemmstoid),'') aritemno, s.custname, apm.arretitemmststatus, apm.arretitemmstnote FROM QL_trnarretitemmst apm INNER JOIN QL_mstcust s ON s.custoid=apm.custoid WHERE apm.cmpcode = '" + CompnyCode + "' AND ISNULL(apm.arretitemmstres1,'')='Jasa' ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND apm.arretitemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND apm.arretitemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND apm.arretitemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY apm.arretitemdate";

            List<arretitemmst> dt = db.Database.SqlQuery<arretitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: sretitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarretitemmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnarretitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.arretitemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.arretitemmststatus = "In Process";

                Session["QL_trnarretitemdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnarretitemmst.Find(CompnyCode, id);
                sSql = "select aritemdtloid, arretitemdtlseq, i.jasaoid itemoid, i.jasacode itemcode, jasadesc itemdesc, i.acctgoid, ISNULL((SELECT refno FROM QL_trnshipmentitemdtl mrd INNER JOIN QL_trnaritemdtl ad ON ad.shipmentitemdtloid = mrd.shipmentitemdtloid AND mrd.shipmentitemmstoid = ad.shipmentitemmstoid WHERE ad.aritemdtloid = apd.aritemdtloid),'') AS refno, ISNULL((SELECT serialnumber FROM QL_trnshipmentitemdtl mrd INNER JOIN QL_trnaritemdtl ad ON ad.shipmentitemdtloid = mrd.shipmentitemdtloid AND mrd.shipmentitemmstoid = ad.shipmentitemmstoid WHERE ad.aritemdtloid = apd.aritemdtloid),'') AS serialnumber, arretitemqty, arretitemunitoid arretitemunitoid, gn.gndesc arretitemunit, apd.arretitemprice, apd.arretitemdtlamt, apd.arretitemdtltaxvalue, apd.arretitemdtltaxamt, apd.arretitemdtlnetto from QL_trnarretitemdtl apd INNER JOIN QL_m05GN gn ON gn.gnoid = apd.arretitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_mstjasa i ON i.jasaoid = apd.itemoid WHERE apd.cmpcode='" + CompnyCode + "' AND apd.arretitemmstoid=" + id + " ORDER BY apd.arretitemdtlseq";
                Session["QL_trnarretitemdtl"] = db.Database.SqlQuery<arretitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: sretitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnarretitemmst tbl, string action, string tglmst, string type)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.arretitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("arretitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.arretitemmststatus == "Post")
            {
                tbl.arretitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.arretitemno = "";
            }
            tbl.arretitemmstres1 = "Jasa";
            if (string.IsNullOrEmpty(tbl.arretitemmstres2))
                tbl.arretitemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.arretitemmstnote))
                tbl.arretitemmstnote = "";

            List<arretitemdtl> dtDtl = (List<arretitemdtl>)Session["QL_trnarretitemdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].arretitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY FJ item " + dtDtl[i].itemdesc + "  tidak boleh 0!");
                        }
                        else
                        {
                            if (dtDtl[i].arretitemqty > dtDtl[i].arretitemqty)
                            {
                                ModelState.AddModelError("", "QTY Retur FJ item " + dtDtl[i].itemdesc + " Harus kurang dari FJ QTY!");
                            }
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].arretitemdtlnote))
                            dtDtl[i].arretitemdtlnote = "";
                        sSql = "SELECT (aritemqty - ISNULL((SELECT SUM(arretitemqty) FROM QL_trnarretitemdtl sretd INNER JOIN QL_trnarretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.arretitemmstoid=sretd.arretitemmstoid WHERE sretd.cmpcode=apd.cmpcode AND sretd.aritemdtloid=apd.aritemdtloid AND arretitemmststatus<>'Rejected' AND sretd.arretitemmstoid<>" + tbl.arretitemmstoid + "), 0)) AS aritemqty FROM QL_trnaritemdtl apd WHERE apd.cmpcode='" + CompnyCode + "' AND apd.aritemdtloid=" + dtDtl[i].aritemdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dtDtl[i].arretitemqty > dQty)
                            ModelState.AddModelError("", "QTY FJ item " + dtDtl[i].itemdesc + " sebagian detail data telah diupdate oleh user yg lain. Silahkan cek setiap detail QTY FJ harus lebih dari retur FJ QTY");

                        var dtlRes2 = db.Database.SqlQuery<string>("select DISTINCT ISNULL(mrd.shipmentitemdtlres2,'') from QL_trnaritemdtl apd INNER JOIN QL_trnshipmentitemdtl mrd ON mrd.shipmentitemdtloid = apd.shipmentitemdtloid AND apd.shipmentitemmstoid = apd.shipmentitemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND aritemdtloid=" + dtDtl[i].aritemdtloid).FirstOrDefault();
                        dtDtl[i].arretitemdtlres2 = dtlRes2;

                        if (tbl.arretitemmststatus == "Post")
                        {
                            if (string.IsNullOrEmpty(tbl.arretitemno))
                                ModelState.AddModelError("arretitemno", "Silahkan isi No. FJ!");
                            else if (db.QL_trnarretitemmst.Where(w => w.arretitemno == tbl.arretitemno & w.arretitemmstoid != tbl.arretitemmstoid).Count() > 0)
                                ModelState.AddModelError("arretitemno", "No. FJ yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");

                            ////Stock Value
                            //decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].itemoid);
                            //decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dtDtl[i].arretitemqty, sValue, "OUT");
                            dtDtl[i].arretitemvalueidr = (dtDtl[i].arretitemqty * dtDtl[i].arretitemprice);
                        }
                        else
                        {
                            dtDtl[i].arretitemvalueidr = 0;
                        }
                    }
                }
            }

            if (tbl.arretitemmststatus == "Post")
            {
                if (!ClassFunction.IsInterfaceExists("VAR_PENDAPATAN_JASA", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PENDAPATAN_JASA"));
                if (!ClassFunction.IsInterfaceExists("VAR_AR", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AR"));
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_OUT", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_OUT"));
                if (!ClassFunction.IsInterfaceExists("VAR_JUAL_RETUR", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_JUAL_RETUR"));
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AR_RETUR", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DP_AR_RETUR"));
                if (!ClassFunction.IsInterfaceExists("VAR_CASH_AR_RETUR", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_CASH_AR_RETUR"));
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.arretitemmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            if (ModelState.IsValid)
            {
                tbl.rate2oid = rate2oid;

                var conmatoid = ClassFunction.GenerateID("QL_CONMAT");
                var dpoid = ClassFunction.GenerateID("QL_trndpar");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var ConARoid = ClassFunction.GenerateID("QL_conar");
                var mstoid = ClassFunction.GenerateID("QL_trnarretitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnarretitemdtl");

                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                // VAR retur 'Belum Dibayar'
                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PENDAPATAN_JASA", CompnyCode));
                var iAcctgOidAR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR_JASA", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", CompnyCode));
                var iAcctgOidJualRet = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_JUAL_RETUR", CompnyCode));
                var iAcctgOidARRet = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DP_AR_RETUR", CompnyCode));
                var iAcctgOidCashRet = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_CASH_AR_RETUR", CompnyCode));

                var custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND s.custoid=" + tbl.custoid).FirstOrDefault();

                var aritemtaxamt = db.Database.SqlQuery<decimal>("select aritemtaxamt from QL_trnaritemmst WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid).FirstOrDefault();

                var aritemno = db.Database.SqlQuery<string>("select aritemno from QL_trnaritemmst WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid).FirstOrDefault();

                var aritemtotalamt = db.Database.SqlQuery<decimal>("select aritemtotalamt from QL_trnaritemmst WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid).FirstOrDefault();

                var aritemgrandtotal = db.Database.SqlQuery<decimal>("select aritemgrandtotal from QL_trnaritemmst WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid).FirstOrDefault();

                decimal dMRAmt = 0;
                dMRAmt = dtDtl.Sum(x => x.arretitemvalueidr);

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.arretitemmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.arretitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnarretitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.arretitemmstoid + " WHERE tablename='QL_trnarretitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnaritemdtl SET aritemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND aritemdtloid IN (SELECT aritemdtloid FROM QL_trnarretitemdtl WHERE cmpcode='" + CompnyCode + "' AND arretitemmstoid=" + tbl.arretitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnaritemmst SET aritemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid IN (SELECT aritemmstoid FROM QL_trnarretitemmst WHERE cmpcode='" + CompnyCode + "' AND arretitemmstoid=" + tbl.arretitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnarretitemdtl.Where(a => a.arretitemmstoid == tbl.arretitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnarretitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnarretitemdtl tbldtl;

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnarretitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.arretitemdtloid = dtloid++;
                            tbldtl.arretitemmstoid = tbl.arretitemmstoid;
                            tbldtl.arretitemdtlseq = i + 1;
                            tbldtl.aritemdtloid = dtDtl[i].aritemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.arretitemqty = dtDtl[i].arretitemqty;
                            tbldtl.arretitemprice = dtDtl[i].arretitemprice;
                            tbldtl.arretitemdtlamt = dtDtl[i].arretitemdtlamt;
                            tbldtl.arretitemdtltaxvalue = dtDtl[i].arretitemdtltaxvalue;
                            tbldtl.arretitemdtltaxamt = dtDtl[i].arretitemdtltaxamt;
                            tbldtl.arretitemdtlnetto = dtDtl[i].arretitemdtlnetto;
                            tbldtl.arretitemunitoid = dtDtl[i].arretitemunitoid;
                            tbldtl.arretitemdtlstatus = "";
                            tbldtl.arretitemdtlnote = (dtDtl[i].arretitemdtlnote == null ? "" : dtDtl[i].arretitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.arretitemdtlres1 = "";
                            tbldtl.arretitemdtlres2 = dtDtl[i].arretitemdtlres2;
                            tbldtl.arretitemvalueidr = dtDtl[i].arretitemvalueidr;

                            db.QL_trnarretitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnaritemdtl SET aritemdtlres1='Complete' WHERE cmpcode='" + CompnyCode + "' AND aritemdtloid=" + dtDtl[i].aritemdtloid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnaritemmst SET aritemmstres1='Closed' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid + " AND (SELECT COUNT(*) FROM QL_trnaritemdtl WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid + " AND aritemdtloid<>" + dtDtl[i].aritemdtloid + " AND ISNULL(aritemdtlres1, '')<>'Complete')=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.arretitemmststatus == "Post")
                            {
                                var whoid = db.Database.SqlQuery<int>("select DISTINCT mrd.shipmentitemwhoid from QL_trnaritemdtl apd INNER JOIN QL_trnshipmentitemdtl mrd ON mrd.shipmentitemdtloid = apd.shipmentitemdtloid AND apd.shipmentitemmstoid = apd.shipmentitemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.aritemmstoid).FirstOrDefault();

                                var refno = db.Database.SqlQuery<string>("select mrd.refno from QL_trnaritemdtl apd INNER JOIN QL_trnshipmentitemdtl mrd ON mrd.shipmentitemdtloid = apd.shipmentitemdtloid AND apd.shipmentitemmstoid = apd.shipmentitemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND aritemdtloid=" + dtDtl[i].aritemdtloid).FirstOrDefault();

                                var serialnumber = db.Database.SqlQuery<string>("select ISNULL(mrd.serialnumber,'') from QL_trnaritemdtl apd INNER JOIN QL_trnshipmentitemdtl mrd ON mrd.shipmentitemdtloid = apd.shipmentitemdtloid AND apd.shipmentitemmstoid = apd.shipmentitemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND aritemdtloid=" + dtDtl[i].aritemdtloid).FirstOrDefault();

                                var rabAR = db.Database.SqlQuery<int>("select apm.rabmstoid from QL_trnaritemdtl apd INNER JOIN QL_trnaritemmst apm ON apm.aritemmstoid = apd.aritemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND aritemdtloid=" + dtDtl[i].aritemdtloid).FirstOrDefault();

                                int rab = rabAR;
                                if (dtDtl[i].arretitemdtlres2 == "Booking")
                                {
                                    rab = db.Database.SqlQuery<int>("SELECT ISNULL(SELECT rabmstoid FROM QL_matbooking WHERE formoid=" + rabAR + " AND refoid=" + dtDtl[i].itemoid + " AND mtrwhoid=" + whoid + " AND ISNULL(refno,'')='" + refno + "' AND ISNULL(serialnumber,'')='" + serialnumber + "'),0) AS tbl").FirstOrDefault();
                                }

                                // Insert QL_conmat
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "APRETFG", "QL_trnarretitemdtl", tbl.arretitemmstoid, dtDtl[i].itemoid, "FINISH GOOD", whoid, dtDtl[i].arretitemqty, "Faktur Jual Retur", "Faktur Jual Retur No. " + tbl.arretitemno + "", Session["UserID"].ToString(), refno, dtDtl[i].arretitemvalueidr, 0, 0, null, tbldtl.arretitemdtloid, rab, dtDtl[i].arretitemvalueidr, serialnumber));
                                db.SaveChanges();
                            }

                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.arretitemmststatus == "Post")
                        {
                            if (tbl.arretitemtype != "Belum di Bayar")
                            {
                                iAcctgOidAR = iAcctgOidCashRet; //jika lunas piutang diganti kas/bank
                            }

                            //// Insert QL_trnglmst / HPP(C)
                            //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jual Retur|No. " + tbl.arretitemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            //db.SaveChanges();

                            var glseq = 1;

                            //for (int i = 0; i < dtDtl.Count(); i++)
                            //{
                            //    int acctgoidJasa = dtDtl[i].acctgoid; decimal amtJasa = dtDtl[i].arretitemvalueidr;
                            //    // Insert QL_trngldtl
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, acctgoidJasa, "D", amtJasa, tbl.arretitemno, "Faktur Jual Retur|No. " + tbl.arretitemno + tbl.arretitemmstnote + " | Penerima. " + custname, "Post", Session["UserID"].ToString(), servertime, amtJasa * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                            //    db.SaveChanges();
                            //}  

                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "C", dMRAmt, tbl.arretitemno, "Faktur Jual Retur|No. " + aritemno + tbl.arretitemmstnote + " | Penerima. " + custname, "Post", Session["UserID"].ToString(), servertime, dMRAmt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                            //db.SaveChanges();

                            //glmstoid += 1;
                            // Insert QL_trnglmst / Penjualan Retur
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jual Retur|No. " + tbl.arretitemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidJualRet, "D", aritemtotalamt, tbl.arretitemno, "Faktur Jual Retur|No. " + aritemno + tbl.arretitemmstnote + " | Penerima. " + custname, "Post", Session["UserID"].ToString(), servertime, aritemtotalamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                            db.SaveChanges();

                            decimal totaltax = Convert.ToDecimal(aritemtaxamt);

                            if (aritemtaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.arretitemno, "Faktur Jual| Note. PPN | Penerima." + custname, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAR, "C", aritemgrandtotal, tbl.arretitemno, "Faktur Jual Retur|No. " + tbl.arretitemno + tbl.arretitemmstnote + " | Penerima. " + custname, "Post", Session["UserID"].ToString(), servertime, aritemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                            db.SaveChanges();

                            if (tbl.arretitemtype == "Belum di Bayar")
                            {
                                // Insert QL_conar                        
                                db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, ConARoid++, "QL_trnarretitemmst", tbl.aritemmstoid, tbl.arretitemmstoid, tbl.custoid, iAcctgOidAR, "Post", "APRETFG", new DateTime(1900, 01, 01), sPeriod, 0, servertime, "", 0, servertime, 0, aritemgrandtotal, "RETUR FJ (cust=" + custname + " NO=" + tbl.arretitemno + ")", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, aritemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, ""));
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (ConARoid - 1) + " WHERE tablename='QL_conar'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            // Jika Faktur Jual sudah lunas **
                            else
                            {
                                QL_trncashbankmst tblcb;
                                tblcb = new QL_trncashbankmst();
                                tblcb.cmpcode = CompnyCode;
                                tblcb.cashbankoid = cashbankoid;
                                tblcb.cashbankno = GenerateCashBankNo(CompnyCode, ClassFunction.GetServerTime(), "BKM", iAcctgOidCashRet);
                                tblcb.cashbankdate = DateTime.Parse(servertime.ToString("MM/dd/yyyy"));
                                tblcb.periodacctg = sPeriod;
                                tblcb.cashbanktype = "BKM";
                                tblcb.cashbankgroup = "DPAR";
                                tblcb.acctgoid = iAcctgOidCashRet;
                                tblcb.curroid = 1;
                                tblcb.curroid_to = 0;
                                tblcb.cashbanknote = "";
                                tblcb.cashbankrefno = tbl.arretitemno;
                                tblcb.personoid = Session["UserID"].ToString();
                                tblcb.cashbankduedate = DateTime.Parse(servertime.ToString("MM/dd/yyyy"));
                                tblcb.cashbankamt = aritemgrandtotal;
                                tblcb.cashbankamtidr = aritemgrandtotal * cRate.GetRateDailyIDRValue;
                                tblcb.cashbankamtusd = 0;
                                tblcb.cashbankres1 = "";
                                tblcb.cashbankres2 = "";
                                tblcb.cashbankres3 = "";
                                tblcb.cashbankstatus = "Post";
                                tblcb.createuser = Session["UserID"].ToString();
                                tblcb.createtime = servertime;
                                tblcb.upduser = Session["UserID"].ToString();
                                tblcb.updtime = servertime;
                                tblcb.giroacctgoid = 0;
                                tblcb.refsuppoid = 0;
                                tblcb.cashbanktakegiro = DateTime.Parse(servertime.ToString("MM/dd/yyyy"));
                                tblcb.cashbanktakegiroreal = DateTime.Parse("1900-01-01");
                                tblcb.cashbankgiroreal = DateTime.Parse("1900-01-01");
                                tblcb.cashbanktaxtype = "NON TAX";
                                tblcb.cashbanktaxpct = 0;
                                tblcb.cashbanktaxamt = 0;
                                tblcb.cashbankothertaxamt = 0;
                                tblcb.cashbankdpp = aritemgrandtotal;
                                tblcb.cashbankresamt = 0;
                                tblcb.cashbankresamt2 = 0;
                                tblcb.addacctgoid1 = 0;
                                tblcb.addacctgamt1 = 0;
                                tblcb.addacctgoid2 = 0;
                                tblcb.addacctgamt2 = 0;
                                tblcb.addacctgoid3 = 0;
                                tblcb.addacctgamt3 = 0;
                                tblcb.cashbankapoid = 0;
                                tblcb.cashbankaptype = "";
                                tblcb.deptoid = 0;
                                tblcb.cashbanksuppaccoid = 0;
                                tblcb.groupoid = 0;
                                db.QL_trncashbankmst.Add(tblcb);
                                db.SaveChanges();

                                QL_trndpar dp;
                                dp = new QL_trndpar();
                                dp.cmpcode = CompnyCode;
                                dp.dparoid = dpoid;
                                dp.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                dp.dparno = generateDPNo(servertime);
                                dp.dpardate = servertime;
                                dp.custoid = tbl.custoid;
                                dp.acctgoid = iAcctgOidARRet;
                                dp.cashbankoid = tblcb.cashbankoid;
                                dp.dparpaytype = "BKM";
                                dp.dparpayacctgoid = iAcctgOidCashRet;
                                dp.dparpayrefno = tbl.arretitemno;
                                dp.dparduedate = servertime;
                                dp.curroid = 1;
                                dp.rateoid = 0;
                                dp.rate2oid = 0;
                                dp.dparamt = aritemgrandtotal;
                                dp.dparamtidr = aritemgrandtotal * cRate.GetRateMonthlyIDRValue;
                                dp.dparamtusd = 0;
                                dp.dparaccumamt = 0;
                                dp.dparnote = "RETUR FJ (cust=" + custname + " NO=" + tbl.arretitemno + ")";
                                dp.dparres1 = "Return";
                                dp.dparres2 = "";
                                dp.dparres3 = "";
                                dp.dparstatus = "Post";
                                dp.createuser = Session["UserID"].ToString();
                                dp.createtime = servertime;
                                dp.upduser = Session["UserID"].ToString();
                                dp.updtime = servertime;
                                dp.dpartakegiro = new DateTime(1900, 01, 01);
                                dp.giroacctgoid = 0;
                                dp.addacctgoid1 = 0;
                                dp.addacctgamt1 = 0;
                                dp.addacctgoid2 = 0;
                                dp.addacctgamt2 = 0;
                                dp.addacctgoid3 = 0;
                                dp.addacctgamt3 = 0;
                                dp.somstoid = 0;
                                dp.soreftype = "QL_trnsoitemmst";
                                db.QL_trndpar.Add(dp);
                                db.SaveChanges();

                                glmstoid += 1;
                                // Insert QL_trnglmst / Uang Muka(D)-Kas(C)
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jual Retur|No. " + tbl.arretitemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                glseq = 1;

                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidARRet, "C", aritemgrandtotal, tbl.arretitemno, "Faktur Jual Retur|No. " + tbl.arretitemno + tbl.arretitemmstnote + " | Penerima. " + custname, "Post", Session["UserID"].ToString(), servertime, aritemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                                db.SaveChanges();


                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCashRet, "D", aritemgrandtotal, tbl.arretitemno, "Faktur Jual Retur|No. " + aritemno + tbl.arretitemmstnote + " | Penerima. " + custname, "Post", Session["UserID"].ToString(), servertime, aritemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarretitemmst " + tbl.arretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                                // end insert QL_trnglmst

                                sSql = "UPDATE QL_ID SET lastoid=" + dpoid + " WHERE tablename='QL_trndpar'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnarretitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.arretitemmststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.arretitemmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);

        }


        // POST: sretitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarretitemmst tbl = db.QL_trnarretitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnaritemdtl SET aritemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND aritemdtloid IN (SELECT aritemdtloid FROM QL_trnarretitemdtl WHERE cmpcode='" + CompnyCode + "' AND arretitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnaritemmst SET aritemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid IN (SELECT aritemmstoid FROM QL_trnarretitemmst WHERE cmpcode='" + CompnyCode + "' AND arretitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnarretitemdtl.Where(a => a.arretitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnarretitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnarretitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FJR/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(arretitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnarretitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND arretitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private string generateDPNo(DateTime tanggal)
        {
            string sNo = "dpar/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dparno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND dparno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public string GenerateCashBankNo(string cmpcode, DateTime sDate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' AND acctgoid=" + acctgoid;
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            }
            return cashbankno;
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptFJReturnJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE srm.cmpcode='" + CompnyCode + "' AND srm.arretitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "FJReturnReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}