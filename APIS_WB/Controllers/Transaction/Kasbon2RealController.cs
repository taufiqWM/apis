﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class Kasbon2RealController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public Kasbon2RealController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class kasbon2dtl
        {
            public int kasbon2dtlseq { get; set; }
            public int rabdtl5oid { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal kasbonos { get; set; }
            public decimal kasbon2dtlamt { get; set; }
            public string kasbon2dtlnote { get; set; }
            public decimal kasbon2dtlamtidr { get; set; }
            public decimal kasbon2dtlamtusd { get; set; }
            public int pphcreditacctgdtloid { get; set; }
            public decimal pphcreditdtlamt { get; set; }
        }

        public class listapitemdtl2
        {
            public int apitemdtl2seq { get; set; }
            public int fabelioid { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
            public decimal apitemdtl2amt { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabtype { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public int curroid { get; set; }
            public string rabmstnote { get; set; }
            public string personoid { get; set; }
        }

        public class listkasbon
        {
            public int kasbon2mstoid { get; set; }
            public string kasbon2no { get; set; }
            public DateTime kasbon2date { get; set; }
            public string kasbon2group { get; set; }
            public int acctgoid { get; set; }
            public decimal kasbon2amt { get; set; }
            public string kasbon2note { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
            public int deptoid { get; set; }
            public int curroid { get; set; }
            public string personoid { get; set; }
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public int kasbon2paytypeoid { get; set; }
            public int pphoid { get; set; }
            public decimal pphamt { get; set; }
        }
        public class listduedate
        {
            public DateTime duedate { get; set; }
            public string duedatestr { get; set; }
        }

        public class listacctg
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public int gnother1 { get; set; }
        }

        private void InitDDL(QL_trnkasbon2mst tbl)
        {
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE /*usflag='ACTIVE' AND*/ usoid<>'admin' AND cmpcode='" + CompnyCode + "' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND usoid<>'admin' AND cmpcode='" + CompnyCode + "' ORDER BY usname";
            var personoiddtl = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", ViewBag.personoiddtl);
            ViewBag.personoiddtl = personoiddtl;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoidcb = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoidcb);
            ViewBag.acctgoidcb = acctgoidcb;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoiddtl = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", ViewBag.acctgoiddtl);
            ViewBag.acctgoiddtl = acctgoiddtl;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var kasbon2paytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.kasbon2paytypeoid);
            ViewBag.kasbon2paytypeoid = kasbon2paytypeoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var pphdebetoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.pphdebetoid);
            ViewBag.pphdebetoid = pphdebetoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var pphcreditoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.pphcreditoid);
            ViewBag.pphcreditoid = pphcreditoid;
        }

        [HttpPost]
        public ActionResult InitDDLPerson()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m01US> tbl = new List<QL_m01US>();
            sSql = "SELECT * FROM QL_m01US WHERE /*usflag='ACTIVE' AND*/ cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            tbl = db.Database.SqlQuery<QL_m01US>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindPersonAmt(string kasbon2group, string personoid)
        {
            decimal amt = 0;
            if (kasbon2group != "PEMBERIAN")
            {
                sSql = "SELECT ISNULL((SELECT SUM(kd.kasbon2dtlamt * km.typemin) AS amt FROM QL_trnkasbon2mst km INNER JOIN QL_trnkasbon2dtl kd ON kd.kasbon2mstoid=km.kasbon2mstoid WHERE kd.personoid='" + personoid + "' AND km.kasbon2mststatus IN('Post')),0.0) AS amt";
                amt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            }

            JsonResult js = Json(amt, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDueDate(int paytype, string tgldue, int paytypeoid)
        {
            var result = "sukses";
            var msg = "";
            sSql = "SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid =" + paytypeoid + "";
            paytype = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select dateadd(D, " + paytype + ",'" + DateTime.Parse(ClassFunction.toDate(tgldue)) + "') duedate,CONVERT(char(20),dateadd(D, " + paytype + ",'" + DateTime.Parse(ClassFunction.toDate(tgldue)) + "'),103) duedatestr ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindSupplierData()
        {
            List<listsupp> tbl = new List<listsupp>();

            sSql = "SELECT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid, CAST(g.gnother1 AS int) gnother1 FROM QL_mstsupp s INNER JOIN QL_m05GN g ON g.gnoid=s.supppaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND g.gngroup='PAYMENT TERM' AND s.activeflag = 'ACTIVE' AND s.supptype='Other' ORDER BY suppcode";
            tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT DISTINCT rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabtype, rm.rabmstnote, rm.curroid, rm.deptoid, de.groupdesc departemen, rm.salesoid personoid FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid INNER JOIN QL_trnrabdtl5 rd ON rd.rabmstoid=rm.rabmstoid WHERE rm.rabmststatus='Approved' AND rd.rabdtl5status=''";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetKasbonData()
        {
            List<listkasbon> tbl = new List<listkasbon>();

            sSql = "SELECT km.kasbon2mstoid, km.kasbon2no, km.kasbon2date, km.kasbon2group, km.acctgoid, (km.kasbon2amt + km.pphcreditamt) kasbon2amt, km.kasbon2note, km.rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=km.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=km.rabmstoid),'') departemen, km.deptoid, km.curroid, km.personoid, km.suppoid, km.kasbon2paytypeoid, s.suppname, km.pphcreditoid pphoid, km.pphcreditamt pphamt FROM QL_trnkasbon2mst km INNER JOIN QL_mstsupp s ON s.suppoid=km.suppoid WHERE km.kasbon2mststatus IN('Closed') AND ISNULL(km.kasbon2mstres1,'')='' AND km.kasbon2group='KASBON'";
            tbl = db.Database.SqlQuery<listkasbon>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetFakturData(int suppoid)
        {
            List<listapitemdtl2> tbl = new List<listapitemdtl2>();

            sSql = "SELECT 0 AS apitemdtl2seq, fa.fabelioid, fa.fabelicode, fa.fakturno, (fa.fabeliqty-fa.fabeliaccumqty) fabeliaccumqty, 0.0 AS apitemdtl2amt FROM QL_mstfabeli fa WHERE fa.activeflag='ACTIVE' AND fa.suppoid=" + suppoid + " AND (fabeliqty-fabeliaccumqty)>0 ORDER BY fa.fakturno";
            tbl = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOAHeader(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDDLMarkup()
        {
            var result = "sukses";
            var msg = "";

            List<listacctg> tbl = new List<listacctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid("VAR_PPH", CompnyCode);
            sSql = "SELECT * FROM( SELECT 0 acctgoid, 'NONE' acctgdesc UNION ALL SELECT acctgoid, acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ) AS d ORDER BY d.acctgoid";
            tbl = db.Database.SqlQuery<listacctg>(sSql).ToList();

            JsonResult js = Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar, int rabmstoid, string sType)
        {
            List<kasbon2dtl> tbl = new List<kasbon2dtl>();
            if (sType == "KASBON" && rabmstoid != 0)
            {
                sSql = "SELECT rd.rabdtl5oid, a.acctgoid, a.acctgcode, a.acctgdesc, ('(' + a.acctgcode + ') ' + a.acctgdesc) acctgdesc2, (rd.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd.rabmstoid AND ard.rabdtl5oid=rd.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd.rabmstoid AND ard.rabdtl5oid=rd.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd.rabmstoid AND ard.rabdtl5oid=rd.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd.rabmstoid AND ard.rabdtl5oid=rd.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd.rabmstoid AND gl.acctgoid=rd.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd.rabmstoid AND cb.rabdtl5oid=rd.rabmstoid),0.0)) kasbon2dtlamt, '' kasbon2dtlnote FROM QL_mstacctg a INNER JOIN  QL_trnrabdtl5 rd ON rd.acctgoid=a.acctgoid WHERE rd.cmpcode='" + CompnyCode + "' AND a.activeflag='ACTIVE' AND rd.rabmstoid=" + rabmstoid + " ORDER BY acctgcode";
            }
            else
            {
                string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
                sSql = "SELECT 0 rabdtl5oid, acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 kasbon2dtlamt, '' kasbon2dtlnote, 0 pphcreditacctgdtloid, 0.0 pphcreditdtlamt FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            }

            tbl = db.Database.SqlQuery<kasbon2dtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string kasbon2no, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(kasbon2no, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<kasbon2dtl> dtDtl, List<listapitemdtl2> dtDtl2, Guid? uid)
        {
            Session["QL_trnkasbon2dtlreal"+ uid] = dtDtl;
            Session["QL_trnkasbon2dtlreal2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<kasbon2dtl> dtDtl, Guid? uid)
        {
            Session["QL_trnkasbon2dtlreal"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trnkasbon2dtlreal"+ uid] == null)
            {
                Session["QL_trnkasbon2dtlreal"+ uid] = new List<kasbon2dtl>();
            }

            List<kasbon2dtl> dataDtl = (List<kasbon2dtl>)Session["QL_trnkasbon2dtlreal"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listapitemdtl2> dtDtl, Guid? uid)
        {
            Session["QL_trnkasbon2dtlreal2"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["QL_trnkasbon2dtlreal2"+ uid] == null)
            {
                Session["QL_trnkasbon2dtlreal2"+ uid] = new List<listapitemdtl2>();
            }

            List<listapitemdtl2> dataDtl = (List<listapitemdtl2>)Session["QL_trnkasbon2dtlreal2"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnkasbon2mst tbl)
        {
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT de.groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.kasbonrefno = db.Database.SqlQuery<string>("SELECT kasbon2no FROM QL_trnkasbon2mst r WHERE r.kasbon2mstoid ='" + tbl.kasbon2refoid + "'").FirstOrDefault();
            ViewBag.kasbonrefamt = db.Database.SqlQuery<decimal>("SELECT (kasbon2amt + pphcreditamt) FROM QL_trnkasbon2mst r WHERE r.kasbon2mstoid ='" + tbl.kasbon2refoid + "'").FirstOrDefault();
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp r WHERE r.suppoid ='" + tbl.suppoid + "'").FirstOrDefault();
            ViewBag.gnother1 = db.Database.SqlQuery<int>("SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid ='" + tbl.kasbon2paytypeoid + "'").FirstOrDefault();
        }

        private string GenerateExpenseNo2(string cmpcode, string kasbon2date, string kasbon2type, int acctgoid)
        {
            var kasbon2no = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(kasbon2date));
            if (CompnyCode != "")
            {
                string sNo = "KBN/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(kasbon2no, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnkasbon2mst WHERE cmpcode='" + CompnyCode + "' AND kasbon2no LIKE '%" + sNo + "%'";
                kasbon2no = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return kasbon2no;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmpcode, string kasbon2date, string kasbon2type, int acctgoid)
        {
            var kasbon2no = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(kasbon2date));
            if (CompnyCode != "")
            {
                string sNo = "KBN/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(kasbon2no, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnkasbon2mst WHERE cmpcode='" + CompnyCode + "' AND kasbon2no LIKE '%" + sNo + "%'";
                kasbon2no = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(kasbon2no, JsonRequestBehavior.AllowGet);
        }

        // GET: kasbon2Material
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Kasbon",["Project"] = "Project" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cb.kasbon2mstoid, cb.kasbon2no, cb.kasbon2date, cb.kasbon2group, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ISNULL(cb.rabmstoid,'')),'') projectname, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, cb.kasbon2mststatus, cb.kasbon2note, cb.kasbon2amt, CASE WHEN cb.kasbon2mststatus = 'Revised' THEN revisereason WHEN cb.kasbon2mststatus = 'Rejected' THEN rejectreason ELSE '' END reason FROM QL_trnkasbon2mst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid WHERE cb.cmpcode='" + CompnyCode + "' AND cb.kasbon2group='REALISASI' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.kasbon2date >='" + param.filterperiodfrom + " 00:00:00' AND t.kasbon2date <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.kasbon2mstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.kasbon2no LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.kasbon2mststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.kasbon2mststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.kasbon2mststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.kasbon2mststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: kasbon2Material/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnkasbon2mst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trnkasbon2mst();
                tbl.kasbon2mstoid = ClassFunction.GenerateID("QL_trnkasbon2mst");
                tbl.kasbon2date = ClassFunction.GetServerTime();
                tbl.kasbon2duedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.kasbon2mststatus = "In Process";
                tbl.kasbon2refoid = 0;
                tbl.suppoid = 0;
                tbl.rabmstoid = 0;
                ViewBag.kasbonrefamt = 0;

                Session["QL_trnkasbon2dtlreal"+ ViewBag.uid] = null;
                Session["QL_trnkasbon2dtlreal2"+ ViewBag.uid] = null;
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trnkasbon2mst.Find(CompnyCode, id);

                sSql = "SELECT cbgl.kasbon2dtlseq, cbgl.rabdtl5oid, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.kasbon2dtlamt, cbgl.kasbon2dtlnote, cbgl.pphcreditacctgdtloid, cbgl.pphcreditdtlamt FROM QL_trnkasbon2dtl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.kasbon2mstoid=" + id + " AND cbgl.cmpcode='" + CompnyCode + "' ORDER BY cbgl.kasbon2dtlseq";
                var tbldtl = db.Database.SqlQuery<kasbon2dtl>(sSql).ToList();
                Session["QL_trnkasbon2dtlreal"+ ViewBag.uid] = tbldtl;

                sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trnkasbon2mst' ORDER BY rd.apitemdtl2seq";
                Session["QL_trnkasbon2dtlreal2"+ ViewBag.uid] = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: kasbon2Material/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnkasbon2mst tbl, string action, string tglmst, string tglduedate, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                tbl.kasbon2date = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("kasbon2date", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            //try
            //{
            //    tbl.kasbon2duedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            //}
            //catch (Exception ex)
            //{
            //    ModelState.AddModelError("kasbon2date", "Format Tanggal Due Date Tidak Valid!!" + ex.ToString());
            //}

            DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(DAY, (CASE g.gnother1 WHEN 0 THEN 0 ELSE CAST(g.gnother1 AS INT) END), '" + tbl.kasbon2date + "') AS [Due Date] FROM QL_m05gn g WHERE g.gnoid=" + tbl.kasbon2paytypeoid + " AND g.gngroup='PAYMENT TERM'").FirstOrDefault();
            tbl.kasbon2duedate = duedate;

            if (tbl.kasbon2no == null)
                tbl.kasbon2no = "";

            List<kasbon2dtl> dtDtl = (List<kasbon2dtl>)Session["QL_trnkasbon2dtlreal"+ uid];
            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["QL_trnkasbon2dtlreal2"+ uid];

            string sErrReply = "";
            if (tbl.kasbon2type == "BBK" || tbl.kasbon2type == "BBM")
            {
                if (tbl.kasbon2date > tbl.kasbon2duedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than EXPENSE DATE");
            }
            if (tbl.suppoid == 0)
            {
                ModelState.AddModelError("", "Please Select Supplier!");
            }
            if (tbl.kasbon2amt == 0)
            {
                ModelState.AddModelError("", "GRAND TOTAL must be more than 0!");
            }
            else
            {
                if (!ClassFunction.isLengthAccepted("kasbon2amt", "QL_trnkasbon2mst", tbl.kasbon2amt, ref sErrReply))
                    ModelState.AddModelError("", "GRAND TOTAL must be less than MAX GRAND TOTAL (" + sErrReply + ") allowed stored in database!");
            }
            if (tbl.kasbon2group == "REALISASI")
            {
                if (tbl.kasbon2refoid == 0)
                {
                    ModelState.AddModelError("", "Please Select Kasbon Ref");
                }
                if (tbl.pphdebetamt > 0 || tbl.pphcreditamt > 0)
                {
                    if (tbl.pphdebetamt != tbl.pphcreditamt)
                    {
                        //ModelState.AddModelError("", "Amount PPH Debet Dan PPH Credit Harus Sama!");
                    }
                }
                //if (tbl.pphdebetamt <= 0 || tbl.pphcreditamt <= 0)
                //{
                //    ModelState.AddModelError("", "Amount PPH Debet/Credit Tidak Boleh 0!");
                //}
            }
            if (tbl.kasbon2mststatus.ToUpper() == "REVISED")
            {
                tbl.kasbon2mststatus = "In Process";
            }

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    decimal amtReal = 0;
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].acctgdesc))
                        {
                            ModelState.AddModelError("", "Please Select Detail Account !");
                        }
                        if (dtDtl[i].kasbon2dtlamt == 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be not equal to 0!");
                        }
                        else
                        {
                            if (!ClassFunction.isLengthAccepted("kasbon2dtlamt", "QL_trnkasbon2dtl", dtDtl[i].kasbon2dtlamt, ref sErrReply))
                            {
                                ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!");
                            }
                        }
                        if (tbl.kasbon2group == "KASBON")
                        {
                            if (tbl.rabmstoid > 0)
                            {
                                sSql = "SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON' AND arm.kasbon2mstoid<>" + tbl.kasbon2mstoid + "),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0)) AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.cmpcode='" + CompnyCode + "' AND rabdtl5oid= " + dtDtl[i].rabdtl5oid + "";
                                decimal PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                                dtDtl[i].kasbonos = PriceOs;
                                if (dtDtl[i].kasbon2dtlamt > dtDtl[i].kasbonos)
                                {
                                    ModelState.AddModelError("", "AMOUNT Tidak boleh Lebih Besar dari Budget!");
                                }
                            }
                        }
                        else //REALISASI
                        {
                            amtReal += dtDtl[i].kasbon2dtlamt;

                            if (dtDtl[i].pphcreditdtlamt > 0)
                            {
                                if (dtDtl[i].pphcreditacctgdtloid == 0)
                                {
                                    ModelState.AddModelError("", "Pilih Akun Pph Credit!");
                                }
                            }
                        }
                    }
                    if (tbl.kasbon2group == "REALISASI")
                    {
                        if (tbl.rabmstoid > 0)
                        {
                            sSql = "SELECT ISNULL((SELECT SUM(rd.rabdtl5price) totalamt FROM QL_trnrabdtl5 rd INNER JOIN QL_trnkasbon2dtl kd ON kd.rabdtl5oid=rd.rabdtl5oid INNER JOIN QL_trnkasbon2mst km ON km.kasbon2mstoid=kd.kasbon2mstoid WHERE km.kasbon2mstoid=" + tbl.kasbon2refoid + " AND km.rabmstoid=" + tbl.rabmstoid + " AND km.kasbon2group='KASBON'),0.0) - ISNULL((SELECT SUM(kd.kasbon2dtlamt) FROM QL_trnkasbon2dtl kd INNER JOIN QL_trnkasbon2mst km ON km.kasbon2mstoid=kd.kasbon2mstoid WHERE km.kasbon2group='REALISASI' AND km.rabmstoid=" + tbl.rabmstoid + " AND km.kasbon2refoid IN(SELECT km.kasbon2mstoid FROM QL_trnrabdtl5 rd INNER JOIN QL_trnkasbon2dtl kd ON kd.rabdtl5oid=rd.rabdtl5oid INNER JOIN QL_trnkasbon2mst km ON km.kasbon2mstoid=kd.kasbon2mstoid WHERE km.rabmstoid=" + tbl.rabmstoid + " AND km.kasbon2group='KASBON' AND rd.acctgoid IN(SELECT kd2.acctgoid FROM QL_trnkasbon2mst km2 INNER JOIN QL_trnkasbon2dtl kd2 ON kd2.kasbon2mstoid=km2.kasbon2mstoid WHERE km2.kasbon2mstoid=" + tbl.kasbon2refoid + ")) AND kd.kasbon2mstoid<>" + tbl.kasbon2mstoid + "),0.0) AS dt";
                            decimal PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (amtReal > PriceOs)
                            {
                                ModelState.AddModelError("", "TOTAL AMOUNT Tidak boleh Lebih Besar dari Budget!");
                            }
                        }
                    }
                }
            }

            string apitemdtl2type = "QL_trnkasbon2mst";
            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.suppoid).FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;
            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                        fakturnoGL += dtDtl2[i].fakturno + ",";
                    }
                    fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                }
            }

            //DateTime sDueDate = new DateTime();
            //if (tbl.kasbon2type == "BKK" || tbl.kasbon2type == "BKM")
            //    sDueDate = tbl.kasbon2date;
            //else
            //    sDueDate = tbl.kasbon2duedate;
            //DateTime sDate = tbl.kasbon2date;
            //if (tbl.kasbon2type == "BBK" || tbl.kasbon2type == "BBM")
            //    sDate = tbl.kasbon2duedate;
            DateTime sDate = tbl.kasbon2date;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            if (tbl.kasbon2mststatus == "Post")
            {
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_IN"));
            }

            var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();
            if (tbl.kasbon2mststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.kasbon2mststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.kasbon2date;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.kasbon2mststatus = "In Process";
            }
            if (tbl.kasbon2mststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.kasbon2mststatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.kasbon2mststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnkasbon2mst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trnkasbon2mst WHERE kasbon2mstoid=" + tbl.kasbon2mstoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trnkasbon2mst");
                        tbl.kasbon2no = GenerateExpenseNo2(tbl.cmpcode, tglmst, tbl.kasbon2type, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trnkasbon2dtl");
                var dtl2oid = ClassFunction.GenerateID("QL_trnapitemdtl2");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var conapoid = ClassFunction.GenerateID("QL_CONAP");

                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));

                tbl.kasbon2amtidr = tbl.kasbon2amt * cRate.GetRateMonthlyIDRValue;
                tbl.kasbon2amtusd = 0;
                tbl.kasbon2refno = "";
                tbl.kasbon2note = (tbl.kasbon2note == null ? "" : ClassFunction.Tchar(tbl.kasbon2note));

                decimal TotalAmt = 0; decimal TotalAmtIDR = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.cmpcode = CompnyCode;
                            if (db.QL_trnkasbon2mst.Find(tbl.cmpcode, tbl.kasbon2mstoid) != null)
                                tbl.kasbon2mstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.kasbon2date);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnkasbon2mst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.kasbon2mstoid + " WHERE tablename='QL_trnkasbon2mst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (tbl.kasbon2group == "KASBON")
                            {
                                sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(SELECT rabdtl5oid FROM QL_trnkasbon2dtl WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + tbl.kasbon2mstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else //Realsasi
                            {
                                sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mstres1 ='' WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + tbl.kasbon2refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var trndtl = db.QL_trnkasbon2dtl.Where(a => a.kasbon2mstoid == tbl.kasbon2mstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnkasbon2dtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tbl.kasbon2mstoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        QL_trnkasbon2dtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnkasbon2dtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.kasbon2dtloid = dtloid++;
                            tbldtl.kasbon2mstoid = tbl.kasbon2mstoid;
                            tbldtl.kasbon2dtlseq = i + 1;
                            tbldtl.rabdtl5oid = dtDtl[i].rabdtl5oid;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.kasbon2dtlamt = dtDtl[i].kasbon2dtlamt;
                            tbldtl.kasbon2dtlamtidr = dtDtl[i].kasbon2dtlamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.kasbon2dtlamtusd = 0;
                            tbldtl.kasbon2dtlduedate = tbl.kasbon2duedate;
                            tbldtl.kasbon2dtlrefno = tbl.kasbon2refno;
                            tbldtl.kasbon2dtlstatus = (tbl.kasbon2mststatus == "In Approval" ? "Post" : tbl.kasbon2mststatus);
                            tbldtl.kasbon2dtlnote = (dtDtl[i].kasbon2dtlnote == null ? "" : dtDtl[i].kasbon2dtlnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.pphcreditacctgdtloid = dtDtl[i].pphcreditacctgdtloid;
                            tbldtl.pphcreditdtlamt = dtDtl[i].pphcreditdtlamt;

                            db.QL_trnkasbon2dtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.kasbon2group == "KASBON")
                            {
                                if (dtDtl[i].kasbon2dtlamt >= dtDtl[i].kasbonos)
                                {
                                    sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='Kasbon' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid=" + dtDtl[i].rabdtl5oid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                            else //Realsasi
                            {
                                sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mstres1 ='Closed' WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + tbl.kasbon2refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            TotalAmt += dtDtl[i].kasbon2dtlamt;
                            TotalAmtIDR += dtDtl[i].kasbon2dtlamt * cRate.GetRateMonthlyIDRValue;
                        }
                        sSql = "UPDATE QL_id SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnkasbon2dtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnapitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnapitemdtl2();
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.apitemdtl2oid = dtl2oid++;
                                    tbldtl2.apitemmstoid = tbl.kasbon2mstoid;
                                    tbldtl2.apitemdtl2seq = i + 1;
                                    tbldtl2.fabelioid = dtDtl2[i].fabelioid;
                                    tbldtl2.apitemdtl2amt = dtDtl2[i].apitemdtl2amt;
                                    tbldtl2.createuser = tbl.upduser;
                                    tbldtl2.createtime = tbl.updtime;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.apitemdtl2type = apitemdtl2type;

                                    db.QL_trnapitemdtl2.Add(tbldtl2);
                                    db.SaveChanges();

                                    if (tbl.kasbon2mststatus == "Post")
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }

                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnapitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.kasbon2mststatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.kasbon2mstoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnkasbon2mst";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.kasbon2mststatus == "Post")
                        {
                            //jurnal
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, tbl.kasbon2no, tbl.kasbon2mststatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            if (tbl.kasbon2group == "KASBON")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", TotalAmt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, TotalAmtIDR, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;

                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoidcb, "C", tbl.kasbon2amt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, tbl.kasbon2amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;

                                //Jurnal PPH
                                if (tbl.pphcreditamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.pphcreditoid, "C", tbl.pphcreditamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, tbl.pphcreditamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }

                                // Insert QL_conap                        
                                db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnkasbon2mst", tbl.kasbon2mstoid, 0, tbl.suppoid, tbl.acctgoidcb, "Post", "APKBN", sDate, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tbl.kasbon2amt, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tbl.kasbon2amt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                                db.SaveChanges();
                            }
                            else //Realisasi
                            {
                                for (var i = 0; i < dtDtl.Count(); i++)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", dtDtl[i].kasbon2dtlamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note + " | " + dtDtl[i].kasbon2dtlnote + "", tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, dtDtl[i].kasbon2dtlamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }

                                var total = (tbl.kasbon2amtnetto + tbl.kasbon2amtselisih);
                                var totalIDR = (total * cRate.GetRateMonthlyIDRValue);
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", total, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, totalIDR, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;

                                decimal totaltax = Convert.ToDecimal(totaltaxhdr);
                                if (totaltax > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid, iSeq, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.kasbon2no, tbl.kasbon2no + " | " + suppname + " | " + tbl.kasbon2note, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid, null, null, null, 0, fakturnoGL));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }

                                var selisihamt = tbl.kasbon2amtselisih;
                                if (selisihamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoidcb, "D", selisihamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                                else if (selisihamt < 0)
                                {
                                    selisihamt = selisihamt * (-1);
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoidcb, "C", selisihamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;

                                    // Insert QL_conap                        
                                    db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnkasbon2mst", tbl.kasbon2mstoid, 0, tbl.suppoid, tbl.acctgoidcb, "Post", "APKBN", sDate, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, selisihamt, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                                    db.SaveChanges();
                                }

                                //Jurnal PPH
                                if (tbl.pphcreditamt > 0)
                                {
                                    for (var i = 0; i < dtDtl.Count(); i++)
                                    {
                                        if (dtDtl[i].pphcreditdtlamt > 0)
                                        {
                                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].pphcreditacctgdtloid, "C", dtDtl[i].pphcreditdtlamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note + " | " + dtDtl[i].kasbon2dtlnote + "", tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, dtDtl[i].pphcreditdtlamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                            db.SaveChanges();
                                            iSeq += 1;
                                            gldtloid += 1;
                                        }
                                    }
                                }
                                if (tbl.pphdebetamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.pphdebetoid, "D", tbl.pphdebetamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, tbl.pphdebetamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: kasbon2Material/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnkasbon2mst tbl = db.QL_trnkasbon2mst.Find(CompnyCode, id);
            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["QL_trnkasbon2dtlreal2"+ uid];
            var servertime = ClassFunction.GetServerTime();
            string apitemdtl2type = "QL_trnkasbon2mst";

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl.kasbon2group == "KASBON")
                        {
                            sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(SELECT rabdtl5oid FROM QL_trnkasbon2dtl WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else //Realsasi
                        {
                            sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mstres1 ='' WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + tbl.kasbon2refoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tbl.kasbon2mstoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tbl.cmpcode);
                        db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl = db.QL_trnkasbon2dtl.Where(a => a.kasbon2mstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnkasbon2dtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnkasbon2mst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, Boolean cbprintbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sGroup = db.Database.SqlQuery<string>("SELECT kasbon2group FROM QL_trnkasbon2mst WHERE kasbon2mstoid=" + id + "").FirstOrDefault();

            ReportDocument report = new ReportDocument();
            if (sGroup == "KASBON")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKasbon2Trn.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKasbon2TrnReal.rpt"));
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + CompnyCode + "' AND apm.kasbon2mstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "Kasbon2PrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}