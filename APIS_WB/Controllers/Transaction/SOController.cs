﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class SOController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string filetemppath = "~/Images/SalesOrder";

        public SOController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listsoitem
        {
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string rabno { get; set; }
            public string soitemmststatus { get; set; }
            public string soitemmstnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal soitemgrandtotalamt { get; set; }
        }

        public class listsoitemdtl
        {
            public int soitemdtlseq { get; set; }
            public int rabdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public decimal soitemqty { get; set; }
            public int soitemunitoid { get; set; }
            public string soitemunit { get; set; }
            public decimal soitemprice { get; set; }
            public decimal soitemdtlamt { get; set; }
            public decimal soitemdtltaxvalue { get; set; }
            public decimal soitemdtltaxamt { get; set; }
            public decimal soitemdtlpphvalue { get; set; }
            public decimal soitemdtlpphamt { get; set; }
            public decimal soitemdtlnetto { get; set; }           
            public string soitemdtlnote { get; set; }
            public decimal soitemdtlongkiramt { get; set; }
            public DateTime? soitemdtletd { get; set; }
            public string soitemdtletdstr { get; set; }
            public int soitemdtllocoid { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public string reqrabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public int custoid { get; set; }
            public int custpaymentoid { get; set; }
            public string rabmstnote { get; set; }
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public string salesoid { get; set; }
            public int deptoid { get; set; }            
            public string departement { get; set; }
            public string creatorname { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listsoitemdtl> dtDtl, Guid? uid)
        {
            Session["QL_trnsoitemdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trnsoitemdtl"+ uid] == null)
            {
                Session["QL_trnsoitemdtl"+ uid] = new List<listsoitemdtl>();
            }

            List<listsoitemdtl> dataDtl = (List<listsoitemdtl>)Session["QL_trnsoitemdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SO/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(soitemno,14), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND soitemno LIKE '" + sNo + "%' AND revsoitemtype='Baru'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }
        private void InitDDL(QL_trnsoitemmst tbl)
        {
            sSql = "SELECT deptoid FROM QL_trnrabmst WHERE rabmstoid='"+ tbl.rabmstoid +"'";
            ViewBag.deptoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", ViewBag.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 as int)";
            var soitempaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.soitempaytypeoid);
            ViewBag.soitempaytypeoid = soitempaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            //sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PROVINSI' AND gnflag='ACTIVE'";
            //var rabprovoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabprovoid);
            //ViewBag.rabprovoid = rabprovoid;

            //sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' AND gnflag='ACTIVE'";
            //var rabcityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabcityoid);
            //ViewBag.rabcityoid = rabcityoid;

            sSql = "select * from ql_m05gn where gngroup='SOURCE TRANSAKSI' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var catid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.catid = catid;

            sSql = "select * from ql_m05gn where gngroup='SOURCE NEW CUSTOMER' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var catid2 = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.catid2 = catid2;

            sSql = "select * from ql_m05gn where gngroup='SOURCE TOKO' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var cattoko = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.cattoko = cattoko;
        }

        private void FillAdditionalField(QL_trnsoitemmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.sales = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tblmst.salesoid + "'").FirstOrDefault();
            ViewBag.salesadmin = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tblmst.salesadminoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 a WHERE a.custdtl2oid ='" + tblmst.alamatoid + "'").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst a WHERE a.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT reqrabno FROM QL_trnreqrabmst r WHERE r.reqrabmstoid IN (SELECT reqrabmstoid FROM QL_trnrabmst where rabmstoid ='" + tblmst.rabmstoid + "')").FirstOrDefault();
            var creatoroid = db.QL_trnrabmst.FirstOrDefault(x => x.rabmstoid == tblmst.rabmstoid)?.creatoroid ?? "";
            ViewBag.creatorname = db.QL_m01US.FirstOrDefault(x => x.usoid == creatoroid)?.usname ?? "";
        }

        [HttpPost]
        public ActionResult InitDDLToko(string[] fromtrn)
        {
            var result = "sukses";
            var msg = ""; var mstoid = "";
            if (fromtrn != null)
            {
                if (fromtrn.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < fromtrn.Count(); i++)
                    {
                        stsval += "'" + fromtrn[i] + "',";
                    }
                    mstoid = ClassFunction.Left(stsval, stsval.Length - 1);
                }
            }
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='SOURCE TOKO' AND gnother1 IN(" + mstoid + ") ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT rm.rabmstoid, rm.rabno, rm.rabdate, CONVERT(CHAR(10), rm.rabdate, 103) rabdatestr, c.custname, c.custoid, c.custpaymentoid, isnull(req.projectname, '') projectname, req.reqrabno, rm.rabmstnote, rm.alamatoid, c2.custdtl2addr alamat, rm.salesoid, rm.deptoid, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid = rm.deptoid),'') departement, ISNULL(rm.creatoroid, 0) creatoroid, ISNULL(cr.usname, '') creatorname FROM QL_trnrabmst rm LEFT JOIN QL_trnreqrabmst req ON req.reqrabmstoid = rm.reqrabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_mstcustdtl2 c2 ON c2.custdtl2oid = rm.alamatoid LEFT JOIN QL_m01US cr ON cr.usoid = rm.creatoroid WHERE rm.cmpcode = '" + Session["CompnyCode"].ToString() + "' AND rabmststatus='Approved' AND statusjual = '' ORDER BY rabdate DESC";

            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailData(int rabmstoid)
        {
            List<listsoitemdtl> tbl = new List<listsoitemdtl>();
            sSql = "select rabseq soitemdtlseq, rabdtloid, i.itemoid, i.itemcode, i.itemdesc, rd.rabqty soitemqty, rd.rabunitoid soitemunitoid, gn.gndesc soitemunit, rd.rabprice soitemprice, rd.rabdtlamt soitemdtlamt,rd.rabtaxvalue soitemdtltaxvalue, rd.rabtaxamt soitemdtltaxamt, rd.rabpphvalue soitemdtlpphvalue, rd.rabpphamt soitemdtlpphamt, rd.rabdtlnetto soitemdtlnetto, rd.rabongkiramt soitemdtlongkiramt, rd.rabetd soitemdtletd, CONVERT(CHAR(10),rd.rabetd,103) soitemdtletdstr, rd.rablocoid soitemdtllocoid, rd.rabdtlnote soitemdtlnote from ql_trnrabdtl rd INNER JOIN QL_mstitem i ON i.itemoid = rd.itemoid INNER JOIN QL_m05gn gn ON gn.gnoid = rd.rabunitoid INNER JOIN QL_m05gn gn1 ON gn1.gnoid = rd.rablocoid where rd.rabmstoid = " + rabmstoid +"";

                tbl = db.Database.SqlQuery<listsoitemdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: SOItem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No SO",["Customer"] = "Customer",["Project"] = "Project" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select som.soitemmstoid, som.soitemno, som.soitemdate, c.custname, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=som.rabmstoid),'') projectname, CASE WHEN ISNULL(som.soitemmstres1,'')='' THEN som.soitemmststatus ELSE ISNULL(som.soitemmstres1,'') END soitemmststatus, soitemmstnote, som.soitemgrandtotalamt FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.soitemtype NOT IN ('Jasa') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.soitemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.soitemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.soitemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.soitemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Customer") sSql += " AND t.custname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.soitemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.soitemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.soitemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.soitemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = new QL_trnsoitemmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.soitemno = generateNo(ClassFunction.GetServerTime());
                tblmst.soitemdate = ClassFunction.GetServerTime();
                tblmst.soitemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_trnsoitemdtl"+ ViewBag.uid] = null;
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tblmst = db.QL_trnsoitemmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT rd.soitemdtlseq, rd.rabdtloid, rd.itemoid, i.itemcode, i.itemdesc, rd.soitemqty, rd.soitemunitoid, g.gndesc soitemunit, rd.soitemprice, rd.soitemdtlamt, rd.soitemdtltaxvalue, rd.soitemdtltaxamt, rd.soitemdtlnetto, rd.soitemdtlpphvalue, rd.soitemdtlpphamt, rd.soitemdtlnote, rd.soitemdtlongkiramt, rd.soitemdtletd, CONVERT(CHAR(10),rd.soitemdtletd,103) soitemdtletdstr, rd.soitemdtllocoid FROM QL_trnsoitemdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.soitemunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.soitemmstoid=" + id + " ORDER BY rd.soitemdtlseq";
                Session["QL_trnsoitemdtl"+ ViewBag.uid] = db.Database.SqlQuery<listsoitemdtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsoitemmst tblmst, string action, string tglmst, string tglpodate, string tglpodate2, string[] catid, string[] catid2, string[] cattoko, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tblmst.custoid == null)
                ModelState.AddModelError("custoid", "Pilih Customer Dulu!!");
            try
            {
                tblmst.soitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.soitemcustpodate = DateTime.Parse(ClassFunction.toDate(tglpodate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soitemcustpodate", "Format Tanggal PO Customer Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.soitemcustpodate2 = DateTime.Parse(ClassFunction.toDate(tglpodate2));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soitemcustpodate2", "Format Tanggal Penyelesaian Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tblmst.soitemtype))
                tblmst.soitemtype = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstnote))
                tblmst.soitemmstnote = "";            
            if (string.IsNullOrEmpty(tblmst.soitemrefno))
                tblmst.soitemrefno = "";
            if (string.IsNullOrEmpty(tblmst.soitemcustpo))
                tblmst.soitemcustpo = "";
            if (string.IsNullOrEmpty(tblmst.soitemcustdoc))
                tblmst.soitemcustdoc = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstres1))
                tblmst.soitemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstres2))
                tblmst.soitemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstres3))
                tblmst.soitemmstres3 = "";

            //is Input Detail Valid
            List<listsoitemdtl> dtDtl = (List<listsoitemdtl>)Session["QL_trnsoitemdtl"+ uid];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].soitemdtlnote))
                            dtDtl[i].soitemdtlnote = "";
                        if (dtDtl[i].soitemqty == 0)
                        {
                            ModelState.AddModelError("", "Qty " + dtDtl[i].itemdesc + " Belum Diisi !!");
                        }
                        if (dtDtl[i].soitemprice == 0)
                        {
                            ModelState.AddModelError("", "Harga " + dtDtl[i].itemdesc + " Belum Diisi !!");
                        }
                        if (dtDtl[i].soitemdtletd != null)
                        {
                            try
                            {
                                dtDtl[i].soitemdtletd = DateTime.Parse(ClassFunction.toDate(dtDtl[i].soitemdtletdstr));
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("", "Format Tanggal etd Tidak Valid!!" + ex.ToString());
                            }
                        }
                        sSql = "SELECT (rabqty - ISNULL((SELECT SUM(soitemqty) FROM QL_trnsoitemdtl sd INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.soitemmstoid=sd.soitemmstoid WHERE sd.cmpcode=dod.cmpcode AND soitemmststatus<>'Rejected' AND sd.rabdtloid=dod.rabdtloid AND sd.soitemmstoid<>" + tblmst.soitemmstoid + "), 0.0)) AS soitemqty FROM QL_trnrabdtl dod WHERE dod.cmpcode='" + CompnyCode + "' AND rabdtloid=" + dtDtl[i].rabdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].soitemqty)
                            dtDtl[i].soitemqty = dQty;
                        if (dQty < dtDtl[i].soitemqty)
                            ModelState.AddModelError("", "Qty item " + dtDtl[i].itemdesc + " RAB telah diupdate oleh user yg lain. Silahkan cek setiap Qty Harus kurang dari RAB QTY!");

                    }
                }
            }

            if (tblmst.sonewcusttype == "New Customer")
            {
                if (catid2 == null)
                {
                    ModelState.AddModelError("sonewcusttypeoid", "Silahkan Pilih Source New Customer!");
                }
            }

            if (catid != null)
            {
                if (catid.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < catid.Count(); i++)
                    {
                        stsval += "" + catid[i] + ",";
                    }
                    tblmst.sotranstype = ClassFunction.Left(stsval, stsval.Length - 1);
                }
                else
                {
                    tblmst.sotranstype = "";
                }
            }
            else
            {
                tblmst.sotranstype = "";
            }

            if (catid2 != null)
            {
                if (catid2.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < catid2.Count(); i++)
                    {
                        stsval += "" + catid2[i] + ",";
                    }
                    tblmst.sonewcusttypeoid = ClassFunction.Left(stsval, stsval.Length - 1);
                }
                else
                {
                    tblmst.sonewcusttypeoid = "";
                }
            }
            else
            {
                tblmst.sonewcusttypeoid = "";
            }

            if (cattoko != null)
            {
                if (cattoko.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < cattoko.Count(); i++)
                    {
                        stsval += "" + cattoko[i] + ",";
                    }
                    tblmst.sotranstoko = ClassFunction.Left(stsval, stsval.Length - 1);
                }
                else
                {
                    tblmst.sotranstoko = "";
                }
            }
            else
            {
                tblmst.sotranstoko = "";
            }

            var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();
            if (tblmst.soitemmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid.Value, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.soitemdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.soitemmststatus = "In Process";
            }
            if (tblmst.soitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.soitemmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                tblmst.rate2oid = cRate.GetRateMonthlyOid;
                tblmst.soitemrate2toidr = cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtotalamtidr = tblmst.soitemtotalamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtotaltaxamtidr = tblmst.soitemtotaltaxamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtotalpphamtidr = tblmst.soitemtotalpphamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemgrandtotalamtidr = tblmst.soitemgrandtotalamt * cRate.GetRateMonthlyIDRValue;
                tblmst.rateoid = 0;
                tblmst.soitemtype = "";
                tblmst.soitemtotaldiscamt = 0;
                tblmst.soitemtotaldiscamtidr = 0;

                if (tblmst.soitemmststatus == "Post")
                {
                    if (string.IsNullOrEmpty(tblmst.soitemno))
                    {
                        //tblmst.soitemno = generateNo(ClassFunction.GetServerTime());
                        tblmst.soitemno = generateNo(tblmst.soitemdate.Value);
                    }
                }

                var mstoid = ClassFunction.GenerateID("QL_trnsoitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsoitemdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.soitemmstoid = mstoid;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnsoitemmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnsoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == tblmst.soitemmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnsoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tblmst.soitemmstoid && a.catdtltype == "so_transaksi");
                            db.QL_mstcatdtl.RemoveRange(trncat);
                            db.SaveChanges();

                            var trncat2 = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tblmst.soitemmstoid && a.catdtltype == "so_newcust");
                            db.QL_mstcatdtl.RemoveRange(trncat2);
                            db.SaveChanges();

                            var trncat3 = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tblmst.soitemmstoid && a.catdtltype == "so_toko");
                            db.QL_mstcatdtl.RemoveRange(trncat2);
                            db.SaveChanges();
                        }

                        QL_trnsoitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsoitemdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.soitemdtloid = dtloid++;
                            tbldtl.soitemmstoid = tblmst.soitemmstoid;
                            tbldtl.soitemdtlseq = i + 1;
                            tbldtl.rabdtloid = dtDtl[i].rabdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.soitemqty = dtDtl[i].soitemqty;
                            tbldtl.closeqty = 0;
                            tbldtl.soitemprice = dtDtl[i].soitemprice;
                            tbldtl.soitempriceidr = dtDtl[i].soitemprice;
                            tbldtl.soitemunitoid = dtDtl[i].soitemunitoid;
                            tbldtl.soitemdtlamt = dtDtl[i].soitemdtlamt;
                            tbldtl.soitemdtlamtidr = dtDtl[i].soitemdtlamt;
                            tbldtl.soitemdtltaxvalue = dtDtl[i].soitemdtltaxvalue;
                            tbldtl.soitemdtltaxamt = dtDtl[i].soitemdtltaxamt;
                            tbldtl.soitemdtltaxamtidr = dtDtl[i].soitemdtltaxamt;
                            tbldtl.soitemdtlpphvalue = dtDtl[i].soitemdtlpphvalue;
                            tbldtl.soitemdtlpphamt = dtDtl[i].soitemdtlpphamt;
                            tbldtl.soitemdtlpphamtidr = dtDtl[i].soitemdtlpphamt;
                            tbldtl.soitemdtlnetto = dtDtl[i].soitemdtlnetto;
                            tbldtl.soitemdtlnettoidr = dtDtl[i].soitemdtlnetto;
                            tbldtl.soitemdtlstatus = "";
                            tbldtl.soitemdtlnote = dtDtl[i].soitemdtlnote;
                            tbldtl.soitemdtlongkiramt = dtDtl[i].soitemdtlongkiramt;
                            tbldtl.soitemdtletd = dtDtl[i].soitemdtletd;
                            tbldtl.soitemdtllocoid = dtDtl[i].soitemdtllocoid;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.soitemdtldisctype = "";
                            tbldtl.soitemdtldiscamt = 0;
                            tbldtl.soitemdtldiscamtidr = 0;
                            tbldtl.soitemdtldiscvalue = 0;
                            tbldtl.soitemdtlres1 = "";
                            tbldtl.soitemdtlres2 = "";
                            tbldtl.soitemdtlres3 = "";

                            db.QL_trnsoitemdtl.Add(tbldtl);
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnsoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //insert cat detail
                        if (catid != null)
                        {
                            if (catid.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < catid.Count(); i++)
                                {
                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "so_transaksi";
                                    tblcatdtl.catdtlrefoid = tblmst.soitemmstoid;
                                    tblcatdtl.catoid = int.Parse(catid[i]);
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        //insert cat detail
                        if (catid2 != null)
                        {
                            if (catid2.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < catid2.Count(); i++)
                                {
                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "so_newcust";
                                    tblcatdtl.catdtlrefoid = tblmst.soitemmstoid;
                                    tblcatdtl.catoid = int.Parse(catid2[i]);
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        //insert cat toko detail
                        if (cattoko != null)
                        {
                            if (cattoko.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < cattoko.Count(); i++)
                                {
                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "so_toko";
                                    tblcatdtl.catdtlrefoid = tblmst.soitemmstoid;
                                    tblcatdtl.catoid = int.Parse(cattoko[i]);
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        if (tblmst.soitemmststatus == "Post")
                        {
                            sSql = "UPDATE QL_trnrabmst SET statusjual='SO' WHERE rabmstoid="+ tblmst.rabmstoid +"";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                            // Oh we are here, looks like everything is fine - save all the data permanently
                            objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.soitemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.soitemmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tblmst = db.QL_trnsoitemmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tblmst.soitemmstoid && a.catdtltype == "so_transaksi");
                        db.QL_mstcatdtl.RemoveRange(trncat);
                        db.SaveChanges();

                        var trncat2 = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tblmst.soitemmstoid && a.catdtltype == "so_newcust");
                        db.QL_mstcatdtl.RemoveRange(trncat2);
                        db.SaveChanges();

                        var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == id);
                        db.QL_trnsoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnsoitemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public async System.Threading.Tasks.Task<JsonResult> UploadFile()
        {
            var result = "";
            var filepath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(filetemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        filepath = filetemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, filepath);
            }
            result = "Sukses";

            return Json(new { result, filepath }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOItem.rpt"));           

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.soitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SOReport.pdf");
        }

        public ActionResult PrintReport2(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptProInv.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usoid FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.soitemmstoid IN (" + id + ")");
            report.SetParameterValue("sRekening", "");
            report.SetParameterValue("sBank", "");
            report.SetParameterValue("sAtasNama", "");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ProInvReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}