//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnperdindtl
    {
        public int perdindtlid { get; set; }
        public int perdinid { get; set; }
        public int perdindtlseq { get; set; }
        public string klasifikasi { get; set; }
        public decimal perdindtlamt { get; set; }
        public string perdindtlnote { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public Nullable<int> perdindtlrefid { get; set; }
    }
}
