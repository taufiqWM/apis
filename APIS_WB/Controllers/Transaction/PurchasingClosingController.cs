﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PurchasingClosingController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public PurchasingClosingController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class pomst
        {
            public int poitemmstoid { get; set; }
            public int suppoid { get; set; }
            public string poitemno { get; set; }
            public string poitemdate { get; set; }
            public string transstatus { get; set; }
            public string poitemtype { get; set; }
            public string projectname { get; set; }
            public string transnote { get; set; }
            public string suppname { get; set; }
            public decimal poitemgrandtotalamt { get; set; }
        }

        public class rabmst
        {
            public int rabmstoid { get; set; }
            public int custoid { get; set; }
            public string rabno { get; set; }
            public string rabdate { get; set; }
            public string transstatus { get; set; }
            public string rabtype { get; set; }
            public string projectname { get; set; }
            public string transnote { get; set; }
            public string custname { get; set; }
            public decimal rabgrandtotaljualamt { get; set; }
            public decimal rabgrandtotalbeliamt { get; set; }
            public decimal rabgrandtotaljualjasaamt { get; set; }
            public decimal rabgrandtotalbelijasaamt { get; set; }
            public decimal rabgrandtotalcostamt { get; set; }
        }

        public class poitemdtl
        {
            public int pomstoid { get; set; }
            public int podtlseq { get; set; }

            public int podtloid { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal closeqty { get; set; }
            public string pounit { get; set; }
            public string podtlnote { get; set; }
        }

        public class rabdtl
        {
            public int rabmstoid { get; set; }
            public int transoid { get; set; }
            public string name { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public decimal transgrandtotal { get; set; }
            public string transnote { get; set; }
        }


        [HttpPost]
        public ActionResult GetPOData(string sType)
        {
            List<pomst> tbl = new List<pomst>();
            if (sType == "PO")
            {
                sSql = "SELECT poitemmstoid, poitemno, CONVERT(VARCHAR(10), poitemdate, 103) AS poitemdate, poitemtype, (SELECT projectname FROM QL_trnrabmst where rabmstoid = pom.rabmstoid)projectname, poitemmststatus AS transstatus, poitemmstnote AS transnote, s.suppname, poitemgrandtotalamt FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid AND s.activeflag='ACTIVE' WHERE pom.cmpcode='" + CompnyCode + "' AND poitemtype IN('Jasa') AND ISNULL(pom.rabmstoid,0) = 0 AND poitemmststatus IN ('Approved','Closed') AND ISNULL(pom.poitemmstres1,'') = '' AND (CASE WHEN (SELECT COUNT(*) FROM QL_trnmritemmst where pomstoid = pom.poitemmstoid AND ISNULL(mritemmstres1,'')='') = 0 THEN (SELECT COUNT(*) FROM QL_trnmritemmst where pomstoid = pom.poitemmstoid AND ISNULL(mritemmstres1,'')='') ELSE (SELECT COUNT(*) FROM QL_trnapitemmst where poitemmstoid = pom.poitemmstoid AND ISNULL(apitemmstres1,'')='') END) = 0 ORDER BY poitemdate DESC, poitemmstoid DESC";
            }
            else //POA
            {
                sSql = "SELECT poassetmstoid poitemmstoid, poassetno poitemno, CONVERT(VARCHAR(10), poassetdate, 103) AS poitemdate, '' poitemtype, '' projectname, poassetmststatus AS transstatus, poassetmstnote AS transnote, s.suppname, poassetgrandtotalamt poitemgrandtotalamt FROM QL_trnpoassetmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid AND s.activeflag='ACTIVE' WHERE pom.cmpcode='" + CompnyCode + "' AND poassetmststatus IN ('Approved','Closed') AND ISNULL(pom.poassetmstres1,'') = '' AND (CASE WHEN (SELECT COUNT(*) FROM QL_trnmrassetmst where poassetmstoid = pom.poassetmstoid AND ISNULL(mrassetmstres1,'')='') = 0 THEN (SELECT COUNT(*) FROM QL_trnmrassetmst where poassetmstoid = pom.poassetmstoid AND ISNULL(mrassetmstres1,'')='') ELSE (SELECT COUNT(*) FROM QL_trnapassetmst where poassetmstoid = pom.poassetmstoid AND ISNULL(apassetmstres1,'')='') END) = 0 ORDER BY poassetdate DESC, poassetmstoid DESC";
            }

            tbl = db.Database.SqlQuery<pomst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<rabmst> tbl = new List<rabmst>();
            sSql = "SELECT rabmstoid, rabno, CONVERT(VARCHAR(10), rabdate, 103) AS rabdate, rabtype, projectname, rabmststatus AS transstatus, rabmstnote AS transnote, c.custname, rabgrandtotaljualamt, rabgrandtotalbeliamt, rabgrandtotaljualjasaamt, rabgrandtotalbelijasaamt, rabgrandtotalcostamt FROM QL_trnrabmst s INNER JOIN QL_mstcust C ON s.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE s.cmpcode='" + CompnyCode + "' AND ( ISNULL(statusbeli,'') IN ('','PO') OR ISNULL(statusjual,'') IN ('', 'SO') ) AND rabmststatus IN ('Approved')";

            sSql += " ORDER BY rabdate DESC, rabmstoid DESC";

            tbl = db.Database.SqlQuery<rabmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailData(string sType, int iOid, string poitemtype)
        {

            if (sType.ToUpper() == "PO")
            {
                List<poitemdtl> tbl = new List<poitemdtl>();
                if (poitemtype == "Jasa")
                {
                    sSql = "SELECT poitemdtloid AS podtloid, poitemdtlseq AS podtlseq, jasacode AS matcode, jasadesc AS matlongdesc, /*(poitemqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(mritemqty) FROM QL_trnmritemdtl regd INNER JOIN QL_trnmritemmst regm ON regm.cmpcode = regd.cmpcode AND regm.mritemmstoid = regd.mritemmstoid WHERE regd.cmpcode = pod.cmpcode AND regd.podtloid = pod.poitemdtloid AND regm.pomstoid = pod.poitemmstoid), 0) +ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode = pretd.cmpcode AND mrd.mritemdtloid = pretd.mritemdtloid  WHERE pretd.cmpcode = pod.cmpcode AND mrd.podtloid = pod.poitemdtloid AND pretitemmststatus = 'Post' /*AND ISNULL(pretitemmstres2, '')='Retur Ganti Barang'*/), 0))*/ poitemqty AS closeqty, gndesc AS pounit, poitemdtlnote AS podtlnote FROM QL_trnpoitemdtl pod INNER JOIN QL_mstjasa m ON m.jasaoid = pod.itemoid INNER JOIN QL_m05gn g ON gnoid = jasaunitoid WHERE pod.cmpcode = '" + CompnyCode + "' AND poitemmstoid = " + iOid + " /*AND poitemdtlstatus = ''*/ ORDER BY podtlseq";
                }
                else
                {
                    sSql = "SELECT poitemdtloid AS podtloid, poitemdtlseq AS podtlseq, itemcode AS matcode, itemdesc AS matlongdesc, /*(poitemqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(mritemqty) FROM QL_trnmritemdtl regd INNER JOIN QL_trnmritemmst regm ON regm.cmpcode = regd.cmpcode AND regm.mritemmstoid = regd.mritemmstoid WHERE regd.cmpcode = pod.cmpcode AND regd.podtloid = pod.poitemdtloid AND regm.pomstoid = pod.poitemmstoid), 0) +ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode = pretd.cmpcode AND mrd.mritemdtloid = pretd.mritemdtloid  WHERE pretd.cmpcode = pod.cmpcode AND mrd.podtloid = pod.poitemdtloid AND pretitemmststatus = 'Post' /*AND ISNULL(pretitemmstres2, '')='Retur Ganti Barang'*/), 0))*/ poitemqty AS closeqty, gndesc AS pounit, poitemdtlnote AS podtlnote FROM QL_trnpoitemdtl pod INNER JOIN QL_mstitem m ON m.itemoid = pod.itemoid INNER JOIN QL_m05gn g ON gnoid = poitemunitoid WHERE pod.cmpcode = '" + CompnyCode + "' AND poitemmstoid = " + iOid + " /*AND poitemdtlstatus = ''*/ ORDER BY podtlseq";
                }

                tbl = db.Database.SqlQuery<poitemdtl>(sSql).ToList();
                return Json(tbl, JsonRequestBehavior.AllowGet);
            }
            else if (sType.ToUpper() == "POA")
            {
                List<poitemdtl> tbl = new List<poitemdtl>();
                sSql = "SELECT poassetdtloid AS podtloid, poassetdtlseq AS podtlseq, itemcode AS matcode, itemdesc AS matlongdesc, /*(poassetqty - ISNULL(pod.closeqty, 0) - ISNULL((SELECT SUM(mrassetqty) FROM QL_trnmrassetdtl regd INNER JOIN QL_trnmrassetmst regm ON regm.cmpcode = regd.cmpcode AND regm.mrassetmstoid = regd.mrassetmstoid WHERE regd.cmpcode = pod.cmpcode AND regd.poassetdtloid = pod.poassetdtloid AND regm.poassetmstoid = pod.poassetmstoid), 0) +ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode = pretd.cmpcode AND mrd.mritemdtloid = pretd.mritemdtloid  WHERE pretd.cmpcode = pod.cmpcode AND mrd.podtloid = pod.poassetdtloid AND pretitemmststatus = 'Post' /*AND ISNULL(pretitemmstres2, '')='Retur Ganti Barang'*/), 0))*/ poassetqty AS closeqty, gndesc AS pounit, poassetdtlnote AS podtlnote FROM QL_trnpoassetdtl pod INNER JOIN QL_mstitem m ON m.itemoid = pod.poassetrefoid INNER JOIN QL_m05gn g ON gnoid = poassetunitoid WHERE pod.cmpcode = '" + CompnyCode + "' AND poassetmstoid = " + iOid + " /*AND poassetdtlstatus = ''*/ ORDER BY podtlseq";

                tbl = db.Database.SqlQuery<poitemdtl>(sSql).ToList();
                return Json(tbl, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<rabdtl> tbl = new List<rabdtl>();
                sSql = "SELECT rabmstoid, soitemmstoid AS transoid, soitemno AS transno, CONVERT(varchar(10), soitemdate, 103) transdate, c.custname AS name, soitemgrandtotalamt transgrandtotal, soitemmstnote AS transnote FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid where rabmstoid = " + iOid;
                sSql += " UNION ALL";
                sSql += " SELECT rabmstoid, poitemmstoid, poitemno, CONVERT(varchar(10), poitemdate, 103) soitemdate, s.suppname, poitemgrandtotalamt, poitemmstnote FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid where rabmstoid = " + iOid;
                tbl = db.Database.SqlQuery<rabdtl>(sSql).ToList();
                return Json(tbl, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult SetDataDetails(List<poitemdtl> dtDtl)
        {
            Session["QL_trnpoitemdtl_closing"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsRab(List<rabdtl> dtDtl)
        {
            Session["QL_trnrabdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string marktype, string transnote)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<poitemdtl> dtDtl = null;
            if (Session["QL_trnpoitemdtl_closing"] != null)
            {
                dtDtl = (List<poitemdtl>)Session["QL_trnpoitemdtl_closing"];
            }

            if (marktype == "PO")
            {
                if (dtDtl != null)
                {
                    if (dtDtl[0].pomstoid == 0)
                    {
                        ModelState.AddModelError("", "Silahkan Pilih No. PO Jasa!");
                    }
                    else
                    {
                        if (dtDtl == null)
                            ModelState.AddModelError("", "Please Fill Detail Data!");
                        else if (dtDtl.Count() <= 0)
                            ModelState.AddModelError("", "Please Fill Detail Data!");

                    }
                    if (ModelState.IsValid)
                    {
                        var servertime = ClassFunction.GetServerTime();
                        using (var objTrans = db.Database.BeginTransaction())
                        {
                            try
                            {
                                for (var i = 0; i < dtDtl.Count(); i++)
                                {
                                    sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='Complete', closeqty=ISNULL(closeqty, 0.0)+" + (decimal)dtDtl[i].closeqty + " WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid=" + dtDtl[i].podtloid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }                                    

                                sSql  = "UPDATE QL_trnpoitemmst SET poitemmststatus='Closed', poitemmstres1 = 'Force Closed', closereason='"+ transnote +"', closeuser='" + Session["UserID"].ToString() + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + dtDtl[0].pomstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                objTrans.Commit();
                                Session["QL_trnpoitemdtl_closing"] = null;
                                return RedirectToAction("Index");
                            }
                            catch (Exception ex)
                            {
                                objTrans.Rollback();
                                ModelState.AddModelError("Error", ex.ToString());
                            }
                        }
                    }
                }
            }
            else if (marktype == "POA")
            {
                if (dtDtl != null)
                {
                    if (dtDtl[0].pomstoid == 0)
                    {
                        ModelState.AddModelError("", "Silahkan Pilih No. PO!");
                    }
                    else
                    {
                        if (dtDtl == null)
                            ModelState.AddModelError("", "Please Fill Detail Data!");
                        else if (dtDtl.Count() <= 0)
                            ModelState.AddModelError("", "Please Fill Detail Data!");
                    }
                    if (ModelState.IsValid)
                    {
                        var servertime = ClassFunction.GetServerTime();
                        using (var objTrans = db.Database.BeginTransaction())
                        {
                            try
                            {
                                for (var i = 0; i < dtDtl.Count(); i++)
                                {
                                    sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='Complete', closeqty=ISNULL(closeqty, 0.0)+" + (decimal)dtDtl[i].closeqty + " WHERE cmpcode='" + CompnyCode + "' AND poassetdtloid=" + dtDtl[i].podtloid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }

                                sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Closed', poassetmstres1 = 'Force Closed', closereason='" + transnote + "', closeuser='" + Session["UserID"].ToString() + "', closetime=CURRENT_TIMESTAMP WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + dtDtl[0].pomstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                objTrans.Commit();
                                Session["QL_trnpoitemdtl_closing"] = null;
                                return RedirectToAction("Index");
                            }
                            catch (Exception ex)
                            {
                                objTrans.Rollback();
                                ModelState.AddModelError("Error", ex.ToString());
                            }
                        }
                    }
                }
            }
            return View(dtDtl);            
        }
    }
}