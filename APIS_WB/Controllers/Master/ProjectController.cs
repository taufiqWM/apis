﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class ProjectController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public ProjectController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listproject
        {
            public int projectoid { get; set; }
            public string projectname { get; set; }
            public string projectnote { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "DIV/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(projectcode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstproject WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND projectcode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        // GET/POST: Departemen
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select d.projectoid, d.projectname, d.projectnote FROM QL_mstproject d WHERE d.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by d.projectoid ";
            List<listproject> tblmst = db.Database.SqlQuery<listproject>(sSql).ToList();
            return View(tblmst);
        }


        // GET: Departemen/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstproject tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_mstproject();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_mstproject.Find(Session["CompnyCode"].ToString(), id);
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstproject tblmst, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tblmst.projectname))
                ModelState.AddModelError("usoid", "Please Fill Nama Divisi!!");
            else if (db.QL_mstproject.Where(w => w.projectname == tblmst.projectname & w.projectoid == tblmst.projectoid & w.projectoid != tblmst.projectoid).Count() > 0)
                ModelState.AddModelError("usoid", "Nama Divisi Sudah Dipakai!!");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstproject");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.projectoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_mstproject.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstproject'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_mstproject tblmst)
        {
            //ViewBag.projectoid = new SelectList(db.QL_mstproject.Where(w => w.activeflag == "ACTIVE" | w.projectoid == tblmst.projectoid).ToList(), "projectoid", "projectoid", tblmst.projectoid);
        }

        // POST: Departemen/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstproject tblmst = db.QL_mstproject.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstproject.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}