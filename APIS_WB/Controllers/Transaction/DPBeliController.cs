﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class DPBeliController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public DPBeliController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class trndpap
        {
            public string cmpcode { get; set; }
            public int dpapoid { get; set; }
            public string dpapno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime dpapdate { get; set; }
            public string suppname { get; set; }
            public string pono { get; set; }
            public string acctgdesc { get; set; }
            public string dpappaytype { get; set; }
            public string dpapstatus { get; set; }
            public string dpapnote { get; set; }
            public string cashbankno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dpapamt { get; set; }
        }

        public class suppbankaccount
        {
            public int accountoid { get; set; }
            public string accountname { get; set; }
        }

        public class supplier
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string supptaxable { get; set; }
            public decimal supptaxvalue { get; set; }
        }

        public class listpo
        {
            public int pomstoid { get; set; }
            public string poreftype { get; set; }
            public string pono { get; set; }
            public DateTime podate { get; set; }
            public string projectname { get; set; }
            public string rabno { get; set; }
            public string pomstnote { get; set; }
            public int curroid { get; set; }
            public string departemen { get; set; }
            public Decimal pograndtotalamt { get; set; }
        }

        private void InitDDL(QL_trndpap tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";

            var dpappayacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dpappayacctgoid);
            ViewBag.dpappayacctgoid = dpappayacctgoid;
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;            
        }        

        [HttpPost]
        public ActionResult BindSupplierData()
        {
            List<supplier> tbl = new List<supplier>();
            sSql = "SELECT suppoid, suppcode, suppname, suppaddr, (CASE supppajak WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS supptaxable, ISNULL((SELECT TOP 1 CAST (gndesc AS DECIMAL) FROM QL_m05gn WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND gngroup='DEFAULT TAX' ORDER BY updtime DESC), 0) supptaxvalue FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY suppcode";
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(int suppoid, decimal dpapaccumamt, string dpapstatus, string poreftype, int pomstoid)
        {
            List<listpo> tbl = new List<listpo>();
            if (dpapaccumamt == 0 && dpapstatus == "Post" && poreftype == "QL_trnpoitemmst")
            {
                string projectname = db.Database.SqlQuery<string>("SELECT rm.projectname FROM QL_trnpoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid WHERE som.poitemmstoid=" + pomstoid).FirstOrDefault();

                sSql = "SELECT pm.poitemmstoid pomstoid, 'QL_trnpoitemmst' poreftype, pm.poitemno pono, ISNULL((SELECT r. projectname FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') projectname, ISNULL((SELECT r. rabno FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') rabno, pm.poitemdate podate,  pm.poitemmstnote pomstnote, pm.curroid, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid=pm.rabmstoid),'') departemen, (pm.poitemgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoitemmst' AND dp.pomstoid=pm.poitemmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) pograndtotalamt FROM QL_trnpoitemmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.suppoid= " + suppoid + " AND poitemmststatus IN('Approved','Closed','Post') AND ISNULL(poitemmstres1,'')='' AND (pm.poitemgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoitemmst' AND dp.pomstoid=pm.poitemmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) > 0 AND ISNULL((SELECT r. projectname FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') = '" + projectname + "' ";
            }
            else if (dpapaccumamt == 0 && dpapstatus == "Post" && poreftype == "QL_trnpoassetmst")
            {
                sSql = "SELECT pm.poassetmstoid pomstoid, 'QL_trnpoassetmst' poreftype, pm.poassetno pono, '' projectname, '' rabno, pm.poassetdate podate,  pm.poassetmstnote pomstnote, pm.curroid, '' departemen, (pm.poassetgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoassetmst' AND dp.pomstoid=pm.poassetmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) pograndtotalamt FROM QL_trnpoassetmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.suppoid= " + suppoid + " AND poassetmststatus IN('Approved','Closed','Post') AND ISNULL(poassetmstres1,'')='' AND (pm.poassetgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoassetmst' AND dp.pomstoid=pm.poassetmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) > 0";
            }
            else
            {
                sSql = "SELECT pm.poitemmstoid pomstoid, 'QL_trnpoitemmst' poreftype, pm.poitemno pono, ISNULL((SELECT r. projectname FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') projectname, ISNULL((SELECT r. rabno FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') rabno, pm.poitemdate podate,  pm.poitemmstnote pomstnote, pm.curroid, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid=pm.rabmstoid),'') departemen, (pm.poitemgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoitemmst' AND dp.pomstoid=pm.poitemmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) pograndtotalamt FROM QL_trnpoitemmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.suppoid= " + suppoid + " AND poitemmststatus IN('Approved','Closed','Post') /*AND pm.poitemmstoid NOT IN(SELECT apm.poitemmstoid FROM QL_trnapitemmst apm)*/ AND (pm.poitemgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoitemmst' AND dp.pomstoid=pm.poitemmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) > 0";
                sSql += " UNION ALL  SELECT pm.poassetmstoid pomstoid, 'QL_trnpoassetmst' poreftype, pm.poassetno pono, '' projectname, '' rabno, pm.poassetdate podate,  pm.poassetmstnote pomstnote, pm.curroid, '' departemen, (pm.poassetgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoassetmst' AND dp.pomstoid=pm.poassetmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) pograndtotalamt FROM QL_trnpoassetmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.suppoid= " + suppoid + " AND poassetmststatus IN('Approved','Closed','Post') /*AND pm.poassetmstoid NOT IN(SELECT apm.poassetmstoid FROM QL_trnapassetmst apm)*/ AND (pm.poassetgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoassetmst' AND dp.pomstoid=pm.poassetmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')=''),0.0)) > 0";
            }
            tbl = db.Database.SqlQuery<listpo>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            if (sVar == "NONE")
            {
                sSql = "SELECT 'APIS' cmpcode, 0 acctgoid, '-' acctgcode, 'NONE' acctgdesc, '' acctgdbcr, '' acctggrp1, '' acctggrp2, '' acctggrp3, '' acctgnote, ''acctgres1, ''	 acctgres2, '' acctgres3, '' activeflag, '' createuser, getdate() createtime, '' upduser, getdate() updtime, 0 curroid, '' acctgcodeseq";
            }
            else
            {
                sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, CompnyCode) + ") ORDER BY acctgcode";
            }
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string dpapno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(dpapno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndpap tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.dpappayrefno = db.Database.SqlQuery<string>("SELECT dpappayrefno FROM QL_trndpap WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + tbl.dpapoid + "").FirstOrDefault();
            ViewBag.cashbankno = db.Database.SqlQuery<string>("SELECT cashbankno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();            
            ViewBag.cashbankamt = tbl.dpapamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;
            if (tbl.poreftype == "QL_trnpoitemmst")
            {
                ViewBag.pono = db.Database.SqlQuery<string>("SELECT pm.poitemno FROM QL_trnpoitemmst pm WHERE pm.poitemmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                ViewBag.pograndtotalamt = db.Database.SqlQuery<decimal>("SELECT (pm.poitemgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoitemmst' AND dp.pomstoid=pm.poitemmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')='' AND dp.dpapoid<>'" + tbl.dpapoid +"'),0.0)) FROM QL_trnpoitemmst pm WHERE pm.poitemmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r INNER JOIN QL_trnpoitemmst pm ON pm.rabmstoid=r.rabmstoid WHERE pm.poitemmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_trnpoitemmst pm ON pm.rabmstoid=r.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE pm.poitemmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
            }
            else
            {
                ViewBag.pono = db.Database.SqlQuery<string>("SELECT pm.poassetno FROM QL_trnpoassetmst pm WHERE pm.poassetmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                ViewBag.pograndtotalamt = db.Database.SqlQuery<decimal>("SELECT (pm.poassetgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoasetmst' AND dp.pomstoid=pm.poassetmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')='' AND dp.dpapoid<>'" + tbl.dpapoid + "'),0.0)) FROM QL_trnpoassetmst pm WHERE pm.poassetmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                ViewBag.projectname = ""; ViewBag.departemen = "";
            }
        }

        private string GenerateNo(string dpapdate)
        {
            var dpapno = "";
            if (CompnyCode != "")
            {
                DateTime sDate = DateTime.Parse(ClassFunction.toDate(dpapdate));
                string sNo = "DPAP/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dpapno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpap WHERE cmpcode='" + CompnyCode + "' AND dpapno LIKE '%" + sNo + "%' ";
                dpapno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return dpapno;
        }

        private string GenerateExpenseNo2(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (cmp != "" && cashbanktype != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (cmp != "" && cashbanktype != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        // GET: dpapMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT dp.cmpcode, dp.dpapoid, dpapno, dpapdate, suppname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE dpappaytype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'BANK' ELSE 'GIRO' END) AS dpappaytype, dpapstatus, dpapnote, 'False' AS checkvalue, ISNULL(cashbankno,'') cashbankno, dpapamt, CASE WHEN dp.poreftype='QL_trnpoitemmst' THEN ISNULL((SELECT pm.poitemno FROM QL_trnpoitemmst pm WHERE pm.poitemmstoid=dp.pomstoid),'') ELSE ISNULL((SELECT pm.poassetno FROM QL_trnpoassetmst pm WHERE pm.poassetmstoid=dp.pomstoid),'') END pono FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpappaytype<>'DPFM' ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND dpapdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND dpapdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND dpapstatus IN ('In Process')";
            }

            sSql += " ORDER BY CONVERT(DATETIME, dp.dpapdate) DESC, dp.dpapoid DESC";

            List<trndpap> dt = db.Database.SqlQuery<trndpap>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: dpapMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpap tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trndpap();
                tbl.dpapoid = ClassFunction.GenerateID("QL_trndpap");
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.dpapdate = ClassFunction.GetServerTime();
                tbl.dpapduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.poreftype = "";
                tbl.pomstoid = 0;
                tbl.dpapaccumamt = 0;
                tbl.dpapstatus = "In Process";
                tbl.dpaptakegiro = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trndpap.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dpapMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndpap tbl, string action, string cashbankno, string tglmst, string tglduedate, string tgltakegiro)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.dpapdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dpapdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.dpapduedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dpapdate", "Format Tanggal Due Date Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.dpaptakegiro = DateTime.Parse(ClassFunction.toDate(tgltakegiro));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dpaptakegiro", "Format Tanggal Giro Tidak Valid!!" + ex.ToString());
            }

            if (tbl.dpapno == null)
                tbl.dpapno = "";
            string sErrReply = "";
            if (tbl.suppoid == 0)
                ModelState.AddModelError("", "Please select SUPPLIER field!");
            if (tbl.acctgoid == 0)
                ModelState.AddModelError("", "Please select DP ACCOUNT field");
            if (tbl.dpappaytype == "0")
                ModelState.AddModelError("", "Please select Payment Type field");
            if (tbl.dpappayacctgoid == 0)
                ModelState.AddModelError("", "Please select PAYMENT ACCOUNT field!");
            if (tbl.dpappaytype != "BKK")
            {
                if (tbl.dpappaytype == "BBK")
                {
                    if (tbl.dpappayrefno == "" || tbl.dpappayrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                }
                if (tbl.dpapdate > tbl.dpapduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than DP DATE");
                if (tbl.dpappaytype == "BGK")
                {
                    if (tbl.dpapdate > tbl.dpaptakegiro)
                        ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than DP DATE!");
                }
            }
            if (tbl.dpapamt <= 0)
            {
                ModelState.AddModelError("", "DP AMOUNT must be more than 0!");
            }
            else
            {
                decimal dPOAmt = 0;
                if (tbl.poreftype == "QL_trnpoitemmst")
                {
                     dPOAmt = db.Database.SqlQuery<decimal>("SELECT (pm.poitemgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoitemmst' AND dp.pomstoid=pm.poitemmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')='' AND dp.dpapoid<>'" + tbl.dpapoid + "'),0.0)) FROM QL_trnpoitemmst pm WHERE pm.poitemmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                }
                else
                {
                    dPOAmt = db.Database.SqlQuery<decimal>("SELECT (pm.poassetgrandtotalamt - ISNULL((SELECT SUM(dpapamt) FROM QL_trndpap dp WHERE dp.poreftype='QL_trnpoassetmst' AND dp.pomstoid=pm.poassetmstoid AND dp.dpappaytype<>'DPFM' AND ISNULL(dp.dpapres1,'')='' AND dp.dpapoid<>'" + tbl.dpapoid + "'),0.0)) FROM QL_trnpoassetmst pm WHERE pm.poassetmstoid ='" + tbl.pomstoid + "'").FirstOrDefault();
                }
                if (tbl.dpapamt > dPOAmt)
                {
                    ModelState.AddModelError("", "DP Amount > PO Amount!");
                }

                if (!ClassFunction.isLengthAccepted("dpapamt", "QL_trndpap", tbl.dpapamt, ref sErrReply))
                    ModelState.AddModelError("", "DP AMOUNT must be less than MAX DP AMOUNT (" + sErrReply + ") allowed stored in database!");
            }
            if (tbl.addacctgoid1 != 0)
                if (tbl.addacctgamt1 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid2)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                }
                if (tbl.addacctgoid2 != 0)
                {
                    if (tbl.addacctgoid2 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                }
            }
            if (tbl.addacctgamt1 != 0)
            {
                if (tbl.addacctgoid1 == 0)
                {
                    ModelState.AddModelError("", "Additional Cost 1 please select account!");
                }
            }
            if (tbl.addacctgamt2 != 0)
            {
                if (tbl.addacctgoid2 == 0)
                {
                    ModelState.AddModelError("", "Additional Cost 2 please select account!");
                }
            }
            if (tbl.addacctgamt3 != 0)
            {
                if (tbl.addacctgoid3 == 0)
                {
                    ModelState.AddModelError("", "Additional Cost 3 please select account!");
                }
            }

            //var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.dpappaytype == "BKK" || tbl.dpappaytype == "BLK")
                sDueDate = tbl.dpapdate;
            else
                sDueDate = tbl.dpapduedate;
            DateTime sDate = tbl.dpapdate;
            if (tbl.dpappaytype == "BBK")
                sDate = tbl.dpapduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iGiroAcctgOid = tbl.giroacctgoid;
            if (tbl.dpapstatus == "Post")
            {
                //cRate.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.dpapstatus = "In Process";
                //}
                //if (cRate.GetRateMonthlyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.dpapstatus = "In Process";
                //}
                string sVarErr = "";
                if (tbl.dpappaytype == "BGK")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO", CompnyCode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", CompnyCode));
                        tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", CompnyCode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.dpapstatus = "In Process";
                }
            }

            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid='" + tbl.suppoid + "'").FirstOrDefault();

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.dpapdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.dpapstatus = "In Process";
            }
            if (tbl.dpapstatus == "Post")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.dpapstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.dpapstatus = "In Process";

            if (ModelState.IsValid)
            {
                tbl.cmpcode = CompnyCode;
                tbl.dpapamtidr = tbl.dpapamt * 1;
                tbl.dpapamtusd = tbl.dpapamt * 1;
                tbl.dpaptakegiro = (tbl.dpappaytype == "BGK" ? tbl.dpaptakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.dpapduedate = sDueDate;
                tbl.dpapnote = (tbl.dpapnote == null ? "" : ClassFunction.Tchar(tbl.dpapnote));
                if (tbl.suppoid != 0)
                    ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.suppoid).FirstOrDefault();
                decimal cashbankamt = tbl.dpapamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;

                if (cashbankno == null) cashbankno = "";
                if (tbl.dpapstatus == "Post")
                {
                    tbl.dpapno = GenerateNo(tglmst);
                    cashbankno = GenerateExpenseNo2(tbl.cmpcode, tglmst, tbl.dpappaytype, tbl.dpappayacctgoid);
                }

                var mstoid = ClassFunction.GenerateID("QL_trndpap");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_trncashbankmst tblcashbank;
                        if (action == "Create")
                        {
                            tblcashbank = new QL_trncashbankmst();
                        }
                        else
                        {
                            tblcashbank = new QL_APISEntities().QL_trncashbankmst.Where(s => s.cmpcode == tbl.cmpcode && s.cashbankoid == tbl.cashbankoid).FirstOrDefault();
                        }

                        tblcashbank.cmpcode = tbl.cmpcode;
                        tblcashbank.cashbankoid = tbl.cashbankoid;
                        tblcashbank.cashbankno = cashbankno;
                        tblcashbank.cashbankdate = tbl.dpapdate;
                        tblcashbank.cashbanktype = tbl.dpappaytype;
                        tblcashbank.cashbankgroup = "DPAP";
                        tblcashbank.acctgoid = tbl.dpappayacctgoid;
                        tblcashbank.curroid = tbl.curroid;
                        tblcashbank.cashbankamt = cashbankamt;
                        tblcashbank.cashbankamtidr = cashbankamt * 1;
                        tblcashbank.cashbankamtusd = cashbankamt * 1;
                        tblcashbank.personoid = tbl.suppoid.ToString();
                        tblcashbank.cashbankduedate = tbl.dpapduedate;
                        tblcashbank.cashbankrefno = tbl.dpappayrefno;
                        tblcashbank.cashbanknote = tbl.dpapnote;
                        tblcashbank.cashbankstatus = tbl.dpapstatus;
                        tblcashbank.createuser = tbl.createuser;
                        tblcashbank.createtime = tbl.createtime;
                        tblcashbank.cashbanktakegiro = (tbl.dpappaytype == "BGK" ? tbl.dpaptakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                        tblcashbank.giroacctgoid = iGiroAcctgOid;
                        tblcashbank.addacctgoid1 = tbl.addacctgoid1;
                        tblcashbank.addacctgamt1 = tbl.addacctgamt1;
                        tblcashbank.addacctgoid2 = tbl.addacctgoid2;
                        tblcashbank.addacctgamt2 = tbl.addacctgamt2;
                        tblcashbank.addacctgoid3 = tbl.addacctgoid3;
                        tblcashbank.addacctgamt3 = tbl.addacctgamt3;
                        tblcashbank.cashbanksuppaccoid = 0;

                        tblcashbank.cashbanktakegiroreal = (tblcashbank.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbanktakegiroreal);
                        tblcashbank.cashbankgiroreal = (tblcashbank.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbankgiroreal);
                        tblcashbank.cashbankaptype = (tblcashbank.cashbankaptype == null ? "" : tblcashbank.cashbankaptype);
                        tblcashbank.cashbanktaxtype = "NON TAX";

                        if (action == "Create")
                        {
                            tblcashbank.cashbankoid = cashbankoid;
                            tblcashbank.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dpapdate);
                            tblcashbank.upduser = tbl.createuser;
                            tblcashbank.updtime = tbl.createtime;
                            db.QL_trncashbankmst.Add(tblcashbank);
                            db.SaveChanges();
                           
                            tbl.dpapoid = mstoid;
                            tbl.cashbankoid = cashbankoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dpapdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndpap.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + mstoid + " WHERE tablename='QL_trndpap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tblcashbank.updtime = servertime;
                            tblcashbank.upduser = Session["UserID"].ToString();
                            db.Entry(tblcashbank).State = EntityState.Modified;
                            db.SaveChanges();

                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (tbl.dpapstatus == "Post")
                        {
                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DP A/P|No=" + tbl.dpapno + "", tbl.dpapstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            //Uang Muka
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", tbl.dpapamt, tbl.dpapno, cashbankno +"' | " + suppname +" | "+ tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, tbl.dpapamt * 1, tbl.dpapamt * 1, "QL_trndpap " + tbl.dpapoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                            //Additional Cost 1+/-
                            if (tbl.addacctgamt1 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt1), tbl.dpapno, cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * 1, Math.Abs(tbl.addacctgamt1) * 1, "QL_trndpap " + tbl.dpapoid + "", "", "Additional Cost 1 - DP A/P|No=" + tbl.dpapno + "", (tbl.addacctgamt1 > 0 ? "K" : "M"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Additional Cost 2+/-
                            if (tbl.addacctgamt2 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt2), tbl.dpapno, cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * 1, Math.Abs(tbl.addacctgamt2) * 1, "QL_trndpap " + tbl.dpapoid + "", "", "Additional Cost 2 - DP A/P|No=" + tbl.dpapno + "", (tbl.addacctgamt2 > 0 ? "K" : "M"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Additional Cost 3+/-
                            if (tbl.addacctgamt3 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt3), tbl.dpapno, cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * 1, Math.Abs(tbl.addacctgamt3) * 1, "QL_trndpap " + tbl.dpapoid + "", "", "Additional Cost 3 - DP A/P|No=" + tbl.dpapno + "", (tbl.addacctgamt3 > 0 ? "K" : "M"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Cash/Bank/Giro
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, (tbl.dpappaytype == "BGK" ? iGiroAcctgOid : tbl.dpappayacctgoid), "C", cashbankamt, tbl.dpapno, cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, cashbankamt * 1, cashbankamt * 1, "QL_trndpap " + tbl.dpapoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dpapMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpap tbl = db.QL_trndpap.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trncashbank = db.QL_trncashbankmst.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                        db.QL_trncashbankmst.RemoveRange(trncashbank);
                        db.SaveChanges();

                        db.QL_trndpap.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: dparMaterial/Return/5/11
        [HttpPost, ActionName("ReturDP")]
        [ValidateAntiForgeryToken]
        public ActionResult ReturDPConfirmed(int id, int pomstoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpap tbl = db.QL_trndpap.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            else
            {
                if (tbl.dpappaytype == "BGK")
                {
                    var cekbg = db.QL_trncashbankmst.Where(s=>s.cashbankoid == tbl.cashbankoid).FirstOrDefault().cashbankres1;
                    if (cekbg == "Closed")
                    {
                        result = "failed";
                        msg = "- BG Sudah Di cairkan!";
                    }
                }
            }

            if (result == "success")
            {
                //DateTime sDate = tbl.dpapdate;
                //if (tbl.dpappaytype == "BBK")
                //    sDate = tbl.dpapduedate;
                DateTime sDate = ClassFunction.GetServerTime();
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
                var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid='" + tbl.suppoid + "'").FirstOrDefault();
                var cashbankno = db.Database.SqlQuery<string>("SELECT cashbankno FROM QL_trncashbankmst WHERE cashbankoid='" + tbl.cashbankoid + "'").FirstOrDefault();
                decimal cashbankamt = tbl.dpapamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trndpap SET dpapres1='Batal', dpapaccumamt=dpapamt, updtime='" + servertime + "'  WHERE dpapoid=" + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        if (tbl.dpappaytype == "BGK")
                        {
                            sSql = "UPDATE QL_trncashbankmst SET cashbankrefno = cashbankrefno +' Batal', cashbankres1 = 'Closed', updtime='" + servertime + "'  WHERE cashbankoid=" + tbl.cashbankoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //Insert Into GL Mst
                        db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DP A/P|No=" + tbl.dpapno + "", tbl.dpapstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                        db.SaveChanges();

                        int iSeq = 1;
                        //Insert Into GL Dtl
                        //Uang Muka
                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.dpapamt, tbl.dpapno, "Pembatalan DP | " +  cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, tbl.dpapamt * 1, tbl.dpapamt * 1, "QL_trndpap " + tbl.dpapoid + "", "", "", "", 0));
                        db.SaveChanges();
                        iSeq += 1;
                        gldtloid += 1;
                        //Additional Cost 1+/-
                        if (tbl.addacctgamt1 != 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt1), tbl.dpapno, "Pembatalan DP | " +  cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * 1, Math.Abs(tbl.addacctgamt1) * 1, "QL_trndpap " + tbl.dpapoid + "", "", "Additional Cost 1 - DP A/P|No=" + tbl.dpapno + "", (tbl.addacctgamt1 > 0 ? "M" : "K"), 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        //Additional Cost 2+/-
                        if (tbl.addacctgamt2 != 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt2), tbl.dpapno, "Pembatalan DP | " + cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * 1, Math.Abs(tbl.addacctgamt2) * 1, "QL_trndpap " + tbl.dpapoid + "", "", "Additional Cost 2 - DP A/P|No=" + tbl.dpapno + "", (tbl.addacctgamt2 > 0 ? "M" : "K"), 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        //Additional Cost 3+/-
                        if (tbl.addacctgamt3 != 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt3), tbl.dpapno, "Pembatalan DP | " +  cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * 1, Math.Abs(tbl.addacctgamt3) * 1, "QL_trndpap " + tbl.dpapoid + "", "", "Additional Cost 3 - DP A/P|No=" + tbl.dpapno + "", (tbl.addacctgamt3 > 0 ? "M" : "K"), 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        //Cash/Bank/Giro
                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, (tbl.dpappaytype == "BGK" ? tbl.giroacctgoid : tbl.dpappayacctgoid), "D", cashbankamt, tbl.dpapno, "Pembatalan DP | " + cashbankno + "' | " + suppname + " | " + tbl.dpapnote, tbl.dpapstatus, tbl.upduser, tbl.updtime, cashbankamt * 1, cashbankamt * 1, "QL_trndpap " + tbl.dpapoid + "", "", "", "", 0));
                        db.SaveChanges();
                        iSeq += 1;

                        sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: dparMaterial/Revisi/5/11
        [HttpPost, ActionName("RevisiPO")]
        [ValidateAntiForgeryToken]
        public ActionResult RevisiPOConfirmed(int id, int pomstoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpap tbl = db.QL_trndpap.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trndpap SET pomstoid=" + pomstoid + " WHERE dpapoid=" + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, Boolean cbprintbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trndpap.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDPAP.rpt"));
            sSql = "SELECT dp.dpapoid, CONVERT(VARCHAR(20), dp.dpapoid) AS [Draft No.], dpapno AS [DP No.], suppname AS [Supplier], suppcode AS [Supp. Code], suppaddr AS [Supp. Address], g1.gndesc AS [Supp. City], g2.gndesc AS [Supp. Province], g3.gndesc AS [Supp. Country], suppemail AS [Supp. Email], suppphone1 AS [Supp. Phone 1], suppphone2 AS [Supp. Phone 2], '' AS [Supp. Phone 3], suppfax1 AS [Supp. Fax 1], '' AS [Supp. Fax 2], a.acctgcode AS [DP Account No.], a.acctgdesc AS [DP Account], (CASE dpappaytype WHEN 'BKK' THEN 'Kas' WHEN 'BBK' THEN 'Transfer' WHEN 'BGK' THEN 'Giro/Cheque' ELSE '' END) AS [Payment Type], a2.acctgcode AS [Payment Account No.], a2.acctgdesc AS [Payment Account], dpappayrefno AS [Ref. No.], dpapduedate AS [Due Date], dpaptakegiro AS [Date Take Giro], dpapamt AS [Amount], currcode AS [Currency], currsymbol AS [Curr Symbol], dpapstatus AS [Status], dpapnote AS [Note], CASE WHEN dp.poreftype='QL_trnpoitemmst' THEN (SELECT pm.poitemno FROM QL_trnpoitemmst pm WHERE pm.poitemmstoid=dp.pomstoid) ELSE (SELECT pm.poassetno FROM QL_trnpoassetmst pm WHERE pm.poassetmstoid=dp.pomstoid) END [No. PO], CASE WHEN dp.poreftype='QL_trnpoitemmst' THEN (SELECT r.projectname FROM QL_trnpoitemmst pm INNER JOIN QL_trnrabmst r ON r.rabmstoid=pm.rabmstoid WHERE pm.poitemmstoid=dp.pomstoid) ELSE '' END [Project], divname AS [Business Unit], divaddress AS [BU Address], g4.gndesc AS [BU City], g5.gndesc AS [BU Province], g6.gndesc AS [BU Country], ISNULL(divphone, '') AS [BU Phone 1], ISNULL(divphone2, '') AS [BU Phone 2], divemail AS [BU Email], dp.upduser AS [Last Upd. User ID], usname AS [Last Upd. User Name], dp.updtime AS [Last Upd. Datetime], ISNULL(aa1.acctgdesc,'') adddesc1,dp.addacctgamt1, ISNULL(aa2.acctgdesc,'') adddesc2,dp.addacctgamt2, ISNULL(aa3.acctgdesc,'') adddesc3,dp.addacctgamt3, dpapamt+dp.addacctgamt1+dp.addacctgamt2+dp.addacctgamt3 AS dptotal";
            if (cbprintbbk)
            {
                sSql += ", ISNULL((SELECT cbx.cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayapgiro pg ON pg.cmpcode=cbx.cmpcode AND pg.cashbankoid=cbx.cashbankoid WHERE pg.cmpcode=cb.cmpcode AND pg.reftype='QL_trncashbankmst' AND pg.refoid=cb.cashbankoid), '') AS [Cash/Bank No.], ISNULL((SELECT cbx.cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayapgiro pg ON pg.cmpcode=cbx.cmpcode AND pg.cashbankoid=cbx.cashbankoid WHERE pg.cmpcode=cb.cmpcode AND pg.reftype='QL_trncashbankmst' AND pg.refoid=cb.cashbankoid), CAST('1/1/1900' AS DATETIME)) [DP Date]";
            }
            else
            {
                sSql += ", cashbankno AS [Cash/Bank No.], dpapdate AS [DP Date]";
            }

            sSql += " FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid INNER JOIN QL_m05GN g1 ON g1.gnoid=suppcityOid INNER JOIN QL_m05GN g2 ON g2.gnoid=CONVERT(INT, g1.gnother1) INNER JOIN QL_m05GN g3 ON g3.gnoid=CONVERT(INT, g1.gnother2) INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=dpappayacctgoid INNER JOIN QL_mstcurr c ON c.curroid=dp.curroid INNER JOIN QL_mstdivision di ON di.cmpcode=dp.cmpcode INNER JOIN QL_m05GN g4 ON g4.gnoid=divcityoid INNER JOIN QL_m05GN g5 ON g5.gnoid=CONVERT(INT, g4.gnother1) INNER JOIN QL_m05GN g6 ON g6.gnoid=CONVERT(INT, g4.gnother2) INNER JOIN QL_m01US pr ON pr.usoid=dp.upduser LEFT JOIN QL_mstacctg aa1 ON aa1.acctgoid=dp.addacctgoid1 LEFT JOIN QL_mstacctg aa2 ON aa2.acctgoid=dp.addacctgoid2 LEFT JOIN QL_mstacctg aa3 ON aa3.acctgoid=dp.addacctgoid3 WHERE cb.cmpcode='" + CompnyCode + "' ";
            if (cbprintbbk)
            {
                sSql += " AND dpappaytype='BGK'";
            }
            sSql += " AND dp.dpapoid IN (" + id + ") ORDER BY divname ASC, cashbankno ASC";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintDPAP");

            report.SetDataSource(dtRpt);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "DownPaymentAPPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}