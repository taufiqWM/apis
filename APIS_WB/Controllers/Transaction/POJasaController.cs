﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class POJasaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public POJasaController()
        {
            db.Database.CommandTimeout = 0;
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "POJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(poitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND poitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }
        private void InitDDL(QL_trnpoitemmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var poitempaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.poitempaytypeoid);
            ViewBag.poitempaytypeoid = poitempaytypeoid;

            if (tbl.poitemmstoid > 0)
            {
                sSql = "SELECT poitemdtllocoid FROM QL_trnpoitemdtl WHERE poitemmstoid='" + tbl.poitemmstoid + "'";
                ViewBag.kirimke = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='ALAMAT' AND gnflag='ACTIVE' ORDER BY gndesc";
            var kirimke = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.kirimke);
            ViewBag.kirimke = kirimke;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        private void FillAdditionalField(QL_trnpoitemmst tblmst)
        {
            ViewBag.revpoitemno = db.Database.SqlQuery<string>("SELECT poitemno FROM QL_trnpoitemmst r WHERE r.poitemmstoid ='" + tblmst.revpoitemmstoid + "'").FirstOrDefault();
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT suppdtl1addr FROM QL_mstsuppdtl1 a WHERE a.suppdtl1oid ='" + tblmst.alamatoid + "'").FirstOrDefault();
            ViewBag.picname = db.Database.SqlQuery<string>("SELECT suppdtl3picname FROM QL_mstsuppdtl3 a WHERE a.suppdtl3oid ='" + tblmst.suppsalesoid + "'").FirstOrDefault();
            ViewBag.suppsalesphone = db.Database.SqlQuery<string>("SELECT suppdtl3phone1 FROM QL_mstsuppdtl3 a WHERE a.suppdtl3oid ='" + tblmst.suppsalesoid + "'").FirstOrDefault();
            ViewBag.soitemno = db.Database.SqlQuery<string>("SELECT soitemno from QL_trnsoitemmst where soitemmstoid = '" + tblmst.somstoid + "'").FirstOrDefault();
            ViewBag.soitemdate = db.Database.SqlQuery<string>("SELECT CONVERT(char(20),soitemdate,103) soitemdatestr from QL_trnsoitemmst where soitemmstoid = '" + tblmst.somstoid + "'").FirstOrDefault();
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname from QL_mstsupp where suppoid = '" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT (SELECT reqrabno FROM QL_trnreqrabmst where reqrabmstoid = rb.reqrabmstoid) reqrabno FROM QL_trnrabmst rb WHERE rabmstoid = '" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst rb WHERE rabmstoid = '" + tblmst.rabmstoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetRevPOData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT pom.poitemmstoid, pom.poitemno, '' poitemnonew, pom.poitemdate, rm.rabtype, rm.projectname, s.suppname, pom.poitemmstnote FROM QL_trnpoitemmst pom INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=pom.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE poitemmststatus IN('Approved','Closed') AND revpoitemmstoid=0 ORDER BY poitemdate DESC";
                var tbl = new ClassConnection().GetDataTable(sSql, "tbl");
                if (tbl == null || tbl.Rows.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    foreach (DataRow row in tbl.Rows)
                    {
                        string sNo = ClassFunction.Left(row["poitemno"].ToString(), 15) + "-R";
                        sSql = "SELECT ISNULL(MAX(CAST(REPLACE(poitemno, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoitemmst WHERE cmpcode='" + CompnyCode + "' AND poitemno LIKE '" + sNo + "%'";
                        string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                        sNo = sNo + sCounter;

                        row["poitemnonew"] = sNo;
                    }
                    js = Json(new { result = "", tbl = ClassFunction.toObject(tbl) }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT DISTINCT rm.rabmstoid, CASE WHEN rm.revrabtype='Baru' THEN rm.rabmstoid ELSE rm.revrabmstoid END rabmstoid_awal, rm.rabno, rm.rabdate, rm.rabtype, CONVERT(CHAR(10),rm.rabdate,103) rabdatestr, isnull(rm.projectname,'') projectname, ISNULL(req.reqrabno,'') reqrabno, rm.rabmstnote, rm.deptoid, ISNULL((SELECT d.groupdesc FROM QL_mstdeptgroup d WHERE d.groupoid=rm.deptoid),'') departement, som.soitemmstoid, som.soitemno, som.soitemdate, CONVERT(CHAR(10),som.soitemdate,103) soitemdatestr, rm.curroid FROM QL_trnrabmst rm INNER JOIN QL_trnrabdtl4 rd2 ON rd2.rabmstoid = rm.rabmstoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid AND som.soitemtype NOT IN('Jasa') LEFT JOIN QL_trnreqrabmst req ON req.reqrabmstoid = rm.reqrabmstoid WHERE rm.cmpcode = '" + Session["CompnyCode"].ToString() + "' AND rabmststatus='Approved' ORDER BY rabdate DESC";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData(int rabmstoid)
        {
            JsonResult js = null;
            try
            {
                if (rabmstoid != 0)
                {
                    sSql = $"SELECT DISTINCT s.suppoid, s.suppname, s.suppaddr, s.suppcode, s.supppaymentoid FROM QL_trnrabdtl4 rd2 INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = rd2.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = rd2.suppdtl4oid WHERE rm.cmpcode = '" + CompnyCode + "' AND rm.rabmststatus = 'Approved' AND rd2.rabdtl4status = '' AND rd2.rabmstoid = " + rabmstoid + " AND rd2.rabdtl4flagrevisi='Belum' AND rd2.rabdtl4qty>0 AND rd2.rabdtl4oid NOT IN (SELECT rabdtl2oid FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = pod.poitemmstoid AND pom.poitemtype IN ('Jasa') WHERE pom.poitemmststatus NOT IN ('Rejected'))";
                }
                else
                {
                    sSql = $"SELECT s.suppoid, s.suppname, s.suppaddr, s.suppcode, s.supppaymentoid FROM QL_mstsupp s WHERE s.activeflag = 'ACTIVE'";
                }
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatData(int suppoid)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT suppdtl1oid alamatoid, suppdtl1addr alamat, suppcityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi, c.suppdtl1phone phone FROM QL_mstsuppdtl1 c INNER JOIN QL_m05GN g ON g.gnoid=c.suppcityoid WHERE c.suppoid = " + suppoid + " ORDER BY alamatoid";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetSalesData(int suppoid)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT suppdtl3oid suppsalesoid, suppdtl3picname picname, suppdtl3phone1 phone, suppdtl3jabatan jabatan, suppdtl3picproduk produk FROM QL_mstsuppdtl3 c WHERE c.suppoid = " + suppoid + " ORDER BY suppsalesoid";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class listpoitemdtl : QL_trnpoitemdtl
        {
            public decimal rabdtl2qty { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string poitemunit { get; set; }
            public decimal prqty { get; set; }
            public string prno { get; set; }
        }



        [HttpPost]
        public ActionResult GetDataDetails(int suppoid, string ddlpr, int poitemmstoid)
        {
            var result = "";
            List<listpoitemdtl> tbl = new List<listpoitemdtl>();
            var taxval = db.Database.SqlQuery<decimal>($"SELECT (CASE supppajak WHEN 1 THEN CAST(ISNULL((SELECT TOP 1 gndesc FROM QL_m05GN WHERE gngroup='DEFAULT TAX'),'10') AS DECIMAL(18,2)) ELSE 0.0 END) supptaxamt FROM QL_mstsupp WHERE suppoid={suppoid}").FirstOrDefault();

            if (ddlpr == "pr")
            {
                sSql = $"SELECT 0 poitemdtlseq, 0 rabdtl2oid, 0.0 rabdtl2qty, i.jasaoid itemoid, jasacode itemcode, jasadesc itemdesc, (prdtlamt - ISNULL((SELECT SUM(poitemqty * poitemprice) FROM QL_trnpoitemdtl x WHERE x.prdtloid=prd.prdtloid AND x.poitemmstoid<>{poitemmstoid}),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE x.prdtloid=prd.prdtloid),0.0))/NULLIF(prd.prprice,0) poitemqty, jasaunitoid poitemunitoid, g.gndesc poitemunit, prd.prprice poitemprice, 0.0 poitemdtldiscvalue, 0.0 poitemdtldiscamt, prd.prdtlamt poitemdtlamt, {taxval} poitemdtltaxvalue, 0.0 poitemdtltaxamt, prd.prdtlamt poitemdtlnetto, 0.0 poitemdtlongkiramt, GETDATE() poitemdtleta, 0 poitemdtllocoid, prd.prdtlnote poitemdtlnote, prd.prmstoid, prd.prdtloid, (prdtlamt - ISNULL((SELECT SUM(poitemqty * poitemprice) FROM QL_trnpoitemdtl x WHERE x.prdtloid=prd.prdtloid AND x.poitemmstoid<>{poitemmstoid}),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE x.prdtloid=prd.prdtloid),0.0)) prqty, prm.prno FROM QL_trnprmst prm INNER JOIN QL_trnprdtl prd ON prd.prmstoid=prm.prmstoid INNER JOIN QL_mstjasa i ON i.jasaoid=prd.prrefoid INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE i.activeflag='ACTIVE' AND prm.prmststatus IN('Post','Approved') AND prm.prtype='Jasa' AND ISNULL(prd.prdtlstatus,'')='' ORDER BY jasacode";
            }
            else
            {
                sSql = $"SELECT 0 poitemdtlseq, 0 rabdtl2oid, 0.0 rabdtl2qty, i.jasaoid itemoid, jasacode itemcode, jasadesc itemdesc, 0.0 poitemqty, jasaunitoid poitemunitoid, g.gndesc poitemunit, 0.0 poitemprice, 0.0 poitemdtldiscvalue, 0.0 poitemdtldiscamt, 0.0 poitemdtlamt, {taxval} poitemdtltaxvalue, 0.0 poitemdtltaxamt, 0.0 poitemdtlnetto, 0.0 poitemdtlongkiramt, GETDATE() poitemdtleta, 0 poitemdtllocoid, '' poitemdtlnote, 0 prmstoid, 0 prdtloid, 0.0 prqty, '' prno FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE activeflag='ACTIVE' ORDER BY jasacode";
            }
            tbl = db.Database.SqlQuery<listpoitemdtl>(sSql).ToList();

            if (tbl != null && tbl.Count > 0)
            {
                foreach (var item in tbl)
                {
                    if (ddlpr == "pr")
                    {
                        if (taxval > 0)
                        {
                            item.poitemdtltaxamt = item.poitemdtlamt * (taxval / 100);
                            item.poitemdtlnetto = item.poitemdtlamt + item.poitemdtltaxamt;
                        }
                    }
                }
            }

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailData(int rabmstoid, int suppoid, int poitemmstoid)
        {
            var result = "";
            List<listpoitemdtl> tbl = new List<listpoitemdtl>();
            sSql = "select rd.rabdtl4seq poitemdtlseq, rd.rabdtl4oid rabdtl2oid, rd.rabdtl4qty rabdtl2qty, i.jasaoid itemoid, i.jasacode itemcode, i.jasadesc itemdesc, rd.rabdtl4qty - ISNULL((SELECT SUM(poitemqty) FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=pom.cmpcode AND pom.poitemmstoid=pod.poitemmstoid WHERE poitemmststatus<>'Rejected' AND pod.rabdtl2oid=rd.rabdtl4oid AND pom.poitemtype='Jasa' AND pod.poitemmstoid<>" + poitemmstoid + "), 0.0) poitemqty, rd.rabdtl4unitoid poitemunitoid, gn.gndesc poitemunit, rd.rabdtl4price poitemprice, ISNULL(rd.rabdtl4discvalue,0.0) poitemdtldiscvalue, rd.rabdtl4discamt poitemdtldiscamt, rd.rabdtl4amt poitemdtlamt,rd.rabdtl4taxvalue poitemdtltaxvalue, rd.rabdtl4taxamt poitemdtltaxamt, rd.rabdtl4netto poitemdtlnetto, 0.0 poitemdtlongkiramt, rd.rabdtl4eta poitemdtleta, 0 poitemdtllocoid, rd.rabdtl4note poitemdtlnote, 0 prmstoid, 0 prdtloid, 0.0 prqty, '' prno FROM ql_trnrabdtl4 rd INNER JOIN QL_mstjasa i ON i.jasaoid = rd.itemdtl4oid INNER JOIN QL_m05gn gn ON gn.gnoid = rd.rabdtl4unitoid WHERE rd.rabmstoid = " + rabmstoid + " AND rd.suppdtl4oid = " + suppoid + " AND rd.rabdtl4flagrevisi='Belum' AND rd.rabdtl4status='' AND (rd.rabdtl4qty - ISNULL((SELECT SUM(poitemqty) FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=pom.cmpcode AND pom.poitemmstoid=pod.poitemmstoid WHERE poitemmststatus<>'Rejected' AND pod.rabdtl2oid=rd.rabdtl4oid AND pom.poitemtype='Jasa' AND pod.poitemmstoid<>" + poitemmstoid + "), 0.0))>0";
            tbl = db.Database.SqlQuery<listpoitemdtl>(sSql).ToList();

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<listpoitemdtl>();

            try
            {
                sSql = $"SELECT rd.poitemdtlseq, rd.rabdtl2oid, ISNULL((SELECT rdx.rabdtl4qty FROM QL_trnrabdtl4 rdx WHERE rdx.rabdtl4oid=rd.rabdtl2oid),0.0) rabdtl2qty, rd.itemoid, i.jasacode itemcode, i.jasadesc itemdesc, rd.poitemqty, rd.poitemunitoid, g.gndesc poitemunit, rd.poitemprice, rd.poitemdtldiscvalue, rd.poitemdtldiscamt, rd.poitemdtlamt, rd.poitemdtltaxvalue, rd.poitemdtltaxamt, rd.poitemdtlnetto, rd.poitemdtlnote, rd.poitemdtlongkiramt, rd.poitemdtleta, rd.poitemdtllocoid, ISNULL(rd.prmstoid,0) prmstoid, ISNULL(rd.prdtloid,0) prdtloid, (ISNULL(prdtlamt,0.0) - ISNULL((SELECT SUM(poitemdtlamt) FROM QL_trnpoitemdtl x WHERE ISNULL(x.prdtloid,0)=ISNULL(rd.prdtloid,0) AND x.poitemmstoid<>rd.poitemmstoid),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE ISNULL(x.prdtloid,0)=ISNULL(rd.prdtloid,0)),0.0)) prqty, ISNULL(prm.prno,'') prno FROM QL_trnpoitemdtl rd INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.poitemunitoid LEFT JOIN QL_trnprdtl prd ON prd.prdtloid=ISNULL(rd.prdtloid,0) LEFT JOIN QL_trnprmst prm ON prm.prmstoid=ISNULL(rd.prmstoid,0) WHERE rd.cmpcode='{CompnyCode}' AND rd.poitemmstoid={id} ORDER BY rd.poitemdtlseq";
                dtl = db.Database.SqlQuery<listpoitemdtl>(sSql).ToList();

                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: poitem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No PO",["Supplier"] = "Supplier",["Project"] = "Project" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {
            sSql = "SELECT * FROM( Select pr.poitemmstoid, pr.poitemno, pr.poitemdate, c.suppname, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') projectname, ISNULL((SELECT rm.rabno FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') rabno, ISNULL((SELECT rm.soitemno FROM QL_trnsoitemmst rm WHERE rm.soitemmstoid=pr.somstoid),'') soitemno, CASE WHEN ISNULL(pr.poitemmstres1,'')='' THEN  pr.poitemmststatus ELSE ISNULL(pr.poitemmstres1,'') END poitemmststatus, CASE WHEN pr.poitemmststatus = 'Revised' THEN revisereason WHEN pr.poitemmststatus = 'Rejected' THEN rejectreason ELSE '' END reason, pr.poitemgrandtotalamt FROM QL_trnpoitemmst pr INNER JOIN QL_mstsupp c ON c.suppoid=pr.suppoid WHERE pr.cmpcode='" + CompnyCode + "' AND pr.poitemtype IN ('Jasa') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.poitemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.poitemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.poitemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.poitemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.poitemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.poitemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.poitemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.poitemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        // GET: RAB/Form
        public ActionResult Form(int? id, int? revid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpoitemmst tbl;
            string action = "Create";
            ViewBag.adddisc = "";
            if (id == null && revid == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trnpoitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.rabmstoid = 0;
                tbl.suppoid = 0;
                tbl.alamatoid = 0;
                tbl.suppsalesoid = 0;
                tbl.poitemmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.revpoitemtype = "Baru";
                tbl.revpoitemmstoid = 0;

                ViewBag.poitemdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else if (revid != null)//AddDiskon
            {
                action = "Create";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trnpoitemmst.Find(Session["CompnyCode"].ToString(), revid);

                string pono = db.Database.SqlQuery<string>("SELECT poitemno FROM QL_trnpoitemmst r WHERE r.poitemmstoid ='" + revid + "'").FirstOrDefault();

                string sNo = ClassFunction.Left(pono, 15) + "-R";
                sSql = "SELECT ISNULL(MAX(CAST(REPLACE(poitemno, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND poitemno LIKE '" + sNo + "%'";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                sNo = sNo + sCounter;

                tbl.poitemno = sNo;
                tbl.revpoitemtype = "Revisi";
                tbl.revpoitemmstoid = revid.Value;
                tbl.poitemmststatus = "In Process";

                ViewBag.revpoitemno = pono;
                ViewBag.adddisc = "adddisc";
                ViewBag.poitemdate = tbl.poitemdate.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trnpoitemmst.Find(Session["CompnyCode"].ToString(), id);

                ViewBag.poitemdate = tbl.poitemdate.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpoitemmst tbl, List<listpoitemdtl> dtDtl, string action, string adddisc)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";
            
            if (tbl.suppoid == 0)
                msg += "Silahkan Pilih Supplier Terlebih dahulu!<br>";
            if (tbl.alamatoid == 0)
                msg += "Silahkan Pilih Alamat Terlebih dahulu!<br>";
            if (tbl.suppsalesoid == 0)
                msg += "Silahkan Pilih Sales Supplier Terlebih dahulu!<br>";
            if (tbl.poitemmststatus.ToUpper() == "REVISED")
                tbl.poitemmststatus = "In Process";

            if (dtDtl == null)
                msg += "- Please fill detail data!<br />";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br />";
            else
            {
                for (int i = 0; i < dtDtl.Count(); i++)
                {
                    if (string.IsNullOrEmpty(dtDtl[i].poitemdtlnote))
                        dtDtl[i].poitemdtlnote = "";
                    if (dtDtl[i].poitemqty == 0)
                        msg += "Qty " + dtDtl[i].itemdesc + " Belum Diisi !!<br />";
                    if (dtDtl[i].poitemprice == 0)
                        msg += "Harga " + dtDtl[i].itemdesc + " Belum Diisi !!<br />";

                    //cek PR
                    if (dtDtl[i].prdtloid != 0)
                    {
                        decimal AmtOS = db.Database.SqlQuery<decimal>($"SELECT (prdtlamt - ISNULL((SELECT SUM(poitemqty * poitemprice) FROM QL_trnpoitemdtl x WHERE x.prdtloid=prd.prdtloid AND x.poitemmstoid<>{tbl.poitemmstoid}),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE x.prdtloid=prd.prdtloid),0.0)) amt FROM QL_trnprmst prm INNER JOIN QL_trnprdtl prd ON prd.prmstoid=prm.prmstoid WHERE prd.prdtloid={dtDtl[i].prdtloid}").FirstOrDefault();
                        dtDtl[i].prqty = AmtOS;
                        if ((dtDtl[i].poitemqty * dtDtl[i].poitemprice) > dtDtl[i].prqty)
                        {
                            msg += "Qty " + dtDtl[i].itemdesc + " Tidak Boleh Melebihi Qty PR!!<br />";
                        }
                    }
                }
            }

            if (adddisc == "adddisc")
            {
                decimal amtRevPO = db.Database.SqlQuery<decimal>("SELECT poitemgrandtotalamt FROM QL_trnpoitemmst WHERE poitemmstoid =" + tbl.revpoitemmstoid + "").FirstOrDefault();
                if (tbl.poitemgrandtotalamt >= amtRevPO)
                    msg += "Total PO Harus Lebih Kecil Dari Rev PO!!<br />";
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.poitemmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    msg += "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.poitemdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                msg += "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            if (tbl.poitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                    msg += "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }

            if (msg == "")
            {
                tbl.poitemtype = "Jasa";
                if (string.IsNullOrEmpty(tbl.poitemno))
                    tbl.poitemno = "";
                if (string.IsNullOrEmpty(tbl.poitemsuppref))
                    tbl.poitemsuppref = "";
                if (string.IsNullOrEmpty(tbl.poitemmstnote))
                    tbl.poitemmstnote = "";
                if (string.IsNullOrEmpty(tbl.poitemmstres1))
                    tbl.poitemmstres1 = "";
                if (string.IsNullOrEmpty(tbl.poitemmstres2))
                    tbl.poitemmstres2 = "";
                if (string.IsNullOrEmpty(tbl.poitemmstres3))
                    tbl.poitemmstres3 = "";
                if (string.IsNullOrEmpty(tbl.pocustname))
                    tbl.pocustname = "";

                var mstoid = ClassFunction.GenerateID("QL_trnpoitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpoitemdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.poitemmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.poitemtotaldiscamtidr = tbl.poitemtotaldiscamt;
                            tbl.rate2oid = 1;
                            db.QL_trnpoitemmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + tbl.poitemmstoid + " Where tablename = 'QL_trnpoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (tbl.rabmstoid != 0)
                            {
                                sSql = "UPDATE QL_trnrabdtl4 SET rabdtl4status='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl4oid IN (SELECT rabdtl2oid FROM QL_trnpoitemdtl WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + tbl.poitemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET statusbelijasa='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnpoitemdtl pd INNER JOIN QL_trnpoitemmst pm ON pm.poitemmstoid = pd.poitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND pm.poitemmstoid=" + tbl.poitemmstoid + " ANd pm.poitemtype = 'Jasa')";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            //update PR
                            sSql = "UPDATE QL_trnprdtl SET prdtlstatus='' WHERE prdtloid IN (SELECT ISNULL(prdtloid,0) FROM QL_trnpoitemdtl WHERE poitemmstoid=" + tbl.poitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnprmst SET prmststatus='Post' WHERE prmstoid IN (SELECT DISTINCT ISNULL(prmstoid,0) FROM QL_trnpoitemdtl WHERE poitemmstoid=" + tbl.poitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpoitemdtl.Where(a => a.poitemmstoid == tbl.poitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        if (adddisc == "adddisc")
                        {
                            sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Closed', poitemmstres1='Force Closed', closereason='Add Diskon PO', closeuser='" + Session["UserID"].ToString() + "', closetime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid =" + tbl.revpoitemmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        QL_trnpoitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnpoitemdtl)ClassFunction.MappingTable(new QL_trnpoitemdtl(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.poitemdtloid = dtloid++;
                            tbldtl.poitemmstoid = tbl.poitemmstoid;
                            tbldtl.poitemdtlseq = i + 1;
                            tbldtl.closeqty = 0;
                            tbldtl.poitemdtlstatus = "";
                            tbldtl.poitemdtldisctype = "P";
                            tbldtl.poitempriceidr = tbldtl.poitemprice;
                            tbldtl.poitemdtlamtidr = tbldtl.poitemdtlamt;
                            tbldtl.poitemdtldiscamtidr = tbldtl.poitemdtldiscamt;
                            tbldtl.poitemdtltaxamtidr = tbldtl.poitemdtltaxamt;
                            tbldtl.poitemdtlnettoidr = tbldtl.poitemdtlnetto;
                            tbldtl.poitemdtlres1 = "";
                            tbldtl.poitemdtlres2 = "";
                            tbldtl.poitemdtlres3 = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnpoitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.rabmstoid != 0)
                            {
                                if (dtDtl[i].poitemqty >= dtDtl[i].rabdtl2qty)
                                {
                                    sSql = "UPDATE QL_trnrabdtl4 SET rabdtl4status='Complete', upduser='" + tbl.createuser + "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" + CompnyCode + "' AND rabdtl4oid=" + dtDtl[i].rabdtl2oid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnrabmst SET statusbelijasa='PO Jasa PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tbl.rabmstoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnrabmst SET statusbelijasa='PO Jasa' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tbl.rabmstoid + " AND (SELECT COUNT(*) FROM QL_trnrabdtl4 WHERE cmpcode='" + CompnyCode + "' AND rabdtl4status='' AND rabmstoid=" + tbl.rabmstoid + " AND rabdtl4oid<>" + dtDtl[i].rabdtl2oid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            //update PR
                            if (dtDtl[i].prdtloid != 0)
                            {
                                if ((dtDtl[i].poitemqty * dtDtl[i].poitemprice) >= dtDtl[i].prqty)
                                {
                                    sSql = "UPDATE QL_trnprdtl SET prdtlstatus='Complete' WHERE prdtloid=" + dtDtl[i].prdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnprmst SET prmststatus='Closed' WHERE prmstoid=" + dtDtl[i].prmstoid + " AND (SELECT COUNT(prdtloid) FROM QL_trnprdtl WHERE prdtlstatus='' AND prmstoid=" + dtDtl[i].prmstoid + " AND prdtloid<>" + +dtDtl[i].prdtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.poitemmststatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.poitemmstoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnpojasamst";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.poitemmstoid.ToString();
                        sReturnNo = "No. " + tbl.poitemno;
                        if (tbl.poitemmststatus == "In Approval")
                        {
                            sReturnState = "Send Approval";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpoitemmst tblmst = db.QL_trnpoitemmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();
            int porefid = 0;
            if (tblmst.revpoitemmstoid > 0)
            {
                if (db.QL_trnpoitemmst.Where(x => x.poitemmstoid == tblmst.revpoitemmstoid && x.closereason == "Add Diskon PO").Count() > 0)
                {
                    porefid = tblmst.revpoitemmstoid;
                }
            }

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tblmst.rabmstoid != 0)
                        {
                            sSql = "UPDATE QL_trnrabdtl4 SET rabdtl4status='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl4oid IN (SELECT rabdtl2oid FROM QL_trnpoitemdtl WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusbelijasa='PO Jasa PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnpoitemdtl pd INNER JOIN QL_trnpoitemmst pm ON pm.poitemmstoid = pd.poitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND pm.poitemmstoid=" + id + " AND pm.poitemtype = 'Jasa')";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusbelijasa='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnpoitemdtl pd INNER JOIN QL_trnpoitemmst pm ON pm.poitemmstoid = pd.poitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND pm.poitemmstoid=" + id + " AND pm.poitemtype = 'Jasa') AND (SELECT COUNT(*) FROM QL_trnrabdtl4 WHERE cmpcode='" + CompnyCode + "' AND rabdtl4status='' AND rabmstoid IN (SELECT rabmstoid FROM QL_trnpoitemmst where poitemmstoid <> " + id + " AND poitemtype = 'Jasa'))=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //update PR
                        sSql = "UPDATE QL_trnprdtl SET prdtlstatus='' WHERE prdtloid IN (SELECT ISNULL(prdtloid,0) FROM QL_trnpoitemdtl WHERE poitemmstoid=" + tblmst.poitemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnprmst SET prmststatus='Post' WHERE prmstoid IN (SELECT DISTINCT ISNULL(prmstoid,0) FROM QL_trnpoitemdtl WHERE poitemmstoid=" + tblmst.poitemmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpoitemdtl.Where(a => a.poitemmstoid == id);
                        db.QL_trnpoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpoitemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: PRAB/AddDiskon
        [HttpPost, ActionName("AddDiskon")]
        [ValidateAntiForgeryToken]
        public ActionResult AddDiskonConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            var cekPOPB = db.QL_trnmritemmst.Where(m => m.pomstoid == id && m.mritemmstres1 == "").Count();
            var cekPOFB = db.QL_trnapitemmst.Where(m => m.poitemmstoid == id && m.apitemmstres1 == "").Count();
            if (cekPOFB > 0 || cekPOPB > 0)
            {
                msg += "- PO Masih Belum Di buatkan Return! <BR>";
            }
            if (msg != "")
            {
                result = "failed";
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: PRAB/ChangeKrimKe
        [HttpPost, ActionName("ChangeKrimKe")]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeKrimKeConfirmed(int? id, int? idkirimke)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            if (msg != "")
            {
                result = "failed";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnpoitemdtl SET poitemdtllocoid=" + idkirimke + " WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid=" + id + " ";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.poitemno FROM QL_trnpoitemmst arm WHERE arm.poitemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPOJasaItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.poitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptPOItem_" + sNo + ".pdf");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}