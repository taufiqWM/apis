﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class TBController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public TBController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listTBdtl
        {
            public int transformdtl1seq { get; set; }
            public int transformmstoid { get; set; }
            public string transformdtl1reftype { get; set; }
            public int transformdtl1refoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public decimal transformdtl1qty { get; set; }
            public decimal stockqty { get; set; }
            public int transformdtl1unitoid { get; set; }
            public string transformdtl1unit { get; set; }
            public int transformdtl1whoid { get; set; }
            public string transformdtl1wh { get; set; }
            public string transformdtl1note { get; set; }
            public decimal transformdtl1valueidr { get; set; }
            public decimal transformdtl1valueusd { get; set; }
            public string transformdtl1res2 { get; set; }
            public string serialnumber { get; set; }
            public int itemrefoid { get; set; }

        }

        public class listTBdtl2
        {
            public int transformdtl2seq { get; set; }
            public int transformmstoid { get; set; }
            public string transformdtl2reftype { get; set; }
            public int transformdtl2refoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public decimal transformdtl2qty { get; set; }
            public int transformdtl2unitoid { get; set; }
            public string transformdtl2unit { get; set; }
            public int transformdtl2whoid { get; set; }
            public string transformdtl2wh { get; set; }
            public string transformdtl2note { get; set; }
            public decimal transformdtl2valueidr { get; set; }
            public decimal transformdtl2valueusd { get; set; }
            public string serialnumber { get; set; }
            public int assetacctgoid { get; set; }
            public decimal transformdtl2taxamt { get; set; }
            public decimal transformdtl2totalamt { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itempid { get; set; }
            public string itemtype { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal stockqty { get; set; }
            public Decimal valueidr { get; set; }
            public string transformdtl1res2 { get; set; }
            public string serialnumber { get; set; }
            public int itemrefoid { get; set; }
            public int rabdtl2oid { get; set; }
        }

        public class listmat2
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itempid { get; set; }
            public string itemtype { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal stockqty { get; set; }
            public Decimal valueidr { get; set; }
            public string serialnumber { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public int rabmstoid_awal { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabtype { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public int curroid { get; set; }
            public string rabmstnote { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        private string generateCode()
        {
            string sCode = "PM/" + ClassFunction.GetServerTime().ToString("yy") + "/" + ClassFunction.GetServerTime().ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transformno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransformmst WHERE cmpcode='" + CompnyCode + "' AND transformno LIKE '" + sCode + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        private string generateNoSJ(DateTime tanggal)
        {
            string sNo = "SJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND shipmentitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private void InitDDL(QL_trntransformmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            var whoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.whoid);
            ViewBag.whoid = whoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            var towhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.towhoid);
            ViewBag.towhoid = towhoid;

        }

        [HttpPost]
        public ActionResult InitDDLWH()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='GUDANG' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWHTo()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='GUDANG' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select isnull(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + itemoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData(string itemtype)
        {
            List<listrab> tbl = new List<listrab>();
            string sPlus = "";
            if (itemtype == "Membongkar")
            {
                sPlus = " AND rm.rabmststatus IN('Approved','Closed') AND rm.rabmstoid IN(SELECT rabmstoid FROM QL_trntransformmst WHERE transformmststatus IN('Post','Closed'))";
            }
            else
            {
                sPlus = " AND rm.rabmststatus IN('Approved')";
            }
            sSql = "SELECT rm.rabmstoid, CASE WHEN rm.revrabtype='Baru' THEN rm.rabmstoid ELSE rm.revrabmstoid END rabmstoid_awal, rm.rabno, rm.rabdate, rm.projectname, rm.rabtype, rm.rabmstnote, rm.curroid, rm.deptoid, de.groupdesc departemen, c.custoid, c.custname, som.soitemmstoid, som.soitemno FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid WHERE som.soitemtype<>'Jasa' AND som.soitemmstoid IN(SELECT DISTINCT shm.soitemmstoid FROM QL_trnsoitemdtl shm INNER JOIN QL_mstitem i ON i.itemoid=shm.itemoid WHERE i.itemtype='Rakitan' AND shm.soitemmstoid=som.soitemmstoid) "+ sPlus +"";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataMatDetail(string itemtype, int rabmstoid, int rabmstoid_awal, string filteritem, string pidstart, string pidend, string filtermat, string filterpid, int whoid)
        {
            List<listmat> tbl = new List<listmat>();
            if (itemtype == "Membongkar")
            {
                if (rabmstoid != 0)
                {
                    string sPlus = "";
                    if (!string.IsNullOrEmpty(filteritem))
                    {
                        sPlus += " AND i.itemdesc LIKE'%" + filteritem + "%'";
                    }
                    if (!string.IsNullOrEmpty(pidstart) && !string.IsNullOrEmpty(pidend))
                    {
                        sPlus += " AND CAST(RIGHT(ISNULL(mrd.refno,''),4) AS INTEGER) BETWEEN '" + pidstart + "' AND '" + pidend + "'";
                    }

                    sSql = "SELECT 0 itemseq, i.itemoid, itemtype, itemcode, mrd.refno itempid, ISNULL(mrd.serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, mrd.transformdtl1qty stockqty, mrd.transformdtl1res2, mrd.itemrefoid, mrd.transformdtl1valueidr valueidr, 0 rabdtl2oid FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid INNER JOIN QL_trnrabdtl2 rd ON rd.itemdtl2oid=i.itemoid INNER JOIN QL_trntransformmst mrm ON mrm.rabmstoid=rd.rabmstoid INNER JOIN QL_trntransformdtl1 mrd ON mrd.transformmstoid=mrm.transformmstoid AND mrd.transformdtl1refoid=rd.itemdtl2oid WHERE mrm.rabmstoid=" + rabmstoid + " "+ sPlus +" ORDER BY itemcode";
                }    
            }
            else //Membuat & Merubah
            {
                string sPlus = ""; string sPlus2 = ""; string sPlus3 = ""; //string sPlusItemRefoid = "";
                if (itemtype == "Membuat")
                {
                    sPlus += " AND i.itemoid IN(SELECT rd2.itemdtl2oid FROM QL_trnrabdtl2 rd2 WHERE rd2.rabmstoid=" + rabmstoid + " AND ISNULL(rd2.itemdtl2refoid,0)<>0)";
                    sPlus2 += " AND i.itemoid IN(SELECT rd2.itemdtl2oid FROM QL_trnrabdtl2 rd2 WHERE rd2.rabmstoid=" + rabmstoid + " AND ISNULL(rd2.itemdtl2refoid,0)<>0)";
                    sPlus3 += " AND i.itemoid IN(SELECT rd2.itemdtl2oid FROM QL_trnrabdtl2 rd2 WHERE rd2.rabmstoid=" + rabmstoid + " AND ISNULL(rd2.itemdtl2refoid,0)<>0)";
                }
                //else
                //{
                //    sPlus = " AND itemtype='Rakitan'";
                //}
                if (rabmstoid != 0)
                {
                    sPlus += " AND ISNULL(con.rabmstoid,0)=" + rabmstoid_awal + "";
                    sPlus2 += " AND ISNULL(con.formoid,0)=" + rabmstoid + "";
                    sPlus3 += " AND ISNULL(rd.rabmstoid,0)=" + rabmstoid + "";
                    //sPlusItemRefoid += ", ISNULL((SELECT TOP 1 rd.itemdtl2refoid FROM QL_trnrabdtl2 rd WHERE rd.itemdtl2oid=tbl.itemoid AND rd.rabmstoid=" + rabmstoid + "),0) itemrefoid";
                }
                if (rabmstoid != 0)
                {
                    sPlus += " AND ISNULL(con.mtrwhoid,0)=" + whoid + "";
                    sPlus2 += " AND ISNULL(con.mtrwhoid,0)=" + whoid + "";
                    sPlus3 += " AND ISNULL(con.mtrwhoid,0)=" + whoid + "";
                }
                else
                {
                    if (itemtype == "Merubah")
                    {
                        sPlus += " AND ISNULL(con.mtrwhoid,0)=" + whoid + "";
                    }
                }
                if (!string.IsNullOrEmpty(filteritem))
                {
                    sPlus += " AND i.itemdesc LIKE'%" + filteritem + "%'";
                    sPlus2 += " AND i.itemdesc LIKE'%" + filteritem + "%'";
                    //sPlus3 += " AND i.itemdesc LIKE'%" + filteritem + "%'";
                }
                if (!string.IsNullOrEmpty(pidstart) && !string.IsNullOrEmpty(pidend))
                {
                    sPlus += " AND CAST(RIGHT(ISNULL(refno,''),4) AS INTEGER) BETWEEN '" + pidstart + "' AND '" + pidend + "'";
                    sPlus2 += " AND CAST(RIGHT(ISNULL(refno,''),4) AS INTEGER) BETWEEN '" + pidstart + "' AND '" + pidend + "'";
                    //sPlus3 += " AND CAST(RIGHT(ISNULL(con.refno,''),4) AS INTEGER) BETWEEN '" + pidstart + "' AND '" + pidend + "'";
                }
                if (!string.IsNullOrEmpty(filtermat))
                {
                    //sPlus += " AND i.itemoid NOT IN(" + filtermat + ")";
                }
                if (!string.IsNullOrEmpty(filterpid))
                {
                    //sPlus += " AND ISNULL(refno,'') NOT IN(" + filterpid + ")";
                    //sPlus2 += " AND ISNULL(refno,'') NOT IN(" + filterpid + ")";
                    //sPlus3 += " AND ISNULL(con.refno,'') NOT IN(" + filterpid + ")";
                }

                if (itemtype == "Merubah")
                {
                    sSql = "SELECT *, 0.0 valueidr, 0 itemrefoid, 0 rabdtl2oid FROM( ";
                    sSql += "SELECT itemseq, itemoid, itemtype, itemcode, itempid, serialnumber, itemdesc, itemunitoid, itemunit, (stockqty  - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=st.itemoid AND mb.mtrwhoid=st.mtrwhoid AND ISNULL(mb.refno,'')=st.itempid AND ISNULL(mb.serialnumber,'')=st.serialnumber AND mb.flag='Post'),0.0)) stockqty, transformdtl1res2, rabdtl2qty, numb FROM( ";
                    sSql += "SELECT 0 itemseq, itemoid, itemtype, itemcode, ISNULL(refno,'') itempid, ISNULL(serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, ISNULL(SUM(qtyin-qtyout),0.0) stockqty, '' transformdtl1res2, 0.0 rabdtl2qty, con.mtrwhoid, 0 numb FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid INNER JOIN QL_conmat con ON con.refoid = i.itemoid WHERE i.activeflag = 'ACTIVE' AND itemtype IN('Barang','Rakitan','Asset') " + sPlus + " GROUP BY itemoid, itemcode, ISNULL(refno,''), ISNULL(serialnumber,''), itemdesc, itemunitoid, itemtype, con.mtrwhoid, g.gndesc HAVING ISNULL(SUM(qtyin-qtyout),0.0)>0 ";
                    sSql += " )AS st ";
                    sSql += " )AS tbl WHERE tbl.stockqty>0 ORDER BY tbl.itemcode ";
                }
                else //Membuat
                {
                    sSql = "SELECT *, 0.0 valueidr FROM( ";
                    sSql += "select itemseq, itemoid, itemtype, itemcode, itempid, serialnumber, itemdesc, itemunitoid, itemunit, stockqty, transformdtl1res2, rabdtl2qty, numb, itemrefoid, rabdtl2oid from ( SELECT 0 itemseq, i.itemoid, itemtype, itemcode, ISNULL(con.refno,'') itempid, ISNULL(con.serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, CASE WHEN rd2.rabdtl2qty <= ISNULL(SUM(qtyin-qtyout),0.0) THEN rd2.rabdtl2qty ELSE ISNULL(SUM(qtyin-qtyout),0.0) END stockqty, '' transformdtl1res2, mtrwhoid, rd2.rabdtl2qty, 0 numb, rd2.itemdtl2refoid itemrefoid, 0 rabdtl2oid FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid INNER JOIN QL_conmat con ON con.refoid = i.itemoid INNER JOIN QL_trnmritemdtl mrd ON mrd.itemoid=con.refoid AND ISNULL(mrd.refno,'') =ISNULL(con.refno,'') AND ISNULL(mrd.serialnumber,'')=ISNULL(con.serialnumber,'') AND mrd.mritemwhoid=con.mtrwhoid inner join QL_trnmritemmst mrm ON mrm.mritemmstoid = mrd.mritemmstoid AND mrm.rabmstoid_awal = con.rabmstoid AND mrm.mritemmststatus IN('Post','Approved','Closed') /*AND isnull(mrm.mritemmstres1,'')=''*/ inner join QL_trnpoitemdtl pod ON pod.poitemdtloid = mrd.podtloid inner join QL_trnrabdtl2 rd2 ON rd2.rabdtl2oid = pod.rabdtl2oid WHERE i.activeflag = 'ACTIVE' AND itemtype IN('Barang','Rakitan','Asset') " + sPlus + " GROUP BY i.itemoid, itemcode, rd2.itemdtl2refoid, rd2.rabdtl2qty, ISNULL(con.refno,''), ISNULL(con.serialnumber,''), itemdesc, itemunitoid, itemtype, g.gndesc, mtrwhoid HAVING ISNULL(SUM(qtyin-qtyout),0.0)>0 ) as tb ";
                    sSql += " UNION ALL SELECT 0 itemseq, i.itemoid, itemtype, itemcode, ISNULL(refno,'') itempid, ISNULL(serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, ISNULL(SUM(qtyout),0.0) stockqty, 'Booking' transformdtl1res2, 0.0 rabdtl2qty, 0 numb, rd2.itemdtl2refoid itemrefoid, rd2.rabdtl2oid FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid INNER JOIN QL_matbooking con ON con.refoid = i.itemoid INNER JOIN QL_trnrabdtl2 rd2 ON rd2.rabmstoid=con.formoid AND rd2.itemdtl2oid=con.refoid AND rd2.rabdtl2qtybooking > 0 WHERE con.flag = 'Post' " + sPlus2 + " GROUP BY i.itemoid, rd2.itemdtl2refoid, rd2.rabdtl2oid, itemtype, itemcode, ISNULL(refno,''), ISNULL(serialnumber,''), itemdesc, itemunitoid, g.gndesc ";
                    sSql += "  UNION ALL  SELECT itemseq, itemoid, itemtype, itemcode, itempid, serialnumber, itemdesc, itemunitoid, itemunit, CASE WHEN rabdtl2qty>0 AND (stockqty - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid = t.itemoid AND mb.mtrwhoid = t.mtrwhoid AND ISNULL(mb.refno, '') = ISNULL(t.itempid, '') AND ISNULL(mb.serialnumber, '') = ISNULL(t.serialnumber, '') AND mb.flag = 'Post'),0.0))>rabdtl2qty THEN rabdtl2qty ELSE (stockqty - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid = t.itemoid AND mb.mtrwhoid = t.mtrwhoid AND ISNULL(mb.refno, '') = ISNULL(t.itempid, '') AND ISNULL(mb.serialnumber, '') = ISNULL(t.serialnumber, '') AND mb.flag = 'Post'),0.0)) END stockqty, transformdtl1res2, rabdtl2qty, 0 AS numb, itemrefoid, rabdtl2oid FROM( SELECT 0 itemseq, i.itemoid, itemtype, itemcode, ISNULL(mrd.refno, '') itempid, ISNULL(mrd.serialnumber, '') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, SUM(con.qtyin - con.qtyout) stockqty, 'poasset' transformdtl1res2, rd.rabdtl2qty, con.mtrwhoid, rd.itemdtl2refoid itemrefoid, rd.rabdtl2oid FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid AND g.gngroup='SATUAN' INNER JOIN QL_trnrabdtl2 rd ON rd.itemdtl2oid = i.itemoid INNER JOIN QL_trnpoassetdtl pod ON pod.poassetrefoid = rd.itemdtl2oid AND pod.poassetdtloid = ISNULL(rd.poassetdtloid, 0) AND pod.poassetmstoid = ISNULL(rd.poassetmstoid, 0) INNER JOIN QL_trnmrassetdtl mrd ON mrd.poassetdtloid = pod.poassetdtloid AND mrd.mrassetrefoid = rd.itemdtl2oid INNER JOIN QL_trnmrassetmst mrm ON mrm.mrassetmstoid = mrd.mrassetmstoid AND mrm.poassetmstoid = pod.poassetmstoid INNER JOIN QL_conmat con ON con.refoid = mrd.mrassetrefoid AND ISNULL(con.refno,'') = ISNULL(mrd.refno,'') AND ISNULL(con.serialnumber,'') = ISNULL(mrd.serialnumber,'') WHERE con.mtrwhoid IN(SELECT gnoid FROM QL_m05GN WHERE gngroup='gudang' and isnull(gnother1,'')='FPB') " + sPlus3 + " GROUP BY i.itemoid, rd.itemdtl2refoid, rd.rabdtl2oid, itemtype, itemcode, ISNULL(mrd.refno, ''), ISNULL(mrd.serialnumber, ''), itemdesc, itemunitoid, g.gndesc, rd.rabdtl2qty, con.mtrwhoid HAVING SUM(con.qtyin - con.qtyout) > 0 )AS t WHERE (stockqty - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid = t.itemoid AND mb.mtrwhoid = t.mtrwhoid AND ISNULL(mb.refno, '') = ISNULL(t.itempid, '') AND ISNULL(mb.serialnumber, '') = ISNULL(t.serialnumber, '') AND mb.flag = 'Post'),0.0)) > 0";
                    sSql += " )AS tbl ORDER BY tbl.itemcode ";
                }    
            }
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail2(string itemtype, int rabmstoid)
        {
            List<listmat2> tbl = new List<listmat2>();
            string sWhere = "";
            if (itemtype == "Membuat" || itemtype == "Membongkar")
            {
                sWhere = " AND i.itemtype='Rakitan' ";
            }

            if (itemtype == "Merubah")
            {
                sSql = "SELECT 0 itemseq, itemoid, itemtype, itemcode, '' itempid, '' serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, 0.0 stockqty, 0.0 valueidr FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE i.activeflag = 'ACTIVE' AND i.itemtype IN('Barang','Rakitan','Asset') ORDER BY i.itemcode";
            }
            else
            {
                if (rabmstoid != 0)
                {
                    sSql = "SELECT 0 itemseq, i.itemoid, itemtype, itemcode, '' itempid, '' serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, sod.soitemqty stockqty, CASE WHEN i.itemtype='Rakitan' THEN ISNULL((SELECT SUM(rd.rabdtl2price*rd.rabdtl2price) FROM QL_trnrabdtl2 rd INNER JOIN QL_mstitem i2 ON i2.itemoid=rd.itemdtl2oid WHERE i2.itemtype <> 'Ongkir' AND rd.rabmstoid=som.rabmstoid AND ISNULL(rd.itemdtl2refoid,0)=sod.itemoid),0) ELSE ISNULL((SELECT SUM(rd.rabdtl2price*rd.rabdtl2price) FROM QL_trnrabdtl2 rd INNER JOIN QL_mstitem i2 ON i2.itemoid=rd.itemdtl2oid WHERE i2.itemtype IN('Barang','Rakitan','Asset') AND rd.rabmstoid=som.rabmstoid AND ISNULL(rd.itemdtl2oid,0)=sod.itemoid),0) END valueidr FROM QL_trnsoitemmst som INNER JOIN QL_trnsoitemdtl sod ON sod.soitemmstoid=som.soitemmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_mstitem i ON i.itemoid=sod.itemoid INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE i.activeflag = 'ACTIVE' AND i.itemtype <> 'Ongkir' AND som.soitemtype <> 'Jasa' AND som.rabmstoid=" + rabmstoid + " "+ sWhere + " ORDER BY i.itemcode";
                }
            }
            tbl = db.Database.SqlQuery<listmat2>(sSql).ToList();
            if (tbl != null)
            {
                if (tbl.Count > 0)
                {
                    for (int i = 0; i < tbl.Count(); i++)
                    {
                        if (itemtype == "Membongkar")
                        {
                            tbl[i].valueidr = ClassFunction.GetStockValue(CompnyCode, tbl[i].itemoid);
                        }
                    }
                }
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<listTBdtl> dtDtl, List<listTBdtl2> dtDtl2, Guid? uid)
        {
            Session["QL_trntransformdtl1"+ uid] = dtDtl;
            Session["QL_trntransformdtl2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listTBdtl> dtDtl, Guid? uid)
        {
            Session["QL_trntransformdtl1"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listTBdtl2> dtDtl2, Guid? uid)
        {
            Session["QL_trntransformdtl2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trntransformdtl1"+ uid] == null)
            {
                Session["QL_trntransformdtl1"+ uid] = new List<listTBdtl>();
            }

            List<listTBdtl> dataDtl = (List<listTBdtl>)Session["QL_trntransformdtl1"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["QL_trntransformdtl2"+ uid] == null)
            {
                Session["QL_trntransformdtl2"+ uid] = new List<listTBdtl2>();
            }

            List<listTBdtl2> dataDtl = (List<listTBdtl2>)Session["QL_trntransformdtl2"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: TB
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Sumua Data";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Transform",["Project"] = "Project" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select tm.transformmstoid, transformno, transformdate, transformmststatus, transformmstnote, ISNULL(rm.projectname,'') projectname, ISNULL(c.custname,'') custname from QL_trntransformmst tm LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid=ISNULL(tm.rabmstoid,0) LEFT JOIN QL_mstcust c ON c.custoid=ISNULL(tm.custoid,0)) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.transformdate >='" + param.filterperiodfrom + " 00:00:00' AND t.transformdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.transformmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.transformno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.transformmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.transformmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.transformmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.transformmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: TB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransformmst tblmst;
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = new QL_trntransformmst();
                tblmst.transformmstoid = ClassFunction.GenerateID("QL_trntransformmst");
                tblmst.transformno = generateCode();
                tblmst.transformmststatus = "In Process";
                tblmst.cmpcode = CompnyCode;
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.transformdate = ClassFunction.GetServerTime();
                tblmst.transformdocdate = ClassFunction.GetServerTime();
                tblmst.updtime = ClassFunction.GetServerTime();
                tblmst.rabmstoid = 0;
                tblmst.soitemmstoid = 0;
                tblmst.custoid = 0;
                tblmst.shipmentitemmstoid = 0;
                tblmst.transformtaxvalue = 0;
                ViewBag.action = "Create";

                Session["QL_trntransformdtl1"+ ViewBag.uid] = null;
                Session["QL_trntransformdtl2"+ ViewBag.uid] = null;
            }
            else
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = db.QL_trntransformmst.Find(CompnyCode, id);
                ViewBag.action = "Edit";

                sSql = "select td.cmpcode, td.transformdtl1oid, td.transformmstoid, td.transformdtl1seq, td.transformdtl1reftype, td.transformdtl1refoid, td.transformdtl1whoid, transformdtl1qty, td.transformdtl1unitoid, td.transformdtl1note, td.transformdtl1valueidr, td.transformdtl1valueusd, ISNULL(td.refno,'') refno, ISNULL(td.serialnumber,'') serialnumber, i.itemcode, i.itemdesc, g.gndesc transformdtl1wh, g2.gndesc transformdtl1unit, ISNULL((SELECT SUM(con.qtyin-con.qtyout) FROM QL_conmat con WHERE con.cmpcode=td.cmpcode AND con.refoid=td.transformdtl1refoid AND ISNULL(con.refno,'')=ISNULL(td.refno,'') AND con.mtrwhoid=td.transformdtl1whoid),0.0) stockqty, ISNULL(transformdtl1res2,'') transformdtl1res2, ISNULL(td.serialnumber,'') serialnumber, ISNULL(td.itemrefoid,0) itemrefoid from ql_trntransformdtl1 td inner join QL_mstitem i on i.cmpcode = td.cmpcode and i.itemoid = td.transformdtl1refoid INNER JOIN QL_m05GN g on g.cmpcode = td.cmpcode and g.gnoid = td.transformdtl1whoid Inner Join QL_m05GN g2 on g2.cmpcode = td.cmpcode and td.transformdtl1unitoid = g2.gnoid WHERE td.cmpcode='" + CompnyCode + "' AND td.transformmstoid=" + id + " ORDER BY td.transformdtl1seq";
                Session["QL_trntransformdtl1"+ ViewBag.uid] = db.Database.SqlQuery<listTBdtl>(sSql).ToList();

                sSql = "select td.*, i.itemcode, i.itemdesc, g.gndesc transformdtl2wh, g2.gndesc transformdtl2unit, ISNULL(td.serialnumber,'') serialnumber from ql_trntransformdtl2 td inner join QL_mstitem i on i.cmpcode = td.cmpcode and i.itemoid = td.transformdtl2refoid INNER JOIN QL_m05GN g on g.cmpcode = td.cmpcode and g.gnoid = td.transformdtl2whoid Inner Join QL_m05GN g2 on g2.cmpcode = td.cmpcode and td.transformdtl2unitoid = g2.gnoid WHERE td.cmpcode='" + CompnyCode + "' AND td.transformmstoid=" + id + " ORDER BY td.transformdtl2seq";
                Session["QL_trntransformdtl2"+ ViewBag.uid] = db.Database.SqlQuery<listTBdtl2>(sSql).ToList();

            }
            ViewBag.deftax = db.QL_m05GN.FirstOrDefault(x => x.gngroup == "DEFAULT TAX").gndesc ?? "10";

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            InitDDL(tblmst);
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: TB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trntransformmst tblmst, string action, string tglmst, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tblmst.transformdate = DateTime.Parse(ClassFunction.toDate(tglmst));
                tblmst.transformdocdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("rabdate", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }

            List<listTBdtl> dtDtl = (List<listTBdtl>)Session["QL_trntransformdtl1"+ uid];
            if (dtDtl == null)
                ModelState.AddModelError("", "Detail Input Harus Di isi!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Detail Input Harus Di isi!");

            List<listTBdtl2> dtDtl2 = (List<listTBdtl2>)Session["QL_trntransformdtl2"+ uid];
            if (dtDtl2 == null)
                ModelState.AddModelError("", "Detail Output Harus Di isi!");
            else if (dtDtl2.Count <= 0)
                ModelState.AddModelError("", "Detail Output Harus Di isi!");

            if (string.IsNullOrEmpty(tblmst.transformno))
            {
                tblmst.transformno = generateCode();
            }

            if (string.IsNullOrEmpty(tblmst.transformmstnote))
                tblmst.transformmstnote = "";
            if (string.IsNullOrEmpty(tblmst.transformmstres1))
                tblmst.transformmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.transformmstres2))
                tblmst.transformmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.transformmstres3))
                tblmst.transformmstres3 = "";
            var servertime = ClassFunction.GetServerTime();
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            decimal totvalue = 0; decimal totqtyout = 0; decimal totqtyin = 0;
            int iDebetAcctgoid = 0; int iCreditAcctgoid = 0; int iWipAcctgoid = 0;
            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {

                        if (dtDtl[i].transformdtl1qty == 0)
                        {
                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transform tidak boleh 0");
                        }
                        if (dtDtl[i].transformdtl1qty.ToString() == "" )
                        {
                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transform tidak boleh Kosong");
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].transformdtl1note))
                            dtDtl[i].transformdtl1note = "";
                        if (string.IsNullOrEmpty(dtDtl[i].refno))
                            dtDtl[i].refno = "";
                        if (string.IsNullOrEmpty(dtDtl[i].serialnumber))
                            dtDtl[i].serialnumber = "";

                        if (tblmst.transformmststatus == "Post")
                        {
                            if (tblmst.transformtype == "Membongkar")
                            {
                                //decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].transformdtl1refoid);
                                //dtDtl[i].transformdtl1valueidr = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].transformdtl1refoid, dtDtl[i].transformdtl1qty, sValue, "");

                                totvalue += dtDtl[i].transformdtl1valueidr;
                                totqtyout += dtDtl[i].transformdtl1qty;
                            }
                            else
                            {
                                var rabmstoid_temp = tblmst.rabmstoid_awal;
                                if (dtDtl[i].transformdtl1res2 != "Booking")
                                {
                                    if (dtDtl[i].transformdtl1res2 == "poasset")
                                    {
                                        rabmstoid_temp = 0;
                                    }
                                    var a = dtDtl.Where(x => x.transformdtl1refoid == dtDtl[i].transformdtl1refoid && x.refno == dtDtl[i].refno && x.serialnumber == dtDtl[i].serialnumber && x.transformdtl1whoid == dtDtl[i].transformdtl1whoid).GroupBy(x => new { x.transformdtl1refoid, x.refno, x.serialnumber, x.transformdtl1whoid }).Select(x => new { itemoid = x.Key.transformdtl1refoid, refno = x.Key.refno, serialnumber = x.Key.serialnumber, whoid = x.Key.transformdtl1whoid, qty = x.Sum(y => y.transformdtl1qty) }).ToList();
                                    foreach (var da in a)
                                    {
                                        if (!ClassFunction.IsStockAvailable(CompnyCode, servertime, da.itemoid, da.whoid, da.qty, da.refno, rabmstoid_temp, da.serialnumber))
                                        {
                                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transform tidak boleh lebih dari Stock Qty");
                                        }
                                    }
                                }
                                else
                                {
                                    if (!ClassFunction.IsStockBookingAvailable(CompnyCode, servertime, dtDtl[i].transformdtl1refoid, dtDtl[i].transformdtl1whoid, dtDtl[i].transformdtl1qty, dtDtl[i].refno, 0, dtDtl[i].serialnumber))
                                    {
                                        ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transform dari Booking tidak boleh lebih dari Stock Qty");
                                    }
                                }
                                
                                decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].transformdtl1refoid);
                                dtDtl[i].transformdtl1valueidr = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].transformdtl1refoid, dtDtl[i].transformdtl1qty, sValue, "");

                                totvalue += dtDtl[i].transformdtl1valueidr;
                                totqtyout += dtDtl[i].transformdtl1qty;
                            }
                        }
                    }
                }
            }

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totqtyin += dtDtl2[i].transformdtl2qty;
                    }

                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        if (dtDtl2[i].transformdtl2qty == 0)
                        {
                            ModelState.AddModelError("", "Item " + dtDtl2[i].itemdesc + " Qty Transform Output tidak boleh 0");
                        }
                        if (dtDtl2[i].transformdtl2qty.ToString() == "")
                        {
                            ModelState.AddModelError("", "Item " + dtDtl2[i].itemdesc + " Qty Transform Output tidak boleh Kosong");
                        }
                        if (string.IsNullOrEmpty(dtDtl2[i].transformdtl2note))
                            dtDtl2[i].transformdtl2note = "";

                        if (tblmst.transformmststatus == "Post")
                        {
                            //Cek Component Sdh Complete
                            if (tblmst.transformtype == "Membuat")
                            {
                                int transformdtl2refoid_temp = dtDtl2[i].transformdtl2refoid;
                                int rabmstoid_rab = tblmst.rabmstoid.Value;
                                decimal qtyRab = 0;
                                decimal qtyTransform = 0;
                                qtyRab = db.QL_trnrabdtl2.Where(x => x.itemdtl2refoid.Value == transformdtl2refoid_temp && x.rabmstoid.Value == rabmstoid_rab).Sum(y => (y.rabdtl2qty.Value + y.rabdtl2qtybooking.Value));
                                qtyTransform = dtDtl.Where(x => x.itemrefoid == transformdtl2refoid_temp).Sum(y => y.transformdtl1qty);

                                if (qtyTransform != qtyRab)
                                {
                                    ModelState.AddModelError("", "Item " + dtDtl2[i].itemdesc + " Belum Lengkap, Silahkan Lengkapi Bahan Baku nya!");
                                }
                            }
                            //Set HPP
                            if (tblmst.transformtype == "Membuat")
                            {
                                //dtDtl2[i].transformdtl2valueidr = totvalue;
                                decimal valueOutput = dtDtl.Where(x => x.itemrefoid == dtDtl2[i].transformdtl2refoid).Sum(x => x.transformdtl1valueidr);
                                dtDtl2[i].transformdtl2valueidr = valueOutput;
                                dtDtl2[i].transformdtl2taxamt = 0;
                                dtDtl2[i].transformdtl2totalamt = valueOutput;
                            }
                            else if (tblmst.transformtype == "Merubah")
                            {
                                var totvalue2 = (totvalue / totqtyin);
                                var totamt = (totvalue2 * dtDtl2[i].transformdtl2qty);
                                dtDtl2[i].transformdtl2valueidr = totamt;
                                dtDtl2[i].transformdtl2totalamt = totamt;
                                if (tblmst.transformtaxvalue > 0)
                                {
                                    var taxamt = totamt * (tblmst.transformtaxvalue / 100);
                                    dtDtl2[i].transformdtl2taxamt = taxamt;
                                    dtDtl2[i].transformdtl2totalamt = (totamt + taxamt);
                                }

                            }
                            else // Membongkar
                            {
                                if (!ClassFunction.IsStockAvailable(CompnyCode, servertime, dtDtl2[i].transformdtl2refoid, dtDtl2[i].transformdtl2whoid, dtDtl2[i].transformdtl2qty, "", tblmst.rabmstoid_awal, dtDtl2[i].serialnumber))
                                {
                                    ModelState.AddModelError("", "Item " + dtDtl2[i].itemdesc + " Qty Transform tidak boleh lebih dari Stock Qty");
                                }
                                else
                                {
                                    //decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl2[i].transformdtl2refoid);
                                    //dtDtl2[i].transformdtl2valueidr = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl2[i].transformdtl2refoid, dtDtl2[i].transformdtl2qty, sValue, "");
                                    //dtDtl2[i].transformdtl2taxamt = 0;
                                    //dtDtl2[i].transformdtl2totalamt = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl2[i].transformdtl2refoid, dtDtl2[i].transformdtl2qty, sValue, "");
                                    decimal valueOutput = dtDtl.Where(x => x.itemrefoid == dtDtl2[i].transformdtl2refoid).Sum(x => x.transformdtl1valueidr);
                                    dtDtl2[i].transformdtl2valueidr = valueOutput;
                                    dtDtl2[i].transformdtl2taxamt = 0;
                                    dtDtl2[i].transformdtl2totalamt = valueOutput;
                                }
                            }
                        }
                            
                    }
                }
            }

            //if (tblmst.transformtype != "Membuat")
            //{
            //    decimal value2 = dtDtl2.Sum(x => (x.transformdtl2qty * x.transformdtl2valueidr));             
            //    if (value2 != totvalue)
            //    {
            //        ModelState.AddModelError("", "Total Amount Detail Output "+ value2 + " Harus sama Dengan Amount Detail Input " + totvalue + "");
            //    }
            //    else
            //    {
            //        if (dtDtl2 != null)
            //        {
            //            if (dtDtl2.Count > 0)
            //            {
            //                for (int i = 0; i < dtDtl2.Count(); i++)
            //                {
            //                    dtDtl2[i].transformdtl2valueidr = dtDtl2[i].transformdtl2valueidr * dtDtl2[i].transformdtl2qty;
            //                }
            //            }
            //        }
            //    }
            //}

            if (tblmst.transformmststatus == "Post")
            {
                //Ger Var Interfaces
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", tblmst.cmpcode))
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));
                }
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_WIP", tblmst.cmpcode))
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_WIP"));
                }
                //if (!ClassFunction.IsInterfaceExists("VAR_STOCK_RK", tblmst.cmpcode))
                //{
                //    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_RK"));
                //}
                if (tblmst.transformtype == "Membuat")
                {
                    //iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RK", tblmst.cmpcode));
                    iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tblmst.cmpcode));
                    iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tblmst.cmpcode));
                    iWipAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_WIP", tblmst.cmpcode));
                }
                else if (tblmst.transformtype == "Merubah")
                {
                    if (dtDtl2 != null)
                    {
                        if (dtDtl2.Count > 0)
                        {
                            for (int i = 0; i < dtDtl2.Count(); i++)
                            {
                                iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tblmst.cmpcode));
                                var itemoid = dtDtl2[i].transformdtl2refoid;
                                dtDtl2[i].assetacctgoid = db.QL_mstitem.FirstOrDefault(y => y.itemoid == itemoid).assetacctgoid;
                                //set coa asset
                                if (dtDtl2[i].transformdtl2reftype == "Asset")
                                    if (dtDtl2[i].assetacctgoid == 0)
                                        ModelState.AddModelError("", "Silahkan Set COA Asset di Master Item!");

                                //if (dtDtl2[i].transformdtl2reftype == "Asset")
                                //{
                                //    iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_ASSET", tblmst.cmpcode));
                                //}
                                //else if (dtDtl2[i].transformdtl2reftype == "Rakitan")
                                //{
                                //    iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RK", tblmst.cmpcode));
                                //}
                                //else
                                //{
                                //    iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tblmst.cmpcode));
                                //}  
                            }
                        }
                    }
                    if (dtDtl != null)
                    {
                        if (dtDtl.Count > 0)
                        {
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tblmst.cmpcode));
                                //if (dtDtl[i].transformdtl1reftype == "Asset")
                                //{
                                //    iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_ASSET", tblmst.cmpcode));
                                //}
                                //else if (dtDtl2[i].transformdtl2reftype == "Rakitan")
                                //{
                                //    iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RK", tblmst.cmpcode));
                                //}
                                //else
                                //{
                                //    iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_FG", tblmst.cmpcode));
                                //}
                            }
                        }
                    }
                }
                else //Membongkar
                {
                    iDebetAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tblmst.cmpcode));
                    iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tblmst.cmpcode));
                    //iCreditAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_RK", tblmst.cmpcode));
                }
                if (tblmst.shipmenttype == "Draft")
                {
                    ModelState.AddModelError("shipmenttype", "- Tipe Transaksi Draft, Silahkan Ganti Tipe Transaksi Menjadi Complete Jika Barang Sudah Lengkap!!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.transformdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.transformmststatus = "In Process";
            }
            if (tblmst.transformmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.transformmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trntransformmst");
                var dtloid = ClassFunction.GenerateID("QL_trntransformdtl1");
                var dtl2oid = ClassFunction.GenerateID("QL_trntransformdtl2");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                //var shipmentoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");

                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //if (tblmst.shipmentitemmstoid == 0)
                        //{
                        //    if (tblmst.shipmenttype == "SJ")
                        //    {
                        //        QL_trnshipmentitemmst tblsj;
                        //        tblsj = new QL_trnshipmentitemmst();
                        //        tblsj.cmpcode = tblmst.cmpcode;
                        //        tblsj.shipmentitemmstoid = shipmentoid++;
                        //        tblsj.shipmentitemtype = "";
                        //        tblsj.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                        //        tblsj.shipmentitemno = generateNoSJ(ClassFunction.GetServerTime());
                        //        tblsj.shipmentitemdate = tblmst.transformdate;
                        //        tblsj.soitemmstoid = tblmst.soitemmstoid.Value;
                        //        tblsj.custoid = tblmst.custoid.Value;
                        //        tblsj.ekspedisioid = 0;
                        //        tblsj.ekspedisinoresi = "";
                        //        tblsj.driveroid = "";
                        //        tblsj.armadaoid = 0;
                        //        tblsj.shipmentitemmstnote = "";
                        //        tblsj.shipmentitemmstres1 = "";
                        //        tblsj.shipmentitemmstres2 = "";
                        //        tblsj.shipmentitemmstres3 = "";
                        //        tblsj.shipmentitemmststatus = "In Process";
                        //        tblsj.createtime = servertime;
                        //        tblsj.createuser = Session["UserID"].ToString();
                        //        tblsj.updtime = servertime;
                        //        tblsj.upduser = Session["UserID"].ToString();
                        //        tblsj.curroid = 1;
                        //        tblsj.rate2oid = 0;
                        //        tblsj.rabmstoid = tblmst.rabmstoid.Value;

                        //        db.QL_trnshipmentitemmst.Add(tblsj);
                        //        db.SaveChanges();

                        //        //Update lastoid
                        //        sSql = "Update QL_ID set lastoid = " + shipmentoid + " Where tablename = 'QL_trnshipmentitemmst'";
                        //        db.Database.ExecuteSqlCommand(sSql);
                        //        db.SaveChanges();

                        //        tblmst.shipmentitemmstoid = tblsj.shipmentitemmstoid;
                        //    }    
                        //}

                        if (action == "Create")
                        {
                            //Insert
                            tblmst.transformmstoid = mstoid;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trntransformmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trntransformmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            if (tblmst.transformtype == "Membuat")
                            {
                                sSql = "UPDATE QL_matbooking SET flag='Post' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tblmst.rabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var trndtl = db.QL_trntransformdtl1.Where(a => a.transformmstoid == tblmst.transformmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trntransformdtl1.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trntransformdtl2.Where(a => a.transformmstoid == tblmst.transformmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trntransformdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        //--------------------Input
                        QL_trntransformdtl1 tbldtl;
                        QL_conmat con;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trntransformdtl1();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.transformdtl1oid = dtloid++;
                            tbldtl.transformmstoid = tblmst.transformmstoid;
                            tbldtl.transformdtl1seq = i + 1;
                            tbldtl.transformdtl1reftype = dtDtl[i].transformdtl1reftype;
                            tbldtl.transformdtl1refoid = dtDtl[i].transformdtl1refoid;
                            tbldtl.transformdtl1whoid = dtDtl[i].transformdtl1whoid;
                            tbldtl.transformdtl1qty = dtDtl[i].transformdtl1qty;
                            tbldtl.transformdtl1unitoid = dtDtl[i].transformdtl1unitoid;
                            tbldtl.transformdtl1note = dtDtl[i].transformdtl1note;
                            tbldtl.transformdtl1status = "";
                            tbldtl.transformdtl1res1 = "";
                            tbldtl.transformdtl1res2 = dtDtl[i].transformdtl1res2;
                            tbldtl.transformdtl1res3 = "";
                            tbldtl.transformdtl1valueidr = dtDtl[i].transformdtl1valueidr;
                            tbldtl.transformdtl1valueusd = 0;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.refno = dtDtl[i].refno;
                            tbldtl.serialnumber = dtDtl[i].serialnumber;
                            tbldtl.itemrefoid = dtDtl[i].itemrefoid;

                            db.QL_trntransformdtl1.Add(tbldtl);
                            db.SaveChanges();

                            if (tblmst.transformmststatus == "Post")
                            {
                                if (tblmst.transformtype != "Membongkar")
                                {
                                    int rab = tblmst.rabmstoid_awal;
                                    if (dtDtl[i].transformdtl1res2 == "Booking" || dtDtl[i].transformdtl1res2 == "poasset")
                                    {
                                        //rab = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT formoid FROM QL_matbooking WHERE formoid=" + tblmst.rabmstoid_awal + " AND refoid=" + dtDtl[i].transformdtl1refoid + " AND mtrwhoid=" + dtDtl[i].transformdtl1whoid + " AND ISNULL(refno,'')='" + dtDtl[i].refno + "'),0) AS tbl").FirstOrDefault();
                                        rab = 0;
                                    }

                                    var refname1 = "FINISH GOOD";
                                    if (dtDtl[i].transformdtl1reftype == "Asset")
                                    {
                                        refname1 = "FIXED ASSET";
                                    }

                                    //CONMAT OUT
                                    con = new QL_conmat();
                                    con.cmpcode = tblmst.cmpcode;
                                    con.conmatoid = conmatoid++;
                                    con.type = "TBO";
                                    con.typemin = -1;
                                    con.trndate = servertime;
                                    con.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                    con.formaction = "QL_trntransformdtl1";
                                    con.formoid = tblmst.transformmstoid;
                                    con.refoid = dtDtl[i].transformdtl1refoid;
                                    con.refname = refname1;
                                    con.mtrwhoid = dtDtl[i].transformdtl1whoid;
                                    con.qtyin = 0;
                                    con.qtyout = dtDtl[i].transformdtl1qty;
                                    con.reason = refname1 + " Transformation";
                                    con.note = tblmst.transformno;
                                    con.refno = dtDtl[i].refno;
                                    con.valueidr = dtDtl[i].transformdtl1valueidr;
                                    con.valueusd = 0;
                                    con.deptoid = 0;
                                    con.valueidr_backup = dtDtl[i].transformdtl1valueidr;
                                    con.valueusd_backup = 0;
                                    con.conres = "";
                                    con.formdtloid = tbldtl.transformdtl1oid;
                                    con.upduser = tblmst.upduser;
                                    con.updtime = tblmst.updtime;
                                    con.rabmstoid = rab;
                                    con.serialnumber = dtDtl[i].serialnumber;

                                    db.QL_conmat.Add(con);
                                    db.SaveChanges();

                                }
                                else //Membongkar
                                {
                                    var refname1 = "FINISH GOOD";
                                    if (dtDtl[i].transformdtl1reftype == "Asset")
                                    {
                                        refname1 = "FIXED ASSET";
                                    }
                                    //CONMAT IN
                                    con = new QL_conmat();
                                    con.cmpcode = tblmst.cmpcode;
                                    con.conmatoid = conmatoid++;
                                    con.type = "TBI";
                                    con.typemin = 1;
                                    con.trndate = servertime;
                                    con.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                    con.formaction = "QL_trntransformdtl1";
                                    con.formoid = tblmst.transformmstoid;
                                    con.refoid = dtDtl[i].transformdtl1refoid;
                                    con.refname = refname1;
                                    con.mtrwhoid = dtDtl[i].transformdtl1whoid;
                                    con.qtyin = dtDtl[i].transformdtl1qty;
                                    con.qtyout = 0;
                                    con.reason = refname1 + " Transformation";
                                    con.note = tblmst.transformno;
                                    con.refno = dtDtl[i].refno;
                                    con.valueidr = dtDtl[i].transformdtl1valueidr;
                                    con.valueusd = 0;
                                    con.deptoid = 0;
                                    con.valueidr_backup = dtDtl[i].transformdtl1valueidr;
                                    con.valueusd_backup = 0;
                                    con.conres = "";
                                    con.formdtloid = tbldtl.transformdtl1oid;
                                    con.upduser = tblmst.upduser;
                                    con.updtime = tblmst.updtime;
                                    con.rabmstoid = (dtDtl[i].transformdtl1res2 == "poasset" ? 0 : tblmst.rabmstoid_awal);
                                    con.serialnumber = dtDtl[i].serialnumber;

                                    db.QL_conmat.Add(con);
                                    db.SaveChanges();
                                }     
                            }
                            if (tblmst.transformtype == "Membuat")
                            {
                                if (tblmst.shipmenttype == "Complete")
                                {
                                    if (dtDtl[i].transformdtl1res2 == "Booking")
                                    {
                                        sSql = "UPDATE QL_matbooking SET flag='Closed' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tblmst.rabmstoid + " AND refoid=" + dtDtl[i].transformdtl1refoid + " AND ISNULL(refno,'')='" + (dtDtl[i].refno ?? "") + "' AND ISNULL(serialnumber,'')='" + (dtDtl[i].serialnumber ?? "") + "' ";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }  
                                }   
                            }
                            if (tblmst.transformmststatus == "Post")
                            {
                                if (tblmst.transformtype == "Membongkar")
                                {
                                    if (dtDtl[i].transformdtl1res2 == "Booking")
                                    {
                                        sSql = "UPDATE QL_matbooking SET flag='Post' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tblmst.rabmstoid + " AND refoid=" + dtDtl[i].transformdtl1refoid + " AND ISNULL(refno,'')='" + (dtDtl[i].refno ?? "") + "' AND ISNULL(serialnumber,'')='" + (dtDtl[i].serialnumber ?? "") + "' ";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trntransformdtl1'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        
                        //--------------------------end of dtl input

                        //--------------------------Output
                        QL_trntransformdtl2 tbldtl2;
                        for (int i = 0; i < dtDtl2.Count(); i++)
                        {
                            tbldtl2 = new QL_trntransformdtl2();
                            tbldtl2.cmpcode = tblmst.cmpcode;
                            tbldtl2.transformdtl2oid = dtl2oid++;
                            tbldtl2.transformmstoid = tblmst.transformmstoid;
                            tbldtl2.transformdtl2seq = i + 1;
                            tbldtl2.transformdtl2reftype = dtDtl2[i].transformdtl2reftype;
                            tbldtl2.transformdtl2refoid = dtDtl2[i].transformdtl2refoid;
                            tbldtl2.transformdtl2whoid = dtDtl2[i].transformdtl2whoid;
                            tbldtl2.transformdtl2qty = dtDtl2[i].transformdtl2qty;
                            tbldtl2.transformdtl2unitoid = dtDtl2[i].transformdtl2unitoid;
                            tbldtl2.transformdtl2note = dtDtl2[i].transformdtl2note;
                            tbldtl2.transformdtl2status = "";
                            tbldtl2.transformdtl2res1 = "";
                            tbldtl2.transformdtl2res2 = "";
                            tbldtl2.transformdtl2res3 = "";
                            tbldtl2.transformdtl2valueidr = dtDtl2[i].transformdtl2valueidr;
                            tbldtl2.transformdtl2valueusd = 0;
                            tbldtl2.transformdtl2taxamt = dtDtl2[i].transformdtl2taxamt;
                            tbldtl2.transformdtl2totalamt = dtDtl2[i].transformdtl2totalamt;
                            tbldtl2.upduser = tblmst.upduser;
                            tbldtl2.updtime = tblmst.updtime;
                            tbldtl2.refno = "";
                            tbldtl2.serialnumber = "";

                            db.QL_trntransformdtl2.Add(tbldtl2);
                            db.SaveChanges();

                            if (tblmst.transformmststatus == "Post")
                            {
                                if (tblmst.transformtype != "Membongkar")
                                {
                                    var refname2 = "FINISH GOOD";
                                    if (dtDtl2[i].transformdtl2reftype == "Asset")
                                    {
                                        refname2 = "FIXED ASSET";
                                    }
                                    if (dtDtl2[i].transformdtl2reftype != "Asset")
                                    {
                                        //CONMAT IN
                                        con = new QL_conmat();
                                        con.cmpcode = tblmst.cmpcode;
                                        con.conmatoid = conmatoid++;
                                        con.type = "TBI";
                                        con.typemin = 1;
                                        con.trndate = servertime;
                                        con.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                        con.formaction = "QL_trntransformdtl2";
                                        con.formoid = tblmst.transformmstoid;
                                        con.refoid = dtDtl2[i].transformdtl2refoid;
                                        con.refname = refname2;
                                        con.mtrwhoid = dtDtl2[i].transformdtl2whoid;
                                        con.qtyin = dtDtl2[i].transformdtl2qty;
                                        con.qtyout = 0;
                                        con.reason = refname2 + " Transformation";
                                        con.note = tblmst.transformno;
                                        con.refno = "";
                                        con.valueidr = dtDtl2[i].transformdtl2valueidr;
                                        con.valueusd = 0;
                                        con.deptoid = 0;
                                        con.valueidr_backup = dtDtl2[i].transformdtl2valueidr;
                                        con.valueusd_backup = 0;
                                        con.conres = "";
                                        con.formdtloid = tbldtl2.transformdtl2oid;
                                        con.upduser = tblmst.upduser;
                                        con.updtime = tblmst.updtime;
                                        con.rabmstoid = tblmst.rabmstoid_awal;
                                        con.serialnumber = "";

                                        db.QL_conmat.Add(con);
                                        db.SaveChanges();
                                    }
                                }
                                else //Membongkar
                                {
                                    var refname2 = "FINISH GOOD";
                                    if (dtDtl2[i].transformdtl2reftype == "Asset")
                                    {
                                        refname2 = "FIXED ASSET";
                                    }
                                    //CONMAT OUT
                                    con = new QL_conmat();
                                    con.cmpcode = tblmst.cmpcode;
                                    con.conmatoid = conmatoid++;
                                    con.type = "TBO";
                                    con.typemin = -1;
                                    con.trndate = servertime;
                                    con.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                    con.formaction = "QL_trntransformdtl2";
                                    con.formoid = tblmst.transformmstoid;
                                    con.refoid = dtDtl2[i].transformdtl2refoid;
                                    con.refname = refname2;
                                    con.mtrwhoid = dtDtl2[i].transformdtl2whoid;
                                    con.qtyin = 0;
                                    con.qtyout = dtDtl2[i].transformdtl2qty;
                                    con.reason = refname2 + " Transformation";
                                    con.note = tblmst.transformno;
                                    con.refno = "";
                                    con.valueidr = dtDtl2[i].transformdtl2valueidr;
                                    con.valueusd = 0;
                                    con.deptoid = 0;
                                    con.valueidr_backup = dtDtl2[i].transformdtl2valueidr;
                                    con.valueusd_backup = 0;
                                    con.conres = "";
                                    con.formdtloid = tbldtl2.transformdtl2oid;
                                    con.upduser = tblmst.upduser;
                                    con.updtime = tblmst.updtime;
                                    con.rabmstoid = tblmst.rabmstoid_awal;
                                    con.serialnumber = "";

                                    db.QL_conmat.Add(con);
                                    db.SaveChanges();
                                }
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trntransformdtl2'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        //------------------------------- end output

                        if (tblmst.transformmststatus == "Post")
                        {
                            decimal dTotalAmt = dtDtl.Sum(x => x.transformdtl1valueidr);
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Transform|No. " + tblmst.transformno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            // Insert QL_trngldtl
                            if (tblmst.transformtype == "Merubah")
                            {
                                //tipe asset
                                var listGLAsset = dtDtl2.Where(x => x.assetacctgoid != 0).GroupBy(x => x.assetacctgoid).Select(x => new { acctgoid = x.Key, amtIDR = x.Sum(y => y.transformdtl2valueidr), taxamtIDR = x.Sum(y2 => y2.transformdtl2taxamt), totalamtIDR = x.Sum(y3 => y3.transformdtl2totalamt) }).ToList();
                                if (listGLAsset.Count > 0)
                                {
                                    foreach (var item in listGLAsset)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, item.acctgoid, "D", item.totalamtIDR, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, item.totalamtIDR, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));
                                        if (item.taxamtIDR > 0)
                                        {//jurnal tax
                                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "C", item.taxamtIDR, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, item.taxamtIDR, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));
                                        }
                                    }
                                }
                                //tipe selain asset
                                listGLAsset = dtDtl2.Where(x => x.assetacctgoid == 0).GroupBy(x => x.assetacctgoid).Select(x => new { acctgoid = x.Key, amtIDR = x.Sum(y => y.transformdtl2valueidr), taxamtIDR = x.Sum(y => y.transformdtl2taxamt), totalamtIDR = x.Sum(y => y.transformdtl2totalamt) }).ToList();
                                if (listGLAsset.Count > 0)
                                {
                                    foreach (var item in listGLAsset)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iDebetAcctgoid, "D", item.amtIDR, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, item.amtIDR, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));
                                    }
                                }
                            }
                            else //selain tipe merubah
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iDebetAcctgoid, "D", dTotalAmt, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, dTotalAmt, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));
                            }
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iCreditAcctgoid, "C", dTotalAmt, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, dTotalAmt, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));

                            if (tblmst.transformtype == "Membuat")
                            {
                                glmstoid += 1;
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Transform|No. " + tblmst.transformno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                var glseq2 = 1;
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq2++, glmstoid, iWipAcctgoid, "D", dTotalAmt, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, dTotalAmt, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq2++, glmstoid, iWipAcctgoid, "C", dTotalAmt, tblmst.transformno, "Transform|No. " + tblmst.transformno, "Post", Session["UserID"].ToString(), servertime, dTotalAmt, 0, "QL_trntransformmst " + tblmst.transformmstoid, null, null, null, 0));
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        tblmst.transformmststatus = "In Process";
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.transformmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_trntransformmst tblmst)
        {
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.soitemno = db.Database.SqlQuery<string>("SELECT soitemno FROM QL_trnsoitemmst r WHERE r.soitemmstoid ='" + tblmst.soitemmstoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust r WHERE r.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransformmst tblmst = db.QL_trntransformmst.Find(CompnyCode, id);
            List<QL_trntransformdtl1> dtDtl = db.QL_trntransformdtl1.Where(a => a.transformmstoid == id).ToList();
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //if (tblmst.shipmentitemmstoid != 0)
                        //{
                        //    var trnsj = db.QL_trnshipmentitemmst.Where(a => a.shipmentitemmstoid == tblmst.shipmentitemmstoid);
                        //    db.QL_trnshipmentitemmst.RemoveRange(trnsj);
                        //    db.SaveChanges();

                        //    var trnsjdtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == tblmst.shipmentitemmstoid);
                        //    db.QL_trnshipmentitemdtl.RemoveRange(trnsjdtl);
                        //    db.SaveChanges();
                        //}

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (tblmst.transformtype == "Membuat")
                            {
                                if (tblmst.shipmenttype == "Complete")
                                {
                                    if (dtDtl[i].transformdtl1res2 == "Booking")
                                    {
                                        sSql = "UPDATE QL_matbooking SET flag='Post' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tblmst.rabmstoid + " AND refoid=" + dtDtl[i].transformdtl1refoid + " AND ISNULL(refno,'')='" + (dtDtl[i].refno ?? "") + "' AND ISNULL(serialnumber,'')='" + (dtDtl[i].serialnumber ?? "") + "' ";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        } 

                        var trndtl = db.QL_trntransformdtl1.Where(a => a.transformmstoid == id);
                        db.QL_trntransformdtl1.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trntransformdtl2.Where(a => a.transformmstoid == id);
                        db.QL_trntransformdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        db.QL_trntransformmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptTransform.rpt"));

            report.SetParameterValue("swhere", " WHERE transm.cmpcode='" + CompnyCode + "' AND transm.transformmstoid IN (" + id + ")");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("bu", CompnyName);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PrintOutTransformBarang.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}