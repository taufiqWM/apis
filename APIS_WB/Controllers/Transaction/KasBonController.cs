﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class KasBonController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public KasBonController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class kasbonmst
        {
            public string cmpcode { get; set; }
            public int kasbonmstoid { get; set; }
            public string kasbonno { get; set; }
            public DateTime kasbondate { get; set; }
            public string kasbongroup { get; set; }
            public string acctgdesc { get; set; }
            public string kasbonmststatus { get; set; }
            public string kasbonnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal kasbonamt { get; set; }
        }

        public class kasbondtl
        {
            public int kasbondtlseq { get; set; }
            public string personoid { get; set; }
            public string personname { get; set; }
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
            public decimal kasbondtlamt { get; set; }
            public string kasbondtlnote { get; set; }
            public decimal kasbondtlamtidr { get; set; }
            public decimal kasbondtlamtusd { get; set; }
        }

        private void InitDDL(QL_trnkasbonmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            var personoiddtl = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", ViewBag.personoiddtl);
            ViewBag.personoiddtl = personoiddtl;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoiddtl = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", ViewBag.acctgoiddtl);
            ViewBag.acctgoiddtl = acctgoiddtl;

        }

        [HttpPost]
        public ActionResult InitDDLPerson()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m01US> tbl = new List<QL_m01US>();
            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            tbl = db.Database.SqlQuery<QL_m01US>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindPersonAmt(string kasbongroup, string personoid)
        {
            decimal amt = 0;
            if (kasbongroup != "PEMBERIAN")
            {
                sSql = "SELECT ISNULL((SELECT SUM(kd.kasbondtlamt * km.typemin) AS amt FROM QL_trnkasbonmst km INNER JOIN QL_trnkasbondtl kd ON kd.kasbonmstoid=km.kasbonmstoid WHERE kd.personoid='"+ personoid + "' AND km.kasbonmststatus IN('Post')),0.0) AS amt";
                amt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            }

            JsonResult js = Json(amt, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<kasbondtl> tbl = new List<kasbondtl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 kasbondtlamt, '' kasbondtlnote, curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<kasbondtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string kasbonno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(kasbonno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<kasbondtl> dtDtl)
        {
            Session["QL_trnkasbondtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnkasbondtl"] == null)
            {
                Session["QL_trnkasbondtl"] = new List<kasbondtl>();
            }

            List<kasbondtl> dataDtl = (List<kasbondtl>)Session["QL_trnkasbondtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnkasbonmst tbl)
        {
            
        }

        private string GenerateExpenseNo2(string cmpcode, string kasbondate, string kasbontype, int acctgoid)
        {
            var kasbonno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(kasbondate));
            if (CompnyCode != "")
            {
                string sNo = "PTK/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(kasbonno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnkasbonmst WHERE cmpcode='" + CompnyCode + "' AND kasbonno LIKE '%" + sNo + "%'";
                kasbonno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return kasbonno;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmpcode, string kasbondate, string kasbontype, int acctgoid)
        {
            var kasbonno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(kasbondate));
            if (CompnyCode != "")
            {
                string sNo = "PTK/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(kasbonno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnkasbonmst WHERE cmpcode='" + CompnyCode + "' AND kasbonno LIKE '%" + sNo + "%'";
                kasbonno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(kasbonno, JsonRequestBehavior.AllowGet);
        }

        // GET: kasbonMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT cb.cmpcode, cb.kasbonmstoid, cb.kasbonno, cb.kasbondate, cb.kasbongroup, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, cb.kasbonmststatus, cb.kasbonnote, cb.kasbonamt FROM QL_trnkasbonmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid WHERE cb.cmpcode='"+ CompnyCode +"'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND kasbondate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND kasbondate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND kasbonmststatus IN ('In Process')";
            }

            sSql += " ORDER BY CONVERT(DATETIME, cb.kasbondate) DESC";

            List<kasbonmst> dt = db.Database.SqlQuery<kasbonmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: kasbonMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnkasbonmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trnkasbonmst();
                tbl.kasbonmstoid = ClassFunction.GenerateID("QL_trnkasbonmst");
                tbl.kasbondate = ClassFunction.GetServerTime();
                tbl.kasbonduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.kasbonmststatus = "In Process";

                Session["QL_trnkasbondtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnkasbonmst.Find(CompnyCode, id);

                sSql = "SELECT cbgl.kasbondtlseq, cbgl.personoid, p.usname personname, cbgl.acctgoid, a.acctgdesc, cbgl.kasbondtlamt, cbgl.kasbondtlnote FROM QL_trnkasbondtl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid INNER JOIN QL_m01US p ON p.usoid=cbgl.personoid WHERE cbgl.kasbonmstoid=" + id + " AND cbgl.cmpcode='" + CompnyCode + "' ORDER BY cbgl.kasbondtlseq";
                var tbldtl = db.Database.SqlQuery<kasbondtl>(sSql).ToList();
                Session["QL_trnkasbondtl"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: kasbonMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnkasbonmst tbl, string action, string tglmst, string tglduedate)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                tbl.kasbondate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("kasbondate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.kasbonduedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("kasbondate", "Format Tanggal Due Date Tidak Valid!!" + ex.ToString());
            }

            if (tbl.kasbonno == null)
                tbl.kasbonno = "";

            List<kasbondtl> dtDtl = (List<kasbondtl>)Session["QL_trnkasbondtl"];
            string sErrReply = "";
            if (tbl.kasbontype == "BBK" || tbl.kasbontype == "BBM")
            {
                if (tbl.kasbondate > tbl.kasbonduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than EXPENSE DATE");
            }
            if (tbl.kasbonamt == 0)
            {
                ModelState.AddModelError("", "GRAND TOTAL must be more than 0!");
            }
            else
            {
                if (!ClassFunction.isLengthAccepted("kasbonamt", "QL_trnkasbonmst", tbl.kasbonamt, ref sErrReply))
                    ModelState.AddModelError("", "GRAND TOTAL must be less than MAX GRAND TOTAL (" + sErrReply + ") allowed stored in database!");
            }

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].personoid))
                        {
                            ModelState.AddModelError("", "Please Select PIC !");
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].acctgdesc))
                        {
                            ModelState.AddModelError("", "Please Select Detail Account !");
                        }
                        if (dtDtl[i].kasbondtlamt == 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be not equal to 0!");
                        }
                        else
                        {
                            if (tbl.kasbonmststatus == "Post")
                            {
                                decimal amt = 0;
                                if (tbl.kasbongroup != "PEMBERIAN")
                                {
                                    sSql = "SELECT ISNULL((SELECT SUM(kd.kasbondtlamt * km.typemin) AS amt FROM QL_trnkasbonmst km INNER JOIN QL_trnkasbondtl kd ON kd.kasbonmstoid=km.kasbonmstoid WHERE kd.personoid='" + dtDtl[i].personoid + "' AND km.kasbonmststatus IN('Post')),0.0) AS amt";
                                    amt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                                    if (dtDtl[i].kasbondtlamt > amt)
                                    {
                                        ModelState.AddModelError("", "PIC " + dtDtl[i].personname + ", AMOUNT must be more than Max Amount " + amt + " !");
                                    }
                                }
                            }
                            if (!ClassFunction.isLengthAccepted("kasbondtlamt", "QL_trnkasbondtl", dtDtl[i].kasbondtlamt, ref sErrReply))
                            {
                                ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!");
                            }
                        }
                    }
                }
            }
            
            DateTime sDueDate = new DateTime();
            if (tbl.kasbontype == "BKK" || tbl.kasbontype == "BKM")
                sDueDate = tbl.kasbondate;
            else
                sDueDate = tbl.kasbonduedate;
            DateTime sDate = tbl.kasbondate;
            if (tbl.kasbontype == "BBK" || tbl.kasbontype == "BBM")
                sDate = tbl.kasbonduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();
            if (tbl.kasbonmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.kasbondate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.kasbonmststatus = "In Process";
            }
            if (tbl.kasbonmststatus == "Post")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.kasbonmststatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.kasbonmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnkasbonmst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trnkasbonmst WHERE kasbonmstoid=" + tbl.kasbonmstoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trnkasbonmst");
                        tbl.kasbonno = GenerateExpenseNo2(tbl.cmpcode, tglmst, tbl.kasbontype, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trnkasbondtl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var minplus = 1;
                if (tbl.kasbongroup != "PEMBERIAN")
                {
                    minplus = -1;
                }
                tbl.typemin = minplus;
                tbl.kasbonamtidr = tbl.kasbonamt * cRate.GetRateMonthlyIDRValue;
                tbl.kasbonamtusd = 0;
                tbl.kasbonduedate = sDueDate;
                tbl.kasbonrefno = "";
                tbl.kasbonnote = (tbl.kasbonnote == null ? "" : ClassFunction.Tchar(tbl.kasbonnote));

                decimal TotalAmt = 0; decimal TotalAmtIDR = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.cmpcode = CompnyCode;
                            if (db.QL_trnkasbonmst.Find(tbl.cmpcode, tbl.kasbonmstoid) != null)
                                tbl.kasbonmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.kasbondate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnkasbonmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.kasbonmstoid + " WHERE tablename='QL_trnkasbonmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnkasbondtl.Where(a => a.kasbonmstoid == tbl.kasbonmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnkasbondtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnkasbondtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnkasbondtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.kasbondtloid = dtloid++;
                            tbldtl.kasbonmstoid = tbl.kasbonmstoid;
                            tbldtl.kasbondtlseq = i + 1;
                            tbldtl.personoid = dtDtl[i].personoid;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.kasbondtlamt = dtDtl[i].kasbondtlamt;
                            tbldtl.kasbondtlamtidr = dtDtl[i].kasbondtlamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.kasbondtlamtusd = 0;
                            tbldtl.kasbondtlduedate = sDueDate;
                            tbldtl.kasbondtlrefno = tbl.kasbonrefno;
                            tbldtl.kasbondtlstatus = tbl.kasbonmststatus;
                            tbldtl.kasbondtlnote = (dtDtl[i].kasbondtlnote == null ? "" : dtDtl[i].kasbondtlnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnkasbondtl.Add(tbldtl);
                            db.SaveChanges();

                            TotalAmt += dtDtl[i].kasbondtlamt;
                            TotalAmtIDR += dtDtl[i].kasbondtlamt * cRate.GetRateMonthlyIDRValue;
                        }
                        sSql = "UPDATE QL_id SET lastoid=" + dtloid + " WHERE tablename='QL_trnkasbondtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.kasbonmststatus == "Post")
                        {
                            var debet = "D"; var credit = "C";
                            if (tbl.kasbongroup == "PENGEMBALIAN")
                            {
                                debet = "C"; credit = "D";
                            }
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Kas Bon|No=" + tbl.kasbonno + "", tbl.kasbonmststatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, credit, TotalAmt, tbl.kasbonno, ClassFunction.Left("Form. Kas Bon | Note Hdr. " + tbl.kasbonnote + "", 200), tbl.kasbonmststatus, tbl.upduser, tbl.updtime, TotalAmtIDR, 0, "QL_trnkasbonmst " + tbl.kasbonmstoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;

                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, debet, dtDtl[i].kasbondtlamt, tbl.kasbonno, ClassFunction.Left("Form. KasBon | PIC. " + dtDtl[i].personname + "", 200), tbl.kasbonmststatus, tbl.upduser, tbl.updtime, dtDtl[i].kasbondtlamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbonmst " + tbl.kasbonmstoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: kasbonMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnkasbonmst tbl = db.QL_trnkasbonmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnkasbondtl.Where(a => a.kasbonmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnkasbondtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnkasbonmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, Boolean cbprintbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnkasbonmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKasBon.rpt"));
            sSql = "SELECT cb.kasbonmstoid cashbankoid, cb.kasbongroup, kasbonno AS [Cash / Bank No.], kasbondate AS [Expense Date], currcode AS [Currency], currsymbol AS [Curr Symbol], currdesc AS [Curr Desc], (CASE kasbontype WHEN 'BKK' THEN 'Kas' WHEN 'BBK' THEN 'Transfer' WHEN 'BGK' THEN 'Giro/Cheque' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE '' END) AS [Payment Type], ('(' + a1.acctgcode + ') ' + a1.acctgdesc) AS [Payment Account], kasbonrefno AS [Ref. No.], kasbonduedate AS [Due Date], kasbonamt AS [Grand Total], p.usname AS [PIC], kasbonmststatus AS [Status], kasbonnote AS [Header Note], a2.acctgcode AS [COA No.], a2.acctgdesc AS [COA Desc.], kasbondtlamt AS [Amount], kasbondtlnote AS [Note], divname AS [Business Unit], divaddress AS [BU Address], g4.gndesc AS [BU City], g5.gndesc AS [BU Province], g6.gndesc AS [BU Country], ISNULL(divphone, '') AS [BU Phone 1], ISNULL(divphone2, '') AS [BU Phone 2], divemail AS [BU Email], cb.upduser AS [Last Upd. User ID], ISNULL(pr.usname,UPPER(cb.upduser)) AS [Last Upd. User Name], cb.updtime AS [Last Upd. Datetime] FROM QL_trnkasbonmst cb INNER JOIN QL_trnkasbondtl gl ON gl.cmpcode=cb.cmpcode AND gl.kasbonmstoid=cb.kasbonmstoid INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstacctg a1 ON a1.acctgoid=cb.acctgoid INNER JOIN QL_m01US p ON p.usoid=gl.personoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=gl.acctgoid INNER JOIN QL_mstdivision di ON di.cmpcode=cb.cmpcode INNER JOIN QL_m05GN g4 ON g4.gnoid=divcityoid INNER JOIN QL_m05GN g5 ON g5.cmpcode=g4.cmpcode AND g5.gnoid=CONVERT(INT, g4.gnother1) INNER JOIN QL_m05GN g6 ON g6.cmpcode=g4.cmpcode AND g6.gnoid=CONVERT(INT, g4.gnother2) LEFT JOIN QL_m01US pr ON pr.usoid=cb.upduser WHERE cb.cmpcode='" + CompnyCode + "' AND cb.kasbonmstoid IN (" + id + ") ORDER BY cb.kasbonmstoid, gl.kasbondtloid";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintkasbon");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("UserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "KasBonPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}