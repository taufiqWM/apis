﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PBJasaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public PBJasaController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trnmritemmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var mrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.mritemwhoid = mrwhoid;
            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        private void FillAdditionalField(QL_trnmritemmst tblmst)
        {

            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname from QL_mstsupp where suppoid = '" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.pono = db.Database.SqlQuery<string>("SELECT poitemno from QL_trnpoitemmst where poitemmstoid = '" + tblmst.pomstoid + "'").FirstOrDefault();

        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "MRJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mritemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmritemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND mritemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT s.suppoid, s.suppname, s.suppaddr, s.suppcode, s.supppaymentoid FROM QL_mstsupp s WHERE activeflag='ACTIVE' AND s.suppoid IN (SELECT suppoid FROM QL_trnpoitemmst where poitemmststatus = 'Approved' AND poitemtype = 'Jasa') ORDER BY suppname";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(int suppoid)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT pm.poitemmstoid, pm.poitemno, ISNULL(pm.rabmstoid,0) rabmstoid, ISNULL(r.projectname,'') projectname, format(pm.poitemdate,'dd/MM/yyyy') poitemdate, pm.poitemmstnote, pm.curroid FROM QL_trnpoitemmst pm LEFT JOIN QL_trnrabmst r ON pm.rabmstoid = r.rabmstoid  WHERE pm.cmpcode = '{CompnyCode}' AND pm.suppoid={suppoid} AND poitemmststatus='Approved' AND pm.poitemtype = 'Jasa' ORDER BY poitemdate DESC";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class listpbdtl : QL_trnmritemdtl
        {
            public string jasacode { get; set; }
            public string jasadesc { get; set; }
            public string poitemno { get; set; }
            public string poitemdate { get; set; }
            public decimal poitemqty { get; set; }
            public decimal poitemprice { get; set; }
            public string mritemunit { get; set; }
            public int jasaacctgoid { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int pomstoid)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<listpbdtl>($"select 0 mritemdtlseq, pd.poitemdtloid podtloid, i.jasaoid itemoid, i.jasacode, i.jasadesc, pm.poitemno, convert(varchar(10),pm.poitemdate, 103) as poitemdate, (pd.poitemqty-isnull((select sum(mritemqty) from QL_trnmritemdtl md where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid),0)) as poitemqty, (((pd.poitemqty * pd.poitemprice) - pd.poitemdtldiscamt) / pd.poitemqty) poitemprice, 0.0 mritemqty, pd.poitemunitoid mritemunitoid, g.gndesc as mritemunit, 0.0 mritemvalue, '' mritemdtlnote, i.acctgoid jasaacctgoid from ql_trnpoitemdtl pd inner join ql_trnpoitemmst pm on pm.cmpcode=pd.cmpcode and pd.poitemmstoid=pm.poitemmstoid AND pm.poitemtype = 'Jasa' inner join QL_mstjasa i on i.cmpcode=pd.cmpcode and pd.itemoid=i.jasaoid INNER JOIN QL_m05GN g ON g.gnoid=pd.poitemunitoid WHERE i.activeflag='ACTIVE' and pd.poitemmstoid=" + pomstoid + " AND pd.poitemdtlstatus=''").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {

                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<listpbdtl>();

            try
            {
                sSql = $"select rd.mritemdtlseq, rd.mritemdtloid, rd.podtloid, rd.itemoid, i.jasacode, i.jasadesc, pm.poitemno, convert(varchar(10),pm.poitemdate, 103) as poitemdate, (pd.poitemqty-isnull((select sum(mritemqty) from QL_trnmritemdtl md where md.cmpcode=pd.cmpcode and md.podtloid=pd.poitemdtloid and md.mritemmstoid<>{id}),0)) poitemqty, (((pd.poitemqty * pd.poitemprice) - pd.poitemdtldiscamt) / pd.poitemqty) poitemprice, rd.mritemqty, rd.mritemunitoid, g.gndesc as mritemunit, rd.mritemdtlnote, rd.mritemvalue, i.acctgoid jasaacctgoid FROM QL_trnmritemdtl rd inner join ql_trnpoitemdtl pd on rd.podtloid=pd.poitemdtloid inner join ql_trnpoitemmst pm on pd.poitemmstoid=pm.poitemmstoid AND pm.poitemtype = 'Jasa' INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.mritemunitoid  WHERE rd.cmpcode='{CompnyCode}' AND rd.mritemmstoid={id} ORDER BY rd.mritemdtlseq";
                dtl = db.Database.SqlQuery<listpbdtl>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: poitem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No PB",["Supplier"] = "Supplier",["Project"] = "Project",["NomorPO"] = "No PO" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select pr.mritemmstoid, pr.mritemno, pr.mritemtype, pr.mritemdate, c.suppname, ISNULL(rm.projectname,'') projectname, ISNULL((SELECT c.custname FROM QL_mstcust c WHERE ISNULL(c.custoid,0)=ISNULL(rm.custoid,0)),'') custname, pom.poitemmstoid, pom.poitemno, pr.mritemmststatus FROM QL_trnmritemmst pr INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid=pr.pomstoid INNER JOIN QL_mstsupp c ON c.suppoid=pr.suppoid LEFT JOIN QL_trnrabmst rm ON ISNULL(pr.rabmstoid,0)=ISNULL(rm.rabmstoid,0) WHERE pr.mritemtype IN ('Jasa') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.mritemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.mritemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.mritemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.mritemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "NomorPO") sSql += " AND t.poitemno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.mritemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.mritemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.mritemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.mritemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmritemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnmritemmst();
                tblmst.cmpcode = CompnyCode;
                tblmst.mritemno = generateNo(ClassFunction.GetServerTime());
                tblmst.mritemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                ViewBag.mritemdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.tglsjsupp = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnmritemmst.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.mritemdate = tblmst.mritemdate.ToString("dd/MM/yyyy");
                ViewBag.tglsjsupp = tblmst.tglsjsupp.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tblmst.updtime.ToString("dd/MM/yyyy");
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmritemmst tbl, List<listpbdtl> dtDtl, string action, string tglmst, string tglsj)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.mritemtype))
                tbl.mritemtype = "Jasa";
            if (string.IsNullOrEmpty(tbl.nosjsupp))
                tbl.nosjsupp = "";
            if (string.IsNullOrEmpty(tbl.mritemmstnote))
                tbl.mritemmstnote = "";
            if (string.IsNullOrEmpty(tbl.mritemmstres2))
                tbl.mritemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.mritemmstres3))
                tbl.mritemmstres3 = "";
            if (string.IsNullOrEmpty(tbl.nosjsupp))
                tbl.nosjsupp = "";

            sSql = "SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.suppoid + "";
            string suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (dtDtl == null)
                msg += "- Please fill detail data!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].mritemdtlnote))
                            dtDtl[i].mritemdtlnote = "";

                        if (dtDtl[i].mritemqty <= 0)
                        {
                            msg += "- QTY PB item " + dtDtl[i].jasadesc + "  tidak boleh 0!<br>";
                        }
                        else
                        {
                            if (dtDtl[i].mritemqty > dtDtl[i].poitemqty)
                                msg += "- Qty PB item " + dtDtl[i].jasadesc + "  tidak boleh lebih dari Qty SO!<br>";
                        }
                        sSql = "SELECT (poitemqty - ISNULL((SELECT SUM(mritemqty) FROM QL_trnmritemdtl md INNER JOIN QL_trnmritemmst mm ON mm.cmpcode=md.cmpcode AND mm.mritemmstoid=md.mritemmstoid WHERE md.cmpcode=pod.cmpcode AND mritemmststatus<>'Rejected' AND md.podtloid=pod.poitemdtloid AND md.mritemmstoid<>" + tbl.mritemmstoid + "), 0.0)) AS poitemqty FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = pod.poitemmstoid WHERE pod.cmpcode='" + CompnyCode + "' AND pom.poitemtype IN ('JASA') AND poitemdtloid=" + dtDtl[i].podtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].poitemqty)
                            dtDtl[i].poitemqty = dQty;
                        if (dQty < dtDtl[i].poitemqty)
                            msg += "- Qty item " + dtDtl[i].jasadesc + " PB telah diupdate oleh user yg lain. Silahkan cek setiap Qty Harus kurang dari PO QTY!<br>";
                        if (dtDtl[i].mritemqty > dQty)
                            msg += "- Qty item " + dtDtl[i].jasadesc + " PO tidak boleh lebih dari Qty PB (" + dQty + ")<br>";

                        dtDtl[i].poitemqty = dQty;
                        dtDtl[i].mritemvalue = dtDtl[i].poitemprice;
                    }

                    if (tbl.mritemmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tbl.mritemno))
                            msg += "- Silahkan isi No. PB!<br>";
                        else if (db.QL_trnmritemmst.Where(w => w.mritemno == tbl.mritemno & w.mritemmstoid != tbl.mritemmstoid).Count() > 0)
                            msg += "- No. PB yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!<br>";
                    }
                }
            }

            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.mritemmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    msg += cRate.GetRateMonthlyLastError;
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.mritemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }
            if (tbl.mritemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                }
            }

            if (tbl.mritemmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED");
            }

            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmritemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmritemdtl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", CompnyCode));

                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                tbl.mritemmstres2 = "";
                tbl.mritemmstres3 = "";
                tbl.rate2oid = rate2oid;
                tbl.rabmstoid_awal = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.mritemmstoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnmritemmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnmritemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid IN (SELECT podtloid FROM QL_trnmritemdtl WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + tbl.mritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND poitemmstoid IN (SELECT mm.pomstoid FROM QL_trnmritemdtl md INNER JOIN QL_trnmritemmst mm ON mm.mritemmstoid = md.mritemmstoid WHERE mm.cmpcode='" + CompnyCode + "' AND mm.mritemmstoid=" + tbl.mritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnmritemdtl.Where(b => b.mritemmstoid == tbl.mritemmstoid && b.cmpcode == tbl.cmpcode);
                            db.QL_trnmritemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmritemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnmritemdtl)ClassFunction.MappingTable(new QL_trnmritemdtl(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.mritemdtloid = dtloid++;
                            tbldtl.mritemmstoid = tbl.mritemmstoid;
                            tbldtl.mritemdtlseq = i + 1;
                            tbldtl.mritemwhoid = 0;
                            tbldtl.mritembonusqty = 0;
                            tbldtl.mritemvalueidr = dtDtl[i].mritemvalue * cRate.GetRateMonthlyIDRValue;
                            tbldtl.mritemvalueusd = 1;
                            tbldtl.mritemdtlstatus = "";
                            tbldtl.mritembonusqty = 0;
                            tbldtl.mritemdtlres1 = "";
                            tbldtl.mritemdtlres2 = "";
                            tbldtl.mritemdtlres3 = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.refno = "";
                            tbldtl.serialnumber = "";

                            db.QL_trnmritemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].mritemqty >= dtDtl[i].poitemqty)
                            {
                                sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid=" + dtDtl[i].podtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET statusbelijasa='PB Jasa PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid='" + tbl.rabmstoid + "'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND poitemtype IN ('Jasa') AND poitemmstoid=" + tbl.pomstoid + " AND (SELECT COUNT(*) FROM QL_trnpoitemdtl pd WHERE pd.cmpcode='" + CompnyCode + "' AND pd.poitemdtlstatus='' AND pd.poitemmstoid=" + tbl.pomstoid + " AND pd.poitemdtloid<>" + dtDtl[i].podtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET statusbelijasa='PB Jasa' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid='" + tbl.rabmstoid + "' AND (SELECT COUNT(*) FROM QL_trnpoitemdtl pd WHERE pd.cmpcode='" + CompnyCode + "' AND pd.poitemdtlstatus='' AND pd.poitemmstoid=" + tbl.pomstoid + " AND pd.poitemdtloid<>" + dtDtl[i].podtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnmritemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.mritemmststatus == "Post")
                        {
                            decimal gltotal = dtDtl.Sum(x => (x.mritemqty * x.mritemvalue));
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.mritemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            // Insert QL_trngldtl
                            // D : Biaya Lain-Lain
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                var glamt = dtDtl[i].mritemqty * dtDtl[i].mritemvalue;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].jasaacctgoid, "D", glamt, tbl.mritemno, tbl.mritemno + " | " + suppname + " | " + dtDtl[i].mritemdtlnote, "Post", Session["UserID"].ToString(), servertime, glamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnmritemmst " + tbl.mritemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            // C : Persediaan Temp
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "C", gltotal, tbl.mritemno, tbl.mritemno + " | " + suppname + " | " + tbl.mritemmstnote, "Post", Session["UserID"].ToString(), servertime, gltotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnmritemmst " + tbl.mritemmstoid, null, null, null, 0));
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.mritemmstoid.ToString();
                        sReturnNo = "No. " + tbl.mritemno;
                        if (tbl.mritemmststatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmritemmst tblmst = db.QL_trnmritemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnpoitemdtl SET poitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND poitemdtloid IN (SELECT podtloid FROM QL_trnmritemdtl WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Approved' WHERE cmpcode='" + CompnyCode + "' AND poitemtype  IN ('Jasa') AND poitemmstoid IN (SELECT pomstoid FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.mritemmstoid = mrd.mritemmstoid WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mritemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmritemdtl.Where(a => a.mritemmstoid == id);
                        db.QL_trnmritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmritemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPBJasaItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.mritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PBReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}