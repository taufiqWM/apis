﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class SOAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public SOAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class soassetmst
        {
            public string cmpcode { get; set; }
            public int soassetmstoid { get; set; }
            public string soassetno { get; set; }
            public DateTime soassetdate { get; set; }
            public string custname { get; set; }
            public string soassetmststatus { get; set; }
            public string soassetmstnote { get; set; }
            public string divname { get; set; }
            public string sotype { get; set; }
        }

        public class soassetdtl
        {
            public int soassetdtlseq { get; set; }
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public int matrefoid { get; set; }
            public string matrefcode { get; set; }
            public string matreflongdesc { get; set; }
            public string matrefnote { get; set; }
            public decimal stockqty { get; set; }
            public decimal soassetqty { get; set; }
            public int soassetunitoid { get; set; }
            public string soassetunit { get; set; }
            public decimal soassetprice { get; set; }
            public decimal soassetdtlamt { get; set; }
            public decimal soassetdtlnetto { get; set; }
            public string soassetdtlnote { get; set; }
        }

        private void InitDDL(QL_trnsoassetmst tbl)
        {

            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY gndesc";
            var soassetpaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.soassetpaytypeoid);
            ViewBag.soassetpaytypeoid = soassetpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SOA/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(soassetno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoassetmst WHERE cmpcode='" + CompnyCode + "' AND soassetno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust c WHERE c.cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND custoid>0 ORDER BY custname DESC";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int curroid)
        {
            List<soassetdtl> tbl = new List<soassetdtl>();
            sSql = "SELECT 0 soassetdtlseq, a.assetno AS assetno, a.refoid AS matrefoid, i.itemcode AS matrefcode, i.itemdesc AS matreflongdesc, 0.0 AS stockqty, 1.0 AS soassetqty, i.itemunitoid AS soassetunitoid, g.gndesc AS soassetunit, (a.assetvalue - a.assetaccumdep) AS soassetprice, '' AS soassetdtlnote, a.assetmstoid, a.assetno FROM QL_assetmst a INNER JOIN QL_mstitem i ON i.itemoid = a.refoid INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid WHERE a.cmpcode = '" + cmp +"' AND a.curroid="+ curroid +" AND a.assetmstflag = '' AND a.assetmststatus IN ('Post', 'Closed') AND ISNULL(a.assetmstres1, '')<> 'Assets In Progress' ORDER BY a.assetno";

            tbl = db.Database.SqlQuery<soassetdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<soassetdtl> dtDtl)
        {
            Session["QL_trnsoassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsoassetdtl"] == null)
            {
                Session["QL_trnsoassetdtl"] = new List<soassetdtl>();
            }

            List<soassetdtl> dataDtl = (List<soassetdtl>)Session["QL_trnsoassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnsoassetmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
        }

        // GET: soassetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            
            sSql = "SELECT som.cmpcode, som.soassetmstoid, som.soassetno, som.soassetdate, c.custname, som.soassetmststatus, som.soassetmstnote, som.soassettype sotype FROM QL_TRNsoassetMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='"+ CompnyCode +"'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND soassetdate>=CAST('" + modfil.filterperiodfrom + " 00:00:00' AS DATETIME) AND soassetdate<=CAST('" + modfil.filterperiodto + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND soassetmststatus IN ('In Process', 'Revised')";
            }

            List<soassetmst> dt = db.Database.SqlQuery<soassetmst>(sSql).ToList();
            return View(dt);
        }

        // GET: soassetMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoassetmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnsoassetmst();
                tbl.cmpcode = CompnyCode;
                tbl.soassetmstoid = ClassFunction.GenerateID("QL_trnsoassetmst");
                tbl.soassetdate = ClassFunction.GetServerTime();
                tbl.socustrefdate = ClassFunction.GetServerTime();
                tbl.soassetetd= ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.upduser = Session["UserID"].ToString();
                tbl.updtime = ClassFunction.GetServerTime();
                tbl.soassetmststatus = "In Process";
                tbl.isexcludetax = true;
                tbl.soassettaxtype = "TAX";
                tbl.soassettaxamt = 10;
                tbl.soassettotaldiscdtl = 0;

                Session["QL_trnsoassetdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnsoassetmst.Find(CompnyCode, id);

                sSql = "SELECT sod.soassetdtlseq, a.assetno AS assetno, sod.assetmstoid, i.itemoid matrefoid, i.itemcode AS matrefcode, i.itemdesc AS matreflongdesc, sod.soassetqty, sod.soassetunitoid, gndesc AS soassetunit, sod.soassetprice, sod.soassetdtlamt, sod.soassetdtlnetto, sod.soassetdtlnote FROM QL_trnsoassetdtl sod INNER JOIN QL_assetmst a ON a.assetmstoid=sod.assetmstoid INNER JOIN QL_mstitem i ON i.itemoid=a.refoid INNER JOIN QL_m05GN g ON gnoid=soassetunitoid WHERE sod.soassetmstoid=" + id + " ORDER BY sod.soassetdtlseq";
                Session["QL_trnsoassetdtl"] = db.Database.SqlQuery<soassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: soassetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsoassetmst tbl, string action, string tglmst, string tgletd, string tglcustrefdate)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.soassetno == null)
                tbl.soassetno = "";

            try
            {
                tbl.soassetdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soassetdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.soassetetd = DateTime.Parse(ClassFunction.toDate(tgletd));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soassetetd", "Format Tanggal ETD Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.socustrefdate = DateTime.Parse(ClassFunction.toDate(tglcustrefdate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("socustrefdate", "Format Tanggal Customer PO Tidak Valid!!" + ex.ToString());
            }
            if (ModelState.IsValid)
            {
                if (tbl.soassetdate > tbl.soassetetd)
                {
                    ModelState.AddModelError("", "ETD must be more than SO DATE!");
                }
            }

            if (tbl.soassetmststatus == "Post")
            {
                tbl.soassetno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.soassetno = "";
            }

            List<soassetdtl> dtDtl = (List<soassetdtl>)Session["QL_trnsoassetdtl"];

            if (dtDtl == null)
            {
                ModelState.AddModelError("", "Please fill detail data!");
            }
            else
            {
                if (dtDtl.Count <= 0)
                {
                    ModelState.AddModelError("", "Please fill detail data!");
                }
                else
                {
                    if (dtDtl != null)
                    {
                        if (dtDtl.Count > 0)
                        {
                            for (var i = 0; i < dtDtl.Count; i++)
                            {
                                if (dtDtl[i].soassetprice <= 0)
                                {
                                    ModelState.AddModelError("", "SO PRICE for code " + dtDtl[i].matrefcode + " must more than 0");
                                }
                                if (dtDtl[i].soassetqty <= 0)
                                {
                                    ModelState.AddModelError("", "SO QTY for code " + dtDtl[i].matrefcode + " must more than 0");
                                }
                            }
                        }
                    }
                }
            }

            var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();
            if (tbl.soassetmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.soassetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.soassetmststatus = "In Process";
            }
            if (tbl.soassetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.soassetmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                tbl.soassettype = "LOCAL";
                tbl.soassettolerance = 0;
                tbl.rateoid = 0;
                tbl.soassettotaldisc = 0;
                tbl.soassettotaldiscidr = 0;
                tbl.soassettotaldiscusd = 0;
                tbl.soassettotaldiscdtl = 0;
                tbl.soassettotaldiscdtlidr = 0;
                tbl.soassettotaldiscdtlusd = 0;
                tbl.soassetmstdisctype = "P";
                tbl.soassetmstdiscvalue = 0;
                tbl.soassetmstdiscamt = 0;
                tbl.soassetmstdiscamtidr = 0;
                tbl.soassetmstdiscamtusd = 0;
                tbl.soassetdeliverycost = 0;
                tbl.soassetdeliverycostidr = 0;
                tbl.soassetdeliverycostusd = 0;
                tbl.soassetothercost = 0;
                tbl.soassetothercostidr = 0;
                tbl.soassetothercostusd = 0;
                tbl.rate2oid = cRate.GetRateMonthlyOid;
                tbl.soassetratetoidr = 0;
                tbl.soassetratetousd = 0;
                tbl.soassetrate2toidr = cRate.GetRateMonthlyIDRValue;
                tbl.soassetrate2tousd = 0;
                tbl.soassettotalamtidr = tbl.soassettotalamt * cRate.GetRateMonthlyIDRValue;
                tbl.soassettotalamtusd = 0;
                tbl.soassettotalnettoidr = tbl.soassettotalnetto * cRate.GetRateMonthlyIDRValue;
                tbl.soassettotalnettousd = 0;
                tbl.soassetvatidr = tbl.soassetvat * cRate.GetRateMonthlyIDRValue;
                tbl.soassetvatusd = 0;
                tbl.soassetgrandtotalamtidr = tbl.soassetgrandtotalamt * cRate.GetRateMonthlyIDRValue;
                tbl.soassetgrandtotalamtusd = 0;
                tbl.soassetinvoiceval = "";
                tbl.soassetpaymethod = "";
                tbl.soassetportship = "";
                tbl.soassetportdischarge = "";
                tbl.soassetbank = "";
                tbl.accountoid = 0;

                var mstoid = ClassFunction.GenerateID("QL_trnsoassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsoassetdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.soassetmstoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnsoassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.soassetmstoid + " WHERE tablename='QL_trnsoassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_assetmst SET assetmstflag='' WHERE cmpcode='" + tbl.cmpcode + "' AND assetmstoid IN (SELECT assetmstoid FROM QL_trnsoassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND soassetmstoid=" + tbl.soassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnsoassetdtl.Where(a => a.soassetmstoid == tbl.soassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnsoassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnsoassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsoassetdtl();

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.soassetdtloid = dtloid++;
                            tbldtl.soassetmstoid = tbl.soassetmstoid;
                            tbldtl.soassetdtlseq = i + 1;
                            tbldtl.assetmstoid = dtDtl[i].assetmstoid;
                            tbldtl.soassetqty = dtDtl[i].soassetqty;
                            tbldtl.soassetunitoid = dtDtl[i].soassetunitoid;
                            tbldtl.soassetprice = dtDtl[i].soassetprice;
                            tbldtl.soassetprice_intax = dtDtl[i].soassetprice;
                            tbldtl.soassetdtlamt = dtDtl[i].soassetdtlamt;
                            tbldtl.soassetdtlamt_intax = dtDtl[i].soassetdtlamt;
                            tbldtl.soassetdtldisctype = "P";
                            tbldtl.soassetdtldiscvalue = 0;
                            tbldtl.soassetdtldiscvalue_intax = 0;
                            tbldtl.soassetdtldiscamt = 0;
                            tbldtl.soassetdtldiscamt_intax = 0;
                            tbldtl.soassetdtlnetto = dtDtl[i].soassetdtlnetto;
                            tbldtl.soassetdtlnetto_intax = 0;
                            tbldtl.soassetdtlstatus = "";
                            tbldtl.soassetdtlnote = dtDtl[i].soassetdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            db.QL_trnsoassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_assetmst SET assetmstflag='Sold' WHERE cmpcode='" + tbl.cmpcode + "' AND assetmstoid=" + dtDtl[i].assetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnsoassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: soassetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoassetmst tbl = db.QL_trnsoassetmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_assetmst SET assetmstflag='' WHERE cmpcode='" + tbl.cmpcode + "' AND assetmstoid IN (SELECT assetmstoid FROM QL_trnsoassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND soassetmstoid=" + tbl.soassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnsoassetdtl.Where(a => a.soassetmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnsoassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnsoassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOAPrintOut.rpt"));

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE som.cmpcode='"+ CompnyCode +"' AND som.soassetmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SOFixedAssetPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}