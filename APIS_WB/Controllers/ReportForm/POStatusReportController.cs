﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class POStatusReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public POStatusReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetSuppData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, s.suppcode [Kode], s.suppname [Nama], s.suppaddr [Alamat] FROM QL_mstsupp s WHERE s.suppoid IN (SELECT pom.suppoid FROM QL_trnpoitemmst pom WHERE pom.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poitemdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poitemdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += " UNION ALL  SELECT pom.suppoid FROM QL_trnpoassetmst pom WHERE pom.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poassetdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poassetdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND pom.poassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY s.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetPkmData(string[] DDLStatus, string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, som.rabmstoid [No. Draft], som.projectname [Nama Project], CONVERT(VARCHAR(10), som.rabdate, 101) AS [Tgl. RAB],  CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], som.rabmststatus [Status], som.rabmstnote [Note] FROM QL_trnrabmst rm LEFT JOIN QL_trnrabmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.rabmstoid IN (SELECT pom.rabmstoid from QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON pom.suppoid = s.suppoid WHERE pom.poitemtype<>'Jasa' ";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poitemdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poitemdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ) ORDER BY rm.rabdate DESC, som.rabmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPKM");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetPOData(string[] DDLStatus,string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomorRAB)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, pom.poitemmstoid [No. Draft], pom.poitemno [No. Purchase Order], CONVERT(varchar(10),pom.poitemdate,103) [Tgl. PO], s.suppname [Supplier], pom.poitemmststatus [Status], pom.poitemmstnote [Note] FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poitemdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poitemdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " UNION ALL  SELECT 0 seq, pom.poassetmstoid [No. Draft], pom.poassetno [No. Purchase Order], CONVERT(varchar(10),pom.poassetdate,103) [Tgl. PO], s.suppname [Supplier], pom.poassetmststatus [Status], pom.poassetmstnote [Note] FROM QL_trnpoassetmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poassetdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poassetdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                //sSql += " ORDER BY pom.poitemdate DESC, pom.poitemmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, som.soitemmstoid [No. Draft], som.soitemno [Nomer SO], CONVERT(VARCHAR(10), som.soitemdate, 101) AS [Tgl. SO], rm.projectname [Nama Project], CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], som.soitemmststatus [Status], som.soitemmstnote [Note] FROM QL_trnrabmst rm LEFT JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.rabmstoid IN (SELECT pom.rabmstoid from QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON pom.suppoid = s.suppoid WHERE pom.poitemtype<>'Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poitemdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poitemdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ) ORDER BY rm.rabdate DESC, som.soitemmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextSupp, string DDLNomorPO, string TextNomorPO, string TextNomorRAB, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnpoitemdtl pod ON pod.itemoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=pod.poitemunitoid INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid=pod.poitemmstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=pom.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND pom.poitemdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND pom.poitemdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorPO != "")
                {
                    string[] arr = TextNomorPO.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND pom.poitemno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLNomorPO, string TextNomorRAB, string TextPKM, string DDLGroup, string DDLPOType)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptPOStatusSumXls.rpt";
                }
                else
                {
                    rptfile = "rptPOStatusSumPdf.rpt";
                }
                rptname = "POStatus_SUMMARY";
            }
            else
            {
                //if (DDLGroup == "Gudang")
                //{
                //    rptfile = "rptPOStatusDtlPdfGudang.rpt";
                //}                    
                //else if (DDLGroup == "Barang")
                //{
                //    rptfile = "rptPOStatusDtlPdfBarang.rpt";
                //}
                //else if (DDLGroup == "Departement")
                //{
                //    rptfile = "rptPOStatusDtlPdfDept.rpt";
                //}
                //else
                //{
                //    rptfile = "rptPOStatusDtlPdf.rpt";
                //} 
                if (reporttype == "XLS")
                {
                    rptfile = "rptPOStatusDtlXls.rpt";
                }
                else
                {
                    rptfile = "rptPOStatusDtlPdf.rpt";
                }
                rptfile = "rptPOStatusDtlPdf.rpt";
                rptname = "POStatus_DETAIL";
            }

            var Dtl = ""; var Join = "";
            var Dtl2 = ""; var Join2 = "";
            var Dtl3 = ""; var Join3 = "";
            if (DDLType == "Detail")
            {
                //Detail Receive
                Dtl = ", ISNULL(mrd.mritemqty, 0) AS registerqty, ISNULL((CASE mrm.mritemno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), mrm.mritemmstoid) ELSE mrm.mritemno END),'') AS registerno, ISNULL (mrm.mritemdate, CAST('01/01/1900' AS datetime)) AS registerdate, ISNULL(mrm.mritemno, '') AS registerdocrefno, ISNULL (mrm.mritemdate , CAST('01/01/1900' AS datetime)) AS registerdocrefdate, ISNULL(mrm.createuser, '') AS Regcreateuser, ISNULL (mrm.createtime, CAST('01/01/1900' AS datetime)) AS RegCreateDate";
                //Detail Purchase Return
                Dtl += ", (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnpretitemmst pretm INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode = pretm.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid AND pretd.itemoid = mrd.itemoid AND pretd.mritemdtloid IN(SELECT mritemdtloid FROM QL_trnmritemdtl mr WHERE mr.mritemdtloid = mrd.mritemdtloid) WHERE pretm.cmpcode = mrm.cmpcode AND pretm.mritemmstoid = mrm.mritemmstoid AND mrm.mritemtype NOT IN ('Jasa') AND mrm.mritemmststatus IN('Post', 'Closed') AND ISNULL(pretm.pretitemtype,'')='' AND pretm.pretitemmststatus IN('Post')) AS preturnqty";
                Join = " LEFT JOIN QL_trnmritemdtl mrd ON mrd.cmpcode = pod.cmpcode AND mrd.itemoid = pod.itemoid AND mrd.podtloid = pod.poitemdtloid AND mrd.mritemmstoid IN (SELECT x.mritemmstoid FROM QL_trnmritemmst x WHERE x.mritemmststatus IN('Post', 'Closed') AND x.mritemtype NOT IN('Jasa')) LEFT JOIN QL_trnmritemmst mrm ON mrm.cmpcode = pom.cmpcode AND mrm.mritemmstoid = mrd.mritemmstoid AND mrm.mritemtype NOT IN('Jasa') ";

                //Detail Receive Jasa
                Dtl2 = ", ISNULL(mrd.mritemqty, 0) AS registerqty, ISNULL((CASE mrm.mritemno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), mrm.mritemmstoid) ELSE mrm.mritemno END),'') AS registerno, ISNULL (mrm.mritemdate, CAST('01/01/1900' AS datetime)) AS registerdate, ISNULL(mrm.mritemno, '') AS registerdocrefno, ISNULL (mrm.mritemdate , CAST('01/01/1900' AS datetime)) AS registerdocrefdate, ISNULL(mrm.createuser, '') AS Regcreateuser, ISNULL (mrm.createtime, CAST('01/01/1900' AS datetime)) AS RegCreateDate";
                //Detail Purchase Return Jasa
                Dtl2 += ", (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnpretitemmst pretm INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode = pretm.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid AND pretd.itemoid = mrd.itemoid AND pretd.mritemdtloid IN(SELECT mritemdtloid FROM QL_trnmritemdtl mr WHERE mr.mritemdtloid = mrd.mritemdtloid) WHERE pretm.cmpcode = mrm.cmpcode AND pretm.mritemmstoid = mrm.mritemmstoid AND mrm.mritemtype IN ('Jasa') AND mrm.mritemmststatus IN('Post', 'Closed') AND ISNULL(pretm.pretitemtype,'')='Jasa' AND pretm.pretitemmststatus IN('Post')) AS preturnqty";
                Join2 = " LEFT JOIN QL_trnmritemdtl mrd ON mrd.cmpcode = pod.cmpcode AND mrd.itemoid = pod.itemoid AND mrd.podtloid = pod.poitemdtloid AND mrd.mritemmstoid IN (SELECT x.mritemmstoid FROM QL_trnmritemmst x WHERE x.mritemmststatus IN('Post', 'Closed') AND x.mritemtype IN('Jasa')) LEFT JOIN QL_trnmritemmst mrm ON mrm.cmpcode = pom.cmpcode AND mrm.mritemmstoid = mrd.mritemmstoid AND mrm.mritemtype IN('Jasa') ";

                //Detail Receive Asset
                Dtl3 = ", ISNULL(mrd.mrassetqty, 0) AS registerqty, ISNULL((CASE mrm.mrassetno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), mrm.mrassetmstoid) ELSE mrm.mrassetno END),'') AS registerno, ISNULL (mrm.mrassetdate, CAST('01/01/1900' AS datetime)) AS registerdate, ISNULL(mrm.mrassetno, '') AS registerdocrefno, ISNULL (mrm.mrassetdate , CAST('01/01/1900' AS datetime)) AS registerdocrefdate, ISNULL(mrm.createuser, '') AS Regcreateuser, ISNULL (mrm.createtime, CAST('01/01/1900' AS datetime)) AS RegCreateDate";
                //Detail Purchase Return Asset
                Dtl3 += ", (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnpretitemmst pretm INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode = pretm.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid AND pretd.itemoid = mrd.mrassetrefoid AND pretd.mritemdtloid IN(SELECT mrassetdtloid FROM QL_trnmrassetdtl mr WHERE mr.mrassetdtloid = mrd.mrassetdtloid) WHERE pretm.cmpcode = mrm.cmpcode AND pretm.mritemmstoid = mrm.mrassetmstoid AND mrm.mrassetmststatus IN('Post', 'Closed') AND ISNULL(pretm.pretitemtype,'')='Asset' AND pretm.pretitemmststatus IN('Post')) AS preturnqty";
                Join3 = " LEFT JOIN QL_trnmrassetdtl mrd ON mrd.cmpcode = pod.cmpcode AND mrd.mrassetrefoid = pod.poassetrefoid AND mrd.poassetdtloid = pod.poassetdtloid AND mrd.mrassetmstoid IN (SELECT x.mrassetmstoid FROM QL_trnmrassetmst x WHERE x.mrassetmststatus IN('Post', 'Closed')) LEFT JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = pom.cmpcode AND mrm.mrassetmstoid = mrd.mrassetmstoid ";

            }
            else
            {
                //Detail Receive
                Dtl = ", (SELECT ISNULL(SUM(mrd.mritemqty), 0) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid AND mrm.pomstoid=pom.poitemmstoid AND mrd.podtloid=pod.poitemdtloid WHERE mrd.cmpcode=pod.cmpcode AND mrd.itemoid=pod.itemoid AND mrd.podtloid=pod.poitemdtloid AND mrm.mritemtype NOT IN ('Jasa')) AS registerqty";
                //Detail Purchase Return
                Dtl += ", (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid AND mrm.pomstoid=pom.poitemmstoid AND mrd.podtloid=pod.poitemdtloid INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=mrm.cmpcode AND pretm.mritemmstoid=mrm.mritemmstoid INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretitemmstoid=pretd.pretitemmstoid AND pretd.itemoid=mrd.itemoid AND pretd.mritemdtloid IN (SELECT mritemdtloid FROM QL_trnmritemdtl mr WHERE mr.mritemdtloid=mrd.mritemdtloid) WHERE mrm.mritemtype NOT IN ('Jasa') AND mrm.mritemmststatus IN ('Post','Closed')  AND ISNULL(pretm.pretitemtype,'')='' AND pretm.pretitemmststatus IN ('Post')) AS preturnqty";

                //Detail Receive Jasa
                Dtl2 = ", (SELECT ISNULL(SUM(mrd.mritemqty), 0) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid AND mrm.pomstoid=pom.poitemmstoid AND mrd.podtloid=pod.poitemdtloid WHERE mrd.cmpcode=pod.cmpcode AND mrd.itemoid=pod.itemoid AND mrd.podtloid=pod.poitemdtloid AND mrm.mritemtype IN ('Jasa')) AS registerqty";
                //Detail Purchase Return Jasa
                Dtl2 += ", (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid AND mrm.pomstoid=pom.poitemmstoid AND mrd.podtloid=pod.poitemdtloid INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=mrm.cmpcode AND pretm.mritemmstoid=mrm.mritemmstoid INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretitemmstoid=pretd.pretitemmstoid AND pretd.itemoid=mrd.itemoid AND pretd.mritemdtloid IN (SELECT mritemdtloid FROM QL_trnmritemdtl mr WHERE mr.mritemdtloid=mrd.mritemdtloid) WHERE mrm.mritemtype IN ('Jasa') AND mrm.mritemmststatus IN ('Post','Closed')  AND ISNULL(pretm.pretitemtype,'')='Jasa' AND pretm.pretitemmststatus IN ('Post')) AS preturnqty";

                //Detail Receive Asset
                Dtl3 = ", (SELECT ISNULL(SUM(mrd.mrassetqty), 0) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mrassetmstoid=mrd.mrassetmstoid AND mrm.poassetmstoid=pom.poassetmstoid AND mrd.poassetdtloid=pod.poassetdtloid WHERE mrd.cmpcode=pod.cmpcode AND mrd.mrassetrefoid=pod.poassetrefoid AND mrd.poassetdtloid=pod.poassetdtloid) AS registerqty";
                //Detail Purchase Return Asset
                Dtl3 += ", (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mrassetmstoid=mrd.mrassetmstoid AND mrm.poassetmstoid=pom.poassetmstoid AND mrd.poassetdtloid=pod.poassetdtloid INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=mrm.cmpcode AND pretm.mritemmstoid=mrm.mrassetmstoid INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretitemmstoid=pretd.pretitemmstoid AND pretd.itemoid=mrd.mrassetrefoid AND pretd.mritemdtloid IN (SELECT mrassetdtloid FROM QL_trnmrassetdtl mr WHERE mr.mrassetdtloid=mrd.mrassetdtloid) WHERE mrm.mrassetmststatus IN ('Post','Closed') AND ISNULL(pretm.pretitemtype,'')='Asset' AND pretm.pretitemmststatus IN ('Post')) AS preturnqty";
            }

            sSql = "SELECT * FROM ( ";
            sSql += " SELECT 'poitem' tipe, '' potype, pom.poitemmstoid AS pomstoid, pod.poitemdtloid AS podtloid, ('Draft No. ' + CONVERT(VARCHAR(10), pom.poitemmstoid)) AS draftno, pom.poitemno AS pono, pom.poitemdate AS podate, pom.updtime AS poupdtime, pom.poitemmststatus AS pomststatus, m.itemcode AS matcode, m.itemdesc AS matlongdesc, ISNULL(pod.poitemqty, 0) AS poqty, pod.closeqty AS POClosingQty, ISNULL(pod.poitemqty, 0) AS POQtyAwal, pom.cmpcode, pom.suppoid, s.suppname, gu.gndesc Gudang " + Dtl + ", 0.0 AS balanceqty, 0.0 AS sumregqty, 0.0 AS sumretqty, 0.0 AS sumpretqty, g.gndesc AS pounit, '' AS divname, pod.poitemdtlnote AS podtlnote, pod.poitemdtleta [ETA], pom.poitemmstnote AS pomstnote, pom.approvaluser, pom.approvaldatetime, pom.createuser, ISNULL(rm.rabno, '') AS[PR No.], ISNULL(rm.rabdate, '') AS [PR Date], s.suppcode AS [Code Supplier], '' AS [PR Dept], '' AS[PR Exp Date], ISNULL(rm.rabmstnote, '') AS [PR Header Note], GETDATE() AS [PR Date Req], pom.closereason, pom.closeuser, pom.closetime, som.soitemno, rm.projectname, rm.rabtype, rm.deptoid, dp.groupdesc deptname FROM QL_trnpoitemmst pom INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = pom.cmpcode AND pod.poitemmstoid = pom.poitemmstoid INNER JOIN QL_m05GN gu ON gu.gnoid = pod.poitemdtllocoid INNER JOIN QL_trnrabmst rm ON pom.cmpcode = rm.cmpcode AND rm.rabmstoid = pom.rabmstoid INNER JOIN QL_mstdeptgroup dp ON dp.groupoid=rm.deptoid LEFT JOIN QL_trnrabdtl2 rd2 ON pod.cmpcode = rd2.cmpcode AND rd2.rabdtl2oid = pod.rabdtl2oid " + Join + " INNER JOIN QL_mstitem m ON m.itemoid = pod.itemoid INNER JOIN QL_m05GN g ON g.gnoid = pod.poitemunitoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=pom.somstoid AND som.rabmstoid=rm.rabmstoid Where pom.cmpcode = '" + CompnyCode + "' AND pom.poitemtype NOT IN('Jasa')";
            sSql += " UNION ALL  SELECT 'pojasa' tipe, '' potype, pom.poitemmstoid AS pomstoid, pod.poitemdtloid AS podtloid, ('Draft No. ' + CONVERT(VARCHAR(10), pom.poitemmstoid)) AS draftno, pom.poitemno AS pono, pom.poitemdate AS podate, pom.updtime AS poupdtime, pom.poitemmststatus AS pomststatus, m.jasacode AS matcode, m.jasadesc AS matlongdesc, ISNULL(pod.poitemqty, 0) AS poqty, pod.closeqty AS POClosingQty, ISNULL(pod.poitemqty, 0) AS POQtyAwal, pom.cmpcode, pom.suppoid, s.suppname, gu.gndesc Gudang " + Dtl2 + ", 0.0 AS balanceqty, 0.0 AS sumregqty, 0.0 AS sumretqty, 0.0 AS sumpretqty, g.gndesc AS pounit, '' AS divname, pod.poitemdtlnote AS podtlnote, pod.poitemdtleta [ETA], pom.poitemmstnote AS pomstnote, pom.approvaluser, pom.approvaldatetime, pom.createuser, ISNULL(rm.rabno, '') AS[PR No.], ISNULL(rm.rabdate, '') AS [PR Date], s.suppcode AS [Code Supplier], '' AS [PR Dept], '' AS[PR Exp Date], ISNULL(rm.rabmstnote, '') AS [PR Header Note], GETDATE() AS [PR Date Req], pom.closereason, pom.closeuser, pom.closetime, ISNULL(som.soitemno,'') soitemno, ISNULL(rm.projectname,'') projectname, ISNULL(rm.rabtype,'') rabtype, ISNULL(rm.deptoid,0) deptoid, ISNULL(dp.groupdesc,'') deptname FROM QL_trnpoitemmst pom INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = pom.cmpcode AND pod.poitemmstoid = pom.poitemmstoid INNER JOIN QL_m05GN gu ON gu.gnoid = pod.poitemdtllocoid LEFT JOIN QL_trnrabmst rm ON pom.cmpcode = rm.cmpcode AND rm.rabmstoid = pom.rabmstoid LEFT JOIN QL_mstdeptgroup dp ON dp.groupoid=rm.deptoid LEFT JOIN QL_trnrabdtl2 rd2 ON pod.cmpcode = rd2.cmpcode AND rd2.rabdtl2oid = pod.rabdtl2oid " + Join2 + " INNER JOIN QL_mstjasa m ON m.jasaoid = pod.itemoid INNER JOIN QL_m05GN g ON g.gnoid = pod.poitemunitoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid LEFT JOIN QL_trnsoitemmst som ON som.soitemmstoid=pom.somstoid AND som.rabmstoid=rm.rabmstoid Where pom.cmpcode = '" + CompnyCode + "' AND pom.poitemtype IN('Jasa')";
            sSql += " UNION ALL  SELECT 'poasset' tipe, pom.poassettype potype, pom.poassetmstoid AS pomstoid, pod.poassetdtloid AS podtloid, ('Draft No. ' + CONVERT(VARCHAR(10), pom.poassetmstoid)) AS draftno, pom.poassetno AS pono, pom.poassetdate AS podate, pom.updtime AS poupdtime, pom.poassetmststatus AS pomststatus, m.itemcode AS matcode, m.itemdesc AS matlongdesc, ISNULL(pod.poassetqty, 0) AS poqty, pod.closeqty AS POClosingQty, ISNULL(pod.poassetqty, 0) AS POQtyAwal, pom.cmpcode, pom.suppoid, s.suppname, gu.gndesc Gudang " + Dtl3 + ", 0.0 AS balanceqty, 0.0 AS sumregqty, 0.0 AS sumretqty, 0.0 AS sumpretqty, g.gndesc AS pounit, '' AS divname, pod.poassetdtlnote AS podtlnote, pod.poassetdtleta [ETA], pom.poassetmstnote AS pomstnote, pom.approvaluser, pom.approvaldatetime, pom.createuser, '' AS[PR No.], '' AS [PR Date], s.suppcode AS [Code Supplier], '' AS [PR Dept], '' AS[PR Exp Date], '' AS [PR Header Note], GETDATE() AS [PR Date Req], pom.closereason, pom.closeuser, pom.closetime, '' soitemno, '' projectname, '' rabtype, 0 deptoid, '' deptname FROM QL_trnpoassetmst pom INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = pom.cmpcode AND pod.poassetmstoid = pom.poassetmstoid INNER JOIN QL_m05GN gu ON gu.gnoid = pod.poassetdtllocoid " + Join3 + " INNER JOIN QL_mstitem m ON m.itemoid = pod.poassetrefoid INNER JOIN QL_m05GN g ON g.gnoid = pod.poassetunitoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid Where pom.cmpcode = '" + CompnyCode + "' ";
            sSql += ") AS po WHERE 1=1 ";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND po.pomststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextSupp != "")
            {
                string[] arr = TextSupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND po.[Code Supplier] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextPKM != "")
            {
                string[] arm = TextPKM.Split(';'); string filterdata = "";
                for (int i = 0; i < arm.Count(); i++)
                {
                    filterdata += "'" + arm[i] + "',";
                }
                sSql += " AND po.projectname IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }           

            if (TextNomorPO != "")
            {
                string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLNomorPO + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorRAB != "")
            {
                string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND po.soitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLPOType != "ALL")
            {
                sSql += " AND po.potype = '"+ DDLPOType +"' ";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND po.matcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            for (int C1 = 0; C1 < dtRpt.Rows.Count; C1++)
            {
                dtRpt.Rows[C1]["balanceqty"] = (decimal)dtRpt.Rows[C1]["POQtyAwal"] - (decimal)dtRpt.Rows[C1]["POClosingQty"] - (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"] + " AND tipe='"+ dtRpt.Rows[C1]["tipe"] + "'") + (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"] + " AND tipe='" + dtRpt.Rows[C1]["tipe"] + "'");
                dtRpt.Rows[C1]["sumregqty"] = (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"] + " AND tipe='" + dtRpt.Rows[C1]["tipe"] + "'");
                dtRpt.Rows[C1]["sumpretqty"] = (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"] + " AND tipe='" + dtRpt.Rows[C1]["tipe"] + "'");
            }

            //For Status PO Closed (In Process Register)
            decimal SumBalance = 0;
            for (int C2 = 0; C2 < dtRpt.Rows.Count; C2++)
            {
                SumBalance = (decimal)dtRpt.Compute("SUM(balanceqty)", "pomstoid=" + dtRpt.Rows[C2]["pomstoid"]);
                if (dtRpt.Rows[C2]["pomststatus"].ToString() == "Closed" && SumBalance > 0) 
                {
                    dtRpt.Rows[C2]["pomststatus"] = "Approved";
                }
            }
            
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else{
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }  
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}