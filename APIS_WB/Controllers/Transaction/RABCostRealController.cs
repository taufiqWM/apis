﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class RABCostRealController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public RABCostRealController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public int cashbankoid { get; set; }
            public string cashbankno { get; set; }
            //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime cashbankdate { get; set; }
            public string cashbanktype { get; set; }
            public string acctgdesc { get; set; }
            public string personname { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            public string reason { get; set; }
            public string suppname { get; set; }
            public string project { get; set; }
            public string CBType { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamt { get; set; }
        }

        public class cashbankgl
        {
            public int cashbankglseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctgdesc2 { get; set; }
            //[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankglamt { get; set; }
            public string cashbankglnote { get; set; }
            public decimal cashbankglamtidr { get; set; }
            public decimal cashbankglamtusd { get; set; }
            public int curroid { get; set; }
        }

        public class suppbankaccount
        {
            public int accountoid { get; set; }
            public string accountname { get; set; }
        }

        public class trndpap
        {
            public int dpapoid { get; set; }
            public string dpapno { get; set; }
            public DateTime dpapdate { get; set; }
            public int dpapacctgoid { get; set; }
            public string acctgdesc { get; set; }
            public string dpapnote { get; set; }
            //[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dpapamt { get; set; }
            public DateTime dpdateforcheck { get; set; }
        }

        public class trnaritem
        {
            public int rabdtl5oid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal rabdtl5amt { get; set; }
            public string rabdtl5note { get; set; }
        }

        public class supplier
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string supptaxable { get; set; }
            public decimal supptaxvalue { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabtype { get; set; }
            public int deptoid { get; set; }
            public int curroid { get; set; }
            public string rabmstnote { get; set; }
        }

        public class listapitemdtl2
        {
            public int apitemdtl2seq { get; set; }
            public int fabelioid { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
            public decimal apitemdtl2amt { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE activeflag='ACTIVE'";
            var pphacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgoid", tbl.pphacctgoid);
            ViewBag.pphacctgoid = pphacctgoid;
        }

        [HttpPost]
        public ActionResult InitDDLPerson()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m01US> tbl = new List<QL_m01US>();
            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            tbl = db.Database.SqlQuery<QL_m01US>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDiv()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLSuppAccount(int suppoid)
        {
            var result = "sukses";
            var msg = "";
            List<suppbankaccount> tbl = new List<suppbankaccount>();
            if (suppoid != 0)
            {
                sSql = "SELECT COUNT(*) FROM QL_mstsuppbankaccount WHERE activeflag='ACTIVE' AND suppoid=" + suppoid;
                if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() == 0)
                {
                    sSql = "SELECT 0 accountoid, '' accountname";
                }
                else if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() == 1)
                {
                    sSql = "SELECT accountoid, gen.gendesc + ' - ' + accountno + ' - ' + accounttitle AS accountname FROM QL_mstsuppbankaccount acc INNER JOIN QL_mstgen gen ON acc.bankoid=gen.genoid WHERE acc.activeflag='ACTIVE' AND suppoid=" + suppoid + " ORDER BY accounttitle";
                }
                else if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 1)
                {
                    sSql = "SELECT accountoid, gen.gendesc + ' - ' + accountno + ' - ' + accounttitle AS accountname FROM QL_mstsuppbankaccount acc INNER JOIN QL_mstgen gen ON acc.bankoid=gen.genoid WHERE acc.activeflag='ACTIVE' AND suppoid=" + suppoid + " ORDER BY accounttitle";
                }
            }
            else
            {
                sSql = "SELECT 0 accountoid, '' accountname";
            }
            tbl = db.Database.SqlQuery<suppbankaccount>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindSupplierData()
        {
            List<supplier> tbl = new List<supplier>();
            sSql = "SELECT suppoid, suppcode, suppname, suppaddr, (CASE supppajak WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS supptaxable, 10.00 AS supptaxvalue FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY suppcode";
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string filterperiodfrom, string filterperiodto)
        {
            List<listrab> tbl = new List<listrab>();

            sSql = $"SELECT rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabtype, rm.rabmstnote, rm.curroid, rm.deptoid FROM QL_trnrabmst rm WHERE rm.rabmststatus='Approved' AND rm.rabmstoid IN(SELECT rabmstoid FROM QL_trnrabdtl5) {(!string.IsNullOrEmpty(filterperiodfrom) && !string.IsNullOrEmpty(filterperiodto) ? $"AND rm.rabdate >='{filterperiodfrom} 00:00:00' AND rm.rabdate <='{filterperiodto} 23:00:00'" : "")}";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetFakturData(int suppoid)
        {
            List<listapitemdtl2> tbl = new List<listapitemdtl2>();

            sSql = "SELECT 0 AS apitemdtl2seq, fa.fabelioid, fa.fabelicode, fa.fakturno, (fa.fabeliqty-fa.fabeliaccumqty) fabeliaccumqty, 0.0 AS apitemdtl2amt FROM QL_mstfabeli fa WHERE fa.activeflag='ACTIVE' AND fa.suppoid=" + suppoid + " AND (fabeliqty-fabeliaccumqty)>0 ORDER BY fa.fakturno";
            tbl = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindDPData(int suppoid, int curroid)
        {
            List<trndpap> tbl = new List<trndpap>();
            sSql = "SELECT dp.dpapoid, dp.dpapno, dpapdate, dp.acctgoid AS dpapacctgoid, a.acctgdesc, dp.dpapnote, (dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) AS dpapamt, (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt ORDER BY dp.dpapoid";

            tbl = db.Database.SqlQuery<trndpap>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindARData(int rabmstoid)
        {
            List<trnaritem> tbl = new List<trnaritem>();
            sSql = "SELECT rd5.rabdtl5oid, a.acctgcode, a.acctgdesc, (rd5.rabdtl5price - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0) - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0)) rabdtl5amt, rd5.rabdtl5note FROM QL_trnrabdtl5 rd5 INNER JOIN QL_mstacctg a ON a.acctgoid=rd5.acctgoid WHERE rd5.cmpcode='"+ CompnyCode +"' AND rd5.rabmstoid="+ rabmstoid +" /*AND (rd5.rabdtl5price - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0) - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0))>0*/";

            tbl = db.Database.SqlQuery<trnaritem>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindTotalDP(string action, int curroid, int suppoid, int cashbankoid)
        {
            decimal totaldpamt = 0;
            if (action == "Edit")
            {
                sSql = " SELECT ISNULL((SELECT SUM(dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt), 0.0) + cashbankamt FROM QL_trncashbankmst cb WHERE cb.cashbankoid=" + cashbankoid;
            }
            else
            {
                sSql = " SELECT ISNULL(SUM(dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)), 0.0) AS dpapamt FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt ";
            }
            totaldpamt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return Json(totaldpamt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListCOARAB(string sVar, int rabmstoid)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT a.acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0)) cashbankglamt, '' cashbankglnote, curroid FROM QL_mstacctg a INNER JOIN QL_trnrabdtl5 rd5 ON rd5.acctgoid=a.acctgoid WHERE rd5.cmpcode='" + CompnyCode + "' AND a.activeflag='ACTIVE' AND a.acctgoid IN (" + acctgoid + ") AND rd5.rabmstoid=" + rabmstoid + " /*AND (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0))>0*/ ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<cashbankgl> dtDtl, List<listapitemdtl2> dtDtl2)
        {
            Session["QL_trncashbankgl_rabreal"] = dtDtl;
            Session["ql_trnapdirdtl2"] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<cashbankgl> dtDtl)
        {
            Session["QL_trncashbankgl_rabreal"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listapitemdtl2> dtDtl)
        {
            Session["ql_trnapdirdtl2"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trncashbankgl_rabreal"] == null)
            {
                Session["QL_trncashbankgl_rabreal"] = new List<cashbankgl>();
            }

            List<cashbankgl> dataDtl = (List<cashbankgl>)Session["QL_trncashbankgl_rabreal"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2()
        {
            if (Session["ql_trnapdirdtl2"] == null)
            {
                Session["ql_trnapdirdtl2"] = new List<listapitemdtl2>();
            }

            List<listapitemdtl2> dataDtl = (List<listapitemdtl2>)Session["ql_trnapdirdtl2"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.aritemno = db.Database.SqlQuery<string>("SELECT acctgdesc FROM QL_trnrabdtl5 r INNER JOIN QL_mstacctg a ON a.acctgoid=r.acctgoid WHERE r.rabdtl5oid ='" + tbl.rabdtl5oid + "'").FirstOrDefault();
            ViewBag.rabdtl5amt = db.Database.SqlQuery<decimal>("SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid AND cb.cashbankoid<>"+ tbl.cashbankoid +"),0.0)) AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.rabdtl5oid ='" + tbl.rabdtl5oid + "'").FirstOrDefault();
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.cashbankrefno = db.Database.SqlQuery<string>("SELECT cashbankrefno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();
            ViewBag.dpapno = "";
            ViewBag.totaldpamt = 0;
            ViewBag.cashbankresamt = 0;
            ViewBag.dpdateforcheck = DateTime.Parse("1/1/1900 00:00:00");
            if (tbl.cashbanktype == "BLK")
            {
                ViewBag.dpapno = db.Database.SqlQuery<string>("SELECT dpapno FROM QL_trndpap WHERE dpapoid=" + tbl.giroacctgoid).FirstOrDefault();
                ViewBag.totaldpamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)), 0.0) FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + tbl.refsuppoid + " AND dp.curroid=" + tbl.curroid + " AND dp.dpapamt > dp.dpapaccumamt").FirstOrDefault() + tbl.cashbankamt;
                ViewBag.cashbankresamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(dpapamt - ISNULL(dp.dpapaccumamt, 0.0), 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault() + tbl.cashbankamt;
                ViewBag.dpdateforcheck = db.Database.SqlQuery<DateTime>("SELECT (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault();
            }
        }

        private string GenerateExpenseNo2(string cmpcode, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmpcode, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Real",["Project"] = "Project" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cb.cashbankoid, cb.cashbankno, cb.cashbankdate, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, p.usname personname, cb.cashbankstatus, cb.cashbanknote, ISNULL(suppname, '') AS suppname, ISNULL((SELECT rm.projectname FROM ql_trnrabmst rm WHERE rm.rabmstoid=ISNULL(cb.rabmstoid,0)),'') projectname, '' AS CBType, cashbankamt, CASE WHEN cb.cashbankstatus = 'Revised' THEN revisereason WHEN cb.cashbankstatus = 'Rejected' THEN rejectreason ELSE '' END reason FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_m01us p ON p.usoid=cb.personoid LEFT JOIN QL_mstsupp s ON s.suppoid=cb.refsuppoid WHERE cb.cashbankgroup='RABREAL' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();
                tbl.rabmstoid = 0;
                tbl.rabdtl5oid = 0;
                tbl.pphamt = 0;
                tbl.refsuppoid = 0;

                Session["QL_trncashbankgl_rabreal"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                Session["lastcashbanktype"] = tbl.cashbanktype;
                Session["lastdpapoid"] = tbl.giroacctgoid;
                Session["lastdpapamt"] = tbl.cashbankamt;

                sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND cbgl.cmpcode='" + CompnyCode + "' ORDER BY cashbankgloid";
                var tbldtl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].cashbankglseq = i + 1;
                        }
                    }
                }
                Session["QL_trncashbankgl_rabreal"] = tbldtl;

                sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trncashbankmst' ORDER BY rd.apitemdtl2seq";
                Session["ql_trnapdirdtl2"] = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string tglmst, string tglduedate, string tgltakegiro)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                tbl.cashbankdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.cashbankduedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankduedate", "Format Tanggal Due Date Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.cashbanktakegiro = DateTime.Parse(ClassFunction.toDate(tgltakegiro));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbanktakegiro", "Format Tanggal Take Giro Tidak Valid!!" + ex.ToString());
            }

            tbl.cmpcode = CompnyCode;
            if (tbl.cashbankno == null)
                tbl.cashbankno = "";

            if (tbl.rabmstoid == 0)
            {
                ModelState.AddModelError("rabmstoid", "Silahkan Pilih Project!");
            }
            if (tbl.rabdtl5oid == 0)
            {
                ModelState.AddModelError("rabdtl5oid", "Silahkan Pilih Biaya RAB!");
            }
            if (tbl.cashbankstatus.ToUpper() == "REVISED")
            {
                tbl.cashbankstatus = "In Process";
            }

            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankgl_rabreal"];
            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["ql_trnapdirdtl2"];
            string sErrReply = "";
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                //if (tbl.refsuppoid != 0)
                //    if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                //        if (tbl.cashbanksuppaccoid == 0)
                //            ModelState.AddModelError("", "Please fill SUPPLIER ACCOUNT field!");
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than EXPENSE DATE");
            }
            else if (tbl.cashbanktype == "BLK")
            {
                if (tbl.giroacctgoid == 0)
                    ModelState.AddModelError("", "Please select DP NO. field!");
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault())
                    ModelState.AddModelError("", "PAYMENT DATE must be more than DP DATE!");
            }
            if (tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktakegiro > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be less or equal than DUE DATE!");
                if (tbl.cashbankdate > tbl.cashbanktakegiro)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than EXPENSE DATE!");
                if (tbl.refsuppoid == 0)
                    ModelState.AddModelError("", "Please select SUPPLIER field!");
            }
            if (tbl.personoid.ToString() == "0")
                ModelState.AddModelError("", "Please select PIC field!");
            if (!ClassFunction.isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", tbl.cashbanktaxamt, ref sErrReply))
                ModelState.AddModelError("", "OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbankamt < 0)
            {
                ModelState.AddModelError("", "GRAND TOTAL must be more than 0!");
            }
            else
            {
                if (tbl.cashbanktype == "BLK")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        ModelState.AddModelError("", "GRAND TOTAL must be less than DP AMOUNT!");
                    }
                    else
                    {
                        sSql = "SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (action == "Edit")
                            dNewDPBalance += tbl.cashbankamt;
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt)
                            ModelState.AddModelError("", "DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!");
                    }
                }
            }
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "GRAND TOTAL must be less than MAX GRAND TOTAL (" + sErrReply + ") allowed stored in database!");

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].cashbankglamt == 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be not equal to 0!");
                        }
                        else
                        {
                            if (!ClassFunction.isLengthAccepted("cashbankglamt", "QL_trncashbankgl", dtDtl[i].cashbankglamt, ref sErrReply))
                            {
                                ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!");
                            }
                        }
                    }
                }
            }

            //if (tbl.rabmstoid > 0)
            //{
            //    sSql = "SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid AND cb.cashbankoid<>" + tbl.cashbankoid + "),0.0)) AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.rabdtl5oid ='" + tbl.rabdtl5oid + "'";
            //    var PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            //    if (tbl.cashbankdpp > PriceOs)
            //        ModelState.AddModelError("", "Amount Realisasi RAB tidak boleh lebih dari Amount O/S (" + PriceOs + ")");
            //}

            //var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK" || tbl.cashbanktype == "RBB")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = tbl.cashbankduedate;
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = tbl.cashbankduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iTaxAcctgOid = 0;
            int iOtherTaxAcctgOid = 0;
            int iGiroAcctgOid = tbl.giroacctgoid;

            if (tbl.cashbankstatus == "Post")
            {
                //if (tbl.cashbanktype == "BLK")
                //    cRate.SetRateValue(db.Database.SqlQuery<int>("SELECT rateoid FROM QL_trndpap WHERE dpapoid=" + tbl.giroacctgoid).FirstOrDefault(), (db.Database.SqlQuery<int>("SELECT rate2oid FROM QL_trndpap WHERE dpapoid=" + tbl.giroacctgoid).FirstOrDefault()));
                //else
                //    cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cashbankstatus = "In Process";
                //}
                //if (cRate.GetRateMonthlyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cashbankstatus = "In Process";
                //}
                string sVarErr = "";
                if (tbl.cashbanktaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", tbl.cmpcode))
                        sVarErr = "VAR_PPN_IN";
                    else
                        iTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", tbl.cmpcode));
                }
                if (tbl.cashbankothertaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_OTHER_TAX_EXPENSE", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_OTHER_TAX_EXPENSE";
                    else
                        iOtherTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_OTHER_TAX_EXPENSE", tbl.cmpcode));
                }
                if (tbl.cashbanktype == "BGK")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", tbl.cmpcode));
                    tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", tbl.cmpcode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.cashbankstatus = "In Process";
                }
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            //if (tbl.cashbankstatus == "Post")
            //{
                cRate.SetRateValue(tbl.curroid, sDueDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
           // }

            string apitemdtl2type = "QL_trncashbankmst";
            //var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.refsuppoid).FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;
            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                        fakturnoGL += dtDtl2[i].fakturno + ",";
                    }
                    fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.cashbankstatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.cashbankstatus = "In Process";
            }
            if (tbl.cashbankstatus == "Post" || tbl.cashbankstatus == "In Approval")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.cashbankstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateExpenseNo2(tbl.cmpcode, tglmst, tbl.cashbanktype, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var dtl2oid = ClassFunction.GenerateID("QL_trnapitemdtl2");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbankgroup = "RABREAL";
                //tbl.cashbanktakegiro = (tbl.cashbanktype == "BGK" ? tbl.cashbanktakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.refsuppoid).FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (Session["lastcashbanktype"] != null && Session["lastcashbanktype"].ToString() == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " + Session["lastdpapamt"] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + Session["lastdpapoid"];
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            if (tbl.cashbanktype == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(" + tbl.rabdtl5oid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tbl.cashbankoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();

                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trncashbankgl();
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglgiroflag = "";

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamt = dtDtl[i].cashbankglamt;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.cashbankglamtusd = 0;
                            tbldtl.cashbankglduedate = (tbl.cashbanktype != "BKK" ? tbl.cashbankduedate : DateTime.Parse("1/1/1900 00:00:00"));
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.groupoid = 0;

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='Complete' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(" + tbl.rabdtl5oid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnapitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnapitemdtl2();
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.apitemdtl2oid = dtl2oid++;
                                    tbldtl2.apitemmstoid = tbl.cashbankoid;
                                    tbldtl2.apitemdtl2seq = i + 1;
                                    tbldtl2.fabelioid = dtDtl2[i].fabelioid;
                                    tbldtl2.apitemdtl2amt = dtDtl2[i].apitemdtl2amt;
                                    tbldtl2.createuser = tbl.upduser;
                                    tbldtl2.createtime = tbl.updtime;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.apitemdtl2type = apitemdtl2type;

                                    db.QL_trnapitemdtl2.Add(tbldtl2);
                                    db.SaveChanges();

                                    if (tbl.cashbankstatus == "Post")
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }

                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnapitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.cashbankstatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.cashbankoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnrabreal";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.cashbankstatus == "Post")
                        {
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Pengeluaran Cash/Bank | " + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", dtDtl[i].cashbankglamt, tbl.cashbankno, "Pengeluaran Cash/Bank | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + " | " + ViewBag.suppname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            //if (tbl.cashbanktaxamt > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iTaxAcctgOid, "D", tbl.cashbanktaxamt, tbl.cashbankno, "Pengeluaran Cash/Bank | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + " | " + ViewBag.suppname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbanktaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Tax C/B Expense No. " + tbl.cashbanknote + "", "K", 0));
                            //    db.SaveChanges();
                            //    iSeq += 1;
                            //    gldtloid += 1;
                            //}

                            if (tbl.cashbankothertaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iOtherTaxAcctgOid, "D", tbl.cashbankothertaxamt, tbl.cashbankno, "Pengeluaran Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.suppname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankothertaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Other Tax C/B Expense No. " + tbl.cashbanknote + "", "K", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt < 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "C", -dtDtl[i].cashbankglamt, tbl.cashbankno, "Pengeluaran Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.suppname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            if (tbl.cashbanktype == "BGK")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iGiroAcctgOid, "C", tbl.cashbankamt, tbl.cashbankno, "Pengeluaran Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.suppname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                            }
                            else
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt, tbl.cashbankno, "Pengeluaran Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.suppname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                            }

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (Session["lastcashbanktype"] != null && Session["lastcashbanktype"].ToString() == "BLK")
                        {
                            sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " + Session["lastdpapamt"] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dpapoid=" + Session["lastdpapoid"];
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(" + tbl.rabdtl5oid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == CompnyCode);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: dparMaterial/Revisi/5/11
        [HttpPost, ActionName("RevisiAR")]
        [ValidateAntiForgeryToken]
        public ActionResult RevisiARConfirmed(int id, int aritemmstoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trncashbankmst SET aritemmstoid=" + aritemmstoid + " WHERE cashbankoid=" + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, Boolean cbprintbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptRABCostReal.rpt"));
            sSql = "SELECT cb.cashbankoid, currcode AS [Currency], currsymbol AS [Curr Symbol], currdesc AS [Curr Desc], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ISNULL(cb.rabmstoid,0)),'') AS [Project], (CASE cashbanktype WHEN 'BKK' THEN 'Kas' WHEN 'BBK' THEN 'Transfer' WHEN 'BGK' THEN 'Giro/Cheque' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE '' END) AS [Payment Type], ('(' + a1.acctgcode + ') ' + a1.acctgdesc) AS [Payment Account], cashbankrefno AS [Ref. No.], cashbankduedate AS [Due Date], cashbanktakegiro AS [Date Take Giro], ISNULL(suppname, 'CASH') AS [Supplier], ISNULL(suppcode, '') AS [Supp. Code], ISNULL(suppaddr, '') AS [Supp. Address], ISNULL(g1.gndesc, '') AS [Supp. City], ISNULL(g2.gndesc, '') AS [Supp. Province], ISNULL(g3.gndesc, '') AS [Supp. Country], ISNULL(suppemail, '') AS [Supp. Email], ISNULL(suppphone1, '') AS [Supp. Phone 1], ISNULL(suppphone2, '') AS [Supp. Phone 2], '' AS [Supp. Phone 3], ISNULL(suppfax1, '') AS [Supp. Fax 1], '' AS [Supp. Fax 2], cashbankdpp AS [Total Amount], cashbanktaxtype AS [Tax Type], CONVERT(VARCHAR(10), cashbanktaxpct) AS [Tax Pct], cashbanktaxamt AS [Tax Amount], cashbankothertaxamt AS [Other Tax Amount], ISNULL(cb.pphamt,0.0) [PPH Amt], cashbankamt AS [Grand Total], p.usname AS [PIC], cashbankstatus AS [Status], cashbanknote AS [Header Note], a2.acctgcode AS [COA No.], a2.acctgdesc AS [COA Desc.], cashbankglamt AS [Amount], cashbankglnote AS [Note], divname AS [Business Unit], divaddress AS [BU Address], g4.gndesc AS [BU City], g5.gndesc AS [BU Province], g6.gndesc AS [BU Country], ISNULL(divphone, '') AS [BU Phone 1], ISNULL(divphone2, '') AS [BU Phone 2], divemail AS [BU Email], cb.upduser AS [Last Upd. User ID], ISNULL(pr.usname,UPPER(cb.upduser)) AS [Last Upd. User Name], cb.updtime AS [Last Upd. Datetime]";
            if (cbprintbbk)
            {
                sSql += ", ISNULL((SELECT cbx.cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayapgiro pg ON pg.cmpcode=cbx.cmpcode AND pg.cashbankoid=cbx.cashbankoid WHERE pg.cmpcode=cb.cmpcode AND pg.reftype='QL_trncashbankmst' AND pg.refoid=cb.cashbankoid), '') AS [Cash/Bank No.], ISNULL((SELECT cbx.cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayapgiro pg ON pg.cmpcode=cbx.cmpcode AND pg.cashbankoid=cbx.cashbankoid WHERE pg.cmpcode=cb.cmpcode AND pg.reftype='QL_trncashbankmst' AND pg.refoid=cb.cashbankoid), CAST('1/1/1900' AS DATETIME)) [Expense Date]";
            }
            else
            {
                sSql += ", cashbankno AS[Cash / Bank No.], cashbankdate AS[Expense Date]";
            }

            sSql += " FROM QL_trncashbankmst cb INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstacctg a1 ON a1.acctgoid=cb.acctgoid LEFT JOIN QL_mstsupp s ON s.suppoid=cb.refsuppoid INNER JOIN QL_m01US p ON p.usoid=cb.personoid INNER JOIN QL_trncashbankgl gl ON gl.cmpcode=cb.cmpcode AND gl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=gl.acctgoid INNER JOIN QL_mstdivision di ON di.cmpcode=cb.cmpcode LEFT JOIN QL_m05GN g1 ON g1.cmpcode=s.cmpcode AND g1.gnoid=suppcityOid LEFT JOIN QL_m05GN g2 ON g2.cmpcode=g1.cmpcode AND g2.gnoid=CONVERT(INT, g1.gnother1) LEFT JOIN QL_m05GN g3 ON g3.cmpcode=g1.cmpcode AND g3.gnoid=CONVERT(INT, g1.gnother2) INNER JOIN QL_m05GN g4 ON g4.gnoid=divcityoid INNER JOIN QL_m05GN g5 ON g5.cmpcode=g4.cmpcode AND g5.gnoid=CONVERT(INT, g4.gnother1) INNER JOIN QL_m05GN g6 ON g6.cmpcode=g4.cmpcode AND g6.gnoid=CONVERT(INT, g4.gnother2) LEFT JOIN QL_m01US pr ON pr.usoid=cb.upduser WHERE cashbankgroup='RABREAL'";
            if (cbprintbbk)
            {
                sSql += " AND cashbanktype='BGK'";
            }
            sSql += " AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid IN (" + id + ") ORDER BY cb.cashbankoid, gl.cashbankgloid";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintRABCostReal");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("UserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "RABCostRealPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}