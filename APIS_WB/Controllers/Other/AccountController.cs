﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Other
{
    //[Authorize]
    public class AccountController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string sSql = "";

        public AccountController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET: Account
        public ActionResult LogIn()
        {
            return View();
            //return RedirectToAction("Index.html", "Login");
        }
        [HttpPost]
        public ActionResult LogIn(QL_m01US us)
        {
            if (string.IsNullOrEmpty(us.usoid))
                ModelState.AddModelError("usoid", "User ID harap di isi");
            if (string.IsNullOrEmpty(us.uspassword))
                ModelState.AddModelError("uspassword", "Password harap di isi");

            try
            {
                int glmstoid = 0; int glmstoid_new = 0; int gldtloid = 0; int gldtloid_new = 0;
                int conapoid = 0; int conapoid_new = 0; int conaroid = 0; int conaroid_new = 0;
                int conmatoid = 0; int conmatoid_new = 0; int cashbankoid = 0; int cashbankoid_new = 0;
                int cashbankgloid = 0; int cashbankgloid_new = 0; int apitemdtl2oid = 0; int apitemdtl2oid_new = 0;

                glmstoid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_trnglmst'").FirstOrDefault();
                glmstoid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(glmstoid) FROM QL_trnglmst),0) AS t").FirstOrDefault();
                gldtloid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_trngldtl'").FirstOrDefault();
                gldtloid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(gldtloid) FROM QL_trngldtl),0) AS t").FirstOrDefault();
                conapoid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_conap'").FirstOrDefault();
                conapoid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(conapoid) FROM QL_conap),0) AS t").FirstOrDefault();
                conaroid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_conar'").FirstOrDefault();
                conaroid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(conaroid) FROM QL_conar),0) AS t").FirstOrDefault();
                conmatoid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_conmat'").FirstOrDefault();
                conmatoid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(conmatoid) FROM QL_conmat),0) AS t").FirstOrDefault();
                cashbankoid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_trncashbankmst'").FirstOrDefault();
                cashbankoid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(cashbankoid) FROM QL_trncashbankmst),0) AS t").FirstOrDefault();
                cashbankgloid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_trncashbankgl'").FirstOrDefault();
                cashbankgloid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(cashbankgloid) FROM QL_trncashbankgl),0) AS t").FirstOrDefault();
                apitemdtl2oid = db.Database.SqlQuery<int>("SELECT lastoid FROM QL_ID WHERE tablename='QL_trnapitemdtl2'").FirstOrDefault();
                apitemdtl2oid_new = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT MAX(apitemdtl2oid) FROM QL_trnapitemdtl2),0) AS t").FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (glmstoid != glmstoid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid_new + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (gldtloid != gldtloid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + gldtloid_new + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (conapoid != conapoid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + conapoid_new + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (conaroid != conaroid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + conaroid_new + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (conmatoid != conmatoid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + conmatoid_new + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (cashbankoid != cashbankoid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + cashbankoid_new + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (cashbankgloid != cashbankgloid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + cashbankgloid_new + " WHERE tablename='QL_trncashbankgl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (apitemdtl2oid != apitemdtl2oid_new)
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + apitemdtl2oid_new + " WHERE tablename='QL_trnapitemdtl2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.ToString());
            }

            if (ModelState.IsValid)
            {
                var user = db.QL_m01US.Where(w => w.usoid.ToLower() == us.usoid.ToLower() && w.usflag == "ACTIVE").FirstOrDefault();
                
                if (user == null)
                {
                    ModelState.AddModelError("", "User ID atau Password Salah");
                }
                else if (user.uspassword != us.uspassword)
                {
                    ModelState.AddModelError("", "User ID atau Password Salah");
                }
                else
                {
                    Session["UserID"] = user.usoid;
                    Session["UserName"] = user.usname;
                    Session["CompnyCode"] = user.cmpcode;
                    Session["ApprovalLimit"] = 0;
                    Session["Jabatan"] = db.Database.SqlQuery<string>("SELECT g.gndesc FROM QL_m01US us INNER JOIN QL_m05GN g ON g.gnoid=us.Jabatanoid WHERE us.usoid='" + user.usoid + "'").FirstOrDefault();
                    Session["ExpMasterXls"] = db.Database.SqlQuery<string>("SELECT us.expmastertype FROM QL_m01US us WHERE us.usoid='" + user.usoid + "'").FirstOrDefault();

                    if (user.ustype.ToUpper() == "ADMIN" | user.usoid.ToUpper() == "ADMIN")
                    {
                        sSql = "SELECT distinct mn.* FROM QL_m04MN mn WHERE mn.mnflag='ACTIVE'";
                    }
                    else
                    {
                        sSql = "SELECT distinct mn.* FROM QL_m04MN mn INNER JOIN QL_m02RLdtl rld ON rld.mnoid=mn.mnoid INNER JOIN QL_m03UR ur ON ur.rloid=rld.rloid WHERE mn.mnflag='ACTIVE' AND ur.usoid = '" + user.usoid + "'";
                    }
                    Session["Role"] = db.Database.SqlQuery<QL_m04MN>(sSql).ToList();

                    FormsAuthentication.SetAuthCookie(user.usoid, false);
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(us);
        }
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("LogIn", "Account");
        }

        // GET: Account
        public ActionResult NotAuthorize()
        {
            return View();
        }
        
    }
}