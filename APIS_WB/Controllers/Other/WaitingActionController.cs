﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Other
{
    public class WaitingActionController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public WaitingActionController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class waitactmodel
        {
            public int mstoid { get; set; }
            public string cmpcode { get; set; }
            public string tblname { get; set; }
        }

        public class modelforapp
        {
            public string caption { get; set; }
            public string value { get; set; }
        }

        public class listprabdtl
        {
            public int rabdtloid { get; set; }
            public int itemoid { get; set; }
            public int rabunitoid { get; set; }
            public decimal rabqty { get; set; }
            public decimal rabprice { get; set; }
            public decimal rabdtlamt { get; set; }
            public decimal rabtaxvalue { get; set; }
            public decimal rabtaxamt { get; set; }          
            public decimal rabdtlnetto { get; set; }
            public int rablocoid { get; set; }
            public DateTime? rabetd { get; set; }
            
        }

        public class listprabdtl3
        {
            public int rabdtl3oid { get; set; }
            public int itemdtl3oid { get; set; }
            public int rabdtl3unitoid { get; set; }
            public decimal rabdtl3qty { get; set; }
            public decimal rabdtl3price { get; set; }
            public decimal rabdtl3amt { get; set; }
            public decimal rabdtl3taxvalue { get; set; }
            public decimal rabdtl3taxamt { get; set; }
            public decimal rabdtl3netto { get; set; }
            public int rabdtl3locoid { get; set; }
            public DateTime rabdtl3etd { get; set; }

        }

        public class listrabdtl2
        {
            public int rabdtl2seq { get; set; }
            public int rabdtl2oid { get; set; }
            public int itemdtl2oid { get; set; }
            public string itemcode { get; set; }
            public int suppdtl2oid { get; set; }
            public string suppdtl2name { get; set; }
            public string itemdesc { get; set; }
            public decimal rabdtl2qty { get; set; }
            public int rabdtl2unitoid { get; set; }
            public string rabdtl2unit { get; set; }
            public decimal rabdtl2price { get; set; }
            public decimal rabdtl2amt { get; set; }
            public decimal rabdtl2discvalue { get; set; }
            public decimal rabdtl2discamt { get; set; }
            public decimal rabdtl2taxvalue { get; set; }
            public decimal rabdtl2taxamt { get; set; }
            public decimal rabdtl2netto { get; set; }
            public string rabdtl2status { get; set; }
            public string rabdtl2note { get; set; }
            public decimal rabdtl2ongkiramt { get; set; }
            public DateTime rabdtl2eta { get; set; }
            public string rabdtl2etastr { get; set; }
            public int rabdtl2locoid { get; set; }
        }

        public class listrabdtl5
        {
            public int rabdtl5seq { get; set; }
            public int rabdtl5oid { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal rabdtl5price { get; set; }
            public string rabdtl5note { get; set; }
        }

        public class listappfinal
        {
            public int stoid_app { get; set; }
        }

        private string CekStatusApp(string menu, string user)
        {
            sSql = "SELECT ISNULL(a.asflag,'Belum') asflag FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_app INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='"+ CompnyCode +"' AND mnfileloc='"+ menu +"' AND u.usoid='"+ user +"'";
            string stts = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return stts;
        }

        private List<listappfinal> BindAppFinal(string menu)
        {
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='"+ CompnyCode +"' AND mnfileloc='"+ menu +"' AND ISNULL(a.asflag,'')='Final'";
            var app = db.Database.SqlQuery<listappfinal>(sSql).ToList();

            return app;
        }

        [HttpPost]
        public ActionResult setrevisednote(string rv)
        {
            Session["rvnote"] = rv;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult setrejectednote(string rj)
        {
            Session["rjnote"] = rj;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: WaitingAction
        public ActionResult Index(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            ViewBag.TransName = db.Database.SqlQuery<string>("SELECT DISTINCT mn.mnname FROM QL_APP ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform WHERE ap.tablename='" + id + "'").FirstOrDefault();
            ViewBag.tblname = id;
            return View();
        }

        [HttpPost]
        public ActionResult GetDataList(string tblname)
        {
            var msg = "";

            if (tblname == "QL_trnreqrabmst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.reqrabmstoid [Draft No.], CONVERT(VARCHAR(10), h.reqrabdate, 103) [Tgl PRAB], h.reqrabpkm [PKM], h.reqrabtype [Tipe PRAB], h.reqrabtype2 [Jenis PRAB], custname [Customer], h.projectname [Project], reqrabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnreqrabmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.reqrabmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY reqrabdate";

            }
            else if (tblname == "QL_trnrabmst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.rabmstoid [Draft No.], CONVERT(VARCHAR(10), h.rabdate, 103) [Tgl RAB]], h.revrabtype [Tipe Trans], h.rabtype [Tipe RAB], h.rabtype2 [Jenis RAB], custname [Customer], h.projectname [Project], rabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnrabmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.rabmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY rabdate";

            }
            else if (tblname == "QL_trnrabmst_hist")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.rabmstoid_hist [Draft No.], CONVERT(VARCHAR(10), h.rabdate, 103) [Tgl RAB], h.rabtype [Tipe RAB], h.rabtype2 [Jenis RAB], custname [Customer], h.projectname [Project], rabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnrabmst_hist h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.rabmstoid_hist INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY rabdate";

            }
            else if (tblname == "QL_trnrabmst_hist2")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.rabmstoid_hist [Draft No.], CONVERT(VARCHAR(10), h.rabdate, 103) [Tgl RAB], h.rabtype [Tipe RAB], h.rabtype2 [Jenis RAB], custname [Customer], h.projectname [Project], rabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnrabmst_hist h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.rabmstoid_hist INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY rabdate";

            }
            else if (tblname == "QL_trnpoitemmst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.poitemmstoid [Draft No.], CONVERT(VARCHAR(10), h.poitemdate, 103) [Tgl PO], ISNULL((SELECT rabtype FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Tipe RAB], ISNULL((SELECT projectname FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Project], (SELECT soitemno FROM QL_trnsoitemmst where soitemmstoid = h.somstoid) [SO No.], suppname [Supplier], (SELECT projectname FROM QL_trnrabmst where rabmstoid = h.rabmstoid) [Project], ISNULL((SELECT top 1 g.gndesc FROM QL_trnpoitemdtl pd INNER JOIN QL_m05GN g ON g.gnoid=pd.poitemdtllocoid WHERE pd.poitemmstoid=h.poitemmstoid),'') [Kirim Ke], poitemmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnpoitemmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.poitemmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.poitemtype NOT IN ('Jasa') AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY poitemdate";

            }
            else if (tblname == "QL_trnpojasamst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.poitemmstoid [Draft No.], CONVERT(VARCHAR(10), h.poitemdate, 103) [Tgl PO], ISNULL((SELECT rabtype FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Tipe RAB], suppname [Supplier], ISNULL((SELECT projectname FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Project], poitemmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnpoitemmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.poitemmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.poitemtype = 'Jasa' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY poitemdate";

            }
            else if (tblname == "QL_trnpoassetmst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.poassetmstoid [Draft No.], CONVERT(VARCHAR(10), h.poassetdate, 103) [Tgl PO], suppname [Supplier], poassetmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnpoassetmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.poassetmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY poassetdate";

            }
            else if (tblname == "QL_mstcusthist")
            {
                sSql = "SELECT DISTINCT h.cmpcode, ch.custhistoid [Draft No.], custcode [Kode], custname [Nama], ch.custcreditlimitold [Credit Limit Lama], ch.custcreditlimitnew [Credit Limit Baru], custnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_mstcust h INNER JOIN QL_mstcusthist ch ON ch.custoid = h.custoid INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=ch.custhistoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY a.reqdate";

            }
            else if (tblname == "QL_trnstockadj")
            {
                sSql = "SELECT DISTINCT a.cmpcode, CAST(resfield1 AS INTEGER) AS [Draft No.], CONVERT(VARCHAR(10), stockadjdate, 101) AS [Adj Date], stockadjtype AS [Type], ap.requser [Request User], CONVERT(VARCHAR(10), ap.reqdate, 101) AS [Request Date] FROM QL_trnstockadj a INNER JOIN QL_app ap ON ap.appformoid=CAST(resfield1 AS INTEGER) INNER JOIN QL_m01US us ON us.stoid=ap.appstoid WHERE ISNULL(ap.appstatus, '')= '' AND ap.tablename='" + tblname + "' AND stockadjstatus='In Approval' AND us.usoid='" + Session["UserID"].ToString() + "' GROUP BY a.cmpcode, resfield1, stockadjno, stockadjdate, ap.appoid, ap.requser, ap.reqdate, a.upduser, stockadjtype --ORDER BY ap.reqdate DESC";
            }
            else if (tblname == "QL_trnkasbon2mst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.kasbon2mstoid [Draft No.], CONVERT(VARCHAR(10), h.kasbon2date, 103) [Tgl Kasbon Realisasi], ISNULL((SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = ISNULL(h.rabmstoid,0)),'') [Project], suppname [Supplier], h.kasbon2note [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnkasbon2mst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.kasbon2mstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else if (tblname == "QL_trnapdirmst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.apdirmstoid [Draft No.], CONVERT(VARCHAR(10), h.apdirdate, 103) [Tgl AP Direct], ISNULL((SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = ISNULL(h.rabmstoid,0)),'') [Project], suppname [Supplier], h.apdirmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnapdirmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.apdirmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else if (tblname == "QL_trnglmst")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.glmstoid [Draft No.], ISNULL(h.glother1,'') [No. JU], CONVERT(VARCHAR(10), h.gldate, 103) [Tgl JU], h.glnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnglmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.glmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else if (tblname == "QL_trndebetnote")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.dnoid [Draft No.], CONVERT(VARCHAR(10), h.dndate, 103) [Tgl DN], h.dnnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trndebetnote h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.dnoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else if (tblname == "QL_trncreditnote")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.cnoid [Draft No.], CONVERT(VARCHAR(10), h.cndate, 103) [Tgl CN], h.cnnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trncreditnote h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.cnoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else if (tblname == "QL_trnrabreal")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.cashbankoid [Draft No.], CONVERT(VARCHAR(10), h.cashbankdate, 103) [Tgl Realisasi], ISNULL((SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = ISNULL(h.rabmstoid,0)),'') [Project], ISNULL((SELECT a.acctgdesc FROM QL_trnrabdtl5 rm inner join QL_mstacctg a on a.acctgoid=rm.acctgoid where rm.rabdtl5oid = ISNULL(h.rabdtl5oid,0)),'') [Biaya RAB], h.cashbanknote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trncashbankmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.cashbankoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else if (tblname == "QL_trnrabrealmulti")
            {
                sSql = "SELECT DISTINCT h.cmpcode, h.cashbankoid [Draft No.], CONVERT(VARCHAR(10), h.cashbankdate, 103) [Tgl Realisasi], CASE h.cost_type WHEN 'KASBON REAL' THEN 'REALISASI KASBON' ELSE h.cost_type END [Cost Type], h.cashbanknote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trncashbankmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.cashbankoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";
            }
            else
                msg = "Approval action haven't be set";
            
            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, tblname);

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Draft No.")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + item + "", "WaitingAction") + "?tblname=" + tblname + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    return Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: WaitingAction/Form/id/cmp
        public ActionResult Form(int id, string cmp, string tblname)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            ViewBag.TransName = db.Database.SqlQuery<string>("SELECT DISTINCT mn.mnname FROM QL_APP ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform WHERE ap.tablename='" + tblname + "'").FirstOrDefault();
            ViewBag.BusinessUnit = CompnyName;
            ViewBag.namaTabel = tblname;
            //db.Database.SqlQuery<string>("SELECT divname FROM QL_mstdivision WHERE cmpcode='" + cmp + "'").FirstOrDefault();

            waitactmodel tbl = new waitactmodel();
            tbl.mstoid = id;
            tbl.cmpcode = cmp;
            tbl.tblname = tblname;

            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDataApproval(int id, string cmp, string tblname)
        {
            var msg = "";
            var sSqlHdr = "";
            var info = "";
            var sSqlDtl = "";
            var sSqlDtl2 = "";
            var sSqlDtl3 = "";
            var sSqlDtl4 = "";
            var sSqlDtl5 = "";
            var sSqlFtr = "";

            if (tblname == "QL_trnreqrabmst")
            {
                sSqlHdr = "SELECT h.reqrabmstoid [Draft No.], CONVERT(VARCHAR(10), h.reqrabdate, 103) [Tgl PRAB], h.reqrabpkm [PKM], h.reqrabtype [Tipe PRAB], h.reqrabtype2 [Jenis PRAB], custname [Customer], h.projectname [Project], ISNULL((SELECT cr.currcode FROM QL_mstcurr cr WHERE cr.curroid=h.curroid),'') [Mata Uang], reqrabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnreqrabmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=reqrabmstoid INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.reqrabmstoid=" + id + " ORDER BY reqrabdate";

                sSqlDtl = "SELECT rd.reqrabseq [No], i.itemcode [Kode], i.itemdesc [Nama], rd.reqrabqty [DEC_Qty], rd.reqrabprice [DEC_Harga], rd.reqrabdtlamt [DEC_DPP], rd.reqrabtaxamt [DEC_PPN], rd.reqrabpphamt [DEC_PPH], rd.reqrabdtlnetto [DEC_Netto] FROM QL_trnreqrabdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid WHERE rd.reqrabmstoid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.reqrabdtlamt) [Total DPP], SUM(rd.reqrabtaxamt) [Total PPN], SUM(rd.reqrabpphamt) [Total PPH], SUM(rd.reqrabdtlnetto) [Total Netto] FROM QL_trnreqrabdtl rd WHERE rd.reqrabmstoid="+ id +"";

            } else if (tblname == "QL_trnrabmst")
            {
                sSqlHdr = "SELECT h.rabmstoid [Draft No.], CONVERT(VARCHAR(10), h.rabdate, 103) [Tgl RAB], h.revrabtype [Tipe Trans], h.rabtype [Tipe RAB], h.rabtype2 [Jenis RAB], custname [Customer], h.projectname [Project], ISNULL((SELECT cr.currcode FROM QL_mstcurr cr WHERE cr.curroid=h.curroid),'') [Mata Uang], ISNULL((SELECT cr.usname FROM QL_m01US cr WHERE cr.usoid=h.salesoid),'') [Sales], ISNULL((SELECT cr.usname FROM QL_m01US cr WHERE cr.usoid=h.salesadminoid),'') [Sales Admin], rabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnrabmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.rabmstoid INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.rabmstoid=" + id + " ORDER BY rabdate";

                sSqlDtl = "SELECT rd.rabseq [No], i.itemcode [Kode], i.itemdesc [Nama], rd.rabqty [DEC_Qty], rd.rabprice [DEC_Harga], rd.rabdtlamt [DEC_DPP], rd.rabtaxamt [DEC_PPN], rd.rabpphamt [DEC_PPH], rd.rabdtlnetto [DEC_Netto] FROM QL_trnrabdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid WHERE rd.rabmstoid=" + id + "";

                sSqlDtl2 = "SELECT rd.rabdtl2seq [No], s.suppname [Supplier], rd.rabdtl2flagrevisi [Status Revisi], i.itemcode [Kode], i.itemdesc [Nama], rd.rabdtl2qty [DEC_Qty], rd.rabdtl2price [DEC_Harga], rd.rabdtl2discamt [DEC_Diskon], rd.rabdtl2amt [DEC_DPP], rd.rabdtl2taxamt [DEC_PPN], rd.rabdtl2netto [DEC_Total], rd.rabdtl2qtybooking [DEC_Qty_Booking], rd.rabdtl2amtbooking [DEC_Total_Booking] FROM QL_trnrabdtl2 rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemdtl2oid INNER JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl2oid WHERE rd.rabmstoid=" + id + "";

                sSqlDtl3 = "SELECT rd.rabdtl3seq [No], i.jasacode [Kode], i.jasadesc [Nama], rd.rabdtl3qty [DEC_Qty], rd.rabdtl3price [DEC_Harga], rd.rabdtl3amt [DEC_DPP], rd.rabdtl3taxamt [DEC_PPN], rd.rabdtl3netto [DEC_Netto] FROM QL_trnrabdtl3 rd INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemdtl3oid WHERE rd.rabmstoid=" + id + "";

                sSqlDtl4 = "SELECT rd.rabdtl4seq [No], s.suppname [Supplier], i.jasacode [Kode], i.jasadesc [Nama], rd.rabdtl4qty [DEC_Qty], rd.rabdtl4price [DEC_Harga], rd.rabdtl4discamt [DEC_Diskon], rd.rabdtl4amt [DEC_DPP], rd.rabdtl4taxamt [DEC_PPN], rd.rabdtl4pphamt [DEC_PPH], rd.rabdtl4netto [DEC_Netto] FROM QL_trnrabdtl4 rd INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemdtl4oid INNER JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl4oid WHERE rd.rabmstoid=" + id + "";

                sSqlDtl5 = "SELECT rd.rabdtl5seq [No], i.acctgcode [Kode], i.acctgdesc [Nama], rd.rabdtl5price [DEC_Total] FROM QL_trnrabdtl5 rd INNER JOIN QL_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.rabmstoid=" + id + "";

                sSqlFtr = "SELECT rm.rabgrandtotaljualamt [Total Penjualan], rm.rabgrandtotalbeliamt [Total Pembelian] /*, rm.rabgrandtotaljualjasaamt [Total Penjualan Jasa]*/, rm.rabgrandtotalbelijasaamt [Total Pembelian Jasa], rm.rabgrandtotalcostamt [Total Biaya], rm.rabpersenmargin [Margin (%)], rm.rabtotalmargin [Total Margin] FROM QL_trnrabmst rm WHERE rm.rabmstoid=" + id + "";

            } else if (tblname == "QL_trnrabmst_hist")
            {
                sSqlHdr = "SELECT h.rabmstoid_hist [Draft No.], CONVERT(VARCHAR(10), h.rabdate, 103) [Tgl RAB], h.rabtype [Tipe RAB], h.rabtype2 [Jenis RAB], custname [Customer], h.projectname [Project], ISNULL((SELECT cr.currcode FROM QL_mstcurr cr WHERE cr.curroid=h.curroid),'') [Mata Uang], rabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnrabmst_hist h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.rabmstoid_hist INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.rabmstoid_hist=" + id + " ORDER BY rabdate";

                sSqlDtl = "SELECT rd.rabdtl2seq [No], ISNULL((SELECT s.suppname FROM QL_mstsupp s INNER JOIN QL_trnrabdtl2 rd2 ON s.suppoid=rd2.suppdtl2oid WHERE rd2.rabdtl2oid=rd.rabdtl2oid),'') [Supplier_Awal], ISNULL((SELECT s.suppname FROM QL_mstsupp s WHERE s.suppoid=rd.suppdtl2oid),'') [Supplier], i.itemcode [Kode], i.itemdesc [Nama], rd.rabdtl2qty [DEC_Qty], ISNULL((SELECT rd2.rabdtl2price FROM QL_trnrabdtl2 rd2 WHERE rd2.rabdtl2oid=rd.rabdtl2oid),0.0) [DEC_Harga_Awal], rd.rabdtl2price [DEC_Harga], rd.rabdtl2discamt [DEC_Diskon], rd.rabdtl2amt [DEC_DPP], rd.rabdtl2taxamt [DEC_PPN], rd.rabdtl2netto [DEC_Total] FROM QL_trnrabdtl2_hist rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemdtl2oid WHERE rd.rabmstoid_hist=" + id + "";

                sSqlFtr = "SELECT SUM(rd.rabdtl2amt) [Total DPP], SUM(rd.rabdtl2taxamt) [Total PPN], SUM(rd.rabdtl2netto) [Total], rm.rabgrandtotalcostamt [Total Biaya], rm.rabpersenmargin [Margin (%)], rm.rabtotalmargin [Total Margin] FROM QL_trnrabdtl2_hist rd INNER JOIN QL_trnrabmst_hist rm ON rm.rabmstoid=rd.rabmstoid WHERE rd.rabmstoid_hist=" + id + " GROUP BY rd.rabmstoid, rm.rabgrandtotalcostamt, rm.rabpersenmargin, rm.rabtotalmargin";

            }
            else if (tblname == "QL_trnrabmst_hist2")
            {
                sSqlHdr = "SELECT h.rabmstoid_hist [Draft No.], CONVERT(VARCHAR(10), h.rabdate, 103) [Tgl RAB], h.rabtype [Tipe RAB], h.rabtype2 [Jenis RAB], custname [Customer], h.projectname [Project], ISNULL((SELECT cr.currcode FROM QL_mstcurr cr WHERE cr.curroid=h.curroid),'') [Mata Uang], rabmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnrabmst_hist h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.rabmstoid_hist INNER JOIN QL_mstcust d ON d.custoid=h.custoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.rabmstoid_hist=" + id + " ORDER BY rabdate";

                sSqlDtl = "SELECT rd.rabdtl5seq [No], ISNULL((SELECT s.acctgcode FROM QL_mstacctg s INNER JOIN QL_trnrabdtl5 rd2 ON s.acctgoid=rd2.acctgoid WHERE rd2.rabdtl5oid=rd.rabdtl5oid),'') [Kode_Awal], i.acctgcode [Kode], ISNULL((SELECT s.acctgdesc FROM QL_mstacctg s INNER JOIN QL_trnrabdtl5 rd2 ON s.acctgoid=rd2.acctgoid WHERE rd2.rabdtl5oid=rd.rabdtl5oid),'') [Nama_Awal], i.acctgdesc [Nama], ISNULL((SELECT rd2.rabdtl5price FROM QL_trnrabdtl5 rd2 WHERE rd2.rabdtl5oid=rd.rabdtl5oid),0.0) [DEC_Total_Awal], rd.rabdtl5price [DEC_Total] FROM QL_trnrabdtl5_hist rd INNER JOIN QL_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.rabmstoid_hist=" + id + "";

                sSqlFtr = "SELECT SUM(rd.rabdtl2amt) [Total DPP], SUM(rd.rabdtl2taxamt) [Total PPN], SUM(rd.rabdtl2netto) [Total], rm.rabgrandtotalcostamt [Total Biaya], rm.rabpersenmargin [Margin (%)], rm.rabtotalmargin [Total Margin] FROM QL_trnrabdtl2_hist rd INNER JOIN QL_trnrabmst_hist rm ON rm.rabmstoid=rd.rabmstoid WHERE rd.rabmstoid_hist=" + id + " GROUP BY rd.rabmstoid, rm.rabgrandtotalcostamt, rm.rabpersenmargin, rm.rabtotalmargin";

            }
            else if (tblname == "QL_trnpoitemmst")
            {
                sSqlHdr = "SELECT h.cmpcode, h.poitemmstoid [Draft No.], CONVERT(VARCHAR(10), h.poitemdate, 103) [Tgl PO], (SELECT rabno FROM QL_trnrabmst where rabmstoid = h.rabmstoid) [RAB No.], ISNULL((SELECT rabtype FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Tipe RAB], (SELECT soitemno FROM QL_trnsoitemmst where soitemmstoid = h.somstoid) [SO No.], suppname [Supplier], ISNULL((SELECT TOP 1 g.gndesc FROM QL_m05GN g INNER JOIN QL_trnpoitemdtl pd ON pd.poitemdtllocoid=g.gnoid WHERE pd.poitemmstoid=h.poitemmstoid),'') [Kirim Ke], (SELECT projectname FROM QL_trnrabmst where rabmstoid = h.rabmstoid) [Project], poitemmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnpoitemmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.poitemmstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.poitemmstoid = "+ id + " AND h.poitemtype NOT IN ('Jasa') ORDER BY poitemdate";

                sSqlDtl = "SELECT rd.poitemdtlseq [No], i.itemcode [Kode], i.itemdesc [Nama], rd.poitemqty [DEC_Qty], rd.poitemprice [DEC_Harga], rd.poitemdtldiscamt [DEC_Diskon], rd.poitemdtlamt [DEC_DPP], rd.poitemdtltaxamt [DEC_PPN], rd.poitemdtlnetto [DEC_Netto] FROM QL_trnpoitemdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid WHERE rd.poitemmstoid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.poitemdtlamt) [Total DPP], SUM(rd.poitemdtltaxamt) [Total PPN], SUM(rd.poitemdtlnetto) [Total Netto] FROM QL_trnpoitemdtl rd WHERE rd.poitemmstoid=" + id + "";

            } else if (tblname == "QL_trnpojasamst")
            {
                sSqlHdr = "SELECT h.cmpcode, h.poitemmstoid [Draft No.], CONVERT(VARCHAR(10), h.poitemdate, 103) [Tgl PO], iSNULL((SELECT rabno FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [RAB No.], ISNULL((SELECT rabtype FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Tipe RAB], suppname [Supplier], ISNULL((SELECT TOP 1 g.gndesc FROM QL_m05GN g INNER JOIN QL_trnpoitemdtl pd ON pd.poitemdtllocoid=g.gnoid WHERE pd.poitemmstoid=h.poitemmstoid),'') [Kirim Ke], ISNULL((SELECT projectname FROM QL_trnrabmst where rabmstoid = h.rabmstoid),'') [Project], poitemmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnpoitemmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.poitemmstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.poitemmstoid = " + id + " AND h.poitemtype = 'Jasa' ORDER BY poitemdate";

                sSqlDtl = "SELECT rd.poitemdtlseq [No], i.jasacode [Kode], i.jasadesc [Nama], rd.poitemqty [DEC_Qty], rd.poitemprice [DEC_Harga], rd.poitemdtldiscamt [DEC_Diskon], rd.poitemdtlamt [DEC_DPP], rd.poitemdtltaxamt [DEC_PPN], rd.poitemdtlnetto [DEC_Netto] FROM QL_trnpoitemdtl rd INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemoid WHERE rd.poitemmstoid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.poitemdtlamt) [Total DPP], SUM(rd.poitemdtltaxamt) [Total PPN], SUM(rd.poitemdtlnetto) [Total Netto] FROM QL_trnpoitemdtl rd WHERE rd.poitemmstoid=" + id + "";

            } else if (tblname == "QL_trnpoassetmst")
            {
                sSqlHdr = "SELECT h.cmpcode, h.poassetmstoid [Draft No.], h.poassettype [Tipe PO], CONVERT(VARCHAR(10), h.poassetdate, 103) [Tgl PO], suppname [Supplier], ISNULL((SELECT TOP 1 g.gndesc FROM QL_m05GN g INNER JOIN QL_trnpoassetdtl pd ON pd.poassetdtllocoid=g.gnoid WHERE pd.poassetmstoid=h.poassetmstoid),'') [Kirim Ke], poassetmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnpoassetmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.poassetmstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.poassetmstoid = " + id + " ORDER BY poassetdate";

                sSqlDtl = "SELECT rd.poassetdtlseq [No], i.itemcode [Kode], i.itemdesc [Nama], rd.poassetqty [DEC_Qty], rd.poassetprice [DEC_Harga], rd.poassetdtldiscamt [DEC_Diskon], rd.poassetdtlamt [DEC_DPP], rd.poassetdtltaxamt [DEC_PPN], rd.poassetdtlnetto [DEC_Netto] FROM QL_trnpoassetdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.poassetrefoid WHERE rd.poassetmstoid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.poassetdtlamt) [Total DPP], SUM(rd.poassetdtltaxamt) [Total PPN], SUM(rd.poassetdtlnetto) [Total Netto] FROM QL_trnpoassetdtl rd WHERE rd.poassetmstoid=" + id + "";

            }
            else if (tblname == "QL_mstcusthist")
            {
                sSqlHdr = "SELECT h.cmpcode, ch.custhistoid [Draft No.], custcode [Kode], custname [Nama], ch.custcreditlimitold [Credit Limit Lama], ch.custcreditlimitnew [Credit Limit Baru], custnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_mstcust h INNER JOIN QL_mstcusthist ch ON ch.custoid = h.custoid INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=ch.custhistoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND ch.custhistoid = "+ id +" ORDER BY a.reqdate";

                sSqlDtl = "SELECT '' [No Detail]";

                sSqlFtr = "SELECT '' [No Footer]";

            }
            else if (tblname == "QL_trnstockadj")
            {
                sSqlHdr = "SELECT DISTINCT CAST(resfield1 AS INTEGER) [Draft No.], CONVERT(VARCHAR(10), stockadjdate, 103) [Adj Date], stockadjtype [Type], stockadjmstnote [Header Note] FROM QL_trnstockadj h WHERE CAST(resfield1 AS INTEGER)=" + id + "";

                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                sSqlDtl = "SELECT 0 [No], '' dataall, a.resfield1 [mstoid], a.stockadjoid [dtloid], a.refoid [itemoid], a.refno [refno], a.mtrwhoid [whoid], ISNULL(a.rabmstoid,0) [rabmstoid], (SELECT m.itemcode FROM QL_mstitem m WHERE m.itemoid=a.refoid) AS [Kode], (SELECT m.itemdesc FROM QL_mstitem m WHERE m.itemoid=a.refoid)  AS [Deskripsi], ISNULL(a.serialnumber,'') [serialnumber], g1.gndesc AS [Gudang], a.stockadjqtybefore AS [DEC_Stock], a.stockadjqty AS [DEC_Selisih], a.stockadjqtyafter AS [DEC_Opname], g2.gndesc AS [Satuan], /*(CASE stockadjtype WHEN 'ADJUSTMENT' THEN ISNULL((SELECT TOP 1 valueidr / (qtyin+qtyout) FROM QL_conmat con WHERE con.cmpcode=a.cmpcode AND con.refoid=a.refoid ORDER BY updtime DESC), 0.0) ELSE 0.0 END)*/ stockadjamtidr AS [Hpp], stockadjnote [Catatan] FROM QL_trnstockadj a INNER JOIN QL_m05GN g1 ON g1.gnoid=a.mtrwhoid INNER JOIN QL_m05GN g2 ON g2.gnoid=a.stockadjunit WHERE CAST(a.resfield1 AS INTEGER)=" + id + " ORDER BY a.stockadjoid";

                sSqlFtr = "SELECT '' [No Footer]";
            }
            else if (tblname == "QL_trnkasbon2mst")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.kasbon2mstoid [Draft No.], CONVERT(VARCHAR(10), h.kasbon2date, 103) [Tgl Kasbon Realisasi], ISNULL((SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = ISNULL(h.rabmstoid,0)),'') [Project], suppname [Supplier], h.kasbon2note [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnkasbon2mst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.kasbon2mstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.kasbon2mstoid = " + id + " ";

                sSqlDtl = "SELECT rd.kasbon2dtlseq [No], i.acctgcode + ' - ' + i.acctgdesc [Akun], rd.kasbon2dtlamt [DEC_Amount], CASE WHEN rd.pphcreditdtlamt>0 THEN ISNULL((SELECT ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=rd.pphcreditacctgdtloid),'') ELSE '' END [PPH_Credit], rd.pphcreditdtlamt [DEC_PPH_Amount], rd.kasbon2dtlnote [Detail Note] FROM QL_trnkasbon2dtl rd INNER JOIN QL_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.kasbon2mstoid=" + id + "";

                sSqlFtr = "SELECT rd.pphdebetamt [Total PPH Debet Amount], rd.kasbon2amt [Total DPP], rd.kasbon2taxamt [Total PPN], rd.kasbon2amtnetto [Total Netto] FROM QL_trnkasbon2mst rd WHERE rd.kasbon2mstoid=" + id + "";

            }
            else if (tblname == "QL_trnapdirmst")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.apdirmstoid [Draft No.], CONVERT(VARCHAR(10), h.apdirdate, 103) [Tgl AP Direct], ISNULL((SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = ISNULL(h.rabmstoid,0)),'') [Project], suppname [Supplier], h.apdirmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnapdirmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.apdirmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid INNER JOIN QL_mstsupp d ON d.suppoid=h.suppoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.apdirmstoid = " + id + " ";

                sSqlDtl = "SELECT rd.apdirdtlseq [No], i.acctgcode + ' - ' + i.acctgdesc [Akun], rd.apdirprice [DEC_Amount], rd.apdirdtlnote [Detail Note] FROM QL_trnapdirdtl rd INNER JOIN QL_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.apdirmstoid=" + id + "";

                sSqlFtr = "SELECT rd.pphamt [Total PPH Amount], rd.apdirtotalnetto [Total DPP], rd.apdirtaxamt [Total PPN], rd.apdirgrandtotal [Total Netto] FROM QL_trnapdirmst rd WHERE rd.apdirmstoid=" + id + "";

            }
            else if (tblname == "QL_trnglmst")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.glmstoid [Draft No.], ISNULL(h.glother1,'') [No. JU], CONVERT(VARCHAR(10), h.gldate, 103) [Tgl JU], h.glnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnglmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.glmstoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.glmstoid = " + id + " ";

                sSqlDtl = "SELECT rd.glseq [No], i.acctgcode + ' - ' + i.acctgdesc [Akun], CASE WHEN gldbcr ='D' THEN rd.glamt ELSE 0.0 END [DEC_Debet], CASE WHEN gldbcr ='C' THEN rd.glamt ELSE 0.0 END [DEC_Credit], rd.glnote [Detail Note] FROM QL_trngldtl rd INNER JOIN QL_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.glmstoid=" + id + "";

                sSqlFtr = "SELECT '' [No Footer]";

            }
            else if (tblname == "QL_trndebetnote")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.dnoid [Draft No.], CONVERT(VARCHAR(10), h.dndate, 103) [Tgl DN], h.dnnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trndebetnote h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.dnoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.dnoid = " + id + " ";

                sSqlDtl = "SELECT tipe [Tipe], suppcustname [Supplier/Customer], aparno [FB/FJ], dnamt [Amount], dnnote [Note] FROM (" +
                "SELECT dn.cmpcode,'" + CompnyName + "' divname,dn.dnoid,dn.dnno, dn.dnnote,dn.dndate,'FB' AS tipe,s.suppname AS suppcustname," +
                "CASE dn.reftype WHEN 'QL_trnapitemmst' THEN(SELECT apit.apitemno FROM QL_trnapitemmst apit WHERE apit.cmpcode = dn.cmpcode AND apit.apitemmstoid = dn.refoid) WHEN 'QL_trnapdirmst' THEN(SELECT apit.apdirno FROM QL_trnapdirmst apit WHERE apit.cmpcode = dn.cmpcode AND apit.apdirmstoid = dn.refoid) ELSE(SELECT apit.apassetno FROM QL_trnapassetmst apit WHERE apit.cmpcode = dn.cmpcode AND apit.apassetmstoid = dn.refoid)" +
                "END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue, CASE WHEN dnstatus = 'Revised' THEN revisereason WHEN dnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
                "FROM QL_trndebetnote dn " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid " +
                "WHERE left(dn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT dn.cmpcode,'" + CompnyName + "' divname,dn.dnoid,dn.dnno, dn.dnnote,dn.dndate,'FJ' AS tipe,c.custname AS suppcustname," +
                "CASE dn.reftype WHEN 'QL_trnaritemmst' THEN (SELECT arit.aritemno FROM QL_trnaritemmst arit WHERE arit.cmpcode=dn.cmpcode AND arit.aritemmstoid=dn.refoid) ELSE (SELECT arit.arassetno FROM QL_trnarassetmst arit WHERE arit.cmpcode=dn.cmpcode AND arit.arassetmstoid=dn.refoid)" +
                "END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue, CASE WHEN dnstatus = 'Revised' THEN revisereason WHEN dnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
                "FROM QL_trndebetnote dn " +
                "INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid " +
                "WHERE left(dn.reftype,8)='ql_trnar' AND left(dn.reftype,11)<>'ql_trnarret' " +
                ") AS a WHERE a.dnoid=" + id + "";

                sSqlFtr = "SELECT '' [No Footer]";

            }
            else if (tblname == "QL_trncreditnote")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.cnoid [Draft No.], CONVERT(VARCHAR(10), h.cndate, 103) [Tgl CN], h.cnnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trncreditnote h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.cnoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.cnoid = " + id + " ";

                sSqlDtl = "SELECT tipe [Tipe], suppcustname [Supplier/Customer], aparno [FB/FJ], cnamt [Amount], cnnote [Note] FROM (" +
                "SELECT cn.cmpcode,'" + CompnyName + "' divname,cn.cnoid,cn.cnno, cn.cnnote,cn.cndate,'FB' AS tipe,s.suppname AS suppcustname," +
                "CASE cn.reftype WHEN 'QL_trnapitemmst' THEN (SELECT apit.apitemno FROM QL_trnapitemmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apitemmstoid=cn.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apit.apdirno FROM QL_trnapdirmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apdirmstoid=cn.refoid) ELSE (SELECT apit.apassetno FROM QL_trnapassetmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apassetmstoid=cn.refoid) " +
                "END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue, CASE WHEN cnstatus = 'Revised' THEN revisereason WHEN cnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
                "FROM QL_trncreditnote cn " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid " +
                "WHERE left(cn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT cn.cmpcode,'" + CompnyName + "' divname,cn.cnoid,cn.cnno, cn.cnnote,cn.cndate,'FJ' AS tipe,c.custname AS suppcustname," +
                "CASE cn.reftype WHEN 'QL_trnaritemmst' THEN (SELECT arit.aritemno FROM QL_trnaritemmst arit WHERE arit.cmpcode=cn.cmpcode AND arit.aritemmstoid=cn.refoid) ELSE (SELECT arit.arassetno FROM QL_trnarassetmst arit WHERE arit.cmpcode=cn.cmpcode AND arit.arassetmstoid=cn.refoid) " +
                "END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue, CASE WHEN cnstatus = 'Revised' THEN revisereason WHEN cnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
                "FROM QL_trncreditnote cn " +
                "INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid " +
                "WHERE left(cn.reftype,8)='ql_trnar' AND left(cn.reftype,11)<>'ql_trnarret' " +
                ") AS a WHERE a.cnoid=" + id + "";

                sSqlFtr = "SELECT '' [No Footer]";

            }
            else if (tblname == "QL_trnrabreal")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.cashbankoid [Draft No.], CONVERT(VARCHAR(10), h.cashbankdate, 103) [Tgl Realisasi], ISNULL((SELECT projectname FROM QL_trnrabmst rm where rm.rabmstoid = ISNULL(h.rabmstoid,0)),'') [Project], ISNULL((SELECT a.acctgdesc FROM QL_trnrabdtl5 rm inner join QL_mstacctg a on a.acctgoid=rm.acctgoid where rm.rabdtl5oid = ISNULL(h.rabdtl5oid,0)),'') [Biaya RAB], h.cashbanknote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trncashbankmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.cashbankoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.cashbankoid = " + id + " ";

                sSqlDtl = "SELECT cashbankglseq [No], i.acctgcode + ' - ' + i.acctgdesc [Akun], rd.cashbankglamt [DEC_Amount], rd.cashbankglnote [Detail Note] FROM QL_trncashbankgl rd INNER JOIN QL_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.cashbankoid=" + id + "";

                sSqlFtr = "SELECT ISNULL(rd.pphamt,0.0) [Total PPH Amount], rd.cashbankdpp [Total DPP], rd.cashbankothertaxamt [Total PPN], rd.cashbankamt [Total Netto] FROM QL_trncashbankmst rd WHERE rd.cashbankoid=" + id + "";

            }
            else if (tblname == "QL_trnrabrealmulti")
            {
                sSqlHdr = "SELECT DISTINCT h.cmpcode, h.cashbankoid [Draft No.], CONVERT(VARCHAR(10), h.cashbankdate, 103) [Tgl Realisasi], CASE h.cost_type WHEN 'KASBON REAL' THEN 'REALISASI KASBON' ELSE h.cost_type END [Cost Type], h.cashbanknote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trncashbankmst h INNER JOIN QL_APP a ON a.cmpcode=h.cmpcode AND a.appformoid=h.cashbankoid INNER JOIN QL_m01US us ON us.stoid=a.appstoid WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.cashbankoid = " + id + " ";

                sSqlDtl = "SELECT cashbankglseq [No], projectname [Project], soitemno [No SO], i.acctgcode + ' - ' + i.acctgdesc [Akun], cbgl.cashbankglamt [DEC_Amount], ISNULL((SELECT ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=ISNULL(cbgl.pphcreditacctgoid,0)),'') [PPH Credit], ISNULL(cbgl.pphcreditamt,0.0) [DEC_PPH Amount], cbgl.cashbankglnote [Detail Note] FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg i ON i.acctgoid=cbgl.acctgoid INNER JOIN QL_trnrabdtl5 rd5 ON rd5.rabdtl5oid=cbgl.rabdtl5oid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=cbgl.rabmstoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=cbgl.rabmstoid WHERE cbgl.cashbankoid=" + id + " AND ISNULL(cbgl.rabdtl5oid,0)<>0  UNION ALL  SELECT cashbankglseq [No], '' [Project], '' [No SO], i.acctgcode + ' - ' + i.acctgdesc [Akun], cbgl.cashbankglamt [DEC_Amount], ISNULL((SELECT ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=ISNULL(cbgl.pphcreditacctgoid,0)),'') [PPH Credit], ISNULL(cbgl.pphcreditamt,0.0) [DEC_PPH Amount], cbgl.cashbankglnote [Detail Note] FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg i ON i.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND ISNULL(cbgl.rabdtl5oid,0)=0";

                sSqlFtr = "SELECT ISNULL(rd.pphamt,0.0) [Total PPH Amount], rd.cashbankdpp [Total DPP], rd.cashbankothertaxamt [Total PPN], rd.cashbankamt [Total Netto] FROM QL_trncashbankmst rd WHERE rd.cashbankoid=" + id + "";

            }
            else
                msg = "Approval action haven't be set";

            if (msg == "")
            {
                // Process Data for Header
                DataTable tblhdr = new ClassConnection().GetDataTable(sSqlHdr, tblname + "_hdr");
                List<modelforapp> rowshdr = new List<modelforapp>();
                if (tblhdr.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblhdr.Rows)
                    {
                        foreach (DataColumn col in tblhdr.Columns)
                        {
                            var item = dr[col].ToString();
                            rowshdr.Add(new modelforapp()
                            {
                                caption = col.ColumnName,
                                value = item
                            });
                        }
                    }
                }
                else
                    msg = "Data Header Not Found";

                // Process Data for Detail
                DataTable tbldtl = new ClassConnection().GetDataTable(sSqlDtl, tblname + "_dtl");
                List<string> colsdtl = new List<string>();
                List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
                if (msg == "" && tbldtl.Rows.Count > 0)
                {
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbldtl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        Int32 i = 0;
                        foreach (DataColumn col in tbldtl.Columns)
                        {
                            i++;
                            var item = dr[col].ToString();
                            decimal decval = 0;
                            try
                            {
                                //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                if (i != 1 && Decimal.TryParse(item, out decval))
                                {
                                    if (decval == 0)
                                        item = "0.00";
                                    else
                                        item = decval.ToString("#,##0.00");
                                }
                            }
                            catch
                            {
                                item = dr[col].ToString();
                            }
                            row.Add(col.ColumnName.Replace("DEC_", ""), item);
                            if (!colsdtl.Contains(col.ColumnName.Replace("DEC_", "")))
                                colsdtl.Add(col.ColumnName.Replace("DEC_", ""));
                        }
                        rowsdtl.Add(row);
                    }
                }
                else
                    msg = "Data Detail Not Found";

                // Process Data for Detail 2
                DataTable tbldtl2 = null;
                List<string> colsdtl2 = null;
                List<Dictionary<string, object>> rowsdtl2 = null;

                if (sSqlDtl2 != "")
                {
                    tbldtl2 = new ClassConnection().GetDataTable(sSqlDtl2, tblname + "_dtl2");
                    colsdtl2 = new List<string>();
                    rowsdtl2 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl2.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl2.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl2.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl2.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl2.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl2.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Detail 3
                DataTable tbldtl3 = null;
                List<string> colsdtl3 = null;
                List<Dictionary<string, object>> rowsdtl3 = null;

                if (sSqlDtl3 != "")
                {
                    tbldtl3 = new ClassConnection().GetDataTable(sSqlDtl3, tblname + "_dtl3");
                    colsdtl3 = new List<string>();
                    rowsdtl3 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl3.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl3.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl3.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl3.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl3.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl3.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Detail 4
                DataTable tbldtl4 = null;
                List<string> colsdtl4 = null;
                List<Dictionary<string, object>> rowsdtl4 = null;

                if (sSqlDtl4 != "")
                {
                    tbldtl4 = new ClassConnection().GetDataTable(sSqlDtl4, tblname + "_dtl4");
                    colsdtl4 = new List<string>();
                    rowsdtl4 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl4.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl4.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl4.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl4.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl4.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl4.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Detail 5
                DataTable tbldtl5 = null;
                List<string> colsdtl5 = null;
                List<Dictionary<string, object>> rowsdtl5 = null;

                if (sSqlDtl5 != "")
                {
                    tbldtl5 = new ClassConnection().GetDataTable(sSqlDtl5, tblname + "_dtl5");
                    colsdtl5 = new List<string>();
                    rowsdtl5 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl5.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl5.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl5.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl5.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl5.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl5.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Footer
                DataTable tblftr = new ClassConnection().GetDataTable(sSqlFtr, tblname + "_ftr");
                List<modelforapp> rowsftr = new List<modelforapp>();
                if (msg == "" && tblftr.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblftr.Rows)
                    {
                        foreach (DataColumn col in tblftr.Columns)
                        {
                            var item = dr[col].ToString();
                            decimal decval = 0;
                            if (!string.IsNullOrEmpty(item) && Decimal.TryParse(item, out decval))
                            {
                                if (Decimal.Parse(item) > 999)
                                {
                                    item = decval.ToString("0,0.00");
                                }
                                else if (Decimal.Parse(item) < (-999))
                                {
                                    item = decval.ToString("0,0.00");
                                }
                                else
                                {
                                    item = decval.ToString("0.00");
                                }
                            }
                            if (col.ColumnName != "No Footer")
                            {
                                rowsftr.Add(new modelforapp()
                                {
                                    caption = col.ColumnName,
                                    value = item
                                });
                            }
                        }
                    }
                }

                if (msg == "")
                    return Json(new { msg, info, rowshdr, colsdtl, rowsdtl, colsdtl2, rowsdtl2, colsdtl3, rowsdtl3, colsdtl4, rowsdtl4, colsdtl5, rowsdtl5, rowsftr }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: WaitingAction/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(waitactmodel tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var transno = "";
            if (action == "Approved" && string.IsNullOrEmpty(transno))
            {
                var prefix = ""; string transnofld = "";
                if (tbl.tblname == "QL_trnreqrabmst")
                {
                    prefix = "PRAB"; transnofld = "reqrabno";

                } else if (tbl.tblname == "QL_trnrabmst")
                {
                    prefix = "RAB"; transnofld = "rabno";

                }
                else if (tbl.tblname == "QL_trnpoitemmst")
                {
                    prefix = "PO"; transnofld = "poitemno";

                }
                else if (tbl.tblname == "QL_trnpojasamst")
                {
                    prefix = "POJ"; transnofld = "poitemno";
                }
                else if (tbl.tblname == "QL_trnpoassetmst")
                {
                    prefix = "POA"; transnofld = "poassetno";
                }
                else if (tbl.tblname == "QL_mstcusthist")
                {
                    prefix = "CH"; transnofld = "custhistcode";

                }
                else if (tbl.tblname == "QL_trnstockadj")
                {
                    prefix = "ADJ"; transnofld = "stockadjno";

                }
                else if (tbl.tblname == "QL_trnkasbon2mst")
                {
                    prefix = "KBN"; transnofld = "kasbon2no";

                }
                else if (tbl.tblname == "QL_trnapdirmst")
                {
                    prefix = "APD"; transnofld = "apdirno";

                }
                else if (tbl.tblname == "QL_trnglmst")
                {
                    prefix = "MJ"; transnofld = "glother1";

                }
                else if (tbl.tblname == "QL_trndebetnote")
                {
                    prefix = "DN"; transnofld = "dnno";

                }
                else if (tbl.tblname == "QL_trncreditnote")
                {
                    prefix = "CN"; transnofld = "cnno";

                }

                if (tbl.tblname != "QL_trnrabmst_hist" && tbl.tblname != "QL_trnrabmst_hist2" && tbl.tblname != "QL_trnrabreal" && tbl.tblname != "QL_trnrabrealmulti")
                {
                    transno = GenerateTransNo(prefix, CompnyCode, tbl.tblname, transnofld);
                }
            }

            var error = "";
            if (tbl.tblname == "QL_trnreqrabmst")
            {
                ApprovalPRAB(tbl.mstoid, CompnyCode, transno, action, out error);

            } else if (tbl.tblname == "QL_trnrabmst")
            {
                ApprovalRAB(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnrabmst_hist")
            {
                ApprovalRABRevisiPO(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnrabmst_hist2")
            {
                ApprovalRABRevisiBiaya(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnpoitemmst")
            {
                ApprovalPO(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnpojasamst")
            {
                ApprovalPOJasa(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnpoassetmst")
            {
                ApprovalPOAsset(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_mstcusthist")
            {
                ApprovalCust(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnstockadj")
            {
                ApprovalStockAdj(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnkasbon2mst")
            {
                ApprovalKasbon2Real(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnapdirmst")
            {
                ApprovalAPDir(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnglmst")
            {
                ApprovalJurnalUmum(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trndebetnote")
            {
                ApprovalDN(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trncreditnote")
            {
                ApprovalCN(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnrabreal")
            {
                ApprovalRABReal(tbl.mstoid, CompnyCode, transno, action, out error);

            }
            else if (tbl.tblname == "QL_trnrabrealmulti")
            {
                ApprovalRABRealMulti(tbl.mstoid, CompnyCode, transno, action, out error);

            }

            if (string.IsNullOrEmpty(error))
                return RedirectToAction("Index/" + tbl.tblname);

            ModelState.AddModelError("", error);
            return View(tbl);
        }

        public class stockadj
        {
            public int stockadjoid { get; set; }
            public int resfield1 { get; set; }
            public int itemoid { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int mtrwhoid { get; set; }
            public decimal stockadjqty { get; set; }
            public decimal stockvalue { get; set; }
            public decimal stockvalue_new { get; set; }
            public int rabmstoid { get; set; }
        }

        public class listapitemdtl2
        {
            public int apitemdtl2seq { get; set; }
            public int fabelioid { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
            public decimal apitemdtl2amt { get; set; }
        }

        public class apardata
        {
            public string cmpcode { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public int acctgoid { get; set; }
            public string acctgaccount { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public string transcheckdate { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public decimal amttrans { get; set; }
            public decimal amttransidr { get; set; }
            public decimal amttransusd { get; set; }
            public decimal amtbayar { get; set; }
            public decimal amtbayaridr { get; set; }
            public decimal amtbayarusd { get; set; }
            public decimal amtbalance { get; set; }
            public decimal amtbalanceidr { get; set; }
            public decimal amtbalanceusd { get; set; }
            public decimal aparpaidamt { get; set; }
            public decimal aparpaidamtidr { get; set; }
            public decimal aparpaidamtusd { get; set; }
            public decimal aparbalance { get; set; }
            public decimal aparbalanceidr { get; set; }
            public decimal aparbalanceusd { get; set; }
            public decimal aparamt { get; set; }
            public decimal aparamtidr { get; set; }
            public decimal aparamtusd { get; set; }
            public decimal amtdncn { get; set; }
            public decimal amtdncnidr { get; set; }
            public decimal amtdncnusd { get; set; }
            public DateTime apardate { get; set; }
            public DateTime aparcheckdate { get; set; }
            public string cntype { get; set; }
            public string aparno { get; set; }
            public string suppcustname { get; set; }
            public string dbacctgdesc { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        private int GenPID(DateTime tanggal)
        {
            string sNo = tanggal.ToString("yy") + "-" + tanggal.ToString("MM") + "-";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND refno LIKE '" + sNo + "%'";
            var counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            return counter;
        }

        private int GenPIDNew()
        {
            var str = ClassFunction.GetServerTime().ToString("yy") + "-"; int counter = 1;
            sSql = "SELECT COUNT(-1) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'' ) LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "'";
            var cek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (cek > 0)
            {
                sSql = "SELECT ISNULL((SELECT TOP 1 CAST(REPLACE(refno,'" + str + "','') AS INTEGER) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'')<>'' AND ISNULL(refno,'') LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "' ORDER BY CAST(REPLACE(refno,'" + str + "','') AS INTEGER) DESC),0) + 1 AS IDNEW";
                counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            return counter;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<stockadj> dtDtl)
        {
            Session["QL_trnstockadj"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string GenerateTransNo(string prefix, string cmp, string tblname, string transnofld)
        {
            string sWhere = "";
            if (tblname == "QL_trnpojasamst")
            {
                tblname = "QL_trnpoitemmst";
            }
            if (tblname == "QL_trnrabmst")
            {
                sWhere = " AND revrabtype='Baru' ";
            }
            if (tblname == "QL_trnpoitemmst")
            {
                sWhere = " AND revpoitemtype='Baru' ";
            }
            int formatCounter = Convert.ToInt32(DefaultCounter);
            DateTime tanggal = ClassFunction.GetServerTime();
            var sNo = prefix + "/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            var ln = sNo.Length + formatCounter;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(" + transnofld + ","+ ln +"), "+ formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM " + tblname + " WHERE " + transnofld + " LIKE '" + sNo + "%' "+ sWhere + "";
            sNo = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            return sNo;
        }

        #region Approval List
        private void ApprovalPRAB(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "PRAB";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnreqrabmst tbl = db.QL_trnreqrabmst.Find(CompnyCode, id);
            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Approved")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.reqrabdate.Value;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnreqrabmst.Where(x => x.reqrabmstoid == tbl.reqrabmstoid && x.reqrabmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Approved")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnreqrabmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnreqrabmst SET reqrabno='" + transno + "', reqrabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + " " + sqlPlus + " WHERE reqrabmststatus='In Approval' AND reqrabmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.reqrabmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnreqrabmst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } 
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnreqrabmst SET reqrabno='" + transno + "', reqrabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + " " + sqlPlus + " WHERE reqrabmststatus='In Approval' AND reqrabmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }   
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalRAB(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sooid = ClassFunction.GenerateID("QL_trnsoitemmst");
            var sooiddtl = ClassFunction.GenerateID("QL_trnsoitemdtl");

            string ctrlname = "RAB";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnrabmst rabmst = db.QL_trnrabmst.Find(CompnyCode, id);

            List<listprabdtl> rabdtl = new List<listprabdtl>();
            sSql = "SELECT * FROM QL_trnrabdtl where rabmstoid = " + id + "";
            rabdtl = db.Database.SqlQuery<listprabdtl>(sSql).ToList();

            string sNoSO = ""; int revsoitemmstoid = 0;
            string revsomstno = "";
            if (rabmst.revrabtype != "Baru")
            {
                sSql = "SELECT TOP 1 soitemno FROM QL_trnsoitemmst where soitemtype='' AND rabmstoid = " + rabmst.revrabmstoid + "";
                revsomstno = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                if (!string.IsNullOrEmpty(revsomstno))
                {
                    sSql = "SELECT soitemmstoid FROM QL_trnsoitemmst where soitemtype='' AND soitemno = '" + revsomstno + "'";
                    revsoitemmstoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    sNoSO = ClassFunction.Left(revsomstno, 14) + "-R";
                    sSql = "SELECT ISNULL(MAX(CAST(REPLACE(soitemno, '" + sNoSO + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND soitemno LIKE '" + sNoSO + "%'";
                    string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                    sNoSO = sNoSO + sCounter;
                }
            }

            decimal soitemtotalamt = rabdtl.Sum(x => x.rabqty * x.rabprice);
            decimal soitemtotaltaxamt = rabdtl.Sum(x => x.rabtaxamt);
            decimal soitemtotaltaxamtidr = soitemtotaltaxamt;
            decimal totalso = soitemtotalamt + soitemtotaltaxamt;
            decimal soitemtotalnetto = Math.Round(totalso, 0);
            decimal soitemgrandtotalamt = soitemtotalnetto;
            decimal soitemgrandtotalamtidr = soitemtotalnetto;

            List<listprabdtl3> rabdtl3 = new List<listprabdtl3>();
            sSql = "SELECT * FROM QL_trnrabdtl3 where rabmstoid = " + id + "";
            rabdtl3 = db.Database.SqlQuery<listprabdtl3>(sSql).ToList();

            var sojasatotalamt = rabdtl3.Sum(x => x.rabdtl3qty * x.rabdtl3price);
            var sojasatotaltaxamt = rabdtl3.Sum(x => x.rabdtl3taxamt);
            var sojasatotaltaxamtidr = soitemtotaltaxamt;
            var sojasatotalnetto = soitemtotalamt + soitemtotaltaxamt;
            var sojasagrandtotalamt = soitemtotalnetto;
            var sojasagrandtotalamtidr = soitemtotalnetto;

            error = "";
            var sqlPlus = ""; var sqlPlus2 = "";
            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Approved")
            {
                cRate.SetRateValue(rabmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }
                //Generate Nomor Ulang
                int formatCounter = Convert.ToInt32(DefaultCounter);
                DateTime tanggal = rabmst.rabdate.Value;
                var sNo = "RAB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
                var ln = sNo.Length + formatCounter;
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(rabno," + ln + "), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnrabmst WHERE rabno LIKE '" + sNo + "%' AND revrabtype='Baru' ";
                transno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = rabmst.rabdate.Value;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            if (rabmst.rabno == "")
            {
                sqlPlus2 = ", rabno='" + transno + "'";
            }

            string errDup = "";
            if (db.QL_trnrabmst.Where(x => x.rabmstoid == rabmst.rabmstoid && x.rabmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Approved")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                            //Update Booking Stock
                            sSql = "UPDATE QL_matbooking SET flag='" + action + "', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE formoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnrabmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnrabmst SET rabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + " " + sqlPlus + " " + sqlPlus2 + " WHERE rabmststatus='In Approval' AND rabmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = rabmst.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = rabmst.rabmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnrabmst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }   
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnrabmst SET rabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + " " + sqlPlus + " " + sqlPlus2 + " WHERE rabmststatus='In Approval' AND rabmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnreqrabmst SET reqrabmststatus='Closed' WHERE reqrabmststatus='Approved' AND reqrabmstoid IN (SELECT reqrabmstoid FROM QL_trnrabmst where rabmstoid = " + id + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                //if (rabmst.revrabmstoid != 0)
                                //{
                                //    sSql = "UPDATE QL_trnrabmst SET rabmststatus='Closed', rabmstres1='Force Closed' WHERE rabmstoid=" + rabmst.revrabmstoid;
                                //    db.Database.ExecuteSqlCommand(sSql);
                                //    db.SaveChanges();

                                //    sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Closed', soitemmstres1='Force Closed' WHERE rabmstoid=" + rabmst.revrabmstoid;
                                //    db.Database.ExecuteSqlCommand(sSql);
                                //    db.SaveChanges();
                                //}

                                if (action == "Approved")
                                {
                                    sSql = "SELECT COUNT(mrservicemstoid) FROM QL_trnrabmst WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + id;
                                    var cMRservice = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

                                    if (cMRservice > 0)
                                    {
                                        sSql = "UPDATE QL_trnmrservicemst SET mrservicemststatus='Closed' WHERE mrservicemststatus='Post' AND mrservicemstoid IN (SELECT mrservicemstoid FROM QL_trnrabmst where rabmstoid = " + id + ")";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }

                                    //if (rabmst.revrabtype == "Baru")
                                    //{
                                    QL_trnsoitemmst somst;
                                    somst = new QL_trnsoitemmst();

                                    QL_trnsoitemdtl sodtl;
                                    sodtl = new QL_trnsoitemdtl();

                                    QL_trnsoitemmst somst3;
                                    somst3 = new QL_trnsoitemmst();

                                    QL_trnsoitemdtl sodtl3;
                                    sodtl3 = new QL_trnsoitemdtl();

                                    somst.cmpcode = rabmst.cmpcode;
                                    somst.soitemmstoid = sooid;
                                    somst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                    somst.soitemtype = "";
                                    somst.soitemdate = servertime;
                                    somst.soitemno = sNoSO;
                                    somst.rabmstoid = rabmst.rabmstoid;
                                    somst.custoid = rabmst.custoid;
                                    somst.alamatoid = rabmst.alamatoid;
                                    somst.salesoid = rabmst.salesoid;
                                    somst.soitemrefno = "";
                                    somst.soitemcustpo = "";
                                    somst.soitemcustpo = "";
                                    somst.soitemcustpodate = servertime;
                                    somst.soitemcustpodate2 = servertime;
                                    somst.soitemcustdoc = "";
                                    somst.soitempaytypeoid = rabmst.rabpaytermoid;
                                    somst.curroid = rabmst.curroid;
                                    somst.rateoid = 0;
                                    somst.rate2oid = 0;
                                    somst.soitemtotaldiscamt = 0;
                                    somst.soitemtotaldiscamtidr = 0;
                                    somst.soitemtotalpphamt = 0;
                                    somst.soitemtotalpphamtidr = 0;
                                    somst.soitemmstnote = "";
                                    somst.soitemmststatus = "In Process";
                                    somst.soitemmstres1 = "";
                                    somst.soitemmstres2 = "";
                                    somst.soitemmstres3 = "";
                                    somst.soitemrate2toidr = 1;
                                    somst.createuser = rabmst.createuser;
                                    somst.createtime = rabmst.createtime;
                                    somst.upduser = Session["UserID"].ToString();
                                    somst.updtime = servertime;

                                    somst.soitemtotalamt = soitemtotalamt;
                                    somst.soitemtotaltaxamt = soitemtotaltaxamt;
                                    somst.soitemtotaltaxamtidr = soitemtotaltaxamt;
                                    somst.soitemtotalnetto = soitemtotalnetto;
                                    somst.soitemgrandtotalamt = soitemtotalnetto;
                                    somst.soitemgrandtotalamtidr = soitemtotalnetto;
                                    somst.revsoitemtype = rabmst.revrabtype;
                                    somst.revsoitemmstoid = revsoitemmstoid;
                                    somst.salesadminoid = rabmst.salesadminoid;

                                    db.QL_trnsoitemmst.Add(somst);
                                    db.SaveChanges();

                                    for (int i = 0; i < rabdtl.Count(); i++)
                                    {
                                        sodtl = new QL_trnsoitemdtl();
                                        sodtl.cmpcode = CompnyCode;
                                        sodtl.soitemdtloid = sooiddtl++;
                                        sodtl.soitemmstoid = sooid;
                                        sodtl.rabdtloid = rabdtl[i].rabdtloid;
                                        sodtl.soitemdtlseq = i + 1;
                                        sodtl.itemoid = rabdtl[i].itemoid;
                                        sodtl.soitemqty = rabdtl[i].rabqty;
                                        sodtl.soitemunitoid = rabdtl[i].rabunitoid;
                                        sodtl.soitemprice = rabdtl[i].rabprice;
                                        sodtl.soitempriceidr = rabdtl[i].rabprice;
                                        sodtl.soitemdtlamt = rabdtl[i].rabqty * rabdtl[i].rabprice;
                                        sodtl.soitemdtlamtidr = rabdtl[i].rabqty * rabdtl[i].rabprice;
                                        sodtl.soitemdtldisctype = "";
                                        sodtl.soitemdtldiscvalue = 0;
                                        sodtl.soitemdtldiscamt = 0;
                                        sodtl.soitemdtldiscamtidr = 0;
                                        sodtl.soitemdtltaxvalue = rabdtl[i].rabtaxvalue;
                                        sodtl.soitemdtltaxamt = rabdtl[i].rabtaxamt;
                                        sodtl.soitemdtltaxamtidr = rabdtl[i].rabtaxamt;
                                        sodtl.soitemdtlpphvalue = 0;
                                        sodtl.soitemdtlpphamt = 0;
                                        sodtl.soitemdtlpphamtidr = 0;
                                        sodtl.soitemdtlnetto = Math.Round((rabdtl[i].rabqty * rabdtl[i].rabprice) + rabdtl[i].rabtaxamt,0);
                                        sodtl.soitemdtlnettoidr = Math.Round((rabdtl[i].rabqty * rabdtl[i].rabprice) + rabdtl[i].rabtaxamt,0);
                                        sodtl.soitemdtlnote = "";
                                        sodtl.soitemdtlstatus = "";
                                        sodtl.soitemdtlres1 = "";
                                        sodtl.soitemdtlres2 = "";
                                        sodtl.soitemdtlres3 = "";
                                        sodtl.upduser = rabmst.upduser;
                                        sodtl.updtime = rabmst.updtime;
                                        sodtl.closeqty = 0;
                                        sodtl.soitemdtllocoid = rabdtl[i].rablocoid;
                                        sodtl.soitemdtletd = rabdtl[i].rabetd;
                                        sodtl.soitemdtlongkiramt = 0;

                                        db.QL_trnsoitemdtl.Add(sodtl);
                                        db.SaveChanges();
                                    }

                                    sSql = "SELECT COUNT(*) FROM QL_trnrabdtl3 where rabmstoid = " + id + "";
                                    var cJasa = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

                                    if (cJasa > 0)
                                    {
                                        sooid += 1;

                                        somst3.cmpcode = rabmst.cmpcode;
                                        somst3.soitemmstoid = sooid;
                                        somst3.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                        somst3.soitemtype = "Jasa";
                                        somst3.soitemdate = servertime;
                                        somst3.soitemno = "";
                                        somst3.rabmstoid = rabmst.rabmstoid;
                                        somst3.custoid = rabmst.custoid;
                                        somst3.alamatoid = rabmst.alamatoid;
                                        somst3.salesoid = rabmst.salesoid;
                                        somst3.soitemrefno = "";
                                        somst3.soitemcustpo = "";
                                        somst3.soitemcustpo = "";
                                        somst3.soitemcustpodate = servertime;
                                        somst3.soitemcustpodate2 = servertime;
                                        somst3.soitemcustdoc = "";
                                        somst3.soitempaytypeoid = rabmst.rabpaytermoid;
                                        somst3.curroid = 1;
                                        somst3.rateoid = 0;
                                        somst3.rate2oid = 1;
                                        somst3.soitemtotaldiscamt = 0;
                                        somst3.soitemtotaldiscamtidr = 0;
                                        somst3.soitemtotalpphamt = 0;
                                        somst3.soitemtotalpphamtidr = 0;
                                        somst3.soitemmstnote = "";
                                        somst3.soitemmststatus = "In Process";
                                        somst3.soitemmstres1 = "";
                                        somst3.soitemmstres2 = "";
                                        somst3.soitemmstres3 = "";
                                        somst3.soitemrate2toidr = 1;
                                        somst3.createuser = rabmst.createuser;
                                        somst3.createtime = rabmst.createtime;
                                        somst3.upduser = Session["UserID"].ToString();
                                        somst3.updtime = servertime;

                                        somst3.soitemtotalamt = sojasatotalamt;
                                        somst3.soitemtotaltaxamt = sojasatotaltaxamt;
                                        somst3.soitemtotaltaxamtidr = sojasatotaltaxamt;
                                        somst3.soitemtotalnetto = sojasatotalnetto;
                                        somst3.soitemgrandtotalamt = sojasatotalnetto;
                                        somst3.soitemgrandtotalamtidr = sojasatotalnetto;
                                        somst3.revsoitemtype = "Baru";
                                        somst3.revsoitemmstoid = 0;

                                        db.QL_trnsoitemmst.Add(somst3);
                                        db.SaveChanges();

                                        for (int i = 0; i < rabdtl3.Count(); i++)
                                        {
                                            sodtl3 = new QL_trnsoitemdtl();
                                            sodtl3.cmpcode = CompnyCode;
                                            sodtl3.soitemdtloid = sooiddtl++;
                                            sodtl3.soitemmstoid = sooid;
                                            sodtl3.rabdtloid = rabdtl3[i].rabdtl3oid;
                                            sodtl3.soitemdtlseq = i + 1;
                                            sodtl3.itemoid = rabdtl3[i].itemdtl3oid;
                                            sodtl3.soitemqty = rabdtl3[i].rabdtl3qty;
                                            sodtl3.soitemunitoid = rabdtl3[i].rabdtl3unitoid;
                                            sodtl3.soitemprice = rabdtl3[i].rabdtl3price;
                                            sodtl3.soitempriceidr = rabdtl3[i].rabdtl3price;
                                            sodtl3.soitemdtlamt = rabdtl3[i].rabdtl3qty * rabdtl3[i].rabdtl3price;
                                            sodtl3.soitemdtlamtidr = rabdtl3[i].rabdtl3qty * rabdtl3[i].rabdtl3price;
                                            sodtl3.soitemdtldisctype = "";
                                            sodtl3.soitemdtldiscvalue = 0;
                                            sodtl3.soitemdtldiscamt = 0;
                                            sodtl3.soitemdtldiscamtidr = 0;
                                            sodtl3.soitemdtltaxvalue = rabdtl3[i].rabdtl3taxvalue;
                                            sodtl3.soitemdtltaxamt = rabdtl3[i].rabdtl3taxamt;
                                            sodtl3.soitemdtltaxamtidr = rabdtl3[i].rabdtl3taxamt;
                                            sodtl3.soitemdtlpphvalue = 0;
                                            sodtl3.soitemdtlpphamt = 0;
                                            sodtl3.soitemdtlpphamtidr = 0;
                                            sodtl3.soitemdtlnetto = Math.Round((rabdtl3[i].rabdtl3qty * rabdtl3[i].rabdtl3price) + rabdtl3[i].rabdtl3taxamt, 0);
                                            sodtl3.soitemdtlnettoidr = Math.Round((rabdtl3[i].rabdtl3qty * rabdtl3[i].rabdtl3price) + rabdtl3[i].rabdtl3taxamt, 0);
                                            sodtl3.soitemdtlnote = "";
                                            sodtl3.soitemdtlstatus = "";
                                            sodtl3.soitemdtlres1 = "";
                                            sodtl3.soitemdtlres2 = "";
                                            sodtl3.soitemdtlres3 = "";
                                            sodtl3.upduser = rabmst.upduser;
                                            sodtl3.updtime = rabmst.updtime;
                                            sodtl3.closeqty = 0;
                                            sodtl3.soitemdtllocoid = rabdtl3[i].rabdtl3locoid;
                                            sodtl3.soitemdtletd = rabdtl3[i].rabdtl3etd;
                                            sodtl3.soitemdtlongkiramt = 0;

                                            db.QL_trnsoitemdtl.Add(sodtl3);
                                            db.SaveChanges();
                                        }
                                    }

                                    sSql = "UPDATE QL_ID SET lastoid=" + (sooiddtl - 1) + " WHERE tablename='QL_trnsoitemdtl'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_ID SET lastoid=" + sooid + " WHERE tablename='QL_trnsoitemmst'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    //}
                                    //else //Revisi
                                    //{
                                    //    sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Post', soitemmstres1='' WHERE rabmstoid=" + rabmst.revrabmstoid;
                                    //    db.Database.ExecuteSqlCommand(sSql);
                                    //    db.SaveChanges();
                                    //}     
                                }
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }    
        }

        private void ApprovalRABRevisiPO(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();

            string ctrlname = "RABRevisiPO";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnrabmst_hist rabmst = db.QL_trnrabmst_hist.Find(CompnyCode, id);

            List<listrabdtl2> rabdtl2 = new List<listrabdtl2>();
            sSql = "SELECT * FROM QL_trnrabdtl2_hist where rabmstoid_hist = " + id + "";
            rabdtl2 = db.Database.SqlQuery<listrabdtl2>(sSql).ToList();

            error = "";
            var sqlPlus = "";

            if (error == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnrabmst_hist' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnrabmst_hist SET rabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE rabmststatus='In Approval' AND rabmstoid_hist=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = rabmst.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = rabmst.reqrabmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnrabmst_hist";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnrabmst_hist SET rabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE rabmststatus='In Approval' AND rabmstoid_hist=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET rabtotalbeliamt=" + rabmst.rabtotalbeliamt + ", rabtotalbelitaxamt=" + rabmst.rabtotalbelitaxamt + ", rabgrandtotalbeliamt=" + rabmst.rabgrandtotalbeliamt + ", rabpersenmargin=" + rabmst.rabpersenmargin + ", rabtotalmargin=" + rabmst.rabtotalmargin + " WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + rabmst.rabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                for (int i = 0; i < rabdtl2.Count(); i++)
                                {
                                    sSql = "UPDATE QL_trnrabdtl2 SET suppdtl2oid=" + rabdtl2[i].suppdtl2oid + ", rabdtl2price=" + rabdtl2[i].rabdtl2price + ", rabdtl2discvalue=" + rabdtl2[i].rabdtl2discvalue + ", rabdtl2discamt=" + rabdtl2[i].rabdtl2discamt + ", rabdtl2amt=" + rabdtl2[i].rabdtl2amt + ", rabdtl2taxamt=" + rabdtl2[i].rabdtl2taxamt + ", rabdtl2netto=" + rabdtl2[i].rabdtl2netto + ", rabdtl2note='" + rabdtl2[i].rabdtl2note + "', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND rabdtl2oid=" + rabdtl2[i].rabdtl2oid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalRABRevisiBiaya(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();

            string ctrlname = "RABRevisiBiaya";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnrabmst_hist rabmst = db.QL_trnrabmst_hist.Find(CompnyCode, id);

            List<listrabdtl5> rabdtl5 = new List<listrabdtl5>();
            sSql = "SELECT * FROM QL_trnrabdtl5_hist where rabmstoid_hist = " + id + "";
            rabdtl5 = db.Database.SqlQuery<listrabdtl5>(sSql).ToList();

            error = "";
            var sqlPlus = "";

            if (error == "")
            {
                var dtloid5 = ClassFunction.GenerateID("QL_trnrabdtl5");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnrabmst_hist2' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnrabmst_hist SET rabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE rabmststatus='In Approval' AND rabmstoid_hist=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = rabmst.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = rabmst.reqrabmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnrabmst_hist2";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } 
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnrabmst_hist SET rabmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE rabmststatus='In Approval' AND rabmstoid_hist=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET rabtotalcostamt=" + rabmst.rabtotalcostamt + ", rabgrandtotalcostamt=" + rabmst.rabgrandtotalcostamt + ", rabpersenmargin=" + rabmst.rabpersenmargin + ", rabtotalmargin=" + rabmst.rabtotalmargin + " WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + rabmst.rabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                QL_trnrabdtl5 tbldtl5;
                                for (int i = 0; i < rabdtl5.Count(); i++)
                                {
                                    if (rabdtl5[i].rabdtl5oid == 0)
                                    {
                                        tbldtl5 = new QL_trnrabdtl5();
                                        tbldtl5.cmpcode = rabmst.cmpcode;
                                        tbldtl5.rabdtl5oid = dtloid5++;
                                        tbldtl5.rabmstoid = rabmst.rabmstoid;
                                        tbldtl5.rabdtl5seq = i + 1;
                                        tbldtl5.acctgoid = rabdtl5[i].acctgoid;
                                        tbldtl5.rabdtl5qty = 1;
                                        tbldtl5.rabdtl5price = rabdtl5[i].rabdtl5price;
                                        tbldtl5.rabdtl5netto = rabdtl5[i].rabdtl5price;
                                        tbldtl5.rabdtl5note = rabdtl5[i].rabdtl5note;
                                        tbldtl5.rabdtl5status = "";
                                        tbldtl5.upduser = Session["UserID"].ToString();
                                        tbldtl5.updtime = servertime;

                                        db.QL_trnrabdtl5.Add(tbldtl5);
                                    }
                                    else
                                    {
                                        sSql = "UPDATE QL_trnrabdtl5 SET acctgoid=" + rabdtl5[i].acctgoid + ", rabdtl5price=" + rabdtl5[i].rabdtl5price + ", rabdtl5netto=" + rabdtl5[i].rabdtl5price + ", rabdtl5note='" + rabdtl5[i].rabdtl5note + "', upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid=" + rabdtl5[i].rabdtl5oid + "";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid5 - 1) + " WHERE tablename='QL_trnrabdtl5'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalPO(int id, string cmp, string transno, string action, out string error)
        {            
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "PO";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trnpoitemmst tbl = db.QL_trnpoitemmst.Find(CompnyCode, id);
            List<QL_trnpoitemdtl> dtDtl = db.QL_trnpoitemdtl.Where(x => x.cmpcode == CompnyCode && x.poitemmstoid == id).ToList();
            if (tbl.poitemno == "")
            {
                sqlPlus2 = ", poitemno='" + transno + "'";
            }

            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Approved")
            {
                cRate.SetRateValue(tbl.curroid.Value, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.poitemdate.Value;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnpoitemmst.Where(x => x.poitemmstoid == tbl.poitemmstoid && x.poitemmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                tbl.poitemrate2toidr = 1;
                if (action == "Approved")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.poitemrate2toidr = cRate.GetRateMonthlyIDRValue;
                    tbl.poitemtotalamtidr = tbl.poitemtotalamt * cRate.GetRateMonthlyIDRValue;
                    tbl.poitemgrandtotalamtidr = tbl.poitemgrandtotalamt * cRate.GetRateMonthlyIDRValue;
                }
                var dtloid = ClassFunction.GenerateID("QL_mstitemdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnpoitemmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + ", poitemrate2toidr=" + tbl.poitemrate2toidr + ", poitemtotalamtidr=" + tbl.poitemtotalamtidr + ", poitemgrandtotalamtidr=" + tbl.poitemgrandtotalamtidr + " " + sqlPlus + " " + sqlPlus2 + " WHERE poitemmststatus='In Approval' AND poitemtype NOT IN ('Jasa') AND poitemmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.poitemmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnpoitemmst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }   
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + ", poitemrate2toidr=" + tbl.poitemrate2toidr + ", poitemtotalamtidr=" + tbl.poitemtotalamtidr + ", poitemgrandtotalamtidr=" + tbl.poitemgrandtotalamtidr + " " + sqlPlus + " " + sqlPlus2 + " WHERE poitemmststatus='In Approval' AND poitemtype NOT IN ('Jasa') AND poitemmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                //if (tbl.revpoitemmstoid != 0)
                                //{
                                //    sSql = "UPDATE QL_trnpoitemmst SET poitemmststatus='Closed', poitemmstres1='Force Closed' WHERE poitemmstoid=" + tbl.revpoitemmstoid;
                                //    db.Database.ExecuteSqlCommand(sSql);
                                //    db.SaveChanges();
                                //}

                                if (dtDtl != null)
                                {
                                    if (dtDtl.Count > 0)
                                    {
                                        QL_mstitemdtl tbldtl;
                                        for (int i = 0; i < dtDtl.Count(); i++)
                                        {
                                            sSql = "SELECT COUNT(-1) FROM QL_mstitemdtl WHERE cmpcode='" + CompnyCode + "' AND itemoid=" + dtDtl[i].itemoid + " AND suppoid=" + tbl.suppoid + "";
                                            var hit = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                            if (hit <= 0)
                                            {
                                                tbldtl = new QL_mstitemdtl();
                                                tbldtl.cmpcode = tbl.cmpcode;
                                                tbldtl.itemdtloid = dtloid++;
                                                tbldtl.itemoid = dtDtl[i].itemoid.Value;
                                                tbldtl.suppoid = tbl.suppoid.Value;
                                                tbldtl.picoid = 0;
                                                tbldtl.upduser = Session["UserID"].ToString();
                                                tbldtl.updtime = servertime;

                                                db.QL_mstitemdtl.Add(tbldtl);
                                            }
                                        }
                                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_mstitemdtl'";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalPOJasa(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "POJasa";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnpoitemmst tbl = db.QL_trnpoitemmst.Find(CompnyCode, id);
            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Approved")
            {
                cRate.SetRateValue(tbl.curroid.Value, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.poitemdate.Value;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnpoitemmst.Where(x => x.poitemmstoid == tbl.poitemmstoid && x.poitemmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                tbl.poitemrate2toidr = 1;
                if (action == "Approved")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.poitemrate2toidr = cRate.GetRateMonthlyIDRValue;
                    tbl.poitemtotalamtidr = tbl.poitemtotalamt * cRate.GetRateMonthlyIDRValue;
                    tbl.poitemgrandtotalamtidr = tbl.poitemgrandtotalamt * cRate.GetRateMonthlyIDRValue;

                    if (!string.IsNullOrEmpty(tbl.poitemno))
                    {
                        transno = tbl.poitemno;
                    }
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnpojasamst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnpoitemmst SET poitemno='" + transno + "', poitemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + ", poitemrate2toidr=" + tbl.poitemrate2toidr + ", poitemtotalamtidr=" + tbl.poitemtotalamtidr + ", poitemgrandtotalamtidr=" + tbl.poitemgrandtotalamtidr + " " + sqlPlus + " WHERE poitemmststatus='In Approval' AND poitemtype IN ('Jasa') AND poitemmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.poitemmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnpojasamst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } 
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnpoitemmst SET poitemno='" + transno + "', poitemmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + ", poitemrate2toidr=" + tbl.poitemrate2toidr + ", poitemtotalamtidr=" + tbl.poitemtotalamtidr + ", poitemgrandtotalamtidr=" + tbl.poitemgrandtotalamtidr + " " + sqlPlus + " WHERE poitemmststatus='In Approval' AND poitemtype IN ('Jasa') AND poitemmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalPOAsset(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "POA";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnpoassetmst tbl = db.QL_trnpoassetmst.Find(CompnyCode, id);
            List<QL_trnpoassetdtl> dtDtl = db.QL_trnpoassetdtl.Where(x => x.cmpcode == CompnyCode && x.poassetmstoid == id).ToList();

            if (tbl.poassettype == "Konsinyasi")
            {
                transno = GenerateTransNo("POK", CompnyCode, "QL_trnpoassetmst", "poassetno");
            }

            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Approved")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.poassetdate;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnpoassetmst.Where(x => x.poassetmstoid == tbl.poassetmstoid && x.poassetmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                tbl.poassetrate2toidr = 1;
                if (action == "Approved")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.poassetrate2toidr = cRate.GetRateMonthlyIDRValue;
                    tbl.poassettotalamtidr = tbl.poassettotalamt * cRate.GetRateMonthlyIDRValue;
                    tbl.poassetgrandtotalamtidr = tbl.poassetgrandtotalamt * cRate.GetRateMonthlyIDRValue;

                    if (!string.IsNullOrEmpty(tbl.poassetno))
                    {
                        transno = tbl.poassetno;
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_mstitemdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnpoassetmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnpoassetmst SET poassetno='" + transno + "', poassetmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + ", poassetrate2toidr=" + tbl.poassetrate2toidr + ", poassettotalamtidr=" + tbl.poassettotalamtidr + ", poassetgrandtotalamtidr=" + tbl.poassetgrandtotalamtidr + " " + sqlPlus + " WHERE poassetmststatus='In Approval' AND poassetmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.poassetmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnpoassetmst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }  
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnpoassetmst SET poassetno='" + transno + "', poassetmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', rate2oid=" + rate2oid + ", poassetrate2toidr=" + tbl.poassetrate2toidr + ", poassettotalamtidr=" + tbl.poassettotalamtidr + ", poassetgrandtotalamtidr=" + tbl.poassetgrandtotalamtidr + " " + sqlPlus + " WHERE poassetmststatus='In Approval' AND poassetmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (dtDtl != null)
                                {
                                    if (dtDtl.Count > 0)
                                    {
                                        QL_mstitemdtl tbldtl;
                                        for (int i = 0; i < dtDtl.Count(); i++)
                                        {
                                            sSql = "SELECT COUNT(-1) FROM QL_mstitemdtl WHERE cmpcode='" + CompnyCode + "' AND itemoid=" + dtDtl[i].poassetrefoid + " AND suppoid=" + tbl.suppoid + "";
                                            var hit = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                            if (hit <= 0)
                                            {
                                                tbldtl = new QL_mstitemdtl();
                                                tbldtl.cmpcode = tbl.cmpcode;
                                                tbldtl.itemdtloid = dtloid++;
                                                tbldtl.itemoid = dtDtl[i].poassetrefoid;
                                                tbldtl.suppoid = tbl.suppoid;
                                                tbldtl.picoid = 0;
                                                tbldtl.upduser = Session["UserID"].ToString();
                                                tbldtl.updtime = servertime;

                                                db.QL_mstitemdtl.Add(tbldtl);
                                            }
                                        }
                                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_mstitemdtl'";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        } 

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalCust(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "CustomerCreditLimit";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_mstcusthist tbl = db.QL_mstcusthist.Find(CompnyCode, id);

            if (error == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "SELECT TOP 1 custcreditlimitnew FROM QL_mstcusthist where custhistoid = " + id + " AND custhiststatus = 'In Approval' ORDER BY createtime DESC ";
                        var dCL = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_mstcusthist' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_mstcusthist SET custhistcode='" + transno + "',custhiststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE custhiststatus='In Approval' AND custhistoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.custoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_mstcusthist";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            } 
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_mstcusthist SET custhistcode='" + transno + "',custhiststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE custhiststatus='In Approval' AND custhistoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_mstcust SET custcreditlimit = " + dCL + ", updtime = '" + servertime + "' WHERE custoid IN (SELECT custoid FROM QL_mstcusthist where custhistoid = " + id + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }  

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            } 
        }

        private void ApprovalStockAdj(int id, string cmp, string transno, string action, out string error)
        {
            error = ""; 
            var servertime = ClassFunction.GetServerTime();
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            var sqlPlus = ""; var stockadjtype = ""; var pidtype = ""; var stockadjnote = "";

            string ctrlname = "StockOpname";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            string PIDstr = ClassFunction.GetServerTime().ToString("yy") + "-" + ClassFunction.GetServerTime().ToString("MM") + "-";
            var refnoctr = GenPID(ClassFunction.GetServerTime());
            int formatCounter = Convert.ToInt32(DefaultCounter);

            int PIDNew = GenPIDNew();

            List<stockadj> dtDtl = (List<stockadj>)Session["QL_trnstockadj"];
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                sSql = "SELECT stockadjtype FROM QL_trnstockadj a WHERE resfield1=" + dtDtl[i].resfield1 + "";
                stockadjtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                sSql = "SELECT stockadjmstnote FROM QL_trnstockadj a WHERE resfield1=" + dtDtl[i].resfield1 + "";
                stockadjnote = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            }

            if (action == "Approved")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK");
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_ADJUSTMENT", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_STOCK_ADJUSTMENT");

                //Set Avg Value
                for (int i = 0; i < dtDtl.Count(); i++)
                {
                    if (string.IsNullOrEmpty(dtDtl[i].refno))
                        dtDtl[i].refno = "";
                    if (string.IsNullOrEmpty(dtDtl[i].serialnumber))
                        dtDtl[i].serialnumber = "";

                    decimal dQtyAdj = dtDtl[i].stockadjqty; string sType = "IN";
                    if (dtDtl[i].stockadjqty < 0)
                    {
                        dQtyAdj = dtDtl[i].stockadjqty * -1;
                        sType = "OUT";

                        var spr = "";
                        if (!ClassFunction.IsStockAvailable(CompnyCode, ClassFunction.GetServerTime(), dtDtl[i].itemoid, dtDtl[i].mtrwhoid, dQtyAdj, dtDtl[i].refno, dtDtl[i].rabmstoid, dtDtl[i].serialnumber))
                        {
                            if (dtDtl[i].refno != "")
                            {
                                spr = " - ";
                            }
                            ModelState.AddModelError("", "Item PID " + spr + dtDtl[i].refno + " Qty tidak boleh lebih dari Stock Qty");
                        }
                    }
                    decimal sValue = dtDtl[i].stockvalue;
                    decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dQtyAdj, sValue, sType);
                    dtDtl[i].stockvalue_new = sAvgValue;

                    int res1 = dtDtl[i].resfield1;
                    DateTime tgladj = db.QL_trnstockadj.Where(x => x.resfield1 == res1).Select(y => y.stockadjdate).FirstOrDefault();
                    //Cek Tanggal Closing
                    System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                    DateTime cekClosingDate = tgladj;//Tanggal Dokumen
                    if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                    {
                        error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                    }
                }
            }

            string errDup = "";
            if (db.QL_trnstockadj.Where(x => x.resfield1 == id && x.stockadjstatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                var iAcctgOidAdj = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_ADJUSTMENT", CompnyCode));
                var stockadj2oid = ClassFunction.GenerateID("QL_trnstockadj2");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisednote='" + Session["rvnote"].ToString() + "', revisedtime='" + servertime + "', reviseduser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejecttime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        // Update QL_APP
                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnstockadj' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            // Update QL_trnstockadj
                            sSql = "UPDATE QL_trnstockadj SET stockadjno='" + transno + "',stockadjstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE stockadjstatus='In Approval' AND resfield1=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }   

                        if (appStatus == "Belum")
                        {
                            if (action == "Approved")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = CompnyCode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = id;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnstockadj";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }  
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Stock Opname|No. " + transno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                                    var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                                    var itemtype = "FINISH GOOD";
                                    if (type == "Asset")
                                    {
                                        itemtype = "FIXED ASSET";
                                    }

                                    // Update QL_trnstockadj
                                    sSql = "UPDATE QL_trnstockadj SET stockadjno='" + transno + "', reasonoid=" + iAcctgOidAdj + ", reasonacctgoid=" + iAcctgOidStock + ", stockadjstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', stockadjamtidr=" + dtDtl[i].stockvalue_new + ", stockadjamtusd=0 WHERE stockadjstatus='In Approval' AND resfield1=" + id + " AND stockadjoid=" + dtDtl[i].stockadjoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "SELECT pidtype FROM QL_mstitem a WHERE itemoid=" + dtDtl[i].itemoid + "";
                                    pidtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                                    if (stockadjtype == "INITIAL" && pidtype == "PID")
                                    {
                                        QL_trnstockadj2 tbldtl2;
                                        for (int j = 0; j < dtDtl[i].stockadjqty; j++)
                                        {
                                            tbldtl2 = new QL_trnstockadj2();
                                            tbldtl2.cmpcode = CompnyCode;
                                            tbldtl2.stockadj2oid = stockadj2oid;
                                            tbldtl2.stockadjoid = dtDtl[i].stockadjoid;
                                            tbldtl2.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                            tbldtl2.refname = itemtype;
                                            tbldtl2.refoid = dtDtl[i].itemoid;
                                            tbldtl2.mtrwhoid = dtDtl[i].mtrwhoid;
                                            tbldtl2.stockadjqty = 1;
                                            tbldtl2.stockadjamtidr = (dtDtl[i].stockvalue_new / dtDtl[i].stockadjqty);
                                            tbldtl2.stockadjamtusd = 0;
                                            tbldtl2.stockadjstatus = action;
                                            tbldtl2.resfield1 = dtDtl[i].resfield1;
                                            tbldtl2.refno = (pidtype == "PID" ? ClassFunction.GetServerTime().ToString("yy") + "-" + PIDNew.ToString() : "");
                                            //tbldtl2.refno = (pidtype == "PID" ? PIDstr + ClassFunction.GenNumberString(refnoctr, formatCounter) : "");
                                            tbldtl2.upduser = Session["UserID"].ToString();
                                            tbldtl2.updtime = servertime;
                                            tbldtl2.serialnumber = dtDtl[i].serialnumber;

                                            stockadj2oid++;
                                            PIDNew = (pidtype == "PID" ? (PIDNew + 1) : PIDNew);
                                            //refnoctr = refnoctr + 1;
                                            db.QL_trnstockadj2.Add(tbldtl2);
                                            db.SaveChanges();

                                            // Insert QL_conmat
                                            db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "ADJ", "QL_trnstockadj2", dtDtl[i].resfield1, dtDtl[i].itemoid, itemtype, dtDtl[i].mtrwhoid, 1, "Stock Opname", "Stock Opname No. " + transno, Session["UserID"].ToString(), tbldtl2.refno, tbldtl2.stockadjamtidr, 0, 0, null, tbldtl2.stockadj2oid, 0, dtDtl[i].stockvalue, tbldtl2.serialnumber));
                                            db.SaveChanges();
                                        }

                                        sSql = "UPDATE QL_ID SET lastoid=" + (stockadj2oid - 1) + " WHERE tablename='QL_trnstockadj2'";
                                        db.Database.ExecuteSqlCommand(sSql);
                                        db.SaveChanges();

                                    }
                                    else
                                    {
                                        // Insert QL_conmat
                                        db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "ADJ", "QL_trnstockadj", dtDtl[i].resfield1, dtDtl[i].itemoid, itemtype, dtDtl[i].mtrwhoid, dtDtl[i].stockadjqty, "Stock Opname", "Stock Opname No. " + transno, Session["UserID"].ToString(), dtDtl[i].refno, dtDtl[i].stockvalue_new, 0, 0, null, dtDtl[i].stockadjoid, dtDtl[i].rabmstoid, (dtDtl[i].stockvalue * dtDtl[i].stockadjqty), dtDtl[i].serialnumber));
                                        db.SaveChanges();
                                    }

                                    var dbcrFG = "D"; var dbcrADJ = "C";
                                    if (dtDtl[i].stockadjqty < 0)
                                    {
                                        dbcrFG = "C"; dbcrADJ = "D";
                                    }
                                    var glseq = 1;
                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidStock, dbcrFG, dtDtl[i].stockvalue_new, transno, transno + " | " + stockadjnote, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].stockvalue_new, 0, "QL_trnstockadj " + id.ToString(), null, null, null, 0));
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAdj, dbcrADJ, dtDtl[i].stockvalue_new, transno, transno + " | " + stockadjnote, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].stockvalue_new, 0, "QL_trnstockadj " + id.ToString(), null, null, null, 0));
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }           
        }

        private void ApprovalKasbon2Real(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "Kasbon2Real";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trnkasbon2mst tbl = db.QL_trnkasbon2mst.Find(CompnyCode, id);
            List<QL_trnkasbon2dtl> dtDtl = db.QL_trnkasbon2dtl.Where(x => x.cmpcode == CompnyCode && x.kasbon2mstoid == id).ToList();
            sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trnkasbon2mst' ORDER BY rd.apitemdtl2seq";
            List<listapitemdtl2> dtDtl2 = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            DateTime sDate = tbl.kasbon2date;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            if (tbl.kasbon2no == "")
            {
                sqlPlus2 = ", kasbon2no='" + transno + "'";
                tbl.kasbon2no = transno;
            }

            var rate2oid = 0;
            var cRate = new ClassRate();

            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.suppoid).FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;
            decimal TotalAmt = 0; decimal TotalAmtIDR = 0;

            if (action == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    error =  ClassFunction.GetInterfaceWarning("VAR_PPN_IN");

                if (dtDtl != null)
                {
                    if (dtDtl.Count > 0)
                    {
                        decimal amtReal = 0;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            TotalAmt += dtDtl[i].kasbon2dtlamt;
                            TotalAmtIDR += dtDtl[i].kasbon2dtlamt * cRate.GetRateMonthlyIDRValue;

                            if (tbl.kasbon2group == "REALISASI")
                            {
                                amtReal += dtDtl[i].kasbon2dtlamt;
                                if (tbl.rabmstoid > 0)
                                {
                                    sSql = "SELECT ISNULL((SELECT SUM(rd.rabdtl5price) totalamt FROM QL_trnrabdtl5 rd INNER JOIN QL_trnkasbon2dtl kd ON kd.rabdtl5oid=rd.rabdtl5oid INNER JOIN QL_trnkasbon2mst km ON km.kasbon2mstoid=kd.kasbon2mstoid WHERE km.kasbon2mstoid=" + tbl.kasbon2refoid + " AND km.rabmstoid=" + tbl.rabmstoid + " AND km.kasbon2group='KASBON'),0.0) - ISNULL((SELECT SUM(kd.kasbon2dtlamt) FROM QL_trnkasbon2dtl kd INNER JOIN QL_trnkasbon2mst km ON km.kasbon2mstoid=kd.kasbon2mstoid WHERE km.kasbon2group='REALISASI' AND km.rabmstoid=" + tbl.rabmstoid + " AND km.kasbon2refoid IN(SELECT km.kasbon2mstoid FROM QL_trnrabdtl5 rd INNER JOIN QL_trnkasbon2dtl kd ON kd.rabdtl5oid=rd.rabdtl5oid INNER JOIN QL_trnkasbon2mst km ON km.kasbon2mstoid=kd.kasbon2mstoid WHERE km.rabmstoid=" + tbl.rabmstoid + " AND km.kasbon2group='KASBON' AND rd.acctgoid IN(SELECT kd2.acctgoid FROM QL_trnkasbon2mst km2 INNER JOIN QL_trnkasbon2dtl kd2 ON kd2.kasbon2mstoid=km2.kasbon2mstoid WHERE km2.kasbon2mstoid=" + tbl.kasbon2refoid + ")) AND kd.kasbon2mstoid<>" + tbl.kasbon2mstoid + "),0.0) AS dt";
                                    decimal PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                                    if (amtReal > PriceOs)
                                    {
                                        error = "TOTAL AMOUNT Tidak boleh Lebih Besar dari Budget!";
                                    }
                                }
                            }
                        }

                        for (int i = 0; i < dtDtl2.Count(); i++)
                        {
                            totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                            fakturnoGL += dtDtl2[i].fakturno + ",";
                        }
                        fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                    }
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.kasbon2date;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnkasbon2mst.Where(x => x.kasbon2mstoid == tbl.kasbon2mstoid && x.kasbon2mststatus == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.kasbon2mststatus = action;
                }
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var conapoid = ClassFunction.GenerateID("QL_CONAP");

                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));

                decimal total = db.Database.SqlQuery<decimal>("SELECT (kasbon2amt + pphcreditamt) FROM QL_trnkasbon2mst r WHERE r.kasbon2mstoid ='" + tbl.kasbon2refoid + "'").FirstOrDefault();
                decimal totalIDR = total * cRate.GetRateMonthlyIDRValue ;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnkasbon2mst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE kasbon2mststatus='In Approval' AND kasbon2mstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.kasbon2mstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnkasbon2mst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', kasbon2amtidr=" + tbl.kasbon2amt * cRate.GetRateMonthlyIDRValue + " " + sqlPlus + " " + sqlPlus2 + " WHERE kasbon2mststatus='In Approval' AND kasbon2mstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (dtDtl2.Count > 0)
                                {
                                    for (int i = 0; i < dtDtl2.Count(); i++)
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }   
                                }

                                //jurnal
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, tbl.kasbon2no, tbl.kasbon2mststatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                int iSeq = 1;
                                if (tbl.kasbon2group == "KASBON")
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", TotalAmt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, TotalAmtIDR, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;

                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoidcb, "C", tbl.kasbon2amt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, tbl.kasbon2amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;

                                    //Jurnal PPH
                                    if (tbl.pphcreditamt > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.pphcreditoid, "C", tbl.pphcreditamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, tbl.pphcreditamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }

                                    // Insert QL_conap                        
                                    db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnkasbon2mst", tbl.kasbon2mstoid, 0, tbl.suppoid, tbl.acctgoidcb, "Post", "APKBN", sDate, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, tbl.kasbon2duedate, tbl.kasbon2amt, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tbl.kasbon2amt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                                    db.SaveChanges();
                                }
                                else //Realisasi
                                {
                                    for (var i = 0; i < dtDtl.Count(); i++)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", dtDtl[i].kasbon2dtlamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note + " | " + dtDtl[i].kasbon2dtlnote + "", tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, dtDtl[i].kasbon2dtlamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }

                                    //var total = (tbl.kasbon2amtnetto + tbl.kasbon2amtselisih);
                                    //var totalIDR = (total * cRate.GetRateMonthlyIDRValue);
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", total, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, totalIDR, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;

                                    decimal totaltax = Convert.ToDecimal(totaltaxhdr);
                                    if (totaltax > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid, iSeq, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.kasbon2no, tbl.kasbon2no + " | " + suppname + " | " + tbl.kasbon2note, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid, null, null, null, 0, fakturnoGL));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }

                                    var selisihamt = tbl.kasbon2amtselisih;
                                    if (selisihamt > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoidcb, "D", selisihamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                    else if (selisihamt < 0)
                                    {
                                        selisihamt = selisihamt * (-1);
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoidcb, "C", selisihamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;

                                        // Insert QL_conap                        
                                        db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnkasbon2mst", tbl.kasbon2mstoid, 0, tbl.suppoid, tbl.acctgoidcb, "Post", "APKBN", sDate, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, tbl.kasbon2duedate, selisihamt, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                                        db.SaveChanges();
                                    }

                                    //Jurnal PPH
                                    if (tbl.pphcreditamt > 0)
                                    {
                                        for (var i = 0; i < dtDtl.Count(); i++)
                                        {
                                            if (dtDtl[i].pphcreditdtlamt > 0)
                                            {
                                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].pphcreditacctgdtloid, "C", dtDtl[i].pphcreditdtlamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note + " | " + dtDtl[i].kasbon2dtlnote + "", tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, dtDtl[i].pphcreditdtlamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                                db.SaveChanges();
                                                iSeq += 1;
                                                gldtloid += 1;
                                            }
                                        }
                                    }
                                    if (tbl.pphdebetamt > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.pphdebetoid, "D", tbl.pphdebetamt, tbl.kasbon2no, tbl.kasbon2no + " | " + tbl.kasbon2note, tbl.kasbon2mststatus, tbl.upduser, tbl.updtime, tbl.pphdebetamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnkasbon2mst " + tbl.kasbon2mstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                }

                                sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalAPDir(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "APD";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trnapdirmst tbl = db.QL_trnapdirmst.Find(CompnyCode, id);
            List<QL_trnapdirdtl> dtDtl = db.QL_trnapdirdtl.Where(x => x.cmpcode == CompnyCode && x.apdirmstoid == id).ToList();
            sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trnapdirmst' ORDER BY rd.apitemdtl2seq";
            List<listapitemdtl2> dtDtl2 = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            DateTime sDate = tbl.apdirdate.Value;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            if (tbl.apdirno == "")
            {
                sqlPlus2 = ", apdirno='" + transno + "'";
                tbl.apdirno = transno;
            }

            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.suppoid).FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;

            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                if (!ClassFunction.IsInterfaceExists("VAR_AP_DIRECT", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_AP_DIRECT");
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    error =  ClassFunction.GetInterfaceWarning("VAR_PPN_IN");

                if (dtDtl2 != null)
                {
                    if (dtDtl2.Count > 0)
                    {
                        for (int i = 0; i < dtDtl2.Count(); i++)
                        {
                            totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                            fakturnoGL += dtDtl2[i].fakturno + ",";
                        }
                        fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                    }
                }
                if (dtDtl != null)
                {
                    if (dtDtl.Count > 0)
                    {
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (tbl.rabmstoid > 0)
                            {
                                sSql = "SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') AND arm.apdirmstoid <> " + tbl.apdirmstoid + "),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0)) AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.cmpcode='" + CompnyCode + "' AND rabdtl5oid= " + dtDtl[i].rabdtl5oid + "";
                                var PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                                if (PriceOs < dtDtl[i].apdirprice)
                                    error = "Amount O/S Sudah di update, Silahkan refresh terlebih dahulu!";
                                if (dtDtl[i].apdirprice > PriceOs)
                                    error =  "Amount AP Direct tidak boleh lebih dari Amount O/S (" + PriceOs + ")";
                            }
                        }
                    }
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.apdirdate.Value;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnapdirmst.Where(x => x.apdirmstoid == tbl.apdirmstoid && x.apdirmststatus == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.apdirmststatus = action;
                }
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_DIRECT", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnapdirmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE apdirmststatus='In Approval' AND apdirmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.apdirmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnapdirmst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE apdirmststatus='In Approval' AND apdirmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (dtDtl2.Count > 0)
                                {
                                    for (int i = 0; i < dtDtl2.Count(); i++)
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }
                                }

                                // Insert QL_conap                        
                                db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnapdirmst", tbl.apdirmstoid, 0, tbl.suppoid, iAcctgOidAP, "Post", "APDIR", sDate, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, tbl.apdirduedate, tbl.apdirgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tbl.apdirgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                                db.SaveChanges();

                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, sDate, sPeriod, tbl.apdirno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                var glseq = 1;
                                //decimal totaltax = Convert.ToDecimal(tblmst.apdirtaxamt);
                                //if (tblmst.apdirtaxamt > 0)
                                //{
                                //    // Insert QL_trngldtl
                                //    // PPN Masukan (D)
                                //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tblmst.apdirno, "AP Direct| Note. PPN ", "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apdirmstoid, null, null, null, 0));
                                //    db.SaveChanges();
                                //}

                                // Insert QL_trngldtl
                                // Biaya Lain - Lain (D)
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    var amt = dtDtl[i].apdirprice;
                                    if (amt > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "D", amt, tbl.apdirno, tbl.apdirno + " | " + tbl.apdirmstnote + " | " + dtDtl[i].apdirdtlnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tbl.apdirmstoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                    else if (amt < 0)
                                    {
                                        amt = dtDtl[i].apdirprice * (-1);
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "C", amt, tbl.apdirno, tbl.apdirno + " | " + tbl.apdirmstnote + " | " + dtDtl[i].apdirdtlnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tbl.apdirmstoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                }

                                decimal totaltax = Convert.ToDecimal(totaltaxhdr);
                                if (totaltax > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.apdirno, tbl.apdirno + " | " + suppname + " | " + tbl.apdirmstnote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tbl.apdirmstoid, null, null, null, 0, fakturnoGL));
                                    db.SaveChanges();
                                }

                                // Insert QL_trngldtl
                                // Hutang Dagang Lainnya (C)
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAP, "C", tbl.apdirgrandtotal, tbl.apdirno, tbl.apdirno + " | " + tbl.apdirmstnote, "Post", Session["UserID"].ToString(), servertime, tbl.apdirgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tbl.apdirmstoid, null, null, null, 0));
                                db.SaveChanges();

                                decimal totalpph = Convert.ToDecimal(tbl.pphamt);
                                if (tbl.pphamt > 0)
                                {
                                    // Insert QL_trngldtl
                                    // PPH (C)
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.acctgpphoid, "C", totalpph, tbl.apdirno, tbl.apdirno + " | " + tbl.apdirmstnote, "Post", Session["UserID"].ToString(), servertime, totalpph * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tbl.apdirmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalJurnalUmum(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";
            
            string ctrlname = "JU";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trnglmst tbl = db.QL_trnglmst.Find(CompnyCode, id);
            List<QL_trngldtl> dtDtl = db.QL_trngldtl.Where(x => x.cmpcode == CompnyCode && x.glmstoid == id).ToList();
            DateTime sDate = tbl.gldate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            if (tbl.glother1 == "")
            {
                sqlPlus2 = ", glother1='" + transno + "'";
                tbl.glother1 = transno;
            }

            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Post")
            {
                cRate.SetRateValue(1, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.gldate;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trnglmst.Where(x => x.glmstoid == tbl.glmstoid && x.glflag == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnglmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnglmst SET glflag='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE glflag='In Approval' AND glmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.glmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnglmst";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trnglmst SET glflag='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE glflag='In Approval' AND glmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (dtDtl != null)
                                {
                                    if (dtDtl.Count > 0)
                                    {
                                        for (int i = 0; i < dtDtl.Count(); i++)
                                        {
                                            sSql = "UPDATE QL_trngldtl SET glflag='" + action + "', glnote='" + dtDtl[i].glnote + " | " + tbl.glnote + "' WHERE gldtloid=" + dtDtl[i].gldtloid;
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalDN(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "DN";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trndebetnote tbl = db.QL_trndebetnote.Find(CompnyCode, id);
            List<apardata> tbldtl = new List<apardata>();
            sSql = "SELECT * FROM (" +
            "SELECT dn.cmpcode, '" + CompnyCode + "' divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,s.suppname AS suppcustname,dn.reftype tipe,dn.refoid,vap.transno AS aparno, vap.projectname, vap.departemen," +
            "dn.curroid,dn.rateoid,dn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,ap.amttransidr aparamtidr,ap.amttransusd aparamtusd," +
            "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayar," +
            "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayaridr," +
            "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayarusd," +
            " 0.0 AS amtdncn," +
            " 0.0 AS amtdncnidr," +
            " 0.0 AS amtdncnusd," +
            "dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ap.trnapdate aparcheckdate " +
            "FROM QL_trndebetnote dn " +
            "INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
            "INNER JOIN QL_conap ap ON ap.cmpcode=dn.cmpcode AND ap.reftype=dn.reftype AND ap.refoid=dn.refoid AND ap.payrefoid=0 " +
            "INNER JOIN (SELECT cmpcode, conapoid, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemno FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirno FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmstnote FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmstnote FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmstnote FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apitemdate) FROM QL_trnapitemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apitempaytypeoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apdirdate) FROM QL_trnapdirmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apdirpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apassetdate) FROM QL_trnapassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currcode FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currsymbol FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currsymbol FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT rm.projectname FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT rm.projectname FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS projectname, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT de.groupdesc FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT de.groupdesc FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS departemen FROM QL_conap AS con WHERE payrefoid = 0) vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid " +
            "WHERE left(dn.reftype,8)='ql_trnap' " +
            "UNION ALL " +
            "SELECT dn.cmpcode,'" + CompnyName + "' divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,c.custname AS suppcustname,dn.reftype AS tipe,dn.refoid,var.transno AS aparno, var.projectname, var.departemen," +
            "dn.curroid,dn.rateoid,dn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,ar.amttransidr aparamtidr,ar.amttransusd aparamtusd," +
            "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayar," +
            "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayaridr," +
            "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayarusd," +
            " 0.0 AS amtdncn," +
            " 0.0 AS amtdncnidr," +
            " 0.0 AS amtdncnusd," +
            "dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ar.trnardate aparcheckdate " +
            "FROM QL_trndebetnote dn " +
            "INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
            "INNER JOIN QL_conar ar ON ar.cmpcode=dn.cmpcode AND ar.reftype=dn.reftype AND ar.refoid=dn.refoid AND ar.payrefoid=0 " +
            "INNER JOIN (SELECT cmpcode, conaroid, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemno FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetno FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmststatus FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmststatus FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmstnote FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmstnote FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), aritemdate) FROM QL_trnaritemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), arassetdate) FROM QL_trnarassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currcode FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currsymbol FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT rm.projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) projectname, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT de.groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) departemen FROM QL_conar con WHERE payrefoid = 0) var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
            "WHERE left(dn.reftype,8)='ql_trnar' AND left(dn.reftype,11)<>'ql_trnarret' " +
            ") AS a WHERE a.dnoid=" + id;
            tbldtl = db.Database.SqlQuery<apardata>(sSql).ToList();
            string aparno = tbldtl[0].aparno;
            DateTime apardate = tbldtl[0].apardate;
            DateTime aparcheckdate = tbldtl[0].aparcheckdate;
            string reftype = "";
            if (ClassFunction.Left(tbl.reftype.ToUpper(), 8) == "QL_TRNAP")
            {
                reftype = "AP";
            }
            else
            {
                reftype = "AR";
            }
            decimal dmaxamount = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
            string suppcustname = tbldtl[0].suppcustname;

            decimal aparbalance = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
            decimal aparbalanceidr = tbldtl[0].aparamtidr - (tbldtl[0].amtbayaridr + tbldtl[0].amtdncnidr);
            decimal aparbalanceusd = tbldtl[0].aparamtusd - (tbldtl[0].amtbayarusd + tbldtl[0].amtdncnusd);
            decimal aparamt = tbldtl[0].aparamt;
            decimal aparamtidr = tbldtl[0].aparamtidr;
            decimal aparamtusd = tbldtl[0].aparamtusd;
            decimal aparpaidamt = tbldtl[0].amtbayar + tbldtl[0].amtdncn;
            decimal aparbalanceafter = Convert.ToDecimal(aparbalance) + (tbl.dnamt * (reftype.ToString() != "AR" ? -1 : 1));
            string dbacctgdesc = tbldtl[0].dbacctgdesc;
            string projectname = tbldtl[0].projectname;
            string departemen = tbldtl[0].departemen;

            if (tbl.dnno == "")
            {
                sqlPlus2 = ", dnno='" + transno + "'";
                tbl.dnno = transno;
            }

            var sDate = tbl.dndate;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Post")
            {
                cRate.SetRateValue(1, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.dndate;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trndebetnote.Where(x => x.dnoid == tbl.dnoid && x.dnstatus == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.dnstatus = action;
                    tbl.dnamtidr = tbl.dnamt * cRate.GetRateMonthlyIDRValue;
                    tbl.dntaxamtidr = tbl.dntaxamt * cRate.GetRateMonthlyIDRValue;
                    tbl.dnamtusd = tbl.dnamt * cRate.GetRateMonthlyUSDValue;
                    tbl.dntaxamtusd = tbl.dntaxamt * cRate.GetRateMonthlyUSDValue;
                }
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trndebetnote' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trndebetnote SET dnstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', dnamtidr="+ tbl.dnamtidr + ", dntaxamtidr="+ tbl.dntaxamtidr + ", dnamtusd=" + tbl.dnamtusd + ", dntaxamtusd=" + tbl.dntaxamtusd + " " + sqlPlus + " " + sqlPlus2 + " WHERE dnstatus='In Approval' AND dnoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.dnoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trndebetnote";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trndebetnote SET dnstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', dnamtidr=" + tbl.dnamtidr + ", dntaxamtidr=" + tbl.dntaxamtidr + ", dnamtusd=" + tbl.dnamtusd + ", dntaxamtusd=" + tbl.dntaxamtusd + " " + sqlPlus + " " + sqlPlus2 + " WHERE dnstatus='In Approval' AND dnoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (reftype.ToUpper() == "AP")
                                {
                                    QL_conap conap = new QL_conap();
                                    conap.cmpcode = tbl.cmpcode;
                                    conap.conapoid = conapoid++;
                                    conap.reftype = tbl.reftype;
                                    conap.refoid = tbl.refoid;
                                    conap.payrefoid = tbl.dnoid;
                                    conap.suppoid = tbl.suppcustoid;
                                    conap.acctgoid = tbl.dbacctgoid;
                                    conap.trnapstatus = tbl.dnstatus;
                                    conap.trnaptype = "DN" + reftype;
                                    conap.trnapdate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                    conap.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                                    conap.paymentacctgoid = tbl.cracctgoid;
                                    conap.paymentdate = tbl.dndate;
                                    conap.payrefno = tbl.dnno;
                                    conap.paybankoid = 0;
                                    conap.payduedate = tbl.dndate;
                                    conap.amttrans = 0;
                                    conap.amtbayar = tbl.dnamt + tbl.dntaxamt;
                                    conap.trnapnote = "DN No. " + tbl.dnno + " for " + reftype + " No. " + aparno + "";
                                    conap.createuser = tbl.createuser;
                                    conap.createtime = tbl.createtime;
                                    conap.upduser = tbl.upduser;
                                    conap.updtime = tbl.updtime;
                                    conap.amttransidr = 0;
                                    conap.amtbayaridr = tbl.dnamtidr + tbl.dntaxamtidr;
                                    conap.amttransusd = 0;
                                    conap.amtbayarusd = tbl.dnamtusd + tbl.dntaxamtusd;
                                    conap.amtbayarother = 0;
                                    conap.conflag = "";

                                    db.QL_conap.Add(conap);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_id SET lastoid=" + conap.conapoid + " WHERE tablename='QL_conap'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    QL_conar conar = new QL_conar();
                                    conar.cmpcode = tbl.cmpcode;
                                    conar.conaroid = conaroid++;
                                    conar.reftype = tbl.reftype;
                                    conar.refoid = tbl.refoid;
                                    conar.payrefoid = tbl.dnoid;
                                    conar.custoid = tbl.suppcustoid;
                                    conar.acctgoid = tbl.dbacctgoid;
                                    conar.trnarstatus = tbl.dnstatus;
                                    conar.trnartype = "DN" + reftype;
                                    conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                    conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                                    conar.paymentacctgoid = tbl.cracctgoid;
                                    conar.paymentdate = tbl.dndate;
                                    conar.payrefno = tbl.dnno;
                                    conar.paybankoid = 0;
                                    conar.payduedate = tbl.dndate;
                                    conar.amttrans = 0;
                                    conar.amtbayar = (tbl.dnamt + tbl.dntaxamt) * -1;
                                    conar.trnarnote = "DN No. " + tbl.dnno + " for " + reftype + " No. " + aparno + "";
                                    conar.createuser = tbl.createuser;
                                    conar.createtime = tbl.createtime;
                                    conar.upduser = tbl.upduser;
                                    conar.updtime = tbl.updtime;
                                    conar.amttransidr = 0;
                                    conar.amtbayaridr = (tbl.dnamtidr + tbl.dntaxamtidr) * -1;
                                    conar.amttransusd = 0;
                                    conar.amtbayarusd = (tbl.dnamtusd + tbl.dntaxamtusd) * -1;
                                    conar.amtbayarother = 0;

                                    db.QL_conar.Add(conar);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_id SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }

                                //Insert Into GL Mst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue));
                                db.SaveChanges();

                                int iSeq = 1;
                                //Insert Into GL Dtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.dbacctgoid, "D", tbl.dnamt + tbl.dntaxamt, tbl.dnno, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.upduser, tbl.updtime, tbl.dnamtidr + tbl.dntaxamtidr, tbl.dnamtusd + tbl.dntaxamtusd, "QL_trndebetnote " + tbl.dnoid, "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;

                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.cracctgoid, "C", tbl.dnamt + tbl.dntaxamt, tbl.dnno, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.upduser, tbl.updtime, tbl.dnamtidr + tbl.dntaxamtidr, tbl.dnamtusd + tbl.dntaxamtusd, "QL_trndebetnote " + tbl.dnoid, "", "", "", 0));
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalCN(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "CN";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trncreditnote tbl = db.QL_trncreditnote.Find(CompnyCode, id);
            List<apardata> tbldtl = new List<apardata>();
            sSql = "SELECT * FROM (" +
                "SELECT cn.cmpcode,'" + CompnyName + "' divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,s.suppname AS suppcustname,cn.reftype tipe,cn.refoid,vap.transno AS aparno, vap.projectname, vap.departemen," +
                "cn.curroid,cn.rateoid,cn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,ap.amttransidr aparamtidr,ap.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd," +
                "cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ap.trnapdate aparcheckdate " +
                "FROM QL_trncreditnote cn  " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                "INNER JOIN QL_conap ap ON ap.cmpcode=cn.cmpcode AND ap.reftype=cn.reftype AND ap.refoid=cn.refoid AND ap.payrefoid=0 " +
                "INNER JOIN (SELECT cmpcode, conapoid, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemno FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirno FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmstnote FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmstnote FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmstnote FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apitemdate) FROM QL_trnapitemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apitempaytypeoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apdirdate) FROM QL_trnapdirmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apdirpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apassetdate) FROM QL_trnapassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currcode FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currsymbol FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currsymbol FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT rm.projectname FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT rm.projectname FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS projectname, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT de.groupdesc FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT de.groupdesc FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS departemen FROM QL_conap AS con WHERE payrefoid = 0) vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid " +
                "WHERE left(cn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT cn.cmpcode,'" + CompnyName + "' divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,c.custname AS suppcustname,cn.reftype AS tipe,cn.refoid,var.transno AS aparno, var.projectname, var.departemen," +
                "cn.curroid,cn.rateoid,cn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,ar.amttransidr aparamtidr,ar.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd," +
                "cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ar.trnardate aparcheckdate " +
                "FROM QL_trncreditnote cn " +
                "INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                "INNER JOIN QL_conar ar ON ar.cmpcode=cn.cmpcode AND ar.reftype=cn.reftype AND ar.refoid=cn.refoid AND ar.payrefoid=0 " +
                "INNER JOIN (SELECT cmpcode, conaroid, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemno FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetno FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmststatus FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmststatus FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmstnote FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmstnote FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), aritemdate) FROM QL_trnaritemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), arassetdate) FROM QL_trnarassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currcode FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currsymbol FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT rm.projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) projectname, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT de.groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) departemen FROM QL_conar con WHERE payrefoid = 0) var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                "WHERE left(cn.reftype,8)='ql_trnar' AND left(cn.reftype,11)<>'ql_trnarret' " +
                ") AS a WHERE a.cnoid=" + id;
            tbldtl = db.Database.SqlQuery<apardata>(sSql).ToList();
            string aparno = tbldtl[0].aparno;
            DateTime apardate = tbldtl[0].apardate;
            DateTime aparcheckdate = tbldtl[0].aparcheckdate;
            string reftype = "";
            if (ClassFunction.Left(tbl.reftype.ToUpper(), 8) == "QL_TRNAP")
            {
                reftype = "AP";
            }
            else
            {
                reftype = "AR";
            }
            decimal dmaxamount = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
            string suppcustname = tbldtl[0].suppcustname;

            decimal aparbalance = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
            decimal aparbalanceidr = tbldtl[0].aparamtidr - (tbldtl[0].amtbayaridr + tbldtl[0].amtdncnidr);
            decimal aparbalanceusd = tbldtl[0].aparamtusd - (tbldtl[0].amtbayarusd + tbldtl[0].amtdncnusd);
            decimal aparamt = tbldtl[0].aparamt;
            decimal aparamtidr = tbldtl[0].aparamtidr;
            decimal aparamtusd = tbldtl[0].aparamtusd;
            decimal aparpaidamt = tbldtl[0].amtbayar + tbldtl[0].amtdncn;
            decimal aparbalanceafter = Convert.ToDecimal(aparbalance) + (tbl.cnamt * (reftype.ToString() != "AR" ? -1 : 1));
            string dbacctgdesc = tbldtl[0].dbacctgdesc;
            string projectname = tbldtl[0].projectname;
            string departemen = tbldtl[0].departemen;

            if (tbl.cnno == "")
            {
                sqlPlus2 = ", cnno='" + transno + "'";
                tbl.cnno = transno;
            }

            var sDate = tbl.cndate;
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Post")
            {
                cRate.SetRateValue(1, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = tbl.cndate;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trncreditnote.Where(x => x.cnoid == tbl.cnoid && x.cnstatus == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.cnstatus = action;
                    tbl.cnamtidr = tbl.cnamt * cRate.GetRateMonthlyIDRValue;
                    tbl.cntaxamtidr = tbl.cntaxamt * cRate.GetRateMonthlyIDRValue;
                    tbl.cnamtusd = tbl.cnamt * cRate.GetRateMonthlyUSDValue;
                    tbl.cntaxamtusd = tbl.cntaxamt * cRate.GetRateMonthlyUSDValue;
                }
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trncreditnote' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trncreditnote SET cnstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', cnamtidr=" + tbl.cnamtidr + ", cntaxamtidr=" + tbl.cntaxamtidr + ", cnamtusd=" + tbl.cnamtusd + ", cntaxamtusd=" + tbl.cntaxamtusd + " " + sqlPlus + " " + sqlPlus2 + " WHERE cnstatus='In Approval' AND cnoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.cnoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trncreditnote";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trncreditnote SET cnstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "', cnamtidr=" + tbl.cnamtidr + ", cntaxamtidr=" + tbl.cntaxamtidr + ", cnamtusd=" + tbl.cnamtusd + ", cntaxamtusd=" + tbl.cntaxamtusd + " " + sqlPlus + " " + sqlPlus2 + " WHERE cnstatus='In Approval' AND cnoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (reftype.ToUpper() == "AP")
                                {
                                    QL_conap conap = new QL_conap();
                                    conap.cmpcode = tbl.cmpcode;
                                    conap.conapoid = conapoid++;
                                    conap.reftype = tbl.reftype;
                                    conap.refoid = tbl.refoid;
                                    conap.payrefoid = tbl.cnoid;
                                    conap.suppoid = tbl.suppcustoid;
                                    conap.acctgoid = tbl.dbacctgoid;
                                    conap.trnapstatus = tbl.cnstatus;
                                    conap.trnaptype = "CN" + reftype;
                                    conap.trnapdate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                    conap.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                                    conap.paymentacctgoid = tbl.cracctgoid;
                                    conap.paymentdate = tbl.cndate;
                                    conap.payrefno = tbl.cnno;
                                    conap.paybankoid = 0;
                                    conap.payduedate = tbl.cndate;
                                    conap.amttrans = 0;
                                    conap.amtbayar = (tbl.cnamt + tbl.cntaxamt) * -1;
                                    conap.trnapnote = "CN No. " + tbl.cnno + " for " + reftype + " No. " + aparno + "";
                                    conap.createuser = tbl.createuser;
                                    conap.createtime = tbl.createtime;
                                    conap.upduser = tbl.upduser;
                                    conap.updtime = tbl.updtime;
                                    conap.amttransidr = 0;
                                    conap.amtbayaridr = (tbl.cnamtidr + tbl.cntaxamtidr) * -1;
                                    conap.amttransusd = 0;
                                    conap.amtbayarusd = (tbl.cnamtusd + tbl.cntaxamtusd) * -1;
                                    conap.amtbayarother = 0;
                                    conap.conflag = "";

                                    db.QL_conap.Add(conap);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_id SET lastoid=" + conap.conapoid + " WHERE tablename='QL_conap'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    QL_conar conar = new QL_conar();
                                    conar.cmpcode = tbl.cmpcode;
                                    conar.conaroid = conaroid++;
                                    conar.reftype = tbl.reftype;
                                    conar.refoid = tbl.refoid;
                                    conar.payrefoid = tbl.cnoid;
                                    conar.custoid = tbl.suppcustoid;
                                    conar.acctgoid = tbl.dbacctgoid;
                                    conar.trnarstatus = tbl.cnstatus;
                                    conar.trnartype = "CN" + reftype;
                                    conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                    conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                                    conar.paymentacctgoid = tbl.cracctgoid;
                                    conar.paymentdate = tbl.cndate;
                                    conar.payrefno = tbl.cnno;
                                    conar.paybankoid = 0;
                                    conar.payduedate = tbl.cndate;
                                    conar.amttrans = 0;
                                    conar.amtbayar = tbl.cnamt + tbl.cntaxamt;
                                    conar.trnarnote = "CN No. " + tbl.cnno + " for " + reftype + " No. " + aparno + "";
                                    conar.createuser = tbl.createuser;
                                    conar.createtime = tbl.createtime;
                                    conar.upduser = tbl.upduser;
                                    conar.updtime = tbl.updtime;
                                    conar.amttransidr = 0;
                                    conar.amtbayaridr = tbl.cnamtidr + tbl.cntaxamtidr;
                                    conar.amttransusd = 0;
                                    conar.amtbayarusd = tbl.cnamtusd + tbl.cntaxamtusd;
                                    conar.amtbayarother = 0;

                                    db.QL_conar.Add(conar);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_id SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }

                                //Insert Into GL Mst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue));
                                db.SaveChanges();

                                int iSeq = 1;
                                //Insert Into GL Dtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.dbacctgoid, "D", tbl.cnamt + tbl.cntaxamt, tbl.cnno, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.upduser, tbl.updtime, tbl.cnamtidr + tbl.cntaxamtidr, tbl.cnamtusd + tbl.cntaxamtusd, "QL_trncreditnote " + tbl.cnoid, "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;

                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.cracctgoid, "C", tbl.cnamt + tbl.cntaxamt, tbl.cnno, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.upduser, tbl.updtime, tbl.cnamtidr + tbl.cntaxamtidr, tbl.cnamtusd + tbl.cntaxamtusd, "QL_trncreditnote " + tbl.cnoid, "", "", "", 0));
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalRABReal(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "RABCostReal";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            List<QL_trncashbankgl> dtDtl = db.QL_trncashbankgl.Where(x => x.cmpcode == CompnyCode && x.cashbankoid == id).ToList();
            sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trncashbankmst' ORDER BY rd.apitemdtl2seq";
            List<listapitemdtl2> dtDtl2 = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();
            
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK" || tbl.cashbanktype == "RBB")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = tbl.cashbankduedate;
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = tbl.cashbankduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.refsuppoid)?.FirstOrDefault() ?? "";
            string fakturnoGL = ""; decimal totaltaxhdr = 0;

            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }
                
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_IN");

                if (dtDtl2 != null)
                {
                    if (dtDtl2.Count > 0)
                    {
                        for (int i = 0; i < dtDtl2.Count(); i++)
                        {
                            totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                            fakturnoGL += dtDtl2[i].fakturno + ",";
                        }
                        fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                    }
                }
                //if (tbl.rabmstoid > 0)
                //{
                //    sSql = "SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid AND cb.cashbankoid<>" + tbl.cashbankoid + "),0.0)) AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.rabdtl5oid ='" + tbl.rabdtl5oid + "'";
                //    var PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                //    if (tbl.cashbankdpp > PriceOs)
                //        error = "Amount Realisasi RAB tidak boleh lebih dari Amount O/S (" + PriceOs + ")";
                //}

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = sDate;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trncashbankmst.Where(x => x.cashbankoid == tbl.cashbankoid && x.cashbankstatus == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.cashbankstatus = action;
                }
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnrabreal' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trncashbankmst SET cashbankstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE cashbankstatus='In Approval' AND cashbankoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trncashbankgl SET cashbankglstatus='" + action + "' WHERE cashbankglstatus='In Approval' AND cashbankoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.cashbankoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnrabreal";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trncashbankmst SET cashbankstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE cashbankstatus='In Approval' AND cashbankoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_trncashbankgl SET cashbankglstatus='" + action + "' WHERE cashbankglstatus='In Approval' AND cashbankoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (dtDtl2.Count > 0)
                                {
                                    for (int i = 0; i < dtDtl2.Count(); i++)
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                                
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, sDate, sPeriod, tbl.cashbankno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                var glseq = 1;
                                //decimal totaltax = Convert.ToDecimal(tblmst.apdirtaxamt);
                                //if (tblmst.apdirtaxamt > 0)
                                //{
                                //    // Insert QL_trngldtl
                                //    // PPN Masukan (D)
                                //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tblmst.apdirno, "AP Direct| Note. PPN ", "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apdirmstoid, null, null, null, 0));
                                //    db.SaveChanges();
                                //}

                                // Insert QL_trngldtl
                                // Biaya Lain - Lain (D)
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    var amt = dtDtl[i].cashbankglamt;
                                    if (amt > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "D", amt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                    else if (amt < 0)
                                    {
                                        amt = dtDtl[i].cashbankglamt * (-1);
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "C", amt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                }

                                decimal totaltax = Convert.ToDecimal(totaltaxhdr);
                                if (totaltax > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0, fakturnoGL));
                                    db.SaveChanges();
                                }

                                // Insert QL_trngldtl
                                // Hutang Dagang Lainnya (C)
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                db.SaveChanges();

                                decimal totalpph = Convert.ToDecimal(tbl.pphamt);
                                if (tbl.pphamt > 0)
                                {
                                    // Insert QL_trngldtl
                                    // PPH (C)
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.pphacctgoid.Value, "C", totalpph, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, totalpph * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalRABRealMulti(int id, string cmp, string transno, string action, out string error)
        {
            if (action == "Approved")
            {
                action = "Post";
            }

            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = ""; string sqlPlus2 = "";
            error = "";

            string ctrlname = "RABCostRealMulti";
            var appoid = ClassFunction.GenerateID("QL_APP");
            List<listappfinal> appfinal = new List<listappfinal>();
            string appStatus = CekStatusApp(ctrlname, Session["UserID"].ToString());
            if (appStatus == "Belum")
            {
                appfinal = BindAppFinal(ctrlname);
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkan Setting Approval Final Dulu!!";
                }
            }

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            List<QL_trncashbankgl> dtDtl = db.QL_trncashbankgl.Where(x => x.cmpcode == CompnyCode && x.cashbankoid == id).ToList();
            sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trncashbankmst' ORDER BY rd.apitemdtl2seq";
            List<listapitemdtl2> dtDtl2 = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK" || tbl.cashbanktype == "RBB")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = tbl.cashbankduedate;
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = tbl.cashbankduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            //Generate No New
            string sNo = tbl.cashbanktype + "/" + tbl.cashbankdate.ToString("yy/MM") + "/";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + int.Parse(DefaultCounter) + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + tbl.acctgoid + "*/";
            tbl.cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultCounter));

            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.refsuppoid)?.FirstOrDefault() ?? "";
            string fakturnoGL = ""; decimal totaltaxhdr = 0;

            var rate2oid = 0;
            var cRate = new ClassRate();
            if (action == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    error = cRate.GetRateMonthlyLastError;
                }

                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    error = ClassFunction.GetInterfaceWarning("VAR_PPN_IN");

                if (dtDtl2 != null)
                {
                    if (dtDtl2.Count > 0)
                    {
                        for (int i = 0; i < dtDtl2.Count(); i++)
                        {
                            totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                            fakturnoGL += dtDtl2[i].fakturno + ",";
                        }
                        fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                    }
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = sDate;//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    error = "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!";
                }
            }

            string errDup = "";
            if (db.QL_trncashbankmst.Where(x => x.cashbankoid == tbl.cashbankoid && x.cashbankstatus == "Post").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {
                if (action == "Post")
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    tbl.cashbankstatus = action;
                }
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "'  WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnrabrealmulti' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trncashbankmst SET cashbankstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE cashbankstatus='In Approval' AND cashbankoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trncashbankgl SET cashbankglstatus='" + action + "' WHERE cashbankglstatus='In Approval' AND cashbankoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus == "Belum")
                        {
                            if (action == "Post")
                            {
                                QL_APP tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP();
                                    tblapp.cmpcode = tbl.cmpcode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.cashbankoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnrabrealmulti";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Post")
                            {
                                sSql = "UPDATE QL_trncashbankmst SET cashbankno='"+ tbl.cashbankno +"', cashbankstatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " " + sqlPlus2 + " WHERE cashbankstatus='In Approval' AND cashbankoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_trncashbankgl SET cashbankglstatus='" + action + "' WHERE cashbankglstatus='In Approval' AND cashbankoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                if (dtDtl2.Count > 0)
                                {
                                    for (int i = 0; i < dtDtl2.Count(); i++)
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }
                                }

                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, sDate, sPeriod, tbl.cashbankno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                var glseq = 1;
                                // Insert QL_trngldtl
                                // Biaya Lain - Lain (D)
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    var amt = dtDtl[i].cashbankglamt;
                                    if (amt > 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "D", amt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                    else if (amt < 0)
                                    {
                                        amt = dtDtl[i].cashbankglamt * (-1);
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "C", amt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }

                                    decimal totalpph = dtDtl[i].pphcreditamt.Value;
                                    if (tbl.pphamt > 0)
                                    {
                                        // Insert QL_trngldtl
                                        // PPH (C)
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].pphcreditacctgoid.Value, "C", totalpph, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, totalpph * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                }

                                decimal totaltax = Convert.ToDecimal(totaltaxhdr);
                                if (totaltax > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0, fakturnoGL));
                                    db.SaveChanges();
                                }

                                if (tbl.cost_type == "KASBON REAL")
                                {
                                    int kb_id = db.QL_trncashbankmst.FirstOrDefault(x => x.cashbankoid == tbl.kasbon_ref_id.Value).cashbankoid;
                                    decimal selisihamt = 0;
                                    decimal total_kasbon = tbl.kasbonrefamt.Value;
                                    decimal total_hb = tbl.kasbonrefamt2.Value;
                                    decimal total_dtl = dtDtl.Sum(x => x.cashbankglamt);
                                    decimal total_pph = dtDtl.Sum(x => x.pphcreditamt) ?? 0;
                                    decimal ppn = tbl.cashbankothertaxamt;

                                    var liskasbon = db.QL_trncashbankgl.Where(w => w.cashbankoid == kb_id).GroupBy(g => new { g.acctgoid }).Select(s => new { acctgoid = s.Key.acctgoid, amt = s.Sum(x => x.cashbankglamt) }).ToList();
                                    if (liskasbon != null && liskasbon.Count > 0)
                                    {
                                        foreach (var k in liskasbon)
                                        {
                                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, k.acctgoid, "C", total_kasbon, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, total_kasbon * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                            db.SaveChanges();
                                        }
                                        //total_kasbon = liskasbon.Sum(x => x.amt);
                                    }
                                    //var sVar = "VAR_UM_OPRASIONAL_BANK";
                                    //if (tbl.cashbanktype == "BKK")
                                    //    sVar = "VAR_UM_OPRASIONAL_KAS";
                                    //int acctgoid_um = int.Parse(ClassFunction.GetDataAcctgOid(sVar, CompnyCode));
                                    //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, acctgoid_um, "C", total_kasbon, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, total_kasbon * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                    //db.SaveChanges();

                                    var lispph = db.QL_trncashbankgl.Where(w => w.cashbankoid == kb_id).GroupBy(g => new { g.pphcreditacctgoid }).Select(s => new { acctgoid = s.Key.pphcreditacctgoid, amt = s.Sum(x => x.pphcreditamt) }).ToList();
                                    if (lispph != null && lispph.Count > 0)
                                    {
                                        foreach (var k in lispph)
                                        {
                                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, k.acctgoid.Value, "D", total_hb, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, total_hb * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                            db.SaveChanges();
                                        }
                                        //total_hb = lispph.Sum(x => x.amt.Value);
                                    }
                                    //int acctgoid_hb = int.Parse(ClassFunction.GetDataAcctgOid("VAR_HUTANG_BIAYA", CompnyCode));
                                    //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, acctgoid_hb, "D", total_hb, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, total_hb * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                    //db.SaveChanges();

                                    selisihamt = (total_kasbon + total_pph) - total_dtl - ppn - total_hb;
                                    if (selisihamt > 0)
                                    {
                                        // Kas Bank (D)
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.acctgoid, "D", selisihamt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                    else if (selisihamt < 0)
                                    {
                                        selisihamt = Math.Abs(selisihamt);
                                        // Kas Bank (C)
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.acctgoid, "C", selisihamt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, selisihamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    // Insert QL_trngldtl
                                    // Kas Bank (C)
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt, tbl.cashbankno, tbl.cashbankno + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }
        #endregion

    }
}