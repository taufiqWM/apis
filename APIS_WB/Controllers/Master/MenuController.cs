﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class MenuController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public MenuController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET/POST: Menu
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select * FROM QL_m04MN WHERE cmpcode='"+ Session["CompnyCode"].ToString() + "' " + sfilter;
            List<QL_m04MN> mstmenu = db.Database.SqlQuery<QL_m04MN>(sSql).ToList();
            return View(mstmenu);
        }
        
        // GET: Menu/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m04MN qL_m04MN ;
            string action = "Create";
            if (id == null)
            {
                qL_m04MN = new QL_m04MN();
                qL_m04MN.cmpcode = Session["CompnyCode"].ToString();
            }
            else
            {
                action = "Edit";
                qL_m04MN = db.QL_m04MN.Find(Session["CompnyCode"].ToString(), id);
            }
            
            if (qL_m04MN == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            return View(qL_m04MN);
        }

        // POST: Menu/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m04MN qL_m04MN, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(qL_m04MN.mnname))
                ModelState.AddModelError("mnname", "Please Fill Menu Name");
            if (string.IsNullOrEmpty(qL_m04MN.mntype))
                ModelState.AddModelError("mntype", "Please Fill Menu Type");
            if (string.IsNullOrEmpty(qL_m04MN.mnmodule))
                qL_m04MN.mnmodule = "";
                //ModelState.AddModelError("mnmodule", "Please Fill Menu Module");
            if (string.IsNullOrEmpty(qL_m04MN.mnfileloc))
                ModelState.AddModelError("mnicon", "Please Fill Menu File Loc");
            if (string.IsNullOrEmpty(qL_m04MN.mnicon))
                ModelState.AddModelError("mnicon", "Please Fill FA Icon Class");
            
            //Generate ID
            var mstoid = ClassFunction.GenerateID("QL_m04MN");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            //qL_m04MN.mnoid = mstoid;sudah auto increment
                            db.QL_m04MN.Add(qL_m04MN);
                            db.SaveChanges();

                            ////Update lastoid
                            //sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m04MN'";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            db.Entry(qL_m04MN).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            return View(qL_m04MN);
        }
        
        // POST: Menu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (db.QL_m02RLdtl.Where(w => w.mnoid == id).Count() > 0)
            {
                result = "failed";
                msg += "this Menu already used in Role";
            }
            else
            {
                QL_m04MN qL_m04MN = db.QL_m04MN.Find(Session["CompnyCode"].ToString(), id);
                db.QL_m04MN.Remove(qL_m04MN);
                db.SaveChanges();
            }
            
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
