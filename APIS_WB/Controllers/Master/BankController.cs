﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class BankController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public BankController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class bank
        {
            public string cmpcode { get; set; }
            public int bankoid { get; set; }
            public string bankcode { get; set; }
            public string bankname { get; set; }
            public string bankbranch { get; set; }
            public string bankphone { get; set; }
            public string bankphone2 { get; set; }
            public string bankemail { get; set; }
            public string bankaddress { get; set; }
            public string banknote { get; set; }
            public string activeflag { get; set; }

        }
        private void InitDDL()
        {
           
        }
        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

    
            sSql = "SELECT * FROM QL_mstbank WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY bankname DESC";
            List<bank> vbag = db.Database.SqlQuery<bank>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstbank tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstbank();
                tbl.bankoid = ClassFunction.GenerateID("QL_mstbank");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();               
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstbank.Find( id);

            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstbank tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.bankcode == "" | tbl.bankcode == null)
            {
                ModelState.AddModelError("bankcode", "Kolom Kode Tidak Boleh Kosong!!!");
            }
            if (tbl.bankname == "" | tbl.bankname == null)
            {
               ModelState.AddModelError("bankname", "Kolom Nama Tidak Boleh Kosong!!!");
            }
            if (tbl.bankaddress == "" | tbl.bankaddress == null)
            {
                ModelState.AddModelError("bankaddress", "Kolom Alamat Tidak Boleh Kosong!!!");
            }
            if (tbl.bankphone == "" | tbl.bankphone == null)
            {
                ModelState.AddModelError("bankphone", "Kolom Alamat Tidak Boleh Kosong!!!");
            }
            if (tbl.bankbranch == "" | tbl.bankbranch == null)
            {
                tbl.bankbranch = "";
            }
            if (tbl.bankphone2 == "" | tbl.bankphone2 == null)
            {
                tbl.bankphone2 = "";
            }
            if (tbl.bankemail == "" | tbl.bankemail == null)
            {
                tbl.bankemail = "";
            }
            if (tbl.banknote == "" | tbl.banknote == null)
            {
                tbl.banknote = "";
            }
            var mstoid = ClassFunction.GenerateID("QL_mstbank");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.bankoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstbank.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstbank'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                            
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstbank tbl = db.QL_mstbank.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstbank.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
