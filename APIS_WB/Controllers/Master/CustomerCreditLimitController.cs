﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Master
{
    public class CustomerCreditLimitController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public CustomerCreditLimitController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class custmst
        {
            public string cmpcode { get; set; }
            public int custhistoid { get; set; }
            public DateTime custhistdate { get; set; }
            public string custcode { get; set; }
            public String custname { get; set; }
            public string custhistnote { get; set; }
            public string custhiststatus { get; set; }
        }

        public class custdtl1
        {
            public int custdtl1seq { get; set; }
            public string custdtl1picname { get; set; }
            public string custdtl1phone1 { get; set; }
            public string custdtl1phone2 { get; set; }
            public string custdtl1email { get; set; }
            public string custdtl1jabatan { get; set; }
        }

        public class custdtl2
        {
            public int custdtl2seq { get; set; }
            public string custdtl2loc { get; set; }
            public string custdtl2addr { get; set; }
            public int custdtl2cityoid { get; set; }            
            public string custdtl2postcode { get; set; }
            public string custdtl2picitem { get; set; }
            public string custdtl2phone1 { get; set; }
            public string custdtl2phone2 { get; set; }
            public string custdtl2email { get; set; }            
        }

        private void InitDDL(QL_mstcusthist tbl)
        {

            sSql = "select * from ql_m05gn where gngroup='PAYMENT TERM' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var custpaymentoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", "custpaymentoid");
            ViewBag.custpaymentoid = custpaymentoid;

            sSql = "select * from ql_m05gn where gngroup='PROVINSI' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var custprovinceoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", "custprovinceoid");
            ViewBag.custprovinceoid = custprovinceoid;
            
            sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", "custcityoid");
            ViewBag.custcityoid = cityoid;
          
        }

        private void FillAdditionalField(QL_mstcusthist tblmst)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.custcode = db.Database.SqlQuery<string>("SELECT custcode FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.custphone1 = db.Database.SqlQuery<string>("SELECT custphone1 FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.custemail = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.custaddr = db.Database.SqlQuery<string>("SELECT custaddr FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }

        [HttpPost]
        public ActionResult InitDDLCity()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'KOTA' and gnflag = 'ACTIVE' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
      

        // GET: Customer
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT *, c.custname, c.custcode FROM QL_mstcusthist ch INNER JOIN QL_mstcust c ON c.custoid = ch.custoid WHERE ch.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY custhistoid DESC";
            List<custmst> vbag = db.Database.SqlQuery<custmst>(sSql).ToList();
            return View(vbag);
        }

        // GET: Customer/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcusthist tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstcusthist();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.custhiststatus = "In Process";                
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstcusthist.Find(Session["CompnyCode"].ToString(), id);             
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: Customer/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcusthist tbl, string action, string tglupd)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.custhistdate = DateTime.Parse(ClassFunction.toDate(tglupd));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("custhistdate", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }
            //Is Input Valid?               

            if (string.IsNullOrEmpty(tbl.custhistnote))
                tbl.custhistnote = "";
            if (string.IsNullOrEmpty(tbl.custhistcode))
                tbl.custhistcode = "";
            if (tbl.custcreditlimitnew == 0)
            {
                ModelState.AddModelError("custcreditlimit", "Credit Limit tidak Boleh 0!");
            }
            
            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcusthist");

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.custhiststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            sSql = "SELECT COUNT(-1) FROM QL_mstcusthist where custoid = "+ tbl.custoid +" AND custhistoid <> "+ tbl.custhistoid +" AND custhiststatus NOT IN ('Approved', 'Rejected')";
            var count = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (count > 0)
            {
                ModelState.AddModelError("", "Ada transaksi Customer Credit Limit yang masih outstanding, Silahkan tekan Cancel dan cek di List Customer Credit Limit!");
            }

            if (ModelState.IsValid)
            {             
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                       

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.custhistoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcusthist.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcusthist'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }                        

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.custhiststatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.custhistoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_mstcusthist";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        tbl.custhiststatus = "In Process";
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            else
            {
                tbl.custhiststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
            //ViewBag(QL_mstcustdtl1);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcusthist tbl = db.QL_mstcusthist.Find(Session["CompnyCode"].ToString(),id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}