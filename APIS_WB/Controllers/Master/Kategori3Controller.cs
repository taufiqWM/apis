﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class Kategori3Controller : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public Kategori3Controller()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ListCat3
        {
            public string cat1code { get; set; }
            public string cat2code { get; set; }
            public string cat3code { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat3note { get; set; }
            public string activeflag { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }

        }

        private void setViewBag(QL_mstcat3 qL_mstcat3, string action)
        {
            string sPlus = "";
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>("SELECT * FROM QL_mstcat1 WHERE activeflag='ACTIVE'").ToList(), "cat1code", "cat1shortdesc", qL_mstcat3.cat1code);
            ViewBag.cat1code = cat1code;
            if (action == "Edit")
            {
                sPlus = " AND cat1code='" + qL_mstcat3.cat1code + "'";
            }
            var cat2code = new SelectList(db.Database.SqlQuery<QL_mstcat2>("SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE' "+ sPlus+"").ToList(), "cat2code", "cat2shortdesc", qL_mstcat3.cat2code);
            ViewBag.cat2code = cat2code;
        }

        [HttpPost]
        public ActionResult Getcat2(string cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE' AND cat1code='" + cat1 + "' ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND c3.activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT c3.*, c1.cat1shortdesc, c1.cat1code, c2.cat2shortdesc, c2.cat2code FROM QL_mstcat3 c3 inner join ql_mstcat1 c1 on c3.cat1code=c1.cat1code inner join ql_mstcat2 c2 on c3.cat2code=c2.cat2code AND c2.cat1code = c1.cat1code WHERE c3.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and c3.cat3code<>'0' ORDER BY cat3shortdesc";
            List<ListCat3> vbag = db.Database.SqlQuery<ListCat3>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(string c3, string c2, string c1)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat3 tbl;
            string action = "Create";
            if (c3 == null)
            {
                tbl = new QL_mstcat3();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tbl = db.QL_mstcat3.Find(c3, c2, c1);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tbl, action);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat3 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (tbl.cat1code == "")
                ModelState.AddModelError("cat1code", "Kategori 1 Tidak Boleh Kosong !!!");
            if (tbl.cat2code == "")
                ModelState.AddModelError("cat2code", "Kategori 2 Tidak Boleh Kosong !!!");
            if (string.IsNullOrEmpty(tbl.cat3code))
                ModelState.AddModelError("cat3code", "Silahkan isi Kode!");
            else
            {
                if (action == "Create")
                {
                    if (db.QL_mstcat3.Where(w => w.cat3code == tbl.cat3code && w.cat2code == tbl.cat2code && w.cat1code == tbl.cat1code).Count() > 0)
                        ModelState.AddModelError("cat3code", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
                }
            }
            if (string.IsNullOrEmpty(tbl.cat3shortdesc))
                ModelState.AddModelError("cat3shortdesc", "Silahkan isi Deskripsi!");
            else if (db.QL_mstcat3.Where(w => w.cat3shortdesc == tbl.cat3shortdesc && w.cat3code != tbl.cat3code && w.cat2code == tbl.cat2code && w.cat1code == tbl.cat1code).Count() > 0)
                ModelState.AddModelError("cat3shortdesc", "Deskripsi yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Deskripsi lainnya!");
            if (string.IsNullOrEmpty(tbl.cat3note))
                tbl.cat3note = "";
            if (tbl.cat3code.Length != 3)
                ModelState.AddModelError("cat3code", "Kode Maximum 3 Karakter!");

            tbl.cat3longdesc = tbl.cat3shortdesc;

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcat3");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcat3.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat3'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tbl, action);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string c3, string c2, string c1)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat3 tbl = db.QL_mstcat3.Find(c3, c2, c1);
                        db.QL_mstcat3.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
