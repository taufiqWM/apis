﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.IO;
using System.Threading.Tasks;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class BarangController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string itempath = "~/Images/Barang";
        private string imgtemppath = "~/Images/ImagesTemps";

        public BarangController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class item
        {
            public string cmpcode { get; set; }
            public int itemoid { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }
            public string cat4shortdesc { get; set; }
            public string itemcode { get; set; }
            public string itemunit { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string activeflag { get; set; }

        }
        public class itemdtl
        {
            public int itemdtlseq { get; set; }
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }   
        }

        public class itemdtl2
        {
            public int itemdtl2seq { get; set; }
            public int itemrefoid { get; set; }
            public string itemrefcode { get; set; }
            public string itemrefdesc { get; set; }
            public int itemrefunitoid { get; set; }
            public string itemrefunit { get; set; }
            public string itemtype { get; set; }

        }

        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        private void InitDDL(QL_mstitem tbl)
        {
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + tbl.cmpcode + "' AND activeflag='ACTIVE'";
            var cat1oid = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1code", "cat1shortdesc", tbl.cat1code);
            ViewBag.cat1code = cat1oid;
            sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + tbl.cmpcode + "' AND cat1code='" + tbl.cat1code + "' AND activeflag='ACTIVE'";
            var cat2oid = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2code", "cat2shortdesc", tbl.cat2code);
            ViewBag.cat2code = cat2oid;
            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + tbl.cmpcode + "' AND cat1code= '" + tbl.cat1code + "'  AND cat2code= '" + tbl.cat2code + "' AND  activeflag='ACTIVE'";
            var cat3oid = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3code", "cat3shortdesc", tbl.cat3code);
            ViewBag.cat3code = cat3oid;
            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + tbl.cmpcode + "' AND cat1code= '" + tbl.cat1code + "'  AND cat2code= '" + tbl.cat2code + "'   AND cat3code= '" + tbl.cat3code + "' AND activeflag ='ACTIVE'";
            var cat4oid = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4shortdesc", tbl.cat4code);
            ViewBag.cat4code = cat4oid;
            sSql = "select * from QL_m05GN g where g.gngroup = 'SATUAN' AND gnflag='ACTIVE'";
            var itemunitoid= new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.itemunitoid);
            ViewBag.itemunitoid = itemunitoid;

            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst_draft a WHERE a.rabmstoid ='" + tbl.rabmstoid_draft + "'").FirstOrDefault();

            var assetacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_ASSET")).ToList(), "acctgoid", "acctgdesc", tbl.assetacctgoid);
            ViewBag.assetacctgoid = assetacctgoid;
        }

        public class rabmst
        {
            public int rabmstoid_draft { get; set; }
            public int rabdtloid_draft { get; set; }
            public int rabdtloid2_draft { get; set; }
            public string rabno { get; set; }
            public string rabtype { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }

        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<rabmst> tbl = new List<rabmst>();

            sSql = "SELECT rabmstoid_draft, SUM(rabdtloid_draft) rabdtloid_draft, SUM(rabdtloid2_draft) rabdtloid2_draft, rabno, rabtype, projectname, custname, itemdesc, itemtype FROM( SELECT rd.rabmstoid rabmstoid_draft, rabdtloid_draft, 0 rabdtloid2_draft, rabno, rabtype, projectname, custname, rd.itemdtldesc itemdesc, ISNULL(rd.itemdtltype, 'Barang') itemtype FROM QL_trnrabmst_draft rm INNER JOIN QL_trnrabdtl_draft rd ON rd.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid WHERE rm.rabmststatus IN('Post', 'Approved') AND ISNULL(rd.itemoid, 0)= 0 AND ISNULL(rabdtlflag_m,'')= 'Manual'  UNION ALL SELECT rd.rabmstoid rabmstoid_draft, 0 rabdtloid_draft, rabdtloid2_draft, rabno, rabtype, projectname, custname, rd.itemdtl2desc itemdesc, 'Barang' itemtype FROM QL_trnrabmst_draft rm INNER JOIN QL_trnrabdtl2_draft rd ON rd.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid WHERE rm.rabmststatus IN('Post', 'Approved') AND ISNULL(rd.itemdtl2oid, 0)= 0 AND ISNULL(rabdtl2flag_m,'')= 'Manual' )AS tb GROUP BY rabmstoid_draft, rabno, rabtype, projectname, custname, itemdesc, itemtype";
            tbl = db.Database.SqlQuery<rabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: Kategory
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
    
            return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";
            sSql = "SELECT i.itemoid [oid], i.itemcode [Code], c1.cat1shortdesc [Kategori 1], c2.cat2shortdesc [Kategori 2], c3.cat3shortdesc [Kategori 3], c4.cat4shortdesc [Kategori 4], i.itemdesc [Deskripsi], g.gndesc [Unit], i.activeflag [Status], i.itemtype+'/'+i.pidtype [Tipe Barang] FROM QL_mstitem i INNER JOIN QL_mstcat4 c4 on i.cat4code = c4.cat4code AND i.cat3code = c4.cat3code AND i.cat2code = c4.cat2code AND i.cat1code = c4.cat1code INNER JOIN QL_mstcat3 c3 on c3.cat3code = c4.cat3code AND c3.cat2code = c4.cat2code AND c3.cat1code = c4.cat1code INNER JOIN ql_mstcat2 c2 on c2.cat2code = c4.cat2code AND c2.cat1code = c4.cat1code INNER JOIN ql_mstcat1 c1 on c1.cat1code = c4.cat1code INNER JOIN QL_m05gn g ON g.gnoid = i.itemunitoid WHERE i.cmpcode = '" + CompnyCode + "' ORDER BY itemcode DESC";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstitem");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Code")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["oid"].ToString(), "Barang") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstitem tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstitem();
                tbl.itemoid = ClassFunction.GenerateID("QL_mstitem");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.itempricesrp  = 0;
                tbl.itempricedealer = 0;
                tbl.itempricemd = 0;
                tbl.itempricemdminqty = 0;
                tbl.assetacctgoid = 0;
                Session["QL_mstitemdtl"] = null;
                Session["QL_mstitemdtl2"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstitem.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT 0 itemdtlseq, s.suppoid ,suppcode, suppname FROM QL_mstitemdtl md INNER JOIN QL_mstsupp s ON s.suppoid=md.suppoid WHERE md.itemoid="+ id +" ORDER BY suppname";
                Session["QL_mstitemdtl"] = db.Database.SqlQuery<itemdtl>(sSql).ToList();
                List<itemdtl> dtDtl = (List<itemdtl>)Session["QL_mstitemdtl"];
                if (dtDtl != null)
                {
                    if (dtDtl.Count > 0)
                    {
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            dtDtl[i].itemdtlseq = i + 1;
                        }
                    }
                }

                sSql = "SELECT 0 itemdtl2seq, md.itemrefoid, i.itemcode itemrefcode, i.itemdesc itemrefdesc, md.itemrefunitoid, g.gndesc itemrefunit FROM QL_mstitemdtl2 md INNER JOIN QL_mstitem i ON i.itemoid=md.itemrefoid INNER JOIN QL_m05GN g ON g.gnoid=md.itemrefunitoid WHERE md.itemoid=" + id + " ORDER BY i.itemcode";
                Session["QL_mstitemdtl2"] = db.Database.SqlQuery<itemdtl2>(sSql).ToList();
                List<itemdtl2> dtDtl2 = (List<itemdtl2>)Session["QL_mstitemdtl2"];
                if (dtDtl2 != null)
                {
                    if (dtDtl2.Count > 0)
                    {
                        for (int i = 0; i < dtDtl2.Count(); i++)
                        {
                            dtDtl2[i].itemdtl2seq = i + 1;
                        }
                    }
                }
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstitem tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.cat1code))
                ModelState.AddModelError("cat1code", "Silahkan isi Kategory 1!");
            if (string.IsNullOrEmpty(tbl.cat2code))
                ModelState.AddModelError("cat2code", "Silahkan isi Kategory 2!");
            if (string.IsNullOrEmpty(tbl.cat3code))
                ModelState.AddModelError("cat3code", "Silahkan isi Kategory 3!");
            if (string.IsNullOrEmpty(tbl.cat4code))
                ModelState.AddModelError("cat4code", "Silahkan isi Kategory 4!");
            if (string.IsNullOrEmpty(tbl.itemspec))
                tbl.itemspec = "";
            if (tbl.itemtype != "Asset")
                tbl.assetacctgoid = 0;

            List<itemdtl> dtDtl = (List<itemdtl>)Session["QL_mstitemdtl"];

            List<itemdtl2> dtDtl2 = (List<itemdtl2>)Session["QL_mstitemdtl2"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Please fill detail data!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Please fill detail data!");

            var mstoid = ClassFunction.GenerateID("QL_mstitem");
            var dtloid = ClassFunction.GenerateID("QL_mstitemdtl");
            var dtl2oid = ClassFunction.GenerateID("QL_mstitemdtl2");
            var servertime = ClassFunction.GetServerTime();

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    //Move File from temp to real Path
                    if (System.IO.File.Exists(Server.MapPath(tbl.itemimgfile)))
                    {
                        var sExt = Path.GetExtension(tbl.itemimgfile);
                        var sdir = Server.MapPath(itempath);
                        var sfilename = "FOTO_BARANG_" + (action == "New Data" ? mstoid : tbl.itemoid) + sExt;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        if (tbl.itemimgfile.ToLower() != (itempath + "/" + sfilename).ToLower())
                        {
                            System.IO.File.Delete(sdir + "/" + sfilename);
                            System.IO.File.Move(Server.MapPath(tbl.itemimgfile), sdir + "/" + sfilename);
                        }
                        tbl.itemimgfile = itempath + "/" + sfilename;
                    }

                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.itemoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstitem.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstitem'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstitemdtl.Where(a => a.itemoid == tbl.itemoid && a.cmpcode == tbl.cmpcode);
                            db.QL_mstitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_mstitemdtl2.Where(a => a.itemoid == tbl.itemoid && a.cmpcode == tbl.cmpcode);
                            db.QL_mstitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        if (tbl.rabdtloid_draft != null)
                        {
                            sSql = "UPDATE QL_trnrabdtl_draft SET itemoid=" + tbl.itemoid + " WHERE ISNULL(rabdtloid_draft,0)="+ (tbl.rabdtloid_draft ?? 0) +"";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.rabdtloid2_draft != null)
                        {
                            sSql = "UPDATE QL_trnrabdtl2_draft SET itemdtl2oid=" + tbl.itemoid + " WHERE ISNULL(rabdtloid2_draft,0)=" + (tbl.rabdtloid2_draft ?? 0) + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                QL_mstitemdtl tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = new QL_mstitemdtl();
                                    tbldtl.cmpcode = tbl.cmpcode;
                                    tbldtl.itemdtloid = dtloid++;
                                    tbldtl.itemoid = tbl.itemoid;
                                    tbldtl.suppoid = dtDtl[i].suppoid;
                                    tbldtl.picoid = 0;
                                    tbldtl.upduser = tbl.upduser;
                                    tbldtl.updtime = tbl.updtime;

                                    db.QL_mstitemdtl.Add(tbldtl);
                                }
                                dtloid = dtloid - 1;
                                sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_mstitemdtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_mstitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_mstitemdtl2();
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.itemdtl2oid = dtl2oid++;
                                    tbldtl2.itemoid = tbl.itemoid;
                                    tbldtl2.itemrefoid = dtDtl2[i].itemrefoid;
                                    tbldtl2.itemrefunitoid = dtDtl2[i].itemrefunitoid;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;

                                    db.QL_mstitemdtl2.Add(tbldtl2);
                                }
                                dtl2oid = dtl2oid - 1;
                                sSql = "UPDATE QL_ID SET lastoid=" + dtl2oid + " WHERE tablename='QL_mstitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstitemdtl.Where(a => a.itemoid == id);
                        db.QL_mstitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_mstitemdtl2.Where(a => a.itemoid == id);
                        db.QL_mstitemdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        QL_mstitem tbl = db.QL_mstitem.Find(Session["CompnyCode"].ToString(), id);

                        if (tbl != null)
                        {
                            System.IO.File.Delete(Server.MapPath(tbl.itemimgfile));
                        }

                        db.QL_mstitem.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        public ActionResult Getcat2(string cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE' AND cat1code='" + cat1 + "' ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat3(string cat1, string cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            sSql = "SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE' AND cat1code='" + cat1 + "' AND cat2code='" + cat2 + "' ORDER BY cat3code";
            tbl = db.Database.SqlQuery<QL_mstcat3>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat4(string cat1, string cat2, string cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            sSql = "SELECT * FROM QL_mstcat4 WHERE activeflag='ACTIVE' AND cat1code='" + cat1 + "' AND cat2code='" + cat2 + "' AND cat3code='" + cat3 + "' ORDER BY cat4code";
            tbl = db.Database.SqlQuery<QL_mstcat4>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public ActionResult Getcode(string code, string itemtype)
        {
            string sNo = code;
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(itemcode,18), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstitem WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND itemcode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;

            if (itemtype == "Rakitan")
            {
                sNo = sNo + "P";
            }
          
            return Json(sNo);
        }

        [HttpPost]
        public ActionResult GetSuppData()
        {
            List<itemdtl> tbl = new List<itemdtl>();

            sSql = "SELECT 0 itemdtlseq, s.suppoid, suppcode, suppname FROM QL_mstsupp s WHERE activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<itemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getdtl2Data(string cmp)
        {
            List<itemdtl2> tbl = new List<itemdtl2>();

            sSql = "SELECT 0 itemdtl2seq, i.itemoid itemrefoid, itemcode itemrefcode, itemdesc itemrefdesc, itemunitoid itemrefunitoid, g.gndesc itemrefunit, itemtype +'/'+ pidtype itemtype FROM QL_mstitem i inner join QL_m05GN g on g.gnoid = i.itemunitoid WHERE activeflag='ACTIVE' AND i.itemtype='Barang' ORDER BY itemdesc";
            tbl = db.Database.SqlQuery<itemdtl2>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<itemdtl> dtDtl, List<itemdtl2> dtDtl2)
        {
            Session["QL_mstitemdtl"] = dtDtl;
            Session["QL_mstitemdtl2"] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<itemdtl> dtDtl)
        {
            Session["QL_mstitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<itemdtl2> dtDtl)
        {
            Session["QL_mstitemdtl2"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstitemdtl"] == null)
            {
                Session["QL_mstitemdtl"] = new List<itemdtl>();
            }

            List<itemdtl> dataDtl = (List<itemdtl>)Session["QL_mstitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2()
        {
            if (Session["QL_mstitemdtl2"] == null)
            {
                Session["QL_mstitemdtl2"] = new List<itemdtl2>();
            }

            List<itemdtl2> dataDtl = (List<itemdtl2>)Session["QL_mstitemdtl2"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintXlsReport(string typeexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (typeexp == "Barang")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptBarangXls.rpt"));
            }
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "Export_" + typeexp + ".xls");
        }
    }
}
