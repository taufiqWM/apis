﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FJNonPIDReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public FJNonPIDReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY rm.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, arm.cmpcode, arm.aritemno [FJ No], CONVERT(CHAR(10),arm.aritemdate,103) [FJ Date], c.custname [Customer], arm.aritemmstnote [FJ Note] FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY arm.aritemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT rm.custoid FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomor, string TextNomor, string StartPeriod, string EndPeriod, string DDLPeriod, string DDLPono, string TextNomorPO)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnaritemdtl ard ON ard.itemoid=m.itemoid INNER JOIN QL_trnaritemmst arm ON ard.aritemmstoid=arm.aritemmstoid INNER JOIN QL_trnrabmst rm ON rm.cmpcode=rd.cmpcode AND rm.rabmstoid=arm.rabmstoid INNER JOIN QL_m05GN g ON g.gnoid=ard.aritemunitunitoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomorPO != "")
                {
                    var filter = "arm.aritemno";
                    if (DDLPono == "[armstoid]")
                    {
                        filter = "arm.aritemmstoid";
                    }
                    string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + filter + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptARSumPdf.rpt";
                rptname = "FJ_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptARNonPIDDtlXls.rpt";
                }
                else
                {
                    rptfile = "rptARNonPIDDtlPdf.rpt";
                }
                rptname = "FJ_DETAIL_HIDE_PID";
            }

            var Slct = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Slct += " ,ard.aritemdtloid [AR Dtl Oid], ard.aritemdtlseq [Seq], ard.itemoid [Mat Oid], m.itemcode [Mat Code] , m.itemdesc [Mat Longdesc], aritemqty [Qty] , g2.gndesc [Unit], aritemprice [Price], aritemdtlamt [Detail Amount], aritemdtldisctype [Disc Type], aritemdtldiscvalue [Disc Value], aritemdtldiscamt [Disc Amount], '' [Disc Type 2], 0.00 [Disc Value 2], 0.00 [Disc Amount 2], aritemdtlnetto [Dtl Netto], aritemdtlnote [AR Detail Note], 1 AS [Convert Qty], ((aritemqty + 0.00) * 1) AS [Qty Dtl], '' AS [Unit Dtl] ";
                Join += " INNER JOIN QL_trnaritemdtl ard ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid INNER JOIN QL_mstitem m ON m.itemoid=ard.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid=ard.aritemunitoid ";

                Slct += ", shm.shipmentitemmstoid [Shipment Oid],shipmentitemno [Shipment No.], shipmentitemdate [Shipment Date], shipmentitemvalueidr [PriceSJIDR], shipmentitemvalueusd [PriceSJUSD], '' [Shipment Container No.], ISNULL((SELECT usname FROM QL_m01US dr WHERE dr.usoid=shm.driveroid),'') [Shipment Driver], (SELECT vhcdesc FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Vehicle], (SELECT vhcno FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Police No.], shm.shipmentitemmststatus [Shipment Status], arm.aritemfaktur [Shipment SJ Pajak], '' [Shipment Seal No.], shipmentitemmstnote[Shipment Header Note], shm.createuser[Shipment Create User], shm.createtime[Shipment Create Datetime], '' AS[Shipment User Approval] , CURRENT_TIMESTAMP AS[Shipment ApprovalDate] ";

                Join += " INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentitemmstoid=ard.shipmentitemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid ";

                Slct += " , '' AS [Divisi] ,som.soitemmstoid [SO ID], soitemno [SO No.], soitemdate [SO Date], ((SELECT gndesc FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnoid=soitempaytypeoid)) [SO Payment Type] , '' [SO Price Type], som.soitemtype [SO Local/Exp], '' AS [SO Direct Cust PO No.], 0 AS [Payment Method], CURRENT_TIMESTAMP [SO ETD], '' [Invoice Value],'' [SO Account No.], '' [Port Of Ship], '' [Port Of Dicharge], soitemmstnote [SO Header Note], soitemmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], CURRENT_TIMESTAMP AS [App User SO], CURRENT_TIMESTAMP AS [App Date SO]";
            }

            sSql = "SELECT [armstoid], [Draft No.], [AR No.], [AR Date], [Upd Date], [AR Date Take Giro], [Status], [CmpCode], [Custoid],  [Custcode], [Customer], [Payment Type],  [Currency], [Rate IDR], [Rate USD], [AR Total Amt], [AR Total Disc], [Total Netto], [Tax Type], [AR Tax Value], [AR Tax Amount], [AR Grand Total], [BU Name], [AR Header Note], [Invoice No],  [AR Create User],  [AR Create Time], [AR User Approval] , [AR ApprovalDate], [No. Faktur], [Date SO], [No SO], [Total SO],  [SJ NO], [SJ Date], [Bayar NO], [Bayar Date], [Bayar Amt], 0 [AR Dtl Oid], 0 [Seq],[Mat Oid],  [Mat Code], [Mat Longdesc], [Unit], [Price], SUM([Qty]) [Qty], SUM([Detail Amount]) [Detail Amount], SUM([Dtl Netto]) [Dtl Netto], SUM([Qty Dtl]) [Qty Dtl], [Disc Type], [Disc Value], [Disc Amount], [Disc Type 2], [Disc Value 2], [Disc Amount 2], '' [AR Detail Note],  [Convert Qty], [Unit Dtl] ,  [Shipment Oid], [Shipment No.],  [Shipment Date],  [PriceSJIDR], [PriceSJUSD],  [Shipment Container No.],  [Shipment Driver], [Shipment Vehicle], [Shipment Police No.], [Shipment Status],  [Shipment SJ Pajak], [Shipment Seal No.], [Shipment Header Note], [Shipment Create User], [Shipment Create Datetime], [Shipment User Approval] ,  [Shipment ApprovalDate]  ,  [Divisi] , [SO ID],  [SO No.],  [SO Date], [SO Payment Type] ,  [SO Price Type], [SO Local/Exp],  [SO Direct Cust PO No.], [Payment Method],  [SO ETD], [Invoice Value], [SO Account No.],  [Port Of Ship], [Port Of Dicharge],  [SO Header Note], [SO Status], [SO Create User],  [SO Create Date],  [App User SO],  [App Date SO],  [No.RAB],  [RAB Date],  [Project], [Sales] FROM ( SELECT arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], arm.updtime AS[Upd Date], CURRENT_TIMESTAMP [AR Date Take Giro], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], arm.aritemratetoidr [Rate IDR], cast(arm.aritemratetousd as Float) [Rate USD], arm.aritemtotalamt [AR Total Amt], arm.aritemtotaldisc [AR Total Disc], arm.aritemtotalnetto [Total Netto], arm.aritemtaxtype [Tax Type], arm.aritemtaxvalue [AR Tax Value], arm.aritemtaxamt [AR Tax Amount], arm.aritemgrandtotal [AR Grand Total], ISNULL((SELECT divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode),'') AS [BU Name], arm.aritemmstnote AS [AR Header Note], ISNULL(arm.aritemmstres3,'') AS [Invoice No], arm.createuser [AR Create User], arm.createtime [AR Create Time], '' AS [AR User Approval] , CURRENT_TIMESTAMP AS [AR ApprovalDate], aritemfaktur [No. Faktur], som.soitemdate [Date SO], som.soitemno [No SO], som.soitemgrandtotalamt [Total SO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemno FROM (SELECT DISTINCT CAST (cbx.shipmentitemno AS VARCHAR) shipmentitemno FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ NO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.shipmentitemdate,103) shipmentitemdate FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ Date], ISNULL(STUFF((SELECT ', '+ft.cashbankno FROM (SELECT DISTINCT CAST (cbx.cashbankno AS VARCHAR) cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar NO], ISNULL(STUFF((SELECT ', '+ft.cashbankdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.cashbankdate,103) cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar Date], ISNULL((SELECT SUM(cbx.cashbankamt) FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM'),0.0) [Bayar Amt] " + Slct + ", ISNULL(rabno,'') [No.RAB], ISNULL(rm.rabdate,'') [RAB Date], ISNULL(rm.projectname,'') [Project], ISNULL(us.usname,'') [Sales] FROM QL_trnaritemmst arm " + Join + " INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid WHERE arm.cmpcode='" + CompnyCode + "' ";
            sSql += " ) AS ar WHERE [CmpCode]='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND [Status] IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND [Custcode] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorPO != "")
            {
                string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND [Mat Code] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " GROUP BY [armstoid], [Draft No.], [AR No.], [AR Date], [Upd Date], [AR Date Take Giro], [Status], [CmpCode], [Custoid],  [Custcode], [Customer], [Payment Type],  [Currency], [Rate IDR], [Rate USD], [AR Total Amt], [AR Total Disc], [Total Netto], [Tax Type], [AR Tax Value], [AR Tax Amount], [AR Grand Total], [BU Name], [AR Header Note], [Invoice No],  [AR Create User],  [AR Create Time], [AR User Approval] , [AR ApprovalDate], [No. Faktur], [Date SO], [No SO], [Total SO],  [SJ NO], [SJ Date], [Bayar NO], [Bayar Date], [Bayar Amt],[Mat Oid],  [Mat Code], [Mat Longdesc], [Unit], [Price], [Disc Type], [Disc Value], [Disc Amount], [Disc Type 2], [Disc Value 2], [Disc Amount 2],  [Convert Qty], [Unit Dtl] ,  [Shipment Oid], [Shipment No.],  [Shipment Date],  [PriceSJIDR], [PriceSJUSD],  [Shipment Container No.],  [Shipment Driver], [Shipment Vehicle], [Shipment Police No.], [Shipment Status],  [Shipment SJ Pajak], [Shipment Seal No.], [Shipment Header Note], [Shipment Create User], [Shipment Create Datetime], [Shipment User Approval] ,  [Shipment ApprovalDate]  ,  [Divisi] , [SO ID],  [SO No.],  [SO Date], [SO Payment Type] ,  [SO Price Type], [SO Local/Exp],  [SO Direct Cust PO No.], [Payment Method],  [SO ETD], [Invoice Value], [SO Account No.],  [Port Of Ship], [Port Of Dicharge],  [SO Header Note], [SO Status], [SO Create User],  [SO Create Date],  [App User SO],  [App Date SO],  [No.RAB],  [RAB Date],  [Project], [Sales]";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary Status Amount" || DDLType == "Summary2" || DDLType == "Summary3")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary Status Amount" || DDLType == "Summary2" || DDLType == "Summary3")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else if (DDLType == "Detail")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}