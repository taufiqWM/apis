﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class RABDraftController : Controller
    {
       private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public RABDraftController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listDDLsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
        }

        private void InitDDL(QL_trnrabmst_draft tbl)
        {
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS int)";
            var rabpaytermoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabpaytermoid);
            ViewBag.rabpaytermoid = rabpaytermoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PROVINSI' AND gnflag='ACTIVE'";
            var rabprovoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabprovoid);
            ViewBag.rabprovoid = rabprovoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' AND gnflag='ACTIVE'";
            var rabcityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabcityoid);
            ViewBag.rabcityoid = rabcityoid;

            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>("SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'").ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND ISNULL(gnother1,'')='Komersil' AND gnflag='ACTIVE'";
            var whdtl2oid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.whdtl2oid);
            ViewBag.whdtl2oid = whdtl2oid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesadminoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesadminoid);
            ViewBag.salesadminoid = salesadminoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            var m_location_id = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.markup = "";
            foreach (var item in m_location_id)
            {
                ViewBag.markup += "<option value=\'" + item.Value + "\'>" + item.Text + "</option>";
            }
        }

        private void FillAdditionalField(QL_trnrabmst_draft tblmst)
        {
            ViewBag.partnername = db.Database.SqlQuery<string>("SELECT partnername FROM QL_mstpartner a WHERE a.partneroid ='" + tblmst.partneroid + "'").FirstOrDefault();
            ViewBag.salesname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tblmst.salesoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.ToEmail = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 a WHERE a.custdtl2oid ='" + tblmst.alamatoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT reqrabno FROM QL_trnreqrabmst a WHERE a.reqrabmstoid ='" + tblmst.reqrabmstoid + "'").FirstOrDefault();
            ViewBag.mrserviceno = db.Database.SqlQuery<string>("SELECT mrserviceno FROM QL_trnmrservicemst a WHERE a.mrservicemstoid ='" + tblmst.mrservicemstoid + "'").FirstOrDefault();
            ViewBag.revrabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst_draft a WHERE a.rabmstoid ='" + tblmst.rabmstoid_revisi + "'").FirstOrDefault();
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "RAB-DR/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(rabno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnrabmst_draft WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND rabno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public class rabdtl_draft : QL_trnrabdtl_draft
        {
            public string itemcode { get; set; }
            public string itemspec { get; set; }
            public string rabunit { get; set; }
            public decimal stockqty { get; set; }
            public decimal itempricesrp { get; set; }
            public decimal itempricedealer { get; set; }
            public decimal itempricemd { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<rabdtl_draft> tbl = new List<rabdtl_draft>();

            sSql = "SELECT 0 rabseq, 'Master' rabdtlflag_m, i.itemoid, i.itemtype itemdtltype, i.itemcode, i.itemdesc itemdtldesc, 0.0 rabqty, i.itemunitoid rabunitoid, g.gndesc rabunit, 0.0 rabprice, 0.0 rabdtlamt, 10.0 rabtaxvalue, 0.0 rabtaxamt, 0.0 rabdtlnetto, 1.5 rabpphvalue, 0.0 rabpphamt, '' rabdtlnote, 0.0 rabongkiramt, GETDATE() rabetd, 16 rablocoid, itemspec, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=i.itemoid),0.0) stockqty, itempricesrp, itempricedealer, itempricemd FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' AND i.itemtype IN ('Barang', 'Rakitan', 'Ongkir', 'Jasa') ORDER BY itemcode";
            tbl = db.Database.SqlQuery<rabdtl_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatManualDetail(string itemtype)
        {
            List<rabdtl_draft> tbl = new List<rabdtl_draft>();

            sSql = $"SELECT 0 rabseq, 'Manual' rabdtlflag_m, 0 itemoid, '{itemtype}' itemdtltype, '' itemcode, '' itemdtldesc, 0.0 rabqty, 0 rabunitoid, '' rabunit, 0.0 rabprice, 0.0 rabdtlamt, 10.0 rabtaxvalue, 0.0 rabtaxamt, 0.0 rabdtlnetto, 1.5 rabpphvalue, 0.0 rabpphamt, '' rabdtlnote, 0.0 rabongkiramt, GETDATE() rabetd, 16 rablocoid, '' itemspec, 0.0 stockqty, 0.0 itempricesrp, 0.0 itempricedealer, 0.0 itempricemd";
            tbl = db.Database.SqlQuery<rabdtl_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailDataReq(int reqrabmstoid)
        {
            List<rabdtl_draft> tbl = new List<rabdtl_draft>();

            sSql = $"SELECT rd.reqrabseq rabseq, rd.itemtype itemdtltype, reqrabflag_m rabdtlflag_m, rd.itemoid, ISNULL(i.itemcode,'') itemcode, rd.itemdesc itemdtldesc, rd.reqrabqty rabqty, rd.reqrabunitoid rabunitoid, ISNULL(g.gndesc,'') rabunit, rd.reqrabprice rabprice, rd.reqrabdtlamt rabdtlamt, rd.reqrabtaxvalue rabtaxvalue, rd.reqrabtaxamt rabtaxamt, rd.reqrabdtlnetto rabdtlnetto, rd.reqrabpphvalue rabpphvalue, rd.reqrabpphamt rabpphamt, '' rabdtlnote, 0.0 rabongkiramt, GETDATE() rabetd, 16 rablocoid, ISNULL(itemspec,'') itemspec, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=rd.itemoid),0.0) stockqty, ISNULL(itempricesrp,0.0) itempricesrp, ISNULL(itempricedealer,0.0) itempricedealer, ISNULL(itempricemd,0.0) itempricemd FROM QL_trnreqrabdtl rd LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemoid LEFT JOIN QL_m05GN g ON g.gnoid=rd.reqrabunitoid WHERE rd.cmpcode='{CompnyCode}' AND rd.reqrabmstoid={reqrabmstoid} ORDER BY rd.reqrabseq";
            tbl = db.Database.SqlQuery<rabdtl_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabdtl2_draft : QL_trnrabdtl2_draft
        {
            public string suppdtl2name { get; set; }
            public string suppdtl2taxtype { get; set; }
            public string itemrefdesc { get; set; }
            public string itemcode { get; set; }
            public string rabdtl2unit { get; set; }
            public string rabdtl2locdesc { get; set; }
            public string poassetno { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail2(string filtermat)
        {
            List<rabdtl2_draft> tbl = new List<rabdtl2_draft>();

            var sPlus = "";
            if (!string.IsNullOrEmpty(filtermat))
            {
                sPlus = " AND i.itemoid IN(" + filtermat + ")";
            }

            sSql = "SELECT 0 rabdtl2seq, 'Master' rabdtl2flag_m, 0 suppdtl2oid, '' suppdtl2name, '' suppdtl2taxtype, i.itemoid itemdtl2oid, i.itemcode, i.itemdesc itemdtl2desc, 0.0 rabdtl2qty, i.itemunitoid rabdtl2unitoid, g.gndesc rabdtl2unit, 0.0 rabdtl2price, 0.0 rabdtl2qtybooking, 0.0 rabdtl2amt, 0.0 rabdtl2discvalue, 0.0 rabdtl2discamt, 0.0 rabdtl2taxvalue, 0.0 rabdtl2taxamt, 0.0 rabdtl2netto, '' rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, 16 rabdtl2locoid, '' rabdtl2locdesc, 0 itemdtl2refoid, '' itemrefdesc, 0.0 rabdtl2amtbooking, 'Belum' rabdtl2flagrevisi, 0 poassetmstoid, 0 poassetdtloid, '' poassetno FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' " + sPlus + " AND i.itemtype IN ('Barang', 'Ongkir')  UNION ALL  SELECT 0 rabdtl2seq, 'Master' rabdtl2flag_m, 0 suppdtl2oid, '' suppdtl2name, '' suppdtl2taxtype, itm.itemoid itemdtl2oid, itm.itemcode, itm.itemdesc, 0.0 rabdtl2qty, itm.itemunitoid rabdtl2unitoid, g.gndesc rabdtl2unit, 0.0 rabdtl2price, 0.0 rabdtl2qtybooking, 0.0 rabdtl2amt, 0.0 rabdtl2discvalue, 0.0 rabdtl2discamt, 0.0 rabdtl2taxvalue, 0.0 rabdtl2taxamt, 0.0 rabdtl2netto, '' rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, 16 rabdtl2locoid, '' rabdtl2locdesc, i.itemoid itemdtl2refoid, i.itemdesc itemrefdesc, 0.0 rabdtl2amtbooking, 'Belum' rabdtl2flagrevisi, 0 poassetmstoid, 0 poassetdtloid, '' poassetno FROM QL_mstitem i INNER JOIN QL_mstitemdtl2 itd ON itd.itemoid=i.itemoid INNER JOIN QL_mstitem itm ON itm.itemoid=itd.itemrefoid INNER JOIN QL_m05GN g ON g.gnoid=itd.itemrefunitoid WHERE itm.activeflag='ACTIVE' " + sPlus + " AND i.itemtype IN ('Rakitan')";
            tbl = db.Database.SqlQuery<rabdtl2_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatManualDetail2(string itemtype)
        {
            List<rabdtl2_draft> tbl = new List<rabdtl2_draft>();

            sSql = $"SELECT 0 rabdtl2seq, 'Manual' rabdtl2flag_m, 0 suppdtl2oid, '' suppdtl2name, '' suppdtl2taxtype, 0 itemdtl2oid, '' itemcode, '' itemdtl2desc, 0.0 rabdtl2qty, 0 rabdtl2unitoid, '' rabdtl2unit, 0.0 rabdtl2price, 0.0 rabdtl2qtybooking, 0.0 rabdtl2amt, 0.0 rabdtl2discvalue, 0.0 rabdtl2discamt, 0.0 rabdtl2taxvalue, 0.0 rabdtl2taxamt, 0.0 rabdtl2netto, '' rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, 16 rabdtl2locoid, '' rabdtl2locdesc, 0 itemdtl2refoid, '' itemrefdesc, 0.0 rabdtl2amtbooking, 'Belum' rabdtl2flagrevisi, 0 poassetmstoid, 0 poassetdtloid, '' poassetno";
            tbl = db.Database.SqlQuery<rabdtl2_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabdtl4_draft : QL_trnrabdtl4_draft
        {
            public string suppdtl4name { get; set; }
            public string suppdtl4taxtype { get; set; }
            public string jasacode { get; set; }
            public string rabdtl4unit { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail4()
        {
            List<rabdtl4_draft> tbl = new List<rabdtl4_draft>();

            sSql = "SELECT 0 rabdtl4seq, 'Master' rabdtl4flag_m, 0 suppdtl4oid, '' suppdtl4name, '' suppdtl4taxtype, i.jasaoid itemdtl4oid, i.jasacode, i.jasadesc itemdtl4desc, 0.0 rabdtl4qty, i.jasaunitoid rabdtl4unitoid, g.gndesc rabdtl4unit, 0.0 rabdtl4price, 0.0 rabdtl4discvalue, 0.0 rabdtl4discamt, 0.0 rabdtl4amt, 2.0 rabdtl4pphvalue, 0.0 rabdtl4pphamt, 0.0 rabdtl4taxvalue, 0.0 rabdtl4taxamt, 0.0 rabdtl4netto, '' rabdtl4note, GETDATE() rabdtl4eta FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE activeflag='ACTIVE' ORDER BY jasacode";
            tbl = db.Database.SqlQuery<rabdtl4_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatManualDetail4()
        {
            List<rabdtl4_draft> tbl = new List<rabdtl4_draft>();

            sSql = $"SELECT 0 rabdtl4seq, 'Manual' rabdtl4flag_m, 0 suppdtl4oid, '' suppdtl4name, '' suppdtl4taxtype, 0 itemdtl4oid, '' jasacode, '' itemdtl4desc, 0.0 rabdtl4qty, 0 rabdtl4unitoid, '' rabdtl4unit, 0.0 rabdtl4price, 0.0 rabdtl4discvalue, 0.0 rabdtl4discamt, 0.0 rabdtl4amt, 2.0 rabdtl4pphvalue, 0.0 rabdtl4pphamt, 0.0 rabdtl4taxvalue, 0.0 rabdtl4taxamt, 0.0 rabdtl4netto, '' rabdtl4note, GETDATE() rabdtl4eta";
            tbl = db.Database.SqlQuery<rabdtl4_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabdtl5_draft : QL_trnrabdtl5_draft
        {
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail5(string sVar)
        {
            List<rabdtl5_draft> tbl = new List<rabdtl5_draft>();

            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT 0 rabdtl5seq, 'Master' rabdtl5flag_m, a.acctgoid, a.acctgcode, a.acctgdesc itemdtl5desc, 0.0 rabdtl5price, '' rabdtl5note FROM QL_mstacctg a WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<rabdtl5_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetDataMatManualDetail5()
        {
            List<rabdtl5_draft> tbl = new List<rabdtl5_draft>();

            sSql = $"SELECT 0 rabdtl5seq, 'Manual' rabdtl5flag_m, 0 acctgoid, '' acctgcode, '' itemdtl5desc, 0.0 rabdtl5price, '' rabdtl5note";
            tbl = db.Database.SqlQuery<rabdtl5_draft>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<rabdtl_draft>("SELECT rd.rabseq, rd.itemdtltype, rabdtlflag_m, rd.itemoid, ISNULL(i.itemcode,'') itemcode, itemdtldesc, rd.rabqty, rd.rabunitoid, ISNULL(g.gndesc,'') rabunit, rd.rabprice, rd.rabdtlamt, rd.rabtaxvalue, rd.rabtaxamt, rd.rabdtlnetto, rd.rabpphvalue, rd.rabpphamt, rd.rabdtlnote, rd.rabongkiramt, rd.rabetd, rd.rablocoid, ISNULL(itemspec,'') itemspec, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=rd.itemoid),0.0) stockqty, ISNULL(itempricesrp,0.0) itempricesrp, ISNULL(itempricedealer,0.0) itempricedealer, ISNULL(itempricemd,0.0) itempricemd FROM QL_trnrabdtl_draft rd LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemoid LEFT JOIN QL_m05GN g ON g.gnoid=rd.rabunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabseq").ToList();

                var tbl2 = db.Database.SqlQuery<rabdtl2_draft>("SELECT rd.rabdtl2seq, rabdtl2flag_m, rd.suppdtl2oid, ISNULL(s.suppname,'') suppdtl2name, case when supppajak=1 then 'TAX' else 'NON TAX' end suppdtl2taxtype, rd.itemdtl2oid, ISNULL(i.itemcode,'') itemcode, itemdtl2desc, rd.rabdtl2qty, rd.rabdtl2unitoid, ISNULL(g.gndesc,'') rabdtl2unit, rd.rabdtl2price, rd.rabdtl2qtybooking, rd.rabdtl2amt, rd.rabdtl2discvalue, rd.rabdtl2discamt, rd.rabdtl2taxvalue, rd.rabdtl2taxamt, rd.rabdtl2netto, rd.rabdtl2note, rd.rabdtl2ongkiramt, rd.rabdtl2eta, rd.rabdtl2locoid, ISNULL(g2.gndesc,'') rabdtl2locdesc, ISNULL(rd.itemdtl2refoid,0) itemdtl2refoid, ISNULL((SELECT ix.itemdesc FROM QL_mstitem ix WHERE ix.itemoid=ISNULL(rd.itemdtl2refoid,0)),'') itemrefdesc, rabdtl2amtbooking, rabdtl2flagrevisi, ISNULL(poassetmstoid,0) poassetmstoid, ISNULL(poassetdtloid,0) poassetdtloid, ISNULL((SELECT pom.poassetno FROM QL_trnpoassetmst pom WHERE pom.poassetmstoid=ISNULL(rd.poassetmstoid,0)),'') poassetno FROM QL_trnrabdtl2_draft rd LEFT JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl2oid LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemdtl2oid LEFT JOIN QL_m05GN g ON g.gnoid=rd.rabdtl2unitoid LEFT JOIN QL_m05GN g2 ON g2.gnoid=rd.rabdtl2locoid  WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl2seq").ToList();

                var tbl4 = db.Database.SqlQuery<rabdtl4_draft>("SELECT rd.rabdtl4seq, rabdtl4flag_m, rd.suppdtl4oid, ISNULL(s.suppname,'') suppdtl4name, case when supppajak=1 then 'TAX' else 'NON TAX' end suppdtl4taxtype, rd.itemdtl4oid, ISNULL(i.jasacode,'') jasacode, itemdtl4desc, rd.rabdtl4qty, rd.rabdtl4unitoid, ISNULL(g.gndesc,'') rabdtl4unit, rd.rabdtl4price, rd.rabdtl4discvalue, rd.rabdtl4discamt, rd.rabdtl4amt, rd.rabdtl4pphvalue, rd.rabdtl4pphamt, rd.rabdtl4taxvalue, rd.rabdtl4taxamt, rd.rabdtl4netto, rd.rabdtl4note, rd.rabdtl4eta FROM QL_trnrabdtl4_draft rd LEFT JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl4oid LEFT JOIN QL_mstjasa i ON i.jasaoid=rd.itemdtl4oid LEFT JOIN QL_m05GN g ON g.gnoid=rd.rabdtl4unitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl4seq").ToList();

                var tbl5 = db.Database.SqlQuery<rabdtl5_draft>("SELECT rd.rabdtl5seq, rabdtl5flag_m, rd.acctgoid, ISNULL(a.acctgcode,'') acctgcode, itemdtl5desc, rd.rabdtl5price, rd.rabdtl5note FROM QL_trnrabdtl5_draft rd LEFT JOIN QL_mstacctg a ON a.acctgoid=rd.acctgoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl5seq").ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl, tbl2, tbl4, tbl5 }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class reqrabmst : QL_trnreqrabmst
        {
            public string custname { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRABData()
        {
            List<reqrabmst> tbl = new List<reqrabmst>();

            sSql = "SELECT req.*, c.custname FROM QL_trnreqrabmst req INNER JOIN QL_mstcust c ON c.custoid=req.custoid WHERE reqrabmststatus IN('Post','Approved') ORDER BY reqrabdate DESC";
            tbl = db.Database.SqlQuery<reqrabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        

        [HttpPost]
        public ActionResult GetPartnerData()
        {
            List<QL_mstpartner> tbl = new List<QL_mstpartner>();

            sSql = "SELECT * FROM QL_mstpartner WHERE activeflag='ACTIVE' ORDER BY partnername";
            tbl = db.Database.SqlQuery<QL_mstpartner>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            List<QL_mstsupp> tbl = new List<QL_mstsupp>();

            sSql = "SELECT * FROM QL_mstsupp WHERE activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: RAB
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            return View();
        }

        [HttpPost]
        public ActionResult getListDataTable(string ddlstatus, string periodfrom, string periodto)
        {

            sSql = "SELECT * FROM( Select pr.rabmstoid, pr.rabno, pr.rabdate, pr.rabtype, ISNULL(pr.projectname,'') projectname, c.custname, pr.rabgrandtotaljualamt, pr.rabmststatus, CASE WHEN pr.rabmststatus = 'Revised' THEN pr.revisereason WHEN pr.rabmststatus = 'Rejected' THEN pr.rejectreason ELSE '' END reason FROM QL_trnrabmst_draft pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(periodfrom) && !string.IsNullOrEmpty(periodto))
            {
                sSql += " AND t.rabdate >='" + periodfrom + " 00:00:00' AND t.rabdate <='" + periodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(ddlstatus))
            {
                sSql += " AND t.rabmststatus " + ddlstatus;
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnrabmst_draft tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnrabmst_draft();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.rabno = ""; //generateNo(ClassFunction.GetServerTime());
                tblmst.rabtypereq = "Permintaan RAB";
                tblmst.rabgrandtotaljualamt = 0; tblmst.rabtotaljualamt = 0;
                tblmst.rabtotaljualtaxamt = 0; tblmst.rabtotaljualpphamt = 0;
                tblmst.rabgrandtotalbeliamt = 0; tblmst.rabtotalbeliamt = 0; tblmst.rabtotalbelitaxamt = 0;
                tblmst.rabgrandtotaljualjasaamt = 0; tblmst.rabtotaljualjasaamt = 0;
                tblmst.rabtotaljualjasataxamt = 0; tblmst.rabtotaljualjasapphamt = 0;
                tblmst.rabgrandtotalbelijasaamt = 0; tblmst.rabtotalbelijasaamt = 0;
                tblmst.rabtotalbelijasataxamt = 0; tblmst.rabtotalbelijasapphamt = 0;
                tblmst.rabgrandtotalcostamt = 0; tblmst.rabtotalcostamt = 0;
                tblmst.rabmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                ViewBag.rabdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.rabestdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnrabmst_draft.Find(Session["CompnyCode"].ToString(), id);

                ViewBag.rabdate = tblmst.rabdate.Value.ToString("dd/MM/yyyy");
                ViewBag.rabestdate = tblmst.rabestdate.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tblmst.updtime.ToString("dd/MM/yyyy");
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Form(QL_trnrabmst_draft tblmst, string action, string tglmst, string tglest, string custname, SendEmailModel tblse)
        public ActionResult Form(QL_trnrabmst_draft tblmst, List<rabdtl_draft> dtDtl, List<rabdtl2_draft> dtDtl2, List<rabdtl4_draft> dtDtl4, List<rabdtl5_draft> dtDtl5, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            //is Input Valid
            if (tblmst.custoid == null)
                msg += "- Pilih Customer Terlebih Dahulu!!<br>";
            if (tblmst.alamatoid == null)
                msg += "- Pilih Alamat Terlebih Dahulu!!<br>";
            if(tblmst.rabtypereq == "Permintaan RAB")
            {
                if (tblmst.reqrabmstoid == 0)
                    msg += "- Pilih Permintaan RAB Terlebih Dahulu!!<br>";
            }
            if (tblmst.rabtype2 != "PARTNER")
            {
                tblmst.partneroid = 0;
                tblmst.partnerpersen = 0;
                tblmst.partneramt = 0;
            }
            if (string.IsNullOrEmpty(tblmst.projectname))
                msg += "- Silahkan isi Project!<br>";
            if (string.IsNullOrEmpty(tblmst.rabno))
                tblmst.rabno = "";
            if (string.IsNullOrEmpty(tblmst.rabmstnote))
                tblmst.rabmstnote = "";
            if (string.IsNullOrEmpty(tblmst.projectno))
                tblmst.projectno = "";
            if (string.IsNullOrEmpty(tblmst.projectname))
                tblmst.projectname = "";
            if (string.IsNullOrEmpty(tblmst.rabasaljual))
                tblmst.rabasaljual = "";
            if (string.IsNullOrEmpty(tblmst.rabtotalbeliamt.ToString()))
                tblmst.rabtotalbeliamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelitaxamt.ToString()))
                tblmst.rabtotalbelitaxamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotaljualjasaamt.ToString()))
                tblmst.rabtotaljualjasaamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotaljualjasataxamt.ToString()))
                tblmst.rabtotaljualjasataxamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotaljualjasapphamt.ToString()))
                tblmst.rabtotaljualjasapphamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelijasaamt.ToString()))
                tblmst.rabtotalbelijasaamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelijasataxamt.ToString()))
                tblmst.rabtotalbelijasataxamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelijasapphamt.ToString()))
                tblmst.rabtotalbelijasapphamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalcostamt.ToString()))
                tblmst.rabtotalcostamt = 0;
            if (tblmst.rabmststatus.ToUpper() == "REVISED")
            {
                tblmst.rabmststatus = "In Process";
            }
            tblmst.rate2oid = 0;

            if (dtDtl == null)
                msg += "- Silahkan isi Detail Penjualan!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Silahkan isi Detail Penjualan!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].rabdtlnote))
                            dtDtl[i].rabdtlnote = "";
                        if (dtDtl[i].rabqty == 0)
                        {
                            msg += "- Qty " + dtDtl[i].itemdtldesc + " Belum Diisi !!<br>";
                        }
                        if (dtDtl[i].rabprice == 0)
                        {
                            msg += "- Harga " + dtDtl[i].itemdtldesc + " Belum Diisi !!<br>";
                        }

                        //Cek data item beli
                        if (tblmst.rabmststatus == "In Approval")
                        {
                            if (dtDtl2 != null)
                            {
                                if (dtDtl2.Count > 0)
                                {
                                    var itmjual = dtDtl[i].itemoid;
                                    var itmjualtype = db.Database.SqlQuery<string>("SELECT itemtype FROM QL_mstitem WHERE itemoid=" + itmjual + "").FirstOrDefault();
                                    if (itmjualtype == "Barang")
                                    {
                                        var cItmBeli = dtDtl2.Where(x => x.itemdtl2oid == itmjual).Count();
                                        if (cItmBeli == 0)
                                        {
                                            msg += "- Item " + dtDtl[i].itemdtldesc + " Belum Di Tarik Di Pembelian!!<br>";
                                        }
                                    }
                                }
                            }
                        }
                        //End Cek data item beli
                    }
                }
            }

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl2[i].rabdtl2note))
                            dtDtl2[i].rabdtl2note = "";
                    }
                }
            }

            if (dtDtl4 != null)
            {
                if (dtDtl4.Count > 0)
                {
                    for (int i = 0; i < dtDtl4.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl4[i].rabdtl4note))
                            dtDtl4[i].rabdtl4note = "";
                        if (dtDtl4[i].rabdtl4qty == 0)
                        {
                            msg += "- Qty " + dtDtl4[i].itemdtl4desc + " Belum Diisi !!<br>";
                        }
                        if (dtDtl4[i].rabdtl4price == 0)
                        {
                            msg += "- Harga " + dtDtl4[i].itemdtl4desc + " Belum Diisi !!<br>";
                        }
                    }
                }
            }

            if (dtDtl5 != null)
            {
                if (dtDtl5.Count > 0)
                {
                    for (int i = 0; i < dtDtl5.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl5[i].rabdtl5note))
                            dtDtl5[i].rabdtl5note = "";
                        if (dtDtl5[i].rabdtl5price == 0)
                        {
                            msg += "- Amount " + dtDtl5[i].acctgdesc + " Belum Diisi !!<br>";
                        }
                    }
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tblmst.rabmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    msg += "- Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.rabdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                tblmst.rabmststatus = "In Process";
            }
            if (tblmst.rabmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                    tblmst.rabmststatus = "In Process";
                }
            }

            if (tblmst.rabmststatus == "Post")
            {
                tblmst.rabno = generateNo(tblmst.rabdate.Value);
            }
            else
            {
                tblmst.rabno = "";
            }

            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_trnrabmst_draft");
                var dtloid = ClassFunction.GenerateID("QL_trnrabdtl_draft");
                var dtloid2 = ClassFunction.GenerateID("QL_trnrabdtl2_draft");
                var dtloid4 = ClassFunction.GenerateID("QL_trnrabdtl4_draft");
                var dtloid5 = ClassFunction.GenerateID("QL_trnrabdtl5_draft");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.rabmstoid = mstoid;
                            if (tblmst.revrabtype == "Baru")
                            {
                                tblmst.revrabmstoid = mstoid;
                                tblmst.rabmstoid_revisi = 0;
                            }
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnrabmst_draft.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnrabmst_draft'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnrabdtl_draft.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl_draft.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnrabdtl2_draft.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl2_draft.RemoveRange(trndtl2);
                            db.SaveChanges();

                            var trndtl4 = db.QL_trnrabdtl4_draft.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl4_draft.RemoveRange(trndtl4);
                            db.SaveChanges();

                            var trndtl5 = db.QL_trnrabdtl5_draft.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl5_draft.RemoveRange(trndtl5);
                            db.SaveChanges();
                        }

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl_draft>();
                                foreach (var item in dtDtl)
                                {
                                    var rabdtloid = dtloid++;
                                    var new_dtl = (QL_trnrabdtl_draft)ClassFunction.MappingTable(new QL_trnrabdtl_draft(), item);
                                    if (string.IsNullOrEmpty(item.rabdtlstatus)) new_dtl.rabdtlstatus = "";
                                    if (string.IsNullOrEmpty(item.rabdtlnote)) new_dtl.rabdtlnote = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtloid = rabdtloid;
                                    new_dtl.rabdtloid_draft = rabdtloid;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl_draft.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnrabdtl_draft'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl2_draft>();
                                foreach (var item in dtDtl2)
                                {
                                    var rabdtl2oid = dtloid2++;
                                    var new_dtl = (QL_trnrabdtl2_draft)ClassFunction.MappingTable(new QL_trnrabdtl2_draft(), item);
                                    if (string.IsNullOrEmpty(item.rabdtl2status)) new_dtl.rabdtl2status = "";
                                    if (string.IsNullOrEmpty(item.rabdtl2note)) new_dtl.rabdtl2note = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtl2oid = rabdtl2oid;
                                    new_dtl.rabdtloid2_draft = rabdtl2oid;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl2_draft.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid2 - 1) + " WHERE tablename='QL_trnrabdtl2_draft'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl4 != null)
                        {
                            if (dtDtl4.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl4_draft>();
                                foreach (var item in dtDtl4)
                                {
                                    var rabdtl4oid = dtloid4++;
                                    var new_dtl = (QL_trnrabdtl4_draft)ClassFunction.MappingTable(new QL_trnrabdtl4_draft(), item);
                                    if (string.IsNullOrEmpty(item.rabdtl4status)) new_dtl.rabdtl4status = "";
                                    if (string.IsNullOrEmpty(item.rabdtl4note)) new_dtl.rabdtl4note = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtl4oid = rabdtl4oid;
                                    new_dtl.rabdtloid4_draft = rabdtl4oid;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl4_draft.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid4 - 1) + " WHERE tablename='QL_trnrabdtl4_draft'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl5 != null)
                        {
                            if (dtDtl5.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl5_draft>();
                                foreach (var item in dtDtl5)
                                {
                                    var new_dtl = (QL_trnrabdtl5_draft)ClassFunction.MappingTable(new QL_trnrabdtl5_draft(), item);
                                    if (string.IsNullOrEmpty(item.rabdtl5status)) new_dtl.rabdtl5status = "";
                                    if (string.IsNullOrEmpty(item.rabdtl5note)) new_dtl.rabdtl5note = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtl5oid = dtloid5++;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.rabdtl5qty = 1;
                                    new_dtl.rabdtl5netto = item.rabdtl5price;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl5_draft.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid5 - 1) + " WHERE tablename='QL_trnrabdtl5_draft'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tblmst.rabmststatus == "In Approval")
                        {
                            tblapp.cmpcode = tblmst.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tblmst.rabmstoid;
                            tblapp.requser = tblmst.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnrabmst_draft";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnrabmst_draft tblmst = db.QL_trnrabmst_draft.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnrabdtl_draft.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl_draft.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trnrabdtl2_draft.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl2_draft.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl4 = db.QL_trnrabdtl4_draft.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl4_draft.RemoveRange(trndtl4);
                        db.SaveChanges();

                        var trndtl5 = db.QL_trnrabdtl5_draft.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl5_draft.RemoveRange(trndtl5);
                        db.SaveChanges();

                        var trndtl_matbooking = db.QL_matbooking.Where(a => a.formoid == id);
                        db.QL_matbooking.RemoveRange(trndtl_matbooking);
                        db.SaveChanges();

                        db.QL_trnrabmst_draft.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT rabtype FROM QL_trnrabmst_draft WHERE rabmstoid="+ id +"";
            string rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            string rptname = "rptRAB.rpt";

            ReportDocument report = new ReportDocument();
            if (rabtype == "WAPU")
            {
                rptname = "rptRABWapuDraft.rpt";
            }
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE rm.cmpcode='" + CompnyCode + "' AND rm.rabmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "RABDraftReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}