﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class SJAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public SJAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class shipmentassetmst
        {
            public string cmpcode { get; set; }
            public int shipmentassetmstoid { get; set; }
            public string shipmentassetno { get; set; }
            public DateTime? shipmentassetdate { get; set; }
            public string custname { get; set; }
            public string shipmentassetcontno { get; set; }
            public string shipmentassetmststatus { get; set; }
            public string shipmentassetmstnote { get; set; }
            public string divname { get; set; }
        }

        public class shipmentassetdtl
        {
            public int shipmentassetdtlseq { get; set; }
            public int soassetmstoid { get; set; }
            public int soassetdtloid { get; set; }
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public int matrefoid { get; set; }
            public string matrefcode { get; set; }
            public string matreflongdesc { get; set; }
            public decimal soassetqty { get; set; }
            public decimal stockqty { get; set; }
            public decimal shipmentassetqty { get; set; }
            public int shipmentassetunitoid { get; set; }
            public string shipmentassetunit { get; set; }
            public int shipmentassetwhoid { get; set; }
            public string shipmentassetwhdesc { get; set; }
            public string shipmentassetdtlnote { get; set; }
            public decimal shipmentassetvalue { get; set; }
            public decimal shipmentassetvalueidr { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }


        public class soasset
        {
            public int soassetmstoid { get; set; }
            public string soassetno { get; set; }
            public DateTime soassetdate { get; set; }
            public string soassetmstnote { get; set; }
            public int curroid { get; set; }
        }

        private void InitDDL(QL_trnshipmentassetmst tbl)
        {
            sSql = "SELECT us.* FROM QL_m01us us INNER JOIN QL_m05gn gn ON gn.gnoid = us.jabatanoid where usflag = 'ACTIVE' AND gn.gndesc = 'DRIVER'";
            var drivoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.drivoid);
            ViewBag.drivoid = drivoid;

            sSql = "SELECT * FROM QL_mstvehicle WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY vhcdesc";
            var vhcoid = new SelectList(db.Database.SqlQuery<QL_mstvehicle>(sSql).ToList(), "vhcoid", "vhcno", tbl.vhcoid);
            ViewBag.vhcoid = vhcoid;

            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' /*AND gnother1='Asset'*/";
            var shipmentassetwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.shipmentassetwhoid);
            ViewBag.shipmentassetwhoid = shipmentassetwhoid;

            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' /*AND gnother1='Asset'*/";
            var whoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.whoid);
            ViewBag.whoid = whoid;
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SJA/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentassetno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentassetmst WHERE cmpcode='" + CompnyCode + "' AND shipmentassetno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp)
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT cust.custoid, cust.custcode, cust.custname, cust.custaddr FROM QL_mstcust cust WHERE cust.custoid IN (SELECT DISTINCT custoid FROM QL_trnsoassetmst WHERE cmpcode='" + CompnyCode + "' AND soassetmststatus='Post') ORDER BY custname";

            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSOData(string cmp, int custoid)
        {
            List<soasset> tbl = new List<soasset>();
            sSql = "SELECT som.soassetmstoid, som.soassetno, som.soassetdate, som.soassetmstnote, som.curroid FROM QL_trnsoassetmst som WHERE som.cmpcode='" + CompnyCode + "' AND som.soassetmststatus='Post' AND som.custoid="+ custoid +"";

            tbl = db.Database.SqlQuery<soasset>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int soassetmstoid, int shipmentassetmstoid, int shipmentassetwhoid)
        {
            List<shipmentassetdtl> tbl = new List<shipmentassetdtl>();
            sSql = "SELECT 0 shipmentassetdtlseq, dod.soassetmstoid, dod.soassetdtloid, dod.assetmstoid, m.assetno, i.itemoid matrefoid, m.reftype matreftype, i.itemcode AS matrefcode, i.itemdesc AS matreflongdesc, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=i.itemoid AND con.mtrwhoid="+ shipmentassetwhoid + "),0.0) AS stockqty, (dod.soassetqty - ISNULL((SELECT SUM(sd.shipmentassetqty) FROM QL_TRNSHIPMENTASSETDTL sd INNER JOIN QL_TRNSHIPMENTASSETMST sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentassetmstoid=sd.shipmentassetmstoid WHERE sd.cmpcode=dod.cmpcode AND sm.shipmentassetmststatus<>'Rejected' AND sd.soassetdtloid=dod.soassetdtloid AND sd.shipmentassetmstoid<>" + shipmentassetmstoid + "), 0.0)) AS soassetqty, 0.0 AS shipmentassetqty, dod.soassetunitoid shipmentassetunitoid, g.gndesc AS shipmentassetunit, dod.soassetdtlnote AS shipmentassetdtlnote FROM QL_trnsoassetdtl dod INNER JOIN QL_assetmst m ON m.assetmstoid=dod.assetmstoid INNER JOIN QL_m05GN g ON g.gnoid=dod.soassetunitoid INNER JOIN QL_mstitem i ON i.itemoid=m.refoid WHERE dod.cmpcode='" + CompnyCode + "' AND dod.soassetmstoid=" + soassetmstoid + " AND dod.soassetdtlstatus='' ORDER BY soassetdtlseq";

            tbl = db.Database.SqlQuery<shipmentassetdtl>(sSql).ToList();
            if (tbl.Count > 0)
            {
                for(var i = 0; i < tbl.Count(); i++)
                {
                    tbl[i].shipmentassetqty = tbl[i].soassetqty;
                }
            }
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<shipmentassetdtl> dtDtl)
        {
            Session["QL_trnshipmentassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnshipmentassetdtl"] == null)
            {
                Session["QL_trnshipmentassetdtl"] = new List<shipmentassetdtl>();
            }

            List<shipmentassetdtl> dataDtl = (List<shipmentassetdtl>)Session["QL_trnshipmentassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnshipmentassetmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.soassetno = db.Database.SqlQuery<string>("SELECT soassetno FROM QL_trnsoassetmst WHERE cmpcode='"+ tbl.cmpcode + "' AND soassetmstoid=" + tbl.soassetmstoid + "").FirstOrDefault();
        }

        // GET: shipmentassetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT som.cmpcode, som.shipmentassetmstoid, som.shipmentassetno, som.shipmentassetdate, c.custname, som.shipmentassetmststatus, som.shipmentassetmstnote FROM QL_TRNshipmentassetMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "som.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "som.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND shipmentassetdate>=CAST('" + modfil.filterperiodfrom + " 00:00:00' AS DATETIME) AND shipmentassetdate<=CAST('" + modfil.filterperiodto + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND shipmentassetmststatus IN ('In Process', 'Revised')";
            }            

            List<shipmentassetmst> dt = db.Database.SqlQuery<shipmentassetmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: shipmentassetMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentassetmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnshipmentassetmst();
                tbl.cmpcode = CompnyCode;
                tbl.shipmentassetmstoid = ClassFunction.GenerateID("QL_trnshipmentassetmst");
                tbl.shipmentassetno = generateNo(ClassFunction.GetServerTime());
                tbl.shipmentassetdate = ClassFunction.GetServerTime();
                tbl.shipmentassetetd= ClassFunction.GetServerTime();
                tbl.shipmentasseteta = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.shipmentassetmststatus = "In Process";

                Session["QL_trnshipmentassetdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnshipmentassetmst.Find(CompnyCode, id);

                sSql = "SELECT sd.shipmentassetdtlseq, sd.soassetmstoid, sd.soassetdtloid, sd.assetmstoid, m.assetno, sd.soassetdtloid, sd.assetmstoid, m.assetno, i.itemoid matrefoid, i.itemcode matrefcode, i.itemdesc AS matreflongdesc, 0.0 AS stockqty, (dod.soassetqty - ISNULL((SELECT SUM(sdx.shipmentassetqty) FROM QL_trnshipmentassetdtl sdx INNER JOIN QL_trnshipmentassetmst smx ON smx.cmpcode=sdx.cmpcode AND smx.shipmentassetmstoid=sdx.shipmentassetmstoid WHERE sdx.cmpcode=sd.cmpcode AND smx.shipmentassetmststatus<>'Rejected' AND sdx.soassetdtloid=sd.soassetdtloid AND sdx.shipmentassetmstoid<>sd.shipmentassetmstoid), 0.0)) AS soassetqty, sd.shipmentassetqty, sd.shipmentassetwhoid, ISNULL((SELECT g1.gndesc FROM QL_m05GN g1 WHERE g1.gnoid = sd.shipmentassetwhoid),'') shipmentassetwhdesc, sd.shipmentassetunitoid, g2.gndesc AS shipmentassetunit, sd.shipmentassetdtlnote, sd.shipmentassetvalue, sd.shipmentassetvalueidr FROM QL_trnshipmentassetdtl sd INNER JOIN QL_trnsoassetmst dom ON dom.cmpcode = sd.cmpcode AND dom.soassetmstoid = sd.soassetmstoid INNER JOIN QL_trnsoassetdtl dod ON dod.cmpcode = sd.cmpcode AND dod.soassetdtloid = sd.soassetdtloid INNER JOIN QL_assetmst m ON m.assetmstoid = sd.assetmstoid INNER JOIN QL_mstitem i ON i.itemoid = m.refoid INNER JOIN QL_m05GN g2 ON g2.gnoid = sd.shipmentassetunitoid WHERE sd.shipmentassetmstoid=" + tbl.shipmentassetmstoid + " ORDER BY sd.shipmentassetdtlseq";
                Session["QL_trnshipmentassetdtl"] = db.Database.SqlQuery<shipmentassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentassetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnshipmentassetmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (string.IsNullOrEmpty(tbl.shipmentassetno))
            {
                tbl.shipmentassetno = generateNo(ClassFunction.GetServerTime());
            }
                
            try
            {
                tbl.shipmentassetdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("shipmentassetdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            List<shipmentassetdtl> dtDtl = (List<shipmentassetdtl>)Session["QL_trnshipmentassetdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].shipmentassetqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY must be more than 0!");
                        }
                        else
                        {
                            if (dtDtl[i].shipmentassetqty > dtDtl[i].soassetqty)
                            {
                                ModelState.AddModelError("", "QTY must be less than DO QTY!");
                            }
                        }

                        sSql = "SELECT (soassetqty - ISNULL((SELECT SUM(shipmentassetqty) FROM QL_trnshipmentassetdtl sd INNER JOIN QL_trnshipmentassetmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentassetmstoid=sd.shipmentassetmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentassetmststatus<>'Rejected' AND sd.soassetdtloid=dod.soassetdtloid AND sd.shipmentassetmstoid<>" + tbl.shipmentassetmstoid + "), 0.0)) AS soassetqty FROM QL_trnsoassetdtl dod WHERE dod.cmpcode='" + tbl.cmpcode + "' AND soassetdtloid=" + dtDtl[i].soassetdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].soassetqty)
                            dtDtl[i].soassetqty = dQty;
                        if (dQty < dtDtl[i].shipmentassetqty)
                            ModelState.AddModelError("", "Some Shipment Qty has been updated by another user. Please check that every Qty must be less than Shipment Qty!");

                        //if (tbl.shipmentassetmststatus == "Post")
                        //{
                        //    var a = dtDtl.Where(x => x.matrefoid == dtDtl[i].matrefoid).GroupBy(x => x.matrefoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.shipmentassetqty) }).ToList();
                        //    for (int j = 0; j < a.Count(); j++)
                        //    {
                        //        if (!ClassFunction.IsStockAvailable(CompnyCode, ClassFunction.GetServerTime(), dtDtl[i].matrefoid, dtDtl[i].shipmentassetwhoid, a[j].Qty, "", 0, ""))
                        //        {
                        //            ModelState.AddModelError("", "Item " + dtDtl[i].matreflongdesc + " Qty SJ tidak boleh lebih dari Stock Qty");
                        //        }
                        //    }

                        //    //Stock Value
                        //    decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].matrefoid);
                        //    decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].matrefoid, dtDtl[i].shipmentassetqty, sValue, "OUT");
                        //    dtDtl[i].shipmentassetvalue = sAvgValue;
                        //    dtDtl[i].shipmentassetvalueidr = sAvgValue;
                        //}
                        //else
                        //{
                        var assetmstrefoid = dtDtl[i].assetmstoid;
                        var NilaiBuku = db.QL_assetmst.Where(x => x.assetmstoid == assetmstrefoid).Select(y => (y.assetvalue - y.assetaccumdep)).FirstOrDefault();
                        dtDtl[i].shipmentassetvalue = NilaiBuku;
                        dtDtl[i].shipmentassetvalueidr = NilaiBuku;
                        //}
                    }
                }
            }
         
            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.shipmentassetmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.shipmentassetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.shipmentassetmststatus = "In Process";
            }
            if (tbl.shipmentassetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.shipmentassetmststatus = "In Process";
                }
            }

            if (tbl.shipmentassetmststatus == "Revised")
                tbl.shipmentassetmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.shipmentassetmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnshipmentassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnshipmentassetdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_ASSET", CompnyCode));
                var iAcctgOidTrans = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trnshipmentassetmst.Find(tbl.cmpcode, tbl.shipmentassetmstoid) != null)
                                tbl.shipmentassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.shipmentassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnshipmentassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.shipmentassetmstoid + " WHERE tablename='QL_trnshipmentassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoassetdtl SET soassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND soassetdtloid IN (SELECT soassetdtloid FROM QL_trnshipmentassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + tbl.shipmentassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoassetmst SET soassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND soassetmstoid IN (SELECT soassetmstoid FROM QL_trnshipmentassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + tbl.shipmentassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnshipmentassetdtl.Where(a => a.shipmentassetmstoid == tbl.shipmentassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnshipmentassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnshipmentassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.shipmentassetdtloid = dtloid++;
                            tbldtl.shipmentassetmstoid = tbl.shipmentassetmstoid;
                            tbldtl.shipmentassetdtlseq = i + 1;
                            tbldtl.soassetmstoid = dtDtl[i].soassetmstoid;
                            tbldtl.soassetdtloid = dtDtl[i].soassetdtloid;
                            tbldtl.assetmstoid = dtDtl[i].assetmstoid;
                            tbldtl.shipmentassetqty = dtDtl[i].shipmentassetqty;
                            tbldtl.shipmentassetunitoid = dtDtl[i].shipmentassetunitoid;
                            tbldtl.shipmentassetwhoid = dtDtl[i].shipmentassetwhoid;
                            tbldtl.shipmentassetdtlstatus = "";
                            tbldtl.shipmentassetdtlnote = (dtDtl[i].shipmentassetdtlnote == null ? "" : dtDtl[i].shipmentassetdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.shipmentassetdtlres3 = "";
                            tbldtl.shipmentassetvalue = dtDtl[i].shipmentassetvalue;
                            tbldtl.shipmentassetvalueidr = dtDtl[i].shipmentassetvalueidr;
                            tbldtl.shipmentassetvalueusd = 0;

                            db.QL_trnshipmentassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoassetdtl SET soassetdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND soassetmstoid=" + dtDtl[i].soassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            
                            sSql = "UPDATE QL_trnsoassetmst SET soassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND soassetmststatus='Post' AND soassetmstoid=" + dtDtl[i].soassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.shipmentassetmststatus == "Post")
                            {
                                //    sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].matrefoid + "";
                                //    var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                                //    if (type != "Ongkir")
                                //    {
                                //        // Insert QL_conmat
                                //        db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "SJA", "QL_trnshipmentassetdtl", tbl.shipmentassetmstoid, dtDtl[i].matrefoid, "FIXED ASSET", dtDtl[i].shipmentassetwhoid, dtDtl[i].shipmentassetqty * -1, "SJA", "Surat Jalan Asset No. " + tbl.shipmentassetno + "", Session["UserID"].ToString(), "", dtDtl[i].shipmentassetvalueidr, 0, 0, null, tbldtl.shipmentassetdtloid, 0, dtDtl[i].shipmentassetvalueidr));
                                //        db.SaveChanges();
                                //    }

                                sSql = "UPDATE QL_assetdtl SET assetdtlstatus='Complete' WHERE assetmstoid="+ dtDtl[i].assetmstoid + " AND assetdtlstatus='' ";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnshipmentassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.shipmentassetmststatus == "Post")
                        {
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Surat Jalan Asset|No. " + tbl.shipmentassetno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                var assetmstrefoid = dtDtl[i].assetmstoid;
                                var iAssetAcctg = db.QL_assetmst.Where(x => x.assetmstoid == assetmstrefoid).Select(y => y.assetacctgoid).FirstOrDefault();
                                var iAssetakumAcctg = db.QL_assetmst.Where(x => x.assetmstoid == assetmstrefoid).Select(y => y.accumdepacctgoid).FirstOrDefault();
                                var nilaiBuku = dtDtl[i].shipmentassetvalue;
                                var hargaOleh = db.QL_assetmst.Where(x => x.assetmstoid == assetmstrefoid).Select(y => y.assetpurchase).FirstOrDefault();
                                var AmtDep = (hargaOleh - nilaiBuku);

                                // Insert QL_trngldtl
                                // C : Persediaan Barang Dlm Perjalanan
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "D", dtDtl[i].shipmentassetvalue, tbl.shipmentassetno, "Surat Jalan Asset|No. " + tbl.shipmentassetno, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].shipmentassetvalueidr, 0, "QL_trnshipmentassetmst " + tbl.shipmentassetmstoid, null, null, null, 0));
                                if (AmtDep > 0)
                                {
                                    // C : Akum Penyusutan asset
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAssetakumAcctg, "D", AmtDep, tbl.shipmentassetno, "Surat Jalan Asset|No. " + tbl.shipmentassetno, "Post", Session["UserID"].ToString(), servertime, AmtDep, 0, "QL_trnshipmentassetmst " + tbl.shipmentassetmstoid, null, null, null, 0));
                                }
                                // D : Hega perolehan asset
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAssetAcctg, "C", hargaOleh, tbl.shipmentassetno, "Surat Jalan Asset|No. " + tbl.shipmentassetno, "Post", Session["UserID"].ToString(), servertime, hargaOleh, 0, "QL_trnshipmentassetmst " + tbl.shipmentassetmstoid, null, null, null, 0));
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentassetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentassetmst tbl = db.QL_trnshipmentassetmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsoassetdtl SET soassetdtlstatus='' WHERE cmpcode='" + cmp + "' AND soassetdtloid IN (SELECT soassetdtloid FROM QL_trnshipmentassetdtl WHERE cmpcode='" + cmp + "' AND shipmentassetmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsoassetmst SET soassetmststatus='Post' WHERE cmpcode='" + cmp + "' AND soassetmstoid IN (SELECT soassetmstoid FROM QL_trnshipmentassetdtl WHERE cmpcode='" + cmp + "' AND shipmentassetmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnshipmentassetdtl.Where(a => a.shipmentassetmstoid == id && a.cmpcode == cmp);
                        db.QL_trnshipmentassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnshipmentassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSJAset.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentassetmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptSJAset.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}