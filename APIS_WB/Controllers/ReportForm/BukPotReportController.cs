﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class BukPotReportController : Controller
    {
        // GET: BukPotReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public BukPotReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;
            //if (DDLBusinessUnit.Count() > 0)
            //{
            sSql = "SELECT * FROM QL_mstcurr WHERE currcode IN ('IDR', 'USD') order by currcode";
            var DDLCurrency = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "currcode", "currcode");
            ViewBag.DDLCurrency = DDLCurrency;
            //}

            var DDLCOA = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_BANK")).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;
        }

        [HttpPost]
        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_BANK");
                tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("BukPotReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLType, string StartDate, string EndDate, string DDLPeriod, string DDLCurrency, string DDLOrderBy, string ReportType, string TextNomor, string DDLNomor, string DDLCOA, string[] DDLStatus)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "rptBukPot.rpt";
            if (ReportType == "XLS")
                rptfile = "rptBukPotXls.rpt";
            var rptname = "BuktiPotongReport";
            var periode = DateTime.Parse(StartDate).ToString("dd MMM yyyy") + " - " + DateTime.Parse(EndDate).ToString("dd MMM yyyy");

            sSql = $"SELECT bm.cmpcode, ISNULL((SELECT x.divname FROM QL_mstdivision x WHERE x.divcode=bm.cmpcode),'') divname, bm.bukpotmstoid, bm.bukpotdate, bukpotmasa, bukpottahun, bukpotmstnote, bukpotmststatus, bm.createuser, bm.createtime, bukpotdtlseq, bukpotno, bukpotreftype, bukpotrefoid, CASE bukpotreftype WHEN 'QL_trncashbankmst' THEN ISNULL((SELECT x.suppname FROM QL_trncashbankmst xm INNER JOIN QL_mstsupp x ON x.suppoid=xm.refsuppoid WHERE xm.cashbankoid=bukpotrefoid),'') ELSE '' END suppname, CASE bukpotreftype WHEN 'QL_trncashbankmst' THEN ISNULL((SELECT x.suppnpwpno FROM QL_trncashbankmst xm INNER JOIN QL_mstsupp x ON x.suppoid=xm.refsuppoid WHERE xm.cashbankoid=bukpotrefoid),'') ELSE '' END suppnpwpno, CASE bukpotreftype WHEN 'QL_trncashbankmst' THEN ISNULL((SELECT x.suppnote FROM QL_trncashbankmst xm INNER JOIN QL_mstsupp x ON x.suppoid=xm.refsuppoid WHERE xm.cashbankoid=bukpotrefoid),'') ELSE '' END suppktp, CASE bukpotreftype WHEN 'QL_trncashbankmst' THEN ISNULL((SELECT xm.cashbankno FROM QL_trncashbankmst xm WHERE xm.cashbankoid=bukpotrefoid),'') WHEN 'QL_trnkasbon2dtl' THEN ISNULL((SELECT xm.kasbon2no FROM QL_trnkasbon2mst xm INNER JOIN QL_trnkasbon2dtl xgl ON xgl.kasbon2mstoid=xm.kasbon2mstoid WHERE xgl.kasbon2dtloid=bukpotrefoid),'') ELSE ISNULL((SELECT xm.cashbankno FROM QL_trncashbankmst xm INNER JOIN QL_trncashbankgl xgl ON xgl.cashbankoid=xm.cashbankoid WHERE xgl.cashbankgloid=bukpotrefoid),'') END bukpotrefno, bukpotbruto, bd.acctgoid, a.acctgdesc, bukpotpphamt, bukpotdtlnote from QL_trnbukpotmst bm INNER JOIN QL_trnbukpotdtl bd ON bd.bukpotmstoid=bm.bukpotmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=bd.acctgoid WHERE bm.cmpcode='{CompnyCode}' AND bm.bukpotdate>='" + DateTime.Parse(StartDate).ToString("MM/dd/yyyy") + " 00:00:00' AND bm.bukpotdate<='" + DateTime.Parse(EndDate).ToString("MM/dd/yyyy") + " 23:00:00'";
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND bm.bukpotmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY bm.bukpotdate";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", periode);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable(); ;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}