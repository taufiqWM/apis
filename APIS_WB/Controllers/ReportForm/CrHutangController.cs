﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class CrHutangController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public CrHutangController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL()
        {
            sSql =" SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conap con INNER JOIN QL_mstacctg a ON a.acctgoid = con.acctgoid WHERE payrefoid = 0";
            var DDLCOA = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }

        [HttpPost]
        public ActionResult GetSuppData(string StartDate, string EndDate, string TextCustomer)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, suppcode [Code], suppname [Name], suppaddr [Address] FROM QL_mstsupp WHERE (suppcode LIKE '%" + "%' OR suppname LIKE '%" + "%') AND suppoid IN (SELECT suppoid FROM QL_conap) ORDER BY suppname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetSuppoidByCode(string TextCustomer)
        {
            return db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + CAST(suppoid AS VARCHAR(10)) FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppcode IN ('" + TextCustomer.Replace(";", "','") + "') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string StartDate, string EndDate, string DDLType, string ReportType, string TextCustomer, string DDLCOA, bool cbInvoice, bool cbHide, string DDLMonth, string DDLYear, string DDLMonth_Text, string DDLYear_Text, string DateNya)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            if (DDLType == "HD" || DDLType == "HS")
            {
                var rptfile = "";
                var rptname = "";

                if (DDLType == "HD")
                {
                    if (ReportType == "XLS")
                        rptfile = "rptKartuHutangDagangXls.rpt";
                    else
                    {
                        rptfile = "rptKartuHutangDagangPdf.rpt";
                    }
                    rptname = "KartuHutangDagang";
                }
                else //HS
                {
                    if (ReportType == "XLS")
                        rptfile = "rptKartuHutangSuspendXls.rpt";
                    else
                    {
                        rptfile = "rptKartuHutangSuspendPdf.rpt";
                    }
                    rptname = "KartuHutangSuspend";
                }

                if (DDLType == "HD")
                {
                    sSql = $"DECLARE @start AS DATETIME, @end DATETIME SET @start = '{StartDate}' SET @end = '{EndDate}' SELECT s.suppoid suppoid, s.suppname suppname, g.gndesc term, CASE s.supppajak WHEN '1' THEN 'PPN' ELSE 'NON PPN' END taxtype, ISNULL(awal,0.0) awal, ISNULL(beli,0.0) beli, ISNULL(bayar,0.0) bayar, ISNULL(awal,0.0)+ISNULL(beli,0.0)-ISNULL(bayar,0.0) saldo, ISNULL(umur0,0.0) umur0, ISNULL(umur1,0.0) umur1, ISNULL(umur2,0.0) umur2, ISNULL(umur3,0.0) umur3 FROM QL_mstsupp s INNER JOIN QL_m05GN g ON g.gnoid = s.supppaymentoid AND gngroup = 'PAYMENT TERM' INNER JOIN (/*TRANSAKSI*/ SELECT ap.suppoid, SUM(awal) awal, SUM(beli) beli, SUM(bayar) bayar, SUM(umur0) umur0, SUM(umur1) umur1, SUM(umur2) umur2, SUM(umur3) umur3 FROM( SELECT aph.suppoid, aph.apmstoid, aph.apno, aph.apdate, SUM(awal) awal, SUM(beli) beli, SUM(bayar) bayar, SUM(umur0) umur0, SUM(umur1) umur1, SUM(umur2) umur2, SUM(umur3) umur3 FROM( SELECT apm.suppoid suppoid, apm.apitemmstoid apmstoid, apm.apitemno apno, @start apdate, ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid), 0.0) awal, 0.0 beli, 0.0 bayar, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnapitemmst apm WHERE apm.apitemmststatus IN('Post', 'Approved', 'Closed') AND apm.apitemdate < @start + ' 23:00:00' UNION ALL SELECT apm.suppoid suppoid, apm.apitemmstoid apmstoid, apm.apitemno apno, apm.apitemdate apdate, 0.0 awal, ISNULL((SELECT SUM(con.amttrans) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid AND con.payrefoid = 0), 0.0) beli, ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid AND con.payrefoid <> 0), 0.0) bayar, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnapitemmst apm WHERE apm.apitemmststatus IN('Post', 'Approved', 'Closed') AND apm.apitemdate >= @start + ' 00:00:00' AND apm.apitemdate <= @end + ' 23:00:00' UNION ALL SELECT apm.suppoid suppoid, apm.apitemmstoid apmstoid, apm.apitemno apno, apm.apitemdate apdate, 0.0 awal, 0.0 beli, 0.0 bayar, CASE WHEN DATEDIFF(DAY, apm.apitemduedate, GETDATE()) < 0 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid),0.0) ELSE 0.0 END umur0, CASE WHEN DATEDIFF(DAY, apm.apitemduedate, GETDATE()) BETWEEN 0 AND 30 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid),0.0) ELSE 0.0 END umur1, CASE WHEN DATEDIFF(DAY, apm.apitemduedate, GETDATE()) BETWEEN 31 AND 90 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid),0.0) ELSE 0.0 END umur2, CASE WHEN DATEDIFF(DAY, apm.apitemduedate, GETDATE())> 90 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid),0.0) ELSE 0.0 END umur3 FROM QL_trnapitemmst apm WHERE apm.apitemmststatus IN('Post', 'Approved', 'Closed') AND ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapitemmst' AND con.refoid = apm.apitemmstoid),0.0)> 0 )AS aph GROUP BY aph.suppoid, aph.apmstoid, aph.apno, aph.apdate UNION ALL SELECT aph2.suppoid, aph2.apmstoid, aph2.apno, aph2.apdate, SUM(awal) awal, SUM(beli) beli, SUM(bayar) bayar, SUM(umur0) umur0, SUM(umur1) umur1, SUM(umur2) umur2, SUM(umur3) umur3 FROM( SELECT apm.suppoid suppoid, apm.apassetmstoid apmstoid, apm.apassetno apno, @start apdate, ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid), 0.0) awal, 0.0 beli, 0.0 bayar, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnapassetmst apm WHERE apm.apassetmststatus IN('Post', 'Approved', 'Closed') AND apm.apassetdate < @start + ' 23:00:00' UNION ALL SELECT apm.suppoid suppoid, apm.apassetmstoid apmstoid, apm.apassetno apno, apm.apassetdate apdate, 0.0 awal, ISNULL((SELECT SUM(con.amttrans) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid AND con.payrefoid = 0),0.0) beli, ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid AND con.payrefoid <> 0),0.0) bayar, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnapassetmst apm WHERE apm.apassetmststatus IN('Post', 'Approved', 'Closed') AND apm.apassetdate >= @start + ' 00:00:00' AND apm.apassetdate <= @end + ' 23:00:00' UNION ALL SELECT apm.suppoid suppoid, apm.apassetmstoid apmstoid, apm.apassetno apno, apm.apassetdate apdate, 0.0 awal, 0.0 beli, 0.0 bayar, CASE WHEN DATEDIFF(DAY, apm.apassetduedate, GETDATE()) < 0 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid),0.0) ELSE 0.0 END umur0, CASE WHEN DATEDIFF(DAY, apm.apassetduedate, GETDATE()) BETWEEN 0 AND 30 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid),0.0) ELSE 0.0 END umur1, CASE WHEN DATEDIFF(DAY, apm.apassetduedate, GETDATE()) BETWEEN 31 AND 90 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid),0.0) ELSE 0.0 END umur2, CASE WHEN DATEDIFF(DAY, apm.apassetduedate, GETDATE())> 90 THEN ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid),0.0) ELSE 0.0 END umur3 FROM QL_trnapassetmst apm WHERE apm.apassetmststatus IN('Post', 'Approved', 'Closed') AND ISNULL((SELECT SUM(con.amttrans - amtbayar) FROM QL_conap con WHERE con.reftype = 'QL_trnapassetmst' AND con.refoid = apm.apassetmstoid),0.0)> 0 )AS aph2 GROUP BY aph2.suppoid, aph2.apmstoid, aph2.apno, aph2.apdate )AS ap GROUP BY ap.suppoid /*END TRANSAKSI*/ )AS tr ON tr.suppoid = s.suppoid";
                }
                else //HS
                {
                    sSql = $"DECLARE @start AS DATETIME, @end DATETIME SET @start = '{StartDate}' SET @end = '{EndDate}' SELECT s.suppoid suppoid, s.suppname suppname, g.gndesc term, CASE s.supppajak WHEN '1' THEN 'PPN' ELSE 'NON PPN' END taxtype, ISNULL(awal,0.0) awal, ISNULL(beli,0.0) beli, ISNULL(bayar,0.0) bayar, ISNULL(awal,0.0)+ISNULL(beli,0.0)-ISNULL(bayar,0.0) saldo, ISNULL(saldoppn,0.0) saldoppn, ISNULL(umur0,0.0) umur0, ISNULL(umur1,0.0) umur1, ISNULL(umur2,0.0) umur2, ISNULL(umur3,0.0) umur3 FROM QL_mstsupp s INNER JOIN QL_m05GN g ON g.gnoid = s.supppaymentoid AND gngroup = 'PAYMENT TERM' INNER JOIN (/*TRANSAKSI*/ SELECT ap.suppoid, SUM(awal) awal, SUM(beli) beli, SUM(bayar) bayar, SUM(saldoppn) saldoppn, SUM(umur0) umur0, SUM(umur1) umur1, SUM(umur2) umur2, SUM(umur3) umur3 FROM( SELECT aph.suppoid, aph.mrmstoid, aph.mrno, aph.mrdate, SUM(awal) awal, SUM(beli) beli, SUM(bayar) bayar, SUM(saldoppn) saldoppn, SUM(umur0) umur0, SUM(umur1) umur1, SUM(umur2) umur2, SUM(umur3) umur3 FROM( SELECT mrm.suppoid suppoid, mrm.mritemmstoid mrmstoid, mrm.mritemno mrno, mrm.mritemdate mrdate, ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid), 0.0) - ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')), 0.0) awal, 0.0 beli, 0.0 bayar, CAST(ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid), 0.0) + ISNULL((SELECT SUM(CAST(CASE WHEN pod.poitemdtltaxvalue > 0 THEN((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty) * (pod.poitemdtltaxvalue / 100) ELSE 0.0 END AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid), 0.0) -ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt + pod.apitemdtltaxamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) AS DECIMAL(18, 0)) saldoppn, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnmritemmst mrm WHERE mrm.mritemmststatus IN('Post', 'Approved', 'Closed') AND mrm.mritemdate < @start + ' 00:00:00' UNION ALL SELECT mrm.suppoid suppoid, mrm.mritemmstoid mrmstoid, mrm.mritemno mrno, mrm.mritemdate mrdate, 0.0 awal, ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid),0.0) beli, ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) bayar, CAST(ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid), 0.0) + ISNULL((SELECT SUM(CAST(CASE WHEN pod.poitemdtltaxvalue > 0 THEN((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty) * (pod.poitemdtltaxvalue / 100) ELSE 0.0 END AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt + pod.apitemdtltaxamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) AS DECIMAL(18, 0)) saldoppn, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnmritemmst mrm WHERE mrm.mritemmststatus IN('Post', 'Approved', 'Closed') AND mrm.mritemdate >= @start + ' 00:00:00' AND mrm.mritemdate <= @end + ' 23:00:00' UNION ALL SELECT mrm.suppoid suppoid, mrm.mritemmstoid mrmstoid, mrm.mritemno mrno, mrm.mritemdate mrdate, 0.0 awal, 0.0 beli, 0.0 bayar, 0.0 saldoppn, CASE WHEN DATEDIFF(DAY, DATEADD(DAY, ISNULL((SELECT CAST(g.gnother1 AS INTEGER) FROM QL_m05GN g WHERE g.gnoid = pom.poitempaytypeoid AND gngroup = 'PAYMENT TERM'), 0), pom.poitemdate),GETDATE())< 0 THEN ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur0, CASE WHEN DATEDIFF(DAY, mrm.mritemdate, GETDATE()) BETWEEN 0 AND 30 THEN ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur1, CASE WHEN DATEDIFF(DAY, mrm.mritemdate, GETDATE()) BETWEEN 31 AND 90 THEN ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur2, CASE WHEN DATEDIFF(DAY, mrm.mritemdate, GETDATE())> 90 THEN ISNULL((SELECT SUM(CAST((((pod.poitemqty * pod.poitemprice) - pod.poitemdtldiscamt) / pod.poitemqty) * mrd.mritemqty AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poitemdtloid = mrd.podtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apitemqty * pod.apitemprice) - pod.apitemdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnapitemmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apitemmstoid = pod.apitemmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mritemmstoid = mrm.mritemmstoid AND apm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur3 FROM QL_trnmritemmst mrm INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode = mrm.cmpcode AND pom.poitemmstoid = mrm.pomstoid WHERE mrm.mritemmststatus IN('Post', 'Approved', 'Closed') )AS aph GROUP BY aph.suppoid, aph.mrmstoid, aph.mrno, aph.mrdate UNION ALL SELECT aph2.suppoid, aph2.mrmstoid, aph2.mrno, aph2.mrdate, SUM(awal) awal, SUM(beli) beli, SUM(bayar) bayar, SUM(saldoppn) saldoppn, SUM(umur0) umur0, SUM(umur1) umur1, SUM(umur2) umur2, SUM(umur3) umur3 FROM( SELECT mrm.suppoid suppoid, mrm.mrassetmstoid mrmstoid, mrm.mrassetno mrno, mrm.mrassetdate mrdate, ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid), 0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) awal, 0.0 beli, 0.0 bayar, CAST(ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid), 0.0) + ISNULL((SELECT SUM(CAST(CASE WHEN pod.poassetdtltaxvalue > 0 THEN((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty) * (pod.poassetdtltaxvalue / 100) ELSE 0.0 END AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt + pod.apassetdtltaxamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) AS DECIMAL(18, 0)) saldoppn, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnmrassetmst mrm WHERE mrm.mrassetmststatus IN('Post', 'Approved', 'Closed') AND mrm.mrassetdate < @start + ' 00:00:00' UNION ALL SELECT mrm.suppoid suppoid, mrm.mrassetmstoid mrmstoid, mrm.mrassetno mrno, mrm.mrassetdate mrdate, 0.0 awal, ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) beli, ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) bayar, CAST(ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid), 0.0) + ISNULL((SELECT SUM(CAST(CASE WHEN pod.poassetdtltaxvalue > 0 THEN((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty) * (pod.poassetdtltaxvalue / 100) ELSE 0.0 END AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt + pod.apassetdtltaxamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) AS DECIMAL(18, 0)) saldoppn, 0.0 umur0, 0.0 umur1, 0.0 umur2, 0.0 umur3 FROM QL_trnmrassetmst mrm WHERE mrm.mrassetmststatus IN('Post', 'Approved', 'Closed') AND mrm.mrassetdate >= @start + ' 00:00:00' AND mrm.mrassetdate <= @end + ' 23:00:00' UNION ALL SELECT mrm.suppoid suppoid, mrm.mrassetmstoid mrmstoid, mrm.mrassetno mrno, mrm.mrassetdate mrdate, 0.0 awal, 0.0 beli, 0.0 bayar, 0.0 saldoppn, CASE WHEN DATEDIFF(DAY, DATEADD(DAY, ISNULL((SELECT CAST(g.gnother1 AS INTEGER) FROM QL_m05GN g WHERE g.gnoid = pom.poassetpaytypeoid AND gngroup = 'PAYMENT TERM'), 0), pom.poassetdate),GETDATE())< 0 THEN ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur0, CASE WHEN DATEDIFF(DAY, mrm.mrassetdate, GETDATE()) BETWEEN 0 AND 30 THEN ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur1, CASE WHEN DATEDIFF(DAY, mrm.mrassetdate, GETDATE()) BETWEEN 31 AND 90 THEN ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur2, CASE WHEN DATEDIFF(DAY, mrm.mrassetdate, GETDATE())> 90 THEN ISNULL((SELECT SUM(CAST((((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) * mrd.mrassetqty AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid),0.0) -ISNULL((SELECT SUM(CAST((pod.apassetqty * pod.apassetprice) - pod.apassetdtldiscamt AS DECIMAL(18, 4))) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.mrassetdtloid = mrd.mrassetdtloid INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = mrd.cmpcode AND apm.apassetmstoid = pod.apassetmstoid WHERE mrd.cmpcode = mrm.cmpcode AND mrd.mrassetmstoid = mrm.mrassetmstoid AND apm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) ELSE 0.0 END umur3 FROM QL_trnmrassetmst mrm INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode = mrm.cmpcode AND pom.poassetmstoid = mrm.poassetmstoid WHERE mrm.mrassetmststatus IN('Post', 'Approved', 'Closed') )AS aph2 GROUP BY aph2.suppoid, aph2.mrmstoid, aph2.mrno, aph2.mrdate )AS ap GROUP BY ap.suppoid /*END TRANSAKSI*/ )AS tr ON tr.suppoid = s.suppoid";
                }
                var suppoid = GetSuppoidByCode(TextCustomer);
                if (!string.IsNullOrEmpty(TextCustomer))
                {
                    sSql += " WHERE s.suppoid IN (" + suppoid.Replace(";", "', ") + ")";
                }

                DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
                DataView dvRpt = dtRpt.DefaultView;
                Dictionary<string, object> rptparam = new Dictionary<string, object>();
                rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartDate) + " - " + ClassFunction.toDate(EndDate));
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("RptFile", rptfile);

                if (ReportType == "")
                {
                    this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                    this.HttpContext.Session["rptparam"] = rptparam;
                    return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ReportDocument report = new ReportDocument();
                    report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                    report.SetDataSource(dvRpt.ToTable());
                    if (rptparam.Count > 0)
                        foreach (var item in rptparam)
                            report.SetParameterValue(item.Key, item.Value);
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    if (ReportType == "XLS")
                    {
                        Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                        stream.Seek(0, SeekOrigin.Begin);
                        report.Close(); report.Dispose();
                        return File(stream, "application/excel", rptname + ".xls");
                    }
                    else
                    {
                        Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        stream.Seek(0, SeekOrigin.Begin);
                        report.Close(); report.Dispose();
                        return File(stream, "application/pdf", rptname + ".pdf");
                    }
                }
            }
            else //summary & detail
            {
                var rptfile = ""; var rptname = "";
                if (DDLType == "Summary")
                {
                    if (ReportType == "XLS")
                        rptfile = "CrHutangSumPdf.rpt";
                    else
                        rptfile = "CrHutangSumPdf.rpt";
                    rptname = "KARTU_HUTANG_SUMMARY";
                }
                else if (DDLType == "Summary2")
                {
                    if (ReportType == "XLS")
                        rptfile = "CrHutangInvSumPdf.rpt";
                    else
                        rptfile = "CrHutangInvSumPdf.rpt";
                    rptname = "KARTU_HUTANGINV_SUMMARY";
                }
                else
                {
                    if (ReportType == "XLS")
                        rptfile = "CrHutangDtlPdf.rpt";
                    else
                    {
                        rptfile = "CrHutangDtlPdf.rpt";
                    }
                    rptname = "KARTU_HUTANG_DETAIL";
                }

                var swhere = ""; var sWheresupp = "";
                var suppoid = GetSuppoidByCode(TextCustomer);
                if (!string.IsNullOrEmpty(TextCustomer))
                {
                    swhere = " WHERE suppoid IN ('" + suppoid.Replace(";", "', '") + "')";
                    sWheresupp = " WHERE s.suppoid IN ('" + suppoid.Replace(";", "', '") + "')";
                }

                var sFilterCOA = "";
                if (DDLCOA != "")
                {
                    sFilterCOA = " AND c.acctgoid=" + DDLCOA;
                    if (swhere == "")
                    {
                        swhere += " WHERE ";
                    }
                    else
                    {
                        swhere += " AND ";
                    }
                    swhere += " tbldata.acctgoid=" + DDLCOA + " ";
                }

                var DDLMonthUp = DDLMonth_Text.ToUpper();
                System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
                var awal = DateTime.ParseExact(StartDate, "MM/dd/yyyy", AppsCultureInfo);
                var akhir = DateTime.ParseExact(EndDate, "MM/dd/yyyy", AppsCultureInfo);
                string bulan = akhir.ToString("MM");
                string tahun = akhir.ToString("yyyy");
                string periode = ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(tahun), int.Parse(bulan), 1));
                string periode2 = ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(tahun), int.Parse(bulan), 1)));

                if (DDLType == "Summary")
                {
                    sSql = "DECLARE @cmpcode AS VARCHAR(10);" +
                    "DECLARE @periodacctg AS VARCHAR(10);" +
                    "DECLARE @currency AS VARCHAR(10);" +
                    "DECLARE @dateacuan AS DATETIME;" +
                    "SET @cmpcode='" + CompnyCode + "';" +
                    "SET @periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "';" +
                    "SET @dateacuan=CAST('" + new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), DateTime.DaysInMonth(int.Parse(DDLYear), int.Parse(DDLMonth))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME);" +
                    "SET @currency='IDR';" +
                    "/*UNION ALL QUERY*/" +
                    "SELECT suppoid, suppname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, " +
                    "SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, " +
                    "SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, " +
                    "SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, " +
                    "SUM(age3idr) age3idr, SUM(age3usd) age3usd, @currency currency, @cmpcode cmpcode, " +
                    "'" + DDLMonthUp + " " + DDLYear_Text + "' periodreport, '" + CompnyName + "' company, acctgoid" +
                    ", (SELECT acctgcode FROM QL_mstacctg acc WHERE acc.acctgoid=tbldata.acctgoid) kodePiutang, (SELECT acctgdesc FROM QL_mstacctg acc WHERE acc.acctgoid=tbldata.acctgoid) descPiutang " +
                    "FROM (" +
                    "/*SALDO AWAL PERIODE*/" +
                    "SELECT suppoid, suppname, " +
                    "(SUM(beliidr) - SUM(paymentidr)) saidr, " +
                    "(SUM(beliusd) - SUM(paymentusd)) sausd, " +
                    "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                    "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid FROM (" +
                    "SELECT suppoid, suppname, " +
                    "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " +
                    "0.0 paymentidr, 0.0 paymentusd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, " +
                    "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd, acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sWheresupp + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND c.conapoid>0 " +
                    "AND payrefoid=0 AND trnapstatus='Post' " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname, " +
                    "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, ISNULL((SELECT acctgoid FROM ql_conap c2 WHERE c2.cmpcode=c.cmpcode AND c2.reftype=c.reftype AND c2.refoid=c.refoid AND c2.suppoid=c.suppoid AND c2.payrefoid=0),0) acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND trnaptype IN ('DNAP', 'DNAPRET') " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname, " +
                    "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, ISNULL((SELECT acctgoid FROM ql_conap c2 WHERE c2.cmpcode=c.cmpcode AND c2.reftype=c.reftype AND c2.refoid=c.refoid AND c2.suppoid=c.suppoid AND c2.payrefoid=0),0) acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND trnaptype IN ('CNAP', 'CNAPRET','APRET','PAYDP') AND (c.amtbayaridr<0 OR c.amtbayarusd<0) " +
                    ") AS tblBeli GROUP BY suppoid, suppname, acctgoid " +
                    "UNION ALL " +
                    "SELECT suppoid, suppname, 0.0 beliidr, 0.0 beliusd," +
                    "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, " +
                    "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, c.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN ql_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<@periodacctg " + sFilterCOA + " GROUP BY con.suppoid, con.cmpcode, c.acctgoid) AS c ON c.suppoid=s.suppoid " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname," +
                    "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('CNAP', 'CNAPRET','APRET','PAYDP') " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname," +
                    "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('DNAP', 'DNAPRET')  AND (c.amtbayaridr>0 OR c.amtbayarusd>0) " +
                    ") AS tblpayment GROUP BY suppoid, suppname, acctgoid " +
                    ") AS tblSaldoAwalPeriod GROUP BY suppoid, suppname, acctgoid " +
                    "UNION ALL /*SALDO AWAL CUT OFF*/" +
                    "SELECT s.suppoid, s.suppname, " +
                    "(SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, " +
                    "(SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, " +
                    "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                    "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.conapoid<0 " +
                    "GROUP BY s.suppoid, s.suppname, acctgoid " +
                    "UNION ALL /*PENJUALAN*/" +
                    "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, " +
                    "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " +
                    "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                    "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND c.conapoid>0 " +
                    "AND payrefoid=0 AND trnapstatus='Post' " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname, " +
                    "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND trnaptype IN ('DNAP', 'DNAPRET') " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname, " +
                    "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND trnaptype IN ('CNAP', 'CNAPRET','APRET','PAYDP')  AND (c.amtbayaridr<0 OR c.amtbayarusd<0) " +
                    ") AS tblBeli GROUP BY suppoid, suppname, acctgoid " +
                    "UNION ALL /*PAYMENT*/" +
                    "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd," +
                    "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                    "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, " +
                    "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, c.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN ql_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)=@periodacctg " + sFilterCOA + "  GROUP BY con.suppoid, con.cmpcode, c.acctgoid) AS c ON c.suppoid=s.suppoid " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname," +
                    "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('CNAP', 'CNAPRET','APRET','PAYDP') " +
                    "UNION ALL " +
                    "SELECT s.suppoid, s.suppname," +
                    "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                    "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND c.trnaptype IN ('DNAP', 'DNAPRET')  AND (c.amtbayaridr>0 OR c.amtbayarusd>0) " +
                    ") AS tblpayment GROUP BY suppoid, suppname, acctgoid " +
                    "UNION ALL /*BELUM JATUH TEMPO*/" +
                    "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                    "0.0 paymentidr, 0.0 paymentusd, SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, " +
                    "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                    "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                    "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                    "AND c.payduedate>@dateacuan AND c.payrefoid=0 " +
                    "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<=@periodacctg " +
                    "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                    "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                    ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                    "UNION ALL /*0-30 DAYS*/" +
                    "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                    "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                    "SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, " +
                    "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                    "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                    "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                    "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                    "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))=@periodacctg " +
                    "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                    "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                    ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                    "UNION ALL /*31-60 DAYS*/" +
                    "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                    "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                    "SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, " +
                    "0.0 age3idr, 0.0 age3usd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                    "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                    "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                    "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                    "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))='" + ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1))) + "' " +
                    "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                    "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                    ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                    "UNION ALL /*>60 DAYS*/" +
                    "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                    "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                    "0.0 age2idr, 0.0 age2usd, " +
                    "SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd, acctgoid " +
                    "FROM (" +
                    "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                    "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                    "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                    "FROM ql_mstsupp s " +
                    "LEFT JOIN ql_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + "  " +
                    "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                    "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<'" + ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1))) + "' " +
                    "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN ql_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM ql_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('CNAP', 'DNAP', 'CNAPRET', 'DNAPRET','APRET','PAYDP') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                    "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid  " +
                    ") AS ag GROUP BY ag.suppoid, ag.suppname, acctgoid " +
                    ") AS tbldata " +
                    swhere +
                    "GROUP BY suppoid, suppname, acctgoid ";

                    if (cbHide)
                    {
                        sSql += "HAVING SUM(saidr)<>0 OR SUM(beliidr)<>0 OR SUM(paymentidr)<>0 OR SUM(age1idr)<>0 OR SUM(age2idr)<>0 OR SUM(age3idr)<>0 ";
                    }
                    sSql += "ORDER BY suppname";
                }
                else
                {
                    sSql = "DECLARE @cmpcode AS VARCHAR(10); " +
                         " DECLARE @start AS DATETIME;" +
                         " DECLARE @finish AS DATETIME;" +
                         " SET @cmpcode = '" + CompnyCode + "';" +
                         " SET @start = '" + awal + "'; " +
                         " SET @finish = '" + akhir + "';";
                    if (DDLType == "Summary2")
                    {
                        sSql += " SELECT e.cmpcode,e.suppoid, e.suppcode, e.suppname, getdate() transdate, '' transtype, e.transoid, e.trans_asal transno, '' currency, 0 ratetrans, ISNULL(SUM(e.amttrans), 0.0) amttrans, ISNULL(SUM(e.amttransidr), 0.0) amttransidr, ISNULL(SUM(e.amttransusd), 0.0) amttransusd, ISNULL(SUM(e.amtbayar), 0.0) amtbayar, ISNULL(SUM(e.amtbayaridr), 0.0) amtbayaridr, ISNULL(SUM(e.amtbayarusd), 0.0) amtbayarusd, ISNULL(SUM(e.amtbayarother), 0.0) amtbayarother, sawal, sawalidr, sawalusd, 0 no_sort, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=@cmpcode) companyname, '+ periodreport' periodreport, 'IDR' currvalue, '' AS [No.INV], '' AS [No.Faktur], '' AS kodePiutang, '' AS descPiutang, tglstartjt, tgljt, tempo, trans_asal, blmtempo, tempo1, tempo2, tempo3 FROM( ";
                    }
                    sSql += " SELECT * FROM ( /* CUSTOMER */ " +
                          " SELECT ISNULL(a.cmpcode, @cmpcode) cmpcode, s.suppoid, s.suppcode, s.suppname, ISNULL(a.transdate, @start) transdate, a.transtype, a.transoid, a.transno, a.currency, a.ratetrans, ISNULL(a.amttrans, 0.0) amttrans, ISNULL(a.amttransidr, 0.0) amttransidr, ISNULL(a.amttransusd, 0.0) amttransusd, ISNULL(a.amtbayar, 0.0) amtbayar, ISNULL(a.amtbayaridr, 0.0) amtbayaridr, ISNULL(a.amtbayarusd, 0.0) amtbayarusd, ISNULL(a.amtbayarother, 0.0) amtbayarother, SUM(ISNULL(b.amt, 0.0)) sawal, SUM(ISNULL(b.amtidr, 0.0)) sawalidr, SUM(ISNULL(b.amtusd, 0.0)) sawalusd, ISNULL(no_sort, 0) no_sort, '" + CompnyName + "' companyname, '+ periodreport' periodreport, 'IDR' currvalue, ISNULL((SELECT ISNULL(ar.apitemmstres3, '') FROM QL_trnapitemmst ar WHERE ar.cmpcode = @cmpcode AND ar.apitemmstoid = a.transoid AND a.transtype = 'QL_trnapitemmst' AND ar.suppoid = s.suppoid), '') AS [No.INV], ISNULL((SELECT ISNULL(ar.apitemfaktur, '') FROM QL_trnapitemmst ar WHERE ar.cmpcode = @cmpcode AND ar.apitemmstoid = a.transoid AND a.transtype = 'QL_trnapitemmst' AND ar.suppoid = s.suppoid),'') AS [No.Faktur], acc.acctgcode AS kodePiutang, acc.acctgdesc AS descPiutang, (SELECT CASE WHEN cc.reftype='QL_trnapitemmst' THEN (SELECT apm.apitemttdate FROM QL_trnapitemmst apm WHERE apm.apitemmstoid=cc.refoid AND cc.reftype='QL_trnapitemmst') WHEN cc.reftype='QL_trnapassetmst' THEN (SELECT apm.apassetttdate FROM QL_trnapassetmst apm WHERE apm.apassetmstoid=cc.refoid AND cc.reftype='QL_trnapassetmst') ELSE cc.trnapdate END FROM QL_conap cc WHERE cc.refoid=a.transoid AND cc.reftype=a.transtype AND cc.payrefoid=0) tglstartjt, (SELECT cc.payduedate FROM QL_conap cc WHERE cc.refoid=a.transoid AND cc.reftype=a.transtype AND cc.payrefoid=0) tgljt, (SELECT CAST(DATEDIFF(DAY, CASE WHEN cc.reftype='QL_trnapitemmst' THEN (SELECT apm.apitemttdate FROM QL_trnapitemmst apm WHERE apm.apitemmstoid=cc.refoid AND cc.reftype='QL_trnapitemmst') WHEN cc.reftype='QL_trnapassetmst' THEN (SELECT apm.apassetttdate FROM QL_trnapassetmst apm WHERE apm.apassetmstoid=cc.refoid AND cc.reftype='QL_trnapassetmst') ELSE cc.trnapdate END, cc.payduedate) AS VARCHAR(30))+ ' Hari' FROM QL_conap cc WHERE cc.refoid=a.transoid AND cc.reftype=a.transtype AND cc.payrefoid=0) tempo, (SELECT (CASE transtype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE apassetmstoid = cc.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE apitemmstoid = cc.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirno  FROM QL_trnapdirmst arm WHERE apdirmstoid = cc.refoid)  WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE kasbon2mstoid = cc.refoid) ELSE '' END) FROM QL_conap cc WHERE cc.refoid=a.transoid AND cc.reftype=a.transtype AND cc.payrefoid=0) trans_asal, ISNULL((SELECT SUM(cc.amttrans - cc.amtbayar) FROM QL_conap cc WHERE cc.refoid=a.transoid AND cc.reftype=a.transtype AND cc.payduedate > CAST('" + new DateTime(int.Parse(tahun), int.Parse(bulan), DateTime.DaysInMonth(int.Parse(tahun), int.Parse(bulan))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND cc.periodacctg<='" + periode + "'),0.0) blmtempo, ISNULL((SELECT SUM(cc.amttrans - cc.amtbayar) FROM QL_conap cc WHERE cc.refoid = a.transoid AND cc.reftype = a.transtype AND cc.payduedate <= CAST('" + new DateTime(int.Parse(tahun), int.Parse(bulan), DateTime.DaysInMonth(int.Parse(tahun), int.Parse(bulan))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND cc.periodacctg = '" + periode + "'),0.0) tempo1, ISNULL((SELECT SUM(cc.amttrans - cc.amtbayar) FROM QL_conap cc WHERE cc.refoid = a.transoid AND cc.reftype = a.transtype AND cc.payduedate <= CAST('" + new DateTime(int.Parse(tahun), int.Parse(bulan), DateTime.DaysInMonth(int.Parse(tahun), int.Parse(bulan))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND cc.periodacctg = '" + periode2 + "'),0.0) tempo2, ISNULL((SELECT SUM(cc.amttrans - cc.amtbayar) FROM QL_conap cc WHERE cc.refoid = a.transoid AND cc.reftype = a.transtype AND cc.payduedate <= CAST('" + new DateTime(int.Parse(tahun), int.Parse(bulan), DateTime.DaysInMonth(int.Parse(tahun), int.Parse(bulan))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME) AND cc.periodacctg < '" + periode2 + "'),0.0) tempo3 FROM ql_mstsupp s INNER JOIN QL_mstacctg acc ON acc.cmpcode = s.cmpcode AND acc.acctgoid IN (SELECT con.acctgoid FROM ql_conap con WHERE con.cmpcode = @cmpcode AND payrefoid = 0 AND s.suppoid = con.suppoid) " +
                          " LEFT JOIN (/* DETIL IN-OUT AR Detil Invoice  */" +
                          " SELECT ap.cmpcode, ap.suppoid, ap.trnapdate transdate, ap.reftype transtype, ap.refoid transoid, 1 no_sort, vap.transno, '' currency, (CASE WHEN ap.amttrans = 0 THEN 0 ELSE ROUND(ap.amttransidr / ap.amttrans, 2) END) ratetrans, ap.amttrans, ap.amttransidr, ap.amttransusd, ap.amtbayar, ap.amtbayaridr, ap.amtbayarusd, ap.amtbayarother, ap.acctgoid FROM ql_conap ap INNER JOIN ( SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirno  FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid)  WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2mststatus FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid)  ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap.cmpcode AND vap.conapoid = ap.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.payrefoid = 0 AND ap.amttrans >= 0 ";
                    if (DDLCOA != "")
                    {
                        sSql += " AND ap.acctgoid=" + DDLCOA;
                    }

                    sSql += " UNION ALL /* Detil Payment */" +
                        " SELECT ap.cmpcode, ap.suppoid, ISNULL((SELECT CASE WHEN LEFT(cashbanktype, 2) = 'BB' THEN cb.cashbankduedate ELSE cb.cashbankdate END AS transdate FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pap ON pap.cmpcode = cb.cmpcode AND pap.cashbankoid = cb.cashbankoid WHERE pap.cmpcode = ap.cmpcode AND pap.payapoid = ap.payrefoid AND pap.payapres1 <> 'Lebih Bayar' AND cb.cashbankstatus = 'POST'), ap.paymentdate) transdate, ap.reftype transtype, ap.refoid transoid, 3 no_sort , ISNULL((SELECT cb.cashbankno FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pap ON pap.cmpcode = cb.cmpcode AND pap.cashbankoid = cb.cashbankoid AND cb.cashbankstatus = 'POST' WHERE pap.cmpcode = ap.cmpcode AND pap.payapoid = ap.payrefoid AND pap.payapres1 <> 'Lebih Bayar'), ap.payrefno) transno, 'IDR' currency, (CASE WHEN ap2.amttrans = 0 THEN 0 ELSE ROUND(ap2.amttransidr / ap2.amttrans, 2) END) ratetrans, ap.amttrans, ap.amttransidr, ap.amttransusd, ap.amtbayar, ap.amtbayaridr, ap.amtbayarusd, ap.amtbayarother, ap2.acctgoid FROM ql_conap ap INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirno  FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2mststatus FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype LIKE 'PAYAP%' AND ap.payrefoid <> 0 AND ap.payrefoid NOT IN(SELECT pap.payapoid FROM QL_trnpayap pap WHERE pap.cmpcode = ap.cmpcode AND pap.suppoid = ap.suppoid AND ISNULL(pap.payapres1, '') = 'Lebih Bayar') " +
                        " UNION ALL /* Detil DN-CN */ " +
                        " SELECT ap.cmpcode, ap.suppoid, ap.paymentdate transdate, ap.reftype transtype, ap.refoid transoid, 4 no_sort, ap.payrefno transno, 'IDR' currency, (CASE WHEN ap2.amttrans = 0 THEN 0 ELSE ROUND(ap2.amttransidr / ap2.amttrans, 2) END) ratetrans, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN(ap.amtbayar * -1) WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayar < 0 THEN(ap.amtbayar * -1) ELSE 0 END) ELSE ap.amttrans END) amttrans, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN (ap.amtbayaridr * -1) WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayaridr < 0 THEN(ap.amtbayaridr * -1) ELSE 0 END) ELSE ap.amttransidr END) amttransidr, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN(ap.amtbayarusd * -1) WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayarusd < 0 THEN(ap.amtbayarusd * -1) ELSE 0 END) ELSE ap.amttransusd END) amttransusd, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN 0 WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayar > 0 THEN ap.amtbayar ELSE 0 END) ELSE ap.amtbayar END) amtbayar, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN 0 WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayaridr > 0 THEN ap.amtbayaridr ELSE 0 END) ELSE ap.amtbayaridr END) amtbayaridr, (CASE WHEN ap.trnaptype IN ('CNAP', 'CNAPRET') THEN 0 WHEN ap.trnaptype IN ('DNAP', 'DNAPRET') THEN (CASE WHEN ap.amtbayarusd > 0 THEN ap.amtbayarusd ELSE 0 END) ELSE ap.amtbayarusd END) amtbayarusd, ap.amtbayarother, ap2.acctgoid FROM ql_conap ap INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirno  FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2mststatus FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid)  ELSE '' END) AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype IN ('DNAP', 'CNAP', 'DNAPRET', 'CNAPRET', 'GL','APRET','PAYDP')  AND ap.payrefoid <> 0 ";
                    if (DDLCOA != "")
                    {
                        sSql += " AND ap2.acctgoid=" + DDLCOA;
                    }
                    sSql += " ) AS a ON a.suppoid = s.suppoid AND a.transdate >= @start AND a.transdate <= @finish ";

                    sSql += " LEFT JOIN ( /* SALDO AWAL */ /* Detil Invoice */ " +
                        " SELECT ap.cmpcode, ap.suppoid, ap.acctgoid, ap.trnapdate transdate, 'IDR' currency, ap.amttrans amt, ap.amttransidr amtidr, ap.amttransusd amtusd FROM ql_conap ap INNER JOIN(SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN(SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN(SELECT apdirno  FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN(SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN(SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2mststatus FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap.cmpcode AND vap.conapoid = ap.conapoid INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid WHERE ap.cmpcode = @cmpcode AND ap.payrefoid = 0";
                    if (DDLCOA != "")
                    {
                        sSql += " AND ap.acctgoid=" + DDLCOA;
                    }
                    sSql += " UNION ALL /* Detil Payment */ SELECT ap.cmpcode, ap.suppoid, ap2.acctgoid, ISNULL((SELECT CASE WHEN LEFT(cashbanktype, 2) = 'BB' THEN cb.cashbankduedate ELSE cb.cashbankdate END AS transdate FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pap ON pap.cmpcode = cb.cmpcode AND pap.cashbankoid = cb.cashbankoid WHERE pap.cmpcode = ap.cmpcode AND pap.payapoid = ap.payrefoid AND pap.payapres1 <> 'Lebih Bayar' AND cb.cashbankstatus = 'POST'), ap.paymentdate ) transdate, 'IDR' currency, (ap.amtbayar * -1) amt, (ap.amtbayaridr * -1) amtidr, (ap.amtbayarusd * -1) amtusd FROM ql_conap ap INNER JOIN ql_mstsupp c ON c.suppoid = ap.suppoid INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid)  WHEN 'QL_trnapdirmst' THEN (SELECT apdirno  FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid)  WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid)  WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2mststatus FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype LIKE 'PAYAP%' AND ap.payrefoid <> 0 AND ap.payrefoid NOT IN(SELECT pap.payapoid FROM QL_trnpayap pap WHERE pap.cmpcode = ap.cmpcode AND pap.suppoid = ap.suppoid AND ISNULL(pap.payapres1, '') = 'Lebih Bayar') ";
                    if (DDLCOA != "")
                    {
                        sSql += " AND ap2.acctgoid=" + DDLCOA;
                    }

                    sSql += " UNION ALL /* Detil DN-CN */ SELECT ap.cmpcode, ap.suppoid, ap2.acctgoid, ap.paymentdate transdate, 'IDR' currency, (ap.amtbayar * -1) amt, (ap.amtbayaridr * -1) amtidr, (ap.amtbayarusd * -1) amtusd FROM ql_conap ap INNER JOIN ql_conap ap2 ON ap.cmpcode = ap2.cmpcode AND ap.refoid = ap2.refoid AND ap.reftype = ap2.reftype AND ap2.payrefoid = 0 INNER JOIN (SELECT cmpcode, conapoid, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemno  FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirno  FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2no  FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END ) AS transno, (CASE reftype WHEN 'QL_trnapassetmst' THEN (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) WHEN 'QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) WHEN 'QL_trnkasbon2mst' THEN (SELECT kasbon2mststatus FROM QL_trnkasbon2mst arm WHERE arm.cmpcode = con.cmpcode AND kasbon2mstoid = con.refoid) ELSE '' END) AS transstatus, con.payduedate AS transduedate, 'IDR' AS currencysymbol FROM ql_conap AS con WHERE(payrefoid = 0) ) vap ON vap.cmpcode = ap2.cmpcode AND vap.conapoid = ap2.conapoid WHERE ap.cmpcode = @cmpcode AND ap.trnapstatus = 'Post' AND ap.trnaptype IN ('DNAP', 'CNAP', 'DNAPRET', 'CNAPRET', 'GL','APRET','PAYDP') AND ap.payrefoid <> 0";
                    if (DDLCOA != "")
                    {
                        sSql += " AND ap2.acctgoid=" + DDLCOA;
                    }

                    sSql += ") AS b ON b.acctgoid = acc.acctgoid AND b.suppoid = s.suppoid AND b.transdate < @start " +
                           " GROUP BY a.cmpcode, s.suppoid, s.suppcode, s.suppname, a.transdate, a.transtype, a.transoid, a.transno, a.currency, a.ratetrans , a.amttrans, a.amttransidr, a.amttransusd, a.amtbayar, a.amtbayaridr, a.amtbayarusd, a.amtbayarother, no_sort, acc.acctgcode, acc.acctgdesc ) AS d " + swhere + " ";
                    if (DDLType == "Summary2")
                    {
                        sSql += " ) AS e GROUP BY e.cmpcode, e.suppoid, e.suppcode, e.suppname, e.transoid, e.trans_asal, sawal, sawalidr, sawalusd, tglstartjt, tgljt, tempo, blmtempo, tempo1, tempo2, tempo3 ORDER BY suppname, transoid ";
                    }
                    else
                    {
                        sSql += " ORDER BY d.suppname, d.transdate, d.currency, no_sort, d.transoid";
                    }
                }

                DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
                DataView dvRpt = dtRpt.DefaultView;
                Dictionary<string, object> rptparam = new Dictionary<string, object>();
                if (DDLType == "Summary")
                {
                    rptparam.Add("Periode", "Periode : " + DDLMonth_Text + "-" + DDLYear_Text);
                }
                else
                {
                    rptparam.Add("start", ClassFunction.toDate(StartDate));
                    rptparam.Add("Periode", Convert.ToDateTime(StartDate).ToString("dd MM yyyy") + "-" + Convert.ToDateTime(EndDate).ToString("dd MM yyyy"));
                }
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("RptFile", rptfile);
                rptparam.Add("CompnyName", CompnyName);

                if (ReportType == "")
                {
                    this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                    if (DDLType == "Summary")
                    {
                        this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                    }
                    else
                    {
                        this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    }
                    this.HttpContext.Session["rptparam"] = rptparam;
                    return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ReportDocument report = new ReportDocument();
                    report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                    report.SetDataSource(dvRpt.ToTable());
                    if (rptparam.Count > 0)
                        foreach (var item in rptparam)
                            report.SetParameterValue(item.Key, item.Value);
                    if (DDLType == "Summary")
                    {
                        report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                    }
                    else
                    {
                        report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    }
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    if (ReportType == "XLS")
                    {
                        Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                        stream.Seek(0, SeekOrigin.Begin);
                        report.Close(); report.Dispose();
                        return File(stream, "application/excel", rptname + ".xls");
                    }
                    else
                    {
                        Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        stream.Seek(0, SeekOrigin.Begin);
                        report.Close(); report.Dispose();
                        return File(stream, "application/pdf", rptname + ".pdf");
                    }
                }
            }   
        }

    }
}