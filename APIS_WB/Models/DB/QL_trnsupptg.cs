//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnsupptg
    {
        public string cmpcode { get; set; }
        public int supptgoid { get; set; }
        public int suppoid { get; set; }
        public int pomstoid { get; set; }
        public string poreftype { get; set; }
        public decimal pototalamt { get; set; }
        public string imgpo { get; set; }
        public string imgsj { get; set; }
        public string imginv { get; set; }
        public string imgfp { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
