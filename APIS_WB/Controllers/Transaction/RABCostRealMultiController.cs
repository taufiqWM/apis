﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class RABCostRealMultiController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public RABCostRealMultiController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listDDLsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
        }
        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;

            var pphacctgoid = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(BindListDDLCOA("VAR_PPH")).ToList(), "acctgoid", "acctgdesc", tbl.pphacctgoid);
            ViewBag.pphacctgoid = pphacctgoid;

            string get_coa = ClassFunction.GetDataAcctgOid("VAR_PPH", CompnyCode);
            ViewBag.dt_coa = db.Database.SqlQuery<SelectListItem>("SELECT CAST(acctgoid as varchar(30)) [Value], ('(' + acctgcode + ') ' + acctgdesc) [Text] FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + get_coa + ") order by [Value]").ToList();

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname");
            ViewBag.dt_sales = salesoid;
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tbl.rabmstoid + "").FirstOrDefault();
            ViewBag.kasbonrefno = db.Database.SqlQuery<string>("SELECT kasbon_no FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankoid='" + tbl.kasbon_ref_id + "'").FirstOrDefault();
            if (tbl.kasbonrefamt == null)
            {
                tbl.kasbonrefamt = db.Database.SqlQuery<decimal>("SELECT cashbankdpp FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankoid='" + tbl.kasbon_ref_id + "'").FirstOrDefault();
            }
            if (tbl.kasbonrefamt2 == null)
            {
                tbl.kasbonrefamt2 = db.Database.SqlQuery<decimal>("SELECT ISNULL((SELECT SUM(x.pphcreditamt) FROM QL_trncashbankgl x WHERE x.cashbankoid=cb.cashbankoid),0.0) kasbonamt2 FROM QL_trncashbankmst cb WHERE cmpcode='" + CompnyCode + "' AND cashbankoid='" + tbl.kasbon_ref_id + "'").FirstOrDefault();
            }
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmpcode, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        private string GenerateExpenseNo2(string cmpcode, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateKasbonNo(string cmpcode, string cashbankdate)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = "KBN/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(kasbon_no, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND kasbon_no LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        private string GenerateKasbonNo2(string cmpcode, string cashbankdate)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = "KBN/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(kasbon_no, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND kasbon_no LIKE '%" + sNo + "%'";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        private string BindListDDLCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return sSql;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<ReportModels.DDLAccountModel> tbl = new List<ReportModels.DDLAccountModel>();
            tbl = db.Database.SqlQuery<ReportModels.DDLAccountModel>(BindListDDLCOA(sVar)).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT suppoid, suppcode, suppname, suppaddr, (CASE supppajak WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS supptaxable, 10.00 AS supptaxvalue FROM QL_mstsupp WHERE cmpcode='{CompnyCode}' AND activeflag='ACTIVE' ORDER BY suppcode";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetKasbonData(int cashbankoid)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT cb.cashbankoid kasbonoid, cb.kasbon_no kasbonno, cb.cashbankdate kasbondate, (cb.cashbankdpp - ISNULL((SELECT SUM(x.kasbonrefamt) FROM QL_trncashbankmst x WHERE x.cmpcode=cb.cmpcode AND x.cashbankgroup='RABMULTIREAL' AND x.cost_type='KASBON REAL' AND ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid AND x.cashbankoid<>{cashbankoid}),0.0)) kasbonamt, ISNULL((SELECT SUM(x.pphcreditamt) FROM QL_trncashbankgl x WHERE x.cashbankoid=cb.cashbankoid),0.0) - ISNULL((SELECT SUM(x.kasbonrefamt2) FROM QL_trncashbankmst x WHERE x.cmpcode=cb.cmpcode AND x.cashbankgroup='RABMULTIREAL' AND x.cost_type='KASBON REAL' AND ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid AND x.cashbankoid<>{cashbankoid}),0.0)kasbonamt2, cb.cashbanknote kasbonnote, ISNULL(cb.rabmstoid,0) rabmstoid, ISNULL((SELECT projectname FROM QL_trnrabmst x WHERE x.rabmstoid=ISNULL(cb.rabmstoid,0)),'') projectname FROM QL_trncashbankmst cb WHERE cb.cmpcode='{CompnyCode}' AND cb.cashbankgroup='RABMULTIREAL' AND cb.cost_type='KASBON' AND (cb.cashbankdpp - ISNULL((SELECT SUM(x.kasbonrefamt) FROM QL_trncashbankmst x WHERE x.cmpcode=cb.cmpcode AND x.cashbankgroup='RABMULTIREAL' AND x.cost_type='KASBON REAL' AND ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid AND x.cashbankoid<>{cashbankoid}),0.0)) > 0 ORDER BY cb.kasbon_no";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetRABHdrData(string filterperiodfrom, string filterperiodto)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabtype, c.custname, rm.rabmstnote, rm.curroid, rm.deptoid FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid WHERE rm.rabmststatus='Approved' AND rm.rabmstoid IN(SELECT rabmstoid FROM QL_trnrabdtl5) {(!string.IsNullOrEmpty(filterperiodfrom) && !string.IsNullOrEmpty(filterperiodto) ? $"AND rm.rabdate >='{filterperiodfrom} 00:00:00' AND rm.rabdate <='{filterperiodto} 23:00:00'" : "")} ORDER BY rabdate DESC";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetCOAEdit(string sVar)
        {
            JsonResult js = null;
            try
            {
                string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
                sSql = $"SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2 FROM QL_mstacctg WHERE cmpcode='{CompnyCode}' AND activeflag='ACTIVE' AND acctgoid IN ({acctgoid}) ORDER BY acctgcode";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class listcashbankgl : QL_trncashbankgl
        {
            public string projectname { get; set; }
            public string soitemno { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal rabdtl5amt { get; set; }
            public string acctgcodereal { get; set; }
            public string acctgdescreal { get; set; }
            public string referensi { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cashbanktype, string cost_type, int rabmstoid, string filterperiodfrom, string filterperiodto)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<listcashbankgl>($"SELECT 0 cashbankglseq, rd5.rabmstoid, projectname, soitemno, rd5.rabdtl5oid, rd5.acctgoid, a.acctgcode, a.acctgdesc, a.acctgcode acctgcodereal, a.acctgdesc acctgdescreal, /*(rd5.rabdtl5price - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0) - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0) - ISNULL((SELECT SUM(amt) FROM (SELECT CASE ISNULL(cb.cost_type,'') WHEN 'KASBON' THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0)>0 THEN ISNULL((SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0.0)  ELSE SUM(gl.cashbankglamt) END) ELSE SUM(gl.cashbankglamt) END amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND ISNULL(cb.cost_type,'')<>'KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rd5.rabmstoid AND ISNULL(gl.rabdtl5oid,0)=rd5.rabdtl5oid GROUP BY cb.cashbankoid, cb.cost_type) AS tb),0.0))*/ 0.0 rabdtl5amt, 0.0 cashbankglamt, 0 pphcreditacctgoid, 0.0 pphcreditamt, '' salesoid, rd5.rabdtl5note cashbankglnote, 0 prmstoid, 0 prdtloid, '' referensi FROM QL_trnrabdtl5 rd5 INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=rd5.rabmstoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rd5.rabmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=rd5.acctgoid WHERE rd5.cmpcode='{CompnyCode}' AND rm.rabmststatus='Approved' {(rabmstoid == 0 ? "" : $"AND rm.rabmstoid={rabmstoid}")}  {(!string.IsNullOrEmpty(filterperiodfrom) && !string.IsNullOrEmpty(filterperiodto) ? $"AND rm.rabdate >='{filterperiodfrom} 00:00:00' AND rm.rabdate <='{filterperiodto} 23:00:00'" : "")}").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    if (cost_type == "KASBON")
                    {
                        var sVar = "VAR_UM_OPRASIONAL_BANK";
                        if (cashbanktype == "BKK")
                            sVar = "VAR_UM_OPRASIONAL_KAS";
                        string acctgoid_um = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
                        string acctgcode_um = db.Database.SqlQuery<string>($"SELECT acctgcode FROm QL_mstacctg WHERE acctgoid in({acctgoid_um})").FirstOrDefault();
                        string acctgdesc_um = db.Database.SqlQuery<string>($"SELECT acctgdesc FROm QL_mstacctg WHERE acctgoid in({acctgoid_um})").FirstOrDefault();
                        int acctgoid_hb = int.Parse(ClassFunction.GetDataAcctgOid("VAR_HUTANG_BIAYA", CompnyCode));
                        foreach (var item in tbl)
                        {
                            item.acctgoid = int.Parse(acctgoid_um);
                            item.acctgcodereal = acctgcode_um;
                            item.acctgdescreal = acctgdesc_um;
                            item.pphcreditacctgoid = acctgoid_hb;
                        }
                    }
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetailsNonRAB(string sVar, string ddlpr, int cashbankoid)
        {
            JsonResult js = null;
            try
            {
                string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
                if (ddlpr == "pr")
                {
                    sSql = $"SELECT 0 cashbankglseq, 0 rabmstoid, '' projectname, '' soitemno, 0 rabdtl5oid, a.acctgoid, '' acctgcode, '' acctgdesc, a.acctgcode acctgcodereal, a.acctgdesc acctgdescreal, (prdtlamt - ISNULL((SELECT SUM(poitemqty * poitemprice) FROM QL_trnpoitemdtl x WHERE x.prdtloid=prd.prdtloid),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE x.prdtloid=prd.prdtloid AND cashbankoid<>{cashbankoid}),0.0)) rabdtl5amt, (prdtlamt - ISNULL((SELECT SUM(poitemqty * poitemprice) FROM QL_trnpoitemdtl x WHERE x.prdtloid=prd.prdtloid),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE x.prdtloid=prd.prdtloid AND cashbankoid<>{cashbankoid}),0.0)) cashbankglamt, 0 pphcreditacctgoid, 0.0 pphcreditamt, '' salesoid, prd.prdtlnote cashbankglnote, prd.prmstoid, prd.prdtloid, i.jasadesc referensi FROM QL_trnprmst prm INNER JOIN QL_trnprdtl prd ON prd.prmstoid=prm.prmstoid INNER JOIN QL_mstjasa i ON i.jasaoid=prrefoid INNER JOIN QL_mstacctg a ON a.acctgoid=i.acctgoid WHERE a.activeflag='ACTIVE' AND prm.prmststatus IN('Post','Approved') AND prm.prtype='Jasa' AND ISNULL(prd.prdtlstatus,'')='' AND a.acctgoid IN({acctgoid})";
                }
                else
                {
                    sSql = $"SELECT 0 cashbankglseq, 0 rabmstoid, '' projectname, '' soitemno, 0 rabdtl5oid, a.acctgoid, '' acctgcode, '' acctgdesc, a.acctgcode acctgcodereal, a.acctgdesc acctgdescreal, 0.0 rabdtl5amt, 0.0 cashbankglamt, 0 pphcreditacctgoid, 0.0 pphcreditamt, '' salesoid, '' cashbankglnote, 0 prmstoid, 0 prdtloid, '' referensi FROM QL_mstacctg a WHERE a.activeflag='ACTIVE' AND a.acctgoid IN({acctgoid})";
                }
                var tbl = db.Database.SqlQuery<listcashbankgl>(sSql).ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    if (sVar == "VAR_UM_OPRASIONAL_KAS" || sVar == "VAR_UM_OPRASIONAL_BANK" || sVar == "VAR_KASBON")
                    {
                        int acctgoid_hb = int.Parse(ClassFunction.GetDataAcctgOid("VAR_HUTANG_BIAYA", CompnyCode));
                        foreach (var item in tbl)
                        {
                            item.pphcreditacctgoid = acctgoid_hb;
                        }
                    }
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<listcashbankgl>();
            var dtl2 = new List<listapitemdtl2>();

            try
            {
                sSql = $"SELECT * FROM( SELECT cashbankgloid, cashbankglseq, cbgl.rabmstoid, projectname, soitemno, cbgl.rabdtl5oid, cbgl.acctgoid, ISNULL((SELECT ax.acctgcode FROM QL_mstacctg ax WHERE ax.acctgoid=rd5.acctgoid),'') acctgcode, ISNULL((SELECT ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=rd5.acctgoid),'') acctgdesc, a.acctgcode acctgcodereal, a.acctgdesc acctgdescreal, (rd5.rabdtl5price - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0) - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0)) rabdtl5amt, cbgl.cashbankglamt, cbgl.pphcreditacctgoid, cbgl.pphcreditamt, cbgl.salesoid, cbgl.cashbankglnote, ISNULL(cbgl.prmstoid,0) prmstoid, ISNULL(cbgl.prdtloid,0) prdtloid, '' referensi FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid INNER JOIN QL_trnrabdtl5 rd5 ON rd5.rabdtl5oid=cbgl.rabdtl5oid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=cbgl.rabmstoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=cbgl.rabmstoid WHERE cbgl.cashbankoid={id} AND cbgl.cmpcode='{CompnyCode}' AND ISNULL(cbgl.rabdtl5oid,0)<>0  UNION ALL  SELECT cashbankgloid, cashbankglseq, cbgl.rabmstoid, '' projectname, '' soitemno, cbgl.rabdtl5oid, cbgl.acctgoid, '' acctgcode, '' acctgdesc, a.acctgcode acctgcodereal, a.acctgdesc acctgdescreal, 0.0 rabdtl5amt, cbgl.cashbankglamt, cbgl.pphcreditacctgoid, cbgl.pphcreditamt, cbgl.salesoid, cbgl.cashbankglnote, ISNULL(cbgl.prmstoid,0) prmstoid, ISNULL(cbgl.prdtloid,0) prdtloid, ISNULL(i.jasadesc,'') referensi FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid LEFT JOIN QL_trnprdtl prd ON prd.prdtloid=ISNULL(cbgl.prdtloid,0) LEFT JOIN QL_trnprmst prm ON prm.prmstoid=ISNULL(cbgl.prmstoid,0) LEFT JOIN QL_mstjasa i ON i.jasaoid=prd.prrefoid WHERE cbgl.cashbankoid={id} AND cbgl.cmpcode='{CompnyCode}' AND ISNULL(cbgl.rabdtl5oid,0)=0 )AS dt ORDER BY dt.cashbankgloid";
                dtl = db.Database.SqlQuery<listcashbankgl>(sSql).ToList();

                sSql = $"SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='{CompnyCode}' AND rd.apitemmstoid={id} AND rd.apitemdtl2type='QL_trncashbankmst' ORDER BY rd.apitemdtl2seq";
                dtl2 = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl, dtl2 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listapitemdtl2 : QL_trnapitemdtl2
        {
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
        }

        [HttpPost]
        public ActionResult GetFakturData(int suppoid)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<listapitemdtl2>($"SELECT 0 AS apitemdtl2seq, fa.fabelioid, fa.fabelicode, fa.fakturno, (fa.fabeliqty-fa.fabeliaccumqty) fabeliaccumqty, 0.0 AS apitemdtl2amt FROM QL_mstfabeli fa WHERE fa.activeflag='ACTIVE' AND fa.suppoid={suppoid} AND (fabeliqty-fabeliaccumqty)>0 ORDER BY fa.fakturno").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {

                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        // GET/POST: aritem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Real" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cb.cashbankoid, cb.cashbankno, cb.cashbankdate, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, p.usname personname, CASE cb.cost_type WHEN 'KASBON REAL' THEN 'REALISASI KASBON' ELSE cb.cost_type END cost_type, cb.cashbankstatus, cb.cashbanknote, cashbankamt, CASE WHEN cb.cashbankstatus = 'Revised' THEN revisereason WHEN cb.cashbankstatus = 'Rejected' THEN rejectreason ELSE '' END reason FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_m01us p ON p.usoid=cb.personoid LEFT JOIN QL_mstsupp s ON s.suppoid=cb.refsuppoid WHERE cb.cashbankgroup='RABMULTIREAL' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.rabmstoid = 0;
                tbl.rabdtl5oid = 0;
                tbl.pphamt = 0;
                tbl.refsuppoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.upduser = Session["UserID"].ToString();

                ViewBag.cashbankdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                ViewBag.cashbankdate = tbl.cashbankdate.ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = tbl.cashbankduedate.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, List<listcashbankgl> dtDtl, List<listapitemdtl2> dtDtl2, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.cashbankno))
                tbl.cashbankno = "";
            if (tbl.cashbankstatus.ToUpper() == "REVISED")
                tbl.cashbankstatus = "In Process";

            string sErrReply = "";
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                msg += "- TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" + sErrReply + ") allowed stored in database!<br>";
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                msg += "- TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" + sErrReply + ") allowed stored in database!<br>";
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        msg += "- Please fill REF. NO. field!<br>";
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    msg += "- DUE DATE must be more or equal than Tgl Transaksi!<br>";
            }
            if (tbl.personoid.ToString() == "0")
                msg += "- Please select PIC field!<br>";

            if (dtDtl == null)
                msg += "- Please fill detail data!<br />";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br />";
            else
            {
                for (int i = 0; i < dtDtl.Count(); i++)
                {
                    var sPlus = "";
                    if (dtDtl[i].cashbankglamt == 0)
                        msg += "- AMOUNT must be not equal to 0!<br>";
                    else
                    {
                        if (!ClassFunction.isLengthAccepted("cashbankglamt", "QL_trncashbankgl", dtDtl[i].cashbankglamt, ref sErrReply))
                            msg += "- AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!<br>";
                    }
                    if (tbl.cost_type != "KASBON REAL")
                    {
                        if (tbl.cost_type == "KASBON")
                        {
                            if (dtDtl.Count != 1) msg += "- Tipe Kasbon, Detail UM tidak boleh lebih dari 1 data!<br>";
                        }
                        sPlus = " - ISNULL((SELECT SUM(amt) FROM (SELECT CASE ISNULL(cb.cost_type,'') WHEN 'KASBON' THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0)>0 THEN /*ISNULL((SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0.0)*/ 0.0 ELSE SUM(gl.cashbankglamt) END) ELSE SUM(gl.cashbankglamt) END amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND ISNULL(cb.cost_type,'')<>'KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rd5.rabmstoid AND ISNULL(gl.rabdtl5oid,0)=rd5.rabdtl5oid AND cb.cashbankoid<>" + tbl.cashbankoid + " GROUP BY cb.cashbankoid, cb.cost_type) AS tb),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND ISNULL(cb.cost_type,'')='KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rd5.rabmstoid AND ISNULL(gl.rabdtl5oid,0)=rd5.rabdtl5oid AND ISNULL(cb.rabmstoid,0)=0),0.0)";
                    }
                    //else
                    //{
                    //    dtDtl[i].rabmstoid = db.QL_trncashbankgl.FirstOrDefault(x => x.cashbankoid == tbl.kasbon_ref_id).rabmstoid;
                    //    dtDtl[i].rabdtl5oid = db.QL_trncashbankgl.FirstOrDefault(x => x.cashbankoid == tbl.kasbon_ref_id).rabdtl5oid;
                    //}
                    //cek amount biaya RAB
                    if (dtDtl[i].rabdtl5oid != 0)
                    {
                        var rabdtl5oid_dtl = dtDtl[i].rabdtl5oid; var rabmstoid_dtl = dtDtl[i].rabmstoid;
                        var no_so = db.QL_trnsoitemmst.FirstOrDefault(x => x.rabmstoid == rabmstoid_dtl).soitemno;
                        var a = dtDtl.Where(x => x.rabdtl5oid == rabdtl5oid_dtl && x.rabmstoid == rabmstoid_dtl).GroupBy(x => x.rabdtl5oid).Select(x => new { Oid = x.Key, Amt = x.Sum(y => y.cashbankglamt) }).ToList();
                        foreach (var z in a)
                        {
                            sSql = "SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabdtl5oid),0.0) " + sPlus + ") AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.rabdtl5oid ='" + z.Oid + "'";
                            dtDtl[i].rabdtl5amt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (z.Amt > dtDtl[i].rabdtl5amt)
                                msg += "- Amount Realisasi RAB project " + dtDtl[i].projectname + ", No SO " + no_so + " tidak boleh lebih dari Amount Budget!<br>";
                        }
                    }
                    //end cek amount biaya RAB

                    //cek PR
                    if (dtDtl[i].prdtloid != 0)
                    {
                        decimal AmtOS = db.Database.SqlQuery<decimal>($"SELECT (prdtlamt - ISNULL((SELECT SUM(poitemqty * poitemprice) FROM QL_trnpoitemdtl x WHERE x.prdtloid=prd.prdtloid),0.0) - ISNULL((SELECT SUM(x.cashbankglamt) FROM QL_trncashbankgl x WHERE x.prdtloid=prd.prdtloid AND x.cashbankoid<>{tbl.cashbankoid}),0.0)) amt FROM QL_trnprmst prm INNER JOIN QL_trnprdtl prd ON prd.prmstoid=prm.prmstoid WHERE prd.prdtloid={dtDtl[i].prdtloid}").FirstOrDefault();
                        dtDtl[i].rabdtl5amt = AmtOS;
                        if (dtDtl[i].cashbankglamt > dtDtl[i].rabdtl5amt)
                        {
                            msg += "Amount " + dtDtl[i].referensi + " Tidak Boleh Melebihi Amount PR!!<br />";
                        }
                    }

                    if (dtDtl[i].pphcreditamt > 0 && dtDtl[i].pphcreditacctgoid == 0)
                    {
                        msg += "- Silahkan Pilih COA PPH!<br>";
                    }
                }
            }

            if (tbl.cost_type == "KASBON REAL")
            {
                sSql = $"SELECT (cb.cashbankdpp - ISNULL((SELECT SUM(x.kasbonrefamt) FROM QL_trncashbankmst x WHERE x.cmpcode=cb.cmpcode AND x.cashbankgroup='RABMULTIREAL' AND x.cost_type='KASBON REAL' AND ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid AND x.cashbankoid<>{tbl.cashbankoid}),0.0)) FROM QL_trncashbankmst cb WHERE cb.cmpcode='{CompnyCode}' AND cb.cashbankgroup='RABMULTIREAL' AND cb.cost_type='KASBON' AND cb.cashbankoid = {tbl.kasbon_ref_id}";
                var OSKasbon = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                if (tbl.kasbonrefamt > OSKasbon)
                {
                    msg += "Amount Kasbon Tidak Boleh Melebihi Sisa Kasbon Sebesar " + OSKasbon + " !!<br />";
                }

                sSql = $"SELECT ISNULL((SELECT SUM(x.pphcreditamt) FROM QL_trncashbankgl x WHERE x.cashbankoid=cb.cashbankoid),0.0) - ISNULL((SELECT SUM(x.kasbonrefamt2) FROM QL_trncashbankmst x WHERE x.cmpcode=cb.cmpcode AND x.cashbankgroup='RABMULTIREAL' AND x.cost_type='KASBON REAL' AND ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid AND x.cashbankoid<>{tbl.cashbankoid}),0.0)kasbonamt2 FROM QL_trncashbankmst cb WHERE cb.cmpcode='{CompnyCode}' AND cb.cashbankgroup='RABMULTIREAL' AND cb.cost_type='KASBON' AND cb.cashbankoid = {tbl.kasbon_ref_id}";
                var OSKasbon2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                if (tbl.kasbonrefamt2 > OSKasbon2)
                {
                    msg += "Hutang Biaya Kasbon Tidak Boleh Melebihi Sisa Hutang Biaya Kasbon Sebesar " + OSKasbon + " !!<br />";
                }
            }

            string apitemdtl2type = "QL_trncashbankmst";
            string fakturnoGL = ""; decimal totaltaxhdr = 0;
            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                        fakturnoGL += dtDtl2[i].fakturno + ",";
                    }
                    fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                }
            }

            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK" || tbl.cashbanktype == "RBB")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = tbl.cashbankduedate;
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = tbl.cashbankduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            //if (tbl.cashbankstatus == "Post")
            //{
            cRate.SetRateValue(tbl.curroid, sDueDate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateMonthlyLastError != "")
            {
                msg += cRate.GetRateMonthlyLastError;
            }
            else
            {
                rate2oid = cRate.GetRateMonthlyOid;
                rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
            }
            //}

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.cashbankstatus == "In Approval")
            {
                if (stoid_app <= 0)
                    msg += "- Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            if (tbl.cashbankstatus == "Post" || tbl.cashbankstatus == "In Approval")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }

            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateExpenseNo2(tbl.cmpcode, tbl.cashbankdate.ToString("dd/MM/yyyy"), tbl.cashbanktype, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var dtl2oid = ClassFunction.GenerateID("QL_trnapitemdtl2");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbankgroup = "RABMULTIREAL";
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.refsuppoid).FirstOrDefault();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (tbl.cost_type != "KASBON REAL")
                            {
                                sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(SELECT ISNULL(rabdtl5oid,0) FROM QL_trncashbankgl WHERE cashbankoid=" + tbl.cashbankoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            //update PR
                            sSql = "UPDATE QL_trnprdtl SET prdtlstatus='' WHERE prdtloid IN (SELECT ISNULL(prdtloid,0) FROM QL_trncashbankgl WHERE cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnprmst SET prmststatus='Post' WHERE prmstoid IN (SELECT DISTINCT ISNULL(prmstoid,0) FROM QL_trncashbankgl WHERE cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tbl.cashbankoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();

                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trncashbankgl)ClassFunction.MappingTable(new QL_trncashbankgl(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.cashbankglamtusd = 0;
                            tbldtl.cashbankglduedate = tbl.cashbankduedate;
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.groupoid = 0;
                            tbldtl.cashbankglgiroflag = "";

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.cost_type != "KASBON REAL")
                            {
                                if (dtDtl[i].rabdtl5oid != 0)
                                {
                                    var a = dtDtl.Where(x => x.rabdtl5oid == dtDtl[i].rabdtl5oid && x.rabmstoid == dtDtl[i].rabmstoid).GroupBy(x => x.rabdtl5oid).Select(x => new { Oid = x.Key, Amt = x.Sum(y => y.cashbankglamt) }).ToList();
                                    foreach (var z in a)
                                    {
                                        if (z.Amt >= dtDtl[i].rabdtl5amt)
                                        {
                                            sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='Complete' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(" + z.Oid + ")";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }

                            //update PR
                            if (dtDtl[i].prdtloid != 0)
                            {
                                if (dtDtl[i].cashbankglamt >= dtDtl[i].rabdtl5amt)
                                {
                                    sSql = "UPDATE QL_trnprdtl SET prdtlstatus='Complete' WHERE prdtloid=" + dtDtl[i].prdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnprmst SET prmststatus='Closed' WHERE prmstoid=" + dtDtl[i].prmstoid + " AND (SELECT COUNT(prdtloid) FROM QL_trnprdtl WHERE prdtlstatus='' AND prmstoid=" + dtDtl[i].prmstoid + " AND prdtloid<>" + +dtDtl[i].prdtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnapitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = (QL_trnapitemdtl2)ClassFunction.MappingTable(new QL_trnapitemdtl2(), dtDtl2[i]);
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.apitemdtl2oid = dtl2oid++;
                                    tbldtl2.apitemmstoid = tbl.cashbankoid;
                                    tbldtl2.apitemdtl2seq = i + 1;
                                    tbldtl2.createuser = tbl.upduser;
                                    tbldtl2.createtime = tbl.updtime;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.apitemdtl2type = apitemdtl2type;

                                    db.QL_trnapitemdtl2.Add(tbldtl2);
                                    db.SaveChanges();

                                    if (tbl.cashbankstatus == "Post")
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }

                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnapitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.cashbankstatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.cashbankoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnrabrealmulti";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.cashbankoid.ToString();
                        sReturnNo = "No. " + tbl.cashbankno;
                        if (tbl.cashbankstatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl.cost_type != "KASBON REAL")
                        {
                            sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(SELECT ISNULL(rabdtl5oid,0) FROM QL_trncashbankgl WHERE cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //update PR
                        sSql = "UPDATE QL_trnprdtl SET prdtlstatus='' WHERE prdtloid IN (SELECT ISNULL(prdtloid,0) FROM QL_trncashbankgl WHERE cashbankoid=" + tbl.cashbankoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnprmst SET prmststatus='Post' WHERE prmstoid IN (SELECT DISTINCT ISNULL(prmstoid,0) FROM QL_trncashbankgl WHERE cashbankoid=" + tbl.cashbankoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == CompnyCode);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, Boolean cbprintbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptRABCostRealMulti.rpt"));
            sSql = "SELECT cb.cashbankoid, currcode AS [Currency], currsymbol AS [Curr Symbol], currdesc AS [Curr Desc], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ISNULL(cb.rabmstoid,0)),'') AS [Project], (CASE cashbanktype WHEN 'BKK' THEN 'Kas' WHEN 'BBK' THEN 'Transfer' WHEN 'BGK' THEN 'Giro/Cheque' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE '' END) AS [Payment Type], ('(' + a1.acctgcode + ') ' + a1.acctgdesc) AS [Payment Account], cashbankrefno AS [Ref. No.], cashbankduedate AS [Due Date], cashbanktakegiro AS [Date Take Giro], ISNULL(suppname, 'CASH') AS [Supplier], ISNULL(suppcode, '') AS [Supp. Code], ISNULL(suppaddr, '') AS [Supp. Address], ISNULL(g1.gndesc, '') AS [Supp. City], ISNULL(g2.gndesc, '') AS [Supp. Province], ISNULL(g3.gndesc, '') AS [Supp. Country], ISNULL(suppemail, '') AS [Supp. Email], ISNULL(suppphone1, '') AS [Supp. Phone 1], ISNULL(suppphone2, '') AS [Supp. Phone 2], '' AS [Supp. Phone 3], ISNULL(suppfax1, '') AS [Supp. Fax 1], '' AS [Supp. Fax 2], cashbankdpp AS [Total Amount], cashbanktaxtype AS [Tax Type], CONVERT(VARCHAR(10), cashbanktaxpct) AS [Tax Pct], cashbanktaxamt AS [Tax Amount], cashbankothertaxamt AS [Other Tax Amount], cashbankamt AS [Grand Total], p.usname AS [PIC], cashbankstatus AS [Status], cashbanknote AS [Header Note], a2.acctgcode AS [COA No.], a2.acctgdesc AS [COA Desc.], cashbankglamt AS [Amount], cashbankglnote AS [Note], divname AS [Business Unit], divaddress AS [BU Address], g4.gndesc AS [BU City], g5.gndesc AS [BU Province], g6.gndesc AS [BU Country], ISNULL(divphone, '') AS [BU Phone 1], ISNULL(divphone2, '') AS [BU Phone 2], divemail AS [BU Email], cb.upduser AS [Last Upd. User ID], ISNULL(pr.usname,UPPER(cb.upduser)) AS [Last Upd. User Name], cb.updtime AS [Last Upd. Datetime], ISNULL((SELECT x.usname FROM QL_m01US x WHERE x.usoid=cb.createuser),'') [CreateName], ISNULL((select TOP 1 x.usname from QL_APP ap INNER JOIN QL_m08AS st On st.stoid_app=ap.appstoid INNER JOIN QL_m01US x On x.usoid=ap.appuser WHERE ap.tablename='QL_trnrabrealmulti' AND ap.appformoid=cb.cashbankoid AND ISNULL(st.asflag,'')<>'Final'),'-') [Approved1], ISNULL((select TOP 1 x.usname from QL_APP ap INNER JOIN QL_m08AS st On st.stoid_app=ap.appstoid INNER JOIN QL_m01US x On x.usoid=ap.appuser WHERE ap.tablename='QL_trnrabrealmulti' AND ap.appformoid=cb.cashbankoid AND ISNULL(st.asflag,'')='Final'),'-') [Approved2], cashbankno AS[Cash / Bank No.], cashbankdate AS[Expense Date], ISNULL(rm.projectname,'') projectname, ISNULL(rm.rabno,'') rabno, ISNULL(som.soitemno,'') soitemno, ISNULL((SELECT ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=ISNULL(gl.pphcreditacctgoid,0)),'') pphcredit, ISNULL(gl.pphcreditamt,0.0) pphamt, isnull(cb.kasbonrefamt,0.0) kasbonrefamt, isnull(cb.kasbonrefamt2,0.0) kasbonrefamt2, ISNULL(cb.cost_type,'') cost_type FROM QL_trncashbankmst cb INNER JOIN QL_mstcurr c ON c.curroid=cb.curroid INNER JOIN QL_mstacctg a1 ON a1.acctgoid=cb.acctgoid INNER JOIN QL_m01US p ON p.usoid=cb.personoid INNER JOIN QL_trncashbankgl gl ON gl.cmpcode=cb.cmpcode AND gl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=gl.acctgoid INNER JOIN QL_mstdivision di ON di.cmpcode=cb.cmpcode LEFT JOIN QL_mstsupp s ON s.suppoid=cb.refsuppoid LEFT JOIN QL_m05GN g1 ON g1.cmpcode=s.cmpcode AND g1.gnoid=suppcityOid LEFT JOIN QL_m05GN g2 ON g2.cmpcode=g1.cmpcode AND g2.gnoid=CONVERT(INT, g1.gnother1) LEFT JOIN QL_m05GN g3 ON g3.cmpcode=g1.cmpcode AND g3.gnoid=CONVERT(INT, g1.gnother2) LEFT JOIN QL_m05GN g4 ON g4.gnoid=divcityoid INNER JOIN QL_m05GN g5 ON g5.cmpcode=g4.cmpcode AND g5.gnoid=CONVERT(INT, g4.gnother1) LEFT JOIN QL_m05GN g6 ON g6.cmpcode=g4.cmpcode AND g6.gnoid=CONVERT(INT, g4.gnother2) LEFT JOIN QL_m01US pr ON pr.usoid=cb.upduser LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid=gl.rabmstoid LEFT JOIN QL_trnsoitemmst som ON som.rabmstoid=gl.rabmstoid WHERE cashbankgroup='RABMULTIREAL' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid IN (" + id + ") ORDER BY cb.cashbankoid, gl.cashbankgloid";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, "rptPrintCashBank");

            report.SetDataSource(dtRpt);
            report.SetParameterValue("UserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "RABCostRealMultiPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}