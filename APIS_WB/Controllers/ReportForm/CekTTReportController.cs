﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class CekTTReportController : Controller
    {
        // GET: CekTTReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public CekTTReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetDataChecklist(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "Select DISTINCT 0 seq, cm.checkttmstoid [No. Draft], Case When cm.checkttno='' then CONVERT(varchar(10), cm.checkttmstoid) Else cm.checkttno End [No. Transaksi], Convert(Char(20), cm.checkttdate,103) Tanggal, c.suppname Supplier, cm.checkttmststatus [Status] FROM QL_trncheckttmst cm INNER JOIN QL_mstsupp c ON c.suppoid=cm.suppoid Where cm.cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND cm.checkttmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY cm.checkttmstoid";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_trncheckttmst");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextChecklist)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "SELECT DISTINCT 0 seq, suppcode [Kode], suppname Supplier, suppaddr Alamat FROM QL_mstsupp s INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=s.cmpcode AND pom.suppoid=s.suppoid WHERE s.cmpcode='" + CompnyCode + "' AND s.supppengakuan = 'TT' AND pom.poitemmstoid NOT IN (SELECT DISTINCT apitemmstoid FROM QL_trncheckttdtl apd INNER JOIN QL_trncheckttmst apm ON apm.cmpcode=apd.cmpcode AND apm.checkttmstoid=apd.checkttmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND checkttmststatus<>'Cancel') AND s.suppoid IN (Select suppoid from QL_trncheckttmst cm Where cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND checkttmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextChecklist != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND checkttmstoid IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += ")";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_suppplier");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        //, TextChecklist: TextChecklist, TextCust: TextCust, TextNomorRAB: TextNomorRAB
        [HttpPost]
        public ActionResult GetCustData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextChecklist, string TextCust, string TextNomorRAB)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "SELECT DISTINCT 0 seq, [ID Sales], Sales, Telp, Jabatan FROM (SELECT cmpcode, suppoid, suppdtl2oid [ID Sales], suppdtl2picname Sales, suppdtl2phone1 [Telp], suppdtl2email Email, suppdtl2jabatan Jabatan FROM QL_mstsuppdtl2 UNION ALL SELECT cmpcode, suppoid,suppdtl3oid [ID Sales], suppdtl3picname Sales, suppdtl3phone1 [Telp], suppdtl3email Email, suppdtl3jabatan Jabatan FROM QL_mstsuppdtl3) gg Where gg.cmpcode='" + CompnyCode + "' AND suppoid IN (Select cm.suppoid from QL_trncheckttmst cm INNER JOIN QL_mstsupp s ON s.suppoid=cm.suppoid Where cm.cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }                

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND checkttmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }               

                if (TextChecklist != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND checkttmstoid IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomorRAB != "" )
                {
                    string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                sSql += ") Order By Sales";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_suppplier");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextChecklist, string TextNomorRAB)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "SELECT DISTINCT 0seq, pom.poitemmstoid [Draft], pom.poitemno [No. PO], CONVERT(varchar(20), poitemdate, 103) Tanggal FROM QL_trnpoitemmst pom INNER JOIN QL_trncheckttdtl cd ON cd.poitemmstoid= pom.poitemmstoid INNER JOIN QL_trncheckttmst cm ON cm.checkttmstoid = cd.checkttmstoid INNER JOIN QL_mstsupp s ON s.suppoid=cm.suppoid Where cd.cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND cm.checkttmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextChecklist != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND cm.checkttmstoid IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                sSql += " Order By pom.poitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_trnpoitemmst");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }
        //DDLStatus: , StartPeriod: StartPeriod, EndPeriod: EndPeriod, reporttype: reporttype, TextChecklist: TextChecklist, TextCust: TextCust, TextSO: TextSO, TextNomorRAB: TextNomorRAB, DDLType: DDLType
        [HttpPost]
        public ActionResult PrintReport(string[] DDLStatus, string StartPeriod, string EndPeriod, string reporttype, string TextChecklist, string TextCust, string TextSO, string TextNomorRAB, string DDLType, string DDLPeriod)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "rptCheckTTS.rpt";
            var rptname = "CHECK_TANDA_TERIMA";

            sSql = "SELECT cm.checkttmstoid, cm.checkttno, cm.checkttdate, c.suppcode, c.suppname, cm.checkttgrandtotal grandtotal, cm.checkttnote, cm.checkttmststatus, cm.updtime, cd.checkttdtlseq, cd.apitemmstoid, cd.apitemno, cd.checkttdtlamt poitemgrandtotal, cd.checkttdtllengkap checkttlengkap, cd.checkttdtlfaktur checkttfaktur, cd.checkttdtlkwitansi checkttkwitansi, cd.checkttdtlsj checkttsj, cd.poitemmstoid, CASE WHEN cd.potype='QL_trnpoitemmst' THEN (SELECT pom.poitemno FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid=cd.poitemmstoid) ELSE (SELECT pom.poassetno FROM QL_trnpoassetmst pom WHERE pom.poassetmstoid=cd.poitemmstoid) END poitemno, cd.soitemmstoid, CASE WHEN cd.potype='QL_trnpoitemmst' THEN ISNULL((SELECT pom.soitemno FROM QL_trnsoitemmst pom WHERE pom.soitemmstoid=cd.soitemmstoid),'') ELSE ISNULL((SELECT pom.soassetno FROM QL_trnsoassetmst pom WHERE pom.soassetmstoid=cd.soitemmstoid),'') END soitemno, cd.rabmstoid, CASE WHEN cd.potype='QL_trnpoitemmst' THEN ISNULL((SELECT pom.projectname FROM QL_trnrabmst pom WHERE pom.rabmstoid=cd.rabmstoid),'') ELSE'' END rabno, CASE WHEN cd.potype='QL_trnpoitemmst' THEN ISNULL((SELECT pom.mritemno FROM QL_trnmritemmst pom WHERE pom.mritemmstoid=cd.mrmstoid),'') ELSE ISNULL((SELECT pom.mrassetno FROM QL_trnmrassetmst pom WHERE pom.mrassetmstoid=cd.mrmstoid),'') END mrno, cd.checkttdtlnote, sl.Sales FROM QL_trncheckttdtl cd INNER JOIN QL_trncheckttmst cm ON cm.checkttmstoid=cd.checkttmstoid INNER JOIN QL_mstsupp c ON c.suppoid=cm.suppoid INNER JOIN (SELECT cmpcode, suppoid, suppdtl2oid[ID Sales], suppdtl2picname Sales, suppdtl2phone1[Telp], suppdtl2email Email, suppdtl2jabatan Jabatan FROM QL_mstsuppdtl2 UNION ALL SELECT cmpcode, suppoid,suppdtl3oid[ID Sales], suppdtl3picname Sales, suppdtl3phone1[Telp], suppdtl3email Email, suppdtl3jabatan Jabatan FROM QL_mstsuppdtl3) sl ON sl.suppoid = cm.suppoid WHERE cm.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND cm.checkttmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND sl.Sales IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextChecklist != "")
            {
                string[] arr = TextChecklist.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND cm.checkttmstoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextSO != "")
            {
                string[] arr = TextSO.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND CASE WHEN cd.potype='QL_trnpoitemmst' THEN (SELECT pom.poitemno FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid=cd.poitemmstoid) ELSE (SELECT pom.poassetno FROM QL_trnpoassetmst pom WHERE pom.poassetmstoid=cd.poitemmstoid) END IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }


            if (TextNomorRAB != "")
            {
                string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += " Order By cm.checkttno";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }

            //DDLStatus: DDLStatus, StartPeriod: StartPeriod, EndPeriod: EndPeriod, reporttype: reporttype, TextChecklist: TextChecklist, TextCust: TextCust, TextSO: TextSO, TextNomorRAB: TextNomorRAB, DDLType: DDLType
            //public ActionResult Index()
            //{
            //    return View();
            }
        }
    }