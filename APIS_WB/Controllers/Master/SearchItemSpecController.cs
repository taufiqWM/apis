﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Master
{
    public class SearchItemSpecController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public SearchItemSpecController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetDataListItemSpec()
        {
            var msg = "";
            sSql = "SELECT i.itemoid [ID], itemcode [Kode], itemdesc [Nama Barang], i.itemspec [Specification], ISNULL((SELECT SUM(c.qtyin - c.qtyout) FROM QL_conmat c WHERE c.refoid=i.itemoid),0.0) [DEC_Free Stock], ISNULL((SELECT TOP 1 harga FROM( SELECT pom.poitemdate tanggal, pod.poitemprice harga, pod.itemoid refoid FROM QL_trnpoitemmst pom INNER JOIN QL_trnpoitemdtl pod ON pod.poitemmstoid=pom.poitemmstoid WHERE pom.poitemmststatus IN('Approved','Post','Closed') AND ISNULL(pom.poitemmstres1,'')='' UNION ALL SELECT pom.poassetdate tanggal, pod.poassetprice harga, pod.poassetrefoid refoid FROM QL_trnpoassetmst pom INNER JOIN QL_trnpoassetdtl pod ON pod.poassetmstoid=pom.poassetmstoid WHERE pom.poassetmststatus IN('Approved','Post','Closed') AND ISNULL(pom.poassetmstres1,'')='') AS t WHERE t.refoid=i.itemoid ORDER BY t.tanggal DESC),0.0) [DEC_Harga Beli Terakhir] FROM QL_mstitem i WHERE ISNULL(i.itemspec,'')<>'' ";

            DataTable tbl = new ClassConnection().GetDataTable(sSql, "ListNotifPO");

            if (tbl.Rows.Count > 0)
            {
                List<string> colname = new List<string>();

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in tbl.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in tbl.Columns)
                    {
                        var item = dr[col].ToString();
                        decimal decval = 0;
                        try
                        {
                            if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                            {
                                if (decval == 0)
                                    item = "0.00";
                                else
                                    item = decval.ToString("#,##0.00");
                            }
                        }
                        catch
                        {
                            item = dr[col].ToString();
                        }
                        row.Add(col.ColumnName.Replace("DEC_", ""), item);
                        if (!colname.Contains(col.ColumnName.Replace("DEC_", "")))
                            colname.Add(col.ColumnName.Replace("DEC_", ""));
                    }
                    rows.Add(row);
                }

                return Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
            }


            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            return View();
        }
    }
}