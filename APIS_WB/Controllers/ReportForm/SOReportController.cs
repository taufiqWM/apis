﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using static APIS_WB.Controllers.ClassFunction; 
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SOReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public SOReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class listprab
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabnote { get; set; }
        }

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; } 
        }

        public ActionResult GetSalesData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string[] DDLKategori)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, s.usoid [Username], s.usname [Nama], g.gndesc [Jabatan], s.usaddress [Alamat] FROM QL_m01US s INNER JOIN QL_trnsoitemmst som ON som.salesoid=s.usoid INNER JOIN QL_trnrabmst r ON r.rabmstoid = som.rabmstoid INNER JOIN QL_m05GN g ON gnoid=s.Jabatanoid AND gngroup='JABATAN' Where som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++) stsval += "'" + DDLStatus[i] + "',";
                        if (!string.IsNullOrEmpty(stsval)) sSql += " AND som.soitemmststatus IN (" + Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (DDLKategori != null)
                {
                    if (DDLKategori.Count() > 0)
                    {
                        string katval = "";
                        for (int i = 0; i < DDLKategori.Count(); i++) katval += "'" + DDLKategori[i] + "',";
                        sSql += " AND r.rabtype2 IN (" + Left(katval, katval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY s.usname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSales");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate, string TextSales, string[] DDLKategori)
        {
            sSql = "SELECT 0 seq, som.soitemmstoid, som.soitemno, soitemdate, c.custname FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst r ON r.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "' AND som.soitemtype<>'Jasa'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + Left(stsval, stsval.Length - 1) + ")";
                }
            } 

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (DDLKategori != null)
            {
                if (DDLKategori.Count() > 0)
                {
                    string katval = "";
                    for (int i = 0; i < DDLKategori.Count(); i++) katval += "'" + DDLKategori[i] + "',";
                    sSql += " AND r.rabtype2 IN (" + Left(katval, katval.Length - 1) + ")";
                }
            }

            sSql += "ORDER BY som.soitemno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate, string TextSales, string[] DDLKategori)
        {
            sSql = "SELECT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst r ON r.rabmstoid = som.rabmstoid WHERE som.cmpcode='" + CompnyCode + "' AND som.soitemtype<>'Jasa'";  
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (DDLKategori != null)
            {
                if (DDLKategori.Count() > 0)
                {
                    string katval = "";
                    for (int i = 0; i < DDLKategori.Count(); i++) katval += "'" + DDLKategori[i] + "',";
                    sSql += " AND r.rabtype2 IN (" + Left(katval, katval.Length - 1) + ")";
                }
            }

            sSql += ") ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] status, string custcode, string startdate, string enddate, string fdate, string TextSales, string DDLKategori)
        {
            sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "' AND som.soitemtype<>'Jasa' ";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (DDLKategori != null)
            {
                if (DDLKategori.Count() > 0)
                {
                    string katval = "";
                    for (int i = 0; i < DDLKategori.Count(); i++) katval += "'" + DDLKategori[i] + "',";
                    sSql += " AND rm.rabtype2 IN (" + Left(katval, katval.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY rm.rabno";

            List<listprab> tbl = new List<listprab>();
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string prabno, string startdate, string enddate, string fdate, string TextSales, string[] DDLKategori)
        {
            sSql = "SELECT DISTINCT m.itemcode, m.itemdesc, m.itemtype FROM QL_mstitem m INNER JOIN QL_trnrabdtl rd ON rd.itemoid=m.itemoid INNER JOIN QL_trnrabmst rm ON rm.cmpcode=rd.cmpcode AND rm.rabmstoid=rd.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabunitoid INNER JOIN QL_trnsoitemmst som ON rm.rabmstoid=som.rabmstoid WHERE rd.cmpcode='" + CompnyCode +"'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (prabno != "")
            {
                string[] arr = prabno.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND rm.rabno IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (DDLKategori != null)
            {
                if (DDLKategori.Count() > 0)
                {
                    string katval = "";
                    for (int i = 0; i < DDLKategori.Count(); i++) katval += "'" + DDLKategori[i] + "',";
                    sSql += " AND rm.rabtype2 IN (" + Left(katval, katval.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        #region Report SO
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (checkPagePermission("SOReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string soitemno, string TextSales, string[] DDLKategori)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptSOSumPdf.rpt";
                rptname = "SO_SUMMARY";
            }
            else if (DDLType == "Summary Status Amount")
            {
                if (reporttype == "XLS") rptfile = "rptSOStatusAmtSumXls.rpt";
                else rptfile = "rptSOStatusAmtSumPdf.rpt";
                rptname = "SOJUAL_SUMMARY";
            } 
            else
            {
                rptfile = "rptSODtlPdf.rpt";
                rptname = "SO_DETAIL";
            }

            if (DDLType == "Summary Status Amount")
            {
                sSql = "SELECT * FROM ( SELECT som.cmpcode [cmpcode], som.soitemmstoid, som.soitemno, som.soitemdate , som.updtime [Upd Date], CASE WHEN ISNULL(som.soitemmstres1,'')='' THEN som.soitemmststatus ELSE ISNULL(som.soitemmstres1,'') END [Status], som.rabmstoid, rm.rabno [No. RAB], rm.projectname, rm.rabdate, rm.rabtype, rm.rabtype2, rm.salesoid, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.salesoid),'') sales, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.salesadminoid),'') salesadmin, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.creatoroid),'') creator, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT arm.aritemfaktur + '.' + us.fakturno refno FROM QL_trnaritemmst arm INNER JOIN QL_mstfa us ON us.faoid = arm.faoid WHERE arm.cmpcode = som.cmpcode AND arm.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') fakturno, c.custcode[Custcode], c.custname, CASE WHEN ISNULL(som.soitemmstres1, '') = '' THEN som.soitemgrandtotalamt ELSE 0.0 END soitemgrandtotalamt, CASE WHEN ISNULL(som.soitemmstres1, '') = '' THEN som.soitemtotalamt ELSE 0.0 END soitemtotalamt, CASE WHEN ISNULL(som.soitemmstres1, '')= '' THEN som.soitemtotaltaxamt ELSE 0.0 END soitemtotaltaxamt, 0 [armstoid], ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT arm.aritemno refno FROM QL_trnaritemmst arm WHERE arm.cmpcode = som.cmpcode AND arm.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [AR No.], ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(arm.aritemdate,'dd/MM/yyyy') refno FROM QL_trnaritemmst arm WHERE arm.cmpcode=som.cmpcode AND arm.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') aritemdate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(arm.aritemdate,'dd/MM/yyyy') refno FROM QL_trnaritemmst arm WHERE arm.cmpcode=som.cmpcode AND arm.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') ardate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(arm.updtime,'dd/MM/yyyy') refno FROM QL_trnaritemmst arm WHERE arm.cmpcode=som.cmpcode AND arm.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') arupdtime, CASE WHEN ISNULL(som.soitemmstres1,'')='' THEN ISNULL((SELECT SUM(arm.aritemgrandtotal) FROM QL_trnaritemmst arm WHERE arm.cmpcode=som.cmpcode AND arm.somstoid = som.soitemmstoid),0.0) ELSE 0.0 END aritemgrandtotal, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT arm.shipmentitemno refno FROM QL_trnshipmentitemmst arm WHERE arm.cmpcode = som.cmpcode AND arm.soitemmstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') shipmentitemno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(arm.shipmentitemdate,'dd/MM/yyyy') refno FROM QL_trnshipmentitemmst arm WHERE arm.cmpcode=som.cmpcode AND arm.soitemmstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') shipmentitemdate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.cashbankno refno FROM QL_trnaritemmst apx INNER JOIN QL_trnpayar pyx ON pyx.refoid = apx.aritemmstoid AND pyx.reftype = 'QL_trnaritemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.cmpcode = som.cmpcode AND apx.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(cbx.cashbankdate,'dd/MM/yyyy') refno FROM QL_trnaritemmst apx INNER JOIN QL_trnpayar pyx ON pyx.refoid = apx.aritemmstoid AND pyx.reftype = 'QL_trnaritemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.cmpcode = som.cmpcode AND apx.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankdate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10),ard2.aritemdtl2date,103) refno FROM QL_trnaritemdtl2 ard2 INNER JOIN QL_trnaritemmst arm ON ard2.cmpcode=arm.cmpcode AND ard2.aritemmstoid= arm.aritemmstoid WHERE arm.cmpcode= som.cmpcode AND arm.somstoid= som.soitemmstoid AND ard2.aritemdtl2type= 'PPH 22') ft FOR XML PATH('')), 1, 1, ''),'') tglpph, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT ard2.ntpnno refno FROM QL_trnaritemdtl2 ard2 INNER JOIN QL_trnaritemmst arm ON ard2.cmpcode= arm.cmpcode AND ard2.aritemmstoid= arm.aritemmstoid WHERE arm.cmpcode= som.cmpcode AND arm.somstoid= som.soitemmstoid AND ard2.aritemdtl2type= 'PPH 22') ft FOR XML PATH('')), 1, 1, ''),'') ntpnno, ISNULL((SELECT SUM(ard2.aritemdtl2amt) FROM QL_trnaritemdtl2 ard2 INNER JOIN QL_trnaritemmst arm ON ard2.cmpcode=arm.cmpcode AND ard2.aritemmstoid=arm.aritemmstoid WHERE arm.cmpcode=som.cmpcode AND arm.somstoid=som.soitemmstoid AND ard2.aritemdtl2type='PPH 22'),0.0) pphamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT apx.dparno refno FROM QL_trndpar apx WHERE apx.cmpcode = som.cmpcode AND apx.somstoid = som.soitemmstoid AND apx.soreftype= 'QL_trnsoitemmst' AND apx.dparpaytype<>'DPFK') ft FOR XML PATH('')), 1, 1, ''),'') dpno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(apx.dpardate,'dd/MM/yyyy') refno FROM QL_trndpar apx WHERE apx.cmpcode = som.cmpcode AND apx.somstoid = som.soitemmstoid AND apx.soreftype= 'QL_trnsoitemmst' AND apx.dparpaytype<>'DPFK') ft FOR XML PATH('')), 1, 1, ''),'') dpdate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.cashbankno refno FROM QL_trnaritemmst apx INNER JOIN QL_trnpaydp pyx ON pyx.refoid = apx.aritemmstoid AND pyx.reftype = 'QL_trnaritemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.cmpcode = som.cmpcode AND apx.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankdpno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT FORMAT(cbx.cashbankdate,'dd/MM/yyyy') refno FROM QL_trnaritemmst apx INNER JOIN QL_trnpaydp pyx ON pyx.refoid = apx.aritemmstoid AND pyx.reftype = 'QL_trnaritemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.cmpcode = som.cmpcode AND apx.somstoid = som.soitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankdpdate FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.cmpcode=som.cmpcode AND rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = som.custoid WHERE som.cmpcode='" + CompnyCode + "' ) AS ar WHERE [cmpcode]='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var sFilter = "ar.soitemdate";
                    if (DDLPeriod == "som.updtime")
                        sFilter = "[Upd Date]";
                    sSql += " AND " + sFilter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + sFilter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                            stsval += "'" + DDLStatus[i] + "',";
                        sSql += " AND [Status] IN (" + Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (soitemno != "")
                {
                    string[] arr = soitemno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                        filterdata += "'" + arr[i] + "',";
                    sSql += " AND ar.soitemno IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                        filterdata += "'" + arr[i] + "',";
                    sSql += " AND [Custcode] IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND ar.[No. RAB] IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND ar.salesoid IN (" + Left(datafilter, datafilter.Length - 1) + ")";
                }                
            } 
            else
            {
                var Dtl = "";
                var Join = "";
                if (DDLType == "Detail")
                {
                    Dtl = ",sod.soitemdtletd [ETD DTL], soitemdtlseq [No.], itemcode [Code], m.itemdesc [Material], itemnote [SKU], soitemqty [Qty], g2.gndesc [Unit], soitemprice [Price], soitemdtlamt [Detail Amount], soitemdtldisctype [Disc Dtl Type], soitemdtldiscvalue [Disc Dtl Value], soitemdtldiscamt [Disc Dtl Amt], soitemdtlnetto [Detail Netto], soitemdtlnote [Detail Note], 0.00 [Pack Volume FG]";
                    Join = " INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode=som.cmpcode AND sod.soitemmstoid=som.soitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = sod.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid = soitemunitoid";
                }
                sSql = "SELECT '" + CompnyName + "' AS [Business Unit], som.cmpcode [CMPCODE], CONVERT(VARCHAR(20), som.soitemmstoid) [Draft No.], som.soitemmstoid [ID], soitemtype [Type], soitemdate [Date], GETDATE() [ETD], soitemno [SO No.], custname [Customer], custcode [Customer Code], som.soitemcustpo [Customer Ref.], som.soitemcustpodate [Cust Ref Date], ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.salesoid),'') sales, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.salesadminoid),'') salesadmin, currcode [Currency], g1.gndesc [Payment Type], 0.00 [Daily Rate To IDR], '' [Daily Rate To USD], rate2idrvalue [Monthly Rate To IDR], rate2usdvalue [Monthly Rate To USD], soitemtotalamt [Total Amt], 0.00 [Total Disc Dtl Amt], soitemtotalnetto [Total Netto], '' [Tax Type], soitemtotaltaxamt [Tax Pct], soitemtotaltaxamtidr [Tax Amount], soitemgrandtotalamt [Grand Total Amt], soitemmstnote [Header Note], CASE WHEN ISNULL(soitemmstres1,'')='' THEN soitemmststatus ELSE ISNULL(soitemmstres1,'') END [Status], '' AS [App User], GETDATE() AS [App Date], som.createuser AS [Create User], som.createtime AS [Create Date], 'FG' AS [TipeneX],0.00 AS [Total CBF], som.closereason, som.closeuser, som.closetime, som.cmpcode[Division], som.cmpcode[Division Code], rm.rabtype2 [Kategori] " + Dtl + " FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid INNER JOIN QL_mstcurr curr ON curr.curroid = som.curroid INNER JOIN QL_m05GN g1 ON g1.gnoid = soitempaytypeoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid LEFT JOIN QL_mstrate2 r2 ON r2.rate2oid = som.rate2oid " + Join + " WHERE som.cmpcode='" + CompnyCode + "' AND som.soitemtype<>'Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND (CASE WHEN ISNULL(soitemmstres1,'')='' THEN soitemmststatus ELSE ISNULL(soitemmstres1,'') END) IN (" + Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (soitemno != "")
                {
                    string[] arr = soitemno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.soitemno IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                        filterdata += "'" + arr[i] + "',";
                    sSql += " AND rm.rabno IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                        datafilter += "'" + arr[i] + "',";
                    sSql += " AND som.salesoid IN (" + Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (DDLKategori != null)
                {
                    if (DDLKategori.Count() > 0)
                    {
                        string katval = "";
                        for (int i = 0; i < DDLKategori.Count(); i++) katval += "'" + DDLKategori[i] + "',";
                        sSql += " AND rm.rabtype2 IN (" + Left(katval, katval.Length - 1) + ")";
                    }
                }

                if (DDLType == "Detail")
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                            filterdata += "'" + arr[i] + "',";
                        sSql += " AND m.itemcode IN (" + Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
                sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + toDate(StartPeriod) + " - " + toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                if (DDLType == "Summary Status Amount")
                    this.HttpContext.Session["rptpaper"] = null;//CrystalDecisions.Shared.PaperSize.PaperA3;
                else
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary Status Amount") { }
                else
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        #endregion

        public class sFilter
        {
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string DDLPeriod { get; set; }
            public string filtercust { get; set; }
            public string filtersono { get; set; }
            public string filtermat { get; set; }
            public string filterrab { get; set; }
            public string filtersales { get; set; }
        }

        [HttpPost]
        public ActionResult GetModalData(sFilter param, string modaltype)
        {
            var result = ""; var sGroupBy = ""; var sOrderBy = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                if (modaltype.ToUpper() == "SALES")
                {
                    sSql = $"Select 0 seq, s.usoid [Username], s.usname [Nama], g.gndesc [Jabatan], s.usaddress [Alamat] FROM QL_m01US s INNER JOIN QL_trnsoitemmst som ON som.salesoid=s.usoid INNER JOIN QL_m05GN g ON gnoid=s.Jabatanoid AND gngroup='JABATAN' INNER JOIN QL_mstcust c ON c.custoid=som.custoid Inner Join QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid Inner Join QL_trnsoitemdtl d ON d.soitemmstoid=som.soitemmstoid Inner Join QL_mstitem i ON i.itemoid=d.itemoid Where som.cmpcode='{CompnyCode}' AND som.soitemtype<>'Jasa' AND som.soitemmststatus IN ('Post','Closed') ";
                    sGroupBy = " Group By s.usoid, s.usname, g.gndesc, s.usaddress";
                    sOrderBy = " Order By s.usname";
                }
                else if (modaltype.ToUpper() == "SONO")
                {
                    sSql = $"SELECT 0 seq, som.soitemno [No. SO], soitemdate [Tanggal], c.custname [Customer] FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid Inner Join QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid Inner Join QL_trnsoitemdtl d ON d.soitemmstoid=som.soitemmstoid Inner Join QL_mstitem i ON i.itemoid=d.itemoid Inner Join QL_m01US s ON s.usoid=som.salesoid WHERE som.cmpcode='{CompnyCode}' AND som.soitemtype<>'Jasa' AND som.soitemmststatus IN ('Post','Closed') ";
                    sGroupBy = " Group By som.soitemno, soitemdate, c.custname";
                    sOrderBy = " Order By soitemdate";
                }
                else if (modaltype.ToUpper() == "CUST")
                {
                    sSql = $"SELECT 0 seq, c.custcode [Kode], c.custname [Customer], c.custaddr [Alamat] FROM QL_mstcust c Inner Join QL_trnsoitemmst som ON som.custoid=c.custoid Inner Join QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid Inner Join QL_trnsoitemdtl d ON d.soitemmstoid=som.soitemmstoid Inner Join QL_mstitem i ON i.itemoid=d.itemoid Inner Join QL_m01US s ON s.usoid=som.salesoid WHERE som.cmpcode='{CompnyCode}' AND som.soitemtype<>'Jasa' AND som.soitemmststatus IN ('Post','Closed') ";
                    sGroupBy = " Group By c.custcode, c.custname, c.custaddr";
                    sOrderBy = " Order By custcode";
                }
                else if (modaltype.ToUpper() == "RAB")
                {
                    sSql = $"SELECT 0 seq, rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid Inner Join QL_trnsoitemdtl d ON d.soitemmstoid=som.soitemmstoid Inner Join QL_mstitem i ON i.itemoid=d.itemoid Inner Join QL_m01US s ON s.usoid=som.salesoid WHERE rm.cmpcode='{CompnyCode}' AND som.soitemtype<>'Jasa' AND som.soitemmststatus IN ('Post','Closed') ";
                    sGroupBy = " Group By rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote";
                    sOrderBy = " Order By rm.rabdate";
                }
                else if (modaltype.ToUpper() == "MAT")
                {
                    sSql = $"SELECT 0 seq, i.itemcode, i.itemdesc FROM QL_trnsoitemmst som INNER JOIN QL_trnsoitemdtl d ON d.soitemmstoid=som.soitemmstoid Inner Join QL_mstitem i ON i.itemoid=d.itemoid Inner Join QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid Inner Join QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid Inner Join QL_m01US s ON s.usoid=som.salesoid WHERE som.cmpcode='{CompnyCode}' AND som.soitemtype<>'Jasa' AND som.soitemmststatus IN ('Post','Closed') ";

                    sGroupBy = " Group By i.itemcode, i.itemdesc";
                    sOrderBy = " Order By itemcode";
                }
                if (!string.IsNullOrEmpty(param.filtersales)) sSql += $" AND s.usoid IN ('{param.filtersales.Replace(";", "', '")}')";
                if (!string.IsNullOrEmpty(param.filtersono)) sSql += $" AND som.soitemno IN ('{param.filtersono.Replace(";", "', '")}')";
                if (!string.IsNullOrEmpty(param.filtercust)) sSql += $" AND c.custcode IN ('{param.filtercust.Replace(";", "', '")}')";
                if (!string.IsNullOrEmpty(param.filterrab)) sSql += $" AND rabno IN ('{param.filterrab.Replace(";", "', '")}')";
                if (!string.IsNullOrEmpty(param.filtermat)) sSql += $" AND itemcode IN ('{param.filtermat.Replace(";", "', '")}')";

                if (param.periodstart != "" && param.periodend != "")
                {
                    if (param.DDLPeriod == "Transaksi")
                        param.DDLPeriod = "som.updtime";
                    else
                        param.DDLPeriod = "som.soitemdate";
                }

                sSql += $" AND {param.DDLPeriod}>=CAST('{toDate(param.periodstart)} 00:00:00' AS DATETIME) AND {param.DDLPeriod}<=CAST('{toDate(param.periodend)} 23:59:59' AS DATETIME)";
                sSql += sGroupBy + sOrderBy;

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString() + sSql;
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        #region Report TeleSales 

        public ActionResult Telesales()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (checkPagePermission("SOReport/Telesales", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        } 

        [HttpPost]
        public ActionResult ViewReport(sFilter param, string reporttype)
        {
            if (Session["UserID"] == null) return RedirectToAction("LogIn", "Account"); 
            var rptfile = $"rptSOTeleSales{reporttype.Replace("View", "Pdf")}.rpt";
            var rptname = $"LAPORAN_TELESALES_TGL_{param.periodstart} - {param.periodend}";  

            sSql = $"Select * from AP_v_report_telesales WHERE cmpcode='{CompnyCode}'";
            if (!string.IsNullOrEmpty(param.filtersales)) sSql += $" AND salesoid IN ('{param.filtersales.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtersono)) sSql += $" AND soitemno IN ('{param.filtersono.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtercust)) sSql += $" AND custcode IN ('{param.filtercust.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filterrab)) sSql += $" AND rabno IN ('{param.filterrab.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtermat)) sSql += $" AND itemcode IN ('{param.filtermat.Replace(";", "', '")}')";

            if (param.periodstart != "" && param.periodend != "")
            {
                if (param.DDLPeriod == "Transaksi")
                    param.DDLPeriod = "updtime";
                else
                    param.DDLPeriod = "soitemdate";
            }

            sSql += $" AND {param.DDLPeriod}>=CAST('{toDate(param.periodstart)} 00:00:00' AS DATETIME) AND {param.DDLPeriod}<=CAST('{toDate(param.periodend)} 23:59:59' AS DATETIME) Order By soitemno, soitemdate, itemcode";          

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + param.periodstart + " - " + param.periodend);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype.Replace("View", "") == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = null;//CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);

                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        #endregion

        #region Report Management
        public ActionResult SOManagement()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (checkPagePermission("SOReport/SOManagement", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult ViewReportManagement(sFilter param, string reporttype)
        {
            if (Session["UserID"] == null) return RedirectToAction("LogIn", "Account");
            var rptfile = $"rptSOManagement{reporttype.Replace("View", "Pdf")}.rpt";
            var rptname = $"LAPORAN_SALES_MANAGEMENT_TGL_{param.periodstart} - {param.periodend}"; 
            //rptname = "SO_MANAGEMENT";

            sSql = $"Select * from AP_v_report_management WHERE cmpcode='{CompnyCode}'";
            if (!string.IsNullOrEmpty(param.filtersales)) sSql += $" AND salesoid IN ('{param.filtersales.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtersono)) sSql += $" AND soitemno IN ('{param.filtersono.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtercust)) sSql += $" AND custcode IN ('{param.filtercust.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filterrab)) sSql += $" AND rabno IN ('{param.filterrab.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtermat)) sSql += $" AND itemcode IN ('{param.filtermat.Replace(";", "', '")}')";

            if (param.periodstart != "" && param.periodend != "")
            {
                if (param.DDLPeriod == "Transaksi")
                    param.DDLPeriod = "updtime";
                else
                    param.DDLPeriod = "soitemdate";
            }

            sSql += $" AND {param.DDLPeriod}>=CAST('{toDate(param.periodstart)} 00:00:00' AS DATETIME) AND {param.DDLPeriod}<=CAST('{toDate(param.periodend)} 23:59:59' AS DATETIME) Order By soitemno, soitemdate, itemcode";
            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + param.periodstart + " - " + param.periodend);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype.Replace("View", "") == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = null;//CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);

                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        #endregion

        #region Report SO Admin
        public ActionResult SOAdmin()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (checkPagePermission("SOReport/SOAdmin", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult ViewSOAdmin(sFilter param, string reporttype)
        {
            if (Session["UserID"] == null) return RedirectToAction("LogIn", "Account");
            var rptfile = $"rptSOAdmin{reporttype.Replace("View", "Pdf")}.rpt";
            var rptname = $"LAPORAN_SO_ADMIN_TGL_{param.periodstart} - {param.periodend}";
            sSql = $"Select * from AP_v_report_so_admin WHERE cmpcode='{CompnyCode}'";
            if (!string.IsNullOrEmpty(param.filtersales)) sSql += $" AND salesoid IN ('{param.filtersales.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtersono)) sSql += $" AND soitemno IN ('{param.filtersono.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filtercust)) sSql += $" AND custcode IN ('{param.filtercust.Replace(";", "', '")}')";
            if (!string.IsNullOrEmpty(param.filterrab)) sSql += $" AND rabno IN ('{param.filterrab.Replace(";", "', '")}')";
            //if (!string.IsNullOrEmpty(param.filtermat)) sSql += $" AND itemcode IN ('{param.filtermat.Replace(";", "', '")}')";

            if (param.periodstart != "" && param.periodend != "")
            {
                if (param.DDLPeriod == "Transaksi")
                    param.DDLPeriod = "updtime";
                else
                    param.DDLPeriod = "soitemdate";
            }

            sSql += $" AND {param.DDLPeriod}>=CAST('{toDate(param.periodstart)} 00:00:00' AS DATETIME) AND {param.DDLPeriod}<=CAST('{toDate(param.periodend)} 23:59:59' AS DATETIME) Order By soitemno, soitemdate";
            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + param.periodstart + " - " + param.periodend);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype.Replace("View", "") == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = null;//CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);

                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        #endregion
    }
}