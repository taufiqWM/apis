﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class PartnerController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string partktppath = "~/Images/PartnerKTP";
        private string partnpwppath = "~/Images/PartnerNPWP";
        private string imgtemppath = "~/Images/ImagesTemps";

        public PartnerController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class mstpartner : QL_mstpartner { }

        private void InitDDL(QL_mstpartner tbl)
        {
            var dt_provinceoid = db.QL_m05GN.Where(x => x.gngroup == "PROVINSI" && x.cmpcode == tbl.cmpcode && x.gnflag == "ACTIVE").ToList();
            var partnerprovinceoid = new SelectList(dt_provinceoid, "gnoid", "gndesc", tbl.partnerprovinceoid);
            ViewBag.partnerprovinceoid = partnerprovinceoid;
            
            var catid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "KATEGORI PARTNER" && x.cmpcode == tbl.cmpcode && x.gnflag == "ACTIVE"), "gnoid", "gndesc", null);
            ViewBag.catid = catid;
            
            var partnerpaymentoid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "PAYMENT TERM" && x.cmpcode == tbl.cmpcode && x.gnflag == "ACTIVE"), "gnoid", "gndesc", tbl.partnerpaymentoid);
            ViewBag.partnerpaymentoid = partnerpaymentoid;

            var provinceoid = (tbl.partnerprovinceoid != 0 ? tbl.partnerprovinceoid : dt_provinceoid.First().gnoid);
            var cityoid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "KOTA" && x.cmpcode == tbl.cmpcode && x.gnother2 == provinceoid.ToString() && x.gnflag == "ACTIVE"), "gnoid", "gndesc", tbl.partnercityoid);
            ViewBag.partnercityoid = cityoid;
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }

        [HttpPost]
        public ActionResult InitDDLCity()
        {
            JsonResult js = null;
            try
            {
                var tbl = db.QL_m05GN.Where(x => x.gngroup == "KOTA").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class partnerdtl1 : QL_mstpartnerdtl1
        {
            public List<SelectListItem> dt_city { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 partneroid, 0 partnerdtl1seq, 0 partnerdtl1oid, '' partnerdtl1addr, 0 partnercityoid, '' partnerdtl1phone, 'Active' partnerdtl1status, '[]' dt_city";
                var tbl = db.Database.SqlQuery<partnerdtl1>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    var dt_city = db.QL_m05GN.Where(x => x.gngroup == "KOTA").Select(x => new SelectListItem { Text = x.gndesc, Value = x.gnoid.ToString() }).ToList();
                    var partnercityoid = int.Parse(dt_city.FirstOrDefault().Value);
                    foreach (var item in tbl)
                    {
                        item.partnercityoid = partnercityoid;
                        item.dt_city = dt_city;
                    }
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails2()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT 0 partnerdtl2oid, 0 partnerdtl2seq, '' partnerdtl2picname, '' partnerdtl2phone1, '' partnerdtl2phone2, '' partnerdtl2email, '' partnerdtl2jabatan, GETDATE() partnertdtl2birthdate";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "data"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT sd1.partnerdtl1oid, sd1.partnerdtl1seq, sd1.partnerdtl1addr, sd1.partnercityoid, ISNULL((SELECT gndesc FROM QL_m05gn where gnoid = sd1.partnercityoid and gngroup = 'KOTA'), '') partnercitydesc, sd1.partnerdtl1phone, sd1.partnerdtl1status FROM QL_mstpartnerdtl1 sd1 where partneroid = {id} ORDER BY partnerdtl1seq";
                var dtl = db.Database.SqlQuery<partnerdtl1>(sSql).ToList();
                sSql = $"SELECT sd2.partnerdtl2oid, sd2.partnerdtl2seq, sd2.partnerdtl2picname, sd2.partnerdtl2phone1, sd2.partnerdtl2phone2, sd2.partnerdtl2email, sd2.partnerdtl2jabatan, sd2.partnerdtl2birthdate FROM QL_mstpartnerdtl2 sd2 where partneroid = {id} ORDER BY partnerdtl2seq";
                var dtl2 = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "data"));
                sSql = $"SELECT sd3.partnerdtl3oid, sd3.partnerdtl3seq, sd3.partnerdtl3picname, sd3.partnerdtl3phone1, sd3.partnerdtl3phone2, sd3.partnerdtl3email, sd3.partnerdtl3jabatan, sd3.partnerdtl3picproduk FROM QL_mstpartnerdtl3 sd3 where partneroid = {id} ORDER BY partnerdtl3seq";
                var dtl3 = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "data"));
                if (dtl == null || dtl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    var dt_city = db.QL_m05GN.Where(x => x.gngroup == "KOTA").Select(x => new SelectListItem { Text = x.gndesc, Value = x.gnoid.ToString() }).ToList();
                    foreach (var item in dtl)
                    {
                        item.dt_city = dt_city;
                    }
                    js = Json(new { result = "", dtl, dtl2, dtl3 }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        // GET: Partner
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT * FROM QL_mstpartner WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY partneroid DESC";
            var vbag = db.Database.SqlQuery<mstpartner>(sSql).ToList();
            return View(vbag);
        }

        // GET: Partner/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstpartner tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstpartner();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.partnercode = generateCode();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstpartner.Find(Session["CompnyCode"].ToString(), id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstpartner tbl, List<partnerdtl1> dtl, List<QL_mstpartnerdtl2> dtl2, List<QL_mstpartnerdtl3> dtl3, string action, string[] catid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var msg = ""; var result = "failed"; var hdrid = "";

            if (dtl == null) msg += "Silahkan isi detail pic kantor!<br/>";
            
            if (string.IsNullOrEmpty(tbl.partnercode)) msg += "Silahkan isi Kode!<br/>";
            else if (db.QL_mstpartner.Where(w => w.partnercode == tbl.partnercode & w.partneroid != tbl.partneroid).Count() > 0) msg += "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!<br/>";
            if (string.IsNullOrEmpty(tbl.partnername)) msg += "Silahkan isi Nama!<br/>";
            else if (db.QL_mstpartner.Where(w => w.partnername == tbl.partnername & w.partneroid != tbl.partneroid).Count() > 0) msg += "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!<br/>";
            if (string.IsNullOrEmpty(tbl.partneraddr)) tbl.partneraddr = "";
            if (string.IsNullOrEmpty(tbl.partnerpostcode)) tbl.partnerpostcode = "";
            if (string.IsNullOrEmpty(tbl.partnerphone1)) tbl.partnerphone1 = "";
            if (string.IsNullOrEmpty(tbl.partnerphone2)) tbl.partnerphone2 = "";
            if (string.IsNullOrEmpty(tbl.partnerfax1)) tbl.partnerfax1 = "";
            if (string.IsNullOrEmpty(tbl.partneremail)) tbl.partneremail = "";
            if (string.IsNullOrEmpty(tbl.partnergaransi)) tbl.partnergaransi = "";
            if (string.IsNullOrEmpty(tbl.partnernpwpno)) tbl.partnernpwpno = "";
            if (string.IsNullOrEmpty(tbl.partnernpwp)) tbl.partnernpwp = "";
            if (string.IsNullOrEmpty(tbl.partnernpwpaddr)) tbl.partnernpwpaddr = "";
            if (string.IsNullOrEmpty(tbl.partnerktpimg)) tbl.partnerktpimg = "";
            if (string.IsNullOrEmpty(tbl.partnernpwpimg)) tbl.partnernpwpimg = "";
            if (string.IsNullOrEmpty(tbl.partnernote)) tbl.partnernote = "";
            if (string.IsNullOrEmpty(tbl.partnerktpimg)) tbl.partnerktpimg = "~/Images/";
            if (string.IsNullOrEmpty(tbl.partnernpwpimg)) tbl.partnernpwpimg = "~/Images/";

            if (catid != null)
            {
                if (catid.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < catid.Count(); i++) stsval += "" + catid[i] + ",";
                    tbl.partnercategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                }
                else tbl.partnercategoryid = "";
            }
            else tbl.partnercategoryid = "";

            if (dtl != null)
            {
                for (int i = 0; i < dtl.Count(); i++)
                {
                    if (string.IsNullOrEmpty(dtl[i].partnerdtl1addr)) msg += "Detail Kantor No. " + dtl[i].partnerdtl1seq + " - Alamat tidak boleh kosong!<br/>";
                }
            }

            if (dtl2 != null)
            {
                for (int i = 0; i < dtl2.Count(); i++)
                {
                    if (string.IsNullOrEmpty(dtl2[i].partnerdtl2picname)) msg += "Detail PIC Partnet No. " + dtl2[i].partnerdtl2seq + " - Nama PIC tidak boleh kosong!<br/>";
                }
            }

            if (dtl3 != null)
            {
                for (int i = 0; i < dtl3.Count(); i++)
                {
                    if (string.IsNullOrEmpty(dtl3[i].partnerdtl3picname)) msg += "Detail PIC Produk No. " + dtl3[i].partnerdtl3seq + " - Nama PIC tidak boleh kosong!<br/>";
                }
            }

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstpartner");

            if (string.IsNullOrEmpty(msg))
            {
                var partnerdtl1oid = ClassFunction.GenerateID("QL_mstpartnerdtl1");
                var partnerdtl2oid = ClassFunction.GenerateID("QL_mstpartnerdtl2");
                var partnerdtl3oid = ClassFunction.GenerateID("QL_mstpartnerdtl3");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(tbl.partnerktpimg)))
                        {
                            var sExt = Path.GetExtension(tbl.partnerktpimg);
                            var sdir = Server.MapPath(partktppath);
                            var sfilename = tbl.partneroid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.partnerktpimg.ToLower() != (partktppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.partnerktpimg), sdir + "/" + sfilename);
                            }
                            tbl.partnerktpimg = partktppath + "/" + sfilename;
                        }
                        if (System.IO.File.Exists(Server.MapPath(tbl.partnernpwpimg)))
                        {
                            var sExt = Path.GetExtension(tbl.partnernpwpimg);
                            var sdir = Server.MapPath(partnpwppath);
                            var sfilename = tbl.partneroid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.partnernpwpimg.ToLower() != (partnpwppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.partnernpwpimg), sdir + "/" + sfilename);
                            }
                            tbl.partnernpwpimg = partnpwppath + "/" + sfilename;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.partneroid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstpartner.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstpartner'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Cat Detail
                            sSql = "DELETE FROM QL_mstcatdtl WHERE catdtltype='QL_mstpartner' AND catdtlrefoid=" + tbl.partneroid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //insert cat detail
                        if (catid != null)
                        {
                            if (catid.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < catid.Count(); i++)
                                {
                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstpartner";
                                    tblcatdtl.catdtlrefoid = tbl.partneroid;
                                    tblcatdtl.catoid = int.Parse(catid[i]);
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        //insert detail 1
                        if (dtl != null)
                        {
                            QL_mstpartnerdtl1 tbldtl1;
                            for (int i = 0; i < dtl.Count(); i++)
                            {
                                if (dtl[i].partnerdtl1oid == 0)
                                {
                                    tbldtl1 = new QL_mstpartnerdtl1();
                                    tbldtl1.partnerdtl1oid = partnerdtl1oid++;
                                }
                                else
                                {
                                    tbldtl1 = db.QL_mstpartnerdtl1.Find(CompnyCode, dtl[i].partnerdtl1oid);
                                }
                                tbldtl1.cmpcode = CompnyCode;
                                tbldtl1.partnerdtl1seq = i + 1;
                                tbldtl1.partneroid = tbl.partneroid;
                                tbldtl1.partnerdtl1addr = dtl[i].partnerdtl1addr ?? "";
                                tbldtl1.partnercityoid = dtl[i].partnercityoid;
                                tbldtl1.partnerdtl1phone = dtl[i].partnerdtl1phone ?? "";
                                tbldtl1.partnerdtl1status = dtl[i].partnerdtl1status ?? "";
                                tbldtl1.updtime = tbl.updtime;
                                tbldtl1.upduser = tbl.upduser;
                                if (dtl[i].partnerdtl1oid == 0) db.QL_mstpartnerdtl1.Add(tbldtl1);
                                else db.Entry(tbldtl1).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (partnerdtl1oid - 1) + " Where tablename = 'QL_mstpartnerdtl1'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //insert detail 2
                        if (dtl2 != null)
                        {
                            QL_mstpartnerdtl2 tbldtl2;
                            for (int i = 0; i < dtl2.Count(); i++)
                            {
                                if (dtl2[i].partnerdtl2oid == 0)
                                {
                                    tbldtl2 = new QL_mstpartnerdtl2();
                                    tbldtl2.partnerdtl2oid = partnerdtl2oid++;
                                }
                                else
                                {
                                    tbldtl2 = db.QL_mstpartnerdtl2.Find(CompnyCode, dtl2[i].partnerdtl2oid);
                                }
                                tbldtl2.cmpcode = CompnyCode;
                                tbldtl2.partnerdtl2seq = i + 1;
                                tbldtl2.partneroid = tbl.partneroid;
                                tbldtl2.partnerdtl2picname = dtl2[i].partnerdtl2picname;
                                tbldtl2.partnerdtl2phone1 = dtl2[i].partnerdtl2phone1 ?? "";
                                tbldtl2.partnerdtl2phone2 = dtl2[i].partnerdtl2phone2 ?? "";
                                tbldtl2.partnerdtl2email = dtl2[i].partnerdtl2email ?? "";
                                tbldtl2.partnerdtl2jabatan = dtl2[i].partnerdtl2jabatan ?? "";
                                tbldtl2.partnerdtl2birthdate = dtl2[i].partnerdtl2birthdate;
                                tbldtl2.updtime = tbl.updtime;
                                tbldtl2.upduser = tbl.upduser;
                                if (dtl2[i].partnerdtl2oid == 0) db.QL_mstpartnerdtl2.Add(tbldtl2);
                                else db.Entry(tbldtl2).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (partnerdtl2oid - 1) + " Where tablename = 'QL_mstpartnerdtl2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //insert detail 3
                        if (dtl3 != null)
                        {
                            QL_mstpartnerdtl3 tbldtl3;
                            for (int i = 0; i < dtl3.Count(); i++)
                            {
                                if (dtl3[i].partnerdtl3oid == 0)
                                {
                                    tbldtl3 = new QL_mstpartnerdtl3();
                                    tbldtl3.partnerdtl3oid = partnerdtl3oid++;
                                }
                                else
                                {
                                    tbldtl3 = db.QL_mstpartnerdtl3.Find(CompnyCode, dtl3[i].partnerdtl3oid);
                                }
                                tbldtl3.cmpcode = CompnyCode;
                                tbldtl3.partnerdtl3seq = i + 1;
                                tbldtl3.partneroid = tbl.partneroid;
                                tbldtl3.partnerdtl3picname = dtl3[i].partnerdtl3picname;
                                tbldtl3.partnerdtl3phone1 = dtl3[i].partnerdtl3phone1 ?? "";
                                tbldtl3.partnerdtl3phone2 = dtl3[i].partnerdtl3phone2 ?? "";
                                tbldtl3.partnerdtl3email = dtl3[i].partnerdtl3email ?? "";
                                tbldtl3.partnerdtl3jabatan = dtl3[i].partnerdtl3jabatan ?? "";
                                tbldtl3.partnerdtl3picproduk = dtl3[i].partnerdtl3picproduk ?? "";
                                tbldtl3.updtime = tbl.updtime;
                                tbldtl3.upduser = tbl.upduser;
                                if (dtl3[i].partnerdtl3oid == 0) db.QL_mstpartnerdtl3.Add(tbldtl3);
                                else db.Entry(tbldtl3).State = EntityState.Modified;
                            }
                            db.SaveChanges();
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (partnerdtl3oid - 1) + " Where tablename = 'QL_mstpartnerdtl3'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        
                        objTrans.Commit();
                        hdrid = tbl.partneroid.ToString();
                        msg = "Data Berhasil di Simpan! <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Partner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstpartner tbl = db.QL_mstpartner.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl != null)
                        {
                            System.IO.File.Delete(Server.MapPath(tbl.partnerktpimg));
                            System.IO.File.Delete(Server.MapPath(tbl.partnernpwpimg));
                        }

                        var trndtl = db.QL_mstpartnerdtl1.Where(a => a.partneroid == id);
                        var trn2dtl = db.QL_mstpartnerdtl2.Where(a => a.partneroid == id);
                        var trn3dtl = db.QL_mstpartnerdtl3.Where(a => a.partneroid == id);
                        db.QL_mstpartnerdtl1.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_mstpartnerdtl2.RemoveRange(trn2dtl);
                        db.SaveChanges();

                        db.QL_mstpartnerdtl3.RemoveRange(trn3dtl);
                        db.SaveChanges();

                        db.QL_mstpartner.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string generateCode()
        {
            string sCode = "PARTN-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(partnerCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstpartner WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND partnerCode LIKE '" + sCode + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintXlsReport(string typeexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (typeexp == "Partner")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPartnerXls.rpt"));
            }
            else if (typeexp == "PIC")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPartnerPICXls.rpt"));
            }
            else if (typeexp == "PICProd")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPartnerPICProdXls.rpt"));
            }
            else if (typeexp == "Alamat")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPartnerAlamatXls.rpt"));
            }
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "ExportPartner_" + typeexp + ".xls");
        }
    }
}
