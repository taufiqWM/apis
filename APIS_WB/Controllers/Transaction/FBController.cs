﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class FBController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public FBController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listapitemdtl
        {
            public int apitemdtlseq { get; set; }            
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public decimal apitemqty { get; set; }
            public int apitemunitoid { get; set; }
            public string apitemunit { get; set; }
            public decimal apitemprice { get; set; }
            public decimal apitemdtldiscvalue { get; set; }
            public decimal apitemdtldiscamt { get; set; }
            public decimal apitemdtlamt { get; set; }
            public decimal apitemdtltaxvalue { get; set; }
            public decimal apitemdtltaxamt { get; set; }            
            public decimal apitemdtlnetto { get; set; }           
            public string apitemdtlnote { get; set; }            
            public string mritemno { get; set; }
            public DateTime mritemdate { get; set; }
            public int mritemmstoid { get; set; }
            public int mritemdtloid { get; set; }
        }

        public class listapitemdtl2
        {
            public int apitemdtl2seq { get; set; }
            public int fabelioid { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
            public decimal apitemdtl2amt { get; set; }
        }

        public class trnapdtl
        {
            public decimal apqty { get; set; }
            public decimal mrvalue { get; set; }
            public decimal mrvalueidr { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public int rabmstoid_awal { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }
            public int poitemmstoid { get; set; }
            public string poitemno { get; set; }
            public int reqrabmstoid { get; set; }
            public string reqrabno { get; set; }
            public string suppname { get; set; }
            public int suppoid { get; set; }
            public string rabmstnote { get; set; }
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public int suppsalesoid { get; set; }
            public string suppsales { get; set; }
            public int deptoid { get; set; }            
            public string departement { get; set; }
            public int supppaymentoid { get; set; }
            public int gnother1 { get; set; }
            public DateTime checkttdate { get; set; }
            public string apitemno { get; set; }
            public int curroid { get; set; }
            public decimal dpfmamt { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        public class mritem
        {
            public int mritemmstoid { get; set; }
            public string mritemno { get; set; }
            public DateTime mritemdate { get; set; }           
            public string mritemdatestr { get; set; }           
            public string mritemmstnote { get; set; }
            public string poitemno { get; set; }
        }

        public class listduedate
        {
            public DateTime duedate { get; set; }
            public string duedatestr { get; set; }
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppengakuan { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<listapitemdtl> dtDtl, List<listapitemdtl2> dtDtl2, Guid? uid)
        {
            Session["QL_trnapitemdtl"+ uid] = dtDtl;
            Session["QL_trnapitemdtl2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listapitemdtl> dtDtl, Guid? uid)
        {
            Session["QL_trnapitemdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trnapitemdtl"+ uid] == null)
            {
                Session["QL_trnapitemdtl"+ uid] = new List<listapitemdtl>();
            }

            List<listapitemdtl> dataDtl = (List<listapitemdtl>)Session["QL_trnapitemdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listapitemdtl2> dtDtl, Guid? uid)
        {
            Session["QL_trnapitemdtl2"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["QL_trnapitemdtl2"+ uid] == null)
            {
                Session["QL_trnapitemdtl2"+ uid] = new List<listapitemdtl2>();
            }

            List<listapitemdtl2> dataDtl = (List<listapitemdtl2>)Session["QL_trnapitemdtl2"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND apitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetSupplierData(int apitemmstoid)
        {
            List<listsupp> tbl = new List<listsupp>();

            sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid, supppengakuan FROM QL_mstsupp s INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=s.cmpcode AND mrm.suppoid=s.suppoid WHERE s.cmpcode='" + CompnyCode + "' AND mrm.mritemmststatus='Post' AND mrm.mritemmstoid NOT IN (SELECT DISTINCT mritemmstoid FROM QL_trnapitemdtl apd INNER JOIN QL_trnapitemmst apm ON apm.cmpcode=apd.cmpcode AND apm.apitemmstoid=apd.apitemmstoid AND apm.apitemtype NOT IN ('Jasa') WHERE apd.cmpcode='" + CompnyCode +"' AND apitemmststatus<>'Rejected' AND apm.apitemmstoid<>"+ apitemmstoid +" ) ORDER BY suppcode";
            tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();           

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMRData(int poitemmstoid)
        {
            List<mritem> tbl = new List<mritem>();
            sSql = "SELECT DISTINCT sm.mritemmstoid, mritemno, mritemdate,CONVERT(char(20),mritemdate,103) mritemdatestr, mritemmstnote, poitemno FROM QL_trnmritemmst sm INNER JOIN QL_trnmritemdtl sd ON sd.cmpcode=sm.cmpcode AND sd.mritemmstoid=sm.mritemmstoid AND sm.mritemtype NOT IN ('Jasa') INNER JOIN QL_trnpoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.poitemmstoid=sm.pomstoid AND dom.poitemtype NOT IN ('Jasa') WHERE sm.cmpcode='" + CompnyCode +"' AND sm.pomstoid = " + poitemmstoid + " AND mritemmststatus='Post' AND ISNULL(mritemmstres1,'')<>'Closed' ORDER BY mritemdate DESC, sm.mritemmstoid DESC";

            tbl = db.Database.SqlQuery<mritem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private void InitDDL(QL_trnapitemmst tbl)
        {
           sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var apitempaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.apitempaytypeoid);
            ViewBag.apitempaytypeoid = apitempaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;          
        }

        private void FillAdditionalField(QL_trnapitemmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("select (SELECT reqrabno FROM QL_trnreqrabmst where reqrabmstoid = rb.reqrabmstoid) from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid +"").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("select rb.projectname from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.poitemno = db.Database.SqlQuery<string>("select poitemno from QL_trnpoitemmst rb where poitemmstoid = " + tblmst.poitemmstoid + "").FirstOrDefault();
            ViewBag.suppsales = db.Database.SqlQuery<string>("SELECT suppdtl2picname FROM QL_mstsuppdtl2 where suppdtl2oid = '"+ tblmst.suppsalesoid +"'").FirstOrDefault();
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp a WHERE a.suppoid ='" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.gnother1 = db.Database.SqlQuery<int>("SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid ='" + tblmst.apitempaytypeoid + "'").FirstOrDefault();
            ViewBag.supppengakuan = db.Database.SqlQuery<string>("SELECT supppengakuan FROM QL_mstsupp WHERE suppoid ='" + tblmst.suppoid + "'").FirstOrDefault();
            //ViewBag.apitemttdate = db.Database.SqlQuery<string>("SELECT CONVERT(varchar(20),apitemttdate,103) FROM QL_trnapitemmst WHERE apitemmstoid ='" + tblmst.apitemmstoid + "'").FirstOrDefault();
            //ViewBag.dpfmamt = db.Database.SqlQuery<decimal>("SELECT ISNULL((SELECT SUM(dp.dpapamt) FROM QL_trndpap dp WHERE dp.pomstoid = '" + tblmst.poitemmstoid + "' AND dp.poreftype = 'QL_trnpoitemmst' AND dp.dpappaytype = 'DPFM'),0.0) dpfmamt").FirstOrDefault();
            
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDueDate(int paytype, string tgldue)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select dateadd(D, " + paytype + ",'"+ DateTime.Parse(ClassFunction.toDate(tgldue)) + "') duedate,CONVERT(char(20),dateadd(D, " + paytype +",'"+ DateTime.Parse(ClassFunction.toDate(tgldue)) + "'),103) duedatestr ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData(int suppoid, string supppengakuan)
        {
            List<listrab> tbl = new List<listrab>();
            var Join = "";
            var SqlPlus = ", getdate() checkttdate";
            if (supppengakuan == "TT")
            {
                Join = " INNER JOIN QL_trncheckttdtl cd ON cd.poitemmstoid = pm.poitemmstoid AND cd.rabmstoid = rm.rabmstoid INNER JOIN QL_trncheckttmst cm ON cm.checkttmstoid = cd.checkttmstoid AND cm.checkttmststatus = 'Post' AND cd.potype='QL_trnpoitemmst' ";
                SqlPlus = ", (SELECT checkttdate FROM QL_trncheckttmst where checkttmstoid = cd.checkttmstoid) checkttdate, (SELECT apitemno FROM QL_trncheckttdtl where poitemmstoid = pm.poitemmstoid AND potype='QL_trnapitemmst') apitemno";
            }

            sSql = "SELECT DISTINCT rm.rabmstoid, CASE WHEN rm.revrabtype='Baru' THEN rm.rabmstoid ELSE rm.revrabmstoid END rabmstoid_awal, s.supppaymentoid, (SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = supppaymentoid) gnother1, rm.rabno, rm.rabdate, CONVERT(CHAR(10),rm.rabdate,103) rabdatestr, pm.poitemmstoid, poitemno, ISNULL(rm.reqrabmstoid,0) reqrabmstoid, ISNULL((SELECT reqrabno FROM QL_trnreqrabmst where reqrabmstoid = rm.reqrabmstoid),'') AS reqrabno, rm.projectname, ISNULL(rm.salesoid,'') salesoid, s.suppname, s.suppoid, rm.rabmstnote, rm.alamatoid, pm.suppsalesoid, (SELECT suppdtl2picname FROM QL_mstsuppdtl2 where suppdtl2oid = pm.suppsalesoid) suppsales, rm.curroid, ISNULL((SELECT SUM(dp.dpapamt-dp.dpapaccumamt) FROM QL_trndpap dp WHERE dp.pomstoid=pm.poitemmstoid AND dp.poreftype='QL_trnpoitemmst' AND dp.dpappaytype='DPFM' AND ISNULL(dpapres1,'')<>'Batal'),0.0) dpfmamt " + SqlPlus + " FROM QL_trnrabmst rm INNER JOIN QL_trnrabdtl2 rd2 ON rd2.rabmstoid = rm.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = rd2.suppdtl2oid INNER JOIN QL_trnpoitemmst pm ON pm.rabmstoid = rm.rabmstoid and pm.suppoid = s.suppoid " + Join + " WHERE rm.cmpcode = '" + CompnyCode + "' AND pm.poitemtype NOT IN ('Jasa')  AND pm.poitemmstoid IN (SELECT pomstoid FROM QL_trnmritemmst mr where mritemmststatus = 'POST' AND mritemtype NOT IN ('Jasa')) AND rabmststatus IN('Approved','Closed') AND s.suppoid = " + suppoid + " ORDER BY rabdate DESC";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailData(int mritemmstoid)
        {
            List<listapitemdtl> tbl = new List<listapitemdtl>();
            sSql = "SELECT sod.poitemdtlseq apitemdtlseq, sm.mritemno, sm.mritemdate, rabdtl2oid, m.itemoid, m.itemcode, m.itemdesc, sd.refno, (sd.mritemqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretitemmstoid=pretd.pretitemmstoid WHERE pretd.cmpcode=sd.cmpcode AND pretd.mritemdtloid=sd.mritemdtloid AND ISNULL(pretm.pretitemtype,'')='' AND pretitemmststatus<>'Rejected'), 0.0)) AS apitemqty, sod.poitemunitoid apitemunitoid, gn.gndesc apitemunit, sod.poitemprice apitemprice, sod.poitemdtltaxvalue apitemdtltaxvalue, sod.poitemdtldiscvalue apitemdtldiscvalue, '' apitemdtlnote, sd.mritemmstoid, sd.mritemdtloid FROM QL_trnmritemdtl sd INNER JOIN QL_trnmritemmst sm ON sm.mritemmstoid = sd.mritemmstoid AND sm.mritemtype NOT IN ('Jasa') INNER JOIN QL_mstitem m ON m.itemoid = sd.itemoid INNER JOIN QL_m05gn gn ON gnoid = mritemunitoid INNER JOIN QL_trnpoitemmst som ON som.cmpcode = sm.cmpcode AND som.poitemmstoid = sm.pomstoid AND som.poitemtype NOT IN ('Jasa') INNER JOIN QL_trnpoitemdtl sod ON sod.cmpcode = sd.cmpcode AND sod.poitemmstoid = sm.pomstoid AND sod.poitemdtloid = sd.podtloid WHERE sd.cmpcode = '" + CompnyCode +"' AND sm.mritemmstoid = " + mritemmstoid + " AND mritemdtlstatus = '' AND (mritemqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid WHERE pretd.cmpcode = sd.cmpcode AND pretd.mritemdtloid = sd.mritemdtloid AND ISNULL(pretm.pretitemtype,'')='' AND pretitemmststatus <> 'Rejected'), 0.0)) > 0 ORDER BY mritemdtlseq";

                tbl = db.Database.SqlQuery<listapitemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();
            //tblmst.apitemduedate = ClassFunction.GetServerTime().AddDays(7);
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFakturData(int suppoid)
        {
            List<listapitemdtl2> tbl = new List<listapitemdtl2>();

            sSql = "SELECT 0 AS apitemdtl2seq, fa.fabelioid, fa.fabelicode, fa.fakturno, (fa.fabeliqty-fa.fabeliaccumqty) fabeliaccumqty, 0.0 AS apitemdtl2amt FROM QL_mstfabeli fa WHERE fa.activeflag='ACTIVE' AND fa.suppoid=" + suppoid + " AND (fabeliqty-fabeliaccumqty)>0 ORDER BY fa.fakturno";
            tbl = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
      
        // GET/POST: apitem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No FB",["Supplier"] = "Supplier",["Project"] = "Project",["NomorPO"] = "No PO" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select pr.apitemmstoid, pr.apitemno, pr.apitemdate, c.suppname, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') rabno, ISNULL((SELECT rm.poitemno FROM QL_trnpoitemmst rm WHERE rm.poitemmstoid=pr.poitemmstoid),'') poitemno, pr.apitemgrandtotal grandtotal, pr.apitemmststatus FROM QL_trnapitemmst pr INNER JOIN QL_mstsupp c ON c.suppoid=pr.suppoid WHERE pr.cmpcode='" + CompnyCode + "' AND pr.apitemtype NOT IN ('Jasa') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.apitemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.apitemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.apitemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.apitemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "NomorPO") sSql += " AND t.poitemno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.apitemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.apitemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.apitemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.apitemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapitemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = new QL_trnapitemmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();                
                tblmst.apitemdate = ClassFunction.GetServerTime();
                tblmst.apitemttdate = ClassFunction.GetServerTime();
                tblmst.apitemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.apitemmstdiscamt = 0;

                Session["QL_trnapitemdtl"+ ViewBag.uid] = null;
                Session["QL_trnapitemdtl2"+ ViewBag.uid] = null;
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tblmst = db.QL_trnapitemmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT rd.apitemdtlseq, sm.mritemno, rd.itemoid, i.itemcode, i.itemdesc, (SELECT refno FROM QL_trnmritemdtl where mritemdtloid = rd.mritemdtloid) refno, rd.apitemqty, rd.apitemunitoid, g.gndesc apitemunit, rd.apitemprice, rd.apitemdtlamt, rd.apitemdtldiscvalue, rd.apitemdtldiscamt, rd.apitemdtltaxvalue, rd.apitemdtltaxamt, rd.apitemdtlnetto, rd.apitemdtlnote, rd.mritemdtloid, rd.mritemmstoid FROM QL_trnapitemdtl rd INNER JOIN QL_trnmritemmst sm ON sm.mritemmstoid = rd.mritemmstoid AND sm.mritemtype NOT IN ('Jasa') INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.apitemunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " ORDER BY rd.apitemdtlseq";
                Session["QL_trnapitemdtl"+ ViewBag.uid] = db.Database.SqlQuery<listapitemdtl>(sSql).ToList();

                sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trnapitemmst' ORDER BY rd.apitemdtl2seq";
                Session["QL_trnapitemdtl2"+ ViewBag.uid] = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();
            }
            ViewBag.deftax = db.QL_m05GN.FirstOrDefault(x => x.gngroup == "DEFAULT TAX").gndesc ?? "10";

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapitemmst tblmst, string action, string tglmst, string tgljt, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tblmst.apitemmststatus == "Post")
            {
                tblmst.apitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tblmst.apitemno = "";
            }            
            try
            {
                tblmst.apitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }           
            try
            {
                tblmst.apitemttdate = DateTime.Parse(ClassFunction.toDate(tgljt));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apitemttdate", "Format Tanggal JT Tidak Valid!!" + ex.ToString());
            }                   

            if (string.IsNullOrEmpty(tblmst.apitemsuppfjno))
                tblmst.apitemsuppfjno = "";
            if (string.IsNullOrEmpty(tblmst.apitemtype))
                tblmst.apitemtype = "";
            if (string.IsNullOrEmpty(tblmst.apitemmstnote))
                tblmst.apitemmstnote = "";           
            if (string.IsNullOrEmpty(tblmst.apitemmstres1))
                tblmst.apitemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.apitemmstres2))
                tblmst.apitemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.apitemmstres3))
                tblmst.apitemmstres3 = "";
            if (string.IsNullOrEmpty(tblmst.apitemfaktur))
                tblmst.apitemfaktur = "";


            //is Input Detail Valid
            List<listapitemdtl> dtDtl = (List<listapitemdtl>)Session["QL_trnapitemdtl"+ uid];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["QL_trnapitemdtl2"+ uid];
            if (tblmst.apitemmststatus == "Post")
            {
                if (Math.Round(tblmst.apitemtaxamt.Value, 0) > 10 || Math.Round(tblmst.apitemtaxamt.Value, 0) < (-10))
                {
                    if (dtDtl2 == null)
                        ModelState.AddModelError("", "Please fill detail faktur data!");
                    else if (dtDtl2.Count <= 0)
                        ModelState.AddModelError("", "Please fill detail faktur data!");
                }
            }

            decimal dAmtOngkir = 0;
            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].apitemdtlnote))
                            dtDtl[i].apitemdtlnote = "";
                        if (dtDtl[i].apitemqty == 0)
                        {
                            ModelState.AddModelError("", "Qty " + dtDtl[i].itemdesc + " Belum Diisi !!");
                        }
                        if (dtDtl[i].apitemprice == 0)
                        {
                            ModelState.AddModelError("", "Harga " + dtDtl[i].itemdesc + " Belum Diisi !!");
                        }

                        sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                        var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                        //Set Amt Untuk Ongkir
                        if (type == "Ongkir")
                        {
                            dAmtOngkir += dtDtl[i].apitemqty * dtDtl[i].apitemprice;
                        }

                        if (action == "Create")
                        {
                            //Cek MR Sdh di tarik
                            var mrdtloid = dtDtl[i].mritemdtloid;
                            if (db.QL_trnmritemdtl.Where(md => md.mritemdtloid == mrdtloid && md.mritemdtlstatus == "Complete").Count() > 0)
                                ModelState.AddModelError("", "BPB No. " + dtDtl[i].mritemno + " Sudah Dibuatkan FB!!");
                        }
                        else
                        {
                            //Cek MR Sdh di tarik
                            var mrdtloid = dtDtl[i].mritemdtloid; var mrmstoid = dtDtl[i].mritemmstoid;
                            if (db.QL_trnmritemdtl.Where(md => md.mritemdtloid == mrdtloid && md.mritemdtlstatus == "Complete" && md.mritemmstoid != mrmstoid).Count() > 0)
                                ModelState.AddModelError("", "BPB No. " + dtDtl[i].mritemno + " Sudah Dibuatkan FB!!");
                        }
                    }

                    if (tblmst.apitemmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tblmst.apitemno))
                            ModelState.AddModelError("apitemno", "Silahkan isi No. Faktur!");
                        else if (db.QL_trnapitemmst.Where(w => w.apitemno == tblmst.apitemno & w.apitemmstoid != tblmst.apitemmstoid).Count() > 0)
                            ModelState.AddModelError("apitemno", "No. Faktur yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");                        
                    }
                }
            }

            string apitemdtl2type = "QL_trnapitemmst";
            decimal dpfmamt = tblmst.apdpamt;
            //db.Database.SqlQuery<decimal>("SELECT ISNULL((SELECT SUM(dp.dpapamt) FROM QL_trndpap dp WHERE dp.pomstoid = " + tblmst.poitemmstoid + " AND dp.poreftype = 'QL_trnpoitemmst' AND dp.dpappaytype = 'DPFM' AND ISNULL(dp.dpapres1,'')<>'Batal'),0.0) dpfmamt").FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    totaltaxhdr = tblmst.apitemtaxamt.Value;
                    decimal totaltaxdtl = 0;
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totaltaxdtl += dtDtl2[i].apitemdtl2amt;
                        fakturnoGL += dtDtl2[i].fakturno + ",";
                    }
                    fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                    if (dpfmamt > 0)
                    {
                        //totaltaxhdr = totaltaxhdr + dpfmamt;
                    }
                    decimal ht_tax = Math.Round(totaltaxhdr, 0) - Math.Round(totaltaxdtl, 0);
                    if (ht_tax > 10 || ht_tax < (-10))
                    {
                        ModelState.AddModelError("apitemtaxamt", "Total Detail Faktur Pajak Harus Sama Dengan Jumlah PPN!");
                    }
                }
            }

            if (tblmst.apitemmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED"));
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_IN"));
                if (!ClassFunction.IsInterfaceExists("VAR_AP", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AP"));
                if (!ClassFunction.IsInterfaceExists("VAR_DISC_AP", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DISC_AP"));
                if (!ClassFunction.IsInterfaceExists("VAR_ONGKIR_BELI", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_ONGKIR_BELI"));
                if (!ClassFunction.IsInterfaceExists("VAR_DPFAKTUR_M", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DPFAKTUR_M"));
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tblmst.apitemmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("",cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            decimal totalrab = 0;
            decimal margin = 0;
            decimal persenmargin = 0;
            if (tblmst.apitemmststatus == "Post")
            {
                if (tblmst.apitemmstdiscamt > 0)
                {
                    //sSql = "SELECT rabgrandtotaljualamt + (rabgrandtotalbeliamt - " + tblmst.apitemmstdiscamt + ") + rabgrandtotaljualjasaamt + rabgrandtotalbelijasaamt + rabgrandtotalcostamt AS total FROM QL_trnrabmst WHERE rabmstoid=" + tblmst.rabmstoid + "";
                    sSql = "SELECT CASE WHEN rabtype='WAPU' THEN (rabtotaljualamt - rabtotaljualpphamt) ELSE (rabtotaljualamt + rabtotaljualtaxamt) END AS total FROM QL_trnrabmst WHERE rabmstoid=" + tblmst.rabmstoid + "";
                    totalrab = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                    //sSql = "SELECT (rabgrandtotaljualamt - (rabgrandtotalbeliamt - " + tblmst.apitemmstdiscamt + ")) + (rabgrandtotaljualjasaamt - rabgrandtotalbelijasaamt) - rabgrandtotalcostamt AS margin FROM QL_trnrabmst WHERE rabmstoid=" + tblmst.rabmstoid + "";
                    sSql = "SELECT CASE WHEN rabtype='WAPU' THEN ((rabtotaljualamt - rabtotaljualpphamt) - (rabtotalbeliamt + rabtotalbelitaxamt - " + tblmst.apitemmstdiscamt + ") - (rabtotalbelijasaamt + rabtotalbelijasataxamt) - rabgrandtotalcostamt) ELSE ((rabtotaljualamt + rabtotaljualtaxamt) - (rabtotalbeliamt + rabtotalbelitaxamt - " + tblmst.apitemmstdiscamt + ") - (rabtotalbelijasaamt + rabtotalbelijasataxamt) - rabgrandtotalcostamt) END AS margin FROM QL_trnrabmst WHERE rabmstoid=" + tblmst.rabmstoid + "";
                    margin = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                    if (margin > 0 && totalrab > 0)
                    {
                        persenmargin = (margin / totalrab) * 100;
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.apitemdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.apitemmststatus = "In Process";
            }
            if (tblmst.apitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.apitemmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var mstoid = ClassFunction.GenerateID("QL_trnapitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnapitemdtl");
                var dtl2oid = ClassFunction.GenerateID("QL_trnapitemdtl2");

                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));
                var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP", CompnyCode));
                var iAcctgOidDisc = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DISC_AP", CompnyCode));
                var iAcctgOidOngkir = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ONGKIR_BELI", CompnyCode));
                var iAcctgOidDPFM = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DPFAKTUR_M", CompnyCode));

                tblmst.apitemsuppfjdate = tblmst.apitemdate;
                tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                tblmst.rateoid = 1;
                tblmst.rate2oid = rate2oid;
                //tblmst.apitemmstdiscamt = 0;
                //tblmst.apitemmstdisctype = "";
                tblmst.apitemmstdiscvalue = 0;
                tblmst.apitemothercost = 0;
                tblmst.apitemrate2tousd = "0";
                tblmst.apitemratetoidr = "1";
                tblmst.apitemrate2toidr = rate2toidr;
                tblmst.apitemratetousd = "0";
                tblmst.apitemtaxtype = "TAX";
                tblmst.apitemtaxvalue = 10;
                tblmst.apitemtotaldisc = (tblmst.apitemmstdiscamt + tblmst.apitemtotaldiscdtl);
                //tblmst.apitemtotaldiscdtl = 0;
                tblmst.apitemtype = "";

                var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tblmst.suppoid).FirstOrDefault();
                DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(DAY, (CASE g.gnother1 WHEN 0 THEN 0 ELSE CAST(g.gnother1 AS INT) END), '"+ tblmst.apitemttdate + "') AS [Due Date] FROM QL_m05gn g WHERE g.gnoid="+ tblmst.apitempaytypeoid + " AND g.gngroup='PAYMENT TERM'").FirstOrDefault();
                tblmst.apitemduedate = duedate;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.apitemmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                           
                            db.QL_trnapitemmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnapitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                          
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmritemmst SET mritemmststatus = 'Post' WHERE mritemmstoid IN (SELECT mritemmstoid FROM QL_trnapitemdtl where apitemmstoid = " + tblmst.apitemmstoid + ") AND mritemtype NOT IN ('Jasa')";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmritemdtl SET mritemdtlstatus = '' WHERE mritemdtloid IN (SELECT mritemdtloid FROM QL_trnapitemdtl where apitemmstoid = " + tblmst.apitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapitemdtl.Where(a => a.apitemmstoid == tblmst.apitemmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnapitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tblmst.apitemmstoid && a.apitemdtl2type == apitemdtl2type  && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        QL_trnapitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnapitemdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.apitemdtloid = dtloid++;
                            tbldtl.apitemmstoid = tblmst.apitemmstoid;
                            tbldtl.apitemdtlseq = i + 1;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.apitemqty = dtDtl[i].apitemqty;
                            tbldtl.apitemprice = dtDtl[i].apitemprice;
                            tbldtl.apitemunitoid = dtDtl[i].apitemunitoid;
                            tbldtl.apitemdtlamt = dtDtl[i].apitemdtlamt;
                            tbldtl.apitemdtlnetto = dtDtl[i].apitemdtlnetto;
                            tbldtl.apitemdtlstatus = "";
                            tbldtl.apitemdtlnote = dtDtl[i].apitemdtlnote;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.apitemdtldisctype = "";
                            tbldtl.apitemdtldiscamt = dtDtl[i].apitemdtldiscamt;
                            tbldtl.apitemdtldiscvalue = dtDtl[i].apitemdtldiscvalue;
                            tbldtl.apitemdtlres1 = "";
                            tbldtl.apitemdtlres2 = "";
                            tbldtl.apitemdtlres3 = "";
                            tbldtl.mritemmstoid = dtDtl[i].mritemmstoid;
                            tbldtl.mritemdtloid = dtDtl[i].mritemdtloid;
                            tbldtl.apitemdtltaxvalue = dtDtl[i].apitemdtltaxvalue;
                            tbldtl.apitemdtltaxamt = dtDtl[i].apitemdtltaxamt;

                            db.QL_trnapitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmritemdtl SET mritemdtlstatus = 'Complete' WHERE mritemdtloid = " + dtDtl[i].mritemdtloid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusbeli='FB PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid = " + tblmst.rabmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmritemmst SET mritemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND mritemtype NOT IN ('Jasa') AND mritemmstoid=" + dtDtl[i].mritemmstoid + " AND (SELECT COUNT(*) FROM QL_trnmritemdtl WHERE cmpcode='" + CompnyCode + "' AND mritemdtlstatus='' AND mritemmstoid=" + dtDtl[i].mritemmstoid + " AND mritemdtloid<>" + dtDtl[i].mritemdtloid + ")=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusbeli='FB' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid = " + tblmst.rabmstoid + " AND (SELECT COUNT(*) FROM QL_trnmritemdtl md INNER JOIN QL_trnmritemmst mm ON mm.mritemmstoid = md.mritemmstoid AND mm.mritemtype NOT IN ('Jasa') WHERE mm.cmpcode='" + CompnyCode + "' AND mritemdtlstatus='' AND mm.pomstoid IN (SELECT poitemmstoid from ql_trnpoitemmst where rabmstoid = " + tblmst.rabmstoid + ") AND md.mritemdtloid<>" + dtDtl[i].mritemdtloid + ")=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();                           
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnapitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnapitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnapitemdtl2();
                                    tbldtl2.cmpcode = tblmst.cmpcode;
                                    tbldtl2.apitemdtl2oid = dtl2oid++;
                                    tbldtl2.apitemmstoid = tblmst.apitemmstoid;
                                    tbldtl2.apitemdtl2seq = i + 1;
                                    tbldtl2.fabelioid = dtDtl2[i].fabelioid;
                                    tbldtl2.apitemdtl2amt = dtDtl2[i].apitemdtl2amt;
                                    tbldtl2.createuser = tblmst.upduser;
                                    tbldtl2.createtime = tblmst.updtime;
                                    tbldtl2.upduser = tblmst.upduser;
                                    tbldtl2.updtime = tblmst.updtime;
                                    tbldtl2.apitemdtl2type = apitemdtl2type;

                                    db.QL_trnapitemdtl2.Add(tbldtl2);
                                    db.SaveChanges();

                                    if (tblmst.apitemmststatus == "Post")
                                    {
                                       if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                       else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }

                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnapitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dpfmamt > 0)
                        {
                            //Update Accum DPFM
                            sSql = "UPDATE QL_trndpap SET dpapaccumamt=dpapamt WHERE pomstoid = " + tblmst.poitemmstoid + " AND poreftype = 'QL_trnpoitemmst' AND dpappaytype = 'DPFM' AND ISNULL(dpapres1,'')<>'Batal'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tblmst.apitemmststatus == "Post")
                        {
                            //Update RAB
                            if (tblmst.apitemmstdiscamt > 0 && margin > 0 && persenmargin > 0)
                            {
                                if (tblmst.apitemmstdisctype == "H" || tblmst.apitemmstdisctype == "P")
                                {
                                    sSql = "UPDATE QL_trnrabmst SET rabpersenmargin=" + persenmargin + ", rabtotalmargin=" + margin + ", rabtotaldiscbeliamt=" + tblmst.apitemmstdiscamt + " WHERE rabmstoid=" + tblmst.rabmstoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }   
                            }

                            // Insert QL_conap                        
                            db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnapitemmst", tblmst.apitemmstoid, 0, tblmst.suppoid, iAcctgOidAP, "Post", "APFG", servertime, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tblmst.apitemgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tblmst.apitemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.apitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();           
                                             
                            var glseq = 1;

                            decimal amtDPP = (tblmst.apitemtotalnetto - dAmtOngkir);                         

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "D", amtDPP, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, amtDPP * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                            db.SaveChanges();
                            
                            if (dAmtOngkir > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidOngkir, "D", dAmtOngkir, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, dAmtOngkir * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            decimal totaltax = Convert.ToDecimal(tblmst.apitemtaxamt);
                            if (totaltax > 10)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0, fakturnoGL));
                                db.SaveChanges();
                            }

                            if (dpfmamt > 0)
                            {
                                //glmstoid += 1;
                                //// Insert QL_trnglmst
                                //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.apitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                //db.SaveChanges();

                                // Insert QL_trngldtl
                                // D : Hutang Suspend
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "D", dpfmamt, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, dpfmamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                db.SaveChanges();

                                //// C : DPFM
                                //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidDPFM, "C", dpfmamt, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, dpfmamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                //db.SaveChanges();
                            }

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAP, "C", tblmst.apitemgrandtotal, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, tblmst.apitemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                            db.SaveChanges();

                            decimal totaldisc = Convert.ToDecimal(tblmst.apitemmstdiscamt);
                            if (totaldisc > 0)
                            {
                                if (tblmst.apitemmstdisctype == "H")
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidDisc, "C", totaldisc, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                                else if (tblmst.apitemmstdisctype == "P")
                                {
                                    glmstoid += 1;

                                    // Insert QL_trnglmst
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.apitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                    db.SaveChanges();

                                    var glseq2 = 1;

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq2++, glmstoid, iAcctgOidRec, "D", totaldisc, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                    db.SaveChanges();

                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq2++, glmstoid, iAcctgOidDisc, "C", totaldisc, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.apitemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.apitemmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapitemmst tblmst = db.QL_trnapitemmst.Find(Session["CompnyCode"].ToString(), id);
            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["QL_trnapitemdtl2"+ uid];
            var servertime = ClassFunction.GetServerTime();
            string apitemdtl2type = "QL_trnapitemmst";

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmritemmst SET mritemmststatus = 'Post' WHERE mritemmstoid IN (SELECT mritemmstoid FROM QL_trnapitemdtl where apitemmstoid = " + id + ") AND mritemtype NOT IN ('Jasa')";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmritemdtl SET mritemdtlstatus = '' WHERE mritemdtloid IN (SELECT mritemdtloid FROM QL_trnapitemdtl where apitemmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusbeli='FB PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnapitemmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.apitemmstoid=" + id + " AND apitemtype NOT IN ('Jasa'))";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusbeli='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnapitemmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.apitemmstoid=" + id + " AND pm.apitemtype NOT IN ('Jasa')) AND (SELECT COUNT(*) FROM QL_trnmritemdtl md INNER JOIN QL_trnmritemmst mm ON mm.mritemmstoid = md.mritemmstoid AND mm.mritemtype NOT IN ('Jasa') INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = mm.pomstoid WHERE mm.cmpcode='" + CompnyCode + "' AND mritemdtlstatus='' AND pom.rabmstoid IN (SELECT rabmstoid from QL_trnapitemmst where apitemmstoid <> " + id + "))=0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        
                        if (tblmst.apdpamt > 0)
                        {
                            //Update Accum DPFM
                            sSql = "UPDATE QL_trndpap SET dpapaccumamt=0 WHERE pomstoid = " + tblmst.poitemmstoid + " AND poreftype = 'QL_trnpoitemmst' AND dpappaytype = 'DPFM' AND ISNULL(dpapres1,'')<>'Batal'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tblmst.apitemmstoid && a.apitemdtl2type == apitemdtl2type  && a.cmpcode == tblmst.cmpcode);
                        db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl = db.QL_trnapitemdtl.Where(a => a.apitemmstoid == id);
                        db.QL_trnapitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapitemmst.Remove(tblmst);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    //update fa beli
                                    sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(SELECT COUNT(*) AS jml FROM QL_trnapitemdtl2 apd WHERE apd.fabelioid=QL_mstfabeli.fabelioid) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPTrn.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + CompnyCode + "' AND apm.apitemmstoid IN (" + id + ") AND apm.apitemtype NOT IN ('Jasa')");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "FBReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}