﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class RealPerjalananDinasController : Controller
    {
        private QL_APISEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public RealPerjalananDinasController()
        {
            db = new QL_APISEntities();
            db.Database.CommandTimeout = 0;
        }

        public class trnperdinmst : QL_trnperdinmst { }

        private void InitDDL(QL_trnperdinmst tbl)
        {
            ViewBag.rabno = db.QL_trnrabmst.FirstOrDefault(x => x.rabmstoid == tbl.rabmstoid)?.rabno ?? "";
            ViewBag.perdinrefno = db.QL_trnperdinmst.FirstOrDefault(x => x.perdinid == tbl.perdinrefid)?.perdinno ?? "";
            ViewBag.jabatanid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "JABATAN" && x.gnflag == "ACTIVE"), "gnoid", "gndesc", tbl.jabatanid);
            ViewBag.deptid = new SelectList(db.QL_mstdeptgroup.Where(x => x.activeflag == "ACTIVE"), "groupoid", "groupdesc", tbl.deptid);
            ViewBag.kotaid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "KOTA" && x.gnflag == "ACTIVE"), "gnoid", "gndesc", tbl.kotaid);
            ViewBag.usname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + tbl.pemohon + "'")?.FirstOrDefault() ?? tbl.pemohon;
        }

        public class trnperdindtl : QL_trnperdindtl
        {
            public decimal perdindtlrefamt { get; set; }
            public decimal perdindtlsisaamt { get; set; }
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"select rabmstoid, rabno, format(rabdate, 'dd/MM/yyyy') rabdate, h.projectname, c.custname, rabmstnote from QL_trnrabmst h inner join QL_mstcust c on c.custoid = h.custoid where rabmststatus = 'Approved' order by rabdate desc, rabmstoid desc";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetPerdinData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"select h.perdinid, h.perdinno, format(h.perdindate, 'dd/MM/yyyy') perdindate, h.pemohon, h.obyektif, h.jabatanid, h.deptid, h.kotaid, format(h.tglberangkat, 'dd/MM/yyyy') tglberangkat, format(h.tglpulang, 'dd/MM/yyyy') tglpulang, h.perdinamt, h.perdinmstnote from QL_trnperdinmst h inner join QL_m05GN j on j.gnoid = h.jabatanid inner join QL_mstdept d on d.deptoid = h.deptid inner join QL_m05GN c on c.gnoid = h.kotaid where h.perdinrefid = 0 and h.perdinid not in (select perdinrefid from QL_trnperdinmst)";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetPerdinDtlData(int id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT d.perdindtlseq, perdindtlid perdindtlrefid, klasifikasi, perdindtlamt perdindtlrefamt, 0.0 perdindtlamt, 0.0 perdindtlsisaamt, '' perdindtlnote FROM QL_trnperdindtl d where perdinid = {id} ORDER BY d.perdindtlseq";
                var dtl = db.Database.SqlQuery<trnperdindtl>(sSql).ToList();
                if (dtl == null || dtl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    //var seq = 1;
                    //foreach (var item in dtl) item.perdindtlseq = seq++;
                    js = Json(new { result = "", dtl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            JsonResult js = null;
            try
            {
                sSql = $"select 0 perdindtlseq, 0 perdindtlrefid, '' klasifikasi, 0.0 perdindtlrefamt, 0.0 perdindtlamt, 0.0 perdindtlsisaamt, '' perdindtlnote";
                var tbl = db.Database.SqlQuery<trnperdindtl>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT d.perdindtlseq, d.perdindtlrefid, d.klasifikasi, rd.perdindtlamt perdindtlrefamt, d.perdindtlamt, d.perdindtlamt - ISNULL(rd.perdindtlamt, 0.0) perdindtlsisaamt, d.perdindtlnote FROM QL_trnperdindtl d inner join QL_trnperdindtl rd on rd.perdindtlid = d.perdindtlrefid where d.perdinid = {id} ORDER BY d.perdindtlseq";
                var dtl = db.Database.SqlQuery<trnperdindtl>(sSql).ToList();
                if (dtl == null || dtl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    //var seq = 1;
                    //foreach (var item in dtl) item.perdindtlseq = seq++;
                    js = Json(new { result = "", dtl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        // GET: Partner
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Form", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = $"SELECT * FROM QL_trnperdinmst WHERE 1=1 {sfilter} ORDER BY perdinid DESC";
            var vbag = db.Database.SqlQuery<trnperdinmst>(sSql).ToList();
            return View();
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "RPD/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(perdinno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnperdinmst WHERE perdinno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        // GET: Partner/Form/5
        public ActionResult Form(int? id = 0, int? refid = 0)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Form", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnperdinmst tbl;
            string action = "New Data";
            if (id == null || id == 0)
            {               
                tbl = new QL_trnperdinmst();
                tbl.perdinuid = Guid.NewGuid();
                ViewBag.perdindate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.tglberangkat = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.tglpulang = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                tbl.perdinmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

                var dt_ref = db.QL_trnperdinmst.FirstOrDefault(x => x.perdinid == refid);
                if (dt_ref != null)
                {
                    tbl.perdinrefid = dt_ref.perdinid;
                    ViewBag.tglberangkat = dt_ref.tglberangkat.ToString("dd/MM/yyyy");
                    ViewBag.tglpulang = dt_ref.tglpulang.ToString("dd/MM/yyyy");
                    tbl.pemohon = dt_ref.pemohon;
                    tbl.obyektif = dt_ref.obyektif;
                    tbl.jabatanid = dt_ref.jabatanid;
                    tbl.deptid = dt_ref.deptid;
                    tbl.kotaid = dt_ref.kotaid;
                    tbl.perdinamt = dt_ref.perdinamt;
                }
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnperdinmst.Find(id);
                ViewBag.perdindate = tbl.perdindate.ToString("dd/MM/yyyy");
                ViewBag.tglberangkat = tbl.tglberangkat.ToString("dd/MM/yyyy");
                ViewBag.tglpulang = tbl.tglpulang.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnperdinmst tbl, List<trnperdindtl> dtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Form", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var msg = ""; var result = "failed"; var hdrid = "";

            if (string.IsNullOrEmpty(tbl.perdinno)) tbl.perdinno = generateNo(tbl.perdindate);
            if (tbl.rabmstoid == 0) msg += "Silahkan pilih No. RAB!<br/>";
            if (tbl.perdinrefid == 0) msg += "Silahkan pilih No. Ref Perdin!<br/>";
            else {
                if (action == "New Data")
                {
                    var cek_perdin = db.QL_trnperdinmst.Where(x => x.perdinrefid == tbl.perdinrefid).Count();
                    if (cek_perdin > 0) msg += "No. Ref Perdin Sudah Dibuatkan Realisasi, Silahkan Pilih No. Ref Perdin yg Lain!<br/>";
                }   
            }
            if (string.IsNullOrEmpty(tbl.pemohon)) msg += "Silahkan isi nama pemohon!<br/>";
            if (string.IsNullOrEmpty(tbl.obyektif)) msg += "Silahkan isi obyektif!<br/>";
            if (tbl.jabatanid == 0) msg += "Silahkan pilih jabatan!<br/>";
            if (tbl.deptid == 0) msg += "Silahkan pilih departemen!<br/>";
            if (tbl.kotaid == 0) msg += "Silahkan pilih tujuan!<br/>";
            if (dtl == null) msg += "Silahkan isi data detail!<br/>";
            else
            {
                foreach (var item in dtl)
                {
                    if (string.IsNullOrEmpty(item.klasifikasi)) msg += $"Detail No. {item.perdindtlseq} : silahkan isi klasifikasi!<br/>";
                    //if (item.perdindtlamt <= 0) msg += $"Detail No. {item.perdindtlseq} : jumlah harus lebih dari 0!<br/>";
                }
            }

            if (tbl.perdinmststatus.ToUpper() == "REVISED")
                tbl.perdinmststatus = "In Process";

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString() + "/Form";
            var appoid = ClassFunction.GenerateID("QL_APP2");
            sSql = "SELECT stoid_app FROM QL_m08AS2 a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.usoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid  WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "' AND a.asflag='Level 1'";
            var stoid_app = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (tbl.perdinmststatus == "In Approval")
            {
                if (string.IsNullOrEmpty(stoid_app))
                {
                    msg += "Approval User Level 1 untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            var servertime = ClassFunction.GetServerTime();
            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.perdinmstnote = tbl.perdinmstnote ?? "";
                        if (action == "New Data")
                        {
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnperdinmst.Add(tbl);
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            var trndtl = db.QL_trnperdindtl.Where(a => a.perdinid == tbl.perdinid);
                            db.QL_trnperdindtl.RemoveRange(trndtl);
                        }
                        db.SaveChanges();

                        var datadtl = new List<QL_trnperdindtl>();
                        foreach (var item in dtl)
                        {
                            var new_dtl = (QL_trnperdindtl)ClassFunction.MappingTable(new QL_trnperdindtl(), item);
                            new_dtl.perdinid = tbl.perdinid;
                            if (string.IsNullOrEmpty(item.perdindtlnote)) new_dtl.perdindtlnote = "";
                            new_dtl.upduser = tbl.upduser;
                            new_dtl.updtime = tbl.updtime;
                            datadtl.Add(new_dtl);
                        }
                        db.QL_trnperdindtl.AddRange(datadtl);
                        db.SaveChanges();

                        // Deklarasi data utk approval
                        QL_APP2 tblapp = new QL_APP2();
                        if (tbl.perdinmststatus == "In Approval")
                        {
                            tblapp.cmpcode = CompnyCode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.perdinid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appuser = stoid_app;
                            tblapp.tablename = "QL_trnperdinrealmst";
                            db.QL_APP2.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        hdrid = tbl.perdinid.ToString();
                        msg = "Data Berhasil di Simpan! <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Partner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Form", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var tbl = db.QL_trnperdinmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnperdindtl.Where(a => a.perdinid == id);
                        db.QL_trnperdindtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnperdinmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string generateCode()
        {
            string sCode = "RPD-";
            int formatCounter = 4;
            sSql = $"SELECT ISNULL(MAX(CAST(RIGHT(perdinno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnperdinmst WHERE perdinno LIKE '{sCode}%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sCode += sCounter;
            return sCode;
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Form", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string rptname = "RealPerdin";
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));

            sSql = $"select * from QL_v_print_perdin where perdinid = {ids} order by perdindtlseq";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data");
            report.SetDataSource(dtRpt);

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }
    }
}
