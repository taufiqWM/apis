﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class POJasaReportController : Controller
    {
        // GET: POJasaReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public POJasaReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextPKM)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.suppcode [Kode], c.suppname [Nama], c.suppaddr [Alamat] FROM QL_mstsupp c WHERE c.suppoid IN (SELECT pom.suppoid FROM QL_trnpoitemmst pom LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid=pom.rabmstoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextPKM != "")
                {
                    string[] arr = TextPKM.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.projectname IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += ") ORDER BY c.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetPkmData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, som.rabmstoid [No. Draft], rm.projectname [Nama Project], CONVERT(VARCHAR(10), rm.rabdate, 101) AS [Tgl. RAB], CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], rm.rabmststatus [Status], rm.rabmstnote [Note] FROM QL_trnrabmst rm LEFT JOIN QL_trnsoitemmst som ON som.rabmstoid = rm.rabmstoid  WHERE rm.rabmstoid IN (SELECT pom.rabmstoid from QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON pom.suppoid = s.suppoid AND s.activeflag = 'ACTIVE' AND pom.poitemtype='Jasa'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ) ORDER BY rm.rabdate DESC, som.rabmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPKM");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextPKM)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, pom.poitemmstoid [No. Draft], pom.poitemno [No. PO], Convert(Char(20),pom.poitemdate,103) Tanggal, c.suppname Supplier, pom.poitemmststatus [Status] FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp c ON c.suppoid=pom.suppoid LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid=pom.rabmstoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextPKM != "")
                {
                    string[] arr = TextPKM.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.projectname IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY pom.poitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod, string TextPKM)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, som.soitemmstoid [Draft SO], som.soitemno [SO No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm LEFT JOIN QL_trnpoitemmst pom ON pom.rabmstoid=rm.rabmstoid INNER JOIN QL_mstsupp c ON c.suppoid=pom.suppoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "' AND pom.poitemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextPKM != "")
                {
                    string[] arr = TextPKM.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.projectname IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY som.soitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomor, string TextNomor, string StartPeriod, string EndPeriod, string DDLPeriod, string TextPKM)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.jasacode[Kode], m.jasadesc[Deskripsi], 'Jasa'[Tipe], g.gndesc[Unit] FROM QL_mstjasa m INNER JOIN QL_trnrabdtl rd ON rd.itemoid = m.jasaoid INNER JOIN QL_trnrabmst prm ON prm.cmpcode = rd.cmpcode AND prm.rabmstoid = rd.rabmstoid INNER JOIN QL_m05GN g ON g.gnoid = rd.rabunitoid LEFT JOIN QL_trnpoitemmst pom ON pom.rabmstoid = prm.rabmstoid INNER JOIN QL_mstsupp c ON c.suppoid = pom.suppoid WHERE prm.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (TextPKM != "")
                {
                    string[] arr = TextPKM.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND prm.projectname IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.jasadesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono, string TextPKM)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptPOJasaSumPdf.rpt";
                else
                    rptfile = "rptPOJasaSumPdf.rpt";
                rptname = "PO_JASA_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptPOJasaDtlPdf.rpt";
                else
                {
                    rptfile = "rptPOJasaDtlPdf.rpt";
                }
                rptname = "PO_JASA_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = ", pod.poitemdtloid AS [Dtl Oid], poitemdtlseq [No.], jasacode [Code], jasadesc [Material], ISNULL((soitemno),'') AS [PR No.], ISNULL((soitemdate), CONVERT(DATETIME, '01/01/1900')) [ETA], pom.approvaldatetime [PR Approval Date], '' AS [PR App User], poitemqty [Qty], g2.gndesc [Unit], poitemprice [Price], poitemdtlamt [Detail Amount], poitemdtldisctype [Disc Dtl Type], poitemdtldiscvalue [Disc Dtl Value], poitemdtldiscamt [Disc Dtl Amt], poitemdtlnetto [Detail Netto], poitemdtlnote [Detail Note]";
                Join = " INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode=pom.cmpcode AND pod.poitemmstoid=pom.poitemmstoid INNER JOIN QL_mstjasa m ON m.jasaoid = pod.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid = poitemunitoid ";
            }
            sSql = "SELECT '" + CompnyName + "' AS [Business Unit], pom.cmpcode [CMPCODE], CONVERT(VARCHAR(20), pom.poitemmstoid) [Draft No.], pom.poitemmstoid [ID], poitemtype [Type], poitemdate [Date], poitemno [PO No.], suppname [Supplier], suppcode [Supplier Code], poitemsuppref [Supplier Ref.], currcode [Currency], g1.gndesc [Payment Type], 0.00 [Daily Rate To IDR], 0.00 [Daily Rate To USD], 1 [Monthly Rate To IDR], 1 [Monthly Rate To USD], poitemtotalamt [Total Amt], 0.00 [Total Disc Dtl Amt], poitemtotalnetto [Total Netto], '' [Tax Type], 0.00 [Tax Pct], 0.00 [Tax Amount], poitemgrandtotalamt [Grand Total Amt], poitemmstnote [Header Note], poitemmststatus [Status], '' AS [App User], GETDATE() AS [App Date], pom.createuser AS [Create User], pom.createtime AS [Create Date], pom.closereason, pom.closeuser, pom.closetime " + Dtl + ", ISNULL(som.soitemno,'') soitemno, ISNULL(prm.projectname,'') projectname, ISNULL(mr.mritemdate,1/1/1900) mritemdate, ISNULL(mr.mritemno,'') mritemno, Case ISNULL(prm.rabtype,'') When 'WAPU' Then 'WAPU' Else '-' End WAPU, Case ISNULL(prm.rabtype,'') When 'NON WAPU' Then 'NON WAPU' Else '-' End [NON WAPU], '' AvailableItemSupp, ISNULL(usname,'') Sales, ISNULL(som.soitemtotalnetto,0.0) soitemtotalnetto, ISNULL(som.soitemgrandtotalamt,0.0) soitemgrandtotalamt, ISNULL(som.soitemtotaltaxamt,0.0) soitemtotaltaxamt, ISNULL(cus.custname,'') custname, ISNULL(mr.mritemno,'') mritemno, ISNULL((Select TOP 1 (sod.soitemdtletd) From QL_trnsoitemdtl sod Where sod.soitemmstoid=ISNULL(som.soitemmstoid,0) Order By sod.soitemdtletd DESC),pom.poitemdate) ETD, 0.0 TotalETD FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid INNER JOIN QL_mstcurr c ON c.curroid = pom.curroid INNER JOIN QL_m05GN g1 ON g1.gnoid = poitempaytypeoid LEFT JOIN QL_trnmritemmst mr ON mr.pomstoid=pom.poitemmstoid LEFT JOIN QL_trnrabmst prm ON prm.cmpcode=pom.cmpcode AND prm.rabmstoid=pom.rabmstoid LEFT JOIN QL_trnsoitemmst som ON som.cmpcode = pom.cmpcode AND som.soitemmstoid = pom.somstoid AND som.rabmstoid=pom.rabmstoid LEFT JOIN QL_mstcust cus ON cus.custoid=prm.custoid LEFT JOIN QL_m01US sls ON sls.usoid=som.salesoid " + Join + " WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype='Jasa'";
            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (TextPKM != "")
            {
                string[] arr = TextPKM.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND prm.projectname IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorPO != "")
            {
                string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.jasacode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                } 
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}