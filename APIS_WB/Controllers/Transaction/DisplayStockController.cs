﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class DisplayStockController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public DisplayStockController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listdisplaystmst
        {
            public int displaystoid { get; set; }
            public string custname { get; set; }
            public string displaystlistprice { get; set; }
            public string displaysttype { get; set; }
        }

        public class listdisplaystdtl
        {
            public int seq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' AND custoid NOT IN(SELECT custoid FROM QL_displaystmst) ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listdisplaystdtl> tbl = new List<listdisplaystdtl>();

            sSql = "SELECT 0 seq, itemoid, itemcode, itemdesc FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' AND i.itemtype IN('Barang','Rakitan') ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listdisplaystdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public JsonResult SetDataDetails(List<listdisplaystdtl> dtDtl)
        {
            Session["QL_displaystdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_displaystdtl"] == null)
            {
                Session["QL_displaystdtl"] = new List<listdisplaystdtl>();
            }

            List<listdisplaystdtl> dataDtl = (List<listdisplaystdtl>)Session["QL_displaystdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: Driver
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            sSql = "SELECT displaystoid, gndesc custname, displaystlistprice, displaysttype FROM QL_displaystmst ex INNER JOIN QL_m05GN kt ON kt.gnoid=ex.catoid WHERE ex.cmpcode='" + Session["CompnyCode"].ToString() + "' order by ex.displaystoid ASC";
            List<listdisplaystmst> ExpList = db.Database.SqlQuery<listdisplaystmst>(sSql).ToList();
            return View(ExpList);
        }

        // GET: Driver/Form/5
        //pageload
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_displaystmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_displaystmst();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.custoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

                Session["QL_displaystdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_displaystmst.Find(id);
                List<listdisplaystdtl> dtl = new List<listdisplaystdtl>();
                sSql = "SELECT 0 seq, dd.itemoid, itemcode, itemdesc FROM QL_displaystdtl dd INNER JOIN QL_mstitem i ON i.itemoid=dd.itemoid WHERE displaystoid=" + id + " ORDER BY dd.itemoid";

                dtl = db.Database.SqlQuery<listdisplaystdtl>(sSql).ToList();
                if (dtl.Count > 0)
                {
                    for (int i = 0; i < dtl.Count; i++)
                    {
                        dtl[i].seq = i + 1;
                    }
                }
                Session["QL_displaystdtl"] = dtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_displaystmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();

            List<listdisplaystdtl> dtDtl = (List<listdisplaystdtl>)Session["QL_displaystdtl"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Maaf Tolong isi data detail,,!!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Maaf Tolong isi data detail,,!!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].itemoid == 0)
                        {
                            ModelState.AddModelError("", "Silahkan Isi Barang!!");
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                //var mstoid = ClassFunction.GenerateID("QL_displaystmst");
                //var dtloid = ClassFunction.GenerateID("QL_displaystdtl");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_displaystmst.Add(tbl);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_displaystdtl.Where(a => a.displaystoid == tbl.displaystoid);
                            db.QL_displaystdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                QL_displaystdtl tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = new QL_displaystdtl();
                                    tbldtl.cmpcode = tbl.cmpcode;
                                    tbldtl.displaystoid = tbl.displaystoid;
                                    tbldtl.itemoid = dtDtl[i].itemoid;
                                    tbldtl.upduser = tbl.upduser;
                                    tbldtl.updtime = tbl.updtime;
                                    db.QL_displaystdtl.Add(tbldtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private void InitDDL(QL_displaystmst tbl, string action)
        {
            string sPlus = "";
            if (action == "Create")
            {
                sPlus = " AND gnoid NOT IN(SELECT catoid FROM QL_displaystmst)";
            }
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KATEGORI DISPLAY STOCK' "+ sPlus + " ORDER BY gnoid";
            var gnoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.catoid);
            ViewBag.catoid = gnoid;
        }

        private void FillAdditionalField(QL_displaystmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tbl.custoid + "'").FirstOrDefault();
        }


        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_displaystmst tblmst = db.QL_displaystmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_displaystdtl.Where(a => a.displaystoid == id);
                        db.QL_displaystdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_displaystmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}