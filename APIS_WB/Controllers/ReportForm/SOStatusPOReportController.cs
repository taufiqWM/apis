﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SOStatusPOReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public SOStatusPOReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL1(QL_mstitem tbl1)
        {
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1code", "cat1shortdesc", tbl1.cat1code);
            ViewBag.cat1code = cat1code;

            sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1code='" + tbl1.cat1code + "' AND activeflag='ACTIVE'";
            var cat2code = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2code", "cat2shortdesc", tbl1.cat2code);
            ViewBag.cat2code = cat2code;

            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + CompnyCode + "' AND cat1code= '" + tbl1.cat1code + "'  AND cat2code= '" + tbl1.cat2code + "' AND  activeflag='ACTIVE'";
            var cat3code = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3code", "cat3shortdesc", tbl1.cat3code);
            ViewBag.cat3code = cat3code;

            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "' AND cat1code= '" + tbl1.cat1code + "'  AND cat2code= '" + tbl1.cat2code + "'   AND cat3code= '" + tbl1.cat3code + "' AND activeflag ='ACTIVE'";
            var cat4code = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4shortdesc", tbl1.cat4code);
            ViewBag.cat4code = cat4code;
        }

        public ActionResult GetSalesData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, s.usoid [Username], s.usname [Nama], g.gndesc [Jabatan], s.usaddress [Alamat] FROM QL_m01US s INNER JOIN QL_trnsoitemmst som ON som.salesoid=s.usoid INNER JOIN QL_m05GN g ON gnoid=s.Jabatanoid AND gngroup='JABATAN' Where som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY s.usname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSales");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSuppData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSales)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, suppcode Kode, Supplier, suppnpwpaddr [Alamat] FROM ( SELECT som.cmpcode, c.custcode, c.custname, som.soitemno sono, som.soitemmstoid somstoid, som.salesoid AS Marketing, som.soitemdate transdate, som.soitemmststatus somststatus, rm.rabno FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid ) AS tblmst INNER JOIN ( SELECT pom.somstoid, s.suppcode, s.suppname [Supplier], s.suppnpwpaddr FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = pod.poitemmstoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid INNER JOIN QL_mstitem m ON m.itemoid = pod.itemoid INNER JOIN QL_m05gn g ON g.gnoid=pod.poitemunitoid AND pod.poitemmstoid IN (SELECT poitemmstoid FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid = pod.poitemmstoid AND pom.poitemmststatus IN ('Approved', 'Closed'))) AS podtl ON podtl.somstoid = tblmst.somstoid WHERE tblmst.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND transdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND transdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += "AND tblmst.somststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND tblmst.Marketing IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += "ORDER BY suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSales)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnsoitemmst som WHERE som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetSOData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomorRAB, string TextSales)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, som.soitemmstoid [No. Draft],som.soitemno [No. Sales Order], CONVERT(varchar(10),som.soitemdate,103) [Tgl. SO],c.custname [Customer], som.soitemmststatus [Status], som.soitemmstnote [Note] FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY som.soitemdate DESC, som.soitemmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSales)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, rm.rabmstoid [No. Draft], rm.rabno [No. RAB], CONVERT(VARCHAR(10), rm.rabdate, 101) AS [Tgl. RAB], rm.projectname [Nama Project], CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], rm.rabmststatus [Status], rm.rabmstnote [Note] /*, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereq*/ FROM QL_trnrabmst rm WHERE rm.rabmstoid IN (SELECT som.rabmstoid from QL_trnsoitemmst som INNER JOIN QL_mstcust c ON som.custoid=c.custoid AND c.activeflag='ACTIVE'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " )";
                sSql += " ORDER BY rm.rabdate DESC, rm.rabmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult Getcat2(string[] cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat3(string[] cat1, string[] cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            sSql = "SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 != null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat3code";
            tbl = db.Database.SqlQuery<QL_mstcat3>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat4(string[] cat1, string[] cat2, string[] cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            sSql = "SELECT * FROM QL_mstcat4 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 != null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            if (cat3 != null)
            {
                if (cat3.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3.Count(); i++)
                    {
                        stsval3 += "'" + cat3[i] + "',";
                    }
                    sSql += " AND cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat4code";
            tbl = db.Database.SqlQuery<QL_mstcat4>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomorPO, string TextNomorSO, string TextNomorRAB, string StartPeriod, string EndPeriod, string DDLPeriod, string[] cat1, string[] cat2, string[] cat3, string[] cat4, string TextSales)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=sod.soitemunitoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=sod.soitemmstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid INNER JOIN QL_mstcust c ON C.custoid = som.custoid WHERE som.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomorSO != "")
                {
                    string[] arr = TextNomorSO.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomorPO + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (cat1 != null)
                {
                    if (cat1.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < cat1.Count(); i++)
                        {
                            stsval += "'" + cat1[i] + "',";
                        }
                        sSql += " AND m.cat1code IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (cat2 != null)
                {
                    if (cat2.Count() > 0)
                    {
                        string stsval1 = "";
                        for (int i = 0; i < cat2.Count(); i++)
                        {
                            stsval1 += "'" + cat2[i] + "',";
                        }
                        sSql += " AND m.cat2code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                    }
                }

                if (cat3 != null)
                {
                    if (cat3.Count() > 0)
                    {
                        string stsval2 = "";
                        for (int i = 0; i < cat3.Count(); i++)
                        {
                            stsval2 += "'" + cat3[i] + "',";
                        }
                        sSql += " AND m.cat3code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                    }
                }

                if (cat4 != null)
                {
                    if (cat4.Count() > 0)
                    {
                        string stsval3 = "";
                        for (int i = 0; i < cat4.Count(); i++)
                        {
                            stsval3 += "'" + cat4[i] + "',";
                        }
                        sSql += " AND m.cat4code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                    }
                }

                if (TextSales != "")
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_mstitem tbl1;
            tbl1 = new QL_mstitem();
            if (tbl1 == null)
                return HttpNotFound();
            InitDDL1(tbl1);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorSO, string DDLSoNo, string TextNomorRAB, string[] cat1code, string[] cat2code, string[] cat3code, string[] cat4code, string TextSupp, string TextSales)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            var sPeriode = "";
            var sDDLSono = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusPOSumXls.rpt";
                else
                    rptfile = "rptSOStatusPOSumPdf.rpt";
                rptname = "SOStatus_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusPODtlXls.rpt";
                else
                {
                    rptfile = "rptSOStatusPODtlPdf.rpt";
                }
                rptname = "SOStatus_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                //Detail PO
                Dtl = " , ISNULL([PO No.],'') [PO No.],  ISNULL([PO Qty],0.0) [PO Qty],  ISNULL([PO Price],0.0) [PO Price], ISNULL(Supplier,'') AS Supplier, cat1code, cat2code, cat3code, cat4code, suppcode ";
                Join = " LEFT JOIN ";
                Join += " (SELECT pom.somstoid, pom.poitemno [PO No.], pod.poitemmstoid, pod.poitemdtloid, pod.itemoid, m.itemcode AS matcode, m.itemdesc AS matlongdsc, 0.0 AS POClosingQty, ISNULL(pod.poitemqty,0) AS [PO Qty], ISNULL(pod.poitemprice,0) AS [PO Price], s.suppcode, s.suppname [Supplier] FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = pod.poitemmstoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid INNER JOIN QL_mstitem m ON m.itemoid = pod.itemoid INNER JOIN QL_m05gn g ON g.gnoid=pod.poitemunitoid AND pod.poitemmstoid IN (SELECT poitemmstoid FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid = pod.poitemmstoid AND pom.poitemmststatus IN('Approved', 'Closed'))) AS podtl ON podtl.somstoid = tblmst.somstoid AND podtl.itemoid = tbldtl.itemoid ";
            }
            else
            {
                //Detail Summary PO
                Dtl = " , ISNULL([PO Qty],0.0) [PO Qty], ISNULL([PO Price],0.0) [PO Price]";
                Join = " LEFT JOIN";
                Join += " (SELECT pom.somstoid, pod.itemoid, m.itemcode AS matcode, m.itemdesc AS matlongdsc, 0.0 AS POClosingQty, SUM(ISNULL(pod.poitemqty,0)) AS [PO Qty], ISNULL(pod.poitemprice,0) AS [PO Price] FROM QL_trnpoitemdtl pod INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = pod.poitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = pod.itemoid INNER JOIN QL_m05gn g ON g.gnoid=pod.poitemunitoid AND pod.poitemmstoid IN (SELECT poitemmstoid FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid = pod.poitemmstoid AND pom.poitemmststatus IN ('Approved', 'Closed')) GROUP BY pom.somstoid, pod.itemoid, m.itemcode, m.itemdesc, pod.poitemprice) AS podtl ON podtl.somstoid = tblmst.somstoid AND podtl.itemoid = tbldtl.itemoid";
            }
            sSql = "SELECT tblmst.cmpcode, tblmst.sssflag ,tblmst.groupoid, [Division], divname, tblmst.somstoid, draftno, sono, rabno, sodate,transdate, somststatus, [Customer PO], custoid, custcode, custname, somstnote, approvaluser,  approvaldatetime, createuser, createtime AS createdate  , [Close Reason], [Close Date], [Close User], tbldtl.matcode, matlongdesc, sounit, ISNULL(SOClosingQty,0.0) SOClosingQty, ISNULL(SOQtyAwal,0.0) SOQtyAwal, sodtloid, sodtlnote, 0.0 AS BalanceShipmentQty, 0.0 AS BalancePOQty, soitemcustref, Marketing " + Dtl + " ";
            sSql += " FROM";
            sSql += " (SELECT som.cmpcode, '' sssflag , 0 groupoid, '' [Division], '' AS divname, som.soitemmstoid AS somstoid, (CONVERT(VARCHAR(10), som.soitemmstoid)) AS draftno, som.soitemno AS sono, rm.rabno, som.soitemdate AS sodate, som.updtime AS transdate, CASE WHEN ISNULL(som.soitemmstres1,'')='' THEN som.soitemmststatus ELSE ISNULL(som.soitemmstres1,'') END AS somststatus, som.soitemcustpo [Customer PO], som.custoid, c.custcode, c.custname, som.soitemmstnote AS somstnote, '' approvaluser, getdate() approvaldatetime, som.createuser, som.createtime, som.closereason [Close Reason], som.closetime [Close Date], som.closeuser [Close User], '' AS soitemcustref, som.salesoid AS Marketing FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid WHERE som.soitemtype='' ) AS tblmst";
            sSql += " INNER JOIN";
            sSql += " (SELECT sod.cmpcode, sod.soitemmstoid, sod.soitemdtloid, sod.itemoid, m.itemcode AS matcode, m.itemdesc AS matlongdesc, 0.0 AS SOClosingQty, ISNULL(sod.soitemqty,0) AS SOQtyAwal, sod.soitemdtloid AS sodtloid, g.gndesc AS sounit, sod.soitemdtlnote AS sodtlnote, m.cat1code, m.cat2code, m.cat3code, m.cat4code FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON  m.itemoid=sod.itemoid INNER JOIN QL_m05gn g ON g.gnoid=sod.soitemunitoid) as tbldtl ON tbldtl.cmpcode=tblmst.cmpcode AND tbldtl.soitemmstoid=tblmst.somstoid " + Join + "";
            sSql += " WHERE tblmst.cmpcode LIKE '" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                if (DDLPeriod == "som.updtime")
                {
                    sPeriode = "transdate";
                }
                else
                {
                    sPeriode = "sodate";
                }
                sSql += " AND " + sPeriode + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + sPeriode + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND somststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorSO != "")
            {
                if (DDLSoNo == "som.soitemno")
                {
                    sDDLSono = "sono";
                }
                else
                {
                    sDDLSono = "draftno";
                }
                string[] arr = TextNomorSO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + sDDLSono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorRAB != "")
            {
                string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                } 

                if (cat1code != null)
                {
                    if (cat1code.Count() > 0)
                    {
                        string stsval1 = "";
                        for (int i = 0; i < cat1code.Count(); i++)
                        {
                            stsval1 += "'" + cat1code[i] + "',";
                        }
                        sSql += " AND tbldtl.cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                    }
                }

                if (cat2code != null)
                {
                    if (cat2code.Count() > 0)
                    {
                        string stsval2 = "";
                        for (int i = 0; i < cat2code.Count(); i++)
                        {
                            stsval2 += "'" + cat2code[i] + "',";
                        }
                        sSql += " AND tbldtl.cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                    }
                }

                if (cat3code != null)
                {
                    if (cat3code.Count() > 0)
                    {
                        string stsval3 = "";
                        for (int i = 0; i < cat3code.Count(); i++)
                        {
                            stsval3 += "'" + cat3code[i] + "',";
                        }
                        sSql += " AND tbldtl.cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                    }
                }

                if (cat4code != null)
                {
                    if (cat4code.Count() > 0)
                    {
                        string stsval4 = "";
                        for (int i = 0; i < cat4code.Count(); i++)
                        {
                            stsval4 += "'" + cat4code[i] + "',";
                        }
                        sSql += " AND tbldtl.cat4code IN (" + ClassFunction.Left(stsval4, stsval4.Length - 1) + ")";
                    }
                }

                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND tbldtl.matcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (TextSales != "")
            {
                if (TextSales != null)
                {
                    string[] arr = TextSales.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND tblmst.Marketing IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }                
            }

            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";


            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            for (int C1 = 0; C1 < dtRpt.Rows.Count; C1++)
            {
                dtRpt.Rows[C1]["BalancePOQty"] = (decimal)dtRpt.Rows[C1]["SOQtyAwal"] - (decimal)dtRpt.Compute("SUM([PO Qty])", "sodtloid=" + dtRpt.Rows[C1]["sodtloid"]);
            }
            
            ////For Status SO Closed (In Process Register)            
            //for (int C2 = 0; C2 < dtRpt.Rows.Count; C2++)
            //{
            //    if ((string)dtRpt.Rows[C2]["somststatus"] == "Closed" && (decimal)dtRpt.Rows[C2]["BalancePOQty"] > 0)
            //    {
            //        dtRpt.Rows[C2]["somststatus"] = "Approved";
            //    }
            //}

            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA5;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA5;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}