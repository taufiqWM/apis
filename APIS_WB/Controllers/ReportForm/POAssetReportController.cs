﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class POAssetReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public POAssetReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ReportFilter
        {
            public string ddltype { get; set; }
            public string[] ddlstatus { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filtersupp { get; set; }
            public string filterpo { get; set; }
            public string filterrab { get; set; }
            public string filtermat { get; set; }
        }

        [HttpPost]
        public ActionResult GetModalData(ReportFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                if (modaltype.ToLower() == "supp") sSql = $"SELECT DISTINCT 0 seq, suppcode [Kode], suppname [Nama], suppaddr [Alamat]";
                else if (modaltype.ToLower() == "po") sSql = $"SELECT DISTINCT 0 seq, poassetno [No. PO], poassetmstoid [No. Draft], FORMAT(poassetdate, 'dd/MM/yyyy') [Tgl. PO], poassetmststatus [Status], poassetmstnote [Catatan]";
                else if (modaltype.ToLower() == "mat") sSql = $"SELECT DISTINCT 0 seq, itemcode [Kode], itemdesc [Deskripsi]";

                sSql += $" FROM QL_v_report_po_asset WHERE 1=1";
                sSql += $" AND poassetdate >= CAST('{param.periodstart} 00:00:00' AS DATETIME) AND poassetdate <= CAST('{param.periodend} 23:59:59' AS DATETIME)";
                if (param.ddlstatus != null)
                {
                    if (param.ddlstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < param.ddlstatus.Count(); i++) stsval += "'" + param.ddlstatus[i] + "',";
                        sSql += " AND poassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(param.filtersupp) && modaltype.ToLower() != "supp")
                {
                    string[] arr = param.filtersupp.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (!string.IsNullOrEmpty(param.filterpo) && modaltype.ToLower() != "po")
                {
                    string[] arr = param.filterpo.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND poassetno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (!string.IsNullOrEmpty(param.filtermat) && modaltype.ToLower() != "mat")
                {
                    string[] arr = param.filtermat.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(ReportFilter param, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            var rptfile = $"rptPOAssetDtl{reporttype.Replace("View","Pdf")}.rpt"; var rptname = $"LAPORAN_PO_ASSET_TGL_{ClassFunction.toDate(param.periodstart)} - {ClassFunction.toDate(param.periodend)}";

            sSql = $"SELECT * FROM QL_v_report_po_asset WHERE 1=1";
            if (param.periodstart != "" && param.periodend != "") sSql += $" AND poassetdate >= CAST('{param.periodstart} 00:00:00' AS DATETIME) AND poassetdate <= CAST('{param.periodend} 23:59:59' AS DATETIME)";

            if (param.ddlstatus != null)
            {
                if (param.ddlstatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < param.ddlstatus.Count(); i++) stsval += "'" + param.ddlstatus[i] + "',";
                    sSql += " AND poassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (!string.IsNullOrEmpty(param.filtersupp))
            {
                string[] arr = param.filtersupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                sSql += " AND suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (!string.IsNullOrEmpty(param.filterpo))
            {
                string[] arr = param.filterpo.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                sSql += " AND poassetno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (!string.IsNullOrEmpty(param.filtermat))
            {
                string[] arr = param.filtermat.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                sSql += " AND itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            sSql += " ORDER BY poassetno, poassetdate";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(param.periodstart) + " - " + ClassFunction.toDate(param.periodend));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype.Replace("View", "") == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}