﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class PDPBeliController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public PDPBeliController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usname<>'admin' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            var sVar = "VAR_CASH";
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                sVar = "VAR_BANK";
            else if (tbl.cashbanktype == "BLK")
                sVar = "VAR_DP_AP";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar, tbl.cashbanktype)).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
            sSql = GetQueryBindListCOA("VAR_ADD_COST", tbl.cashbanktype);
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
            ViewBag.addacctgoid1_temp = (tbl.addacctgoid1.ToString() == null ? '0' : tbl.addacctgoid1);
            ViewBag.addacctgoid2_temp = (tbl.addacctgoid2.ToString() == null ? '0' : tbl.addacctgoid2);
            ViewBag.addacctgoid3_temp = (tbl.addacctgoid3.ToString() == null ? '0' : tbl.addacctgoid3);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            if (tbl.cashbanktype == "BLK")
            {
                ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
                ViewBag.paytype = db.Database.SqlQuery<int>("SELECT supppaymentoid FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
                ViewBag.paytype = db.Database.SqlQuery<string>("SELECT gndesc FROM QL_mstsupp s INNER JOIN QL_m05GN g ON gnoid=supppaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
                ViewBag.email = db.Database.SqlQuery<string>("SELECT suppemail FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
                if (tbl.cashbankres2 == "QL_trnapitemmst")
                {
                    ViewBag.dpno = db.Database.SqlQuery<string>("SELECT apitemno FROM QL_trnapitemmst dp WHERE dp.cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.giroacctgoid + "").FirstOrDefault();
                }
                else
                {
                    ViewBag.dpno = db.Database.SqlQuery<string>("SELECT apassetno FROM QL_trnapassetmst dp WHERE dp.cmpcode='" + CompnyCode + "' AND apassetmstoid=" + tbl.giroacctgoid + "").FirstOrDefault();
                }
            }
            else
            {
                ViewBag.suppname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
                ViewBag.paytype = db.Database.SqlQuery<int>("SELECT custpaymentoid FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
                ViewBag.paytype = db.Database.SqlQuery<string>("SELECT gndesc FROM QL_mstcust s INNER JOIN QL_m05GN g ON gnoid=custpaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
                ViewBag.email = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
                if (tbl.cashbankres2 == "QL_trnaritemmst")
                {
                    ViewBag.dpno = db.Database.SqlQuery<string>("SELECT aritemno FROM QL_trnaritemmst dp WHERE dp.cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.giroacctgoid + "").FirstOrDefault();
                }
                else
                {
                    ViewBag.dpno = db.Database.SqlQuery<string>("SELECT arassetno FROM QL_trnarassetmst dp WHERE dp.cmpcode='" + CompnyCode + "' AND arassetmstoid=" + tbl.giroacctgoid + "").FirstOrDefault();
                }
            }
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public string GenerateCashBankNo(string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' AND acctgoid=" + acctgoid;
            cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);

            return cashbankno;
        }

        private string GetQueryBindListCOA(string sVar, string cashbanktype)
        {
            string acctgoid = "";
            if (cashbanktype == "BLK")
            {
                string[] sVar2 = { "VAR_AP", "VAR_AP_JASA", "VAR_AP_DIRECT" };
                acctgoid = ClassFunction.GetDataAcctgOid(sVar2, CompnyCode);
            }
            else
            {
                string[] sVar2 = { "VAR_AR", "VAR_AR_JASA" };
                acctgoid = ClassFunction.GetDataAcctgOid(sVar2, CompnyCode);
            }
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar, string cashbanktype)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            tbl = db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar, cashbanktype)).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string suppnote { get; set; }
            public string suppemail { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(string action, int id, int curroid, string cashbanktype)
        {
            JsonResult js = null;
            try
            {
                List<listsupp> tbl = new List<listsupp>();
                if (cashbanktype == "BLK")
                {
                    sSql = "SELECT suppoid, suppcode, suppname, suppaddr, suppemail, ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),0) AS suppnote FROM QL_mstsupp s WHERE suppoid IN(SELECT DISTINCT con.suppoid FROM QL_trnapitemmst ap INNER JOIN QL_conap con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.apitemmstoid AND con.suppoid=ap.suppoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apitemmststatus='Post' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0)";
                    sSql += " UNION ALL  SELECT suppoid, suppcode, suppname, suppaddr, suppemail, ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),0) AS suppnote FROM QL_mstsupp s WHERE suppoid IN(SELECT DISTINCT con.suppoid FROM QL_trnapassetmst ap INNER JOIN QL_conap con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.apassetmstoid AND con.suppoid=ap.suppoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apassetmststatus='Post' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0)";
                }
                else
                {
                    sSql = "SELECT custoid suppoid, custcode suppcode, custname suppname, custaddr suppaddr, custemail suppemail, ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.custpaymentoid),0) AS suppnote FROM QL_mstcust s WHERE custoid IN(SELECT DISTINCT con.custoid FROM QL_trnaritemmst ap INNER JOIN QL_conar con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.aritemmstoid AND con.custoid=ap.custoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.aritemmststatus='Post' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0)";
                    sSql += " UNION ALL  SELECT custoid suppoid, custcode suppcode, custname suppname, custaddr suppaddr, custemail suppemail, ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.custpaymentoid),0) AS suppnote FROM QL_mstcust s WHERE custoid IN(SELECT DISTINCT con.custoid FROM QL_trnarassetmst ap INNER JOIN QL_conar con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.arassetmstoid AND con.custoid=ap.custoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.arassetmststatus='Post' AND con.reftype='QL_trnarassetmst' AND con.payrefoid=0)";
                }
                tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class listap
        {
            public int refoid { get; set; }
            public string reftype { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public decimal amttrans { get; set; }
            public decimal amtpaid { get; set; }
            public decimal amtbalance { get; set; }
            public int acctgoid { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        [HttpPost]
        public ActionResult GetDPData(int curroid, int suppoid, string cashbanktype)
        {
            JsonResult js = null;
            string sPlus = ""; string sPlus2 = "";
            if (cashbanktype == "BLK")
            {
                sPlus = " /*AND ap.poitemmstoid IN(SELECT pomstoid FROM QL_trndpap WHERE poreftype='QL_trnpoitemmst' AND dpappaytype NOT IN('DPFM') AND dpapstatus IN('Post','Closed') AND (dpapamt-dpapaccumamt) > 0)*/";
                sPlus2 = " /*AND ap.poassetmstoid IN(SELECT pomstoid FROM QL_trndpap WHERE poreftype='QL_trnpoassetmst' AND dpappaytype NOT IN('DPFM') AND dpapstatus IN('Post','Closed') AND (dpapamt-dpapaccumamt) > 0)*/";
            }
            else
            {
                sPlus = " /*AND ap.somstoid IN(SELECT somstoid FROM QL_trndpar WHERE soreftype='QL_trnsoitemmst' AND dparpaytype NOT IN('DPFK') AND dparstatus IN('Post','Closed') AND (dparamt-dparaccumamt) > 0)*/";
                sPlus2 = " /*AND ap.somstoid IN(SELECT somstoid FROM QL_trndpar WHERE soreftype='QL_trnsoassetmst' AND dparpaytype NOT IN('DPFK') AND dparstatus IN('Post','Closed') AND (dparamt-dparaccumamt) > 0)*/";
            }

            try
            {
                List<listap> tbl = new List<listap>();

                sSql = "SELECT *, (amttrans - amtpaid) amtbalance FROM (";
                if (cashbanktype == "BLK")
                {
                    sSql += " SELECT con.refoid, con.reftype, ap.apitemno transno, CONVERT(CHAR(10),con.trnapdate,103) transdate, con.acctgoid, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, ISNULL((SELECT rm.rabmstoid FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON ap.cmpcode=con.cmpcode AND ap.apitemmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0 AND con.refoid>0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus + " ";
                    sSql += " UNION ALL  SELECT con.refoid, con.reftype, ap.apitemno transno, CONVERT(CHAR(10),con.trnapdate,103) transdate, con.acctgoid, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, ISNULL((SELECT rm.rabmstoid FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON ap.cmpcode=con.cmpcode AND ap.apitemmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0 AND con.refoid<0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                    sSql += " UNION ALL  SELECT con.refoid, con.reftype, ap.apassetno transno, CONVERT(CHAR(10),con.trnapdate,103) transdate, con.acctgoid, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, 0 rabmstoid, '' projectname, '' departemen FROM QL_conap con INNER JOIN QL_trnapassetmst ap ON ap.cmpcode=con.cmpcode AND ap.apassetmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus2 + " ";
                }
                else
                {
                    sSql += " SELECT con.refoid, con.reftype, ap.aritemno transno, CONVERT(CHAR(10),con.trnardate,103) transdate, con.acctgoid, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, ISNULL((SELECT rm.rabmstoid FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conar con INNER JOIN QL_trnaritemmst ap ON ap.cmpcode=con.cmpcode AND ap.aritemmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0 AND con.refoid>0 AND con.custoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus + " ";
                    sSql += " UNION ALL  SELECT con.refoid, con.reftype, ap.aritemno transno, CONVERT(CHAR(10),con.trnardate,103) transdate, con.acctgoid, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, ISNULL((SELECT rm.rabmstoid FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conar con INNER JOIN QL_trnaritemmst ap ON ap.cmpcode=con.cmpcode AND ap.aritemmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0 AND con.refoid<0 AND con.custoid=" + suppoid + " AND ap.curroid=" + curroid + " ";
                    sSql += " UNION ALL  SELECT con.refoid, con.reftype, ap.arassetno transno, CONVERT(CHAR(10),con.trnardate,103) transdate, con.acctgoid, con.amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, 0 rabmstoid, '' projectname, '' departemen FROM QL_conar con INNER JOIN QL_trnarassetmst ap ON ap.cmpcode=con.cmpcode AND ap.arassetmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarassetmst' AND con.payrefoid=0 AND con.custoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus2 + " ";
                }
                sSql += ") con WHERE amttrans>amtpaid";
                tbl = db.Database.SqlQuery<listap>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class paydp : QL_trnpaydp
        {
            public string dpno { get; set; }
            public string dpdate { get; set; }
            public decimal paydpbalance { get; set; }
            public string acctgdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int suppoid, int curroid, int refoid, string reftype, string cashbanktype)
        {
            JsonResult js = null;
            try
            {
                List<paydp> tbl = new List<paydp>();
                if (cashbanktype == "BLK")
                {
                    if (reftype == "QL_trnapitemmst")
                    {
                        sSql = "SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpap' dpreftype, dp.dpapoid dprefoid, dp.dpapno dpno, CONVERT(CHAR(10),dp.dpapdate,103) dpdate, (dpapamt-dpapaccumamt) paydpbalance, (dpapamt-dpapaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpap dp INNER JOIN QL_trnapitemmst ap ON ap.poitemmstoid=dp.pomstoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dpapstatus IN('Post','Closed') AND dp.suppoid=" + suppoid + " AND ap.apitemmstoid=" + refoid + " AND dp.poreftype='QL_trnpoitemmst' AND ISNULL(dp.dpapres1,'')='' AND dpappaytype NOT IN('DPFM') AND (dpapamt-dpapaccumamt) > 0  UNION ALL  SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpap' dpreftype, dp.dpapoid dprefoid, dp.dpapno dpno, CONVERT(CHAR(10),dp.dpapdate,103) dpdate, (dpapamt-dpapaccumamt) paydpbalance, (dpapamt-dpapaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dpapstatus IN('Post','Closed') AND dp.suppoid=" + suppoid + " AND ISNULL(dp.dpapres1,'') IN ('Return','Saldo') AND (dpapamt-dpapaccumamt) > 0 ";
                    }
                    else
                    {
                        sSql = "SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpap' dpreftype, dp.dpapoid dprefoid, dp.dpapno dpno, CONVERT(CHAR(10),dp.dpapdate,103) dpdate, (dpapamt-dpapaccumamt) paydpbalance, (dpapamt-dpapaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpap dp INNER JOIN QL_trnapassetmst ap ON ap.poassetmstoid=dp.pomstoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dpapstatus IN('Post','Closed') AND dp.suppoid=" + suppoid + " AND ap.apassetmstoid=" + refoid + " AND dp.poreftype='QL_trnpoassetmst' AND ISNULL(dp.dpapres1,'')='' AND dpappaytype NOT IN('DPFM') AND (dpapamt-dpapaccumamt) > 0  UNION ALL  SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpap' dpreftype, dp.dpapoid dprefoid, dp.dpapno dpno, CONVERT(CHAR(10),dp.dpapdate,103) dpdate, (dpapamt-dpapaccumamt) paydpbalance, (dpapamt-dpapaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dpapstatus IN('Post','Closed') AND dp.suppoid=" + suppoid + " AND ISNULL(dp.dpapres1,'') IN ('Return') AND (dpapamt-dpapaccumamt) > 0 ";
                    }
                }
                else
                {
                    if (reftype == "QL_trnaritemmst")
                    {
                        sSql = "SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpar' dpreftype, dp.dparoid dprefoid, dp.dparno dpno, CONVERT(CHAR(10),dp.dpardate,103) dpdate, (dparamt-dparaccumamt) paydpbalance, (dparamt-dparaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpar dp INNER JOIN QL_trnaritemmst ap ON ap.somstoid=dp.somstoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dparstatus IN('Post','Closed') AND dp.custoid=" + suppoid + " AND ap.aritemmstoid=" + refoid + " AND dp.soreftype='QL_trnsoitemmst' AND ISNULL(dp.dparres1,'')='' AND dparpaytype NOT IN('DPFK') AND (dparamt-dparaccumamt) > 0  UNION ALL  SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpar' dpreftype, dp.dparoid dprefoid, dp.dparno dpno, CONVERT(CHAR(10),dp.dpardate,103) dpdate, (dparamt-dparaccumamt) paydpbalance, (dparamt-dparaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dparstatus IN('Post','Closed') AND dp.custoid=" + suppoid + " AND ISNULL(dp.dparres1,'') IN ('Return','Saldo')  AND (dparamt-dparaccumamt) > 0 ";
                    }
                    else
                    {
                        sSql = "SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpar' dpreftype, dp.dparoid dprefoid, dp.dparno dpno, CONVERT(CHAR(10),dp.dpardate,103) dpdate, (dparamt-dparaccumamt) paydpbalance, (dparamt-dparaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpar dp INNER JOIN QL_trnarassetmst ap ON ap.somstoid=dp.somstoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dparstatus IN('Post','Closed') AND dp.custoid=" + suppoid + " AND ap.arassetmstoid=" + refoid + " AND dp.soreftype='QL_trnsoassetmst' AND ISNULL(dp.dparres1,'')='' AND dparpaytype NOT IN('DPFK') AND (dparamt-dparaccumamt) > 0  UNION ALL  SELECT 0 paydpseq, " + suppoid + " suppoid, " + refoid + " refoid, '" + reftype + "' reftype, 'QL_trndpar' dpreftype, dp.dparoid dprefoid, dp.dparno dpno, CONVERT(CHAR(10),dp.dpardate,103) dpdate, (dparamt-dparaccumamt) paydpbalance, (dparamt-dparaccumamt) paydpamt, '' paydpnote, dp.acctgoid, a.acctgdesc FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.dparstatus IN('Post','Closed') AND dp.custoid=" + suppoid + " AND ISNULL(dp.dparres1,'') IN ('Return')  AND (dparamt-dparaccumamt) > 0 ";
                    }
                }
                tbl = db.Database.SqlQuery<paydp>(sSql).ToList();

                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<paydp>();

            try
            {
                sSql = "DECLARE @oid as INTEGER; DECLARE @cmpcode as VARCHAR(30) SET @oid = " + id + "; SET @cmpcode = '" + CompnyCode + "' ";
                sSql += " SELECT pay.paydpseq, pay.suppoid, pay.reftype, pay.refoid refoid, pay.dpreftype, pay.dprefoid, CASE WHEN pay.dpreftype='QL_trndpap' THEN (SELECT apm.dpapno FROM QL_trndpap apm WHERE apm.dpapoid=pay.dprefoid) ELSE (SELECT apm.dparno FROM QL_trndpar apm WHERE apm.dparoid=pay.dprefoid) END dpno, CASE WHEN pay.dpreftype='QL_trndpap' THEN (SELECT CONVERT(CHAR(10),apm.dpapdate,103) FROM QL_trndpap apm WHERE apm.dpapoid=pay.dprefoid) ELSE (SELECT CONVERT(CHAR(10),apm.dpardate,103) FROM QL_trndpar apm WHERE apm.dparoid=pay.dprefoid) END dpdate, CASE WHEN pay.dpreftype='QL_trndpap' THEN (SELECT (apm.dpapamt - dpapaccumamt) FROM QL_trndpap apm WHERE apm.dpapoid=pay.dprefoid) ELSE (SELECT (apm.dparamt - dparaccumamt) FROM QL_trndpar apm WHERE apm.dparoid=pay.dprefoid) END paydpbalance, pay.paydpamt, pay.paydpamtidr, pay.paydpamtusd, pay.paydpnote, pay.acctgoid AS acctgoid, a.acctgdesc FROM QL_trnpaydp pay INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode";
                dtl = db.Database.SqlQuery<paydp>(sSql).ToList();
                if (dtl.Count <= 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl = dtl.OrderBy(o => o.paydpseq) }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: APPayment
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Pelunasan",["Supplier"] = "NoSupplier/Customer" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cashbankoid, cashbankno, cashbankdate, (CASE cashbanktype WHEN 'BLK' THEN (SELECT s.suppname FROM QL_mstsupp s WHERE s.suppoid=cb.refsuppoid) ELSE (SELECT s.custname FROM QL_mstcust s WHERE s.custoid=cb.refsuppoid) END) suppname, (CASE cashbanktype WHEN 'BLK' THEN 'DP Pembelian' ELSE 'DP Penjualan' END) AS cashbanktype, cashbankstatus, cashbanknote, '' divname, cashbankamt, cb.createuser FROM QL_trncashbankmst cb WHERE cb.cashbankgroup IN('AP','AR') AND ISNULL(cb.cashbankres1,'') IN('DPAP', 'DPAR') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: APPayment/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.cashbankgroup = "";
                tbl.cashbankres1 = "";
                tbl.cashbankres2 = "";
                tbl.cashbankresamt = 0;
                tbl.giroacctgoid = 0;
                tbl.rabmstoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                ViewBag.cashbankdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbanktakegiro = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                ViewBag.cashbankdate = tbl.cashbankdate.ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = tbl.cashbankduedate.ToString("dd/MM/yyyy");
                ViewBag.cashbanktakegiro = tbl.cashbanktakegiro.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, List<paydp> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            if (string.IsNullOrEmpty(tbl.cashbankno)) tbl.cashbankno = "";
            if (string.IsNullOrEmpty(tbl.cashbanknote)) tbl.cashbanknote = "";
            if (string.IsNullOrEmpty(tbl.cashbankrefno)) tbl.cashbankrefno = "";
            if (string.IsNullOrEmpty(tbl.cashbankres1)) tbl.cashbankres1 = "";

            string sErrReply = "";
            if (tbl.giroacctgoid == 0) msg += "- Please select Faktur NO. field!<br>";
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankdpp, ref sErrReply)) msg += "- TOTAL PAYMENT must be less than MAX TOTAL PAYMENT allowed stored in database!<br>";
            if (tbl.cashbankdpp <= 0) msg += "- TOTAL PAYMENT must be more than 0!<br>";
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply)) msg += "- AMOUNT must be less than MAX AMOUNT allowed stored in database!<br>";
            if (tbl.cashbankamt <= 0)
            {
                msg += "- GRAND TOTAL must be more than 0!<br>";
            }
            else
            {
                if (tbl.cashbankamt != tbl.cashbankresamt) msg += "- TOTAL DP Harus Sama Dengan Faktur AMOUNT!<br>";
            }

            if (tbl.addacctgoid1 != 0)
            {
                if (tbl.addacctgamt1 == 0)
                {
                    msg += "- Amount ADD Cost 1 must be more than 0!<br>";
                }
            }
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                {
                    msg += "- Amount ADD Cost 2 must be more than 0!<br>";
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                {
                    msg += "- Amount ADD Cost 3 must be more than 0!<br>";
                }
            }

            //is Input Detail Valid
            if (dtDtl == null)
                msg += "- Silahkan isi detail data !!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Silahkan isi detail data !!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        dtDtl[i].suppoid = tbl.refsuppoid;
                        if (string.IsNullOrEmpty(dtDtl[i].paydpnote)) dtDtl[i].paydpnote = "";
                        if (dtDtl[i].paydpamt <= 0) msg += "- DP Amount " + dtDtl[i].dpno + " Belum Diisi !!<br>";
                        decimal dNewDPBalance = 0;
                        if (tbl.cashbanktype == "BLK")
                        {
                            sSql = "SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapoid=" + dtDtl[i].dprefoid;
                        }
                        else
                        {
                            sSql = "SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparoid=" + dtDtl[i].dprefoid;
                        }
                        dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dtDtl[i].paydpamt > dNewDPBalance) msg += "- No. " + dtDtl[i].dpno + ", DP AMOUNT has been updated by another user. Balance DP must be less than DP AMOUNT!<br>";
                    }
                }
            }

            if (tbl.cashbanktype == "BLK")
            {
                if (tbl.cashbankres2 == "QL_trnapitemmst")
                {
                    sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.00)) AS amtbalance FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON ap.cmpcode=con.cmpcode AND ap.apitemmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0 AND ap.apitemmstoid=" + tbl.giroacctgoid + " ";
                }
                else
                {
                    sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.00)) AS amtbalance FROM QL_conap con INNER JOIN QL_trnapassetmst ap ON ap.cmpcode=con.cmpcode AND ap.apassetmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0 AND ap.apassetmstoid=" + tbl.giroacctgoid + " ";
                }
            }
            else
            {
                if (tbl.cashbankres2 == "QL_trnaritemmst")
                {
                    sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.00)) AS amtbalance FROM QL_conar con INNER JOIN QL_trnaritemmst ar ON ar.cmpcode=con.cmpcode AND ar.aritemmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0 AND ar.aritemmstoid=" + tbl.giroacctgoid + " ";
                }
                else
                {
                    sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.00)) AS amtbalance FROM QL_conar con INNER JOIN QL_trnarassetmst ar ON ar.cmpcode=con.cmpcode AND ar.arassetmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarassetmst' AND con.payrefoid=0 AND ar.arassetmstoid=" + tbl.giroacctgoid + " ";
                }
            }
            decimal amtbalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            if (tbl.cashbankresamt > amtbalance) msg += "- Faktur AMOUNT Melebihi AMOUNT Balance!<br>";

            if (tbl.cashbankstatus == "Post")
            {
                // Interface Validation
                //if (!ClassFunction.IsInterfaceExists("VAR_AP", CompnyCode))
                //    msg += ClassFunction.GetInterfaceWarning("VAR_AP");
                if (!ClassFunction.IsInterfaceExists("VAR_GIRO", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_GIRO");
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AP", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_DP_AP");
            }
            
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.cashbankstatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    msg += cRate.GetRateMonthlyLastError;
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            string sFlagDP = "DPAR"; string sFlagGroup = "AR";
            string dbcrDP = "D"; string dbcrFaktur = "C";
            if (tbl.cashbanktype == "BLK")
            {
                sFlagDP = "DPAP"; sFlagGroup = "AP";
                dbcrDP = "C"; dbcrFaktur = "D";
            }
            tbl.cashbankgroup = sFlagGroup;
            tbl.cashbankres1 = sFlagDP;

            string suppcust = ""; string aparno = "";
            if (tbl.cashbanktype == "BLK")
            {
                suppcust = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid='" + tbl.refsuppoid + "'").FirstOrDefault();
                if (tbl.cashbankres2 == "QL_trnapitemmst")
                {
                    aparno = db.Database.SqlQuery<string>("SELECT apitemno FROM QL_trnapitemmst WHERE apitemmstoid='" + tbl.giroacctgoid + "'").FirstOrDefault();
                }
                else if (tbl.cashbankres2 == "QL_trnapassetmst")
                {
                    aparno = db.Database.SqlQuery<string>("SELECT apassetno FROM QL_trnapassetmst WHERE apassetmstoid='" + tbl.giroacctgoid + "'").FirstOrDefault();
                }
            }
            else
            {
                suppcust = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid='" + tbl.refsuppoid + "'").FirstOrDefault();
                if (tbl.cashbankres2 == "QL_trnaritemmst")
                {
                    aparno = db.Database.SqlQuery<string>("SELECT aritemno FROM QL_trnaritemmst WHERE aritemmstoid='" + tbl.giroacctgoid + "'").FirstOrDefault();
                }
                else if (tbl.cashbankres2 == "QL_trnarassetmst")
                {
                    aparno = db.Database.SqlQuery<string>("SELECT arassetno FROM QL_trnarassetmst WHERE arassetmstoid='" + tbl.giroacctgoid + "'").FirstOrDefault();
                }
            }

            DateTime sDate = tbl.cashbankdate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            DateTime sDateGL = new DateTime();
            if (tbl.cashbanktype == "BLK" || tbl.cashbanktype == "BLM")
            {
                tbl.cashbankduedate = tbl.cashbankdate;
                sDateGL = tbl.cashbankdate;
            }
            else
            {
                sDateGL = tbl.cashbankduedate;
            }
            string sPeriodGL = ClassFunction.GetDateToPeriodAcctg(sDateGL);

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate)) msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            if (tbl.cashbankstatus == "Post")
            {
                cekClosingDate = sDateGL;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate)) msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }

            if (msg == "")
            {
                tbl.cmpcode = CompnyCode;
                tbl.periodacctg = sPeriod;
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbanktaxtype = "";
                tbl.cashbanktaxpct = 0;
                tbl.cashbanktaxamt = 0;
                tbl.cashbankothertaxamt = 0;
                tbl.cashbankaptype = "";
                tbl.cashbankapoid = 0;
                tbl.deptoid = 0;
                tbl.curroid_to = 0;
                tbl.cashbankresamt2 = 0;
                tbl.cashbanksuppaccoid = 0;
                tbl.groupoid = 0;
                tbl.cashbankgiroreal = DateTime.Parse("01/01/1900");
                tbl.cashbanktakegiroreal = DateTime.Parse("01/01/1900");

                var iAcctgOidCB = tbl.acctgoid;

                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpaydp");
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;
                            
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Invoice
                            if (tbl.cashbanktype == "BLK")
                            {
                                if (tbl.cashbankres2 == "QL_trnapitemmst")
                                {
                                    sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (tbl.cashbankres2 == "QL_trnapassetmst")
                                {
                                    sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }

                                //Delete Conap
                                sSql = "DELETE FROM QL_conap WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYDP' AND trnapnote='DPAP' AND payrefoid = " + tbl.cashbankoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                if (tbl.cashbankres2 == "QL_trnaritemmst")
                                {
                                    sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (tbl.cashbankres2 == "QL_trnarassetmst")
                                {
                                    sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }

                                //Delete Conar
                                sSql = "DELETE FROM QL_conar WHERE cmpcode='" + CompnyCode + "' AND trnartype='PAYDP' AND trnarnote='DPAR' AND payrefoid = " + tbl.cashbankoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var trndtl = db.QL_trnpaydp.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpaydp.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpaydp tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnpaydp)ClassFunction.MappingTable(new QL_trnpaydp(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.paydpoid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.paydpseq = i + 1;
                            tbldtl.paydprefno = "";
                            tbldtl.paydpduedate = tbl.cashbankduedate;
                            tbldtl.paydpamtidr = dtDtl[i].paydpamt;
                            tbldtl.paydpamtusd = 0;
                            tbldtl.paydpstatus = "";
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.upduser = tbl.upduser;

                            db.QL_trnpaydp.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.cashbankstatus == "Post")
                            {
                                if (tbl.cashbanktype == "BLK")
                                {
                                    sSql = "UPDATE QL_trndpap SET dpapaccumamt = (dpapaccumamt + " + dtDtl[i].paydpamt + ") WHERE cmpcode = '" + CompnyCode + "' AND dpapoid = " + dtDtl[i].dprefoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    sSql = "UPDATE QL_trndpar SET dparaccumamt = (dparaccumamt + " + dtDtl[i].paydpamt + ") WHERE cmpcode = '" + CompnyCode + "' AND dparoid = " + dtDtl[i].dprefoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpaydp'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankresamt >= amtbalance)
                        {
                            //Update Invoice
                            if (tbl.cashbanktype == "BLK")
                            {
                                if (tbl.cashbankres2 == "QL_trnapitemmst")
                                {
                                    sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (tbl.cashbankres2 == "QL_trnapassetmst")
                                {
                                    sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                if (tbl.cashbankres2 == "QL_trnaritemmst")
                                {
                                    sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (tbl.cashbankres2 == "QL_trnarassetmst")
                                {
                                    sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + tbl.giroacctgoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }

                        if (tbl.cashbankstatus == "Post")
                        {
                            if (tbl.cashbanktype == "BLK")
                            {
                                // Insert QL_conap                        
                                db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, tbl.cashbankres2, tbl.giroacctgoid, tbl.cashbankoid, tbl.refsuppoid, tbl.acctgoid, "Post", "PAYDP", servertime, sPeriod, tbl.acctgoid, tbl.cashbankdate, tbl.cashbankno, 0, tbl.cashbankduedate, 0, tbl.cashbankresamt, "DPAP", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, tbl.cashbankresamt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, ""));
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                // Insert QL_conar                       
                                db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, conaroid++, tbl.cashbankres2, tbl.giroacctgoid, tbl.cashbankoid, tbl.refsuppoid, tbl.acctgoid, "Post", "PAYDP", servertime, sPeriod, tbl.acctgoid, tbl.cashbankdate, tbl.cashbankno, 0, tbl.cashbankduedate, 0, tbl.cashbankresamt, "DPAR", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, tbl.cashbankresamt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, ""));
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (conaroid - 1) + " WHERE tablename='QL_conar'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var glseq = 1; var dAmt = dtDtl.Sum(m => m.paydpamt);
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(sDateGL.ToString("MM/dd/yyyy")), sPeriodGL, "Pelunasan DP|No. " + tbl.cashbankno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, dbcrDP, dtDtl[i].paydpamt, tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote + " | " + dtDtl[i].paydpnote, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].paydpamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                db.SaveChanges();
                            }
                            //Add Cost +
                            if (tbl.addacctgamt1 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "D", tbl.addacctgamt1, tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt1 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt2 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "D", tbl.addacctgamt2, tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt2 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt3 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "D", tbl.addacctgamt3, tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt3 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCB, dbcrFaktur, dAmt, tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                            db.SaveChanges();
                            //Add Cost -
                            if (tbl.addacctgamt1 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "C", (tbl.addacctgamt1 * (-1)), tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt1 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt2 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "C", (tbl.addacctgamt2 * (-1)), tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt2 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgamt3 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "C", (tbl.addacctgamt3 * (-1)), tbl.cashbankno, aparno + " | " + suppcust + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt3 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.cashbankoid.ToString();
                        sReturnNo = "No. " + tbl.cashbankno;
                        if (tbl.cashbankstatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var dtDtl = db.QL_trnpaydp.Where(x => x.cashbankoid == id).ToList();
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            if (dtDtl == null)
                msg = "Silahkan isi detail data !!";
            else if (dtDtl.Count <= 0)
                msg = "Silahkan isi detail data !!";

            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Update Invoice
                        if (tbl.cashbanktype == "BLK")
                        {
                            if (tbl.cashbankres2 == "QL_trnapitemmst")
                            {
                                sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (tbl.cashbankres2 == "QL_trnapassetmst")
                            {
                                sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            //Delete Conap
                            sSql = "DELETE FROM QL_conap WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYDP' AND trnapnote='DPAP' AND payrefoid = " + id + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            if (tbl.cashbankres2 == "QL_trnaritemmst")
                            {
                                sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (tbl.cashbankres2 == "QL_trnarassetmst")
                            {
                                sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            //Delete Conap
                            sSql = "DELETE FROM QL_conar WHERE cmpcode='" + CompnyCode + "' AND trnartype='PAYDP' AND trnarnote='DPAR' AND payrefoid = " + id + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl = db.QL_trnpaydp.Where(a => a.cashbankoid == id);
                        db.QL_trnpaydp.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        // GET: APPayment/PrintReport?id=&cmp=&isbbk=
        public ActionResult PrintReport(int id, bool isbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPaymentDPAll.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", "WHERE cashbankgroup IN('AP','AR') AND ISNULL(cashbankres1,'') IN('DPAP','DPAR') AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid=" + id + "");
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "DPPaymentAllPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }   
    }
}