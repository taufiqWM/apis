﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class BSReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public BSReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;

        }
        
        // GET: PReturnReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("BSReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLCurrency, string ReportType, string DDLType, string DDLMonth, string DDLYear)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            if (!string.IsNullOrEmpty(CompnyCode))
                sSql += " WHERE pretm.cmpcode='" + CompnyCode + "'";

            var rptfile = ""; var rptname = "";
            if (DDLType == "SUMMARY")
                rptfile = "crNeraca.rpt";
            else
                rptfile = "crNeracaDtl.rpt";
            rptname = "BALANCESHEET";

            System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
            var period = new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1).ToString("MMMM yyyy");
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("cmpcode", CompnyCode);
            rptparam.Add("periodacctg", ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)));
            rptparam.Add("speriode", period);
            rptparam.Add("currency", DDLCurrency);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("ReportPath", "Accounting > Report > Balance Sheet > ");
            rptparam.Add("RptFile", rptfile);

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = "";
                this.HttpContext.Session["rptpaper"] = null;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                ClassProcedure.SetDBLogonForReport(report);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

    }
}