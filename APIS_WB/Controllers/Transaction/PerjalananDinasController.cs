﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class PerjalananDinasController : Controller
    {
        private QL_APISEntities db;
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public PerjalananDinasController()
        {
            db = new QL_APISEntities();
            db.Database.CommandTimeout = 0;
        }

        public class trnperdinmst : QL_trnperdinmst { }

        private void InitDDL(QL_trnperdinmst tbl)
        {
            ViewBag.jabatanid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "JABATAN" && x.gnflag == "ACTIVE"), "gnoid", "gndesc", tbl.jabatanid);
            ViewBag.deptid = new SelectList(db.QL_mstdeptgroup.Where(x => x.activeflag == "ACTIVE"), "groupoid", "groupdesc", tbl.deptid);
            ViewBag.kotaid = new SelectList(db.QL_m05GN.Where(x => x.gngroup == "KOTA" && x.gnflag == "ACTIVE"), "gnoid", "gndesc", tbl.kotaid);
            ViewBag.usname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + tbl.pemohon + "'")?.FirstOrDefault() ?? tbl.pemohon;
            ViewBag.jabatan = db.Database.SqlQuery<string>("SELECT g.gndesc FROM QL_m05GN g WHERE g.gnoid='" + tbl.jabatanid + "'")?.FirstOrDefault() ?? "";
        }

        [HttpPost]
        public ActionResult GetUserData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT u.usoid, u.usname, u.Jabatanoid jabatanid, g.gndesc jabatan FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE u.usoid<>'admin' AND u.usflag='ACTIVE' ORDER BY u.usname";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class trnperdindtl : QL_trnperdindtl
        { }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            JsonResult js = null;
            try
            {
                //sSql = $"select 0 perdindtlseq, '' klasifikasi, 0.0 perdindtlamt, '' perdindtlnote";
                //var tbl = db.Database.SqlQuery<trnperdindtl>(sSql).ToList();
                var tbl = new List<trnperdindtl>();
                tbl.Add(new trnperdindtl{
                    perdindtlseq = 1,
                    klasifikasi = "Biaya Penginapan",
                    perdindtlamt = 0,
                    perdindtlnote = ""
                });
                tbl.Add(new trnperdindtl
                {
                    perdindtlseq = 2,
                    klasifikasi = "Biaya Transportasi",
                    perdindtlamt = 0,
                    perdindtlnote = ""
                });
                tbl.Add(new trnperdindtl
                {
                    perdindtlseq = 3,
                    klasifikasi = "Uang Makan / Perdin",
                    perdindtlamt = 0,
                    perdindtlnote = ""
                });
                tbl.Add(new trnperdindtl
                {
                    perdindtlseq = 4,
                    klasifikasi = "Entertainment",
                    perdindtlamt = 0,
                    perdindtlnote = ""
                });
                tbl.Add(new trnperdindtl
                {
                    perdindtlseq = 5,
                    klasifikasi = "Lain-lain",
                    perdindtlamt = 0,
                    perdindtlnote = ""
                });
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT d.perdindtlseq, klasifikasi, perdindtlamt, perdindtlnote FROM QL_trnperdindtl d where perdinid = {id} ORDER BY d.perdindtlseq";
                var dtl = db.Database.SqlQuery<trnperdindtl>(sSql).ToList();
                if (dtl == null || dtl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    //var seq = 1;
                    //foreach (var item in dtl) item.perdindtlseq = seq++;
                    js = Json(new { result = "", dtl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Nomor"] = "No SPD" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select h.perdinid, isnull((select x.perdinid from ql_trnperdinmst x where perdinrefid = h.perdinid), 0) perdinrefid, h.perdinno, isnull((select x.usname from QL_m01US x where x.usoid=h.pemohon),'') pemohon, h.obyektif, h.perdindate, h.tglberangkat, h.tglpulang, isnull((select x.gndesc from QL_m05GN x where x.gnoid=h.jabatanid),'') jabatan, isnull((select x.groupdesc from QL_mstdeptgroup x where x.groupoid=h.deptid),'') dept, isnull((select x.gndesc from QL_m05GN x where x.gnoid=h.kotaid),'') tujuan, isnull((select x.usname from QL_m01US x where x.usoid=h.createuser),'') handler, h.perdinmststatus, CONCAT('Submited On ',FORMAT(h.createtime,'dd/MM/yyyy hh:mm:ss')) createtime, CONCAT('Approved On ',FORMAT(h.approvaldatetime,'dd/MM/yyyy hh:mm:ss')) approvaldatetime, CONCAT('Approved On ',FORMAT(h.approvaldatetime1,'dd/MM/yyyy hh:mm:ss')) approvaldatetime1, isnull((select x.perdinmststatus from QL_trnperdinmst x where x.perdinrefid=h.perdinid),'') realstatus, isnull((select CONCAT('Submited On ',FORMAT(x.createtime,'dd/MM/yyyy hh:mm:ss')) from QL_trnperdinmst x where x.perdinrefid=h.perdinid),'') createreal, isnull((select CONCAT('Approved On ',FORMAT(x.approvaldatetime,'dd/MM/yyyy hh:mm:ss')) from QL_trnperdinmst x where x.perdinrefid=h.perdinid),'') approvereal, isnull((select CONCAT('Approved On ',FORMAT(x.approvaldatetime1,'dd/MM/yyyy hh:mm:ss')) from QL_trnperdinmst x where x.perdinrefid=h.perdinid),'') approvereal1, h.perdinamt FROM QL_trnperdinmst h WHERE isnull(h.perdinrefid,0)=0 ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.perdindate >='" + param.filterperiodfrom + " 00:00:00' AND t.perdindate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Nomor") sSql += " AND t.perdinno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.perdinmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.perdinmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.perdinmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.perdinmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SPD/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(perdinno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnperdinmst WHERE perdinno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        // GET: Partner/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnperdinmst tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_trnperdinmst();
                tbl.perdinuid = Guid.NewGuid();
                ViewBag.perdindate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.tglberangkat = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.tglpulang = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                tbl.perdinmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnperdinmst.Find(id);
                ViewBag.perdindate = tbl.perdindate.ToString("dd/MM/yyyy");
                ViewBag.tglberangkat = tbl.tglberangkat.ToString("dd/MM/yyyy");
                ViewBag.tglpulang = tbl.tglpulang.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnperdinmst tbl, List<trnperdindtl> dtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            var msg = ""; var result = "failed"; var hdrid = "";

            if (string.IsNullOrEmpty(tbl.perdinno)) tbl.perdinno = generateNo(tbl.perdindate);
            if (string.IsNullOrEmpty(tbl.pemohon)) msg += "Silahkan isi nama pemohon!<br/>";
            if (string.IsNullOrEmpty(tbl.obyektif)) msg += "Silahkan isi obyektif!<br/>";
            if (tbl.jabatanid == 0) msg += "Silahkan pilih jabatan!<br/>";
            if (tbl.deptid == 0) msg += "Silahkan pilih departemen!<br/>";
            if (tbl.kotaid == 0) msg += "Silahkan pilih tujuan!<br/>";
            if (dtl == null) msg += "Silahkan isi data detail!<br/>";
            else
            {
                foreach (var item in dtl)
                {
                    if (string.IsNullOrEmpty(item.klasifikasi)) msg += $"Detail No. {item.perdindtlseq} : silahkan isi klasifikasi!<br/>";
                    //if (item.perdindtlamt <= 0) msg += $"Detail No. {item.perdindtlseq} : jumlah harus lebih dari 0!<br/>";
                }
            }

            if (tbl.perdinmststatus.ToUpper() == "REVISED")
                tbl.perdinmststatus = "In Process";

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP2");
            sSql = "SELECT stoid_app FROM QL_m08AS2 a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.usoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid  WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "' AND a.asflag='Level 1'";
            var stoid_app = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (tbl.perdinmststatus == "In Approval")
            {
                if (string.IsNullOrEmpty(stoid_app))
                {
                    msg += "Approval User Level 1 untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            var servertime = ClassFunction.GetServerTime();
            if (string.IsNullOrEmpty(msg))
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.perdinmstnote = tbl.perdinmstnote ?? "";
                        if (action == "New Data")
                        {
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnperdinmst.Add(tbl);
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;

                            var trndtl = db.QL_trnperdindtl.Where(a => a.perdinid == tbl.perdinid);
                            db.QL_trnperdindtl.RemoveRange(trndtl);
                        }
                        db.SaveChanges();

                        var datadtl = new List<QL_trnperdindtl>();
                        foreach (var item in dtl)
                        {
                            var new_dtl = (QL_trnperdindtl)ClassFunction.MappingTable(new QL_trnperdindtl(), item);
                            new_dtl.perdinid = tbl.perdinid;
                            if (string.IsNullOrEmpty(item.perdindtlnote)) new_dtl.perdindtlnote = "";
                            new_dtl.upduser = tbl.upduser;
                            new_dtl.updtime = tbl.updtime;
                            datadtl.Add(new_dtl);
                        }
                        db.QL_trnperdindtl.AddRange(datadtl);
                        db.SaveChanges();

                        // Deklarasi data utk approval
                        QL_APP2 tblapp = new QL_APP2();
                        if (tbl.perdinmststatus == "In Approval")
                        {
                            tblapp.cmpcode = CompnyCode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.perdinid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appuser = stoid_app;
                            tblapp.tablename = "QL_trnperdinmst";
                            db.QL_APP2.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        hdrid = tbl.perdinid.ToString();
                        msg = "Data Berhasil di Simpan! <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: Partner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var tbl = db.QL_trnperdinmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnperdindtl.Where(a => a.perdinid == id);
                        db.QL_trnperdindtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnperdinmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string generateCode()
        {
            string sCode = "SPD-";
            int formatCounter = 4;
            sSql = $"SELECT ISNULL(MAX(CAST(RIGHT(perdinno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnperdinmst WHERE perdinno LIKE '{sCode}%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sCode += sCounter;
            return sCode;
        }

        public ActionResult PrintReport(string ids)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string rptname = "Perdin";
            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), $"rpt{rptname}.rpt"));

            sSql = $"select * from QL_v_print_perdin where perdinid = {ids} order by perdindtlseq";
            var dtRpt = new ClassConnection().GetDataTable(sSql, "data");
            report.SetDataSource(dtRpt);

            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());

            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
            return new FileStreamResult(stream, "application/pdf");
        }
    }
}
