﻿using APIS_WB.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace APIS_WB.Controllers.Other
{
    public class NotificationController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public NotificationController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class dtnotif
        {
            public int jml { get; set; }
            public string mnname { get; set; }
            public string ctrlname { get; set; }
            public string notiftype { get; set; }
            public string notifaction { get; set; }
            public string notiflink { get; set; }
            public string notifclass { get; set; }
            public string notifinfo { get; set; }

        }

        // GET: Notification
        [HttpPost]
        public ActionResult getNotification()
        {

            //var result = "";
            var usoid = Session["UserID"] == null ? "" : Session["UserID"].ToString();

            sSql = "select count(*) jml, mn.mnname, 'WaitingAction' ctrlname, 'Index' notifaction, 'Approval' notiftype,'' notifinfo, map.appform notiflink from QL_APP ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform INNER JOIN QL_m01US us ON us.stoid=ap.appstoid WHERE ISNULL(appstatus,'')='' AND us.usoid = '" + usoid + "' GROUP BY mn.mnname, ap.appform";
            var dtnotif = db.Database.SqlQuery<dtnotif>(sSql).ToList();

            for (int i = 0; i < dtnotif.Count(); i++)
            {
                dtnotif[i].notiflink = Url.Action(dtnotif[i].notifaction + dtnotif[i].notiflink, dtnotif[i].ctrlname);

                if (dtnotif[i].notiftype == "Approval")
                {
                    dtnotif[i].notifclass = "fa fa-envelope-o";
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu Approval ";
                }
                else
                {
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu " + dtnotif[i].notiftype;
                    dtnotif[i].notifclass = "fa fa-bell-o";
                }

            }

            return Json(dtnotif, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getNotification2()
        {

            //var result = "";
            var usoid = Session["UserID"] == null ? "" : Session["UserID"].ToString();

            sSql = "select count(*) jml, mn.mnname, 'WaitingAction2' ctrlname, 'Index' notifaction, 'Approval' notiftype,'' notifinfo, ap.appform notiflink from QL_APP2 ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform INNER JOIN QL_m01US us ON us.usoid=ap.appuser WHERE ISNULL(appstatus,'')='' AND us.usoid = '" + usoid + "' GROUP BY mn.mnname, ap.appform";
            var dtnotif = db.Database.SqlQuery<dtnotif>(sSql).ToList();

            for (int i = 0; i < dtnotif.Count(); i++)
            {
                dtnotif[i].notiflink = Url.Action(dtnotif[i].notifaction + dtnotif[i].notiflink, dtnotif[i].ctrlname);

                if (dtnotif[i].notiftype == "Approval")
                {
                    dtnotif[i].notifclass = "fa fa-envelope-o";
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu Approval ";
                }
                else
                {
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu " + dtnotif[i].notiftype;
                    dtnotif[i].notifclass = "fa fa-bell-o";
                }

            }

            return Json(dtnotif, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            return View();
        }
    }
}