﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class KasBon2ReportController : Controller
    {
        // GET: KasBon2Report
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public KasBon2ReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetSupptData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.suppcode [Kode], c.suppname [Nama], c.suppaddr [Alamat] FROM QL_mstsupp c WHERE c.suppoid IN (SELECT apm.suppoid FROM QL_trnapdirmst apm Where apm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataAPD(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, apm.kasbon2mstoid, apm.kasbon2no [No. Kasbon], CONVERT(CHAR(10),apm.kasbon2date,103) Tanggal, apm.kasbon2mststatus [Status] From QL_trnkasbon2mst apm WHERE apm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND apm.kasbon2mststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                //if (TextSupp != "")
                //{
                //    if (TextSupp != null)
                //    {
                //        string[] arr = TextSupp.Split(';'); string datafilter = "";
                //        for (int i = 0; i < arr.Count(); i++)
                //        {
                //            datafilter += "'" + arr[i] + "',";
                //        }
                //        sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                //    }
                //}

                sSql += " ORDER BY apm.kasbon2no";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        //[HttpPost]
        //public ActionResult GetRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string apdirno, string TextSupp)
        //{
        //    var result = ""; JsonResult js = null;
        //    List<string> tblcols = new List<string>();
        //    List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

        //    try
        //    {
        //        sSql = "SELECT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnapdirmst apm ON apm.rabmstoid=rm.rabmstoid INNER JOIN QL_mstsupp c ON c.suppoid=apm.suppoid WHERE apm.cmpcode='" + CompnyCode + "'";
        //        if (StartPeriod != "" && EndPeriod != "")
        //        {
        //            sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
        //        }

        //        if (DDLStatus != null)
        //        {
        //            if (DDLStatus.Count() > 0)
        //            {
        //                string stsval = "";
        //                for (int i = 0; i < DDLStatus.Count(); i++)
        //                {
        //                    stsval += "'" + DDLStatus[i] + "',";
        //                }
        //                sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
        //            }
        //        }

        //        if (TextSupp != "")
        //        {
        //            if (TextSupp != null)
        //            {
        //                string[] arr = TextSupp.Split(';'); string datafilter = "";
        //                for (int i = 0; i < arr.Count(); i++)
        //                {
        //                    datafilter += "'" + arr[i] + "',";
        //                }
        //                sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
        //            }                    
        //        }

        //        if (apdirno != "")
        //        {
        //            if (apdirno != null)
        //            {
        //                string[] arr = apdirno.Split(';'); string datafilter = "";
        //                for (int i = 0; i < arr.Count(); i++)
        //                {
        //                    datafilter += "'" + arr[i] + "',";
        //                }
        //                sSql += " AND apm.apdirno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
        //            }                    
        //        }

        //        sSql += " ORDER BY apm.apdirno";

        //        DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
        //        if (tbl.Rows.Count > 0)
        //        {
        //            int i = 1;
        //            Dictionary<string, object> row;
        //            foreach (DataRow dr in tbl.Rows)
        //            {
        //                row = new Dictionary<string, object>();
        //                foreach (DataColumn col in tbl.Columns)
        //                {
        //                    var item = dr[col].ToString();
        //                    if (col.ColumnName == "seq")
        //                        item = (i++).ToString();
        //                    row.Add(col.ColumnName, item);
        //                    if (!tblcols.Contains(col.ColumnName))
        //                        tblcols.Add(col.ColumnName);
        //                }
        //                tblrows.Add(row);
        //            }
        //        }
        //        else
        //            result = "Data Not Found.";
        //    }
        //    catch (Exception e)
        //    {
        //        result = e.ToString();
        //    }

        //    js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
        //    js.MaxJsonLength = Int32.MaxValue;
        //    return js;
        //}

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod, string apdirno)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, acc.acctgcode [Kode], acctgdesc [Deskripsi] From QL_trnapdirmst apm INNER JOIN QL_mstsupp sup ON sup.suppoid=apm.suppoid INNER JOIN QL_trnapdirdtl apd ON apm.apdirmstoid=apd.apdirmstoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=apd.acctgoid LEFT JOIN (Select rabmstoid, rabno, projectname from QL_trnrabmst rab ) rab ON rab.rabmstoid=apm.rabmstoid Where apm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    if (TextSupp != null)
                    {
                        string[] arr = TextSupp.Split(';'); string datafilter = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            datafilter += "'" + arr[i] + "',";
                        }
                        sSql += " AND acc.acctgcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                    }
                }

                if (apdirno != "")
                {
                    if (apdirno != null)
                    {
                        string[] arr = apdirno.Split(';'); string datafilter = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            datafilter += "'" + arr[i] + "',";
                        }
                        sSql += " AND apm.apdirno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                    }
                }
                sSql += " ORDER BY acctgcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listddl
        {
            public int ddlvalue { get; set; }
            public string ddltext { get; set; }
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT * FROM( SELECT 0 ddlvalue, 'ALL' ddltext  UNION ALL  SELECT groupoid ddlvalue, groupdesc ddltext FROM QL_mstdeptgroup WHERE activeflag='ACTIVE' )AS tb";
            var deptoid = new SelectList(db.Database.SqlQuery<listddl>(sSql).ToList(), "ddlvalue", "ddltext", null);
            ViewBag.DDLDept = deptoid;

            return View();
        }


        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string kasbon2no, string DDLNomorAP, string TextMaterial, string DDLDept, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptname = ""; var rptfile = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptKasBon2SumXls.rpt";
                }
                else
                {
                    rptfile = "rptKasBon2SumPdf.rpt";
                }  
                rptname = "KasBon2_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptKasBon2DtlXls.rpt";
                }
                else
                {
                    rptfile = "rptKasBon2DtlPdf.rpt";
                }  
                rptname = "KasBon2_DETAIL";
            }

            if (DDLType == "Detail")
            {
                sSql = "SELECT * FROM ( SELECT apm.cmpcode, apm.kasbon2mstoid kasbonmstoid, CASE WHEN kasbon2group='KASBON' THEN apm.kasbon2no ELSE ISNULL((SELECT kmx.kasbon2no FROM QL_trnkasbon2mst kmx WHERE kmx.kasbon2mstoid=apm.kasbon2refoid),'') END kasbonno, apm.kasbon2date kasbondate, us.usname [Sales], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=apm.rabmstoid AND som.soitemtype<>'Jasa'),'') [SO No], a.acctgdesc [Akun], CASE WHEN kasbon2group='KASBON' THEN (kasbon2amt + pphcreditamt) ELSE kasbon2amt END [Invoice], CASE WHEN kasbon2group='KASBON' THEN pphcreditamt ELSE pphdebetamt END [Total PPH], CASE WHEN kasbon2group='KASBON' THEN kasbon2amt ELSE 0 END kasbon, CASE WHEN kasbon2group='REALISASI' THEN kasbon2amt ELSE 0 END realisasi, kasbon2amtselisih [Selisih Amt], CASE WHEN kasbon2group='KASBON' THEN kasbon2amt ELSE (ISNULL((SELECT kmx.kasbon2amt FROM QL_trnkasbon2mst kmx WHERE kmx.kasbon2mstoid=apm.kasbon2refoid) - apm.kasbon2amt + pphcreditamt - apm.kasbon2amtselisih,0.0)) END [Saldo Akhir], apm.kasbon2note [Header Note], s.suppname [Supplier], g.gndesc [Payment Type], ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype='QL_trnkasbon2mst' AND con.refoid=apm.kasbon2mstoid AND con.payrefoid<>0),0.0)  [Paid Amt], apm.kasbon2amt - ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype='QL_trnkasbon2mst' AND con.refoid=apm.kasbon2mstoid AND con.payrefoid<>0),0.0)  [Balance Amt], apm.updtime [Trans Date], apm.kasbon2duedate [Due Date], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=apm.rabmstoid),'') [Project], CASE WHEN kasbon2group='KASBON' THEN apd.kasbon2dtlamt ELSE 0 END kasbondtl, CASE WHEN kasbon2group='REALISASI' THEN apd.kasbon2dtlamt ELSE 0 END realisasidtl, a2.acctgdesc [Detail Akun], CASE WHEN kasbon2group='REALISASI' THEN apm.kasbon2no ELSE '' END realisasino, apm.kasbon2mststatus kasbonstatus, apm.deptoid FROM QL_trnkasbon2mst apm INNER JOIN QL_m01US us ON usoid=apm.personoid INNER JOIN QL_mstacctg a ON a.acctgoid=apm.acctgoid INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_m05GN g ON g.gnoid=apm.kasbon2paytypeoid AND g.gngroup='PAYMENT TERM' INNER JOIN QL_trnkasbon2dtl apd ON apd.kasbon2mstoid=apm.kasbon2mstoid INNER JOIN QL_mstacctg a2 ON a2.acctgoid=apd.acctgoid WHERE apm.cmpcode='" + CompnyCode + "'  UNION ALL  SELECT apm.cmpcode, apm.cashbankoid kasbonmstoid, CASE WHEN isnull(cost_type,'')='KASBON' THEN apm.kasbon_no ELSE ISNULL((SELECT TOP 1 kmx.kasbon_no FROM QL_trncashbankmst kmx WHERE kmx.cashbankoid=isnull(apm.kasbon_ref_id,0)),'') END kasbonno, apm.cashbankdate kasbondate, ISNULL((select x.usname from QL_m01US x where x.usoid=apd.salesoid),'') [Sales], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=apd.rabmstoid AND som.soitemtype<>'Jasa'),'') [SO No], a.acctgdesc [Akun], CASE WHEN isnull(cost_type,'')='KASBON' THEN (apd.cashbankglamt + apd.pphcreditamt) ELSE cashbankdpp END [Invoice], CASE WHEN isnull(cost_type,'')='KASBON' THEN apd.pphcreditamt ELSE apd.pphcreditamt END [Total PPH], CASE WHEN isnull(cost_type,'')='KASBON' THEN apd.cashbankglamt ELSE 0 END kasbon, CASE WHEN isnull(cost_type,'')='KASBON REAL' THEN cashbankdpp ELSE 0 END realisasi, ISNULL((SELECT SUM((x.kasbonrefamt + x.pphamt) - x.cashbankdpp - x.kasbonrefamt2) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0) [Selisih Amt], CASE WHEN isnull(cost_type,'')='KASBON' THEN apd.cashbankglamt ELSE (apd.cashbankglamt + apd.pphcreditamt) - ISNULL((SELECT SUM(x.cashbankdpp) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0) END [Saldo Akhir], apm.cashbanknote [Header Note], ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=isnull(apm.refsuppoid,0)),'') [Supplier], ISNULL((SELECT gx.gndesc FROM QL_mstsupp x inner join QL_m05GN gx on gx.gnoid=x.supppaymentoid WHERE x.suppoid=isnull(apm.refsuppoid,0)),'') [Payment Type], (apd.cashbankglamt + apd.pphcreditamt)  [Paid Amt], (apd.cashbankglamt + apd.pphcreditamt) - ISNULL((SELECT SUM(x.cashbankdpp) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0)  [Balance Amt], apm.updtime [Trans Date], apm.cashbankduedate [Due Date], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=apd.rabmstoid),'') [Project], CASE WHEN isnull(cost_type,'')='KASBON' THEN apd.cashbankglamt ELSE 0 END kasbondtl, CASE WHEN isnull(cost_type,'')='KASBON REAL' THEN apd.cashbankglamt ELSE 0 END realisasidtl, a.acctgdesc [Detail Akun], ISNULL((SELECT TOP 1 x.kasbon_no FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),isnull(apm.kasbon_no,'')) realisasino, apm.cashbankstatus kasbonstatus, apm.deptoid FROM QL_trncashbankmst apm INNER JOIN QL_trncashbankgl apd ON apd.cashbankoid=apm.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid=apd.acctgoid WHERE apm.cmpcode='" + CompnyCode + "' AND isnull(cost_type,'') IN('KASBON','KASBON REAL')) AS tb WHERE 1=1 ";
            }
            else
            {
                sSql = "SELECT * FROM ( SELECT apm.cmpcode, apm.kasbon2mstoid kasbonmstoid, apm.kasbon2no kasbonno, apm.kasbon2date kasbondate, us.usname [Sales], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=apm.rabmstoid AND som.soitemtype<>'Jasa'),'') [SO No], a.acctgdesc [Akun], (kasbon2amt + pphcreditamt) [Invoice], pphcreditamt [Total PPH], ISNULL((SELECT ap2.pphdebetamt FROM QL_trnkasbon2mst ap2 WHERE ap2.kasbon2group='REALISASI' AND ap2.kasbon2refoid=apm.kasbon2mstoid),0.0) [Total PPH2], apm.kasbon2amt kasbon, ISNULL((SELECT ap2.kasbon2amt FROM QL_trnkasbon2mst ap2 WHERE ap2.kasbon2group='REALISASI' AND ap2.kasbon2refoid=apm.kasbon2mstoid),0.0) realisasi, kasbon2amtselisih [Selisih Amt], (kasbon2amt + pphcreditamt) - ISNULL((SELECT ap2.kasbon2amt FROM QL_trnkasbon2mst ap2 WHERE ap2.kasbon2group='REALISASI' AND ap2.kasbon2refoid=apm.kasbon2mstoid),0.0)  [Saldo Akhir], apm.kasbon2note [Header Note], s.suppname [Supplier], g.gndesc [Payment Type], ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype='QL_trnkasbon2mst' AND con.refoid=apm.kasbon2mstoid AND con.payrefoid<>0),0.0)  [Paid Amt], apm.kasbon2amt - ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype='QL_trnkasbon2mst' AND con.refoid=apm.kasbon2mstoid AND con.payrefoid<>0),0.0)  [Balance Amt], apm.updtime [Trans Date], apm.kasbon2duedate [Due Date], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=apm.rabmstoid),'') [Project], ISNULL((SELECT ap2.kasbon2no FROM QL_trnkasbon2mst ap2 WHERE ap2.kasbon2group='REALISASI' AND ap2.kasbon2refoid=apm.kasbon2mstoid),0.0) realisasino, apm.kasbon2mststatus kasbonstatus, apm.deptoid FROM QL_trnkasbon2mst apm INNER JOIN QL_m01US us ON usoid=apm.personoid INNER JOIN QL_mstacctg a ON a.acctgoid=apm.acctgoid INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_m05GN g ON g.gnoid=apm.kasbon2paytypeoid AND g.gngroup='PAYMENT TERM' WHERE apm.cmpcode='" + CompnyCode + "' AND kasbon2group='KASBON'  UNION ALL SELECT apm.cmpcode, apm.cashbankoid kasbonmstoid, apm.kasbon_no kasbonno, apm.cashbankdate kasbondate, ISNULL((select x.usname from QL_m01US x where x.usoid=apd.salesoid),'') [Sales], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=ISNULL(apd.rabmstoid,0) AND som.soitemtype<>'Jasa'),'') [SO No], a.acctgdesc [Akun], (apd.cashbankglamt + apd.pphcreditamt) [Invoice], apd.pphcreditamt [Total PPH], ISNULL((SELECT SUM(x.pphamt) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0) [Total PPH2], apd.cashbankglamt kasbon, ISNULL((SELECT SUM(x.cashbankdpp) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0) realisasi, ISNULL((SELECT SUM((x.kasbonrefamt + x.pphamt) - x.cashbankdpp - x.kasbonrefamt2) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0) [Selisih Amt], (apd.cashbankglamt + apd.pphcreditamt) - ISNULL((SELECT SUM(x.cashbankdpp) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0)  [Saldo Akhir], apm.cashbanknote [Header Note], ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=isnull(apm.refsuppoid,0)),'') [Supplier], ISNULL((SELECT gx.gndesc FROM QL_mstsupp x inner join QL_m05GN gx on gx.gnoid=x.supppaymentoid WHERE x.suppoid=isnull(apm.refsuppoid,0)),'') [Payment Type], (apd.cashbankglamt + apd.pphcreditamt)  [Paid Amt], (apd.cashbankglamt + apd.pphcreditamt) - ISNULL((SELECT SUM(x.cashbankdpp) FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),0.0)  [Balance Amt], apm.updtime [Trans Date], apm.cashbankduedate [Due Date], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=isnull(apd.rabmstoid,0)),'') [Project], ISNULL((SELECT TOP 1 x.kasbon_no FROM QL_trncashbankmst x WHERE isnull(x.cost_type,'')='KASBON REAL' AND isnull(x.kasbon_ref_id,0)=apm.cashbankoid),'') realisasino, apm.cashbankstatus kasbonstatus, apm.deptoid FROM QL_trncashbankmst apm INNER JOIN QL_trncashbankgl apd ON apd.cashbankoid=apm.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid=apd.acctgoid WHERE apm.cmpcode='" + CompnyCode + "' AND ISNULL(apm.cost_type,'')='KASBON' )AS tb WHERE 1=1 ";
            }

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND kasbonstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            //if (TextSupp != "")
            //{
            //    if (TextSupp != null)
            //    {
            //        string[] arr = TextSupp.Split(';'); string filterdata = "";
            //        for (int i = 0; i < arr.Count(); i++)
            //        {
            //            filterdata += "'" + arr[i] + "',";
            //        }
            //        sSql += " AND sup.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            //    }
            //}

            if (kasbon2no != "")
            {
                if (kasbon2no != "")
                {
                    string[] arr = kasbon2no.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomorAP + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (DDLDept != "0")
            {
                sSql += " AND deptoid = " + DDLDept + "";
            }

            sSql += " ORDER BY kasbon, kasbonmstoid ASC";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "" + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                    
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                }  
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}