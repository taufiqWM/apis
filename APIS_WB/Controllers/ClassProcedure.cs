﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;

namespace APIS_WB.Controllers
{
    public class ClassProcedure
    {
        public static string SetDBLogonForReport(ReportDocument reportDoc)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.ServerName= System.Configuration.ConfigurationManager.AppSettings["DB-Server"];
            connectionInfo.DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DB-Name"];
            //connectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["DB-User"];
            //connectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["DB-Password"];
            connectionInfo.IntegratedSecurity = true;

            Tables tables = reportDoc.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
            return "OK";
        }
    }
}