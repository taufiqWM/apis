﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
namespace APIS_WB.Controllers.ReportForm
{
    public class CRARReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public CRARReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL()
        {
                sSql = "SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conar con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE payrefoid=0";
                var DDLCOA = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList(), "acctgoid", "acctgdesc");
                ViewBag.DDLCOA = DDLCOA; 

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }

        [HttpPost]
        public ActionResult GetCustData(string DDLBusinessUnit, string StartDate, string EndDate, string DDLType, string TextCustomer)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, custcode [Code], custname [Name], custaddr [Address] FROM QL_mstcust WHERE (custcode LIKE '%" + "%' OR custname LIKE '%" + "%') AND custoid IN (SELECT custoid FROM QL_conar) ORDER BY custname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetCustOidByCode(string TextCustomer)
        {
            return db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + CAST(custoid AS VARCHAR(10)) FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custcode IN ('" + TextCustomer.Replace(";", "','") + "') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
        }

        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLAccountModel> tbl = new List<ReportModels.DDLAccountModel>();

            try
            {
                if (string.IsNullOrEmpty(CompnyCode))
                {
                    sSql = "SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conar con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE payrefoid=0";
                }
                tbl = db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetFJData(string StartDate, string EndDate, string TextCustomer)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, Convert(Char(20),arm.aritemdate, 103) aritemdate, arm.aritemno, c.custname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON arm.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (StartDate != "" && EndDate != "")
                {
                    sSql += " AND arm.aritemdate >=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND arm.aritemdate <=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                }
                          
                if (TextCustomer != null)
                {
                    string[] arr = TextCustomer.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY arm.aritemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblFJ");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string StartDate, string EndDate, string TextCustomer, string DDLType, string reporttype, bool cbHide, string DDLCOA, string DDLMonth, string DDLYear, string DDLTypeBayar)
        {
           // var Slct = ""; var Join = "";

            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptLaporanPiutangXls.rpt";
                else
                    rptfile = "rptLaporanPiutangPdf.rpt";
                rptname = "LAP_PIUTANG_SUMMARY";
            }
            //else
            //{
            //    if (reporttype == "XLS")
            //        rptfile = "rptUBDtlXls.rpt";
            //    else
            //    {
            //        rptfile = "rptUBDtlPdf.rpt";
            //    }
            //    rptname = "UB_DETAIL";
            //}

            //if (DDLType == "Detail")
            //{
            //    Slct += ", m.itemoid, m.itemcode, m.itemdesc, m.itemtype, g.gndesc, ard.matusageqty QtyUsage, refno, ard.matusagedtlnote ";
            //    Join += " INNER JOIN QL_trnmatusagedtl ard ON ard.matusagemstoid=arm.matusagemstoid INNER JOIN QL_mstitem m ON m.itemoid = ard.matusagerefoid INNER JOIN QL_m05GN g ON g.gnoid = ard.matusageunitoid AND g.gngroup = 'SATUAN' INNER JOIN QL_m05GN g2 ON g2.gnoid = ard.matusagewhoid AND g2.gngroup = 'GUDANG' ";
            //}

            sSql = "SELECT arm.cmpcode, arm.aritemmstoid, arm.custoid, KODE, arm.[NAMA CUSTOMER], arm.JUMLAH, NOMOR, 'Hari' HARI, arm.TANGGAL, arm.NOMINAL, arm.[TGL UPDATE PIUTANG USAHA], arm.[UMUR PIUTANG], Case When [JATUH TEMPO] <= 0 Then [TOTAL PIUTANG] Else 0.00 End [BELUM JATUH TEMPO], Case When [JATUH TEMPO] Between 1 AND 30 Then [TOTAL PIUTANG] Else 0.00 End [1 - 30 HARI], Case When [JATUH TEMPO] Between 31 AND 60 Then [TOTAL PIUTANG] Else 0.00 End [31 - 60 HARI], Case When [JATUH TEMPO] Between 60 AND 90 Then [TOTAL PIUTANG] Else 0.00 End [60 - 90 HARI], Case When [JATUH TEMPO] > 90 Then [TOTAL PIUTANG] Else 0.00 End [ > 90 HARI], Case When [JATUH TEMPO] Between 2 AND 90 Then [TOTAL PIUTANG] Else 0.00 End [TOTAL PIUTANG JT], arm.[TOTAL PIUTANG], [No Pelunasan], [Tgl Pelunasan] FROM ( SELECT c.cmpcode, c.custoid, arm.aritemmstoid, c.custcode [KODE], c.custname [NAMA CUSTOMER], CAST(tp.gnother1 AS integer) JUMLAH, arm.aritemno NOMOR, arm.aritemdate TANGGAL, arm.aritemgrandtotal NOMINAL, Case When arm.aritemgrandtotal-ISNULL((SELECT SUM(amtbayar) FROM QL_conar con WHERE con.refoid=arm.aritemmstoid AND con.custoid=c.custoid AND con.payrefoid<>0 AND con.reftype='QL_trnaritemmst'),0.00)<=0.00 Then ISNULL((SELECT TOP 1 cx.paymentdate FROM QL_conar cx WHERE cx.refoid=con.refoid AND cx.reftype=con.reftype AND cx.payrefoid<>0 ORDER BY cx.updtime DESC),GETDATE()) ELSE GETDATE() END [TGL UPDATE PIUTANG USAHA], /*Case When arm.aritemgrandtotal-ISNULL((SELECT SUM(amtbayar) FROM QL_conar con WHERE con.refoid=arm.aritemmstoid AND con.custoid=c.custoid AND con.payrefoid<>0 AND con.reftype='QL_trnaritemmst'),0.00)>0.00 Then (CASE WHEN DATEDIFF(d,arm.aritemduedate, CURRENT_TIMESTAMP)>0 THEN DATEDIFF(d,arm.aritemduedate, CURRENT_TIMESTAMP) ELSE 0.00 END) Else 0.00 End*/ Case When arm.aritemgrandtotal-ISNULL((SELECT SUM(amtbayar) FROM QL_conar con WHERE con.refoid=arm.aritemmstoid AND con.custoid=c.custoid AND con.payrefoid<>0 AND con.reftype='QL_trnaritemmst'),0.00)<=0.00 Then Datediff(d,arm.aritemdate, ISNULL((SELECT TOP 1 cx.paymentdate FROM QL_conar cx WHERE cx.refoid=con.refoid AND cx.reftype=con.reftype AND cx.payrefoid<>0 ORDER BY cx.updtime DESC),GETDATE())) Else DATEDIFF(d,arm.aritemdate, CURRENT_TIMESTAMP) End [UMUR PIUTANG], arm.aritemgrandtotal-ISNULL((SELECT SUM(amtbayar) FROM QL_conar con WHERE con.refoid=arm.aritemmstoid AND con.custoid=c.custoid AND con.payrefoid<>0 AND con.reftype='QL_trnaritemmst'),0.00) [TOTAL PIUTANG], Case When (arm.aritemgrandtotal-ISNULL((SELECT SUM(amtbayar) FROM QL_conar con WHERE con.refoid=arm.aritemmstoid AND con.custoid=c.custoid AND con.payrefoid<>0 AND con.reftype='QL_trnaritemmst'),0.00)>0) Then (Datediff(d,arm.aritemduedate,CURRENT_TIMESTAMP)) Else 0.00 End [JATUH TEMPO], ISNULL((SELECT TOP 1 CASE WHEN cn.trnartype='PAYAR' THEN ISNULL((SELECT cb.cashbankno FROM QL_trnpayar pay INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid WHERE pay.payaroid=cn.payrefoid),'') ELSE cn.payrefno END  FROM QL_conar cn WHERE cn.reftype='QL_trnaritemmst' AND cn.refoid=arm.aritemmstoid AND cn.payrefoid<>0 ORDER BY cn.updtime DESC),'') [No Pelunasan], ISNULL((SELECT TOP 1 cn.paymentdate FROM QL_conar cn WHERE cn.reftype='QL_trnaritemmst' AND cn.refoid=arm.aritemmstoid AND cn.payrefoid<>0 ORDER BY cn.updtime DESC),'') [Tgl Pelunasan] FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid = arm.custoid INNER JOIN QL_m05GN tp ON tp.gnoid = arm.aritempaytypeoid AND tp.gngroup = 'PAYMENT TERM' INNER JOIN QL_conar con ON con.refoid = arm.aritemmstoid AND con.payrefoid = 0 AND reftype = 'QL_trnaritemmst') arm WHERE arm.cmpcode='" + CompnyCode + "'";

            if (StartDate != "" && EndDate != "")
            {
                //sSql += " AND arm.TANGGAL>=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND arm.TANGGAL<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
                sSql += " AND arm.TANGGAL<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            }
                       
            if (TextCustomer != "")
            {
                string[] arr = TextCustomer.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND KODE IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLTypeBayar != "None")
            {
                if (DDLTypeBayar == "Belum")
                {
                    sSql += " AND arm.[TOTAL PIUTANG] > 0 ";
                }
                else
                {
                    sSql += " AND arm.[TOTAL PIUTANG] <= 0 ";
                }
            }
                    
            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartDate) + " - " + ClassFunction.toDate(EndDate));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            rptparam.Add("CompnyName", CompnyName);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}