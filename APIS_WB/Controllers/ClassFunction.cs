﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.ViewModel;
using APIS_WB.Models.DB;
using System.IO;
using System.Drawing;
using System.ComponentModel;
using System.Data;
using System.Net;
using System.Security.AccessControl;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Reflection;

namespace APIS_WB.Controllers
{
    static class ClassFunction
    {
        public class mstacctg
        {
            public int acctgoid { get; set; }
        }

        public class trngldtl
        {
            public int glseq { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal acctgdebet { get; set; }
            public decimal acctgcredit { get; set; }
        }

        public class dataTableResponse
        {
            public System.Data.DataTable dtResponse { get; set; }
            public string msg { get; set; }
        }

        public static void GetListDataFromDataTable(DataTable dtSource, ref List<string> tblcols, ref List<Dictionary<string, object>> tblrows)
        {
            if (dtSource != null && dtSource.Rows.Count > 0)
            {
                tblcols = new List<string>(); tblrows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dtSource.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtSource.Columns)
                    {
                        var item = dr[col].ToString();
                        row.Add(col.ColumnName, item);
                        if (!tblcols.Contains(col.ColumnName))
                            tblcols.Add(col.ColumnName);
                    }
                    tblrows.Add(row);
                }
            }
        }

        public static bool IsDataColumnValid(DataColumnCollection cols, string[] valid_cols, ref string msg)
        {
            foreach (DataColumn item in cols) item.ColumnName = item.ColumnName.ToLower();
            foreach (var item in valid_cols)
                if (!cols.Contains(item)) msg += (!string.IsNullOrEmpty(msg) ? ", " : "") + item;
            if (!string.IsNullOrEmpty(msg)) msg = $"Column {msg} is not found.";
            return string.IsNullOrEmpty(msg);
        }

        public static async System.Threading.Tasks.Task<dataTableResponse> GetDataFromFileUpload(HttpPostedFileBase file, string uploadpath)
        {
            var result = new dataTableResponse(); result.dtResponse = new DataTable();
            if (file != null && file.ContentLength > 0)
            {
                var sfilename = "FileUpload_" + DateTime.Today.ToString("MMddyyyyHHmmss");
                var sext = Path.GetExtension(file.FileName).ToLower();
                if (sext == ".xls" || sext == ".xlsx")
                {
                    try
                    {
                        ISheet sheet;
                        var stream = file.InputStream;
                        var path = Path.Combine(uploadpath, sfilename + sext);
                        if (!Directory.Exists(uploadpath))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(uploadpath, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                            stream2.Position = 0;
                            if (sext == "xls")
                            {
                                HSSFWorkbook hssfwb = new HSSFWorkbook(stream2); //This will read the Excel 97-2000 formats
                                sheet = hssfwb.GetSheetAt(0);
                            }
                            else
                            {
                                XSSFWorkbook hssfwb = new XSSFWorkbook(stream2); //This will read 2007 Excel format
                                sheet = hssfwb.GetSheetAt(0);
                            }
                            IRow headerRow = sheet.GetRow(0); //Get Header Row
                            int cellCount = headerRow.LastCellNum;
                            for (int j = 0; j < cellCount; j++)
                            {
                                ICell cell = headerRow.GetCell(j, MissingCellPolicy.RETURN_NULL_AND_BLANK);
                                if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                                result.dtResponse.Columns.Add(System.Text.RegularExpressions.Regex.Replace(cell.ToString(), @"\s+", ""));
                            }
                            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                            {
                                IRow row = sheet.GetRow(i);
                                if (row == null) continue;
                                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                                DataRow dr = result.dtResponse.NewRow();
                                for (int j = row.FirstCellNum; j < cellCount; j++)
                                {
                                    ICell cell_x = row.GetCell(j, MissingCellPolicy.RETURN_NULL_AND_BLANK);
                                    try
                                    {
                                        if (!string.IsNullOrEmpty(cell_x.ToString()))
                                            dr[j] = row.GetCell(j).ToString();
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                                result.dtResponse.Rows.Add(dr);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result.msg = "Upload failed : " + ex.ToString();
                    }
                }
                else result.msg = "Please upload Excel (.xls or .xlsx) file only.";
            }
            else result.msg = "Invalid uploaded file.";
            if (string.IsNullOrEmpty(result.msg) && (result == null || result.dtResponse.Rows.Count <= 0)) result.msg = "No data found in uploaded file.";
            return result;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public static bool checkPagePermission(string PagePath, List<QL_m04MN> dtAccess)
        {
            if (HttpContext.Current.Session["UserID"].ToString().ToLower() == "admin")
            {
                return false;
            }
            List<QL_m04MN> newrole = dtAccess.FindAll(o => o.mnfileloc == PagePath);
            return newrole.Count() <= 0;
        }

        public static string toRoman(int number)
        {
            var romanNumerals = new string[][]
            {
            new string[]{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // ones
            new string[]{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // tens
            new string[]{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}, // hundreds
            new string[]{"", "M", "MM", "MMM"} // thousands
            };

            // split integer string into array and reverse array
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;

            // starting with the highest place (for 3046, it would be the thousands
            // place, or 3), get the roman numeral representation for that place
            // and add it to the final roman numeral string
            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][Int32.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }

        public static DateTime GetServerTime()
        {
            QL_APISEntities db = new QL_APISEntities();
            DateTime sDate = db.Database.SqlQuery<DateTime>("SELECT CONVERT(DATETIME,FORMAT(GETDATE(),'yyyy-MM-dd HH:mm:ss')) as tanggal").FirstOrDefault();
            return sDate;
        }

        public static bool CheckDataExists(string sSql)
        {
            bool isExist = false;
            QL_APISEntities db = new QL_APISEntities();
            isExist = db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0;
            return isExist;
        }

        public static string GetMultiUserStatus(string sTable, string sOidField, string sOidValue, string sStatusField, DateTime sUpdTime, string sType)
        {
            string MultiUserStatus = "";
            string sSql = "SELECT COUNT(*) FROM " + sTable + " WHERE " + sOidField + "=" + sOidValue + " AND " + sStatusField + "='In Process' AND (CONVERT(VARCHAR(10), updtime, 101) + ' ' + CONVERT(VARCHAR(10), updtime, 108))<>'" + sUpdTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";
            if (CheckDataExists(sSql))
            {
                MultiUserStatus = "This data has been updated by another user. Please CANCEL this transaction and try again!";
            }
            else
            {
                sSql = "SELECT COUNT(*) FROM " + sTable + " WHERE " + sOidField + "=" + sOidValue + " AND " + sStatusField + "='" + ((sType == "Post") ? sType : "In Approval") +"'";

                if (CheckDataExists(sSql))
                {
                    MultiUserStatus = "This data has been " + ((sType == "Post") ? "posted" : "sent for approval") + " by another user. Please CANCEL this transaction!";
                }
            }
            return MultiUserStatus;
        }

        public static string GenNumberString(int iNumber, int iDefaultCounter)
        {
            int iAdd = iNumber.ToString().Length - iDefaultCounter;
            if (iAdd > 0)
            {
                iDefaultCounter += iAdd;
            }
            string sFormat = "";
            for (int i = 1; i <= iDefaultCounter; i++)
            {
                sFormat += "0";
            }
            return iNumber.ToString(sFormat);
        }

        public static int GenerateID(string sTableName)
        {
            QL_APISEntities db = new QL_APISEntities();
            int oid = 1;
            var mstoid = db.QL_ID.Where(w => w.tablename.ToUpper() == sTableName.ToUpper()).FirstOrDefault();

            if (mstoid != null)
            {
                oid = mstoid.lastoid + 1;
            }
            else
            {
                QL_ID ql_id = new QL_ID();
                ql_id.tablename = sTableName;
                ql_id.lastoid = 1;
                db.QL_ID.Add(ql_id);
                db.SaveChanges();
            }
            return oid;
        }

        public static List<dtFiles> getFileFromDir(UrlHelper Url ,string fileloc, string fileflag, List<dtFiles> dtFiles)
        {
            //fileflag : "set to empty if delete able"
            var sdir = HttpContext.Current.Server.MapPath(fileloc);
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var dtfile = new dtFiles();
                    dtfile.seq = dtFiles.Count() + 1;
                    var sname = file.Split('\\');
                    dtfile.filename = sname[sname.Length -1];
                    dtfile.fileurl = Url.Content(fileloc + "/" + dtfile.filename);
                    dtfile.fileflag = fileflag;
                    dtFiles.Add(dtfile);
                }
            }
            return dtFiles;
        }

        public static Byte[] getTandaTangan(string sPath)
        {
            Byte[] arrImage = null;
            if (File.Exists(sPath))
            {
                Image img = Image.FromFile(sPath);
                MemoryStream ms = new MemoryStream();
                img.Save(ms, img.RawFormat);
                arrImage = ms.GetBuffer();
                ms.Close();
            }
            return arrImage;
        }

        public static string toDate(string sTgl)
        {
            string[] arTgl = sTgl.Split('/');
            try
            {
                return arTgl[1] + "/" + arTgl[0] + "/" + arTgl[2];
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public static String Left(string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length))
            {
                length = input.Length;
            }
            result = input.Substring(0, length);
            return result;
        }

        public static String Mid(string input, int start, int length)
        {
            var result = "";
            if (((input.Length <= 0) || (start >= input.Length))) return result;
            if ((start + length > input.Length))
            {
                length = (input.Length - start);
            }
            result = input.Substring(start, length);
            return result;
        }

        public static String Right(string input, int length)
        {
            var result = "";
            if ((input.Length <= 0)) return result;
            if ((length > input.Length))
            {
                length = input.Length;
            }
            result = input.Substring((input.Length - length), length);
            return result;
        }

        public static string GetLastPeriod(string speriod)
        {
            string periodbefore = "";
            if (speriod != "")
            {
                if (speriod.Length == 6)
                {
                    var iVal_1 = Convert.ToInt32(Left(speriod, 2));
                    if (iVal_1 == 1)
                    {
                        var sVal_2 = (Convert.ToInt32(Left(speriod, 4)) - 1).ToString("0000");
                        periodbefore = sVal_2 + "12";
                    }
                    else
                    {
                        var sVal_3 = Left(speriod, 4);
                        var sVal_4 = (Convert.ToInt32(Right(speriod, 2)) - 1).ToString("00");
                        periodbefore = sVal_3 + sVal_4;
                    }
                }
            }
            return periodbefore;
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static string GetDateToPeriodAcctg(DateTime date)
        {
            return date.ToString("yyyyMM");
        }

        public static string Tchar(string svar)
        {
            return svar.Trim().Replace("'", "''");
        }

        //public static bool IsStockAvailable_SN(string sCmpcode, DateTime sDate, int iMatOid, int iWHOid, Decimal dQty, string sRefno, int iRabOid, string sSN)
        //{
        //    bool isExist = false;
        //    string sSql = "SELECT COUNT(-1) FROM QL_conmat_sn con WHERE con.cmpcode='" + sCmpcode + "' AND con.refoid=" + iMatOid + " AND ISNULL(con.refno,'')='" + sRefno + "' AND ISNULL(con.serialnumber,'')='" + sSN + "' AND con.updtime<='" + sDate.ToString("MM/dd/yyyy") + " 23:59:59' GROUP BY con.refoid, ISNULL(con.refno,''), ISNULL(con.serialnumber,'') HAVING (SUM(qtyin-qtyout)) >= " + dQty + "";
        //    QL_APISEntities db = new QL_APISEntities();
        //    isExist = db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0;
        //    return isExist;
        //}

        public static bool IsStockAvailable(string sCmpcode, DateTime sDate, int iMatOid, int iWHOid, Decimal dQty, string sRefno, int iRabOid, string sSN)
        {
            bool isExist = false;
            string sSql = "SELECT COUNT(-1) FROM QL_conmat con WHERE con.cmpcode='" + sCmpcode + "' AND con.refoid=" + iMatOid + " AND con.mtrwhoid=" + iWHOid + " AND ISNULL(con.refno,'')='" + sRefno + "' AND ISNULL(con.rabmstoid,0)=" + iRabOid + " AND ISNULL(con.serialnumber,'')='" + sSN + "' AND con.updtime<='" + sDate.ToString("MM/dd/yyyy") + " 23:59:59' GROUP BY con.refoid, con.mtrwhoid, ISNULL(con.refno,''), ISNULL(con.serialnumber,'') HAVING (SUM(qtyin-qtyout) - ISNULL((SELECT SUM(qtyout) FROM QL_matbooking mb WHERE mb.refoid=con.refoid AND mb.mtrwhoid=con.mtrwhoid AND ISNULL(mb.refno,'')=ISNULL(con.refno,'') AND ISNULL(mb.serialnumber,'')=ISNULL(con.serialnumber,'') AND mb.flag='Post'),0.0)) >= " + dQty + "";
            QL_APISEntities db = new QL_APISEntities();
            isExist = db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0;
            return isExist;
        }

        public static bool IsStockBookingAvailable(string sCmpcode, DateTime sDate, int iMatOid, int iWHOid, Decimal dQty, string sRefno, int iRabOid, string sSN)
        {
            bool isExist = false;
            string sSql = "SELECT COUNT(-1) FROM QL_conmat con WHERE con.cmpcode='" + sCmpcode + "' AND con.refoid=" + iMatOid + " AND con.mtrwhoid=" + iWHOid + " AND ISNULL(con.refno,'')='" + sRefno + "' AND ISNULL(con.rabmstoid,0)=" + iRabOid + " AND ISNULL(con.serialnumber,'')='" + sSN + "' AND con.updtime<='" + sDate.ToString("MM/dd/yyyy") + " 23:59:59' GROUP BY con.refoid, con.mtrwhoid, ISNULL(con.refno,''), ISNULL(con.serialnumber,'') HAVING (SUM(qtyin-qtyout)) >= " + dQty + "";
            QL_APISEntities db = new QL_APISEntities();
            isExist = db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0;
            return isExist;
        }

        public static decimal GetStockValue(string sCmpcode, int iMatOid)
        {
            decimal value = 0;
            string sSql = "SELECT CAST(ISNULL((SELECT TOP 1 valueidr / (qtyin+qtyout) FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + " ORDER BY updtime DESC),0.0) AS DECIMAL(18,4)) ";
            QL_APISEntities db = new QL_APISEntities();
            if (db.Database.SqlQuery<decimal>(sSql).FirstOrDefault() > 0)
            {
                value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            }
            return value;
        }

        public static decimal GetAvgStockValue(string sCmpcode, int iMatOid, Decimal dQty, Decimal dVal, string flag)
        {
            decimal value = 0; decimal stockAkhir = 0; decimal valueAkhir = 0;
            if (flag == "IN")
            {
                string sSql = "SELECT ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + "),0.0) ";
                QL_APISEntities db = new QL_APISEntities();
                if (db.Database.SqlQuery<decimal>(sSql).FirstOrDefault() > 0)
                {
                    stockAkhir = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                string sSql2 = "SELECT CAST(ISNULL((SELECT TOP 1 CASE WHEN valueidr = 0 THEN 0 ELSE (valueidr / (qtyin+qtyout)) END FROM QL_conmat WHERE cmpcode='" + sCmpcode + "' AND refoid=" + iMatOid + " ORDER BY updtime DESC),0.0) AS DECIMAL(18,4)) ";
                QL_APISEntities db2 = new QL_APISEntities();
                if (db2.Database.SqlQuery<decimal>(sSql2).FirstOrDefault() > 0)
                {
                    valueAkhir = db2.Database.SqlQuery<decimal>(sSql2).FirstOrDefault();
                }
                //if (stockAkhir > 0 & valueAkhir > 0 & dQty > 0 & dVal > 0)
                if (stockAkhir > 0)
                {
                    value = ((stockAkhir * valueAkhir) + (dQty * dVal)) / (stockAkhir + dQty);
                }
                else if (stockAkhir == 0)
                {
                    value = dVal;
                }
            }
            else
            {
                value = dVal;
            }                      
            return (value * dQty);
        }

        public static Boolean isLengthAccepted(string sField, string sTable, decimal dValue, ref string sErrReply)
        {
            int iPrec = 0, iScale = 0;
            using (var conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnString"]))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT col.precision, col.scale FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name='" + sField + "' AND ta.name='" + sTable + "'";
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            iPrec = reader.GetOrdinal("precision");
                            iScale = reader.GetOrdinal("scale");
                        }
                        reader.Close();
                    }
                }
                catch
                {
                    iPrec = 0; iScale = 0;
                    sErrReply = "- Can't check the result. Please check that the parameter has been assigned correctly!";
                    return false;
                }
                conn.Close();
            }
            if (iPrec > 0 && iScale > 0)
            {
                string sTextValue = "";
                for (var i = 0; i < (iPrec - iScale - 1); i++)
                {
                    sTextValue += "9";
                }
                if (iScale > 0)
                {
                    sTextValue += ".";
                    for (var i = 0; i < iScale - 1; i++)
                    {
                        sTextValue += "9";
                    }
                }
                if (dValue > Convert.ToDecimal(sTextValue))
                {
                    sErrReply = string.Format("{0:#,0.00}", Convert.ToDecimal(sTextValue));
                    return false;
                }
            }
            return true;
        }

        public static Boolean IsInterfaceExists(string sVar, string cmp)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_APISEntities db = new QL_APISEntities();
            var sSql = ""; var flag = false;
            sSql = "SELECT COUNT(-1) FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND interfacevar='" + sVar + "' AND interfaceres1='" + cmp + "'";
            var iCountVar = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (iCountVar > 0)
            {
                flag = true;
            }
            else
            {
                sSql = "SELECT COUNT(-1) FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND interfacevar='" + sVar + "' AND interfaceres1='All'";
                var iCountVarAll = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                if (iCountVar > 0)
                {
                    flag = true;
                }
            }
            return flag;
        }

        public static string GetInterfaceWarning(string sVar)
        {
            return "Please Define some COA for Variable " + sVar + " in Accounting -> Master -> Interface before continue this form!";
        }

        public static string GetVarInterface(string sVar, string cmp)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_APISEntities db = new QL_APISEntities();
            var sSql = "";
            sSql = "SELECT interfacevalue FROM QL_mstinterface WHERE interfacevar='" + sVar + "' AND cmpcode='" + CompnyCode + "' AND interfaceres1='" + cmp + "'";
            var sAcctgCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            return sAcctgCode;
        }

        public static int GetAcctgOID(string sVar)
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_APISEntities db = new QL_APISEntities();
            var sSql = "";
            sSql = "SELECT TOP 1 acctgoid FROM QL_mstacctg WHERE acctgcode='" + sVar + "' and cmpcode='" + CompnyCode + "'";
            var iAcctgOid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            return iAcctgOid;
        }

        public static string GetVarInterface(string sCmpcode, string[] sVar)
        {
            string sSql = ""; string sCode = "";
            for (int i = 0; i < sVar.Length; i++)
            {
                if (i > 0)
                {
                    sCode += ",";
                }
                string sSql2 = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + sCmpcode + "' AND interfacevar='" + sVar[i] + "'";
                QL_APISEntities db2 = new QL_APISEntities();
                if (db2.Database.SqlQuery<int>(sSql2).FirstOrDefault() > 0)
                {
                    sCode += db2.Database.SqlQuery<string>(sSql2).FirstOrDefault();
                }
            }
            sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" + sCmpcode + "' AND a.activeflag='ACTIVE' AND (";
            string[] sCode2 = sCode.Split(',');
            for (int i = 0; i < sCode2.Length - 1; i++)
            {
                sSql += "a.acctgcode LIKE '" + sCode2[i] + "%'";
                if (i < sCode2.Length - 1) {
                    sSql += " OR ";
                }                      
            }
            sSql += ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc";
            return sSql;
        }

        public static Boolean isPeriodAcctgClosed(string cmpcode, DateTime sDate)
        {
            QL_APISEntities db = new QL_APISEntities();
            string sPeriod = GetDateToPeriodAcctg(sDate);
            string sSql = "SELECT COUNT(*) FROM QL_crdgl WHERE cmpcode='" + cmpcode + "' AND crdglflag='CLOSED' AND periodacctg='" + sPeriod + "'";
            if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                return true;
            else
                return false;
        }

        public static QL_trnglmst InsertGLMst(string cmpcode, int glmstoid, DateTime gldate, string periodacctg, string glnote, string glflag, DateTime postdate, string createuser, DateTime createtime, string upduser, DateTime updtime, int rateoid, int rate2oid, decimal glrateidr, decimal glrate2idr, decimal glrateusd, decimal glrate2usd, string gl1side = "")
        {
            QL_trnglmst tbl = new QL_trnglmst();

            tbl.cmpcode = cmpcode;
            tbl.glmstoid = glmstoid;
            tbl.gldate = gldate;
            tbl.periodacctg = periodacctg;
            tbl.glnote = (glnote == null ? "" : glnote);
            tbl.glflag = glflag;
            tbl.postdate = postdate;
            tbl.createuser = createuser;
            tbl.createtime = createtime;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.type = "";
            tbl.rateoid = rateoid;
            tbl.rate2oid = rate2oid;
            tbl.glrateidr = glrateidr;
            tbl.glrateusd = glrateusd;
            tbl.glrate2idr = glrate2idr;
            tbl.glrate2usd = glrate2usd;
            tbl.glres = "";
            tbl.gl1side = gl1side;

            return tbl;
        }

        public static QL_trngldtl InsertGLDtl(string cmpcode, int gldtloid, int glseq, int glmstoid, int acctgoid, string gldbcr, decimal glamt, string noref, string glnote, string glflag, string upduser, DateTime updtime, decimal glamtidr, decimal glamtusd, string glother1, string glother2, string glother3, string glother4, int groupoid, string fakturno = "")
        {
            QL_trngldtl tbldtl = new QL_trngldtl();

            tbldtl.cmpcode = cmpcode;
            tbldtl.gldtloid = gldtloid;
            tbldtl.glmstoid = glmstoid;
            tbldtl.glseq = glseq;
            tbldtl.acctgoid = acctgoid;
            tbldtl.gldbcr = gldbcr;
            tbldtl.glamt = glamt;
            tbldtl.noref = (noref == null ? "" : noref);
            tbldtl.glnote = (glnote == null ? "" : glnote);
            tbldtl.glother1 = glother1;
            tbldtl.glother2 = glother2;
            tbldtl.glother3 = glother3;
            tbldtl.glother4 = glother4;
            tbldtl.glflag = glflag;
            tbldtl.upduser = upduser;
            tbldtl.updtime = updtime;
            tbldtl.glamtidr = glamtidr;
            tbldtl.glamtusd = glamtusd;
            tbldtl.groupoid = Convert.ToInt32(groupoid);
            tbldtl.fakturno = fakturno;

            return tbldtl;
        }

        public static List<trngldtl> ShowCOAPosting(string sNoRef, string sCmpCode, string sRateType = "Default", string sOther = "")
        {
            QL_APISEntities db = new QL_APISEntities();
            List<trngldtl> tbl = new List<trngldtl>();
            var sSql = "";
            var sFieldAmt = "glamt";
            if (sRateType == "IDR")
                sFieldAmt = "glamtidr";
            else if (sRateType == "USD")
                sFieldAmt = "glamtusd";

            sSql = "SELECT * FROM (SELECT d.glseq, a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref='" + sNoRef + "' AND d.cmpcode='" + sCmpCode + "' AND d." + sFieldAmt + ">0 AND ISNULL(d.glother1, '')='" + sOther + "') tbl_posting ORDER BY glseq";
            tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();

            if (tbl == null)
            {
                sSql = "SELECT * FROM (SELECT d.glseq, a.acctgcode, a.acctgdesc, (CASE d.gldbcr WHEN 'D' THEN d." + sFieldAmt + " ELSE 0 END) AS acctgdebet, (CASE d.gldbcr WHEN 'C' THEN d." + sFieldAmt + " ELSE 0 END) AS accgtcredit FROM QL_trngldtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.noref LIKE '%" + sNoRef + "' AND d.cmpcode='" + sCmpCode + "' AND d." + sFieldAmt + ">0 AND ISNULL(d.glother1, '')='" + sOther + "') tbl_posting ORDER BY glseq";
                tbl = db.Database.SqlQuery<trngldtl>(sSql).ToList();
            }
            return tbl;
        }

        public static decimal GetStockValueIDR(string cmpcode, string periodacctg, string refname, int refoid)
        {
            return new QL_APISEntities().Database.SqlQuery<decimal>("SELECT ISNULL((SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueidr, 0)) / NULLIF(SUM(ISNULL(stockqty, 0)), 0)), 0) FROM QL_stockvalue WHERE cmpcode='" + cmpcode + "' AND periodacctg IN ('" + periodacctg + "', '" + GetLastPeriod(periodacctg) + "') AND refoid=" + refoid + " AND refname='" + refname + "' AND closeflag=''").FirstOrDefault();
        }

        public static decimal GetStockValueUSD(string cmpcode, string periodacctg, string refname, int refoid)
        {
            return new QL_APISEntities().Database.SqlQuery<decimal>("SELECT ISNULL((SUM(ISNULL(stockqty, 0) * ISNULL(stockvalueusd, 0)) / NULLIF(SUM(ISNULL(stockqty, 0)), 0)), 0) FROM QL_stockvalue WHERE cmpcode='" + cmpcode + "' AND periodacctg IN ('" + periodacctg + "', '" + GetLastPeriod(periodacctg) + "') AND refoid=" + refoid + " AND refname='" + refname + "' AND closeflag=''").FirstOrDefault();
        }

        public static QL_conmat InsertConMat(string cmpcode, int conmtroid, string type, string formaction, int formoid, int refoid, string refname, int mtrwhoid, decimal qty, string reason, string note, string upduser, string refno, decimal valueidr, decimal valueusd, int deptoid, string conres, int formdtloid, int rabmstoid = 0, decimal valueidr_backup = 0, string serialnumber = "")
        {
            QL_conmat tbl = new QL_conmat();
            var typemin = qty > 0 ? 1 : -1;
            var updtime = GetServerTime();

            tbl.cmpcode = cmpcode;
            tbl.conmatoid = conmtroid;
            tbl.type = type;
            tbl.typemin = typemin;
            tbl.trndate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
            tbl.periodacctg = GetDateToPeriodAcctg(updtime);
            tbl.formaction = formaction;
            tbl.formoid = formoid;
            tbl.refoid = refoid;
            tbl.refname = refname;
            tbl.mtrwhoid = mtrwhoid;
            tbl.qtyin = qty >= 0 ? qty : 0;
            tbl.qtyout = qty < 0 ? qty * -1 : 0;
            tbl.reason = reason;
            tbl.note = note;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.refno = refno ?? "";
            tbl.valueidr = valueidr;
            tbl.valueusd = valueusd;
            tbl.deptoid = deptoid;
            tbl.conres = conres;
            tbl.valueidr_backup = valueidr_backup;
            tbl.valueusd_backup = 0;
            tbl.formdtloid = formdtloid;
            tbl.rabmstoid = rabmstoid;
            tbl.serialnumber = serialnumber ?? "";

            return tbl;
        }

        //public static QL_conmat_sn InsertConMat_sn(string cmpcode, int conmtroid, string type, string formaction, int formoid, int refoid, string refname, int mtrwhoid, decimal qty, string reason, string note, string upduser, string refno, decimal valueidr, decimal valueusd, int deptoid, string conres, int formdtloid, int rabmstoid = 0, decimal valueidr_backup = 0, string serialnumber = "")
        //{
        //    QL_conmat_sn tbl = new QL_conmat_sn();
        //    var typemin = qty > 0 ? 1 : -1;
        //    var updtime = GetServerTime();

        //    tbl.cmpcode = cmpcode;
        //    tbl.conmatoid = conmtroid;
        //    tbl.type = type;
        //    tbl.typemin = typemin;
        //    tbl.trndate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
        //    tbl.periodacctg = GetDateToPeriodAcctg(updtime);
        //    tbl.formaction = formaction;
        //    tbl.formoid = formoid;
        //    tbl.refoid = refoid;
        //    tbl.refname = refname;
        //    tbl.mtrwhoid = 0;
        //    tbl.qtyin = qty >= 0 ? qty : 0;
        //    tbl.qtyout = qty < 0 ? qty * -1 : 0;
        //    tbl.reason = reason;
        //    tbl.note = note;
        //    tbl.upduser = upduser;
        //    tbl.updtime = updtime;
        //    tbl.refno = refno;
        //    tbl.valueidr = valueidr;
        //    tbl.valueusd = valueusd;
        //    tbl.deptoid = deptoid;
        //    tbl.conres = conres;
        //    tbl.valueidr_backup = valueidr_backup;
        //    tbl.valueusd_backup = 0;
        //    tbl.formdtloid = formdtloid;
        //    tbl.rabmstoid = rabmstoid;
        //    tbl.serialnumber = serialnumber;

        //    return tbl;
        //}

        public static QL_matbooking InsertMatBooking(string cmpcode, int conmtroid, string formaction, int formoid, int refoid, string refname, int mtrwhoid, decimal qty, string upduser, string refno, int formdtloid, int rabmstoid = 0, string serialnumber = "")
        {
            QL_matbooking tbl = new QL_matbooking();
            var updtime = GetServerTime();

            tbl.cmpcode = cmpcode;
            tbl.matbookingoid = conmtroid;
            tbl.trndate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
            tbl.periodacctg = GetDateToPeriodAcctg(updtime);
            tbl.formaction = formaction;
            tbl.formoid = formoid;
            tbl.refoid = refoid;
            tbl.refname = refname;
            tbl.mtrwhoid = mtrwhoid;
            tbl.qtyin = 0;
            tbl.qtyout = qty;
            tbl.flag = "Post";
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.refno = refno ?? "";
            tbl.formdtloid = formdtloid;
            tbl.rabmstoid = rabmstoid;
            tbl.serialnumber = serialnumber ?? "";
            tbl.valueidr = 0;
            tbl.taxvalue = 0;
            tbl.taxamt = 0;
            tbl.amtnetto = 0;

            return tbl;
        }

        public static QL_stockvalue UpdateOrInsertStockValue(string cmpcode, int stockvalueoid, int refoid, string refname, decimal stockqty, decimal stockvalueidr, decimal stockvalueusd, string lasttranstype, string note, string upduser, out string status)
        {
            var updtime = GetServerTime();
            var periodacctg = GetDateToPeriodAcctg(updtime);
            QL_stockvalue tbl = new QL_APISEntities().QL_stockvalue.Where(s => s.cmpcode == cmpcode && s.periodacctg == periodacctg && s.refoid == refoid && s.refname == refname).FirstOrDefault();

            if (tbl == null)
            {
                status = "Insert";
                tbl = new QL_stockvalue();
                tbl.cmpcode = cmpcode;
                tbl.stockvalueoid = stockvalueoid;
                tbl.periodacctg = periodacctg;
                tbl.refoid = refoid;
                tbl.refname = refname;
                tbl.stockqty = stockqty;
                tbl.stockvalueidr = stockvalueidr;
                tbl.stockvalueusd = stockvalueusd;
                tbl.lasttranstype = lasttranstype;
                tbl.lasttransdate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
                tbl.note = null;
                tbl.upduser = upduser;
                tbl.updtime = updtime;
                tbl.backupqty = 0;
                tbl.backupvalueidr = 0;
                tbl.backupvalueusd = 0;
                tbl.closeflag = "";
                tbl.flag_temp = "";
            }
            else
            {
                status = "Update";
                tbl.backupqty = tbl.stockqty;
                tbl.backupvalueidr = tbl.stockvalueidr;
                tbl.backupvalueusd = tbl.stockvalueusd;
                if (tbl.stockqty + stockqty == 0)
                {
                    tbl.stockvalueidr = 0;
                    tbl.stockvalueusd = 0;
                }
                else
                {
                    tbl.stockvalueidr = (tbl.stockvalueidr * tbl.stockqty + stockvalueidr * stockqty) / (tbl.stockqty + stockqty);
                    tbl.stockvalueusd = (tbl.stockvalueusd * tbl.stockqty + stockvalueusd * stockqty) / (tbl.stockqty + stockqty);
                }
                tbl.stockqty += stockqty;
                tbl.lasttranstype = lasttranstype;
                tbl.lasttransdate = DateTime.Parse(updtime.ToString("MM/dd/yyyy"));
                tbl.upduser = upduser;
                tbl.updtime = updtime;
            }

            return tbl;
        }

        public static QL_conap InsertConAP(string cmpcode, int conapoid, string reftype, int refoid, int payrefoid, int suppoid, int acctgoid, string trnapstatus, string trnaptype, DateTime trnapdate, string periodacctg, int paymentacctgoid, DateTime paymentdate, string payrefno, int paybankoid, DateTime payduedate, decimal amttrans, decimal amtbayar, string trnapnote, string trnapres1, string trnapres2, string trnapres3, string createuser, DateTime createtime, string upduser, DateTime updtime, decimal amttransidr, decimal amtbayaridr, decimal amttransusd, decimal amtbayarusd, decimal amtbayarother, string conflag)

        {
            QL_conap tbl = new QL_conap();


            tbl.cmpcode = cmpcode;
            tbl.conapoid = conapoid;
            tbl.reftype = reftype;
            tbl.refoid = refoid;
            tbl.payrefoid = payrefoid;
            tbl.suppoid = suppoid;
            tbl.acctgoid = acctgoid;
            tbl.trnapstatus = trnapstatus;
            tbl.trnaptype = trnaptype;
            tbl.trnapdate = DateTime.Parse(trnapdate.ToString("MM/dd/yyyy")); ;
            tbl.periodacctg = periodacctg;
            tbl.paymentacctgoid = paymentacctgoid;
            tbl.paymentdate = DateTime.Parse(paymentdate.ToString("MM/dd/yyyy")); ;
            tbl.payrefno = payrefno;
            tbl.paybankoid = paybankoid;
            tbl.payduedate = DateTime.Parse(payduedate.ToString("MM/dd/yyyy")); ;
            tbl.amttrans = amttrans;
            tbl.amtbayar = amtbayar;
            tbl.trnapnote = trnapnote;
            tbl.trnapres1 = trnapres1;
            tbl.trnapres2 = trnapres2;
            tbl.trnapres3 = trnapres3;
            tbl.createuser = createuser;
            tbl.createtime = createtime;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.amttransidr = amttransidr;
            tbl.amtbayaridr = amtbayaridr;
            tbl.amttransusd = amttransusd;
            tbl.amtbayarusd = amtbayarusd;
            tbl.amtbayarother = amtbayarother;
            tbl.conflag = conflag;


            return tbl;
        }

        public static QL_conar InsertConAR(string cmpcode, int conaroid, string reftype, int refoid, int payrefoid, int custoid, int acctgoid, string trnarstatus, string trnartype, DateTime trnardate, string periodacctg, int paymentacctgoid, DateTime paymentdate, string payrefno, int paybankoid, DateTime payduedate, decimal amttrans, decimal amtbayar, string trnarnote, string trnarres1, string trnarres2, string trnarres3, string createuser, DateTime createtime, string upduser, DateTime updtime, decimal amttransidr, decimal amtbayaridr, decimal amttransusd, decimal amtbayarusd, decimal amtbayarother, string conflag)

        {
            QL_conar tbl = new QL_conar();


            tbl.cmpcode = cmpcode;
            tbl.conaroid = conaroid;
            tbl.reftype = reftype;
            tbl.refoid = refoid;
            tbl.payrefoid = payrefoid;
            tbl.custoid = custoid;
            tbl.acctgoid = acctgoid;
            tbl.trnarstatus = trnarstatus;
            tbl.trnartype = trnartype;
            tbl.trnardate = DateTime.Parse(trnardate.ToString("MM/dd/yyyy")); ;
            tbl.periodacctg = periodacctg;
            tbl.paymentacctgoid = paymentacctgoid;
            tbl.paymentdate = DateTime.Parse(paymentdate.ToString("MM/dd/yyyy")); ;
            tbl.payrefno = payrefno;
            tbl.paybankoid = paybankoid;
            tbl.payduedate = DateTime.Parse(payduedate.ToString("MM/dd/yyyy")); ;
            tbl.amttrans = amttrans;
            tbl.amtbayar = amtbayar;
            tbl.trnarnote = trnarnote;
            tbl.trnarres1 = trnarres1;
            tbl.trnarres2 = trnarres2;
            tbl.trnarres3 = trnarres3;
            tbl.createuser = createuser;
            tbl.createtime = createtime;
            tbl.upduser = upduser;
            tbl.updtime = updtime;
            tbl.amttransidr = amttransidr;
            tbl.amtbayaridr = amtbayaridr;
            tbl.amttransusd = amttransusd;
            tbl.amtbayarusd = amtbayarusd;
            tbl.amtbayarother = amtbayarother;

            return tbl;
        }

        public static string GetDataAcctgOid(string sVar, string cmp, string sFilter = "")
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_APISEntities db = new QL_APISEntities();
            List<mstacctg> tbl = new List<mstacctg>();
            string sSql = "";
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND interfacevar='" + sVar + "' AND interfaceres1='" + cmp + "'";
            string sCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (sCode == "")
            {
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND interfacevar='" + sVar + "' AND interfaceres1='All'";
                sCode = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            }
            if (sCode != "")
            {
                sSql = "SELECT DISTINCT a.acctgoid FROM QL_mstacctg a WHERE a.cmpcode='" + CompnyCode + "' AND a.activeflag='ACTIVE' " + sFilter + " AND (";
                string[] sSplitCode = sCode.Split(',');
                for (var i = 0; i < sSplitCode.Length; i++)
                {
                    sSql += "a.acctgcode LIKE '" + sSplitCode[i].TrimStart() + "%'";
                    if (i < sSplitCode.Length - 1)
                    {
                        sSql += " OR ";
                    }
                }
                sSql += ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode)";
            }
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();
            string sOid = "0";
            string sOidTemp = "";
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        sOidTemp += tbl[i].acctgoid.ToString() + ", ";
                    }
                    if (sOidTemp != "")
                        sOid = Left(sOidTemp, sOidTemp.Length - 2);
                }
            }
            return sOid;
        }

        public static string GetDataAcctgOid(string[] sVar, string cmp, string sFilter = "")
        {
            string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
            QL_APISEntities db = new QL_APISEntities();
            List<mstacctg> tbl = new List<mstacctg>();
            string sSql = "";
            string sCode = "";
            for (var i = 0; i < sVar.Length; i++)
            {
                sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND interfacevar='" + sVar[i] + "' AND interfaceres1='" + cmp + "'";
                string sTmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                if (sTmp == "")
                {
                    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" + CompnyCode + "' AND interfacevar='" + sVar[i] + "' AND interfaceres1='All'";
                    sTmp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                }
                if (sCode == "")
                    sCode = sTmp;
                else
                    sCode += ", " + sTmp;
            }
            if (sCode != "")
            {
                sSql = "SELECT DISTINCT a.acctgoid FROM QL_mstacctg a WHERE a.cmpcode='" + CompnyCode + "' AND a.activeflag='ACTIVE' " + sFilter + " AND (";
                string[] sSplitCode = sCode.Split(',');
                for (var i = 0; i < sSplitCode.Length; i++)
                {
                    sSql += "a.acctgcode LIKE '" + sSplitCode[i].TrimStart() + "%'";
                    if (i < sSplitCode.Length - 1)
                    {
                        sSql += " OR ";
                    }
                }
                sSql += ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ";
            }
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();
            string sOid = "0";
            string sOidTemp = "";
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        sOidTemp += tbl[i].acctgoid.ToString() + ", ";
                    }
                    if (sOidTemp != "")
                        sOid = Left(sOidTemp, sOidTemp.Length - 2);
                }
            }
            return sOid;
        }

        public static object MappingTable(object tbl_dest, object tbl_source)
        {
            foreach (var prop in tbl_dest.GetType().GetProperties())
            {
                var prop_source = tbl_source.GetType().GetProperties().Where(g => g.Name == prop.Name).FirstOrDefault();
                if (prop_source != null)
                {
                    var fieldvalue = prop_source.GetValue(tbl_source, null);
                    prop.SetValue(tbl_dest, fieldvalue);
                }
            }
            return tbl_dest;
        }

        public static string GetCompnyName(string sCmpcode)
        {
            string value = "";
            string sSql = "SELECT divname FROM QL_mstdivision WHERE divcode='" + sCmpcode + "'";
            QL_APISEntities db = new QL_APISEntities();
            value = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            return value;
        }

        public static decimal CekCreditLimit(int custoid)
        {
            decimal value = 0;
            string sSql = "SELECT (c.custcreditlimit - custcreditusage) creditlimit FROM QL_mstcust c WHERE c.custoid="+ custoid +"";
            QL_APISEntities db = new QL_APISEntities();
            value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return value;
        }

        public static string ReCalCreditLimit()
        {
            string msg = "OK";
            string sSql = "UPDATE tb SET custcreditusage=(tb.soamt-payamt+retamt) FROM( SELECT c.custoid, c.custname, c.custcreditlimit, c.custcreditusage, ISNULL((SELECT SUM(som.soitemgrandtotalamt) FROM QL_trnsoitemmst som WHERE som.soitemtype = '' AND ISNULL(som.soitemmstres1, '') = '' AND som.soitemmststatus IN('Post', 'Approvved', 'Closed') AND som.custoid = c.custoid),0.0)  +ISNULL((SELECT SUM(som.soassetgrandtotalamt) FROM QL_trnsoassetmst som WHERE ISNULL(som.soassetmstres1, '') = '' AND som.soassetmststatus IN('Post', 'Approvved', 'Closed') AND som.custoid = c.custoid),0.0)AS soamt, ISNULL((SELECT SUM(pay.amtbayar) FROM QL_conar pay WHERE pay.reftype IN('QL_trnaritemmst', 'QL_trnarassetmst') AND pay.payrefoid <> 0  AND pay.trnarstatus IN('Post', 'Approvved', 'Closed') AND pay.custoid = c.custoid),0.0) AS payamt, ISNULL((SELECT SUM(arm.aritemgrandtotal) FROM QL_trnarretitemmst retm INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid = retm.aritemmstoid WHERE retm.arretitemmststatus IN('Post', 'Approvved', 'Closed') AND retm.custoid = c.custoid),0.0) AS retamt FROM QL_mstcust c)AS tb";
            QL_APISEntities db = new QL_APISEntities();
            db.Database.ExecuteSqlCommand(sSql);
            db.SaveChanges();
            return msg;
        }

        public static string GetIPAddress()
        {
            string IPAddress = HttpContext.Current.Request.UserHostAddress;
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    IPAddress = Convert.ToString(IP);
                }
            }
            return IPAddress;
        }

        public static List<Dictionary<string, object>> toObject(DataTable tbl)
        {
            List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
            if (tbl.Rows.Count > 0)
            {
                foreach (DataRow dr in tbl.Rows)
                {
                    var row = new Dictionary<string, object>();
                    foreach (DataColumn col in tbl.Columns)
                    {
                        row.Add(col.ColumnName, (dr[col] == DBNull.Value) ? "" : dr[col]);
                    }
                    rowsdtl.Add(row);
                }
            }
            return rowsdtl;
        }

        public static IList<Dictionary<string, object>> getListDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string select_query, string fixed_filter = "", string default_order = "")
        {
            QL_APISEntities db = new QL_APISEntities();
            var take = model.length;
            var skip = model.start;

            var and_filter = "";
            if ((model.search != null) ? !string.IsNullOrEmpty(model.search.value) : false)//search in all column
            {
                var col_tosearch = model.columns.Where(w => w.searchable & w.data != "sAddFilter").Select(s => s.data).ToList();
                foreach (var s in model.search.value.Trim().Split(' '))
                {
                    var or_filter = "";
                    foreach (var a in col_tosearch)
                    {
                        if (or_filter != "") or_filter += " OR ";
                        or_filter += a + " LIKE '%" + s.tChar() + "%'";
                    }
                    if (or_filter != "")
                    {
                        and_filter += " AND (" + or_filter + ")";
                    }
                }
            }
            foreach (var obj in model.columns.Where(w => w.searchable & w.search != null).ToList())//search by column
            {
                if (!string.IsNullOrEmpty(obj.search.value))
                {
                    if (obj.search.value.ToLower().StartsWith("between") | obj.search.value.ToLower().StartsWith("in(") | obj.search.value.StartsWith("=") | obj.search.value.StartsWith("<>"))
                    {
                        and_filter += " AND " + obj.data + " " + obj.search.value;
                    }
                    else if (obj.data == "sAddFilter")
                    {
                        and_filter += obj.search.value;
                    }
                    else
                    {
                        var n_filter = "";
                        foreach (var s in obj.search.value.Split(' '))
                        {
                            if (n_filter != "") n_filter += " OR ";
                            n_filter += obj.data + " LIKE '" + obj.search.value + "'";
                        }
                        if (n_filter != "")
                        {
                            and_filter += " AND (" + n_filter + ")";
                        }
                    }
                }
            }

            string orderBy = "";
            if (model.order != null)
            {
                foreach (var o in model.order)
                {
                    if (!string.IsNullOrEmpty(orderBy)) orderBy += ", ";
                    orderBy += model.columns[o.column].data + " " + o.dir.ToLower();
                }
            }

            // search the dbase taking into consideration table sorting and paging
            //var result = GetDataFromDbase(and_filter, take, skip, orderBy, out filteredResultsCount, out totalResultsCount);
            if (String.IsNullOrEmpty(orderBy))
            {
                if (string.IsNullOrEmpty(default_order))
                    default_order = model.columns.Select(s => s.data).First() + " DESC";
                orderBy = default_order;
            }

            var result_query = select_query + " WHERE 1=1 " + and_filter + fixed_filter + " ORDER BY " + orderBy + " OFFSET " + skip + " ROWS FETCH NEXT " + take + " ROWS ONLY";
            var result = ClassFunction.toObject(new ClassConnection().GetDataTable(result_query, "result")); // SQL server 2016

            var rs_count = new ClassConnection().GetDataTable(select_query + " WHERE 1=1 " + and_filter + fixed_filter + " ", "result2");
            var rs_count2 = new ClassConnection().GetDataTable(select_query + " WHERE 1=1 " + fixed_filter + " ", "result3");
            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            filteredResultsCount = rs_count.Rows.Count;
            totalResultsCount = rs_count2.Rows.Count;
            if (result == null)
            {
                // empty collection...
                return new List<Dictionary<string, object>>();
            }
            return result;
        }
    }

    public static class StringExtensions
    {
        public static string Right(this string str, int length)
        {
            if (length < 0 | string.IsNullOrEmpty(str))
            {
                str = "";
            }
            else if (!string.IsNullOrEmpty(str) & str.Length > length)
            {
                str = str.Substring(str.Length - length, length);
            }
            return str;
        }
        public static string Left(this string str, int length)
        {
            if (length < 0 | string.IsNullOrEmpty(str))
            {
                str = "";
            }
            else if (!string.IsNullOrEmpty(str) & str.Length > length)
            {
                str = str.Substring(0, length);
            }
            return str;
        }
        public static decimal tDecimal(this string str)
        {
            decimal num = 0;
            decimal.TryParse(str.Replace(",", ""), out num);
            return num;
        }
        public static int tInt(this string str)
        {
            int num = 0;
            int.TryParse(str.Replace(",", ""), out num);
            return num;
        }
        public static string tChar(this string str)
        {
            return (str ?? "").Replace("'", "''");
        }
    }
}