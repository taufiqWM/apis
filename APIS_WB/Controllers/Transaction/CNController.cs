﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class CNController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public CNController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class trncreditnote
        {
            public string cmpcode { get; set; }
            public int cnoid { get; set; }
            public string cnno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime cndate { get; set; }
            public string tipe { get; set; }
            public string suppcustname { get; set; }
            public string aparno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cnamt { get; set; }
            public string cnstatus { get; set; }
            public string cnnote { get; set; }
            public string divname { get; set; }
            public string reason { get; set; }
        }

        public class apardata
        {
            public string cmpcode { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public int acctgoid { get; set; }
            public string acctgaccount { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public string transcheckdate { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public decimal amttrans { get; set; }
            public decimal amttransidr { get; set; }
            public decimal amttransusd { get; set; }
            public decimal amtbayar { get; set; }
            public decimal amtbayaridr { get; set; }
            public decimal amtbayarusd { get; set; }
            public decimal amtbalance { get; set; }
            public decimal amtbalanceidr { get; set; }
            public decimal amtbalanceusd { get; set; }
            public decimal aparpaidamt { get; set; }
            public decimal aparpaidamtidr { get; set; }
            public decimal aparpaidamtusd { get; set; }
            public decimal aparbalance { get; set; }
            public decimal aparbalanceidr { get; set; }
            public decimal aparbalanceusd { get; set; }
            public decimal aparamt { get; set; }
            public decimal aparamtidr { get; set; }
            public decimal aparamtusd { get; set; }
            public decimal amtdncn { get; set; }
            public decimal amtdncnidr { get; set; }
            public decimal amtdncnusd { get; set; }
            public DateTime apardate { get; set; }
            public DateTime aparcheckdate { get; set; }
            public string cntype { get; set; }
            public string aparno { get; set; }
            public string suppcustname { get; set; }
            public string dbacctgdesc { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        public class supplier
        {
            public int oid { get; set; }
            public string code { get; set; }
            public string name { get; set; }
        }

        private void InitDDL(QL_trncreditnote tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var dbacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dbacctgoid);
            ViewBag.dbacctgoid = dbacctgoid;
            var cracctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.cracctgoid);
            ViewBag.cracctgoid = cracctgoid;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindSupplierData(string reftype)
        {
            List<supplier> tbl = new List<supplier>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT DISTINCT a.suppoid AS oid,a.suppcode AS code,a.suppname AS name FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,(ap.amttrans+ap.amttransidr+ap.amttransusd) apamt," +
                "ap.trnapdate, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 And ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid = ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS appaidamt," +
                " 0.0 AS apcncnamt " +
                "FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + CompnyCode + "' AND ISNULL(ap.payrefoid,0)=0 " +
                ") AS a WHERE ROUND(a.apamt, 4)<>ROUND(a.appaidamt+a.apcncnamt, 4) " +
                "ORDER BY a.suppcode ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,(ar.amttrans+ar.amttransidr+ar.amttransusd) aramt," +
                "ar.trnardate, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS arpaidamt," +
                " 0.0 AS arcncnamt " +
                "FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + CompnyCode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.trnartype NOT LIKE 'ARRET%' " +
                ") AS a WHERE ROUND(a.aramt, 4)<>ROUND(a.arpaidamt+a.arcncnamt, 4) " +
                "ORDER BY a.custcode ";
            }
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataAPAR(string reftype, string suppcustoid)
        {
            List<apardata> tbl = new List<apardata>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT a.cmpcode, a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode, a.projectname, a.departemen," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,ap.amttransidr,ap.amttransusd,vap.transno,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transcheckdate,vap.currency, vap.projectname, vap.departemen," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd " +
                "FROM QL_conap ap INNER JOIN (SELECT con.cmpcode, conapoid, vapno transno, ap.* FROM QL_conap con CROSS APPLY ( SELECT apitemdate vapdate, apitemno vapno, apitempaytypeoid vappaytypeoid,c.currcode currency,  ap.curroid vapcurr, ap.rateoid vaprate, ap.curroid, ap.rate2oid vaprate2, apitemtotalamt vapamt, apitemtaxamt vaptax, apitemmstnote vapnote, apitemmststatus vapstatus, ap.createuser vapcrtuser, ap.createtime vapcrtdate, ap.upduser vapupduser, ap.updtime vapupddate, getdate() vapdatetakegiro, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_trnapitemmst ap INNER JOIN QL_mstcurr c ON c.curroid = ap.curroid WHERE con.reftype='QL_trnapitemmst' AND apitemmstoid=con.refoid AND ap.cmpcode=con.cmpcode UNION ALL  SELECT apdirdate vapdate, apdirno vapno, apdirpaytypeoid vappaytypeoid,c.currcode currency,  ap.curroid vapcurr, ap.rateoid vaprate, ap.curroid, ap.rate2oid vaprate2, apdirtotalamt vapamt, apdirtaxamt vaptax, apdirmstnote vapnote, apdirmststatus vapstatus, ap.createuser vapcrtuser, ap.createtime vapcrtdate, ap.upduser vapupduser, ap.updtime vapupddate, getdate() vapdatetakegiro, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_trnapdirmst ap INNER JOIN QL_mstcurr c ON c.curroid = ap.curroid WHERE con.reftype='QL_trnapdirmst' AND apdirmstoid=con.refoid AND ap.cmpcode=con.cmpcode UNION ALL  SELECT apassetdate vapdate, apassetno vapno, apassetpaytypeoid vappaytypeoid,c.currcode currency,  ap.curroid vapcurr, ap.rateoid vaprate, ap.curroid, ap.rate2oid vaprate2, apassettotalamt vapamt, apassettotaltax vaptax, apassetmstnote vapnote, apassetmststatus vapstatus, ap.createuser vapcrtuser, ap.createtime vapcrtdate, ap.upduser vapupduser, ap.updtime vapupddate, getdate() vapdatetakegiro, '' AS projectname, '' AS departemen FROM QL_trnapassetmst ap INNER JOIN QL_mstcurr c ON c.curroid = ap.curroid WHERE con.reftype='QL_trnapassetmst' AND apassetmstoid=con.refoid AND ap.cmpcode=con.cmpcode ) ap WHERE con.payrefoid=0) vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + CompnyCode + "' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" + suppcustoid + "" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno ";
            }
            else 
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode, a.projectname, a.departemen," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans,ar.amttransidr,ar.amttransusd,var.transno,CONVERT(VARCHAR(10),ar.trnardate,101) AS transdate,CONVERT(VARCHAR(10),ar.trnardate,101) AS transcheckdate,var.currency, var.projectname, var.departemen," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd " +
                "FROM QL_conar ar INNER JOIN (SELECT cmpcode, conaroid, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemno FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetno FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmststatus FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmststatus FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmstnote FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmstnote FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), aritemdate) FROM QL_trnaritemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), arassetdate) FROM QL_trnarassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currcode FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currsymbol FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT rm.projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) projectname, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT de.groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) departemen FROM QL_conar AS con WHERE payrefoid = 0) var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + CompnyCode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" + suppcustoid + " AND ar.trnartype NOT LIKE 'ARRET%'" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno";
            }
            tbl = db.Database.SqlQuery<apardata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, CompnyCode) + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncreditnote tbl)
        {
            if (tbl.reftype != "")
            {
                ViewBag.suppcustname = Session["suppcustname"];
                ViewBag.aparno = Session["aparno"];
                ViewBag.aparamt = Session["aparamt"];
                ViewBag.aparamtidr = Session["aparamtidr"];
                ViewBag.aparamtusd = Session["aparamtusd"];
                ViewBag.aparpaidamt = Session["aparpaidamt"];
                ViewBag.aparbalance = Session["aparbalance"];
                ViewBag.aparbalanceidr = Session["aparbalanceidr"];
                ViewBag.aparbalanceusd = Session["aparbalanceusd"];
                ViewBag.aparbalanceafter = Session["aparbalanceafter"];
                ViewBag.aparcheckdate = Session["aparcheckdate"];
                ViewBag.apardate = Session["apardate"];
                ViewBag.cntype = Session["cntype"];
                ViewBag.dmaxamount = Session["dmaxamount"];
                ViewBag.dbacctgdesc = Session["dbacctgdesc"];
                ViewBag.projectname = Session["projectname"];
                ViewBag.departemen = Session["departemen"];
            }
        }

        private string GenerateNo()
        {
            var cnno = "";
            if (CompnyCode != "")
            {
                string sNo = "CN/" + ClassFunction.GetServerTime().ToString("yy") + "/" + ClassFunction.GetServerTime().ToString("MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cnno, "+ int.Parse(DefaultFormatCounter) + ") AS INTEGER)) + 1, 1) AS IcnEW FROM QL_trncreditnote WHERE cmpcode='" + CompnyCode + "' AND cnno LIKE '%" + sNo + "%' ";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
                cnno = sNo + sCounter;
            }
            return cnno;
        }

        // GET: creditnoteMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT * FROM (" +
            "SELECT cn.cmpcode,'"+ CompnyName +"' divname,cn.cnoid,cn.cnno, cn.cnnote,cn.cndate,'A/P' AS tipe,s.suppname AS suppcustname," +
            "CASE cn.reftype WHEN 'QL_trnapitemmst' THEN (SELECT apit.apitemno FROM QL_trnapitemmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apitemmstoid=cn.refoid) WHEN 'QL_trnapdirmst' THEN (SELECT apit.apdirno FROM QL_trnapdirmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apdirmstoid=cn.refoid) ELSE (SELECT apit.apassetno FROM QL_trnapassetmst apit WHERE apit.cmpcode=cn.cmpcode AND apit.apassetmstoid=cn.refoid) " +
            "END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue, CASE WHEN cnstatus = 'Revised' THEN revisereason WHEN cnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
            "FROM QL_trncreditnote cn " +
            "INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid " +
            "WHERE left(cn.reftype,8)='ql_trnap' " +
            "UNION ALL " +
            "SELECT cn.cmpcode,'" + CompnyName + "' divname,cn.cnoid,cn.cnno, cn.cnnote,cn.cndate,'A/R' AS tipe,c.custname AS suppcustname," +
            "CASE cn.reftype WHEN 'QL_trnaritemmst' THEN (SELECT arit.aritemno FROM QL_trnaritemmst arit WHERE arit.cmpcode=cn.cmpcode AND arit.aritemmstoid=cn.refoid) ELSE (SELECT arit.arassetno FROM QL_trnarassetmst arit WHERE arit.cmpcode=cn.cmpcode AND arit.arassetmstoid=cn.refoid) " +
            "END AS aparno,cn.cnamt,cn.cnstatus,cn.createuser,cn.reftype,cn.updtime, 'False' AS checkvalue, CASE WHEN cnstatus = 'Revised' THEN revisereason WHEN cnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
            "FROM QL_trncreditnote cn " +
            "INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid " +
            "WHERE left(cn.reftype,8)='ql_trnar' AND left(cn.reftype,11)<>'ql_trnarret' " +
            ") AS a WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " a.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " a.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND cndate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND cndate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND cnstatus IN ('In Process', 'Revised')";
            }

            sSql += " ORDER BY CONVERT(DATETIME, a.cndate) DESC, a.cnoid DESC";

            List<trncreditnote> dt = db.Database.SqlQuery<trncreditnote>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: creditnoteMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncreditnote tbl;
            string action = "New Data";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncreditnote();
                tbl.cnoid = ClassFunction.GenerateID("QL_trncreditnote");
                tbl.cndate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cnstatus = "In Process";
                tbl.reftype = "";
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trncreditnote.Find(CompnyCode, id);

                List<apardata> tbldtl = new List<apardata>();
                sSql = "SELECT * FROM (" +
                "SELECT cn.cmpcode,'"+ CompnyName + "' divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,s.suppname AS suppcustname,cn.reftype tipe,cn.refoid,vap.transno AS aparno, vap.projectname, vap.departemen," +
                "cn.curroid,cn.rateoid,cn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,ap.amttransidr aparamtidr,ap.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd," +
                "cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ap.trnapdate aparcheckdate " +
                "FROM QL_trncreditnote cn  " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                "INNER JOIN QL_conap ap ON ap.cmpcode=cn.cmpcode AND ap.reftype=cn.reftype AND ap.refoid=cn.refoid AND ap.payrefoid=0 " +
                "INNER JOIN (SELECT cmpcode, conapoid, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemno FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirno FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmstnote FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmstnote FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmstnote FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apitemdate) FROM QL_trnapitemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apitempaytypeoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apdirdate) FROM QL_trnapdirmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apdirpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apassetdate) FROM QL_trnapassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currcode FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currsymbol FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currsymbol FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT rm.projectname FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT rm.projectname FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS projectname, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT de.groupdesc FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT de.groupdesc FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS departemen FROM QL_conap AS con WHERE payrefoid = 0) vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid " +
                "WHERE left(cn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT cn.cmpcode,'" + CompnyName + "' divname,cn.cnoid,cn.cnno,cn.cndate,cn.suppcustoid,c.custname AS suppcustname,cn.reftype AS tipe,cn.refoid,var.transno AS aparno, var.projectname, var.departemen," +
                "cn.curroid,cn.rateoid,cn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,ar.amttransidr aparamtidr,ar.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd," +
                "cn.cnamt,cn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,cn.cracctgoid,cn.cnnote,cn.cnstatus,cn.createuser,cn.createtime,cn.upduser,cn.updtime,ar.trnardate aparcheckdate " +
                "FROM QL_trncreditnote cn " +
                "INNER JOIN QL_mstcust c ON c.custoid=cn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=cn.dbacctgoid " +
                "INNER JOIN QL_conar ar ON ar.cmpcode=cn.cmpcode AND ar.reftype=cn.reftype AND ar.refoid=cn.refoid AND ar.payrefoid=0 " +
                "INNER JOIN (SELECT cmpcode, conaroid, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemno FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetno FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmststatus FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmststatus FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmstnote FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmstnote FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), aritemdate) FROM QL_trnaritemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), arassetdate) FROM QL_trnarassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currcode FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currsymbol FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT rm.projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) projectname, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT de.groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) departemen FROM QL_conar con WHERE payrefoid = 0) var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                "WHERE left(cn.reftype,8)='ql_trnar' AND left(cn.reftype,11)<>'ql_trnarret' " +
                ") AS a WHERE a.cnoid=" + id;
                tbldtl = db.Database.SqlQuery<apardata>(sSql).ToList();

                Session["aparno"] = tbldtl[0].aparno;
                Session["apardate"] = tbldtl[0].apardate;
                Session["aparcheckdate"] = tbldtl[0].aparcheckdate;
                if (ClassFunction.Left(tbl.reftype.ToUpper(), 8) == "QL_TRNAP")
                {
                    Session["cntype"] = "AP";
                }
                else
                {
                    Session["cntype"] = "AR";
                }
                Session["dmaxamount"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["suppcustname"] = tbldtl[0].suppcustname;

                Session["aparbalance"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["aparbalanceidr"] = tbldtl[0].aparamtidr - (tbldtl[0].amtbayaridr + tbldtl[0].amtdncnidr);
                Session["aparbalanceusd"] = tbldtl[0].aparamtusd - (tbldtl[0].amtbayarusd + tbldtl[0].amtdncnusd);
                Session["aparamt"] = tbldtl[0].aparamt;
                Session["aparamtidr"] = tbldtl[0].aparamtidr;
                Session["aparamtusd"] = tbldtl[0].aparamtusd;
                Session["aparpaidamt"] = tbldtl[0].amtbayar + tbldtl[0].amtdncn;
                Session["aparbalanceafter"] = Convert.ToDecimal(Session["aparbalance"]) + (tbl.cnamt * (Session["cntype"].ToString() == "AR" ? -1 : 1));
                Session["dbacctgdesc"] = tbldtl[0].dbacctgdesc;
                Session["projectname"] = tbldtl[0].projectname;
                Session["departemen"] = tbldtl[0].departemen;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: creditnoteMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncreditnote tbl, string action, string tglmst, string aparno, string apardate, string aparcheckdate, string cntype, decimal aparbalanceafter, decimal aparbalanceidr, decimal aparbalanceusd, decimal aparbalance, decimal dmaxamount, string suppcustname, decimal aparamt, decimal aparamtidr, decimal aparamtusd, decimal aparpaidamt, string dbacctgdesc, string projectname, string departemen)
        {
            Session["aparno"] = aparno;
            Session["apardate"] = apardate;
            Session["aparcheckdate"] = aparcheckdate;
            Session["cntype"] = cntype;
            Session["aparbalanceafter"] = aparbalanceafter;
            Session["aparbalanceidr"] = aparbalanceidr;
            Session["aparbalanceusd"] = aparbalanceusd;
            Session["aparbalance"] = aparbalance;
            Session["dmaxamount"] = dmaxamount;
            Session["suppcustname"] = suppcustname;
            Session["aparamt"] = aparamt;
            Session["aparamtidr"] = aparamtidr;
            Session["aparamtusd"] = aparamtusd;
            Session["aparpaidamt"] = aparpaidamt;
            Session["dbacctgdesc"] = dbacctgdesc;
            Session["projectname"] = projectname;
            Session["departemen"] = departemen;

            try
            {
                tbl.cndate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cndate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            var reftype = cntype;
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            if (tbl.cmpcode == null)
                tbl.cmpcode = CompnyCode;
            if (tbl.cnno == null)
                tbl.cnno = "";
            string sErrReply = "";
            if (Convert.ToDateTime(tbl.cndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(apardate) || Convert.ToDateTime(tbl.cndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(aparcheckdate))
            {
                ModelState.AddModelError("", "CN DATE must be more than AP/AR DATE");
            }
            if (tbl.refoid == 0)
            {
                ModelState.AddModelError("", "Please select " + reftype + " first.");
            }
            else
            {
                if (tbl.cnamt <= 0 && aparbalance > 0)
                {
                    ModelState.AddModelError("", "Credit Note amount must be greater than 0.");
                }
                else
                {
                    if (reftype == "AR")
                    {
                        if (tbl.cnamt > dmaxamount)
                        {
                            ModelState.AddModelError("", "Maximum Credit Note amount is " + dmaxamount + "");
                        }
                        else
                        {
                            sSql = "SELECT ISNULL(SUM(amttrans - amtbayar), 0) FROM QL_conar con WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.cnoid;
                            }
                            decimal cnewBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            sSql = "SELECT ISNULL(SUM(amttransidr - amtbayaridr), 0) FROM QL_conar con WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.cnoid;
                            }
                            decimal cnewBalanceIDR = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceidr = cnewBalanceIDR;
                            sSql = "SELECT ISNULL(SUM(amttransusd - amtbayarusd), 0) FROM QL_conar con WHERE con.cmpcode='" + tbl.cmpcode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.cnoid;
                            }
                            decimal cnewBalanceUSD = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceusd = cnewBalanceUSD;
                            if (dmaxamount != cnewBalance)
                            {
                                dmaxamount = cnewBalance;
                                aparbalance = cnewBalance;
                            }
                            if (tbl.cnamt > dmaxamount)
                            {
                                ModelState.AddModelError("", "Maximum Credit Note has been updated by another user. Maximum Credit Note amount is " + dmaxamount + "");
                            }
                        }
                    }
                }
            }
            if (tbl.suppcustoid == 0)
                ModelState.AddModelError("", "Please select SUPPLIER/CUSTOMER field!");
            if (tbl.dbacctgoid == 0)
                ModelState.AddModelError("", "Please select DEBET ACCOUNT field");
            if (tbl.cracctgoid == 0)
                ModelState.AddModelError("", "Please select CREDIT ACCOUNT field");
            if (tbl.cnamt <= 0)
            {
                ModelState.AddModelError("", "CN AMOUNT must be more than 0!");
            }
            else
            {
                if (!ClassFunction.isLengthAccepted("cnamt", "QL_trncreditnote", tbl.cnamt, ref sErrReply))
                    ModelState.AddModelError("", "CN AMOUNT must be less than MAX CN AMOUNT (" + sErrReply + ") allowed stored in database!");
            }
            if (tbl.cnstatus.ToUpper() == "REVISED")
            {
                tbl.cnstatus = "In Process";
            }

            var cRate = new ClassRate();
            if (tbl.cnstatus == "Post")
            {
                tbl.cnno = GenerateNo();
                cRate.SetRateValue(tbl.curroid, Convert.ToDateTime(apardate).ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cnstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cnstatus = "In Process";
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.cnstatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cndate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.cnstatus = "In Process";
            }
            if (tbl.cnstatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.cnstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.cnstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncreditnote");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncreditnote WHERE cnoid=" + tbl.cnoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trncreditnote");
                    }
                }
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cntaxtype = "NON TAX";
                tbl.cntaxamt = 0;
                tbl.cnno = (tbl.cnno == null ? "" : tbl.cnno);
                tbl.cnnote = (tbl.cnnote == null ? "" : tbl.cnnote);

                tbl.cnamtidr = tbl.cnamt * cRate.GetRateMonthlyIDRValue;
                tbl.cnamtusd = tbl.cnamt * cRate.GetRateMonthlyUSDValue;
                tbl.cntaxamtidr = tbl.cntaxamt * cRate.GetRateMonthlyIDRValue;
                tbl.cntaxamtusd = tbl.cntaxamt * cRate.GetRateMonthlyUSDValue;
                var dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conar con WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnarstatus='Post'").FirstOrDefault();
                if (reftype.ToUpper() == "AP")
                {
                    dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conap WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnapstatus='Post'").FirstOrDefault();
                }
                if (dbalancepost == 0)
                {
                    tbl.cnamtidr = aparbalanceidr;
                    tbl.cnamtusd = aparbalanceusd;
                    tbl.cntaxamtidr = 0;
                    tbl.cntaxamtusd = 0;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trncreditnote.Find(tbl.cmpcode, tbl.cnoid) != null)
                                tbl.cnoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncreditnote.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.cnoid + " WHERE tablename='QL_trncreditnote'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Select OLD Data
                            var sOldType = tbl.reftype.ToUpper();
                            var sOldOid = tbl.refoid;
                            if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                            {
                                var conap = db.QL_conap.Where(x => x.payrefoid == tbl.cnoid && x.trnaptype == "CNAP");
                                db.QL_conap.RemoveRange(conap);
                                db.SaveChanges();
                            }
                            else
                            {
                                var conar = db.QL_conar.Where(x => x.payrefoid == tbl.cnoid && x.trnartype == "CNAR");
                                db.QL_conar.RemoveRange(conar);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.cnstatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.cnoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trncreditnote";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.cnstatus == "Post")
                        {
                            if (reftype.ToUpper() == "AP")
                            {
                                QL_conap conap = new QL_conap();
                                conap.cmpcode = tbl.cmpcode;
                                conap.conapoid = conapoid++;
                                conap.reftype = tbl.reftype;
                                conap.refoid = tbl.refoid;
                                conap.payrefoid = tbl.cnoid;
                                conap.suppoid = tbl.suppcustoid;
                                conap.acctgoid = tbl.dbacctgoid;
                                conap.trnapstatus = tbl.cnstatus;
                                conap.trnaptype = "CN" + reftype;
                                conap.trnapdate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                conap.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                                conap.paymentacctgoid = tbl.cracctgoid;
                                conap.paymentdate = tbl.cndate;
                                conap.payrefno = tbl.cnno;
                                conap.paybankoid = 0;
                                conap.payduedate = tbl.cndate;
                                conap.amttrans = 0;
                                conap.amtbayar = (tbl.cnamt + tbl.cntaxamt) * -1;
                                conap.trnapnote = "CN No. " + tbl.cnno + " for " + reftype + " No. " + aparno + "";
                                conap.createuser = tbl.createuser;
                                conap.createtime = tbl.createtime;
                                conap.upduser = tbl.upduser;
                                conap.updtime = tbl.updtime;
                                conap.amttransidr = 0;
                                conap.amtbayaridr = (tbl.cnamtidr + tbl.cntaxamtidr) * -1;
                                conap.amttransusd = 0;
                                conap.amtbayarusd = (tbl.cnamtusd + tbl.cntaxamtusd) * -1;
                                conap.amtbayarother = 0;
                                conap.conflag = "";

                                db.QL_conap.Add(conap);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + conap.conapoid + " WHERE tablename='QL_conap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                QL_conar conar = new QL_conar();
                                conar.cmpcode = tbl.cmpcode;
                                conar.conaroid = conaroid++;
                                conar.reftype = tbl.reftype;
                                conar.refoid = tbl.refoid;
                                conar.payrefoid = tbl.cnoid;
                                conar.custoid = tbl.suppcustoid;
                                conar.acctgoid = tbl.dbacctgoid;
                                conar.trnarstatus = tbl.cnstatus;
                                conar.trnartype = "CN" + reftype;
                                conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);
                                conar.paymentacctgoid = tbl.cracctgoid;
                                conar.paymentdate = tbl.cndate;
                                conar.payrefno = tbl.cnno;
                                conar.paybankoid = 0;
                                conar.payduedate = tbl.cndate;
                                conar.amttrans = 0;
                                conar.amtbayar = tbl.cnamt + tbl.cntaxamt;
                                conar.trnarnote = "CN No. " + tbl.cnno + " for "+ reftype +" No. " + aparno + "";
                                conar.createuser = tbl.createuser;
                                conar.createtime = tbl.createtime;
                                conar.upduser = tbl.upduser;
                                conar.updtime = tbl.updtime;
                                conar.amttransidr = 0;
                                conar.amtbayaridr = tbl.cnamtidr + tbl.cntaxamtidr;
                                conar.amttransusd = 0;
                                conar.amtbayarusd = tbl.cnamtusd + tbl.cntaxamtusd;
                                conar.amtbayarother = 0;

                                db.QL_conar.Add(conar);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var sDate = tbl.cndate;
                            var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.cndate);

                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.dbacctgoid, "D", tbl.cnamt + tbl.cntaxamt, tbl.cnno, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.upduser, tbl.updtime, tbl.cnamtidr + tbl.cntaxamtidr, tbl.cnamtusd + tbl.cntaxamtusd, "QL_trncreditnote " + tbl.cnoid, "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.cracctgoid, "C", tbl.cnamt + tbl.cntaxamt, tbl.cnno, "CN No. " + tbl.cnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.cnstatus, tbl.upduser, tbl.updtime, tbl.cnamtidr + tbl.cntaxamtidr, tbl.cnamtusd + tbl.cntaxamtusd, "QL_trncreditnote " + tbl.cnoid, "", "", "", 0));
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: creditnoteMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncreditnote tbl = db.QL_trncreditnote.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Select OLD Data
                        var sOldType = tbl.reftype.ToUpper();
                        var sOldOid = tbl.refoid;
                        if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                        {
                            var conap = db.QL_conap.Where(x => x.payrefoid == tbl.cnoid && x.trnaptype == "CNAP");
                            db.QL_conap.RemoveRange(conap);
                            db.SaveChanges();
                        }
                        else
                        {
                            var conar = db.QL_conar.Where(x => x.payrefoid == tbl.cnoid && x.trnartype == "CNAR");
                            db.QL_conar.RemoveRange(conar);
                            db.SaveChanges();
                        }

                        db.QL_trncreditnote.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCreditNote.rpt"));
            report.SetParameterValue("sWhere", " WHERE [CmpCode]='" + CompnyCode + "' AND [Oid] IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "CreditNotePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}