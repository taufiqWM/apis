//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnpaydp
    {
        public string cmpcode { get; set; }
        public int paydpoid { get; set; }
        public int paydpseq { get; set; }
        public int cashbankoid { get; set; }
        public int suppoid { get; set; }
        public string reftype { get; set; }
        public int refoid { get; set; }
        public int acctgoid { get; set; }
        public string dpreftype { get; set; }
        public int dprefoid { get; set; }
        public string paydprefno { get; set; }
        public Nullable<System.DateTime> paydpduedate { get; set; }
        public decimal paydpamt { get; set; }
        public decimal paydpamtidr { get; set; }
        public decimal paydpamtusd { get; set; }
        public string paydpnote { get; set; }
        public string paydpres1 { get; set; }
        public string paydpres2 { get; set; }
        public string paydpres3 { get; set; }
        public string paydpstatus { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
    }
}
