﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FJJasaReportController : Controller
    {
        // GET: FJJasaReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public FJJasaReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE rm.cmpcode='" + CompnyCode + "' AND arm.aritemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY rm.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, arm.cmpcode, arm.aritemno [FJ No], CONVERT(CHAR(10),arm.aritemdate,103) [FJ Date], c.custname [Customer], arm.aritemmstnote [FJ Note] FROM QL_trnaritemmst arm LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY arm.aritemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT rm.custoid FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "' AND arm.aritemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomor, string TextNomor, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.jasacode [Kode], m.jasadesc [Deskripsi], m.jasanote [Keterangan], g.gndesc [Unit] FROM QL_mstjasa m INNER JOIN QL_trnaritemdtl ard ON ard.itemoid=m.jasaoid INNER JOIN QL_trnaritemmst arm ON ard.aritemmstoid=arm.aritemmstoid LEFT JOIN QL_trnrabmst rm ON rm.cmpcode=ard.cmpcode AND rm.reqrabmstoid=arm.rabmstoid INNER JOIN QL_m05GN g ON g.gnoid=ard.aritemunitoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE rm.cmpcode='" + CompnyCode + "' AND arm.aritemtype='Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.jasadesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptARjasaSumPdf.rpt";
                rptname = "FJJasa_SUMMARY";
            }
            else
            {
                rptfile = "rptARDtlJasaPdf.rpt";
                rptname = "FJJasa_DETAIL";
            }
            var Slct = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Slct += " ,ard.aritemdtloid [AR Dtl Oid], ard.aritemdtlseq [Seq], ard.itemoid [Mat Oid], m.jasacode [Mat Code] , m.jasadesc [Mat Longdesc], aritemqty [Qty] , g2.gndesc [Unit], (aritemdtlamt / (aritemqty + 0.00)) [Price], aritemdtlamt [Detail Amount], aritemdtldisctype [Disc Type], aritemdtldiscvalue [Disc Value], aritemdtldiscamt [Disc Amount], '' [Disc Type 2], 0.00 [Disc Value 2], 0.00 [Disc Amount 2], aritemdtlnetto [Dtl Netto], aritemdtlnote [AR Detail Note], 1 AS [Convert Qty], ((aritemqty + 0.00) * 1) AS [Qty Dtl], '' AS [Unit Dtl] ";
                Join += " INNER JOIN QL_trnaritemdtl ard ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid INNER JOIN QL_mstjasa m ON m.jasaoid=ard.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid=ard.aritemunitoid ";

                Slct += ", shm.shipmentitemmstoid [Shipment Oid],shipmentitemno [Shipment No.], shipmentitemdate [Shipment Date], shipmentitemvalueidr [PriceSJIDR], shipmentitemvalueusd [PriceSJUSD], '' [Shipment Container No.], ISNULL((SELECT usname FROM QL_m01US dr WHERE dr.usoid=shm.driveroid),'') [Shipment Driver], (SELECT vhcdesc FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Vehicle], (SELECT vhcno FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Police No.], shm.shipmentitemmststatus [Shipment Status], arm.aritemfaktur [Shipment SJ Pajak], '' [Shipment Seal No.], shipmentitemmstnote[Shipment Header Note], shm.createuser[Shipment Create User], shm.createtime[Shipment Create Datetime], '' AS[Shipment User Approval], CURRENT_TIMESTAMP AS[Shipment ApprovalDate] ";

                Join += " INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentitemmstoid=ard.shipmentitemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid ";

                Slct += " , 'APIS' AS [Divisi], som.soitemmstoid [SO ID], soitemno [SO No.], soitemdate [SO Date], ((SELECT gndesc FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnoid=soitempaytypeoid)) [SO Payment Type], '' [SO Price Type], som.soitemtype [SO Local/Exp], '' AS [SO Direct Cust PO No.], 0 AS [Payment Method], CURRENT_TIMESTAMP [SO ETD], '' [Invoice Value],'' [SO Account No.], '' [Port Of Ship], '' [Port Of Dicharge], soitemmstnote [SO Header Note], soitemmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], CURRENT_TIMESTAMP AS [App User SO], CURRENT_TIMESTAMP AS [App Date SO]";

                Join += " INNER JOIN QL_trnsoitemmst som ON som.cmpcode=arm.cmpcode AND som.soitemmstoid=arm.somstoid ";
            }

            sSql = "SELECT arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], CURRENT_TIMESTAMP [AR Date Take Giro], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], arm.aritemratetoidr [Rate IDR], cast(arm.aritemratetousd as Float) [Rate USD], arm.aritemtotalamt [AR Total Amt], arm.aritemtotaldisc [AR Total Disc], arm.aritemtotalnetto [Total Netto], arm.aritemtaxtype [Tax Type], arm.aritemtaxvalue [AR Tax Value], arm.aritemtaxamt [AR Tax Amount], arm.aritemgrandtotal [AR Grand Total], arm.cmpcode AS [BU Name], arm.aritemmstnote AS [AR Header Note], ISNULL(arm.aritemmstres3,'') AS [Invoice No], arm.createuser [AR Create User], arm.createtime [AR Create Time], '' AS [AR User Approval], CURRENT_TIMESTAMP AS [AR ApprovalDate], aritemfaktur [No. Faktur] " + Slct + ", ISNULL(rabno,'') [No. RAB], ISNULL(rm.rabdate,1/1/1900) [RAB Date] FROM QL_trnaritemmst arm " + Join + " INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid=rm.rabmstoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemtype='Jasa'";


            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            //if (TextNomorPO != "")
            //{
            //    string[] arr = TextNomorPO.Split(';'); string filterdata = "";
            //    for (int i = 0; i < arr.Count(); i++)
            //    {
            //        filterdata += "'" + arr[i] + "',";
            //    }
            //    sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            //}

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.jasacode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            //sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}