﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class PGReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public PGReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_conmat tbl)
        {
            sSql = "SELECT * FROM QL_m05GN g WHERE gngroup='GUDANG' ORDER BY g.gndesc";
            var DDLWH = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.mtrwhoid);
            ViewBag.DDLWH = DDLWH;
        }

        [HttpPost]
        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, mt.transitemmstoid [ID], transitemdate [TransferDate], transitemno [Transfer No.], CONVERT(VARCHAR(10), mt.transitemmstoid) [Draft No.], transitemdocrefno [Doc Ref No], transitemmstnote [Header Note], transitemmststatus [Status] FROM QL_trntransitemmst mt WHERE mt.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND mt.transitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY mt.transitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string DDLPono, string TextNomorPO, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, m.itemcode,itemdesc,mtd.transitemdtlnote FROM QL_trntransitemdtl mtd INNER JOIN QL_trntransitemmst mt ON mt.transitemmstoid = mtd.transitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = mtd.itemoid  WHERE mt.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND mt.transitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }


                if (TextNomorPO != "")
                {
                    string[] arr = TextNomorPO.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_conmat tbl;
            tbl = new QL_conmat();
            if (tbl == null)
                return HttpNotFound();
            InitDDL(tbl);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono, string DDLWH)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptTrans_Sum.rpt";
                //if (reporttype == "XLS")
                //    rptfile = "rptPRABSumXls.rpt";
                //else
                //    rptfile = "rptPRABSumPdf.rpt";
                rptname = "TRANS_SUMMARY";
            }
            else
            {
                rptfile = "rptTrans_Dtl.rpt";
                //if (reporttype == "XLS")
                //    rptfile = "rptPRABDtlXls.rpt";
                //else
                //{
                //    rptfile = "rptPRABDtlPdf.rpt";
                //}
                rptname = "TRANS_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = ", fw.gndesc AS [From Warehouse], tw.gndesc AS [To Warehouse], m.itemcode AS [Material Code], m.itemdesc AS [Material], mtd.transitemqty [Qty], mtd.refno [PID], ISNULL(mtd.serialnumber,'') [serialnumber], u.gndesc AS [Unit], mtd.transitemdtlnote AS [Detail Note], mtd.transitemdtloid [Dtloid]";

                Join = " INNER JOIN QL_trntransitemdtl mtd ON mtd.transitemmstoid=mt.transitemmstoid INNER JOIN QL_m05GN fw ON fw.gnoid=mtd.transitemfromwhoid INNER JOIN QL_m05GN tw ON tw.gnoid=mtd.transitemtowhoid INNER JOIN QL_mstitem m ON m.itemoid=mtd.itemoid INNER JOIN QL_m05GN u ON u.gnoid=mtd.transitemunitoid";
            }
            sSql = "SELECT mt.cmpcode, '" + CompnyName + "' [Bussines Unit], mt.transitemmstoid [ID], transitemdate [TransferDate], transitemno [Transfer No.], CONVERT(VARCHAR(10), mt.transitemmstoid) [Draft No.], transitemdocrefno [Doc Ref No], transitemmstnote [Header Note], transitemmststatus [Status], mt.createuser [Create User], mt.createtime [CreateDate],(CASE mt.transitemmststatus WHEN 'In Process' THEN '' ELSE mt.upduser END) AS [Post User] , (CASE mt.transitemmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mt.updtime END) AS [Post Date], 0.00 [Transfer To], 0.00 [Address], 0 transitemmstcityOid, '' [City] " + Dtl + " FROM QL_trntransitemmst mt " + Join + " WHERE mt.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND mt.transitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextNomorPO != "")
            {
                string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (DDLWH != null)
                {
                    if (DDLWH.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLWH.Count(); i++)
                        {
                            stsval += "'" + DDLWH[i] + "',";
                        }
                        sSql += " AND mt.transitemfromwhoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
            }

            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}