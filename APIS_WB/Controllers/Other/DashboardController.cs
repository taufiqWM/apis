﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Other
{
    public class DashboardController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public DashboardController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class dtnotif
        {
            public int jml { get; set; }
            public string mnname { get; set; }
            public string ctrlname { get; set; }
            public string notiftype { get; set; }
            public string notifaction { get; set; }
            public string notiflink { get; set; }
            public string notifclass { get; set; }
            public string notifinfo { get; set; }

        }

        // GET: Notification
        [HttpPost]
        public ActionResult getNotification()
        {

            //var result = "";
            var usoid = Session["UserID"] == null ? "" : Session["UserID"].ToString();

            sSql = "select count(*) jml, mn.mnname, 'WaitingAction' ctrlname, 'Index' notifaction, 'Approval' notiftype,'' notifinfo, ('/' + ap.tablename) notiflink from QL_APP ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform INNER JOIN QL_m01US us ON us.stoid=ap.appstoid WHERE ISNULL(appstatus,'')='' AND us.usoid = '" + usoid + "' GROUP BY mn.mnname, ap.appform, ap.tablename";
            var dtnotif = db.Database.SqlQuery<dtnotif>(sSql).ToList();

            for (int i = 0; i < dtnotif.Count(); i++)
            {
                dtnotif[i].notiflink = Url.Action(dtnotif[i].notifaction + dtnotif[i].notiflink, dtnotif[i].ctrlname);

                if (dtnotif[i].notiftype == "Approval")
                {
                    dtnotif[i].notifclass = "fa fa-envelope-o";
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu Approval ";
                }
                else
                {
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu " + dtnotif[i].notiftype;
                    dtnotif[i].notifclass = "fa fa-bell-o";
                }

            }

            return Json(dtnotif, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getNotification2()
        {

            //var result = "";
            var usoid = Session["UserID"] == null ? "" : Session["UserID"].ToString();

            sSql = "select count(*) jml, mn.mnname, 'WaitingAction2' ctrlname, 'Index' notifaction, 'Approval' notiftype,'' notifinfo, ('/' + ap.tablename) notiflink from QL_APP2 ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform INNER JOIN QL_m01US us ON us.usoid=ap.appuser WHERE ISNULL(appstatus,'')='' AND us.usoid = '" + usoid + "' GROUP BY mn.mnname, ap.appform, ap.tablename";
            var dtnotif = db.Database.SqlQuery<dtnotif>(sSql).ToList();

            for (int i = 0; i < dtnotif.Count(); i++)
            {
                dtnotif[i].notiflink = Url.Action(dtnotif[i].notifaction + dtnotif[i].notiflink, dtnotif[i].ctrlname);

                if (dtnotif[i].notiftype == "Approval")
                {
                    dtnotif[i].notifclass = "fa fa-envelope-o";
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu Approval ";
                }
                else
                {
                    dtnotif[i].notifinfo = "" + dtnotif[i].jml + " data " + dtnotif[i].mnname + " menunggu " + dtnotif[i].notiftype;
                    dtnotif[i].notifclass = "fa fa-bell-o";
                }

            }

            return Json(dtnotif, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataListPO()
        {
            var msg = ""; var lisths = "";

            sSql = "SELECT d.mnfileloc FROM QL_m03UR a INNER JOIN QL_m02RL b ON b.rloid=a.rloid INNER JOIN QL_m02RLdtl c ON c.rloid=b.rloid INNER JOIN QL_m04MN d ON d.mnoid=c.mnoid WHERE usoid='" + Session["UserID"] + "' AND d.mnfileloc='PO'";

            DataTable tblcek = new ClassConnection().GetDataTable(sSql, "cekmenuPO");

            if (tblcek.Rows.Count > 0)
            {
                sSql = "SELECT pom.cmpcode, pom.poitemmstoid [Draft No.], CONVERT(VARCHAR(10), pom.poitemdate, 103) [Tgl PO], pom.poitemno [No PO], s.suppname [Supplier], CONVERT(VARCHAR(10),(SELECT TOP 1 (sod.soitemdtletd) FROM QL_trnsoitemdtl sod INNER JOIN QL_trnsoitemmst som ON sod.soitemmstoid=som.soitemmstoid WHERE som.soitemmstoid=pom.somstoid ORDER BY sod.soitemdtletd DESC), 103) [ETD], poitemmstnote [Note] FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa' AND pom.poitemmststatus IN('Approved') AND DATEDIFF(D,(SELECT TOP 1 (sod.soitemdtletd) FROM QL_trnsoitemdtl sod INNER JOIN QL_trnsoitemmst som ON sod.soitemmstoid=som.soitemmstoid WHERE som.soitemmstoid=pom.somstoid ORDER BY sod.soitemdtletd DESC),GETDATE())>0 ORDER BY pom.poitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "ListNotifPO");

                if (tbl.Rows.Count > 0)
                {
                    lisths = "PO";
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Draft No.")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + item + "", "PO") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    return Json(new { msg, colname, rows, lisths }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataListItemSpec()
        {
            var msg = ""; 
            sSql = "SELECT i.itemoid [ID], itemcode [Kode], itemdesc [Nama Barang], i.itemspec [Specification], ISNULL((SELECT SUM(c.qtyin - c.qtyout) FROM QL_conmat c WHERE c.refoid=i.itemoid),0.0) [DEC_Free Stock], ISNULL((SELECT TOP 1 harga FROM( SELECT pom.poitemdate tanggal, pod.poitemprice harga, pod.itemoid refoid FROM QL_trnpoitemmst pom INNER JOIN QL_trnpoitemdtl pod ON pod.poitemmstoid=pom.poitemmstoid WHERE pom.poitemmststatus IN('Approved','Post','Closed') AND ISNULL(pom.poitemmstres1,'')='' UNION ALL SELECT pom.poassetdate tanggal, pod.poassetprice harga, pod.poassetrefoid refoid FROM QL_trnpoassetmst pom INNER JOIN QL_trnpoassetdtl pod ON pod.poassetmstoid=pom.poassetmstoid WHERE pom.poassetmststatus IN('Approved','Post','Closed') AND ISNULL(pom.poassetmstres1,'')='') AS t WHERE t.refoid=i.itemoid ORDER BY t.tanggal DESC),0.0) [DEC_Harga Beli Terakhir] FROM QL_mstitem i WHERE ISNULL(i.itemspec,'')<>'' ";

            DataTable tbl = new ClassConnection().GetDataTable(sSql, "ListNotifPO");

            if (tbl.Rows.Count > 0)
            {
                List<string> colname = new List<string>();

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in tbl.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in tbl.Columns)
                    {
                        var item = dr[col].ToString();
                        decimal decval = 0;
                        try
                        {
                            if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                            {
                                if (decval == 0)
                                    item = "0.00";
                                else
                                    item = decval.ToString("#,##0.00");
                            }
                        }
                        catch
                        {
                            item = dr[col].ToString();
                        }
                        row.Add(col.ColumnName.Replace("DEC_", ""), item);
                        if (!colname.Contains(col.ColumnName.Replace("DEC_", "")))
                            colname.Add(col.ColumnName.Replace("DEC_", ""));
                    }
                    rows.Add(row);
                }

                return Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
            }
            

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            
            return View();
        }
    }
}