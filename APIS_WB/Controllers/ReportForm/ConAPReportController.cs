﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class ConAPReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public ConAPReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL()
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";            
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            if (DDLBusinessUnit.Count() > 0)
            {
                sSql = "SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conap con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE payrefoid=0";
                var DDLCOA = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList(), "acctgoid", "acctgdesc");
                ViewBag.DDLCOA = DDLCOA;
            }

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2013;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }

        [HttpPost]
        public ActionResult GetSuppData(string DDLBusinessUnit, string StartDate, string EndDate, string DDLType, string TextSupplier)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq,suppcode [Code],suppname [Name],suppaddr [Address] FROM QL_mstsupp WHERE (suppcode LIKE '%" + "%' OR suppname LIKE '%" + "%') ORDER BY suppname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetSuppOidByCode(string TextSupplier)
        {
            return db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + CAST(suppoid AS VARCHAR(10)) FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppcode IN ('" + TextSupplier.Replace(";", "','") + "') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<ReportModels.DDLAccountModel> tbl = new List<ReportModels.DDLAccountModel>();

            try
            {
                sSql = "SELECT DISTINCT a.acctgoid, acctgdesc FROM QL_conap con INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE payrefoid=0";
                tbl = db.Database.SqlQuery<ReportModels.DDLAccountModel>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: ARReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("ConAPReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {                
                case "Index":
                    ViewBag.FormType = "Finish Goods";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }


        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string StartDate, string EndDate, string TextSupplier, string DDLType, string ReportType, string DDLCurrency, bool cbInvoice, bool cbHide, string DDLCOA, string DDLMonth, string DDLYear, string DDLYear_Text, string DDLMonth_Text, string DDLBusinessUnit_Text)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var swhere = ""; var swheresupp = "";
            var suppoid = GetSuppOidByCode(TextSupplier);
            if (!string.IsNullOrEmpty(TextSupplier))
            {
                swhere = " WHERE suppoid IN (" + suppoid.Replace(";", "', ") + ")";
                swheresupp = " WHERE s.suppoid IN (" + suppoid.Replace(";", "', ") + ")";
            }

            var sFilterCOA = "";

            if (DDLCOA != "")
            {
                sFilterCOA = " AND c.acctgoid=" + DDLCOA;
                sSql = "SELECT DISTINCT acctgoid FROM QL_conap con WHERE cmpcode='" + CompnyCode + "' AND payrefoid<>0 AND acctgoid NOT IN (SELECT DISTINCT conx.acctgoid FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.payrefoid=0 AND conx.suppoid=con.suppoid)";
                DataTable dtTmp = new ClassConnection().GetDataTable(sSql, "QL_conapacctg");

                if (!string.IsNullOrEmpty(TextSupplier))
                    swheresupp = " AND suppcode IN (" + TextSupplier.Replace(";", "', ") + "')";
            }

            var DDLMonthUp = DDLMonth_Text.ToUpper();
            var DDLBusUp = DDLBusinessUnit_Text.ToUpper();

            sSql = "DECLARE @cmpcode AS VARCHAR(10);" +
                "DECLARE @periodacctg AS VARCHAR(10);" +
                "DECLARE @currency AS VARCHAR(10);" +
                "DECLARE @dateacuan AS DATETIME;" +
                "SET @cmpcode='" + CompnyCode + "';" +
                "SET @periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "';" +
               "SET @dateacuan=CAST('" + new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), DateTime.DaysInMonth(int.Parse(DDLYear), int.Parse(DDLMonth))).ToString("MM/dd/yyyy") + " 23:59:59' AS DATETIME);" +
                "SET @currency='" + DDLCurrency + "';" +
                "/*UNION ALL QUERY*/" +
                "SELECT suppoid, suppname, SUM(saidr) saidr, SUM(sausd) sausd, SUM(beliidr) beliidr, " +
                "SUM(beliusd) beliusd, SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, " +
                "SUM(ageblmjtidr) ageblmjtidr, SUM(ageblmjtusd) ageblmjtusd, " +
                "SUM(age1idr) age1idr, SUM(age1usd) age1usd, SUM(age2idr) age2idr, SUM(age2usd) age2usd, " +
                "SUM(age3idr) age3idr, SUM(age3usd) age3usd, @currency currency, @cmpcode cmpcode, " +
                "'" + DDLMonthUp + " " + DDLYear_Text + "' periodreport, '" + DDLBusUp + "' company, acctgoid, (SELECT acctgcode FROM QL_mstacctg acc WHERE acc.acctgoid=tbldata.acctgoid) kodeHutang, (SELECT acctgdesc FROM QL_mstacctg acc WHERE acc.acctgoid=tbldata.acctgoid) descHutang " +
                "FROM (" +
                "/*SALDO AWAL PERIODE*/" +
                "SELECT suppoid, suppname, " +
                "(SUM(beliidr) - SUM(paymentidr)) saidr, " +
                "(SUM(beliusd) - SUM(paymentusd)) sausd, " +
                "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid FROM (" +
                "SELECT suppoid, suppname, " +
                "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND c.conapoid>0 " +
                "AND payrefoid=0 AND trnapstatus='Post' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, ISNULL((SELECT acctgoid FROM QL_conap c2 WHERE c2.cmpcode=c.cmpcode AND c2.reftype=c.reftype AND c2.refoid=c.refoid AND c2.suppoid=c.suppoid AND c2.payrefoid=0),0) acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND trnaptype='CNAP' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, ISNULL((SELECT acctgoid FROM QL_conap c2 WHERE c2.cmpcode=c.cmpcode AND c2.reftype=c.reftype AND c2.refoid=c.refoid AND c2.suppoid=c.suppoid AND c2.payrefoid=0),0) acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND trnaptype='GL' AND (c.amtbayaridr<0 OR c.amtbayarusd<0) " +
                ") AS tblBeli GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL " +
                "SELECT suppoid, suppname, 0.0 beliidr, 0.0 beliusd," +
                "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, c.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN QL_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'GL') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<@periodacctg " + sFilterCOA + " GROUP BY con.suppoid, con.cmpcode, c.acctgoid) AS c ON c.suppoid=s.suppoid " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND c.trnaptype='DNAP' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg<@periodacctg AND trnapstatus='Post' AND c.trnaptype='GL' AND (c.amtbayaridr>0 OR c.amtbayarusd>0) " +
                ") AS tblpayment GROUP BY suppoid, suppname, acctgoid " +
                ") AS tblSaldoAwalPeriod GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL /*SALDO AWAL CUT OFF*/" +
                "SELECT s.suppoid, s.suppname, " +
                "(SUM(ISNULL(c.amttransidr, 0.0)) - SUM(ISNULL(c.amtbayaridr, 0.0))) AS saidr, " +
                "(SUM(ISNULL(c.amttransusd, 0.0)) - SUM(ISNULL(c.amtbayarusd, 0.0))) AS sausd, " +
                "0.0 beliidr, 0.0 beliusd, 0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.conapoid<0 " +
                "GROUP BY s.suppoid, s.suppname, c.acctgoid " +
                "UNION ALL /*PEMBELIAN*/" +
                "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, " +
                "SUM(ISNULL(beliidr, 0.0)) beliidr, SUM(ISNULL(beliusd, 0.0)) beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amttransidr, 0.0) beliidr, ISNULL(c.amttransusd, 0.0) beliusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND c.conapoid>0 " +
                "AND payrefoid=0 AND trnapstatus='Post' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND trnaptype='CNAP' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname, " +
                "ISNULL(c.amtbayaridr * -1, 0.0) beliidr, ISNULL(c.amtbayarusd * -1, 0.0) beliusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND trnaptype='GL' AND (c.amtbayaridr<0 OR c.amtbayarusd<0) " +
                ") AS tblBeli GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL /*PAYMENT*/" +
                "SELECT suppoid, suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd," +
                "SUM(paymentidr) paymentidr, SUM(paymentusd) paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, " +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, c.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid INNER JOIN QL_conap c ON c.cmpcode=con.cmpcode AND c.reftype=con.reftype AND c.refoid=con.refoid AND c.payrefoid=0 WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)=@periodacctg " + sFilterCOA + " GROUP BY con.suppoid, con.cmpcode, c.acctgoid) AS c ON c.suppoid=s.suppoid " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND c.trnaptype='DNAP' " +
                "UNION ALL " +
                "SELECT s.suppoid, s.suppname," +
                "c.amtbayaridr paymentidr, c.amtbayarusd paymentusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid " + sFilterCOA + " " +
                "AND c.cmpcode=@cmpcode AND c.periodacctg=@periodacctg AND trnapstatus='Post' AND c.trnaptype='GL' AND (c.amtbayaridr>0 OR c.amtbayarusd>0) " +
                ") AS tblpayment GROUP BY suppoid, suppname, acctgoid " +
                "UNION ALL /*BELUM JATUH TEMPO*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) ageblmjtidr, SUM(ag.amttransusd - ag.amtbayarusd) ageblmjtusd, " +
                "0.0 age1idr, 0.0 age1usd, 0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate>@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<=@periodacctg " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'GL') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'GL') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, ag.acctgoid " +
                "UNION ALL /*0-30 DAYS*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) age1idr, SUM(ag.amttransusd - ag.amtbayarusd) age1usd, " +
                "0.0 age2idr, 0.0 age2usd, 0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))=@periodacctg " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'GL') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'GL') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, ag.acctgoid " +
                "UNION ALL /*31-60 DAYS*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) age2idr, SUM(ag.amttransusd - ag.amtbayarusd) age2usd, " +
                "0.0 age3idr, 0.0 age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))='" + ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1))) + "' " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'GL') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'GL') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, ag.acctgoid " +
                "UNION ALL /*>60 DAYS*/" +
                "SELECT ag.suppoid, ag.suppname, 0.0 saidr, 0.0 sausd, 0.0 beliidr, 0.0 beliusd, " +
                "0.0 paymentidr, 0.0 paymentusd, 0.0 ageblmjtidr, 0.0 ageblmjtusd, 0.0 age1idr, 0.0 age1usd, " +
                "0.0 age2idr, 0.0 age2usd, " +
                "SUM(ag.amttransidr - ag.amtbayaridr) age3idr, SUM(ag.amttransusd - ag.amtbayarusd) age3usd, acctgoid " +
                "FROM (" +
                "SELECT s.suppoid, s.suppname, ISNULL(c.reftype, '') reftype, ISNULL(c.refoid, 0) refoid, " +
                "ISNULL(c.amttransidr, 0.0) amttransidr, SUM(ISNULL(c1.amtbayaridr, 0.0)) amtbayaridr, " +
                "ISNULL(c.amttransusd, 0.0) amttransusd, SUM(ISNULL(c1.amtbayarusd, 0.0)) amtbayarusd, c.acctgoid " +
                "FROM QL_mstsupp s " +
                "LEFT JOIN QL_conap c ON s.suppoid=c.suppoid AND c.cmpcode=@cmpcode " + sFilterCOA + " " +
                "AND c.payduedate<=@dateacuan AND c.payrefoid=0 " +
                "AND (RIGHT(CONVERT(VARCHAR(10), c.trnapdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), c.trnapdate, 101), 2))<'" + ClassFunction.GetLastPeriod(ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1))) + "' " +
                "LEFT JOIN (SELECT SUM(con.amtbayaridr) amtbayaridr, SUM(con.amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.cmpcode=pay.cmpcode AND con.payrefoid=pay.payapoid WHERE cb.cmpcode=@cmpcode AND ISNULL(payapres1, '')<>'lebih bayar' AND cashbankstatus='post' AND con.trnaptype NOT IN ('DNAP', 'CNAP', 'GL') AND (CASE WHEN LEFT(cashbanktype, 2)='BB' THEN (RIGHT(CONVERT(VARCHAR(10), cashbankduedate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankduedate, 101), 2)) ELSE (RIGHT(CONVERT(VARCHAR(10), cashbankdate, 101), 4) + LEFT(CONVERT(VARCHAR(10), cashbankdate, 101), 2)) END)<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid UNION ALL SELECT SUM(amtbayaridr) amtbayaridr, SUM(amtbayarusd) amtbayarusd, con.suppoid, con.cmpcode, con.reftype, con.refoid FROM QL_conap con WHERE con.cmpcode=@cmpcode AND trnapstatus='post' AND trnaptype IN ('DNAP', 'CNAP', 'GL') AND con.periodacctg<=@periodacctg GROUP BY con.suppoid, con.cmpcode, con.reftype, con.refoid) c1 ON c.cmpcode=c1.cmpcode AND c.suppoid=c1.suppoid AND c.reftype=c1.reftype AND c.refoid=c1.refoid " +
                "GROUP BY s.suppoid, s.suppname, c.amttransidr, c.amttransusd, c.reftype, c.refoid, c.acctgoid " +
                ") AS ag GROUP BY ag.suppoid, ag.suppname, ag.acctgoid " +
                ") AS tbldata " +
                swhere +
                "GROUP BY suppoid, suppname, acctgoid ";

            if (cbHide)
            {
                if (DDLCurrency == "IDR")
                    sSql += "HAVING SUM(saidr)<>0 OR SUM(beliidr)<>0 OR SUM(paymentidr)<>0 OR SUM(age1idr)<>0 OR SUM(age2idr)<>0 OR SUM(age3idr)<>0 ";
                else if (DDLCurrency == "USD")
                    sSql += "HAVING SUM(sausd)<>0 OR SUM(beliusd)<>0 OR SUM(paymentusd)<>0 OR SUM(age1usd)<>0 OR SUM(age2usd)<>0 OR SUM(age3usd)<>0 ";
                else
                    sSql += "HAVING SUM(saidr)<>0 OR SUM(sausd)<>0 OR SUM(beliidr)<>0 OR SUM(beliusd)<>0 OR SUM(paymentidr)<>0 OR SUM(paymentusd)<>0 OR SUM(age1idr)<>0 OR SUM(age1usd)<>0 OR SUM(age2idr)<>0 OR SUM(age2usd)<>0 OR SUM(age3idr)<>0 OR SUM(age3usd)<>0 ";
            }
            sSql += "ORDER BY suppname";
            //var sGroup = "";
            //if (cbInvoice)
            //    sGroup = "PerNota";

            var rptfile = ""; var rptname = "ACCOUNTPAYABLEREPORT";

            if (DDLType == "Summary")
            {
                if (ReportType == "XLS")
                    rptfile = "rptAPXls.rpt";
                else
                    rptfile = "rptAP.rpt";
                //DataTable dt = new ClassConnection().GetDataTable(sSql, "tbldata");
            }
            else
            {
                if (ReportType == "XLS")
                {
                    //if (DDLCurrency == "TransactionRate")
                    //    rptfile = "crKartuAPDtl" + sGroup + "Xls.rpt";
                    //else
                        rptfile = "crKartuAPDtlXls.rpt";
                }
                else
                {
                    //if (DDLCurrency == "TransactionRate")
                    //    rptfile = "crKartuAPDtl" + sGroup + ".rpt";
                    //else
                        rptfile = "crKartuAPDtl.rpt";
                }
            }

            var strLastModified = System.IO.File.GetLastWriteTime(rptfile.ToString());
            System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
            var awal = DateTime.ParseExact(StartDate, "MM/dd/yyyy", AppsCultureInfo);
            var akhir = DateTime.ParseExact(EndDate, "MM/dd/yyyy", AppsCultureInfo);

            DataTable dtRpt = null;
            //DataTable dtTmp = null;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            var type = DDLType.ToUpper();
            if (type == "DETAIL")
            {
                rptparam.Add("cmpcode", CompnyCode);
                rptparam.Add("start", awal);
                rptparam.Add("finish", akhir);

                if (cbInvoice)
                {
                    var sAcctg = "";
                    if (DDLCOA != "")
                    {
                        sAcctg = " AND a.acctgoid=" + DDLCOA + "";
                    }
                    rptparam.Add("swhere", swheresupp + sAcctg);
                }
                else
                {
                    rptparam.Add("swhere", swheresupp);
                }

                if (cbHide)
                {
                    if (DDLCurrency == "IDR")
                        rptparam.Add("swherezero", " WHERE sawalidr>0 OR ISNULL(amtinidr,0)>0 OR ISNULL(amtoutidr,0)>0 ");
                    else if (DDLCurrency == "USD")
                        rptparam.Add("swherezero", " WHERE sawalusd>0 OR ISNULL(amtinusd,0)>0 OR ISNULL(amtoutusd,0)>0 ");
                    else
                        rptparam.Add("swherezero", " WHERE sawal>0 OR sawalidr>0 OR sawalusd>0 OR ISNULL(amtin,0)>0 OR ISNULL(amtinidr,0)>0 OR ISNULL(amtinusd,0)>0 OR ISNULL(amtout,0)>0 OR ISNULL(amtoutidr,0)>0 OR ISNULL(amtout,0)>0 ");
                }
                else
                {
                    rptparam.Add("swherezero", "");
                }

                rptparam.Add("currvalue", DDLCurrency);
                rptparam.Add("companyname", DDLBusinessUnit_Text);
                rptparam.Add("periodreport", Convert.ToDateTime(StartDate).ToString("dd MM yyyy") + "-" + Convert.ToDateTime(EndDate).ToString("dd MM yyyy"));
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("PrintLastModified", strLastModified);
                rptparam.Add("RptFile", rptfile);
                if (DDLCOA != "")
                {
                    rptparam.Add("swhereacctg", " AND ap.acctgoid=" + DDLCOA + "");
                    rptparam.Add("swhereacctg2", " AND ap2.acctgoid=" + DDLCOA + "");
                }
                else
                {
                    rptparam.Add("swhereacctg", "");
                    rptparam.Add("swhereacctg2", "");
                }
            }
            else
            {
                dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
                rptparam.Add("PrintLastModified", strLastModified);
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("RptFile", rptfile);
            }
            if (ReportType == "")
            {
                if (DDLType == "Summary")
                {
                    this.HttpContext.Session["rptsource"] = dtRpt;
                    //this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperOrientation.Landscape;
                    //this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    //if (DDLCurrency == "TransactionRate")
                    //{
                    //    //this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperOrientation.Portrait;
                    //}
                    //else
                    //{
                    //    //this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperOrientation.Landscape;
                    //    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    //}
                }
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                if (DDLType == "Summary")
                {
                    report.SetDataSource(dtRpt);
                }
                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    ClassProcedure.SetDBLogonForReport(report);
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }

}