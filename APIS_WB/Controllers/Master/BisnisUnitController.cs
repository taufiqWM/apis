﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class BisnisUnitController : Controller
    {
        // GET: BisnisUnit
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public BisnisUnitController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ListCat1
        {
            public string cat1code { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat1note { get; set; }
            public string activeflag { get; set; }

        }

        private void InitDDL(QL_mstdivision tbl)
        {
            sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.divcityoid);
            ViewBag.divcityoid = cityoid;
        }

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";
            sSql = "SELECT i.divoid [oid], i.divcode [Kode], i.divname [Nama], i.divaddress [Alamat], i.divemail [Email], i.divnote [Keterangan] FROM QL_mstdivision i INNER JOIN QL_m05gn g ON g.gnoid = i.divcityoid WHERE i.cmpcode = '" + CompnyCode + "' ORDER BY divcode DESC";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstdivision");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Kode")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["oid"].ToString(), "BisnisUnit") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdivision tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_mstdivision();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tbl = db.QL_mstdivision.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstdivision tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.divname))
                tbl.divname = "";
            if (string.IsNullOrEmpty(tbl.divaddress))
                tbl.divaddress = "";
            if (string.IsNullOrEmpty(tbl.divphone))
                tbl.divphone = "";
            if (string.IsNullOrEmpty(tbl.divfax1))
                tbl.divfax1 = "";
            if (string.IsNullOrEmpty(tbl.divemail))
                tbl.divemail = "";
            if (string.IsNullOrEmpty(tbl.divpostcode))
                tbl.divpostcode = "";

            tbl.divphone2 = "";
            tbl.divfax2 = "";

            var servertime = ClassFunction.GetServerTime();
            //var mstoid = ClassFunction.GenerateID("QL_mstdivision");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            ////Insert
                            //tbl.createtime = servertime;
                            //tbl.createuser = Session["UserID"].ToString();
                            //tbl.updtime = servertime;
                            //tbl.upduser = Session["UserID"].ToString();
                            //db.QL_mstcat1.Add(tbl);
                            //db.SaveChanges();

                            ////Update lastoid
                            //sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat1'";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat1 tbl = db.QL_mstcat1.Find(id);
                        db.QL_mstcat1.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}