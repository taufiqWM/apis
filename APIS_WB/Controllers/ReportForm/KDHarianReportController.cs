﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class KDHarianReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public KDHarianReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT divcode, divname FROM QL_mstdivision";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;
            //if (DDLBusinessUnit.Count() > 0)
            //{
            sSql = "SELECT * FROM QL_mstcurr WHERE currcode IN ('IDR', 'USD') order by currcode";
            var DDLCurrency = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "currcode", "currcode");
            ViewBag.DDLCurrency = DDLCurrency;
            //}

            var DDLCOA = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_BANK")).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;
        }

        [HttpPost]
        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_BANK");
                tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("KDHarianReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLType, string StartDate, string EndDate, string DDLPeriod, string DDLCurrency, string DDLOrderBy, string ReportType, string TextNomor, string DDLNomor, string DDLCOA)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "rptKDHarian.rpt";
            var rptname = "KebutuhanDanaHarianReport";
            var periode = DateTime.Parse(EndDate).ToString("dd MMM yyyy");

            if (DDLType == "KDH")
            {
                sSql = "SELECT cmpcode, acctgoid, acctgcode, acctgdesc, ISNULL((SELECT SUM(c.amtbalance) FROM QL_crdgl c WHERE c.acctgoid=dt.acctgoid AND c.crdgloid<0),0.0) saldo, SUM(debet) debet,  SUM(credit) credit, (ISNULL((SELECT SUM(c.amtbalance) FROM QL_crdgl c WHERE c.acctgoid=dt.acctgoid AND c.crdgloid<0),0.0)+SUM(debet)-SUM(credit)) balance FROM( SELECT gm.cmpcode, a.acctgoid, acctgcode, acctgdesc, CASE gd.gldbcr WHEN 'D' THEN SUM(glamtidr) ELSE 0.0 END debet, CASE gd.gldbcr WHEN 'C' THEN SUM(glamtidr) ELSE 0.0 END credit FROM QL_trnglmst gm INNER JOIN QL_trngldtl gd ON gd.glmstoid=gm.glmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=gd.acctgoid WHERE (acctgcode LIKE'%11001%' OR acctgcode LIKE'%11002%' OR acctgcode LIKE'111%') AND gm.gldate<='" + DateTime.Parse(EndDate) + "' GROUP BY gm.cmpcode, a.acctgoid, acctgcode, acctgdesc, gd.gldbcr )AS dt GROUP BY cmpcode, acctgoid, acctgcode, acctgdesc ORDER BY acctgcode";
            }
            else if (DDLType == "BG")
            {
                rptfile = "rptBankGaransi.rpt";
                rptname = "BankGaransiReport";

                sSql = "SELECT '' cmpcode, 0 acctgoid, '' acctgcode, '' acctgdesc, 0.0 saldo, 0.0 debet,  0.0 credit, 0.0 balance";
            }
            else if (DDLType == "JM")
            {
                rptfile = "rptJaminan.rpt";
                rptname = "JaminanReport";

                sSql = "SELECT '' cmpcode, 0 acctgoid, '' acctgcode, '' acctgdesc, 0.0 saldo, 0.0 debet,  0.0 credit, 0.0 balance";
            }
            else if (DDLType == "PD")
            {
                rptfile = "rptPenerimaanDana.rpt";
                rptname = "PenerimaanDanaReport";

                sSql = "SELECT a.cmpcode, a.acctgdesc, SUM(py.payapamt) amt FROM QL_mstacctg a INNER JOIN QL_trncashbankmst cb ON cb.acctgoid=a.acctgoid INNER JOIN QL_trnpayap py ON py.cashbankoid=cb.cashbankoid WHERE a.acctgcode LIKE'111%' AND cb.cashbankstatus IN('Post','Approved','Closed') AND cb.cashbankdate='" + DateTime.Parse(EndDate) + "' GROUP BY a.cmpcode, a.acctgcode, a.acctgdesc ORDER BY a.acctgcode";
            }
            else if (DDLType == "PH")
            {
                rptfile = "rptProgressHutang.rpt";
                rptname = "ProgressHutangReport";
                periode = DateTime.Parse(StartDate).ToString("dd MMM yyyy")  + " - " + DateTime.Parse(EndDate).ToString("dd MMM yyyy");

                sSql = "SELECT pom.cmpcode, pom.poitemno, FORMAT(pom.poitemdate,'dd/MM/yyyy') tglpo, suppname, g.gndesc paytype, FORMAT(DATEADD(DAY, CAST(g.gnother1 AS INTEGER), pom.poitemdate),'dd/MM/yyyy') tgljt, pom.poitemtotalamt dpppo, pom.poitemtotaltaxamt ppnpo, pom.poitemgrandtotalamt totalpo ";
                sSql += ", ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid = pom.rabmstoid),'') sono, ISNULL((SELECT c.custname FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid WHERE som.rabmstoid = pom.rabmstoid),'') customer, ISNULL((SELECT som.projectname FROM QL_trnrabmst som WHERE som.rabmstoid = pom.rabmstoid),'') project, ISNULL((SELECT us.usname FROM QL_trnsoitemmst som  INNER JOIN QL_m01US us ON us.usoid = som.salesoid WHERE som.rabmstoid = pom.rabmstoid),'') sales";
                sSql += ", CAST(ROUND(pom.poitemtotalamt - CAST(ROUND(ISNULL((SELECT SUM(mrd.mritemqty * pod.poitemprice) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.poitemdtloid = mrd.podtloid WHERE pod.poitemmstoid = pom.poitemmstoid), 0.0), 0) AS DECIMAL(18, 4)),0) AS DECIMAL(18, 4)) mramtbefore, CAST(ROUND(ISNULL((SELECT SUM(mrd.mritemqty * pod.poitemprice) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.poitemdtloid = mrd.podtloid WHERE pod.poitemmstoid = pom.poitemmstoid), 0.0), 0) AS DECIMAL(18, 4)) mramt, STUFF((SELECT ', ' + ft.cashbankoid FROM(SELECT DISTINCT FORMAT(mrm.mritemdate, 'dd/MM/yyyy') cashbankoid FROM QL_trnmritemmst mrm WHERE mrm.pomstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, '') mrdate";
                sSql += ", CAST(ROUND(pom.poitemtotalamt - CAST(ROUND(ISNULL((SELECT SUM(mrd.apitemqty * mrd.apitemprice) FROM QL_trnapitemdtl mrd INNER JOIN QL_trnapitemmst mrm ON mrm.apitemmstoid=mrd.apitemmstoid WHERE mrm.poitemmstoid = pom.poitemmstoid), 0.0), 0) AS DECIMAL(18, 4)),0) AS DECIMAL(18, 4)) apamtbefore, CAST(ROUND(ISNULL((SELECT SUM(mrd.apitemqty * mrd.apitemprice) FROM QL_trnapitemdtl mrd INNER JOIN QL_trnapitemmst mrm ON mrm.apitemmstoid=mrd.apitemmstoid WHERE mrm.poitemmstoid = pom.poitemmstoid), 0.0), 0) AS DECIMAL(18, 4)) apamt, STUFF((SELECT ', ' + ft.cashbankoid FROM(SELECT DISTINCT FORMAT(mrm.apitemdate, 'dd/MM/yyyy') cashbankoid FROM QL_trnapitemmst mrm WHERE mrm.poitemmstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, '') apdate";
                sSql += ", CAST(ROUND(ISNULL((SELECT sum(amt) FROM(SELECT SUM(prd.pretitemqty * pod.poitemprice) amt FROM QL_trnmritemdtl mrd INNER JOIN QL_trnpoitemdtl pod ON pod.poitemdtloid = mrd.podtloid INNER JOIN QL_trnpretitemdtl prd ON prd.mritemdtloid = mrd.mritemdtloid INNER JOIN QL_trnpretitemmst prm ON prm.pretitemmstoid = prd.pretitemmstoid AND ISNULL(prm.pretitemtype, '') <> 'Asset' WHERE pod.poitemmstoid = pom.poitemmstoid  UNION ALL  SELECT SUM(prd.apretitemqty * pod.poitemprice) amt FROM QL_trnapitemdtl apd INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemdtloid = apd.mritemdtloid INNER JOIN QL_trnpoitemdtl pod ON pod.poitemdtloid = mrd.podtloid INNER JOIN QL_trnapretitemdtl prd ON prd.apitemdtloid = apd.apitemdtloid INNER JOIN QL_trnapretitemmst prm ON prm.apretitemmstoid = prd.apretitemmstoid AND ISNULL(prm.apretitemmstres1, '') <> 'Asset' WHERE pod.poitemmstoid = pom.poitemmstoid)AS dt), 0.0), 0) AS DECIMAL(18, 4)) returamt, '' returdate";
                sSql += ", ISNULL((SELECT SUM(py.paydpamt) FROM QL_trnpaydp py INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = py.refoid AND py.reftype = 'QL_trnapitemmst' WHERE apm.poitemmstoid = pom.poitemmstoid),0.0) bayardp, ISNULL((SELECT SUM(py.payapamt) FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap py ON cb.cashbankoid = py.cashbankoid INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = py.refoid AND py.reftype = 'QL_trnapitemmst' WHERE apm.poitemmstoid = pom.poitemmstoid AND cb.cashbanktype = 'BKK'),0.0) bayarkas, ISNULL((SELECT SUM(py.payapamt) FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap py ON cb.cashbankoid = py.cashbankoid INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = py.refoid AND py.reftype = 'QL_trnapitemmst' WHERE apm.poitemmstoid = pom.poitemmstoid AND cb.cashbanktype = 'BBK'),0.0) bayarbank, ISNULL((SELECT SUM(py.payapamt) FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap py ON cb.cashbankoid = py.cashbankoid INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = py.refoid AND py.reftype = 'QL_trnapitemmst' WHERE apm.poitemmstoid = pom.poitemmstoid AND cb.cashbanktype = 'BGK'),0.0) bayargiro, '' bayardate";
                sSql += " FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid INNER JOIN QL_m05GN g ON g.gnoid = pom.poitempaytypeoid AND gngroup = 'PAYMENT TERM' WHERE pom.poitemmststatus IN('Approved', 'Post', 'Closed') AND ISNULL(pom.poitemmstres1, '')= '' AND ISNULL(pom.rabmstoid, 0) > 0 AND pom.poitemdate>='" + DateTime.Parse(StartDate) + "' AND pom.poitemdate<='" + DateTime.Parse(EndDate) + "'";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", periode);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable(); ;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}