﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SOFAReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string sSql = "";

        public SOFAReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        } 

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string soassetno { get; set; }
            public DateTime soassetdate { get; set; }
            public string custname { get; set; }
        }

        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT 0 seq, som.soassetmstoid, som.soassetno, soassetdate ,c.custname FROM QL_trnsoassetmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += "ORDER BY som.soassetno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }


        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnsoassetmst som WHERE som.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += ") ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string TextSO, string startdate, string enddate, string fdate)
        { 
            sSql= "SELECT DISTINCT m.itemcode, m.itemdesc, m.itemtype, g.gndesc unit FROM QL_mstitem m INNER JOIN QL_trnsoassetdtl sod ON sod.assetmstoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid = m.itemunitoid WHERE m.cmpcode = '" + CompnyCode + "' AND sod.soassetmstoid IN ( SELECT soassetmstoid FROM QL_trnsoassetmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid WHERE c.cmpcode = '" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            if (TextSO != "")
            {
                string[] arr = TextSO.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND soassetno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            sSql += ") ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("SOFAReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string soassetno)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                //rptfile = "rptSOAssetSumPdf.rpt";
                if (reporttype == "XLS")
                    rptfile = "rptSOAssetSumXls.rpt";
                else
                    rptfile = "rptSOAssetSumPdf.rpt";
                rptname = "SO_SUMMARY";
            }
            else
            {
                //rptfile = "rptSOAssetDtlPdf.rpt";
                if (reporttype == "XLS")
                    rptfile = "rptSOAssetDtlXls.rpt";
                else
                {
                    rptfile = "rptSOAssetDtlPdf.rpt";
                }
                rptname = "SO_DETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl = ",GETDATE() [ETD DTL], soassetdtlseq [No.], itemcode [Code], m.itemdesc [Material], itemnote [SKU], soassetqty [Qty], g2.gndesc [Unit], soassetprice [Price], soassetdtlamt [Detail Amount], soassetdtldisctype [Disc Dtl Type], soassetdtldiscvalue [Disc Dtl Value], soassetdtldiscamt [Disc Dtl Amt], soassetdtlnetto [Detail Netto], soassetdtlnote [Detail Note], 0.00 [Pack Volume FG]";

                Join = " INNER JOIN QL_trnsoassetdtl sod ON sod.cmpcode=som.cmpcode AND sod.soassetmstoid=som.soassetmstoid INNER JOIN QL_mstitem m ON m.itemoid = sod.assetmstoid INNER JOIN QL_m05GN g2 ON g2.gnoid = soassetunitoid";
            }
            sSql = "SELECT 'APIS' AS [Business Unit], som.cmpcode [CMPCODE], CONVERT(VARCHAR(20), som.soassetmstoid) [Draft No.], som.soassetmstoid [ID], soassettype [Type], soassetdate [Date], GETDATE() [ETD], soassetno [SO No.], custname [Customer], custcode [Customer Code], '' [Customer Ref.], '' [Cust Ref Date], currcode [Currency], g1.gndesc [Payment Type], 0.00 [Daily Rate To IDR], '' [Daily Rate To USD], rate2idrvalue [Monthly Rate To IDR], rate2usdvalue [Monthly Rate To USD], soassettotalamt [Total Amt], 0.00 [Total Disc Dtl Amt], soassettotalnetto [Total Netto], '' [Tax Type], soassettaxamt [Tax Pct], soassettaxamt [Tax Amount], soassetgrandtotalamt [Grand Total Amt], soassetmstnote [Header Note], soassetmststatus [Status], '' AS [App User], GETDATE() AS [App Date], som.createuser AS [Create User], som.createtime AS [Create Date], 'FG' AS [TipeneX], 0.00 AS [Total CBF], som.closereason, som.closeuser, som.closetime, som.cmpcode[Division], som.cmpcode[Division Code] " + Dtl + " FROM QL_trnsoassetmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid INNER JOIN QL_mstcurr curr ON curr.curroid = som.curroid INNER JOIN QL_m05GN g1 ON g1.gnoid = soassetpaytypeoid LEFT JOIN QL_mstrate2 r2 ON r2.rate2oid = som.rate2oid " + Join + " WHERE som.cmpcode='" + CompnyCode + "'";
            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND som.soassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (soassetno != null)
            {
                string[] arr = soassetno.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND som.soassetno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));

                report.SetDataSource(dtRpt);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}