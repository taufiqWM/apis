﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class FBJasaReturnController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public FBJasaReturnController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class apretitemmst
        {
            public string cmpcode { get; set; }
            public int apretitemmstoid { get; set; }
            public string apretitemno { get; set; }
            public DateTime apretitemdate { get; set; }
            public string apitemno { get; set; }
            public string suppname { get; set; }
            public string apretitemmststatus { get; set; }
            public string apretitemmstnote { get; set; }
        }

        public class apretitemdtl
        {
            public int apretitemdtlseq { get; set; }
            public int apitemdtloid { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int apretitemunitoid { get; set; }
            public string apretitemunit { get; set; }
            public decimal apretitemqty { get; set; }
            public decimal apitemqty { get; set; }
            public decimal apretitemprice { get; set; }
            public decimal apretitemdtlamt { get; set; }
            public decimal apretitemdtldisc { get; set; }
            public decimal apretitemdtltaxvalue { get; set; }
            public decimal apretitemdtltaxamt { get; set; }
            public decimal apretitemdtlnetto { get; set; }
            public string apretitemdtlnote { get; set; }
            public decimal apretitemvalueidr { get; set; }
            public int acctgoid { get; set; }
        }

        public class apitem
        {
            public int apitemmstoid { get; set; }
            public string apitemno { get; set; }
            public DateTime apitemdate { get; set; }
            public string apitemdatestr { get; set; }
            public decimal apitemgrandtotal { get; set; }
            public int soitemmstoid { get; set; }
            public string poitemno { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        public class Supp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string suppphone { get; set; }
        }

        public class trnapdtl
        {
            public decimal apqty { get; set; }
            public decimal mrvalue { get; set; }
        }

        public class warehouse
        {
            public int pretitemwhoid { get; set; }
            public string pretitemwh { get; set; }
        }

        public class listmat
        {
            public int apitemdtloid { get; set; }
            public int apretitemdtlseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int apretitemunitoid { get; set; }
            public string apretitemunit { get; set; }
            public decimal apretitemprice { get; set; }
            public decimal apretitemdtlamt { get; set; }
            public decimal apretitemdtldisc { get; set; }
            public decimal apretitemdtltaxvalue { get; set; }
            public decimal apretitemdtltaxamt { get; set; }
            public decimal apretitemdtlnetto { get; set; }
            public Decimal apretitemqty { get; set; }
            public decimal apitemqty { get; set; }
            public int acctgoid { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        public class listnopol
        {
            public string nopol { get; set; }
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + itemoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listnopol> tbl = new List<listnopol>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listnopol>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnapretitemmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var pretitemwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.pretitemwhoid = pretitemwhoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        [HttpPost]
        public ActionResult GetDetailData(int apitemmstoid, int apretitemmstoid)
        {
            List<listmat> tbl = new List<listmat>();
            sSql = "SELECT apitemdtloid, 0 apretitemdtlseq, apd.itemoid, jasacode itemcode, m.jasadesc itemdesc, m.acctgoid, ISNULL((SELECT refno FROM QL_trnmritemdtl where mritemdtloid = apd.mritemdtloid AND mritemmstoid = apd.mritemmstoid ),'') AS refno, ISNULL((SELECT serialnumber FROM QL_trnmritemdtl where mritemdtloid = apd.mritemdtloid AND mritemmstoid = apd.mritemmstoid ),'') AS serialnumber, apitemunitoid apretitemunitoid, g.gndesc AS apretitemunit, (apd.apitemqty - ISNULL((SELECT SUM(apretitemqty) FROM QL_trnapretitemdtl sretd INNER JOIN QL_trnapretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.apretitemmstoid=sretd.apretitemmstoid WHERE sretd.cmpcode=apd.cmpcode AND sretd.apitemdtloid=apd.apitemdtloid AND ISNULL(sretm.apretitemmstres1, '')='Jasa' AND apretitemmststatus<>'Rejected' AND sretd.apretitemmstoid<>" + apretitemmstoid + "), 0)) apitemqty, (apd.apitemqty - ISNULL((SELECT SUM(apretitemqty) FROM QL_trnapretitemdtl sretd INNER JOIN QL_trnapretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.apretitemmstoid=sretd.apretitemmstoid WHERE sretd.cmpcode=apd.cmpcode AND sretd.apitemdtloid=apd.apitemdtloid AND ISNULL(sretm.apretitemmstres1, '')='Jasa' AND apretitemmststatus<>'Rejected' AND sretd.apretitemmstoid<>" + apretitemmstoid + "), 0)) apretitemqty, apd.apitemprice AS apretitemprice, apd.apitemdtlamt AS apretitemdtlamt, apd.apitemdtldiscvalue AS apretitemdtldisc, apd.apitemdtltaxvalue AS apretitemdtltaxvalue, apd.apitemdtltaxamt AS apretitemdtltaxamt, apd.apitemdtlnetto AS apretitemdtlnetto FROM QL_trnapitemdtl apd INNER JOIN QL_mstjasa m ON m.jasaoid = apd.itemoid INNER JOIN QL_m05gn g ON gnoid = apitemunitoid WHERE apd.cmpcode = '" + CompnyCode + "' AND apitemmstoid = " + apitemmstoid + " AND ISNULL(apitemdtlres1, '')<> 'Complete' ORDER BY apitemdtlseq";

            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFBData(int suppoid, string type, int curroid)
        {
            var sAdd = "";
            List<apitem> tbl = new List<apitem>();
            if (type == "Lunas")
            {
                sAdd = "AND (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND trnapstatus = 'POST') = 0";
            }
            else
            {
                //sAdd = "AND (SELECT COUNT(-1) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND payrefoid > 0) = 0";
                sAdd = "AND (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND trnapstatus = 'POST') > 0";
            }
            sSql = "SELECT apitemmstoid, apitemno, apitemdate, (SELECT poitemno FROM QL_trnpoitemmst where poitemmstoid = apm.poitemmstoid) poitemno, (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND trnapstatus = 'POST') apitemgrandtotal, ISNULL(rm.projectname,'') projectname, ISNULL(de.groupdesc,'') departemen FROM QL_trnapitemmst apm LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid=apm.rabmstoid LEFT JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid where apitemmststatus IN ('Post','Closed') AND apm.curroid=" + curroid + " AND ISNULL(apm.apitemtype,'')='Jasa' AND apm.suppoid = " + suppoid + " AND ISNULL(apitemmstres1, '')<> 'Closed' " + sAdd + " ORDER BY apitemmstoid";

            tbl = db.Database.SqlQuery<apitem>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSuppData(string type, int curroid)
        {
            var sAdd = "";
            List<Supp> tbl = new List<Supp>();
            if (type == "Lunas")
            {
                sAdd = "AND (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND trnapstatus = 'POST') = 0";
            }
            else
            {
                //sAdd = "AND (SELECT COUNT(-1) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND payrefoid > 0) = 0";
                sAdd = "AND (SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conap where refoid = apitemmstoid AND reftype='QL_trnapitemmst' AND trnapstatus = 'POST') > 0";
            }
            sSql = "SELECT DISTINCT suppoid, suppname, suppcode, suppaddr, suppphone1 suppphone FROM QL_mstsupp where activeflag = 'ACTIVE' and suppoid IN (SELECT suppoid FROM QL_trnapitemmst where apitemmststatus IN ('Post','Closed') AND curroid=" + curroid + " AND ISNULL(apitemtype,'')='Jasa' AND ISNULL(apitemmstres1, '')<>'Closed' " + sAdd + ")  ORDER BY Suppcode, suppname";

            tbl = db.Database.SqlQuery<Supp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<apretitemdtl> dtDtl)
        {
            Session["QL_trnapretitemdtljasa"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnapretitemdtljasa"] == null)
            {
                Session["QL_trnapretitemdtljasa"] = new List<apretitemdtl>();
            }

            List<apretitemdtl> dataDtl = (List<apretitemdtl>)Session["QL_trnapretitemdtljasa"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnapretitemmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid='" + tbl.suppoid + "'").FirstOrDefault();

            //ViewBag.apitemgrandtotal = db.Database.SqlQuery<decimal>("SELECT apitemgrandtotal FROM QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid='" + tbl.apitemmstoid + "'").FirstOrDefault();
            ViewBag.apitemgrandtotal = db.Database.SqlQuery<decimal?>("SELECT SUM(amttrans)-SUM(amtbayar) FROM QL_conap where refoid = '" + tbl.apitemmstoid + "' AND reftype='QL_trnapitemmst' AND trnapstatus = 'POST'").FirstOrDefault() ?? 0;

            ViewBag.apitemno = db.Database.SqlQuery<string>("SELECT apitemno FROM QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid='" + tbl.apitemmstoid + "'").FirstOrDefault();

            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.apitemmstoid='" + tbl.apitemmstoid + "'").FirstOrDefault();

            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.apitemmstoid='" + tbl.apitemmstoid + "'").FirstOrDefault();
        }

        // GET: pretitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT apm.cmpcode, apm.apretitemmstoid, apm.apretitemno, apm.apretitemdate, apm.apitemmstoid, ISNULL((SELECT apitemno FROM QL_trnapitemmst where apitemmstoid = apm.apitemmstoid),'') apitemno, s.suppname, apm.apretitemmststatus, apm.apretitemmstnote FROM QL_trnapretitemmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid WHERE apm.cmpcode = '" + CompnyCode + "' AND ISNULL(apm.apretitemmstres1,'')='Jasa' ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND apm.apretitemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND apm.apretitemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND apm.apretitemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY apm.apretitemdate";

            List<apretitemmst> dt = db.Database.SqlQuery<apretitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: pretitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapretitemmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnapretitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.apretitemmstoid = 0;
                tbl.apretitemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.apretitemmststatus = "In Process";

                Session["QL_trnapretitemdtljasa"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnapretitemmst.Find(CompnyCode, id);
                sSql = "select apitemdtloid, apretitemdtlseq, i.jasaoid itemoid, i.jasacode itemcode, i.jasadesc itemdesc, i.acctgoid, ISNULL((SELECT refno FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl ad ON ad.mritemdtloid = mrd.mritemdtloid AND mrd.mritemmstoid = ad.mritemmstoid WHERE ad.apitemdtloid = apd.apitemdtloid),'') AS refno, ISNULL((SELECT serialnumber FROM QL_trnmritemdtl mrd INNER JOIN QL_trnapitemdtl ad ON ad.mritemdtloid = mrd.mritemdtloid AND mrd.mritemmstoid = ad.mritemmstoid WHERE ad.apitemdtloid = apd.apitemdtloid),'') AS serialnumber, ISNULL((SELECT ad.apitemqty FROM QL_trnapitemdtl ad WHERE ad.apitemdtloid = apd.apitemdtloid),0.0) - ISNULL((SELECT SUM(apretitemqty) FROM QL_trnapretitemdtl sretd INNER JOIN QL_trnapretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.apretitemmstoid=sretd.apretitemmstoid WHERE sretd.cmpcode=apd.cmpcode AND sretd.apitemdtloid=apd.apitemdtloid AND ISNULL(sretm.apretitemmstres1, '')='Jasa' AND apretitemmststatus<>'Rejected' AND sretd.apretitemmstoid<>apd.apretitemmstoid), 0.0) apitemqty, apretitemqty, apretitemunitoid apretitemunitoid, gn.gndesc apretitemunit, apd.apretitemprice, apd.apretitemdtlamt, ISNULL((SELECT ad.apitemdtldiscvalue FROM QL_trnapitemdtl ad WHERE ad.apitemdtloid = apd.apitemdtloid),0.0) apitemdtldisc, apd.apretitemdtltaxvalue, apd.apretitemdtltaxamt, apd.apretitemdtlnetto from QL_trnapretitemdtl apd INNER JOIN QL_m05GN gn ON gn.gnoid = apd.apretitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_mstjasa i ON i.jasaoid = apd.itemoid WHERE apd.cmpcode='" + CompnyCode + "' AND apd.apretitemmstoid=" + id + " ORDER BY apd.apretitemdtlseq";
                Session["QL_trnapretitemdtljasa"] = db.Database.SqlQuery<apretitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: pretitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapretitemmst tbl, string action, string tglmst, string type)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.apretitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apretitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.apretitemmststatus == "Post")
            {
                tbl.apretitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.apretitemno = "";
            }
            tbl.apretitemmstres1 = "Jasa";
            if (string.IsNullOrEmpty(tbl.apretitemmstres2))
                tbl.apretitemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.apretitemmstnote))
                tbl.apretitemmstnote = "";


            List<apretitemdtl> dtDtl = (List<apretitemdtl>)Session["QL_trnapretitemdtljasa"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].apretitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY FB item " + dtDtl[i].itemdesc + "  tidak boleh 0!");
                        }
                        else
                        {
                            if (dtDtl[i].apretitemqty > dtDtl[i].apretitemqty)
                            {
                                ModelState.AddModelError("", "QTY Retur FB item " + dtDtl[i].itemdesc + " Harus kurang dari FB QTY!");
                            }
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].apretitemdtlnote))
                            dtDtl[i].apretitemdtlnote = "";
                        sSql = "SELECT (apitemqty - ISNULL((SELECT SUM(apretitemqty) FROM QL_trnapretitemdtl sretd INNER JOIN QL_trnapretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.apretitemmstoid=sretd.apretitemmstoid WHERE sretd.cmpcode=apd.cmpcode AND sretd.apitemdtloid=apd.apitemdtloid AND ISNULL(sretm.apretitemmstres1, '')='Jasa' AND apretitemmststatus<>'Rejected' AND sretd.apretitemmstoid<>" + tbl.apretitemmstoid + "), 0)) AS apitemqty FROM QL_trnapitemdtl apd WHERE apd.cmpcode='" + CompnyCode + "' AND apd.apitemdtloid=" + dtDtl[i].apitemdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dtDtl[i].apretitemqty > dQty)
                            ModelState.AddModelError("", "QTY FB item " + dtDtl[i].itemdesc + " sebagian detail data telah diupdate oleh user yg lain. Silahkan cek setiap detail QTY FB harus lebih dari retur FB QTY");
                        if (tbl.apretitemmststatus == "Post")
                        {
                            if (string.IsNullOrEmpty(tbl.apretitemno))
                                ModelState.AddModelError("apretitemno", "Silahkan isi No. FB!");
                            else if (db.QL_trnapretitemmst.Where(w => w.apretitemno == tbl.apretitemno & w.apretitemmstoid != tbl.apretitemmstoid).Count() > 0)
                                ModelState.AddModelError("apretitemno", "No. FB yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");

                            //var whoid = db.Database.SqlQuery<int>("select DISTINCT mrd.mritemwhoid from QL_trnapitemdtl apd INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemdtloid = apd.mritemdtloid AND apd.mritemmstoid = apd.mritemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();

                            //var rabAR = db.Database.SqlQuery<int>("select apm.rabmstoid from QL_trnapitemdtl apd INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = apd.apitemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apitemdtloid=" + dtDtl[i].apitemdtloid).FirstOrDefault();

                            //var spr = "";
                            //if (!ClassFunction.IsStockAvailable(Session["CompnyCode"].ToString(), ClassFunction.GetServerTime(), dtDtl[i].itemoid, whoid, dtDtl[i].apretitemqty, dtDtl[i].refno, rabAR, dtDtl[i].serialnumber))
                            //{
                            //    if (dtDtl[i].refno != "")
                            //    {
                            //        spr = " - ";
                            //    }
                            //    ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + spr + dtDtl[i].refno + " Qty FB Return tidak boleh lebih dari Stock Qty");
                            //}

                            ////Stock Value
                            //decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].itemoid);
                            //decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dtDtl[i].apretitemqty, sValue, "OUT");
                            //dtDtl[i].apretitemvalueidr = sAvgValue;
                            dtDtl[i].apretitemvalueidr = dtDtl[i].apretitemdtlamt - (dtDtl[i].apretitemdtldisc > 0 ? dtDtl[i].apretitemdtlamt * (dtDtl[i].apretitemdtldisc / 100) : 0);
                        }
                        else
                        {
                            dtDtl[i].apretitemvalueidr = 0;
                        }
                    }
                }
            }
            if (tbl.apretitemtype == "Belum di Bayar")
            {
                //Cek sisa amount FB
                var reftrans = "QL_trnapitemmst";
                sSql = "SELECT SUM(amttrans)-SUM(amtbayar) AS amt FROM QL_conap where refoid = " + tbl.apitemmstoid + " AND reftype='" + reftrans + "' AND trnapstatus = 'POST'";
                decimal aptotal = Math.Round(db.Database.SqlQuery<decimal>(sSql).FirstOrDefault(), 4);
                decimal aprettotal = Math.Round(dtDtl.Sum(x => x.apretitemdtlnetto), 4);
                if (aprettotal > aptotal)
                {
                    ModelState.AddModelError("", "Nilai Retur Tidak Boleh Lebih Besar Nilai FB!");
                }
            }

            if (tbl.apretitemmststatus == "Post")
            {
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_IN"));
                if (!ClassFunction.IsInterfaceExists("VAR_AP", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AP_JASA"));
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AP_RETUR", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DP_AP_RETUR"));
                if (!ClassFunction.IsInterfaceExists("VAR_CASH_AP_RETUR", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_CASH_AP_RETUR"));
                if (!ClassFunction.IsInterfaceExists("VAR_DISC_AP", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DISC_AP"));
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.apretitemmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.apretitemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.apretitemmststatus = "In Process";
            }
            if (tbl.apretitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.apretitemmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                tbl.rate2oid = rate2oid;

                //var conmatoid = ClassFunction.GenerateID("QL_CONMAT");
                var dpoid = ClassFunction.GenerateID("QL_trndpap");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var mstoid = ClassFunction.GenerateID("QL_trnapretitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnapretitemdtl");

                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                // VAR retur 'Belum Dibayar'
                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));
                var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_JASA", CompnyCode));
                var iAcctgOidAPRet = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DP_AP_RETUR", CompnyCode));
                var iAcctgOidCashRet = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_CASH_AP_RETUR", CompnyCode));
                var iAcctgOidDisc = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DISC_AP", CompnyCode));

                var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.suppoid).FirstOrDefault();

                //var apitemdiscamt = db.Database.SqlQuery<decimal>("select apitemtotaldiscdtl from QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();

                var apitemno = db.Database.SqlQuery<string>("select apitemno from QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();

                //var apitemtotalamt = db.Database.SqlQuery<decimal>("select apitemtotalamt from QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();
                var apitemtotalamt = dtDtl.Sum(x => x.apretitemdtlamt - (x.apretitemdtldisc > 0 ? x.apretitemdtlamt * (x.apretitemdtldisc / 100) : 0));

                //var apitemtaxamt = db.Database.SqlQuery<decimal>("select apitemtaxamt from QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();
                var apitemtaxamt = dtDtl.Sum(x => x.apretitemdtltaxamt);

                //var apitemgrandtotal = db.Database.SqlQuery<decimal>("select apitemgrandtotal from QL_trnapitemmst WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();
                var apitemgrandtotal = Math.Round(dtDtl.Sum(x => x.apretitemdtlnetto), 0);

                decimal dMRAmt = 0;
                dMRAmt = dtDtl.Sum(x => x.apretitemvalueidr);

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.apretitemmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.apretitemdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnapretitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.apretitemmstoid + " WHERE tablename='QL_trnapretitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnapitemdtl SET apitemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND apitemdtloid IN (SELECT apitemdtloid FROM QL_trnapretitemdtl WHERE cmpcode='" + CompnyCode + "' AND apretitemmstoid=" + tbl.apretitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnapitemmst SET apitemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid IN (SELECT apitemmstoid FROM QL_trnapretitemmst WHERE cmpcode='" + CompnyCode + "' AND apretitemmstoid=" + tbl.apretitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapretitemdtl.Where(a => a.apretitemmstoid == tbl.apretitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapretitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnapretitemdtl tbldtl;

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnapretitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.apretitemdtloid = dtloid++;
                            tbldtl.apretitemmstoid = tbl.apretitemmstoid;
                            tbldtl.apretitemdtlseq = i + 1;
                            tbldtl.apitemdtloid = dtDtl[i].apitemdtloid;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.apretitemqty = dtDtl[i].apretitemqty;
                            tbldtl.apretitemprice = dtDtl[i].apretitemprice;
                            tbldtl.apretitemdtlamt = dtDtl[i].apretitemdtlamt;
                            tbldtl.apretitemdtltaxvalue = dtDtl[i].apretitemdtltaxvalue;
                            tbldtl.apretitemdtltaxamt = dtDtl[i].apretitemdtltaxamt;
                            tbldtl.apretitemdtlnetto = dtDtl[i].apretitemdtlnetto;
                            tbldtl.apretitemunitoid = dtDtl[i].apretitemunitoid;
                            tbldtl.apretitemdtlstatus = "";
                            tbldtl.apretitemdtlnote = (dtDtl[i].apretitemdtlnote == null ? "" : dtDtl[i].apretitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.apretitemdtlres1 = "";
                            tbldtl.apretitemdtlres2 = "";
                            tbldtl.apretitemvalueidr = dtDtl[i].apretitemvalueidr;

                            db.QL_trnapretitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].apretitemqty >= dtDtl[i].apitemqty)
                            {
                                sSql = "UPDATE QL_trnapitemdtl SET apitemdtlres1='Complete' WHERE cmpcode='" + CompnyCode + "' AND apitemdtloid=" + dtDtl[i].apitemdtloid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnapitemmst SET apitemmstres1='Closed' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnapitemdtl WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid + " AND apitemdtloid<>" + dtDtl[i].apitemdtloid + " AND ISNULL(apitemdtlres1, '')<>'Complete')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }  

                            //if (tbl.apretitemmststatus == "Post")
                            //{
                            //    var whoid = db.Database.SqlQuery<int>("select DISTINCT mrd.mritemwhoid from QL_trnapitemdtl apd INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemdtloid = apd.mritemdtloid AND apd.mritemmstoid = apd.mritemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apitemmstoid=" + tbl.apitemmstoid).FirstOrDefault();

                            //    var refno = db.Database.SqlQuery<string>("select mrd.refno from QL_trnapitemdtl apd INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemdtloid = apd.mritemdtloid AND apd.mritemmstoid = apd.mritemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apitemdtloid=" + dtDtl[i].apitemdtloid).FirstOrDefault();

                            //    var serialnumber = db.Database.SqlQuery<string>("select ISNULL(mrd.serialnumber,'') from QL_trnapitemdtl apd INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemdtloid = apd.mritemdtloid AND apd.mritemmstoid = apd.mritemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apitemdtloid=" + dtDtl[i].apitemdtloid).FirstOrDefault();

                            //    var rabAR = db.Database.SqlQuery<int>("select apm.rabmstoid from QL_trnapitemdtl apd INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = apd.apitemmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apitemdtloid=" + dtDtl[i].apitemdtloid).FirstOrDefault();

                            //    // Insert QL_conmat
                            //    db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "APRETFG", "QL_trnapretitemdtl", tbl.apretitemmstoid, dtDtl[i].itemoid, "FINISH GOOD", whoid, dtDtl[i].apretitemqty * -1, "Faktur Beli Retur", "Faktur Beli Retur No. " + tbl.apretitemno + "", Session["UserID"].ToString(), refno, dtDtl[i].apretitemvalueidr, 0, 0, null, tbldtl.apitemdtloid, rabAR, dtDtl[i].apretitemvalueidr, serialnumber));
                            //    db.SaveChanges();
                            //}

                        }

                        //sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                        //db.Database.ExecuteSqlCommand(sSql);
                        //db.SaveChanges();

                        if (tbl.apretitemmststatus == "Post")
                        {
                            //update fa beli
                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE' WHERE fabelioid IN(SELECT DISTINCT fabelioid FROM QL_trnapitemdtl2 WHERE apitemmstoid=" + tbl.apitemmstoid + " AND apitemdtl2type='QL_trnapitemmst')";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.apretitemtype != "Belum di Bayar")
                            {
                                iAcctgOidAP = iAcctgOidCashRet; //jika lunas hutang diganti kas/bank
                            }

                            // Insert QL_trnglmst / Hutang(C)-Hutang Suspend(D), PPN(D)
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.apretitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAP, "D", apitemgrandtotal, tbl.apretitemno, tbl.apretitemno + " | " + suppname + " | " + tbl.apretitemmstnote, "Post", Session["UserID"].ToString(), servertime, apitemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapretitemmst " + tbl.apretitemmstoid, null, null, null, 0));
                            db.SaveChanges();

                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                int acctgoidJasa = dtDtl[i].acctgoid; decimal amtJasa = dtDtl[i].apretitemvalueidr;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, acctgoidJasa, "C", amtJasa, tbl.apretitemno, tbl.apretitemno + " | " + suppname + " | " + tbl.apretitemmstnote, "Post", Session["UserID"].ToString(), servertime, amtJasa * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapretitemmst " + tbl.apretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }  

                            decimal totaltax = Convert.ToDecimal(apitemtaxamt);
                            if (totaltax > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "C", totaltax, tbl.apretitemno, tbl.apretitemno + " | " + suppname + " | " + tbl.apretitemmstnote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapretitemmst " + tbl.apretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            //decimal totaldisc = Convert.ToDecimal(apitemdiscamt);
                            //if (totaldisc > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidDisc, "D", totaldisc, tbl.apretitemno, tbl.apretitemno + " | " + suppname + " | " + tbl.apretitemmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapretitemmst " + tbl.apretitemmstoid, null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            if (tbl.apretitemtype == "Belum di Bayar")
                            {
                                // Insert QL_conap                        
                                db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnapitemmst", tbl.apitemmstoid, tbl.apretitemmstoid, tbl.suppoid, iAcctgOidAP, "Post", "APRETJ", new DateTime(1900, 01, 01), sPeriod, 0, servertime, "", 0, servertime, 0, apitemgrandtotal, "RETUR FB (SUPP=" + suppname + " NO=" + tbl.apretitemno + ")", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, apitemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, ""));
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            // Jika Faktur Beli sudah lunas **
                            else
                            {
                                QL_trncashbankmst tblcb;
                                tblcb = new QL_trncashbankmst();
                                tblcb.cmpcode = CompnyCode;
                                tblcb.cashbankoid = cashbankoid;
                                tblcb.cashbankno = GenerateCashBankNo(CompnyCode, ClassFunction.GetServerTime(), "BKK", iAcctgOidCashRet);
                                tblcb.cashbankdate = DateTime.Parse(servertime.ToString("MM/dd/yyyy"));
                                tblcb.periodacctg = sPeriod;
                                tblcb.cashbanktype = "BKK";
                                tblcb.cashbankgroup = "DPAP";
                                tblcb.acctgoid = iAcctgOidCashRet;
                                tblcb.curroid = 1;
                                tblcb.curroid_to = 0;
                                tblcb.cashbanknote = "";
                                tblcb.cashbankrefno = tbl.apretitemno;
                                tblcb.personoid = Session["UserID"].ToString();
                                tblcb.cashbankduedate = DateTime.Parse(servertime.ToString("MM/dd/yyyy"));
                                tblcb.cashbankamt = apitemgrandtotal;
                                tblcb.cashbankamtidr = apitemgrandtotal * cRate.GetRateDailyIDRValue;
                                tblcb.cashbankamtusd = 0;
                                tblcb.cashbankres1 = "";
                                tblcb.cashbankres2 = "";
                                tblcb.cashbankres3 = "";
                                tblcb.cashbankstatus = "Post";
                                tblcb.createuser = Session["UserID"].ToString();
                                tblcb.createtime = servertime;
                                tblcb.upduser = Session["UserID"].ToString();
                                tblcb.updtime = servertime;
                                tblcb.giroacctgoid = 0;
                                tblcb.refsuppoid = 0;
                                tblcb.cashbanktakegiro = DateTime.Parse(servertime.ToString("MM/dd/yyyy"));
                                tblcb.cashbanktakegiroreal = DateTime.Parse("1900-01-01");
                                tblcb.cashbankgiroreal = DateTime.Parse("1900-01-01");
                                tblcb.cashbanktaxtype = "NON TAX";
                                tblcb.cashbanktaxpct = 0;
                                tblcb.cashbanktaxamt = 0;
                                tblcb.cashbankothertaxamt = 0;
                                tblcb.cashbankdpp = apitemgrandtotal;
                                tblcb.cashbankresamt = 0;
                                tblcb.cashbankresamt2 = 0;
                                tblcb.addacctgoid1 = 0;
                                tblcb.addacctgamt1 = 0;
                                tblcb.addacctgoid2 = 0;
                                tblcb.addacctgamt2 = 0;
                                tblcb.addacctgoid3 = 0;
                                tblcb.addacctgamt3 = 0;
                                tblcb.cashbankapoid = 0;
                                tblcb.cashbankaptype = "";
                                tblcb.deptoid = 0;
                                tblcb.cashbanksuppaccoid = 0;
                                tblcb.groupoid = 0;
                                db.QL_trncashbankmst.Add(tblcb);
                                db.SaveChanges();

                                QL_trndpap dp;
                                dp = new QL_trndpap();
                                dp.cmpcode = CompnyCode;
                                dp.dpapoid = dpoid;
                                dp.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                dp.dpapno = generateDPNo(servertime);
                                dp.dpapdate = servertime;
                                dp.suppoid = tbl.suppoid;
                                dp.acctgoid = iAcctgOidAPRet;
                                dp.cashbankoid = tblcb.cashbankoid;
                                dp.dpappaytype = "BKK";
                                dp.dpappayacctgoid = iAcctgOidCashRet;
                                dp.dpappayrefno = tbl.apretitemno;
                                dp.dpapduedate = servertime;
                                dp.curroid = 1;
                                dp.rateoid = 0;
                                dp.rate2oid = 0;
                                dp.dpapamt = apitemgrandtotal;
                                dp.dpapamtidr = apitemgrandtotal * cRate.GetRateMonthlyIDRValue;
                                dp.dpapamtusd = 0;
                                dp.dpapaccumamt = 0;
                                dp.dpapnote = "RETUR FB (SUPP=" + suppname + " NO=" + tbl.apretitemno + ")";
                                dp.dpapres1 = "Return";
                                dp.dpapres2 = "";
                                dp.dpapres3 = "";
                                dp.dpapstatus = "Post";
                                dp.createuser = Session["UserID"].ToString();
                                dp.createtime = servertime;
                                dp.upduser = Session["UserID"].ToString();
                                dp.updtime = servertime;
                                dp.dpaptakegiro = new DateTime(1900, 01, 01);
                                dp.giroacctgoid = 0;
                                dp.addacctgoid1 = 0;
                                dp.addacctgamt1 = 0;
                                dp.addacctgoid2 = 0;
                                dp.addacctgamt2 = 0;
                                dp.addacctgoid3 = 0;
                                dp.addacctgamt3 = 0;
                                dp.pomstoid = 0;
                                dp.poreftype = "QL_trnpoitemmst";
                                db.QL_trndpap.Add(dp);
                                db.SaveChanges();

                                glmstoid += 1;
                                // Insert QL_trnglmst / Uang Muka(D)-Kas(C)
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.apretitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                glseq = 1;

                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAPRet, "D", apitemgrandtotal, tbl.apretitemno, tbl.apretitemno + " | " + suppname + " | " + tbl.apretitemmstnote, "Post", Session["UserID"].ToString(), servertime, apitemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapretitemmst " + tbl.apretitemmstoid, null, null, null, 0));
                                db.SaveChanges();


                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCashRet, "C", apitemgrandtotal, tbl.apretitemno, tbl.apretitemno + " | " + suppname + " | " + tbl.apretitemmstnote, "Post", Session["UserID"].ToString(), servertime, apitemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapretitemmst " + tbl.apretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                                // end insert QL_trnglmst

                                sSql = "UPDATE QL_ID SET lastoid=" + dpoid + " WHERE tablename='QL_trndpap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnapretitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.apretitemmststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.apretitemmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);

        }


        // POST: pretitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapretitemmst tbl = db.QL_trnapretitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnapitemdtl SET apitemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND apitemdtloid IN (SELECT apitemdtloid FROM QL_trnapretitemdtl WHERE cmpcode='" + CompnyCode + "' AND apretitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnapitemmst SET apitemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid IN (SELECT apitemmstoid FROM QL_trnapretitemmst WHERE cmpcode='" + CompnyCode + "' AND apretitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnapretitemdtl.Where(a => a.apretitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnapretitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapretitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FBR/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apretitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapretitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND apretitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private string generateDPNo(DateTime tanggal)
        {
            string sNo = "DPAP/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dpapno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpap WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND dpapno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public string GenerateCashBankNo(string cmpcode, DateTime sDate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' AND acctgoid=" + acctgoid;
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            }
            return cashbankno;
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT pretitemno FROM QL_trnpretitemmst WHERE pretitemmstoid=" + id + "";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptFBReturnJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE prm.cmpcode='" + CompnyCode + "' AND prm.apretitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptFBReturnItem_" + sNo + ".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}