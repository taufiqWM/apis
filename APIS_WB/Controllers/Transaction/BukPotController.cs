﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class BukPotController : Controller
    {
        // GET: BukPot
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public BukPotController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trnbukpotmst tbl)
        {
            sSql = "SELECT * FROM QL_m01US WHERE cmpcode='" + CompnyCode + "' AND usflag = 'ACTIVE'";
            var usoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", null);
            ViewBag.usoid = usoid;
        }

        private void FillAdditionalField(QL_trnbukpotmst tbl)
        {
            //ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname from QL_mstsupp where suppoid = '" + tblmst.suppoid + "'").FirstOrDefault();
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FPB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbukpotmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND prno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public class listpbdtl : QL_trnbukpotdtl
        {
            public string suppname { get; set; }
            public string bukpotrefno { get; set; }
            public string acctgdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            JsonResult js = null;
            try
            {
                string acctgoid = ClassFunction.GetDataAcctgOid("VAR_PPH", CompnyCode);
                sSql = $"SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trncashbankmst' bukpotreftype, cb.cashbankoid bukpotrefoid, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=cb.refsuppoid),'') suppname, cb.cashbankno bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(cb.addacctgamt1) bukpotpphamt, cb.cashbanknote bukpotdtlnote from QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.addacctgoid1 WHERE cb.cashbankstatus IN('Post','Approved') AND cb.cashbankgroup='AP' AND a.activeflag='ACTIVE' AND a.acctgoid IN({acctgoid}) AND cb.cashbankoid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype='QL_trncashbankmst')  UNION ALL  SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trncashbankmst' bukpotreftype, cb.cashbankoid bukpotrefoid, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid = cb.refsuppoid),'') suppname, cb.cashbankno bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(cb.addacctgamt2) bukpotpphamt, cb.cashbanknote bukpotdtlnote from QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid = cb.addacctgoid2 WHERE cb.cashbankstatus IN('Post', 'Approved') AND cb.cashbankgroup = 'AP' AND a.activeflag = 'ACTIVE' AND a.acctgoid IN({acctgoid}) AND cb.cashbankoid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype = 'QL_trncashbankmst')  UNION ALL  SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trncashbankmst' bukpotreftype, cb.cashbankoid bukpotrefoid, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid = cb.refsuppoid), '') suppname, cb.cashbankno bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(cb.addacctgamt3) bukpotpphamt, cb.cashbanknote bukpotdtlnote from QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid = cb.addacctgoid3 WHERE cb.cashbankstatus IN('Post', 'Approved') AND cb.cashbankgroup = 'AP' AND a.activeflag = 'ACTIVE' AND a.acctgoid IN({acctgoid}) AND cb.cashbankoid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype = 'QL_trncashbankmst')  UNION ALL  SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trncashbankmst' bukpotreftype, cb.cashbankoid bukpotrefoid, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid = cb.refsuppoid), '') suppname, cb.cashbankno bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(cb.addacctgamt4) bukpotpphamt, cb.cashbanknote bukpotdtlnote from QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid = cb.addacctgoid4 WHERE cb.cashbankstatus IN('Post', 'Approved') AND cb.cashbankgroup = 'AP' AND a.activeflag = 'ACTIVE' AND a.acctgoid IN({acctgoid}) AND cb.cashbankoid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype = 'QL_trncashbankmst')  UNION ALL  SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trncashbankmst' bukpotreftype, cb.cashbankoid bukpotrefoid, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid = cb.refsuppoid), '') suppname, cb.cashbankno bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(cb.addacctgamt5) bukpotpphamt, cb.cashbanknote bukpotdtlnote from QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid = cb.addacctgoid5 WHERE cb.cashbankstatus IN('Post', 'Approved') AND cb.cashbankgroup = 'AP' AND a.activeflag = 'ACTIVE' AND a.acctgoid IN({acctgoid}) AND cb.cashbankoid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype = 'QL_trncashbankmst')  UNION ALL  SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trncashbankgl' bukpotreftype, gl.cashbankgloid bukpotrefoid, '' suppname, cb.cashbankno bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(gl.pphcreditamt) bukpotpphamt, cb.cashbanknote bukpotdtlnote from QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid = cb.cashbankoid INNER JOIN QL_mstacctg a ON a.acctgoid = gl.pphcreditacctgoid WHERE cb.cashbankstatus IN('Post', 'Approved') AND cb.cashbankgroup = 'RABMULTIREAL' AND a.activeflag = 'ACTIVE' AND a.acctgoid IN({acctgoid}) AND gl.cashbankgloid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype = 'QL_trncashbankgl')  UNION ALL  SELECT 0 bukpotdtlseq, '' bukpotno, 'QL_trnkasbon2dtl' bukpotreftype, gl.kasbon2dtloid bukpotrefoid, '' suppname, cb.kasbon2no bukpotrefno, 0.0 bukpotbruto, a.acctgoid, a.acctgdesc, ABS(gl.pphcreditdtlamt) bukpotpphamt, cb.kasbon2note bukpotdtlnote from QL_trnkasbon2mst cb INNER JOIN QL_trnkasbon2dtl gl ON gl.kasbon2mstoid = cb.kasbon2mstoid INNER JOIN QL_mstacctg a ON a.acctgoid = gl.pphcreditacctgdtloid WHERE cb.kasbon2mststatus IN('Post', 'Approved') AND cb.kasbon2group = 'REALISASI' AND a.activeflag = 'ACTIVE' AND a.acctgoid IN({acctgoid}) AND gl.kasbon2dtloid NOT IN(SELECT x.bukpotrefoid FROM QL_trnbukpotdtl x WHERE x.bukpotreftype = 'QL_trnkasbon2dtl') ";
                var tbl = db.Database.SqlQuery<listpbdtl>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {

                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<listpbdtl>();

            try
            {
                sSql = $"SELECT bukpotdtlseq, bukpotno, bukpotreftype, bukpotrefoid, CASE bukpotreftype WHEN 'QL_trncashbankmst' THEN ISNULL((SELECT x.suppname FROM QL_trncashbankmst xm INNER JOIN QL_mstsupp x ON x.suppoid=xm.refsuppoid WHERE xm.cashbankoid=bukpotrefoid),'') ELSE '' END suppname, CASE bukpotreftype WHEN 'QL_trncashbankmst' THEN ISNULL((SELECT xm.cashbankno FROM QL_trncashbankmst xm WHERE xm.cashbankoid=bukpotrefoid),'') ELSE ISNULL((SELECT xm.cashbankno FROM QL_trncashbankmst xm INNER JOIN QL_trncashbankgl xgl ON xgl.cashbankoid=xm.cashbankoid WHERE xgl.cashbankgloid=bukpotrefoid),'') END bukpotrefno, bukpotbruto, bd.acctgoid, a.acctgdesc, bukpotpphamt, bukpotdtlnote from QL_trnbukpotdtl bd INNER JOIN QL_mstacctg a ON a.acctgoid=bd.acctgoid WHERE bd.bukpotmstoid={id} ORDER BY bukpotdtlseq";
                dtl = db.Database.SqlQuery<listpbdtl>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: poitem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            return View();
        }

        [HttpPost]
        public ActionResult getListDataTable(string ddlstatus, string periodfrom, string periodto)
        {

            sSql = "SELECT * FROM( Select pr.bukpotmstoid, pr.bukpotdate, pr.bukpotmstnote, pr.bukpotmststatus FROM QL_trnbukpotmst pr ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(periodfrom) && !string.IsNullOrEmpty(periodto))
            {
                sSql += " AND t.bukpotdate >='" + periodfrom + " 00:00:00' AND t.bukpotdate <='" + periodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(ddlstatus))
            {
                sSql += " AND t.bukpotmststatus " + ddlstatus;
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnbukpotmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnbukpotmst();
                tbl.bukpotuid = Guid.NewGuid();
                tbl.cmpcode = CompnyCode;
                tbl.bukpotmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

                ViewBag.bukpotdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnbukpotmst.Find(id);
                ViewBag.bukpotdate = tbl.bukpotdate.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnbukpotmst tbl, List<listpbdtl> dtDtl, string action, string tglmst, string tglsj)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.bukpotmstnote))
                tbl.bukpotmstnote = "";

            if (dtDtl == null)
                msg += "- Please fill detail data!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].bukpotdtlnote))
                            dtDtl[i].bukpotdtlnote = "";

                        if (string.IsNullOrEmpty(dtDtl[i].bukpotno))
                            msg += "- Silahkan Input No Bukti Potong!<br>";
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.bukpotdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }
            if (tbl.bukpotmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                }
            }

            if (msg == "")
            {
                tbl.bukpotmasa = tbl.bukpotdate.ToString("MM");
                tbl.bukpottahun = tbl.bukpotdate.ToString("yyyy");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnbukpotmst.Add(tbl);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnbukpotdtl.Where(b => b.bukpotmstoid == tbl.bukpotmstoid);
                            db.QL_trnbukpotdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnbukpotdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnbukpotdtl)ClassFunction.MappingTable(new QL_trnbukpotdtl(), dtDtl[i]);
                            tbldtl.bukpotmstoid = tbl.bukpotmstoid;
                            tbldtl.bukpotdtlseq = i + 1;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnbukpotdtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.bukpotmstoid.ToString();
                        sReturnNo = "No. " + tbl.bukpotmstoid.ToString();
                        if (tbl.bukpotmststatus == "Post") sReturnState = "Posted";
                        else sReturnState = "Saved";
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnbukpotmst tblmst = db.QL_trnbukpotmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnbukpotdtl.Where(a => a.bukpotmstoid == id);
                        db.QL_trnbukpotdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnbukpotmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPRItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.bukpotmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PRReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}