﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class CekDocReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public CekDocReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetDataChecklist(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "Select DISTINCT 0 seq, checkdocno [No.Checklist], CONVERT(Char, cd.checkdocdate,103) Tanggal, ISNULL(cu.custname,'') Customer, custaddr Alamat From QL_trncheckdocmst cd LEFT JOIN QL_mstcust cu ON cd.custoid=cu.custoid Where cd.cmpcode='" + CompnyCode + "' ";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND cd.flagstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY cd.checkdocno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_trncheckdocmst");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextChecklist)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "Select DISTINCT 0 seq, cu.custcode, ISNULL(cu.custname,'') Customer, custaddr Alamat From QL_trncheckdocmst cd INNER JOIN QL_mstcust cu ON cd.custoid=cu.custoid INNER JOIN QL_trnsoitemmst som on som.custoid=cu.custoid Where cd.cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND cd.flagstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextChecklist != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND cd.checkdocno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                //sSql += " ORDER BY cu.custname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstcust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextChecklist, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "Select DISTINCT 0 seq, som.soitemno [No. SO], Convert(Char(20), som.soitemdate,103) Tanggal, cu.custname [Customer] From QL_trncheckdocmst cd INNER JOIN QL_mstcust cu ON cd.custoid=cu.custoid INNER JOIN QL_trnsoitemmst som on som.custoid=cu.custoid Where cd.cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND cd.flagstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextChecklist != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND cd.checkdocno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND cu.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += " Order By som.soitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_trnsoitemmst");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextChecklist, string TextCust, string TextSO)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "Select DISTINCT 0 seq, rab.rabno [No. SO], Convert(Char(20), rab.rabdate,103) Tanggal, cu.custname [Customer] From QL_trncheckdocmst cd INNER JOIN QL_mstcust cu ON cd.custoid=cu.custoid INNER JOIN QL_trnsoitemmst som on som.custoid=cu.custoid INNER JOIN QL_trnrabmst rab ON rab.rabmstoid=som.rabmstoid Where cd.cmpcode='" + CompnyCode + "'";

                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND cd.flagstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextChecklist != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND cd.checkdocno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextCust != "")
                {
                    string[] arr = TextChecklist.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND cu.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextSO != "")
                {
                    string[] arr = TextSO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND som.soitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += " Order By rab.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_trnsoitemmst");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account"); 
            return View();
        }
                
        [HttpPost]
        public ActionResult PrintReport(string[] DDLStatus, string StartPeriod, string EndPeriod, string reporttype, string TextChecklist, string TextCust, string TextSO, string TextNomorRAB, string DDLType)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "rptCheckDocs.rpt";
            var rptname = "CHECK_DOCS";

            sSql = "Select cd.checkdocno, cd.checkdocdate, rab.projectname noproject, cd.amtdpar, cd.amtinvoice amtinvoice, ISNULL(cd.aramtsisa,0.00) aramtsisa, rab.rabno [No. RAB], ISNULL(Convert(Char(20), rab.rabdate,103),'1/1/1900') Tanggal, cu.custname [Customer], Case When cdd.docasli='1' Then 'Ya' Else 'Tidak' End docasli, cdd.notedetail, cd.flagstatus, cd.typecheckdoc, ISNULL(som.soitemno,'') [No. SO], cd.noresi, cd.residate, g.gndesc dokument, ISNULL((SELECT expname FROM QL_mstexpedisi e WHERE e.expoid=cd.expoid),'') ekspedisi  From QL_trncheckdocmst cd INNER JOIN QL_trncheckdocdtl cdd ON cdd.checkdocmstoid=cd.checkdocmstoid INNER JOIN QL_mstcust cu ON cd.custoid=cu.custoid INNER JOIN QL_trnsoitemmst som on som.soitemmstoid=cd.soitemmstoid INNER JOIN QL_trnrabmst rab ON rab.rabmstoid=cd.rabmstoid INNER JOIN QL_m05GN g ON g.gnoid=cdd.refoid Where cd.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND cd.checkdocdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND cd.checkdocdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLType != "ALL")
            {
                sSql += " AAND cd.typecheckdoc='" + DDLStatus + "'";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND cd.flagstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND cu.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextChecklist != "")
            {
                string[] arr = TextChecklist.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND cd.checkdocno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextSO != "")
            {
                string[] arr = TextSO.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.soitemno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }


            if (TextNomorRAB != "")
            {
                string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND rab.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += "Order By checkdocno";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            //rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        // GET: CekDocReport
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}