﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class StockOpnameController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public StockOpnameController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listadj
        {
            public string cmpcode { get; set; }
            public int stockadjoid { get; set; }
            public int resfield1 { get; set; }
            public string stockadjno { get; set; }
            public string stockadjtype { get; set; }
            public DateTime stockadjdate { get; set; }
            public string stockadjdocdate { get; set; }
            public string stockadjmstnote { get; set; }
            public String stockadjstatus { get; set; }
            
        }

        public class listadjdtl
        {
            public int stockadjseq { get; set; }
            public int mtrwhoid { get; set; }            
            public string mtrwhdesc { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public decimal stockadjqty { get; set; }
            public decimal stockadjqtyafter { get; set; }
            public decimal stockadjqtybefore { get; set; }
            public int stockadjunitoid { get; set; }
            public string stockadjunit { get; set; }
            public string stockadjnote { get; set; }           
            public decimal stockadjamtidr { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal qtystok { get; set; }
            public int mtrwhoid { get; set; }
            public string mtrwhdesc { get; set; }
            public decimal stockadjamtidr { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
            public string tipepid { get; set; }
        }

        public class liststock
        {            
            public Decimal stokakhir { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listadjdtl> dtDtl)
        {
            Session["QL_trnstockadjdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public ActionResult InitDDLGudangFrom()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'GUDANG' and gnflag = 'ACTIVE' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnstockadj tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            var mtrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.mtrwhoid);
            ViewBag.mtrwhdtloid = mtrwhoid;           

        }

        [HttpPost]
        public ActionResult InitDDLGudangTo(int whfrom)
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'GUDANG' and gnflag = 'ACTIVE' and gnoid <> " + whfrom + " ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno, string type)
        {
            var result = "sukses";
            var msg = "";

            var sQueryInHistStock = "";
            string sLastQtyQuery = "0.0";            
            string join = "";
            string group = "";
            List<liststock> tbl = new List<liststock>();
            if (type == "ADJUSTMENT")
            {
                sQueryInHistStock = " AND itemoid IN (SELECT DISTINCT refoid FROM QL_conmat con WHERE con.cmpcode='" + Session["CompnyCode"].ToString() + "' AND con.refname='FINISH GOOD' AND con.mtrwhoid=" + whfrom + ") and mtrwhoid = " + whfrom + "";
                sLastQtyQuery = " ISNULL((SELECT SUM(qtyin)-SUM(qtyout) FROM QL_conmat where refoid = i.itemoid and mtrwhoid = " + whfrom + "),0.0)";               
                join = "INNER JOIN QL_conmat c ON c.refoid = i.itemoid";
                group = "GROUP BY itemoid, itemcode, refno, itemdesc, itemunitoid, g.gndesc";
            }
            else
            {
                sQueryInHistStock = " AND itemoid NOT IN (SELECT DISTINCT refoid FROM QL_conmat con WHERE con.cmpcode='" + Session["CompnyCode"].ToString() + "' AND con.refname='FINISH GOOD' AND con.mtrwhoid=" + whfrom + ")";
            }

            sSql = "SELECT " + sLastQtyQuery + " FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid " + join + " WHERE activeflag = 'ACTIVE' and itemoid = "+ itemoid +" and refno = '"+ refno +"' " + sQueryInHistStock + " WHERE i.itemtype NOT IN('Ongkir') " + group + " ORDER BY itemcode";

            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataMatDetail(string type, int whoid)
        {
            List<listmat> tbl = new List<listmat>();
            string sQueryInHistStock = "";
            string sQueryLastStock = ""; string sDtl = "";
            string sJoin = ""; string sGroupBy = ""; string sTambah = "qtystok";

            if (type == "ADJUSTMENT")
            {
                sQueryLastStock = "(SUM(qtyin-qtyout))";
                sDtl = ", ISNULL(c.refno,'') refno, ISNULL(c.serialnumber,'') serialnumber, 0.0 AS stockadjamtidr, ISNULL(c.rabmstoid,0) rabmstoid, ISNULL((SELECT r.projectname FROM QL_trnrabmst r WHERE r.rabmstoid=ISNULL(c.rabmstoid,0)),'') projectname";
                sJoin = " INNER JOIN QL_conmat c ON c.refoid = i.itemoid";
                sGroupBy = " GROUP BY itemoid, itemcode, ISNULL(refno,''), ISNULL(c.serialnumber,''), itemdesc, itemunitoid, g.gndesc, ISNULL(c.rabmstoid,0), itemtype, pidtype";
                sQueryInHistStock = " AND itemoid IN (SELECT DISTINCT refoid FROM QL_conmat con WHERE con.cmpcode='" + CompnyCode + "' AND con.mtrwhoid=" + whoid + ") AND mtrwhoid = "+ whoid +"";
                sTambah = "(qtystok - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.itemoid AND mb.mtrwhoid=tbl.mtrwhoid AND ISNULL(mb.refno,'')=tbl.refno AND ISNULL(mb.serialnumber,'')=tbl.serialnumber AND mb.flag='Post'),0.0))";

            }
            else
            {
                sQueryLastStock = "0.0";
                sDtl = ", '' refno, '' serialnumber, 0.0 AS stockadjamtidr, 0 rabmstoid, '' projectname";
                sQueryInHistStock = " /*AND itemoid NOT IN (SELECT DISTINCT refoid FROM QL_conmat con WHERE con.cmpcode='" + CompnyCode + "' /*AND con.refname='FINISH GOOD'*/ AND con.mtrwhoid=" + whoid + ")*/";
            }

            sSql = "SELECT itemseq, itemoid, itemcode, itemdesc, itemunitoid, itemunit, mtrwhdesc, mtrwhoid, " + sTambah + " qtystok,  refno, serialnumber, stockadjamtidr, rabmstoid, projectname, tipepid FROM( ";
            sSql += "SELECT 0 itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit, (SELECT gndesc FROM QL_m05gn where gnoid = "+ whoid + " and gngroup = 'GUDANG') mtrwhdesc, "+ whoid + " mtrwhoid, " + sQueryLastStock + " qtystok " + sDtl + ", i.itemtype+ '/' + i.pidtype tipepid FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid " + sJoin + " WHERE i.itemtype IN ('Barang','Rakitan','Asset') AND activeflag = 'ACTIVE' " + sQueryInHistStock + "  "+ sGroupBy +"";
            sSql += " )AS tbl ORDER BY tbl.itemcode ";

            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            if (type == "ADJUSTMENT")
            {
                if (tbl.Count() > 0)
                {
                    for (int i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].stockadjamtidr = ClassFunction.GetStockValue(CompnyCode, tbl[i].itemoid);
                    }
                }
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnstockadjdtl"] == null)
            {
                Session["QL_trnstockadjdtl"] = new List<listadjdtl>();
            }

            List<listadjdtl> dataDtl = (List<listadjdtl>)Session["QL_trnstockadjdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }
        // GET: PG
        public ActionResult Index(string filter, ModelFilter modfil)
        {

            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            string sfilter = "";
            sSql = "Select DISTINCT ad.resfield1, ad.stockadjno, ad.stockadjdate, ad.stockadjtype, ad.stockadjmstnote, ad.stockadjstatus FROM QL_trnstockadj ad WHERE ad.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + "  ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND ad.stockadjdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND ad.stockadjdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME) AND ad.stockadjstatus IN ('In Approval')";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND ad.stockadjstatus IN ('In Process')";
            }
            sSql += " ORDER BY ad.stockadjdate";
            List<listadj> tblmst = db.Database.SqlQuery<listadj>(sSql).ToList();
            return View(tblmst);
            
        }

        // GET: PG/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string action = "";
            QL_trnstockadj tblmst;
            if (id == null)
            {
                tblmst = new QL_trnstockadj();           
                tblmst.stockadjstatus = "In Process";
                tblmst.cmpcode = CompnyCode;
                tblmst.resfield1 = ClassFunction.GenerateID("resfield1");
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.stockadjdate = ClassFunction.GetServerTime();
                tblmst.stockadjdocdate = ClassFunction.GetServerTime();
                tblmst.updtime = ClassFunction.GetServerTime();
                action = "Create";

                Session["QL_trnstockadjdtl"] = null;
            }
            else
            {
                tblmst = db.QL_trnstockadj.Where(w => w.resfield1 == id).FirstOrDefault();
                action = "Edit";

                sSql = "SELECT ad.stockadjseq, ad.resfield1, mtrwhoid, (SELECT gndesc FROM QL_m05gn where gnoid = mtrwhoid and gngroup = 'GUDANG') mtrwhdesc, ad.refoid itemoid, i.itemcode, refno, ISNULL(serialnumber,'') serialnumber, i.itemdesc, ad.stockadjqty, ad.stockadjqtyafter, ad.stockadjqtybefore, ad.stockadjunit stockadjunitoid, g.gndesc stockadjunit ,ad.stockadjnote, CASE WHEN stockadjstatus='Approved' THEN (CASE WHEN stockadjamtidr=0 THEN 0 ELSE stockadjamtidr/ABS(stockadjqty) END) ELSE stockadjamtidr END AS stockadjamtidr, ISNULL(ad.rabmstoid,0) rabmstoid, ISNULL((SELECT r.projectname FROM QL_trnrabmst r WHERE r.rabmstoid=ISNULL(ad.rabmstoid,0)),'') projectname FROM QL_trnstockadj ad INNER JOIN QL_mstitem i ON i.itemoid=ad.refoid INNER JOIN QL_m05GN g ON g.gnoid=ad.stockadjunit AND g.gngroup = 'SATUAN' WHERE ad.cmpcode='" + CompnyCode + "' AND ad.resfield1='" + id + "' ORDER BY ad.stockadjseq";
                Session["QL_trnstockadjdtl"] = db.Database.SqlQuery<listadjdtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            InitDDL(tblmst);
            ViewBag.action = action;
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnstockadj tblmst, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tblmst.stockadjdate = DateTime.Parse(ClassFunction.toDate(tglmst));
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("transitemdate", "Format Tanggal Tidak Valid!!" + ex.ToString());                
            }           

            List<listadjdtl> dtDtl = (List<listadjdtl>)Session["QL_trnstockadjdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Silahkan isi detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Silahkan isi detail data!");

            //Is Input Valid? 
            if (string.IsNullOrEmpty(tblmst.stockadjno))
                tblmst.stockadjno = "";
            if (string.IsNullOrEmpty(tblmst.stockadjmstnote))
                tblmst.stockadjmstnote = "";

            //Is Detail Input Valid? 
            var servertime = ClassFunction.GetServerTime();
            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].stockadjqty.ToString() == "" | (string.IsNullOrEmpty(dtDtl[i].stockadjqty.ToString())))
                        {
                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Selisih tidak boleh Kosong");
                        }
                        else
                        {
                            if (dtDtl[i].stockadjqty == 0)
                            {
                                ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Selisih tidak boleh 0");
                            }
                        }
                        if (dtDtl[i].stockadjamtidr.ToString() == "" | (string.IsNullOrEmpty(dtDtl[i].stockadjamtidr.ToString())))
                        {
                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Hpp tidak boleh Kosong");
                        }
                        else
                        {
                            if (dtDtl[i].stockadjamtidr == 0)
                            {
                                ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Hpp tidak boleh 0");
                            }
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].stockadjnote))
                            dtDtl[i].stockadjnote = "";
                        if (string.IsNullOrEmpty(dtDtl[i].refno))
                            dtDtl[i].refno = "";
                        if (string.IsNullOrEmpty(dtDtl[i].serialnumber))
                            dtDtl[i].serialnumber = "";
                    }
                }
            }

            if (tblmst.stockadjstatus == "Revised")
            {
                tblmst.stockadjstatus = "In Process";
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (tblmst.stockadjstatus == "In Approval")
            {                
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.stockadjdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.stockadjstatus = "In Process";
            }
            if (tblmst.stockadjstatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.stockadjstatus = "In Process";
                }
            }

            if (ModelState.IsValid)            
            {
                if (action == "Create")
                {
                    tblmst.resfield1 = ClassFunction.GenerateID("resfield1");
                }
                var mstoid = ClassFunction.GenerateID("QL_trnstockadj");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Edit")
                        {
                            //Update                            
                            var trndtl = db.QL_trnstockadj.Where(a => a.resfield1 == tblmst.resfield1 && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnstockadj.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        //Insert
                        QL_trnstockadj tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                            var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                            var itemtype = "FINISH GOOD";
                            if (type == "Asset")
                            {
                                itemtype = "FIXED ASSET";
                            }

                            tbldtl = new QL_trnstockadj();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.stockadjoid = mstoid++;
                            tbldtl.stockadjseq = i + 1;
                            tbldtl.stockadjno = tblmst.stockadjno;
                            tbldtl.stockadjdate = tblmst.stockadjdate;
                            tbldtl.stockadjdocdate = tblmst.stockadjdocdate;
                            tbldtl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tbldtl.refname = itemtype;
                            tbldtl.resfield1 = tblmst.resfield1;
                            tbldtl.resfield2 = "";
                            tbldtl.refoid = dtDtl[i].itemoid;
                            tbldtl.mtrwhoid = dtDtl[i].mtrwhoid;
                            tbldtl.reasonoid = 0;
                            tbldtl.reasonacctgoid = 0;
                            tbldtl.stockadjqtybefore = dtDtl[i].stockadjqtybefore;
                            tbldtl.stockadjqty = dtDtl[i].stockadjqty;
                            tbldtl.stockadjqtyafter = dtDtl[i].stockadjqtyafter;
                            tbldtl.stockadjunit = dtDtl[i].stockadjunitoid;
                            tbldtl.stockadjamtidr = dtDtl[i].stockadjamtidr;
                            tbldtl.stockadjamtusd = 0;
                            tbldtl.stockadjnote = dtDtl[i].stockadjnote;
                            tbldtl.refno = dtDtl[i].refno;
                            tbldtl.stockadjstatus = tblmst.stockadjstatus;
                            tbldtl.createuser = Session["UserID"].ToString();
                            tbldtl.createtime = servertime;
                            tbldtl.upduser = Session["UserID"].ToString();
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.approvaluser = "";
                            tbldtl.approvaldatetime = DateTime.Parse("01/01/1900");
                            tbldtl.stockadjtype = tblmst.stockadjtype;
                            tbldtl.stockadjmstnote = tblmst.stockadjmstnote;
                            tbldtl.reviseduser = "";
                            tbldtl.revisedtime = DateTime.Parse("01/01/1900");
                            tbldtl.revisednote = "";
                            tbldtl.rejectuser = "";
                            tbldtl.rejecttime = DateTime.Parse("01/01/1900");
                            tbldtl.rejectreason = "";
                            tbldtl.rabmstoid = dtDtl[i].rabmstoid;
                            tbldtl.serialnumber = dtDtl[i].serialnumber;

                            db.QL_trnstockadj.Add(tbldtl);
                            db.SaveChanges();
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_trnstockadj'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Create")
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + tblmst.resfield1 + " WHERE tablename='resfield1'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }                       

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tblmst.stockadjstatus == "In Approval")
                        {
                            tblapp.cmpcode = CompnyCode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tblmst.resfield1;
                            tblapp.requser = Session["UserID"].ToString();
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnstockadj";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.stockadjstatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.stockadjstatus = "In Process";
            }
            InitDDL(tblmst);
            ViewBag.action = action;
            return View(tblmst);
        }

        // POST: PG/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnstockadj tblmst = db.QL_trnstockadj.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnstockadj.Where(a => a.resfield1 == id);
                        db.QL_trnstockadj.RemoveRange(trndtl);
                        db.SaveChanges();                        

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "crPrintAdj.rpt"));

            report.SetParameterValue("swhere", " AND a.cmpcode='" + CompnyCode + "' AND a.resfield1 IN (" + id + ")");
            report.SetParameterValue("bu", CompnyName);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PrintOutAdjustment.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}