﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class ArmadaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public ArmadaController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listArmada
        {
            public int vhcoid { get; set; }
            public string vhccode { get; set; }
            public string vhcdesc { get; set; }
            public string drivname { get; set; }
            public DateTime vhclicexpdate { get; set; }
            public string vhcnote { get; set; }
            public string activeflag { get; set; }
        }

        public class listDriver
        {
            public string usoid { get; set; }
            public string usname { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "ARM/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(vhccode, "+ formatCounter  + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstvehicle WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND vhccode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;
            return sNo;
        }

        // GET/POST: Armada
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            sSql = "SELECT vhcoid, vhccode, vhcdesc, usname drivname, vhclicexpdate, vhcnote, activeflag FROM QL_mstvehicle vc INNER JOIN QL_m01US dr ON dr.usoid=vc.drivoid WHERE vc.cmpcode='" + Session["CompnyCode"].ToString() + "' order by vc.vhcoid ASC";
            List<listArmada> armada = db.Database.SqlQuery<listArmada>(sSql).ToList();
            return View(armada);          
        }

        // GET: Driver/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstvehicle Tbl;
            string action = "Create";
            if (id == null)
            {
                Tbl = new QL_mstvehicle();
                Tbl.vhccode = generateNo(ClassFunction.GetServerTime());
                Tbl.cmpcode = Session["CompnyCode"].ToString();
                Tbl.createuser = Session["UserID"].ToString();
                Tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                Tbl = db.QL_mstvehicle.Find(Session["CompnyCode"].ToString(), id); 
            }

            if (Tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(Tbl);
            FillAdditionalField(Tbl);
            return View(Tbl);
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstvehicle Tbldata, string action, string tglexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                Tbldata.vhclicexpdate = DateTime.Parse(ClassFunction.toDate(tglexp));
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("vhclicexpdate", "Tanggal tidak valid..!!" + ex.ToString());
            }

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstvehicle");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            Tbldata.vhcoid = mstoid;
                            Tbldata.createtime = servertime;
                            Tbldata.createuser = Session["UserID"].ToString();
                            Tbldata.updtime = servertime;
                            Tbldata.upduser = Session["UserID"].ToString();
                            db.QL_mstvehicle.Add(Tbldata);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstvehicle'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            Tbldata.updtime = servertime;
                            Tbldata.upduser = Session["UserID"].ToString();
                            db.Entry(Tbldata).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(Tbldata);
            FillAdditionalField(Tbldata);
            return View(Tbldata);
            }

        private void InitDDL(QL_mstvehicle tbl)
        { 
            sSql = "SELECT usoid, usname FROM QL_m01US us INNER JOIN QL_m05GN g ON g.gnoid=us.Jabatanoid WHERE g.gndesc='DRIVER' order by usname ";
            var drivoid = new SelectList(db.Database.SqlQuery<listDriver>(sSql).ToList(), "usoid", "usname", tbl.drivoid);
            ViewBag.drivoid = drivoid;

            sSql = "SELECT * FROM QL_m05GN ORDER BY gnoid";
            var gnoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.vhctypeoid);
            ViewBag.vhctypeoid = gnoid;     
        }

        private void FillAdditionalField(QL_mstvehicle tbl)
        {
            
        }

        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstvehicle tbldata = db.QL_mstvehicle.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstvehicle.Remove(tbldata);
                        db.SaveChanges();
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult PrintXlsReport(string typeexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptArmadaXls.rpt"));
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "ExportExpedisi_" + typeexp + ".xls");
        }
    }
}
