﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class UserRoleController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public UserRoleController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET/POST: UserRole
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            string sSql = "SELECT ur.cmpcode, ur.usoid, usname, COUNT(*) total from QL_m03UR ur INNER JOIN QL_m01US us ON us.usoid=ur.usoid WHERE ur.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " group by ur.cmpcode, ur.usoid, usname";
            var qL_m03UR = db.Database.SqlQuery<listuserrole>(sSql).ToList();
            return View(qL_m03UR);
        }

        // GET: UserRole/Form/5
        public ActionResult Form(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            QL_m03UR qL_m03UR;
            string action = "Create";
            if (id == null)
            {
                qL_m03UR = new QL_m03UR();
                qL_m03UR.cmpcode = Session["CompnyCode"].ToString();
                Session["QL_m03UR"] = null;
            }
            else
            {
                action = "Edit";
                qL_m03UR = db.QL_m03UR.Where(w => w.usoid==id).FirstOrDefault();
                string sSql = "select ur.rloid, rl.rlname, ur.urflag from QL_m03UR ur inner join QL_m02RL rl ON rl.rloid=ur.rloid where ur.usoid='" + id + "'";
                Session["QL_m03UR"] = db.Database.SqlQuery<userrole>(sSql).ToList();
            }
            
            if (qL_m03UR == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(qL_m03UR);
            return View(qL_m03UR);
        }

        // POST: UserRole/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m03UR tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<userrole> dataDtl = (List<userrole>)Session["QL_m03UR"];
            if (dataDtl == null)
                ModelState.AddModelError("", "Please fill User Role Detail");
            else if (dataDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill User Role Detail");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_m03UR");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                        }
                        else if (action == "Edit")
                        {
                            //delete old
                            var trndtl = db.QL_m03UR.Where(a => a.usoid == tbl.usoid);
                            db.QL_m03UR.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        //insert
                        QL_m03UR tbldtl;
                        for (int i = 0; i < dataDtl.Count(); i++)
                        {
                            //sSql = "INSERT INTO QL_m03UR (cmpcode, uroid, usoid, rloid, urflag, upduser, updtime) VALUES ('"+ tbl.cmpcode + "', "+ (mstoid++) + ", '"+ tbl.usoid + "', "+ dataDtl[i].rloid + ", '"+ dataDtl[i].urflag + "', '" + Session["UserID"].ToString() + "', '" + servertime + "')";
                            //db.Database.ExecuteSqlCommand(sSql);
                            tbldtl = new QL_m03UR();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.usoid = tbl.usoid;
                            tbldtl.uroid = mstoid++;
                            tbldtl.rloid = dataDtl[i].rloid;
                            tbldtl.urflag = dataDtl[i].urflag;
                            tbldtl.upduser = Session["UserID"].ToString();
                            tbldtl.updtime = servertime;
                            db.QL_m03UR.Add(tbldtl);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m03UR'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tbl);
            return View(tbl);
        }

        private void setViewBag(QL_m03UR qL_m03UR)
        {

            sSql = " SELECT * FROM QL_m01US WHERE ( usflag = 'ACTIVE' AND usoid<>'admin' AND ustype IN('admin','user') AND usoid not in (select x.usoid from QL_m03UR x)) OR usoid = '" + qL_m03UR.usoid + "'";
            ViewBag.usoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql), "usoid", "usname", qL_m03UR.usoid);
            ViewBag.rloid = new SelectList(db.QL_m02RL, "rloid", "rlname");
        }

        public ActionResult getQL_m03UR()
        {
            if (Session["QL_m03UR"] == null)
            {
                Session["QL_m03UR"] = new List<userrole>();
            }

            List<userrole> dataDtl = (List<userrole>)Session["QL_m03UR"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_m03UR(List<userrole> mdataDtl)
        {
            Session["QL_m03UR"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // POST: UserRole/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    var userroles = db.QL_m03UR.Where(a => a.usoid == id);
                    db.QL_m03UR.RemoveRange(userroles);
                    db.SaveChanges();
                    // Oh we are here, looks like everything is fine - save all the data permanently
                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    // roll back all database operations, if any thing goes wrong
                    objTrans.Rollback();

                    result = "failed";
                    msg += ex.ToString();
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public class userrole
        {
            public int rloid { get; set; }
            public string rlname { get; set; }
            public string urflag { get; set; }
        }

        public class listuserrole
        {
            public string cmpcode { get; set; }
            public string usoid { get; set; }
            public string usname { get; set; }
            public int total { get; set; }
        }
    }
}
