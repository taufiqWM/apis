﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;


namespace APIS_WB.Controllers.Transaction
{
     public class CheckDokController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public CheckDokController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listdok
        {
            public int checkdocmstoid { get; set; }
            public int reqrabmstoid { get; set; }
            public int rabmstoid { get; set; }
            public string checkdocno { get; set; }
            public DateTime checkdocdate { get; set; }
            public string custname { get; set; }
            public string rabno { get; set; }
            public string reqrabno { get; set; }
            public string expname { get; set; }
            public string noresi { get; set; }
            public string nosurat { get; set; }
            public string soitemno  { get; set; }
            public int soitemmstoid { get; set; }
            public string flagstatus { get; set; }
            public string projectname { get; set; }
        }
        
        public class listdokdtl
        {
            public int seq { get; set; }
            public int refoid { get; set; }
            public string docasli { get; set; }
            public string docname { get; set; }
            public string doccopy { get; set; }
            public string notedetail { get; set; }
        }

        public class soitemmst
        {
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; } 
            public int rabmstoid { get; set; }
            public int reqrabmstoid { get; set; }
            public string rabno { get; set; }
            public string reqrabno { get; set; }
            public string projectname { get; set; }
            public decimal soitemgrandtotalamt { get; set; }
            public decimal sisapiutang { get; set; }

        }

        public class expedisinya
        {
            public int expoid { get; set; }
            public string expname { get; set; } 
            public string expaddr { get; set; }
            public string gndesc { get; set; }
            public string expphone1 { get; set; } 
            public string expemail { get; set; }
        }

        public class listdpar
        {
            public int dparoid { get; set; }
            public string dparno { get; set; }
            public DateTime dpardate { get; set; }
            public int dparacctgoid { get; set; }
            public string acctgdesc { get; set; }
            public string dparnote { get; set; }
            public decimal dparamt { get; set; }
        }

        public class listrabmst
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public string reqrabno { get; set; }
            public DateTime rabdate { get; set; }
            public string custname { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }
            public string rabmstnote { get; set; }
        }

        public class listDataDokumen
        {
            public int sequence { get; set; }
            public int gnoid { get; set; }
            public string gndesc { get; set; }
            public string gngroup { get; set; } 
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listdokdtl> dtDtl)
        {
            Session["listdokdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["listdokdtl"] == null)
            {
                Session["listdokdtl"] = new List<listdokdtl>();
            }

            List<listdokdtl> dataDtl = (List<listdokdtl>)Session["listdokdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncheckdocmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();

            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();

            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT reqrabno FROM QL_trnreqrabmst r INNER JOIN QL_trnrabmst d ON d.reqrabmstoid=r.reqrabmstoid WHERE d.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();

            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();

            ViewBag.expname = db.Database.SqlQuery<string>("SELECT expname FROM QL_mstexpedisi e WHERE e.expoid ='" + tblmst.expoid + "'").FirstOrDefault();

            ViewBag.soitemno = db.Database.SqlQuery<string>("SELECT so.soitemno FROM QL_trnsoitemmst so INNER JOIN QL_trnrabmst rb ON so.rabmstoid=rb.rabmstoid WHERE rb.rabmstoid='" + tblmst.rabmstoid + "'").FirstOrDefault();

            ViewBag.amtdpar = db.Database.SqlQuery<decimal>("SELECT dparamt FROM QL_trndpar where dparoid = '"+ tblmst.dparoid +"'").FirstOrDefault();
        }

        // GET: CheckDok
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            string sfilter = "";

            sSql = "SELECT cd.cmpcode, checkdocmstoid, checkdocno, checkdocdate, c.custname, ISNULL(ex.expname,'') expname, ISNULL(nosurat,'') nosurat, ISNULL(noresi,'') noresi, flagstatus,rab.rabno, ISNULL(rr.reqrabno,'') reqrabno, rab.rabmstoid, ISNULL(rr.reqrabmstoid,0) reqrabmstoid, rab.projectname, so.soitemmstoid, so.soitemno FROM QL_trncheckdocmst cd INNER JOIN QL_mstcust c ON c.custoid=cd.custoid INNER JOIN QL_trnrabmst rab ON rab.rabmstoid=cd.rabmstoid INNER JOIN QL_trnsoitemmst so ON so.soitemmstoid=cd.soitemmstoid LEFT JOIN QL_mstexpedisi ex ON ex.expoid=cd.expoid LEFT JOIN QL_trnreqrabmst rr ON rr.reqrabmstoid=cd.reqrabmstoid WHERE cd.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + "";
            
            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CLD" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND cd.checkdocdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND cd.checkdocdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND cd.flagstatus IN ('In Process')";
            }
            sSql += " ORDER BY cd.checkdocdate";
            List<listdok> tblmst = db.Database.SqlQuery<listdok>(sSql).ToList();
            return View(tblmst);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "CLD/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(checkdocno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncheckdocmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND checkdocno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;
            return sNo;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            QL_trncheckdocmst tblmst;

            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trncheckdocmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.checkdocno = generateNo(ClassFunction.GetServerTime());
                tblmst.checkdocdate = ClassFunction.GetServerTime();
                tblmst.residate = ClassFunction.GetServerTime();
                tblmst.custoid = 0;
                tblmst.soitemmstoid = 0;
                tblmst.expoid = 0;
                tblmst.flagstatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                Session["listdokdtl"] = null;
            } else {
                action = "Edit";
                tblmst = db.QL_trncheckdocmst.Find(Session["CompnyCode"].ToString(), id);

                sSql= "SELECT seq,refoid,gndesc docname,docasli,doccopy,notedetail FROM QL_trncheckdocdtl dt INNER JOIN QL_m05GN g ON g.gnoid = refoid AND gngroup = 'DOKUMEN' where dt.checkdocmstoid = "+ id +"";
                Session["listdokdtl"] = db.Database.SqlQuery<listdokdtl>(sSql).ToList();
            }
            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tblmst = new List<QL_mstcust>();
            sSql = "SELECT * FROM QL_mstcust WHERE custoid IN (SELECT custoid FROM QL_trnsoitemmst where soitemmststatus IN ('Post', 'Closed') /*AND soitemmstoid IN (SELECT somstoid FROM QL_trnaritemmst where aritemmststatus IN ('Post', 'Closed'))*/) AND  activeflag='ACTIVE' ORDER BY custname";
            tblmst = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tblmst, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetSOData(int custoid)
        {
            List<soitemmst> tblmst = new List<soitemmst>();
            sSql = "SELECT soitemmstoid, soitemno, soitemdate, soitemgrandtotalamt, ISNULL((SELECT SUM(amttrans-amtbayar) FROM QL_conar con where con.custoid = c.custoid),0.0) sisapiutang, c.custoid, c.custname, rm.rabmstoid,rm.rabno, ISNULL(rq.reqrabno,'') reqrabno, ISNULL(rq.reqrabmstoid,0) reqrabmstoid, rm.projectname FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid LEFT JOIN QL_trnreqrabmst rq ON rq.reqrabmstoid=rm.reqrabmstoid WHERE som.custoid=" + custoid +" and som.soitemmststatus IN('Post','Closed')  ORDER BY soitemno";
            tblmst = db.Database.SqlQuery<soitemmst>(sSql).ToList();

            JsonResult js = Json(tblmst, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetExpData()
        {
            List<expedisinya> tblmst = new List<expedisinya>();
            sSql = "SELECT expoid,expname,expaddr,gndesc,e.expphone1,e.expemail FROM QL_mstexpedisi e INNER JOIN QL_m05GN m ON m.gnoid=e.expcityoid AND gngroup='KOTA' ORDER BY expoid";
            tblmst = db.Database.SqlQuery<expedisinya>(sSql).ToList();

            JsonResult js = Json(tblmst, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetDPARData(int custoid)
        {
            List<listdpar> tblmst = new List<listdpar>();
            sSql = "SELECT dp.dparoid, dp.dparno, dp.dpardate, dp.acctgoid AS dparacctgoid, a.acctgdesc, dp.dparnote, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) AS dparamt, (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='"+ CompnyCode + "' AND dp.dparstatus='Post' AND dp.dparpaytype NOT IN('DPFK') AND dp.custoid=" + custoid + " AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid";
            tblmst = db.Database.SqlQuery<listdpar>(sSql).ToList();

            JsonResult js = Json(tblmst, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetRABData(int soitemmstoid)
        {
            List<listrabmst> tbl = new List<listrabmst>();
            sSql = "SELECT rm.rabmstoid, rm.rabno, rm.rabdate, CONVERT(CHAR(10), rm.rabdate, 103) rabdatestr, c.custname, c.custoid, isnull(req.projectname, '') projectname, req.reqrabno, rm.rabmstnote, rm.alamatoid, c2.custdtl2addr alamat, rm.salesoid, rm.deptoid,req.reqrabmstoid, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid = rm.deptoid),'') departement FROM QL_trnrabmst rm LEFT JOIN QL_trnreqrabmst req ON req.reqrabmstoid = rm.reqrabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_mstcustdtl2 c2 ON c2.custdtl2oid = rm.alamatoid INNER JOIN QL_trnsoitemmst so ON so.rabmstoid=rm.rabmstoid WHERE rm.cmpcode = '" + Session["CompnyCode"].ToString() + "' AND so.soitemmstoid=" + soitemmstoid + "/*rabmststatus='Approved'*/ ORDER BY rabdate DESC";

            tbl = db.Database.SqlQuery<listrabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetDataDokumen()
        {
            List<listDataDokumen> tbl = new List<listDataDokumen>();

            sSql = "SELECT CAST(sequence as Int) sequence,gnoid,gndesc,gngroup FROM (SELECT row_number() over (order by gnoid desc) sequence,gnoid,gndesc,gngroup FROM QL_m05GN WHERE gngroup='DOKUMEN') gn Order By gndesc";
            tbl = db.Database.SqlQuery<listDataDokumen>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncheckdocmst tblmst, string action, string tglmst, string tglresi)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                tblmst.checkdocdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("checkttdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.residate = DateTime.Parse(ClassFunction.toDate(tglresi));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("checkttduedate", "Format Tanggal Resi Tidak Valid!!" + ex.ToString());
            }

            if (tblmst.custoid == 0)
            {
                ModelState.AddModelError("custoid", "Silahkan Pilih Customer!!");
            }

            if (tblmst.soitemmstoid == 0)
            {
                ModelState.AddModelError("soitemmstoid", "Silahkan Pilih SO!!");
            }

            if (tblmst.expoid == 0)
            {
                ModelState.AddModelError("expoid", "Silahkan Pilih Ekspedisi!!");
            }

            var servertime = ClassFunction.GetServerTime();
            //Tbldata.expphone1 = "";
            //Tbldata.expphone2 = "";

            List<listdokdtl> dtDtl = (List<listdokdtl>)Session["listdokdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Silahkan isi data detail!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Silahkan isi data detail!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        //if (string.IsNullOrEmpty(dtDtl[i].phone1))
                        //    dtDtl[i].phone1 = "";
                        //if (string.IsNullOrEmpty(dtDtl[i].phone2))
                        //    dtDtl[i].phone2 = "";
                        //if (string.IsNullOrEmpty(dtDtl[i].jabatan))
                        //    dtDtl[i].jabatan = "";
                        //if (string.IsNullOrEmpty(dtDtl[i].email))
                        //    dtDtl[i].email = "";
                        //if (string.IsNullOrEmpty(dtDtl[i].picname))
                        //{
                        //    dtDtl[i].picname = "";
                        //    ModelState.AddModelError("", "Silahkan Isi PIC!!");
                        //}
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.checkdocdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.flagstatus = "In Process";
            }
            if (tblmst.flagstatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.flagstatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncheckdocmst");
                var dtloid = ClassFunction.GenerateID("QL_trncheckdocdtl");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.checkdocmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trncheckdocmst.Add(tblmst);
                            db.SaveChanges();
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trncheckdocmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var tbldtl = db.QL_trncheckdocdtl.Where(a => a.checkdocmstoid == tblmst.checkdocmstoid);
                            db.QL_trncheckdocdtl.RemoveRange(tbldtl);
                            db.SaveChanges();
                        }

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                QL_trncheckdocdtl tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = new QL_trncheckdocdtl();
                                    tbldtl.cmpcode = tblmst.cmpcode;
                                    tbldtl.checkdocdtloid = dtloid++;
                                    tbldtl.checkdocmstoid = tblmst.checkdocmstoid;
                                    tbldtl.seq = i + 1;
                                    tbldtl.refoid = dtDtl[i].refoid;
                                    tbldtl.docasli = dtDtl[i].docasli;
                                    tbldtl.doccopy = dtDtl[i].doccopy;
                                    tbldtl.notedetail = dtDtl[i].notedetail;
                                    tbldtl.upduser = tblmst.upduser;
                                    tbldtl.updtime = tblmst.updtime;
                                    db.QL_trncheckdocdtl.Add(tbldtl);
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trncheckdocdtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncheckdocmst tblmst = db.QL_trncheckdocmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trncheckdocdtl.Where(a => a.checkdocmstoid == id);
                        db.QL_trncheckdocdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncheckdocmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCheckDoc.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE cm.cmpcode='" + CompnyCode + "' AND cm.checkdocmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptCheckDoc.pdf");
        }

        public ActionResult PrintReportTT(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCheckDoctt.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE cm.cmpcode='" + CompnyCode + "' AND cm.checkdocmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptCheckDocTT.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}