﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SuppReportController : Controller
    {
        // GET: SuppReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public SuppReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetSuppData(string DDLStatus)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.suppcode [Kode], c.suppname [Nama], c.suppaddr [Alamat] FROM QL_mstsupp c WHERE c.cmpcode='" + CompnyCode + "'";               
                if (DDLStatus !="ALL")
                {
                    sSql += " AND s.activeflag='"+ DDLStatus + "'";
                }
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetPkmData(string DDLStatus, string TextSupp)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select 0 Seq, pic [Nama PIC], tlp [No. Telp], Email From (Select d2.suppoid, suppdtl2phone1 tlp, suppdtl2picname pic, suppdtl2jabatan jabatan, suppdtl2email Email From QL_mstsuppdtl2 d2 UNION ALL Select d3.suppoid, suppdtl3phone1 tlp, suppdtl3picname pic, suppdtl3jabatan jabatan, suppdtl3email Email From QL_mstsuppdtl3 d3 ) dt Where suppoid IN (Select suppoid From QL_mstsupp Where cmpcode='" + CompnyCode + "'";

                if (DDLStatus != "ALL")
                {
                    sSql += " AND activeflag='"+ DDLStatus +"'";
                }

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += ") Order by suppoid";
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPKM");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account"); 
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLStatus, string TextSupp, string TextPKM, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "rptSupplier.rpt";
            var rptname = "LAP_SUPPLIER";

            sSql = "Select s.suppoid, s.suppcode, s.suppname, s.suppaddr, c.gndesc kota, d.gndesc propinsi, s.suppnpwp, s.suppnpwpaddr, s.suppnpwpno, s.suppphone1, s.suppphone2, Case s.supppajak When '1' Then 'Ya' Else 'Tidak' End supppajak, Case When s.supppengakuan = 'TT' Then 'Tukar Tagihan' Else 'Penerimaan Barang/ Jasa' End supppengakuan, Case s.suppsurgan When 'N' Then 'Tidak' Else 'Ada' End suppsurgan, p.gndesc Termin, s.suppcategory, s.activeflag, dt.pic, dt.alamat, tlp, Email, jabatan, produk From QL_mstsupp s Inner Join QL_m05GN c ON c.gnoid = s.suppcityOid Inner Join QL_m05GN d ON d.gnoid = s.suppprovinceoid Inner Join QL_m05GN p ON p.gnoid = s.supppaymentoid Left Join (Select d1.suppoid, d1.suppdtl1oid suppdtloid, d1.suppdtl1addr alamat, d1.suppdtl1phone tlp, '' pic, '' jabatan, '' Email, '' produk From QL_mstsuppdtl1 d1 Inner Join QL_m05GN c On c.gnoid = d1.suppcityoid UNION ALL Select d2.suppoid, suppdtl2oid suppdtloid, '' alamat, suppdtl2phone1 tlp, suppdtl2picname pic, suppdtl2jabatan jabatan, suppdtl2email Email, '' produk From QL_mstsuppdtl2 d2 UNION ALL Select d3.suppoid, suppdtl3oid suppdtloid, '' alamat, suppdtl3phone1 tlp, suppdtl3picname pic, suppdtl3jabatan jabatan, suppdtl3email Email, suppdtl3picproduk produk From QL_mstsuppdtl3 d3) dt ON dt.suppoid = s.suppoid Where s.cmpcode='" + CompnyCode + "'";

            if (TextSupp != "")
            {
                string[] arr = TextSupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextPKM != "")
            {
                string[] arr = TextPKM.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND pic IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLStatus != "ALL")
            {
                sSql += " AND  s.activeflag='" + DDLStatus + "'";
            }
            sSql += "Order By s.suppcode ASC"; 

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            //rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}