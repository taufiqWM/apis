﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class FBAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultFormatCounter = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public FBAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class apassetmst
        {
            public string cmpcode { get; set; }
            public int apassetmstoid { get; set; }
            public string apassetno { get; set; }
            public DateTime apassetdate { get; set; }
            public DateTime apassetdatetakegiro { get; set; }
            public DateTime duedate { get; set; }
            public string suppname { get; set; }
            public string apassetmststatus { get; set; }
            public string apassetmstnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal apassetgrandtotal { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal apassetgrandtotalsupp { get; set; }
            public string divname { get; set; }
        }

        public class apassetdtl
        {
            public int apassetdtlseq { get; set; }
            public int mrassetmstoid { get; set; }
            public string mrassetno { get; set; }
            public string poassetno { get; set; }
            public int mrassetdtloid { get; set; }
            public int apassetrefoid { get; set; }
            public string apassetreftype { get; set; }
            public string apassetrefcode { get; set; }
            public string apassetreflongdesc { get; set; }
            public decimal apassetqty { get; set; }
            public int apassetunitoid { get; set; }
            public string apassetunit { get; set; }
            public decimal apassetprice { get; set; }
            public decimal apassetdtlamt { get; set; }
            public string apassetdtldisctype { get; set; }
            public decimal apassetdtldiscvalue { get; set; }
            public decimal apassetdtldiscamt { get; set; }
            public decimal apassetdtlnetto { get; set; }
            public decimal apassetdtltaxvalue { get; set; }
            public decimal apassetdtltaxamt { get; set; }
            public string apassetdtlnote { get; set; }
            public string apassetdtlres1 { get; set; }
            public string apassetdtlres2 { get; set; }
        }

        public class listapitemdtl2
        {
            public int apitemdtl2seq { get; set; }
            public int fabelioid { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
            public decimal apitemdtl2amt { get; set; }
        }

        public class mrassetmst
        {
            public int mrassetmstoid { get; set; }
            public string mrassetno { get; set; }
            public DateTime mrassetdate { get; set; }
            public string mrassetmstnote { get; set; }
            public string mrassetflag { get; set; }
        }
        private void InitDDL(QL_trnapassetmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='PAYMENT TERM' AND gnflag='ACTIVE'";
            var apassetpaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.apassetpaytypeoid);
            ViewBag.apassetpaytypeoid = apassetpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        //[HttpPost]
        //public ActionResult InitDDLAppUser(string cmp)
        //{
        //    var result = "sukses";
        //    var msg = "";
        //    List<QL_APPperson> tbl = new List<QL_APPperson>();
        //    sSql = "SELECT * FROM QL_APPperson WHERE tablename='QL_trnapassetmst' AND gnflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
        //    tbl = db.Database.SqlQuery<QL_APPperson>(sSql).ToList();

        //    return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        //}

        public class mstsupp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public string supppengakuan { get; set; }
        }

        public class listpo
        {
            public int poassetmstoid { get; set; }
            public string poassetno { get; set; }
            public DateTime poassetdate { get; set; }
            public string poassetmstnote { get; set; }
            public int curroid { get; set; }
            public DateTime checkttdate { get; set; }
            public decimal dpfmamt { get; set; }
        }

        [HttpPost]
        public ActionResult GetSupplierData(int apassetmstoid, string sType)
        {
            List<mstsupp> tbl = new List<mstsupp>();

            sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid, supppengakuan FROM QL_mstsupp s INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode=s.cmpcode AND mrm.suppoid=s.suppoid WHERE s.cmpcode='" + CompnyCode + "' AND ISNULL(mrassetmstres1, '')='' AND mrm.mrassetmststatus='Post' AND mrm.tipebarang='"+ sType +"' AND mrm.mrassetmstoid NOT IN (SELECT DISTINCT mrassetmstoid FROM QL_trnapassetdtl apd INNER JOIN QL_trnapassetmst apm ON apm.cmpcode=apd.cmpcode AND apm.apassetmstoid=apd.apassetmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND apassetmststatus<>'Rejected' AND apm.apassetmstoid <> " + apassetmstoid + ") ORDER BY suppcode ";

            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFakturData(int suppoid)
        {
            List<listapitemdtl2> tbl = new List<listapitemdtl2>();

            sSql = "SELECT 0 AS apitemdtl2seq, fa.fabelioid, fa.fabelicode, fa.fakturno, (fa.fabeliqty-fa.fabeliaccumqty) fabeliaccumqty, 0.0 AS apitemdtl2amt FROM QL_mstfabeli fa WHERE fa.activeflag='ACTIVE' AND fa.suppoid=" + suppoid + " AND (fabeliqty-fabeliaccumqty)>0 ORDER BY fa.fakturno";
            tbl = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPOData(int suppoid, string sType, string supppengakuan)
        {
            List<listpo> tbl = new List<listpo>();
            var Join = "";
            var SqlPlus = ", getdate() checkttdate";
            if (supppengakuan == "TT")
            {
                Join = " INNER JOIN QL_trncheckttdtl cd ON cd.poitemmstoid = pm.poassetmstoid INNER JOIN QL_trncheckttmst cm ON cm.checkttmstoid = cd.checkttmstoid AND cm.checkttmststatus = 'Post' AND cd.potype='QL_trnpoassetmst' ";
                SqlPlus = ", (SELECT checkttdate FROM QL_trncheckttmst where checkttmstoid = cd.checkttmstoid) checkttdate, (SELECT apitemno FROM QL_trncheckttdtl where poitemmstoid = pm.poassetmstoid AND potype='QL_trnapassetmst') apitemno";
            }
            sSql = "SELECT DISTINCT pm.poassetmstoid, pm.poassetno, pm.poassetdate,  pm.poassetmstnote, pm.curroid, ISNULL((SELECT SUM(dp.dpapamt-dp.dpapaccumamt) FROM QL_trndpap dp WHERE dp.pomstoid=pm.poassetmstoid AND dp.poreftype='QL_trnpoassetmst' AND dp.dpappaytype='DPFM' AND ISNULL(dpapres1,'')<>'Batal'),0.0) dpfmamt " + SqlPlus + " FROM QL_trnpoassetmst pm "+ Join + " WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.suppoid= " + suppoid + " AND pm.tipebarang='"+ sType +"' AND pm.poassetmststatus IN('Approved','Closed') /*AND pm.poassetmstoid NOT IN(SELECT apm.poassetmstoid FROM QL_trnapassetmst apm WHERE apm.apassetmststatus='Closed')*/ ORDER BY poassetdate DESC";
            tbl = db.Database.SqlQuery<listpo>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMRData(int suppoid, int apassetmstoid, int curroid, int poassetmstoid, string sType)
        {
            List<mrassetmst> tbl = new List<mrassetmst>();
            string sPlus = "";
            if (sType == "Asset")
            {
                //sPlus = " AND regm.mrassetmstoid IN (SELECT mrm.mrassetmstoid FROM QL_trnassetrecmst mrm WHERE mrm.cmpcode = '" + CompnyCode + "' AND mrm.assetrecmststatus = 'Post')";
            }
            sSql = "SELECT DISTINCT regm.mrassetmstoid, mrassetno, mrassetdate, mrassetmstnote FROM QL_trnmrassetmst regm INNER JOIN QL_trnmrassetdtl regd ON regd.cmpcode=regm.cmpcode AND regd.mrassetmstoid=regm.mrassetmstoid INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=regd.cmpcode AND pom.poassetmstoid=regm.poassetmstoid WHERE regm.cmpcode='" + CompnyCode + "' AND regm.suppoid=" + suppoid + " AND pom.poassetmstoid="+ poassetmstoid + " AND pom.tipebarang='"+ sType + "' AND regm.curroid = " + curroid + " AND mrassetmststatus='Post' AND regm.mrassetmstoid NOT IN(SELECT mrassetmstoid FROM QL_trnapassetdtl apd INNER JOIN QL_trnapassetmst apm ON apm.cmpcode = apd.cmpcode AND apm.apassetmstoid = apd.apassetmstoid WHERE apd.cmpcode = '" + CompnyCode + "' AND apassetmststatus <> 'Rejected' AND apm.apassetmstoid <> " + apassetmstoid + ") "+ sPlus +" ORDER BY regm.mrassetmstoid ";

            tbl = db.Database.SqlQuery<mrassetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetDataDetails(int mrassetmstoid, string apassetdtlres1, string apassetdtlres2)
        {

            List<apassetdtl> tbl = new List<apassetdtl>();
            sSql = "SELECT apassetdtlseq, mrassetmstoid, mrassetno, poassetno, mrassetdtloid, apassetreftype, apassetrefoid, apassetrefcode, apassetreflongdesc, apassetqty, apassetunitoid, apassetunit, apassetprice, 0.0 AS apassetdtlamt, apassetdtldisctype, apassetdtldiscvalue, 0.0 apassetdtldiscamt, poassetdtltaxvalue apassetdtltaxvalue, 0.0 AS apassetdtlnetto, 0.0 apassetdtltaxamt, apassetdtlnote, apassetdtlres1 ,apassetdtlres2 FROM (SELECT 0 AS apassetdtlseq, mrm.mrassetmstoid, mrm.mrassetno, ISNULL(pom.poassetno, '') poassetno, mrassetdtloid, mrassetreftype apassetreftype, mrd.mrassetrefoid AS apassetrefoid, (SELECT itemcode FROM QL_mstitem where itemoid = mrassetrefoid) AS apassetrefcode, (SELECT itemdesc FROM QL_mstitem where itemoid = mrassetrefoid) AS apassetreflongdesc, (mrassetqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid WHERE pretd.cmpcode = mrd.cmpcode AND pretd.mritemdtloid = mrd.mrassetdtloid AND ISNULL(pretm.pretitemtype,'')='Asset' AND pretitemmststatus <> 'Rejected'), 0.0)) AS apassetqty, mrassetunitoid AS apassetunitoid, gndesc AS apassetunit, poassetprice apassetprice, poassetdtldisctype apassetdtldisctype, poassetdtldiscvalue AS apassetdtldiscvalue, poassettaxtype, poassetdtltaxvalue, poassetdtltaxamt, '' apassetdtlnote, '" + apassetdtlres1 + "' AS apassetdtlres1, '" + apassetdtlres2 + "' AS apassetdtlres2 FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = mrd.cmpcode  AND mrm.mrassetmstoid = mrd.mrassetmstoid INNER JOIN QL_m05GN g ON gnoid = mrd.mrassetunitoid INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode = mrd.cmpcode AND pom.poassetmstoid = mrm.poassetmstoid INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pom.poassetmstoid = pod.poassetmstoid AND pod.poassetdtloid = mrd.poassetdtloid  WHERE mrd.cmpcode = '" + CompnyCode + "' AND mrd.mrassetmstoid = " + mrassetmstoid + " AND ISNULL(mrassetdtlres1, '')= '' AND ISNULL(mrassetmstres1, '')= '' AND mrassetmststatus = 'Post' AND (mrassetqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl pretd INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode = pretd.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid WHERE pretd.cmpcode = mrd.cmpcode AND pretd.mritemdtloid = mrd.mrassetdtloid AND ISNULL(pretm.pretitemtype,'')='Asset' AND pretitemmststatus <> 'Rejected'), 0.0)) > 0 AND mrassetdtlstatus = '') AS tbl ";

            tbl = db.Database.SqlQuery<apassetdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<apassetdtl> dtDtl, Guid? uid)
        {
            Session["QL_trnapassetdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trnapassetdtl"+ uid] == null)
            {
                Session["QL_trnapassetdtl"+ uid] = new List<apassetdtl>();
            }

            List<apassetdtl> dataDtl = (List<apassetdtl>)Session["QL_trnapassetdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<apassetdtl> dtDtl, List<listapitemdtl2> dtDtl2, Guid? uid)
        {
            Session["QL_trnapassetdtl"+ uid] = dtDtl;
            Session["QL_trnapassetdtl2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listapitemdtl2> dtDtl, Guid? uid)
        {
            Session["QL_trnapassetdtl2"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["QL_trnapassetdtl2"+ uid] == null)
            {
                Session["QL_trnapassetdtl2"+ uid] = new List<listapitemdtl2>();
            }

            List<listapitemdtl2> dataDtl = (List<listapitemdtl2>)Session["QL_trnapassetdtl2"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnapassetmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.poassetno = db.Database.SqlQuery<string>("SELECT poassetno FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + tbl.poassetmstoid + "").FirstOrDefault();
            ViewBag.supppengakuan = db.Database.SqlQuery<string>("SELECT supppengakuan FROM QL_mstsupp WHERE suppoid ='" + tbl.suppoid + "'").FirstOrDefault();
            //ViewBag.apassetttdate = db.Database.SqlQuery<string>("SELECT CONVERT(varchar(20),apassetttdate,103) FROM QL_trnapassetmst WHERE apassetmstoid ='" + tbl.apassetmstoid + "'").FirstOrDefault();
        }

        // GET: apassetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No FB",["Supplier"] = "Supplier"}, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT apassetmstoid, apassetno, apassetdate, suppname, apassetmststatus, apassetmstnote, 'Print' AS printtext, 'rptPrintOutAP' AS rptname, apm.cmpcode, 'raw' AS formtype, 'False' AS checkvalue, apassetgrandtotal , apassetdatetakegiro,  CONVERT(DATETIME,DATEADD(day,(CASE CAST(g1.gnother1 AS INT) WHEN 0 THEN 0 ELSE CAST(g1.gnother1 AS INT) END),apassetdate), 101) duedate FROM QL_trnapassetmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid INNER JOIN QL_m05GN g1 ON g1.gnoid=apassetpaytypeoid AND g1.gngroup='PAYMENT TERM' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.apassetdate >='" + param.filterperiodfrom + " 00:00:00' AND t.apassetdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.apassetmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.apassetno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.apassetmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.apassetmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.apassetmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.apassetmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult generateNo(int apmstoid, string sType)
        {
            sSql = "SELECT apassetno FROM QL_trnapassetmst WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + apmstoid;
            string sNoAP = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            if (string.IsNullOrEmpty(sNoAP))
            {
                var prefix = "FBA";
                if (sType == "Konsinyasi")
                {
                    prefix = "FBK";
                }
                string sNo = prefix + "/" + ClassFunction.GetServerTime().ToString("yy") + "/" + ClassFunction.GetServerTime().ToString("MM") + "/";

                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apassetno, "+ DefaultFormatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapassetmst WHERE cmpcode='" + CompnyCode + "' AND apassetno LIKE '" + sNo + "%'";

                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultFormatCounter);

                sNo = sNo + sCounter;
                return Json(sNo, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(sNoAP, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: apassetMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapassetmst tbl;
            string action = "Create";
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trnapassetmst();
                tbl.apassetmstoid = ClassFunction.GenerateID("QL_trnapassetmst");
                tbl.apassetdate = ClassFunction.GetServerTime();
                tbl.apassetttdate = ClassFunction.GetServerTime();
                tbl.apassetdatetakegiro = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.apassetmstdiscamt = 0;
                tbl.apassetmststatus = "In Process";

                Session["QL_trnapassetdtl"+ ViewBag.uid] = null;
                Session["QL_trnapassetdtl2"+ ViewBag.uid] = null;
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trnapassetmst.Find(CompnyCode, id);

                sSql = "SELECT apassetdtlseq, apd.mrassetmstoid, mrassetno, mrassetdtloid, apd.apassetreftype, apd.apassetrefoid, (SELECT x.itemcode FROM QL_mstitem x WHERE x.itemoid=apd.apassetrefoid) AS apassetrefcode, (SELECT x.itemdesc FROM QL_mstitem x WHERE x.itemoid=apd.apassetrefoid) AS apassetreflongdesc, apassetqty, apassetunitoid, gndesc AS apassetunit, apassetprice, apassetdtlamt, apassetdtldisctype, apassetdtldiscvalue, apassetdtldiscamt, apassetdtlnetto, 10.00 AS apassetdtltaxvalue, apassetdtltaxamt, apassetdtlnote, ISNULL(apassetdtlres1, '') AS apassetdtlres1, ISNULL(apassetdtlres2, '') AS apassetdtlres2, ISNULL((SELECT pom.poassetno FROM QL_trnpoassetmst pom INNER JOIN QL_trnmrassetmst regm ON regm.poassetmstoid = pom.poassetmstoid INNER JOIN QL_trnmrassetdtl regd ON pom.cmpcode = regd.cmpcode AND regd.mrassetmstoid = regm.mrassetmstoid WHERE regd.cmpcode = regm.cmpcode AND regd.mrassetmstoid = mrm.mrassetmstoid AND regd.mrassetdtloid = apd.mrassetdtloid),'') poassetno FROM QL_trnapassetdtl apd INNER JOIN QL_trnmrassetmst mrm ON mrm.cmpcode = apd.cmpcode AND mrm.mrassetmstoid = apd.mrassetmstoid INNER JOIN QL_m05GN g ON gnoid = apassetunitoid WHERE apassetmstoid = " + id + " AND apd.cmpcode = '" + CompnyCode + "' ORDER BY apassetdtlseq ";
                Session["QL_trnapassetdtl"+ ViewBag.uid] = db.Database.SqlQuery<apassetdtl>(sSql).ToList();

                sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trnapassetmst' ORDER BY rd.apitemdtl2seq";
                Session["QL_trnapassetdtl2"+ ViewBag.uid] = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();
            }
            ViewBag.deftax = db.QL_m05GN.FirstOrDefault(x => x.gngroup == "DEFAULT TAX").gndesc ?? "10";

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: apassetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapassetmst tbl, string action, string tglmst, string tglgiro, string tgljt, Guid? uid)
        {
            var cRate = new ClassRate();

            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.apassetdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apassetdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.apassetdatetakegiro = DateTime.Parse(ClassFunction.toDate(tglgiro));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apassetdatetakegiro", "Format Tanggal Ambil Giro Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.apassetttdate = DateTime.Parse(ClassFunction.toDate(tgljt));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apitemttdate", "Format Tanggal JT Tidak Valid!!" + ex.ToString());
            }
            if (tbl.cmpcode == null)
                tbl.cmpcode = CompnyCode;
            if (tbl.apassetno == null)
                tbl.apassetno = "";

            List<apassetdtl> dtDtl = (List<apassetdtl>)Session["QL_trnapassetdtl"+ uid];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        //sSql = "SELECT COUNT(*) FROM QL_trnmrassetmst mrm WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + dtDtl[i].mrassetmstoid + " AND mrassetmststatus='Closed'";
                        //if (action == "Edit")
                        //{
                        //    sSql += " AND mrassetmstoid NOT IN (SELECT apd.mrassetmstoid FROM QL_trnapassetdtl apd WHERE apd.cmpcode=mrm.cmpcode AND apassetmstoid=" + tbl.apassetmstoid + ")";
                        //}
                        //var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        //if (iCek > 0)
                        //    ModelState.AddModelError("", "PB No. " + dtDtl[i].mrassetno + " has been used by another data. Please cancel this transaction or use another PB No.!");

                        if (action == "Create")
                        {
                            //Cek MR Sdh di tarik
                            var mrdtloid = dtDtl[i].mrassetdtloid;
                            if (db.QL_trnmrassetdtl.Where(md => md.mrassetdtloid == mrdtloid && md.mrassetdtlstatus == "Complete").Count() > 0)
                                ModelState.AddModelError("", "BPB No. " + dtDtl[i].mrassetno + " Sudah Dibuatkan FB!!");
                        }
                        else
                        {
                            //Cek MR Sdh di tarik
                            var mrdtloid = dtDtl[i].mrassetdtloid; var mrmstoid = dtDtl[i].mrassetmstoid;
                            if (db.QL_trnmrassetdtl.Where(md => md.mrassetdtloid == mrdtloid && md.mrassetdtlstatus == "Complete" && md.mrassetmstoid != mrmstoid).Count() > 0)
                                ModelState.AddModelError("", "BPB No. " + dtDtl[i].mrassetno + " Sudah Dibuatkan FB!!");
                        }
                           
                    }
                }
            }

            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["QL_trnapassetdtl2"+ uid];
            if (tbl.apassetmststatus == "Post")
            {
                if (Math.Round(tbl.apassettotaltax.Value, 0) > 10 || Math.Round(tbl.apassettotaltax.Value, 0) < (-10))
                {
                    if (dtDtl2 == null)
                        ModelState.AddModelError("", "Please fill detail faktur data!");
                    else if (dtDtl2.Count <= 0)
                        ModelState.AddModelError("", "Please fill detail faktur data!");
                }
            }

            var msg = "";

            cRate.SetRateValue(tbl.curroid, tbl.apassetdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            if (msg != "")
                ModelState.AddModelError("", msg);            

            if (tbl.apassetmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED"));
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_IN"));
                if (!ClassFunction.IsInterfaceExists("VAR_AP", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AP"));
                if (!ClassFunction.IsInterfaceExists("VAR_DISC_AP", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DISC_AP"));
            }

            string apitemdtl2type = "QL_trnapassetmst";
            decimal dpfmamt = tbl.apdpamt;
            //db.Database.SqlQuery<decimal>("SELECT ISNULL((SELECT SUM(dp.dpapamt) FROM QL_trndpap dp WHERE dp.pomstoid = " + tbl.poassetmstoid + " AND dp.poreftype = 'QL_trnpoassetmst' AND dp.dpappaytype = 'DPFM' AND ISNULL(dp.dpapres1,'')<>'Batal'),0.0) dpfmamt").FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    totaltaxhdr = tbl.apassettotaltax.Value;
                    decimal totaltaxdtl = 0;
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totaltaxdtl += dtDtl2[i].apitemdtl2amt;
                        fakturnoGL += dtDtl2[i].fakturno + ",";
                    }
                    fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                    if (dpfmamt > 0)
                    {
                        //totaltaxhdr = totaltaxhdr + dpfmamt;
                    }
                    decimal ht_tax = Math.Round(totaltaxhdr, 0) - Math.Round(totaltaxdtl, 0);
                    if (ht_tax > 10 || ht_tax < (-10))
                    {
                        ModelState.AddModelError("apassettotaltax", "Total Detail Faktur Pajak Harus Sama Dengan Jumlah PPN!");
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.apassetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.apassetmststatus = "In Process";
            }
            if (tbl.apassetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.apassetmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var mstoid = ClassFunction.GenerateID("QL_trnapassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnapassetdtl");
                var dtl2oid = ClassFunction.GenerateID("QL_trnapitemdtl2");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));
                var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP", CompnyCode));
                var iAcctgOidDisc = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DISC_AP", CompnyCode));

                tbl.apassettotalamtidr = (tbl.apassettotalamt * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassettotalamtusd = (tbl.apassettotalamt * decimal.Parse(tbl.apassetrate2tousd));
                tbl.apassettotaldiscidr = (tbl.apassettotaldisc * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassettotaldiscusd = (tbl.apassettotaldisc * decimal.Parse(tbl.apassetrate2tousd));
                tbl.apassettotaltaxidr = (tbl.apassettotaltax * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassettotaltaxusd = (tbl.apassettotaltax * decimal.Parse(tbl.apassetrate2tousd));
                tbl.apassetgrandtotalidr = (tbl.apassetgrandtotal * decimal.Parse(tbl.apassetrate2toidr));
                tbl.apassetgrandtotalusd = (tbl.apassetgrandtotal * decimal.Parse(tbl.apassetrate2tousd));

                tbl.cancelseq = 0;
                tbl.apcloseuser = "";
                tbl.apclosetime = DateTime.Parse("1/1/1900 00:00:00");
                if (tbl.apassetmstnote == null)
                    tbl.apassetmstnote = "";
                if (tbl.apassettotaldisc == null)
                    tbl.apassettotaldisc = 0;
                if (tbl.apassettotaldiscidr == null)
                    tbl.apassettotaldiscidr = 0;
                if (tbl.apassettotaldiscusd == null)
                    tbl.apassettotaldiscusd = 0;
                var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tbl.suppoid).FirstOrDefault();
                DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(DAY, (CASE g.gnother1 WHEN 0 THEN 0 ELSE CAST(g.gnother1 AS INT) END), '" + tbl.apassetttdate + "') AS [Due Date] FROM QL_m05gn g WHERE g.gnoid=" + tbl.apassetpaytypeoid + " AND g.gngroup='PAYMENT TERM'").FirstOrDefault();
                tbl.apassetduedate = duedate;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trnapassetmst.Find(tbl.cmpcode, tbl.apassetmstoid) != null)
                                tbl.apassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.apassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnapassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.apassetmstoid + " WHERE tablename='QL_trnapassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE MRDTL SET mrassetdtlstatus='' FROM (SELECT mrassetdtlstatus FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnapassetdtl apd ON apd.cmpcode=mrd.cmpcode AND apd.mrassetmstoid=mrd.mrassetmstoid AND apd.mrassetdtloid=mrd.mrassetdtloid  WHERE apd.cmpcode='" + tbl.cmpcode + "' AND apd.apassetmstoid=" + tbl.apassetmstoid + ") AS MRDTL ";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnapassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapassetdtl.Where(a => a.apassetmstoid == tbl.apassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tbl.apassetmstoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tbl.cmpcode);
                            db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        QL_trnapassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].apassetdtlnote == null)
                                dtDtl[i].apassetdtlnote = "";
                            if (dtDtl[i].apassetdtlres2 == null)
                                dtDtl[i].apassetdtlres2 = "";

                            tbldtl = new QL_trnapassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.apassetdtloid = dtloid++;
                            tbldtl.apassetmstoid = tbl.apassetmstoid;
                            tbldtl.apassetdtlseq = i + 1;
                            tbldtl.mrassetdtloid = dtDtl[i].mrassetdtloid;
                            tbldtl.mrassetmstoid = dtDtl[i].mrassetmstoid;
                            tbldtl.apassetreftype = dtDtl[i].apassetreftype;
                            tbldtl.apassetrefoid = dtDtl[i].apassetrefoid;
                            tbldtl.apassetqty = dtDtl[i].apassetqty;
                            tbldtl.apassetunitoid = dtDtl[i].apassetunitoid;
                            tbldtl.apassetprice = dtDtl[i].apassetprice;
                            tbldtl.apassetdtlamt = dtDtl[i].apassetdtlamt;
                            tbldtl.apassetdtldisctype = dtDtl[i].apassetdtldisctype;
                            tbldtl.apassetdtldiscvalue = dtDtl[i].apassetdtldiscvalue;
                            tbldtl.apassetdtldiscamt = dtDtl[i].apassetdtldiscamt;
                            tbldtl.apassetdtlnetto = dtDtl[i].apassetdtlnetto;
                            tbldtl.apassetdtltaxamt = dtDtl[i].apassetdtltaxamt;
                            tbldtl.apassetdtlnote = dtDtl[i].apassetdtlnote;
                            tbldtl.apassetdtlstatus = "";
                            tbldtl.apassetdtlres1 = dtDtl[i].apassetdtlres1;
                            tbldtl.apassetdtlres2 = dtDtl[i].apassetdtlres2;
                            tbldtl.apassetdtlres3 = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnapassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetdtloid=" + dtDtl[i].mrassetdtloid + " AND mrassetmstoid=" + dtDtl[i].mrassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + dtDtl[i].mrassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        sSql = "UPDATE QL_id SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnapassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnapitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnapitemdtl2();
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.apitemdtl2oid = dtl2oid++;
                                    tbldtl2.apitemmstoid = tbl.apassetmstoid;
                                    tbldtl2.apitemdtl2seq = i + 1;
                                    tbldtl2.fabelioid = dtDtl2[i].fabelioid;
                                    tbldtl2.apitemdtl2amt = dtDtl2[i].apitemdtl2amt;
                                    tbldtl2.createuser = tbl.upduser;
                                    tbldtl2.createtime = tbl.updtime;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.apitemdtl2type = apitemdtl2type;

                                    db.QL_trnapitemdtl2.Add(tbldtl2);
                                    db.SaveChanges();

                                    if (tbl.apassetmststatus == "Post")
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }

                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnapitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dpfmamt > 0)
                        {
                            //Update Accum DPFM
                            sSql = "UPDATE QL_trndpap SET dpapaccumamt=dpapamt WHERE pomstoid = " + tbl.poassetmstoid + " AND poreftype = 'QL_trnapassetmst' AND dpappaytype = 'DPFM' AND ISNULL(dpapres1,'')<>'Batal'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.apassetmststatus == "Post")
                        {
                            // Insert QL_conap                        
                            db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnapassetmst", tbl.apassetmstoid, 0, tbl.suppoid, iAcctgOidAP, "Post", "APFA", servertime, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tbl.apassetgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tbl.apassetgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.apassetno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;

                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "D", tbl.apassettotalamt, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, tbl.apassettotalamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0));
                            db.SaveChanges();

                            decimal totaltax = Convert.ToDecimal(tbl.apassettotaltax);
                            if (totaltax > 10)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0, fakturnoGL));
                                db.SaveChanges();
                            }

                            if (dpfmamt > 0)
                            {
                                //glmstoid += 1;
                                //// Insert QL_trnglmst
                                //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.apitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                //db.SaveChanges();

                                // Insert QL_trngldtl
                                // D : Hutang Suspend
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "D", dpfmamt, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, dpfmamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0));
                                db.SaveChanges();

                                //// C : DPFM
                                //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidDPFM, "C", dpfmamt, tblmst.apitemno, tblmst.apitemno + " | " + suppname + " | " + tblmst.apitemmstnote, "Post", Session["UserID"].ToString(), servertime, dpfmamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apitemmstoid, null, null, null, 0));
                                //db.SaveChanges();
                            }

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAP, "C", tbl.apassetgrandtotal, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, tbl.apassetgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0));
                            db.SaveChanges();

                            decimal totaldisc = Convert.ToDecimal(tbl.apassetmstdiscamt);
                            if (totaldisc > 0)
                            {
                                if (tbl.apassetmstdisctype == "H")
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidDisc, "C", totaldisc, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                                else if (tbl.apassetmstdisctype == "P")
                                {
                                    glmstoid += 1;

                                    // Insert QL_trnglmst
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.apassetno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                    db.SaveChanges();

                                    var glseq2 = 1;

                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq2++, glmstoid, iAcctgOidRec, "D", totaldisc, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0));
                                    db.SaveChanges();

                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq2++, glmstoid, iAcctgOidDisc, "C", totaldisc, tbl.apassetno, tbl.apassetno + " | " + suppname + " | " + tbl.apassetmstnote, "Post", Session["UserID"].ToString(), servertime, totaldisc * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapassetmst " + tbl.apassetmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        //return View(ex.ToString());
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            tbl.apassetmststatus = "In Process";
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: apassetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapassetmst tbl = db.QL_trnapassetmst.Find(CompnyCode, id);
            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["QL_trnapassetdtl2"+ uid];
            var servertime = ClassFunction.GetServerTime();
            string apitemdtl2type = "QL_trnapassetmst";

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetdtloid IN (SELECT mrassetdtloid FROM QL_trnapassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmrassetmst SET mrassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnapassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND apassetmstoid=" + tbl.apassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.apdpamt > 0)
                        {
                            //Update Accum DPFM
                            sSql = "UPDATE QL_trndpap SET dpapaccumamt=0 WHERE pomstoid = " + tbl.poassetmstoid + " AND poreftype = 'QL_trnapassetmst' AND dpappaytype = 'DPFM' AND ISNULL(dpapres1,'')<>'Batal'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tbl.apassetmstoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tbl.cmpcode);
                        db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl = db.QL_trnapassetdtl.Where(a => a.apassetmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnapassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapassetmst.Remove(tbl);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    //update fa beli
                                    sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(SELECT COUNT(*) AS jml FROM QL_trnapitemdtl2 apd WHERE apd.fabelioid=QL_mstfabeli.fabelioid) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPAssetTrn.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + CompnyCode + "' AND apm.apassetmstoid IN (" + id + ")");


            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "apassetMaterialReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}