﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class TBReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string sWhere = "";

        public TBReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_conmat tbl)
        {
            sSql = "SELECT * FROM QL_m05GN g WHERE gngroup='GUDANG' ORDER BY g.gndesc";
            var DDLWH = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.mtrwhoid);
            ViewBag.DDLWH = DDLWH;
        }


        [HttpPost]
        public ActionResult GetReturData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, transm.transformmstoid [No. Draft], transm.transformno [No. Transform], CONVERT(varchar(10),transm.transformdate, 103) [Tanggal], transm.transformmstnote [Note] FROM QL_trntransformmst transm WHERE transm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND transm.transformmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += " ORDER BY transm.transformno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblTB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string returno, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode Barang], m.itemdesc [Nama Barang], g.gndesc [Satuan] FROM QL_mstitem m INNER JOIN QL_trntransformdtl2 td ON td.transformdtl2refoid=m.itemoid INNER JOIN QL_trntransformmst transm ON transm.transformmstoid=td.transformmstoid INNER JOIN QL_m05GN g ON g.gnoid=td.transformdtl2unitoid WHERE transm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND transm.transformmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (returno != "")
                {
                    string[] arr = returno.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_conmat tbl;
            tbl = new QL_conmat();
            if (tbl == null)
                return HttpNotFound();
            InitDDL(tbl);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string DDLTypeTransform, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string DDLWH, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var Dtl = "";
            var Join = "";
            var pcs1 = "";
            var pcs2 = "";
            var rptfile = "";
            var rptname = "";
            if (DDLType == "Detail")
            {
                if (reporttype == "XLS")
                    rptfile = "rptTBDtlXls.rpt";
                else
                {
                    rptfile = "rptTBDtlPdf.rpt";
                }
                rptname = "TB_DETAIL";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptTBSumXls.rpt";
                else
                {
                    rptfile = "rptTBSumPdf.rpt";
                }
                rptname = "TB_SUMMARY";
            }

            if (DDLTypeTransform != "ALL")
            {
                sWhere += " AND transm.transformtype='"+ DDLTypeTransform + "'";
            }
            if (StartPeriod != "" && EndPeriod != "")
            {
                sWhere += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sWhere += " AND transm.transformmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }            
            if (DDLType == "Detail")
            {
                if (DDLWH != null)
                {
                    if (DDLWH.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLWH.Count(); i++)
                        {
                            stsval += "'" + DDLWH[i] + "',";
                        }
                        sSql += " AND trd1.transformdtl1whoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhere += " AND [Code] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (DDLType == "Detail")
            {
                Dtl = " , [Detail Reference], [Seq],  [Mat. Type], [Code], [Description], [Qty], [PID], [serialnumber], [Detail Note], CAST([Value IDR] AS DECIMAL(18,4)) [Value IDR], CAST([Value USD] AS DECIMAL(18,4)) [Value USD], [Warehouse], [Total Qty], [Unit], [Cost], [Total Cost],[Stock Akhir], [QtydetailPallet], [Last Warehouse]";
                Join = "  LEFT JOIN (";
                //Detail Input
                Join += "SELECT trd1.cmpcode,trd1.transformmstoid, transformdtl1oid [DtlOID], 'Bahan Baku' [Detail Reference], transformdtl1seq [Seq], transformdtl1reftype [Mat. Type], itemcode AS [Code], itemdesc AS [Description], transformdtl1qty [Qty], ISNULL(refno,'') [PID], ISNULL(serialnumber,'') [serialnumber], trd1.transformdtl1note [Detail Note], CASE WHEN trd1.transformdtl1valueidr=0 THEN 0 ELSE (trd1.transformdtl1valueidr/trd1.transformdtl1qty) END [Value IDR], trd1.transformdtl1valueusd [Value USD], fwh.gndesc [Warehouse], transformtotalqtyin [Total Qty], uin.gndesc [Unit],0.0 [Cost], 0.0 [Total Cost]  ,ISNULL((SELECT SUM(qtyin) - SUM(qtyout) FROM QL_conmat where refoid = trd1.transformdtl1refoid and refno = trd1.refno and mtrwhoid = trd1.transformdtl1whoid),0.0) [Stock Akhir], 0.0 [QtydetailPallet]  ,(SELECT TOP 1 g.gndesc FROM QL_conmat con INNER JOIN QL_m05GN g ON g.gnoid = con.mtrwhoid WHERE con.refoid = trd1.transformdtl1refoid AND con.mtrwhoid = trd1.transformdtl1whoid ORDER by con.updtime desc) [Last Warehouse] FROM QL_trntransformdtl1 trd1 INNER JOIN QL_trntransformmst trx ON trd1.cmpcode=trx.cmpcode AND trd1.transformmstoid=trx.transformmstoid INNER JOIN QL_m05gn fwh ON fwh.gnoid=trd1.transformdtl1whoid AND fwh.gngroup='GUDANG' INNER JOIN QL_m05gn uin ON uin.gnoid=trd1.transformdtl1unitoid AND uin.gngroup= 'SATUAN' INNER JOIN QL_mstitem m ON m.itemoid = trd1.transformdtl1refoid";
                Join += " UNION ALL ";
                //detail output
                Join += " SELECT trd2.cmpcode,trd2.transformmstoid, transformdtl2oid [DtlOID], 'Barang Jadi' [Detail Reference], transformdtl2seq [Seq], transformdtl2reftype [Mat. Type], itemcode AS [Code], itemdesc AS [Description], transformdtl2qty [Qty], ISNULL(refno,'') [PID], ISNULL(serialnumber,'') [serialnumber], trd2.transformdtl2note [Detail Note], CASE WHEN trd2.transformdtl2valueidr = 0 THEN 0 ELSE (trd2.transformdtl2valueidr/trd2.transformdtl2qty) END [Value IDR], trd2.transformdtl2valueusd [Value USD], twh.gndesc [Warehouse], transformtotalqtyout [Total Qty], uout.gndesc [Unit], 0.0 [Cost], 0.0 [Total Cost] , ISNULL((SELECT SUM(qtyin) - SUM(qtyout) FROM QL_conmat where refoid = trd2.transformdtl2refoid /*and trd2.refno = c.refno*/ and mtrwhoid = trd2.transformdtl2whoid),0.0) [Stock Akhir] ,0.0 [QtydetailPallet] ,(SELECT TOP 1 g.gndesc FROM QL_conmat con INNER JOIN QL_m05GN g ON g.gnoid = con.mtrwhoid WHERE con.refoid = trd2.transformdtl2refoid AND con.mtrwhoid = trd2.transformdtl2whoid ORDER by con.updtime desc) [Last Warehouse] FROM QL_trntransformdtl2 trd2 INNER JOIN QL_trntransformmst trx ON trd2.cmpcode=trx.cmpcode AND trd2.transformmstoid=trx.transformmstoid INNER JOIN QL_m05gn twh ON twh.gnoid=trd2.transformdtl2whoid AND twh.gngroup='GUDANG'  INNER JOIN QL_m05gn uout ON uout.gnoid=trd2.transformdtl2unitoid AND uout.gngroup= 'SATUAN'  INNER JOIN QL_mstitem m ON m.itemoid = trd2.transformdtl2refoid";
                Join += " ) AS tbltmp ON tbltmp.cmpcode=transm.cmpcode AND tbltmp.transformmstoid=transm.transformmstoid";
            }
            else
            {
                pcs1 = "(SELECT COUNT(transformdtl1oid) FROM QL_trntransformdtl1 d1 WHERE d1.transformmstoid=transm.transformmstoid) [Total Pcs In], ";
                pcs2 = "(SELECT COUNT(transformdtl2oid) FROM QL_trntransformdtl2 d2 WHERE d2.transformmstoid=transm.transformmstoid) [Total Pcs Out], ";
            }

                sSql = " SELECT  transm.cmpcode [CMPCODE], 'CV. ANUGRAH PRATAMA' [BU],transm.transformmstoid [OID], transformno [Transform No.], transformdate [Transform Date], '' [Division], '' [Department], transformtype [Type], '' [From Warehouse], '' [To Warehouse], transformtotalqtyin [Total Qty In], transformtotalqtyout [Total Qty Out], " + pcs1 + pcs2 + " '' [Unit In], '' [Unit Out], '' as [Group Name], 0.0 [Cost In], 0.0 [Cost Out], 0.0 [Total Cost In], 0.0 [Total Cost Out], '' [Currency],transformmststatus [Status], transformmstnote [Note], transm.createtime [Create Date], transm.createuser [Create User], (CASE transformmststatus WHEN 'In Process' THEN '' ELSE transm.upduser END) AS [PostUser], (CASE transformmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE transm.updtime END) AS [PostDate], ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=transm.rabmstoid),'') [Project], ISNULL((SELECT rm.soitemno FROM QL_trnsoitemmst rm WHERE rm.soitemmstoid=transm.soitemmstoid),'') [SO No], ISNULL(STUFF((SELECT ', '+ft.poitemno FROM (SELECT DISTINCT pm.poitemno FROM QL_trntransformdtl1 td INNER JOIN QL_trnmritemdtl mrd ON td.transformdtl1refoid=mrd.itemoid AND td.transformdtl1unitoid=mrd.mritemunitoid AND td.transformdtl1qty= mrd.mritemqty AND ISNULL(td.refno,'')=ISNULL(mrd.refno,'') AND ISNULL(td.serialnumber,'')=ISNULL(mrd.serialnumber,'') INNER JOIN QL_trnpoitemdtl pd ON pd.poitemdtloid=mrd.podtloid AND mrd.itemoid=pd.itemoid INNER JOIN QL_trnpoitemmst pm ON pm.poitemmstoid=pd.poitemmstoid WHERE pm.rabmstoid=transm.rabmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [PO No] " + Dtl + " FROM QL_trntransformmst transm  " + Join + " ";

            sSql += " Where transm.cmpcode = '" + CompnyCode + "' " + sWhere + "";

            if (DDLType == "Detail")
            {
                sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}