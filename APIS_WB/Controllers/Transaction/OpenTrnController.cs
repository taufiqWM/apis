﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class OpenTrnController : Controller
    {
        // GET: OpenTrn
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public OpenTrnController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ReportFilter
        {
            public string startdate { get; set; }
            public string enddate { get; set; }
            public string refaction { get; set; }
            public string reftype { get; set; }
        }

        public class tbl_trn
        {
            public int seq { get; set; }
            public string reftype { get; set; }
            public string reftable { get; set; }
            public int refoid { get; set; }
            public string projectname { get; set; }
            public string refno { get; set; }
            public DateTime refdate { get; set; }
            public string refsuppcust { get; set; }
            public decimal amt { get; set; }
            public string refnote { get; set; }
            public string refaction { get; set; }
            //SALES
            public int rabmstoid { get; set; }
            public int paytermoid { get; set; }
            public string salesoid { get; set; }
            public string salesadminoid { get; set; }
            public string creatoroid { get; set; }
            public List<SelectListItem> dt_payterm { get; set; }
            public List<SelectListItem> dt_salesoid { get; set; }
            public List<SelectListItem> dt_salesadminoid { get; set; }
            public List<SelectListItem> dt_creatoroid { get; set; }
            //RAB
            public int poitemmstoid { get; set; }
            public string poitemno { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string rabdtl2flagrevisi { get; set; }
            public List<SelectListItem> dt_flag { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetail(ReportFilter param)
        {
            JsonResult js = null;
            try
            {
                if (param.reftype == "MJ")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnglmst' reftable, a.glmstoid refoid, '' projectname, a.glother1 refno, a.gldate refdate, '' refsuppcust, ISNULL((SELECT SUM(b.glamt) FROM QL_trngldtl b WHERE b.glmstoid=a.glmstoid AND b.gldbcr='D'),0.0) amt, glnote refnote FROM QL_trnglmst a WHERE a.glflag IN('Post','Approved','Closed') AND a.glother1 LIKE'MJ/%' AND a.gldate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.gldate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "KBM")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.custname FROM QL_mstcust b WHERE b.custoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND a.cashbankgroup LIKE'RECEIPT' AND LEFT(a.cashbanktype,2) IN('BB','BK') AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "KBK")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.suppname FROM QL_mstsupp b WHERE b.suppoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup LIKE'EXPENSE' AND LEFT(a.cashbanktype,2) IN('BB','BK') AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "KBT")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, '' refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup LIKE'MUTATION' AND LEFT(a.cashbanktype,2) IN('BB','BK') AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "GM")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.custname FROM QL_mstcust b WHERE b.custoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND a.cashbankgroup LIKE'GIRO IN' AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "GK")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.suppname FROM QL_mstsupp b WHERE b.suppoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND a.cashbanktype = 'BGK' AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "RB")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.custname FROM QL_mstcust b WHERE b.custoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup LIKE'RABREAL' AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "RM")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.custname FROM QL_mstcust b WHERE b.custoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup LIKE'RABMULTIREAL' AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "PAR")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.custname FROM QL_mstcust b WHERE b.custoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup LIKE'AR' AND LEFT(a.cashbanktype,2) IN('BB','BK') AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "PAP")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, ISNULL((SELECT b.suppname FROM QL_mstsupp b WHERE b.suppoid=a.refsuppoid),'') refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup LIKE'AP' AND LEFT(a.cashbanktype,2) IN('BB','BK') AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "PDP")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncashbankmst' reftable, a.cashbankoid refoid, ISNULL((SELECT b.projectname FROM QL_trnrabmst b WHERE b.rabmstoid=a.rabmstoid),'') projectname, a.cashbankno refno, a.cashbankdate refdate, (CASE cashbanktype WHEN 'BLK' THEN (SELECT s.suppname FROM QL_mstsupp s WHERE s.suppoid=a.refsuppoid) ELSE (SELECT s.custname FROM QL_mstcust s WHERE s.custoid=a.refsuppoid) END) refsuppcust, cashbankamt amt, cashbanknote refnote FROM QL_trncashbankmst a WHERE a.cashbankstatus IN('Post','Approved','Closed') AND  a.cashbankgroup IN('AP','AR') AND LEFT(a.cashbanktype,2) IN('BL') AND a.cashbankdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cashbankdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "PK")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnkasbonmst' reftable, a.kasbonmstoid refoid, '' projectname, a.kasbonno refno, a.kasbondate refdate, '' refsuppcust, kasbonamt amt, kasbonnote refnote FROM QL_trnkasbonmst a WHERE a.kasbonmststatus IN('Post','Approved','Closed') AND a.kasbondate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.kasbondate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "DPAR")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trndpar' reftable, a.dparoid refoid, '' projectname, a.dparno refno, a.dpardate refdate, ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=a.custoid),'') refsuppcust, dparamt amt, dparnote refnote FROM QL_trndpar a WHERE a.dparstatus IN('Post','Approved','Closed') AND a.dparoid>0 AND ISNULL(dparres1,'') <> 'Batal' AND a.dpardate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.dpardate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "DPAP")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trndpap' reftable, a.dpapoid refoid, '' projectname, a.dpapno refno, a.dpapdate refdate, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppoid),'') refsuppcust, dpapamt amt, dpapnote refnote FROM QL_trndpap a WHERE a.dpapstatus IN('Post','Approved','Closed') AND a.dpapoid>0 AND ISNULL(dpapres1,'') <> 'Batal' AND a.dpapdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.dpapdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "DPR")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trndprefund' reftable, a.dprefundoid refoid, '' projectname, a.dprefundno refno, a.dprefunddate refdate, CASE a.dpreftype WHEN 'QL_trndpar' THEN ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=a.suppcustoid),'') ELSE ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppcustoid),'') END refsuppcust, dprefundamt amt, dprefundnote refnote FROM QL_trndprefund a WHERE a.dprefundstatus IN('Post','Approved','Closed') AND a.dprefunddate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.dprefunddate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "DN")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trndebetnote' reftable, a.dnoid refoid, '' projectname, a.dnno refno, a.dndate refdate, CASE a.reftype WHEN 'QL_trnaritemmst' THEN ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=a.suppcustoid),'') ELSE ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppcustoid),'') END refsuppcust, dnamt amt, dnnote refnote FROM QL_trndebetnote a WHERE a.dnstatus IN('Post','Approved','Closed') AND a.dndate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.dndate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "CN")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trncreditnote' reftable, a.cnoid refoid, '' projectname, a.cnno refno, a.cndate refdate, CASE a.reftype WHEN 'QL_trnaritemmst' THEN ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=a.suppcustoid),'') ELSE ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppcustoid),'') END refsuppcust, cnamt amt, cnnote refnote FROM QL_trncreditnote a WHERE a.cnstatus IN('Post','Approved','Closed') AND a.cndate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.cndate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.reftype == "KBN")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnkasbon2mst' reftable, a.kasbon2mstoid refoid, '' projectname, a.kasbon2no refno, a.kasbon2date refdate, kasbon2group refsuppcust, kasbon2amt amt, kasbon2note refnote FROM QL_trnkasbon2mst a WHERE a.kasbon2mststatus IN('Post','Approved','Closed') AND a.kasbon2date>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.kasbon2date<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                var tbl = db.Database.SqlQuery<tbl_trn>(sSql).ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    var i = 1;
                    foreach (var item in tbl)
                        item.seq = i++;
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.startdate = ClassFunction.GetServerTime().ToString("01/MM/yyyy");
            ViewBag.enddate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(List<tbl_trn> tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            if (tbl == null)
                msg += "- Please fill detail data!<br />";
            else if (tbl.Count <= 0)
                msg += "- Please fill detail data!<br />";
            else
            {
                foreach (var m in tbl)
                {
                    if (m.reftype == "DPAR")
                    {
                        var id = m.refoid;
                        var tipe = db.QL_trndpar.FirstOrDefault(x => x.dparoid == id).dparpaytype;
                        if (tipe == "DPFK")
                        {
                            var somstoid = db.QL_trndpar.FirstOrDefault(x => x.dparoid == id && x.soreftype == "QL_trnsoitemmst").somstoid;
                            int cek = db.QL_trnaritemmst.Where(x => x.somstoid == somstoid).Count();
                            if (cek > 0)
                                msg += "- DP Sudah Ditarik di FJ!<br />";
                        }
                        else
                        {
                            int cek = db.QL_trnpaydp.Where(x => x.dpreftype == "QL_trndpar" && x.dprefoid == id).Count();
                            if (cek > 0)
                                msg += "- DP Sudah Ditarik di pelunasan DP!<br />";
                            cek = db.QL_trndprefund.Where(x => x.dpreftype == "QL_trndpar" && x.dpoid == id).Count();
                            if (cek > 0)
                                msg += "- DP Sudah Ditarik di DP Refund!<br />";
                        }
                    }
                    else if (m.reftype == "DPAP")
                    {
                        var id = m.refoid;
                        var tipe = db.QL_trndpap.FirstOrDefault(x => x.dpapoid == id).dpappaytype;
                        if (tipe == "DPFM")
                        {
                            var potable = db.QL_trndpap.FirstOrDefault(x => x.dpapoid == id).poreftype;
                            if (potable == "QL_trnpoitemmst")
                            {
                                var pomstoid = db.QL_trndpap.FirstOrDefault(x => x.dpapoid == id && x.poreftype == potable).pomstoid;
                                int cek = db.QL_trnapitemmst.Where(x => x.poitemmstoid == pomstoid).Count();
                                if (cek > 0)
                                    msg += "- DP Sudah Ditarik di FB!<br />";
                            }
                            else if (potable == "QL_trnpoassetmst")
                            {
                                var pomstoid = db.QL_trndpap.FirstOrDefault(x => x.dpapoid == id && x.poreftype == potable).pomstoid;
                                int cek = db.QL_trnapassetmst.Where(x => x.poassetmstoid == pomstoid).Count();
                                if (cek > 0)
                                    msg += "- DP Sudah Ditarik di FB!<br />";
                            }

                        }
                        else
                        {
                            int cek = db.QL_trnpaydp.Where(x => x.dpreftype == "QL_trndpap" && x.dprefoid == id).Count();
                            if (cek > 0)
                                msg += "- DP Sudah Ditarik di pelunasan DP!<br />";
                            cek = db.QL_trndprefund.Where(x => x.dpreftype == "QL_trndpap" && x.dpoid == id).Count();
                            if (cek > 0)
                                msg += "- DP Sudah Ditarik di DP Refund!<br />";
                        }
                    }
                    else if (m.reftype == "KBN")
                    {
                        var id = m.refoid;
                        if (m.refsuppcust == "KASBON")
                        {
                            int cek = db.QL_trnpayap.Where(x => x.reftype == "QL_trnkasbon2mst" && x.refoid == id).Count();
                            if (cek > 0)
                                msg += "- KASBON Sudah Ditarik di pelunasan!<br />";
                        }
                    }

                    //Cek date Closing 
                    System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                    DateTime cekClosingDate = m.refdate;//Tanggal Dokumen
                    if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                    {
                        ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    }
                }
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in tbl)
                        {
                            if (item.reftype == "MJ")
                            {
                                sSql = $"UPDATE QL_trnglmst SET glflag='In Process' WHERE glmstoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "KBM")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "KBK")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "KBT")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trnglmst WHERE glmstoid IN(SELECT DISTINCT glmstoid FROM QL_trngldtl WHERE glother1 IN(SELECT 'QL_trncashbankmst ' + CAST(cashbankoid AS VARCHAR(18)) FROM QL_trncashbankgl WHERE cashbankglres1 = '{item.refoid}'))";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trngldtl WHERE glother1 IN(SELECT 'QL_trncashbankmst ' + CAST(cashbankoid AS VARCHAR(18)) FROM QL_trncashbankgl WHERE cashbankglres1 = '{item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trncashbankmst WHERE cashbankoid IN(SELECT cashbankoid FROM QL_trncashbankgl WHERE cashbankglres1 = '{item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trncashbankgl WHERE cashbankglres1 = '{item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankgl SET cashbankglstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "GM")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trnpayargiro SET payargirostatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "GK")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trnglmst WHERE glmstoid=(SELECT DISTINCT glmstoid FROM QL_trngldtl WHERE glother1 = 'QL_trncashbankmst ' + CAST((SELECT DISTINCT cashbankoid FROM QL_trnpayapgiro WHERE refoid = {item.refoid}) AS VARCHAR(10)))";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trngldtl WHERE glother1='QL_trncashbankmst ' + CAST((SELECT DISTINCT cashbankoid FROM QL_trnpayapgiro WHERE refoid = {item.refoid}) AS VARCHAR(10))";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trncashbankmst WHERE cashbankoid=(SELECT DISTINCT cashbankoid FROM QL_trnpayapgiro WHERE refoid = {item.refoid})";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE QL_trnpayapgiro WHERE refoid = {item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "RB")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "RM")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "PAR")
                            {
                                var id = item.refoid;
                                var tbldtl = db.QL_trnpayar.Where(x => x.cashbankoid == id).ToList();
                                foreach (var d in tbldtl)
                                {
                                    sSql = $"DELETE FROM QL_conar WHERE reftype='{d.reftype}' AND refoid={d.refoid} AND trnartype='PAYAR' AND payrefoid={d.payaroid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "PAP")
                            {
                                var id = item.refoid;
                                var tbldtl = db.QL_trnpayap.Where(x => x.cashbankoid == id).ToList();
                                foreach (var d in tbldtl)
                                {
                                    sSql = $"DELETE FROM QL_conap WHERE reftype='{d.reftype}' AND refoid={d.refoid} AND trnaptype='PAYAP' AND payrefoid={d.payapoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "PDP")
                            {
                                var id = item.refoid;
                                var reftable = db.QL_trncashbankmst.Where(x => x.cashbankoid == id).FirstOrDefault().cashbankres2;
                                var refoid = db.QL_trncashbankmst.Where(x => x.cashbankoid == id).FirstOrDefault().giroacctgoid;
                                if (reftable == "QL_trnaritemmst")
                                {
                                    sSql = $"DELETE FROM QL_conar WHERE reftype='{reftable}' AND refoid={refoid} AND trnartype='PAYDP' AND payrefoid={id}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                else
                                {
                                    sSql = $"DELETE FROM QL_conap WHERE reftype='{reftable}' AND refoid={refoid} AND trnaptype='PAYDP' AND payrefoid={id}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                var tbldtl = db.QL_trnpaydp.Where(x => x.cashbankoid == id).ToList();
                                foreach (var d in tbldtl)
                                {
                                    if (d.dpreftype == "QL_trndpar")
                                    {
                                        sSql = $"UPDATE QL_trndpar SET dparaccumamt = (dparaccumamt - {d.paydpamt}) WHERE dparoid={d.dprefoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    else
                                    {
                                        sSql = $"UPDATE QL_trndpap SET dpapaccumamt = (dpapaccumamt - {d.paydpamt}) WHERE dpapoid={d.dprefoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                }
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncashbankmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "PK")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trnkasbonmst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trnkasbonmst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trnkasbonmst SET kasbonmststatus='In Process' WHERE kasbonmstoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "DPAR")
                            {
                                var id = item.refoid;
                                var cashbankoid = db.QL_trndpar.FirstOrDefault(x => x.dparoid == id).cashbankoid;
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trndpar {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trndpar {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={cashbankoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trndpar SET dparstatus='In Process' WHERE dparoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "DPAP")
                            {
                                var id = item.refoid;
                                var cashbankoid = db.QL_trndpap.FirstOrDefault(x => x.dpapoid == id).cashbankoid;
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trndpap {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trndpap {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={cashbankoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trndpap SET dpapstatus='In Process' WHERE dpapoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "DPR")
                            {
                                var id = item.refoid;
                                var cashbankoid = db.QL_trndprefund.FirstOrDefault(x => x.dprefundoid == id).cashbankoid;
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trndprefund {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trndprefund {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncashbankmst SET cashbankstatus='In Process' WHERE cashbankoid={cashbankoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trndprefund SET dprefundstatus='In Process' WHERE dprefundoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "DN")
                            {
                                var id = item.refoid;
                                var reftable = db.QL_trndebetnote.FirstOrDefault(x => x.dnoid == id).reftype;
                                var reftableid = db.QL_trndebetnote.FirstOrDefault(x => x.dnoid == id).refoid;
                                if (reftable == "QL_trnaritemmst")
                                {
                                    sSql = $"DELETE FROM QL_conar WHERE reftype='{reftable}' AND refoid={reftableid} AND trnartype='DNAR' AND payrefoid={item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    sSql = $"UPDATE QL_trnaritemmst SET aritemmststatus='Post' WHERE aritemmstoid={item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                else
                                {
                                    sSql = $"DELETE FROM QL_conap WHERE reftype='{reftable}' AND refoid={reftableid} AND trnaptype='DNAP' AND payrefoid={item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    if (reftable == "QL_trnapitemmst")
                                    {
                                        sSql = $"UPDATE QL_trnapitemmst SET apitemmststatus='Post' WHERE apitemmstoid={item.refoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    else if (reftable == "QL_trnapassetmst")
                                    {
                                        sSql = $"UPDATE QL_trnapassetmst SET apassetmststatus='Post' WHERE apassetmstoid={item.refoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                }
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trndebetnote {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trndebetnote {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trndebetnote SET dnstatus='In Process' WHERE dnoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "CN")
                            {
                                var id = item.refoid;
                                var reftable = db.QL_trncreditnote.FirstOrDefault(x => x.cnoid == id).reftype;
                                var reftableid = db.QL_trncreditnote.FirstOrDefault(x => x.cnoid == id).refoid;
                                if (reftable == "QL_trnaritemmst")
                                {
                                    sSql = $"DELETE FROM QL_conar WHERE reftype='{reftable}' AND refoid={reftableid} AND trnartype='CNAR' AND payrefoid={item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    sSql = $"UPDATE QL_trnaritemmst SET aritemmststatus='Post' WHERE aritemmstoid={item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                else
                                {
                                    sSql = $"DELETE FROM QL_conap WHERE reftype='{reftable}' AND refoid={reftableid} AND trnaptype='CNAP' AND payrefoid={item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    if (reftable == "QL_trnapitemmst")
                                    {
                                        sSql = $"UPDATE QL_trnapitemmst SET apitemmststatus='Post' WHERE apitemmstoid={item.refoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                    else if (reftable == "QL_trnapassetmst")
                                    {
                                        sSql = $"UPDATE QL_trnapassetmst SET apassetmststatus='Post' WHERE apassetmstoid={item.refoid}";
                                        db.Database.ExecuteSqlCommand(sSql);
                                    }
                                }
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trncreditnote {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trncreditnote {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trncreditnote SET cnstatus='In Process' WHERE cnoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                            else if (item.reftype == "KBN")
                            {
                                sSql = $"DELETE FROM QL_trnglmst WHERE glmstoid IN(SELECT distinct glmstoid FROM QL_trngldtl WHERE glother1='QL_trnkasbon2mst {item.refoid}')";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_trngldtl WHERE glother1='QL_trnkasbon2mst {item.refoid}'";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"DELETE FROM QL_conap WHERE reftype='QL_trnkasbon2mst' AND refoid={item.refoid} AND payrefoid=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trnkasbon2mst SET kasbon2mststatus='In Process' WHERE kasbon2mstoid={item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data Opened!<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public class listDDLsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
        }
        private List<SelectListItem> GetFlagRevisi()
        {
            var dt_ddl = new List<SelectListItem>();
            dt_ddl.Add(new SelectListItem { Value = "Belum", Text = $"Belum" });
            dt_ddl.Add(new SelectListItem { Value = "Terpenuhi", Text = $"Terpenuhi" });
            return dt_ddl;
        }

        private List<SelectListItem> GetGeneral(string tipe)
        {
            var dt_ddl = new List<SelectListItem>(); dt_ddl.Add(new SelectListItem { Value = "0", Text = $"-- Pilih --" });
            foreach (var x in db.QL_m05GN.Where(x => x.gnflag == "ACTIVE" && x.gngroup == tipe).Select(x => new { x.gnoid, x.gndesc }).ToList()) dt_ddl.Add(new SelectListItem { Value = x.gnoid.ToString(), Text = x.gndesc });
            return dt_ddl;
        }

        private List<SelectListItem> GetSales()
        {
            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesoid = db.Database.SqlQuery<listDDLsales>(sSql).ToList();
            var dt_ddl = new List<SelectListItem>(); dt_ddl.Add(new SelectListItem { Value = "0", Text = $"-- Pilih --" });
            foreach (var x in salesoid) dt_ddl.Add(new SelectListItem { Value = x.salesoid.ToString(), Text = x.salesname });
            return dt_ddl;
        }

        [HttpPost]
        public ActionResult GetDataDetailUpdate(ReportFilter param)
        {
            JsonResult js = null;
            try
            {
                if (param.refaction == "UTD")
                {
                    if (param.reftype == "MR" || param.reftype == "MRJ")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnmritemmst' reftable, '{param.refaction}' refaction, a.mritemmstoid refoid, ISNULL((SELECT x.projectname FROM QL_trnrabmst x WHERE x.rabmstoid=a.rabmstoid),'') projectname, a.mritemno refno, a.mritemdate refdate, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppoid),'') refsuppcust, 0.0 amt, mritemmstnote refnote FROM QL_trnmritemmst a WHERE a.mritemmststatus IN('Post','Approved','Closed') AND a.mritemdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.mritemdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "MRA")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnmrassetmst' reftable, '{param.refaction}' refaction, a.mrassetmstoid refoid, '' projectname, a.mrassetno refno, a.mrassetdate refdate, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppoid),'') refsuppcust, 0.0 amt, mrassetmstnote refnote FROM QL_trnmrassetmst a WHERE a.mrassetmststatus IN('Post','Approved','Closed') AND a.mrassetdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.mrassetdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "FB" || param.reftype == "FBJ")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnapitemmst' reftable, '{param.refaction}' refaction, a.apitemmstoid refoid, ISNULL((SELECT x.projectname FROM QL_trnrabmst x WHERE x.rabmstoid=a.rabmstoid),'') projectname, a.apitemno refno, a.apitemdate refdate, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppoid),'') refsuppcust, apitemgrandtotal amt, apitemmstnote refnote FROM QL_trnapitemmst a WHERE a.apitemmststatus IN('Post','Approved','Closed') AND a.apitemdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.apitemdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "FBA")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnapassetmst' reftable, '{param.refaction}' refaction, a.apassetmstoid refoid, '' projectname, a.apassetno refno, a.apassetdate refdate, ISNULL((SELECT x.suppname FROM QL_mstsupp x WHERE x.suppoid=a.suppoid),'') refsuppcust, 0.0 amt, apassetmstnote refnote FROM QL_trnapassetmst a WHERE a.apassetmststatus IN('Post','Approved','Closed') AND a.apassetdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.apassetdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "SJ")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnshipmentitemmst' reftable, '{param.refaction}' refaction, a.shipmentitemmstoid refoid, ISNULL((SELECT x.projectname FROM QL_trnrabmst x WHERE x.rabmstoid=a.rabmstoid),'') projectname, a.shipmentitemno refno, a.shipmentitemdate refdate, ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=a.custoid),'') refsuppcust, 0.0 amt, shipmentitemmstnote refnote FROM QL_trnshipmentitemmst a WHERE a.shipmentitemmststatus IN('Post','Approved','Closed') AND a.shipmentitemdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.shipmentitemdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "FJ")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnaritemmst' reftable, '{param.refaction}' refaction, a.aritemmstoid refoid, ISNULL((SELECT x.projectname FROM QL_trnrabmst x WHERE x.rabmstoid=a.rabmstoid),'') projectname, a.aritemno refno, a.aritemdate refdate, ISNULL((SELECT x.custname FROM QL_mstcust x WHERE x.custoid=a.custoid),'') refsuppcust, aritemgrandtotal amt, aritemmstnote refnote FROM QL_trnaritemmst a WHERE a.aritemmststatus IN('Post','Approved','Closed') AND a.aritemdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.aritemdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "TRF")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trntransformmst' reftable, '{param.refaction}' refaction, a.transformmstoid refoid, ISNULL((SELECT x.projectname FROM QL_trnrabmst x WHERE x.rabmstoid=a.rabmstoid),'') projectname, a.transformno refno, a.transformdate refdate, '' refsuppcust, 0.0 amt, transformmstnote refnote FROM QL_trntransformmst a WHERE a.transformmststatus IN('Post','Approved','Closed') AND a.transformdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.transformdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                    else if (param.reftype == "PMB")
                    {
                        sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnmatusagemst' reftable, '{param.refaction}' refaction, a.matusagemstoid refoid, '' projectname, a.matusageno refno, a.matusagedate refdate, '' refsuppcust, 0.0 amt, matusagemstnote refnote FROM QL_trnmatusagemst a WHERE a.matusagemststatus IN('Post','Approved','Closed') AND a.matusagedate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.matusagedate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                    }
                }
                else if (param.refaction == "SALES")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnmatusagemst' reftable, '{param.refaction}' refaction, a.soitemmstoid refoid, projectname, a.soitemno refno, a.soitemdate refdate, custname refsuppcust, soitemgrandtotalamt amt, soitemmstnote refnote, a.rabmstoid, a.soitempaytypeoid paytermoid, '[]' dt_payterm, a.salesoid, '[]' dt_salesoid, a.salesadminoid, '[]' dt_salesadminoid, b.creatoroid, '[]' dt_creatoroid FROM QL_trnsoitemmst a INNER JOIN QL_trnrabmst b ON b.rabmstoid=a.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=a.custoid WHERE a.soitemmststatus IN('Post','Approved','Closed') AND a.soitemdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND a.soitemdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.refaction == "RAB")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnmatusagemst' reftable, '{param.refaction}' refaction, rd.rabdtl2oid refoid, projectname, rm.rabno refno, rm.rabdate refdate, suppname refsuppcust, itemcode, itemdesc, rd.rabdtl2netto amt, rd.rabdtl2flagrevisi, '[]' dt_flag, ISNULL((SELECT TOP 1 x.poitemmstoid FROM QL_trnpoitemmst x WHERE x.rabmstoid=rm.rabmstoid AND x.suppoid=rd.suppdtl2oid AND x.poitemmststatus IN('Post','Approved','Closed') AND ISNULL(x.poitemmstres1,'')='' AND x.poitemtype<>'Jasa' ORDER BY x.poitemmstoid DESC),'') poitemmstoid, ISNULL((SELECT TOP 1 x.poitemno FROM QL_trnpoitemmst x WHERE x.rabmstoid=rm.rabmstoid AND x.suppoid=rd.suppdtl2oid AND x.poitemmststatus IN('Post','Approved','Closed') AND ISNULL(x.poitemmstres1,'')='' AND x.poitemtype<>'Jasa' ORDER BY x.poitemmstoid DESC),'') poitemno, rabmstnote refnote FROM QL_trnrabmst rm INNER JOIN QL_trnrabdtl2 rd ON rm.rabmstoid=rd.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = rd.suppdtl2oid INNER JOIN QL_mstitem i ON i.itemoid=rd.itemdtl2oid WHERE rm.rabmststatus IN('Closed') AND ISNULL(rabmstres1,'')='Force Closed' AND rm.rabdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND rm.rabdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                else if (param.refaction == "RABJASA")
                {
                    sSql = $"SELECT 0 seq, '{param.reftype}' reftype, 'QL_trnmatusagemst' reftable, '{param.refaction}' refaction, rd.rabdtl4oid refoid, projectname, rm.rabno refno, rm.rabdate refdate, suppname refsuppcust, itemcode, itemdesc, rd.rabdtl4netto amt, rd.rabdtl4flagrevisi rabdtl2flagrevisi, '[]' dt_flag, ISNULL((SELECT TOP 1 x.poitemmstoid FROM QL_trnpoitemmst x WHERE x.rabmstoid=rm.rabmstoid AND x.suppoid=rd.suppdtl4oid AND x.poitemmststatus IN('Post','Approved','Closed') AND ISNULL(x.poitemmstres1,'')='' AND x.poitemtype='Jasa' ORDER BY x.poitemmstoid DESC),'') poitemmstoid, ISNULL((SELECT TOP 1 x.poitemno FROM QL_trnpoitemmst x WHERE x.rabmstoid=rm.rabmstoid AND x.suppoid=rd.suppdtl4oid AND x.poitemmststatus IN('Post','Approved','Closed') AND ISNULL(x.poitemmstres1,'')='' AND x.poitemtype='Jasa' ORDER BY x.poitemmstoid DESC),'') poitemno, rabmstnote refnote FROM QL_trnrabmst rm INNER JOIN QL_trnrabdtl4 rd ON rm.rabmstoid=rd.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = rd.suppdtl4oid INNER JOIN QL_mstitem i ON i.itemoid=rd.itemdtl4oid WHERE rm.rabmststatus IN('Closed') AND ISNULL(rabmstres1,'')='Force Closed' AND rm.rabdate>='{ClassFunction.toDate(param.startdate)} 00:00:00' AND rm.rabdate<='{ClassFunction.toDate(param.enddate)} 23:00:00'";
                }
                var tbl = db.Database.SqlQuery<tbl_trn>(sSql).ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    var i = 1;
                    foreach (var item in tbl)
                    {
                        item.seq = i++;
                        if (param.refaction == "SALES")
                        {
                            item.dt_payterm = GetGeneral("PAYMENT TERM");
                            item.dt_salesoid = GetSales();
                            item.dt_salesadminoid = GetSales();
                            item.dt_creatoroid = GetSales();
                        }
                        if (param.refaction == "RAB" || param.refaction == "RABJASA")
                        {
                            item.dt_flag = GetFlagRevisi();
                        }
                    }
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult Update()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.startdate = ClassFunction.GetServerTime().ToString("01/MM/yyyy");
            ViewBag.enddate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(List<tbl_trn> tbl, ReportFilter param, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            if (tbl == null)
                msg += "- Please fill detail data!<br />";
            else if (tbl.Count <= 0)
                msg += "- Please fill detail data!<br />";
            else
            {
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                foreach (var m in tbl)
                {
                    DateTime cekClosingDate = m.refdate;//Tanggal Dokumen
                    if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                    {
                        ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    }
                }
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (param.refaction == "UTD")
                        {
                            if (param.reftype == "MR" || param.reftype == "MRJ")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnmritemmst SET mritemdate='{item.refdate}' WHERE mritemmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "MRA")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnmrassetmst SET mrassetdate='{item.refdate}' WHERE mrassetmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "FB" || param.reftype == "FBJ")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnapitemmst SET apitemdate='{item.refdate}' WHERE apitemmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "FBA")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnapassetmst SET apassetdate='{item.refdate}' WHERE apassetmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "SJ")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnshipmentitemmst SET shipmentitemdate='{item.refdate}' WHERE shipmentitemmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "FJ")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnaritemmst SET aritemdate='{item.refdate}' WHERE aritemmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "TRF")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trntransformmst SET transformdate='{item.refdate}' WHERE transformmstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            else if (param.reftype == "PMB")
                            {
                                foreach (var item in tbl)
                                {
                                    sSql = $"UPDATE QL_trnmatusagemst SET matusagedate='{item.refdate}' WHERE matusagemstoid = {item.refoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                        }
                        else if (param.refaction == "SALES")
                        {
                            foreach (var item in tbl)
                            {
                                sSql = $"UPDATE QL_trnsoitemmst SET soitempaytypeoid={item.paytermoid}, salesoid='{item.salesoid}', salesadminoid='{item.salesadminoid}', soitemmstnote='{item.refnote}' WHERE soitemmstoid = {item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                                sSql = $"UPDATE QL_trnrabmst SET rabpaytermoid={item.paytermoid}, salesoid='{item.salesoid}', salesadminoid='{item.salesadminoid}', creatoroid='{item.creatoroid}', rabmstnote='{item.refnote}' WHERE rabmstoid = {item.rabmstoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            } 
                        }
                        else if (param.refaction == "RAB")
                        {
                            foreach (var item in tbl)
                            {
                                if (item.poitemmstoid > 0 && item.rabdtl2flagrevisi == "Belum")
                                {
                                    sSql = $"UPDATE QL_trnpoitemmst SET poitemmststatus='Closed', poitemmstres1='Force Closed' WHERE poitemmstoid = {item.poitemmstoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                sSql = $"UPDATE QL_trnrabdtl2 SET rabdtl2flagrevisi='{item.rabdtl2flagrevisi}' WHERE rabdtl2oid = {item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                        }
                        else if (param.refaction == "RABJASA")
                        {
                            foreach (var item in tbl)
                            {
                                if (item.poitemmstoid > 0 && item.rabdtl2flagrevisi == "Belum")
                                {
                                    sSql = $"UPDATE QL_trnpoitemmst SET poitemmststatus='Closed', poitemmstres1='Force Closed' WHERE poitemmstoid = {item.poitemmstoid}";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                                sSql = $"UPDATE QL_trnrabdtl4 SET rabdtl4flagrevisi='{item.rabdtl2flagrevisi}' WHERE rabdtl4oid = {item.refoid}";
                                db.Database.ExecuteSqlCommand(sSql);
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data Updated!<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}