﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Transaction
{
    public class ASvcController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public ASvcController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class backservicemst
        {
            public string cmpcode { get; set; }
            public int backservicemstoid { get; set; }
            public string backserviceno { get; set; }
            public DateTime backservicedate { get; set; }
            public string mrserviceno { get; set; }
            public string itemdesc { get; set; }   
            public string custname { get; set; }
            public string backservicemststatus { get; set; }
            public string backservicemstnote { get; set; }           
        }        

        public class cust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custphone { get; set; }
        }

        public class listmat
        {          
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string refno { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }           
        }

        public class listmrservice
        {
            public int mrservicemstoid { get; set; }
            public string mrserviceno { get; set; }
            public DateTime mrservicedate { get; set; }
            public string mrservicedatestr { get; set; }
            public int itemoid { get; set; }
            public string itemdesc { get; set; }
            public string custname { get; set; }
            public int custoid { get; set; }
            public string mrservicereq { get; set; }
        }

        public class listteknisi
        {
            public string teknisioid { get; set; }
            public string teknisiname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        private void InitDDL(QL_trnbackservicemst tbl)
        {
           
        }

        [HttpPost]
        public ActionResult GetTeknisiData()
        {
            List<listteknisi> tbl = new List<listteknisi>();

            sSql = "SELECT u.usoid teknisioid, u.usname teknisiname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='TEKNISI' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listteknisi>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataService()
        {
            List<listmrservice> tbl = new List<listmrservice>();

            sSql = "SELECT mrservicemstoid, mrserviceno, mrservicedate,CONVERT(char(20),mrservicedate,103) mrservicedatestr, i.itemdesc, i.itemoid, c.custoid,c.custname, mrservicereq FROM QL_trnmrservicemst sm INNER JOIN QL_mstitem i ON i.itemoid = sm.itemoid INNER JOIN QL_mstcust c ON c.custoid = sm.custoid where sm.cmpcode = '"+ CompnyCode + "' AND sm.mrservicemstoid NOT IN( SELECT mrservicemstoid FROM QL_trnbackservicemst) ORDER BY mrserviceno";
            tbl = db.Database.SqlQuery<listmrservice>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, i.itemoid, itemcode, '' refno, i.itemdesc, itemunitoid, g.gndesc AS itemunit FROM QL_mstitem i INNER JOIN QL_trnmrservicemst sm ON sm.itemoid = i.itemoid INNER JOIN QL_m05gn g ON gnoid=itemunitoid WHERE i.cmpcode='"+ CompnyCode +"' AND activeflag = 'ACTIVE' AND sm.mrservicemststatus = 'POST' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
      
        [HttpPost]
        public ActionResult GetCustData()
        {
            List<cust> tbl = new List<cust>();
            sSql = "SELECT custoid, custname, custcode, custaddr, custphone1 custphone FROM QL_mstcust where activeflag = 'ACTIVE' ORDER BY custcode, custname";

            tbl = db.Database.SqlQuery<cust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
      

        private void FillAdditionalField(QL_trnbackservicemst tbl)
        {
            ViewBag.mrserviceno = db.Database.SqlQuery<string>("SELECT mrserviceno FROM QL_trnmrservicemst WHERE cmpcode='" + CompnyCode + "' AND mrservicemstoid='" + tbl.mrservicemstoid + "'").FirstOrDefault();

            ViewBag.mrservicereq = db.Database.SqlQuery<string>("SELECT mrservicereq FROM QL_trnmrservicemst WHERE cmpcode='" + CompnyCode + "' AND mrservicemstoid='" + tbl.mrservicemstoid + "'").FirstOrDefault();

            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid='" + tbl.custoid + "'").FirstOrDefault();

            ViewBag.itemdesc = db.Database.SqlQuery<string>("SELECT itemdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.itemoid + "'").FirstOrDefault();

            ViewBag.teknisiname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tbl.teknisioid + "'").FirstOrDefault();
        }

        // GET: backserviceMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT som.cmpcode, som.backservicemstoid, som.backserviceno, som.backservicedate, som.itemoid, (SELECT itemdesc FROM QL_mstitem where itemoid = som.itemoid) itemdesc, c.custname, som.backservicemststatus, som.backservicemstnote, (SELECT mrserviceno FROM QL_trnmrservicemst where mrservicemstoid = som.mrservicemstoid) mrserviceno FROM QL_TRNbackserviceMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode = '" + CompnyCode + "'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND som.backservicedate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND som.backservicedate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND som.backservicemststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY som.backservicedate";

            List<backservicemst> dt = db.Database.SqlQuery<backservicemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: backserviceMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnbackservicemst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnbackservicemst();
                tbl.cmpcode = CompnyCode;
                tbl.backservicedate = ClassFunction.GetServerTime();               
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.backservicemststatus = "In Process";
                
                Session["QL_trnbackservicedtl"] = null;
            }
            else
            {
                tbl = db.QL_trnbackservicemst.Find(CompnyCode, id);
                action = "Edit";
            }
            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: backserviceMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnbackservicemst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.backservicedate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("backservicedate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.backservicemststatus == "Post")
            {
                tbl.backserviceno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.backserviceno = "";
            }
            if (string.IsNullOrEmpty(tbl.backserviceres1))
                tbl.backserviceres1 = "";
            if (string.IsNullOrEmpty(tbl.backserviceres2))
                tbl.backserviceres2 = "";
            if (string.IsNullOrEmpty(tbl.backserviceres3))
                tbl.backserviceres3 = "";
            if (string.IsNullOrEmpty(tbl.backservicereq))
                tbl.backservicereq = "";
            if (string.IsNullOrEmpty(tbl.backserviceresult))
                tbl.backserviceresult = "";
            if (string.IsNullOrEmpty(tbl.backservicemstnote))
                tbl.backservicemstnote = "";

            if (tbl.backservicereq == "")
            {
                ModelState.AddModelError("backservicereq", "Silahkan Isi Kelengkapan!!");
            }
            if (tbl.backserviceresult == "")
            {
                ModelState.AddModelError("backserviceresult", "Silahkan Isi Hasil Pengecekan!!");
            }

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.backservicedate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.backservicemststatus = "In Process";
            }
            if (tbl.backservicemststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.backservicemststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnbackservicemst");
                var dtloid = ClassFunction.GenerateID("QL_trnbackservicedtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var servertime = ClassFunction.GetServerTime();               
                          

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                     

                        if (action == "Create")
                        {
                            
                            tbl.backservicemstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());                            
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnbackservicemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.backservicemstoid + " WHERE tablename='QL_trnbackservicemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();                           
                        }                             

                        objTrans.Commit();                       
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.backservicemststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.backservicemststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        
        }
        

        // POST: backserviceMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnbackservicemst tbl = db.QL_trnbackservicemst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                       

                        db.QL_trnbackservicemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "ASVC/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(backserviceno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnbackservicemst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND backserviceno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}