﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Transaction
{
    public class FJUpdateController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public FJUpdateController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listarupdmst
        {
            public int arupdmstoid { get; set; }
            public string aritemno { get; set; }
            public DateTime aritemdate { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string projectname { get; set; }
        }

        public class listarupddtl
        {
            public int arupddtlseq { get; set; }
            public string arupddtldate { get; set; }
            public string arupddtlnote { get; set; }
        }

        public class listaritemmst
        {
            public int aritemmstoid { get; set; }
            public string aritemno { get; set; }
            public string aritemdate { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public string soitemdate { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "DEPT/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(groupcode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnarupdmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND groupcode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listaritemmst> tbl = new List<listaritemmst>();

            sSql = "SELECT arm.aritemmstoid, arm.aritemno, CONVERT(CHAR(10),arm.aritemdate,103) aritemdate, som.soitemmstoid, som.soitemno, CONVERT(CHAR(10),som.soitemdate,103) soitemdate, rm.rabmstoid, rm.projectname, c.custname FROM QL_trnaritemmst arm INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE arm.aritemmststatus IN('Post','Closed') AND arm.aritemmstoid NOT IN(SELECT arum.aritemmstoid FROM QL_trnarupdmst arum) ORDER BY arm.aritemno";
            tbl = db.Database.SqlQuery<listaritemmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            List<listarupddtl> tbl = new List<listarupddtl>();

            sSql = "SELECT /*CAST(ROW_NUMBER() OVER(ORDER BY deptcode) AS INT)*/ 0 AS groupdtlseq, d.deptoid, d.deptcode, d.deptname FROM QL_mstdept d WHERE d.activeflag='ACTIVE' AND d.deptoid NOT IN(SELECT dgd.deptoid FROM QL_trnarupddtl dgd) ORDER BY deptname";
            tbl = db.Database.SqlQuery<listarupddtl>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
            //return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listarupddtl> dtDtl)
        {
            Session["QL_trnarupddtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnarupddtl"] == null)
            {
                Session["QL_trnarupddtl"] = new List<listarupddtl>();
            }

            List<listarupddtl> dataDtl = (List<listarupddtl>)Session["QL_trnarupddtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: Divisi
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "SELECT arum.arupdmstoid, arm.aritemno, arm.aritemdate, som.soitemno, som.soitemdate, rm.projectname FROM QL_trnarupdmst arum INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid=arum.aritemmstoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arum.soitemmstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arum.rabmstoid WHERE arum.cmpcode='" + CompnyCode + "' " + sfilter + " ORDER BY arm.aritemno ";
            List<listarupdmst> tblmst = db.Database.SqlQuery<listarupdmst>(sSql).ToList();
            return View(tblmst);
        }


        // GET: Divisi/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarupdmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnarupdmst();
                tblmst.cmpcode = CompnyCode;
                tblmst.arupdmstoid = ClassFunction.GenerateID("QL_trnarupdmst");
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.aritemmstoid = 0;
                tblmst.soitemmstoid = 0;
                tblmst.rabmstoid = 0;

                Session["QL_trnarupddtl"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnarupdmst.Find(CompnyCode, id);

                sSql = "SELECT arud.arupddtlseq, CONVERT(CHAR(10),arud.arupddtldate,103) arupddtldate, arud.arupddtlnote FROM QL_trnarupddtl arud WHERE arud.cmpcode='" + CompnyCode + "' AND arud.arupdmstoid=" + id + " ORDER BY arud.arupddtlseq";
                Session["QL_trnarupddtl"] = db.Database.SqlQuery<listarupddtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnarupdmst tblmst, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (tblmst.aritemmstoid == 0)
                ModelState.AddModelError("", "Select FJ No.!");

            List<listarupddtl> dtDtl = (List<listarupddtl>)Session["QL_trnarupddtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_trnarupdmst");
            var dtloid = ClassFunction.GenerateID("QL_trnarupddtl");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.arupdmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnarupdmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnarupdmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnarupddtl.Where(a => a.arupdmstoid == tblmst.arupdmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnarupddtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnarupddtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnarupddtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.arupddtloid = dtloid++;
                            tbldtl.arupdmstoid = tblmst.arupdmstoid;
                            tbldtl.arupddtlseq = i + 1;
                            tbldtl.arupddtldate = DateTime.Parse(ClassFunction.toDate(dtDtl[i].arupddtldate));
                            tbldtl.arupddtlnote = dtDtl[i].arupddtlnote;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            db.QL_trnarupddtl.Add(tbldtl);
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnarupddtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_trnarupdmst tblmst)
        {
            ViewBag.aritemno = db.Database.SqlQuery<string>("SELECT aritemno FROM QL_trnaritemmst r WHERE r.aritemmstoid ='" + tblmst.aritemmstoid + "'").FirstOrDefault();
            ViewBag.aritemdate = db.Database.SqlQuery<string>("SELECT CONVERT(CHAR(10),aritemdate,103) tgl FROM QL_trnaritemmst r WHERE r.aritemmstoid ='" + tblmst.aritemmstoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("select c.custname from QL_trnrabmst rb INNER JOIN QL_mstcust c ON c.custoid=rb.custoid where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("select projectname from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.soitemno = db.Database.SqlQuery<string>("select soitemno from QL_trnsoitemmst rb where soitemmstoid = " + tblmst.soitemmstoid + "").FirstOrDefault();
            ViewBag.soitemdate = db.Database.SqlQuery<string>("SELECT CONVERT(CHAR(10),soitemdate,103) tgl FROM QL_trnsoitemmst r WHERE r.soitemmstoid ='" + tblmst.soitemmstoid + "'").FirstOrDefault();
        }

        // POST: Departemen/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarupdmst tblmst = db.QL_trnarupdmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnarupddtl.Where(a => a.arupdmstoid == id);
                        db.QL_trnarupddtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnarupdmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}