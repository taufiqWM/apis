﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class SJJasaReturnController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public SJJasaReturnController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class sretitemmst
        {
            public string cmpcode { get; set; }
            public int sretitemmstoid { get; set; }
            public string sretitemno { get; set; }
            public DateTime sretitemdate { get; set; }
            public string shipmentitemno { get; set; }
            public string custname { get; set; }
            public string sretitemmststatus { get; set; }
            public string sretitemmstnote { get; set; }
        }

        public class sretitemdtl
        {
            public int sretitemdtlseq { get; set; }
            public int shipmentitemdtloid { get; set; }
            public int sretitemwhoid { get; set; }
            public string sretitemwh { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int sretitemunitoid { get; set; }
            public string sretitemunit { get; set; }
            public decimal shipmentitemqty { get; set; }
            public decimal sretitemqty { get; set; }
            public decimal sretitemamt { get; set; }
            public string sretitemdtlnote { get; set; }
            public string sretitemdtlres2 { get; set; }
        }

        public class shipmentitem
        {
            public int shipmentitemmstoid { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }
            public string shipmentitemdatestr { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        public class cust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custphone { get; set; }
        }

        public class warehouse
        {
            public int sretitemwhoid { get; set; }
            public string sretitemwh { get; set; }
        }

        public class trnshdtl
        {
            public decimal shipmentqty { get; set; }
            public decimal mrvalue { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        public class listnopol
        {
            public string nopol { get; set; }
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + itemoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listnopol> tbl = new List<listnopol>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listnopol>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnsretitemmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var sretitemwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.sretitemwhoid = sretitemwhoid;
        }

        [HttpPost]
        public ActionResult GetDetailData(int shipmentitemmstoid, int sretitemmstoid)
        {
            List<sretitemdtl> tbl = new List<sretitemdtl>();
            sSql = "SELECT DISTINCT shipmentitemdtlseq, shipmentitemdtloid, shd.itemoid, jasacode itemcode, jasadesc itemdesc, ISNULL(refno,'') refno, ISNULL(serialnumber,'') serialnumber, shipmentitemunitoid sretitemunitoid, g.gndesc AS sretitemunit, ISNULL(shipmentitemwhoid,0) sretitemwhoid, (shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=shd.cmpcode AND sretd.shipmentitemdtloid=shd.shipmentitemdtloid AND sretitemmststatus<>'Rejected' AND sretd.sretitemmstoid<>" + sretitemmstoid + "), 0)) AS shipmentitemqty, 0.0 AS sretitemqty, '' AS sretitemdtlnote, ISNULL(shipmentitemdtlres2,'') sretitemdtlres2 FROM QL_trnshipmentitemdtl shd INNER JOIN QL_mstjasa m ON m.jasaoid=shd.itemoid INNER JOIN QL_m05GN g ON gnoid=shipmentitemunitoid LEFT JOIN QL_m05GN g2 ON g2.gnoid=shipmentitemwhoid WHERE shd.cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + shipmentitemmstoid + " AND ISNULL(shipmentitemdtlres1, '')<>'Complete' ORDER BY shipmentitemdtlseq";

            tbl = db.Database.SqlQuery<sretitemdtl>(sSql).ToList();
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].sretitemqty = tbl[i].shipmentitemqty;
                    }
                }
            }

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSJData(int custoid)
        {

            List<shipmentitem> tbl = new List<shipmentitem>();
            sSql = "SELECT shipmentitemmstoid, shipmentitemno, shipmentitemdate, (SELECT soitemno FROM QL_trnsoitemmst where soitemmstoid = sm.soitemmstoid) soitemno, rm.rabmstoid, rm.projectname, de.groupdesc departemen FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=ISNULL(sm.rabmstoid,0) INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid where shipmentitemmststatus = 'Post' and sm.custoid = " + custoid + " AND ISNULL(shipmentitemtype, '')='Jasa' AND ISNULL(shipmentitemmstres1, '')<>'Closed' AND shipmentitemmstoid NOT IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl ard INNER JOIN QL_trnaritemmst arm ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid WHERE ard.cmpcode='" + CompnyCode + "' AND aritemmststatus<>'Rejected') ORDER BY shipmentitemmstoid";

            tbl = db.Database.SqlQuery<shipmentitem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustData()
        {
            List<cust> tbl = new List<cust>();
            sSql = "SELECT DISTINCT custoid, custname, custcode, custaddr, custphone1 custphone FROM QL_mstcust where activeflag = 'ACTIVE' and custoid IN (SELECT custoid FROM QL_trnshipmentitemmst where shipmentitemmststatus = 'POST' AND ISNULL(shipmentitemtype, '')='Jasa' AND ISNULL(shipmentitemmstres1, '')<>'Closed' AND shipmentitemmstoid NOT IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl ard INNER JOIN QL_trnaritemmst arm ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid WHERE ard.cmpcode='APIS' AND aritemmststatus<>'Rejected'))  ORDER BY custcode, custname";

            tbl = db.Database.SqlQuery<cust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<sretitemdtl> dtDtl)
        {
            Session["QL_trnsretitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsretitemdtl"] == null)
            {
                Session["QL_trnsretitemdtl"] = new List<sretitemdtl>();
            }

            List<sretitemdtl> dataDtl = (List<sretitemdtl>)Session["QL_trnsretitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnsretitemmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();

            ViewBag.shipmentitemno = db.Database.SqlQuery<string>("SELECT shipmentitemno FROM QL_trnshipmentitemmst WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid='" + tbl.shipmentitemmstoid + "'").FirstOrDefault();

            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
        }

        // GET: sretitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT som.cmpcode, som.sretitemmstoid, som.sretitemno, som.sretitemdate, som.shipmentitemmstoid, (SELECT shipmentitemno FROM QL_trnshipmentitemmst where shipmentitemmstoid = som.shipmentitemmstoid) shipmentitemno, c.custname, som.sretitemmststatus, som.sretitemmstnote FROM QL_TRNsretitemMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode = '" + CompnyCode + "' AND ISNULL(som.sretitemmstres1,'')='Jasa' ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND som.sretitemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND som.sretitemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND som.sretitemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY som.sretitemdate";

            List<sretitemmst> dt = db.Database.SqlQuery<sretitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: sretitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsretitemmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnsretitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.sretitemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.sretitemmststatus = "In Process";
                tbl.rabmstoid = 0;

                Session["QL_trnsretitemdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnsretitemmst.Find(CompnyCode, id);
                sSql = "select shipmentitemdtloid, sretitemdtlseq, i.jasaoid itemoid, i.jasacode itemcode, ISNULL(refno,'') refno, ISNULL(serialnumber,'') serialnumber, jasadesc itemdesc, sretitemwhoid, gn2.gndesc sretitemwh, (SELECT shipmentitemqty FROM QL_trnshipmentitemdtl where shipmentitemdtloid = sd.shipmentitemdtloid) shipmentitemqty, sretitemqty, sretitemunitoid, gn.gndesc sretitemunit, sretitemdtlnote, ISNULL(sretitemdtlres2,'') sretitemdtlres2 from QL_trnsretitemdtl sd INNER JOIN QL_m05GN gn ON gn.gnoid = sd.sretitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_mstjasa i ON i.jasaoid = sd.itemoid LEFT JOIN QL_m05GN gn2 ON gn2.gnoid = sd.sretitemwhoid and gn2.gngroup = 'GUDANG' WHERE sd.cmpcode='" + CompnyCode + "' AND sd.sretitemmstoid='" + id + "' ORDER BY sd.sretitemdtlseq";
                Session["QL_trnsretitemdtl"] = db.Database.SqlQuery<sretitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: sretitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsretitemmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.sretitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("sretitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.sretitemmststatus == "Post")
            {
                tbl.sretitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.sretitemno = "";
            }
            tbl.sretitemmstres1 = "Jasa";
            if (string.IsNullOrEmpty(tbl.sretitemmstres2))
                tbl.sretitemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.sretitemmstres3))
                tbl.sretitemmstres3 = "";
            if (string.IsNullOrEmpty(tbl.sretitemmstnote))
                tbl.sretitemmstnote = "";


            List<sretitemdtl> dtDtl = (List<sretitemdtl>)Session["QL_trnsretitemdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].sretitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY SJ item " + dtDtl[i].itemdesc + "  tidak boleh 0!");
                        }
                        else
                        {
                            if (dtDtl[i].sretitemqty > dtDtl[i].shipmentitemqty)
                            {
                                ModelState.AddModelError("", "QTY SJ item " + dtDtl[i].itemdesc + " Harus kurang dari SO QTY!");
                            }
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].sretitemdtlnote))
                            dtDtl[i].sretitemdtlnote = "";
                        if (string.IsNullOrEmpty(dtDtl[i].refno))
                            dtDtl[i].refno = "";
                        sSql = "SELECT (shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=shd.cmpcode AND sretd.shipmentitemdtloid=shd.shipmentitemdtloid AND sretitemmststatus<>'Rejected' AND sretd.sretitemmstoid<>" + tbl.sretitemmstoid + "), 0)) AS shipmentitemqty FROM QL_trnshipmentitemdtl shd WHERE shd.cmpcode='" + CompnyCode + "' AND shd.shipmentitemdtloid=" + dtDtl[i].shipmentitemdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dtDtl[i].sretitemqty > dQty)
                            ModelState.AddModelError("", "QTY SJ item " + dtDtl[i].itemdesc + " sebagian detail data telah diupdate oleh user yg lain. Silahkan cek setiap detail QTY SJ harus lebih dari retur sj QTY");
                        if (tbl.sretitemmststatus == "Post")
                        {
                            if (string.IsNullOrEmpty(tbl.sretitemno))
                                ModelState.AddModelError("sretitemno", "Silahkan isi No. SJ!");
                            else if (db.QL_trnsretitemmst.Where(w => w.sretitemno == tbl.sretitemno & w.sretitemmstoid != tbl.sretitemmstoid).Count() > 0)
                                ModelState.AddModelError("sretitemno", "No. SJ yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");

                            ////Stock Value
                            //decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].itemoid);
                            //decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dtDtl[i].sretitemqty, sValue, "");
                            //dtDtl[i].sretitemamt = sAvgValue;
                            decimal value = 0;
                            sSql = "select sod.soitemprice from QL_trnshipmentitemdtl shd INNER JOIN QL_trnsoitemdtl sod ON sod.soitemdtloid=shd.soitemdtloid WHERE shd.shipmentitemdtloid="+ dtDtl[i].shipmentitemdtloid + "";
                            value = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            dtDtl[i].sretitemamt = (dtDtl[i].sretitemqty * value);
                        }
                    }
                }
            }

            // Interface Validation
            if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
            if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnsretitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsretitemdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                var iDebetAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                var iCreditAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        tbl.approvalcode = tbl.approvalcode == null ? "" : tbl.approvalcode;
                        tbl.approvaldatetime = ClassFunction.GetServerTime();
                        tbl.approvaluser = tbl.approvaluser == null ? "" : tbl.approvaluser;
                        tbl.revisednote = tbl.revisednote == null ? "" : tbl.revisednote;
                        tbl.revisedtime = ClassFunction.GetServerTime();
                        tbl.reviseduser = tbl.reviseduser == null ? "" : tbl.reviseduser;

                        if (action == "Create")
                        {

                            tbl.sretitemmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.sretitemdate);
                            //tbl.cmpcode = CompnyCode;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;

                            db.QL_trnsretitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.sretitemmstoid + " WHERE tablename='QL_trnsretitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnsretitemdtl WHERE cmpcode='" + CompnyCode + "' AND sretitemmstoid=" + tbl.sretitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnsretitemmst WHERE cmpcode='" + CompnyCode + "' AND sretitemmstoid=" + tbl.sretitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnsretitemdtl.Where(a => a.sretitemmstoid == tbl.sretitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnsretitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnsretitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsretitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.sretitemdtloid = dtloid++;
                            tbldtl.sretitemmstoid = tbl.sretitemmstoid;
                            tbldtl.sretitemdtlseq = i + 1;
                            tbldtl.shipmentitemmstoid = tbl.shipmentitemmstoid;
                            tbldtl.shipmentitemdtloid = dtDtl[i].shipmentitemdtloid;
                            tbldtl.refno = (dtDtl[i].refno == null ? "" : dtDtl[i].refno);
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.sretitemqty = dtDtl[i].sretitemqty;
                            tbldtl.sretitemunitoid = dtDtl[i].sretitemunitoid;
                            tbldtl.sretitemwhoid = dtDtl[i].sretitemwhoid;
                            tbldtl.sretitemdtlstatus = "";
                            tbldtl.sretitemdtlnote = (dtDtl[i].sretitemdtlnote == null ? "" : dtDtl[i].sretitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.sretitemdtlres1 = "";
                            tbldtl.sretitemdtlres2 = dtDtl[i].sretitemdtlres2;
                            tbldtl.sretitemdtlres3 = "";
                            tbldtl.sretitemvalueidr = dtDtl[i].sretitemamt;
                            tbldtl.sretitemvalueusd = 0;
                            tbldtl.serialnumber = (dtDtl[i].serialnumber == null ? "" : dtDtl[i].serialnumber);

                            db.QL_trnsretitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].sretitemqty >= dtDtl[i].shipmentitemqty)
                            {
                                sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlres1='Complete' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemdtloid=" + dtDtl[i].shipmentitemdtloid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmstres1='Closed' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + " AND shipmentitemdtloid<>" + dtDtl[i].shipmentitemdtloid + " AND ISNULL(shipmentitemdtlres1, '')<>'Complete')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            if (tbl.sretitemmststatus == "Post")
                            {
                                // Update status SO
                                sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND soitemdtlstatus<>'' AND soitemdtloid IN (SELECT sd.soitemdtloid FROM QL_trnshipmentitemdtl sd WHERE sd.cmpcode='" + CompnyCode + "' AND shipmentitemdtloid=" + dtDtl[i].shipmentitemdtloid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND soitemmststatus<>'Post' AND soitemmstoid IN (SELECT sm.soitemmstoid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sd.cmpcode='" + CompnyCode + "' AND shipmentitemdtloid=" + dtDtl[i].shipmentitemdtloid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                //int rab = tbl.rabmstoid.Value;
                                //if (dtDtl[i].sretitemdtlres2 == "Booking")
                                //{
                                //    rab = db.Database.SqlQuery<int>("SELECT ISNULL(SELECT rabmstoid FROM QL_matbooking WHERE formoid=" + tbl.rabmstoid + " AND refoid=" + dtDtl[i].itemoid + " AND mtrwhoid=" + dtDtl[i].sretitemwhoid + " AND ISNULL(refno,'')='" + dtDtl[i].refno + "'),0) AS tbl").FirstOrDefault();
                                //}

                                ////Insert conmat
                                //db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "SRETFG", "QL_trnsretitemtl", tbl.sretitemmstoid, dtDtl[i].itemoid, "FINISH GOOD", dtDtl[i].sretitemwhoid, dtDtl[i].sretitemqty, "Sales Return FG", tbl.sretitemno, Session["UserID"].ToString(), null, dtDtl[i].sretitemamt, 0, 0, null, tbldtl.sretitemdtloid, rab, dtDtl[i].sretitemamt));
                                //db.SaveChanges();
                            }

                            //sSql = "UPDATE QL_matbooking SET flag='Post' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tbl.rabmstoid.Value + "";
                            //db.Database.ExecuteSqlCommand(sSql);
                            //db.SaveChanges();
                        }
                        // Update Last ID                        
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnsretitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //if (tbl.sretitemmststatus == "Post")
                        //{
                        //    decimal dSHAmt = 0;
                        //    dSHAmt = dtDtl.Sum(x => (x.sretitemamt));

                        //    // Insert QL_trnglmst
                        //    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Sret FG|No. " + tbl.sretitemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 0, 0));
                        //    db.SaveChanges();

                        //    var glseq = 1;

                        //    // Insert QL_trngldtl
                        //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iDebetAcctgOid, "D", dSHAmt, tbl.sretitemno, "Sret FG|No. " + tbl.sretitemno, "Post", Session["UserID"].ToString(), servertime, dSHAmt, 0, "QL_trnsretitemmst " + tbl.sretitemmstoid, null, null, null, 0));
                        //    db.SaveChanges();

                        //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iCreditAcctgOid, "C", dSHAmt, tbl.sretitemno, "Sret FG|No. " + tbl.sretitemno, "Post", Session["UserID"].ToString(), servertime, dSHAmt, 0, "QL_trnsretitemmst " + tbl.sretitemmstoid, null, null, null, 0));
                        //    db.SaveChanges();
                        //    glmstoid++;

                        //    // Update Last ID
                        //    sSql = "UPDATE QL_id SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //    sSql = "UPDATE QL_id SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //    sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.sretitemmststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.sretitemmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);

        }


        // POST: sretitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsretitemmst tbl = db.QL_trnsretitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnsretitemdtl WHERE cmpcode='" + CompnyCode + "' AND sretitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnsretitemmst WHERE cmpcode='" + CompnyCode + "' AND sretitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnsretitemdtl.Where(a => a.sretitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnsretitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnsretitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SJR/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(sretitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsretitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND sretitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSJReturnJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE srm.cmpcode='" + CompnyCode + "' AND srm.sretitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SJReturnReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}