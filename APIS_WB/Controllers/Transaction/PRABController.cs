﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PRABController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public PRABController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listDDLsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
        }

        private void InitDDL(QL_trnreqrabmst tbl, string action)
        {
            var sqlPlus = "";
            if (action == "Create")
            {
                sqlPlus = "AND gnflag='ACTIVE'";
            }
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' "+ sqlPlus +" ORDER BY CAST(gnother1 as INT)";
            var reqrabpaytermoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.reqrabpaytermoid);
            ViewBag.reqrabpaytermoid = reqrabpaytermoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PROVINSI' " + sqlPlus + "";
            var reqrabprovoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.reqrabprovoid);
            ViewBag.reqrabprovoid = reqrabprovoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' " + sqlPlus + "";
            var reqrabcityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.reqrabcityoid);
            ViewBag.reqrabcityoid = reqrabcityoid;

            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>("SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'").ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesadminoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesadminoid);
            ViewBag.salesadminoid = salesadminoid;
        }

        private void FillAdditionalField(QL_trnreqrabmst tblmst)
        {

            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();

            ViewBag.partnername = db.Database.SqlQuery<string>("SELECT partnername FROM QL_mstpartner a WHERE a.partneroid ='" + tblmst.partneroid + "'").FirstOrDefault();
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "PRAB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(reqrabno,16), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnreqrabmst WHERE cmpcode='" + CompnyCode + "' AND reqrabno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public class reqrabdtl : QL_trnreqrabdtl
        {
            public string itemcode { get; set; }
            public string reqrabunit { get; set; }
        }

        [HttpPost]
        public ActionResult GetDetailData()
        {
            sSql = "SELECT 0 reqrabseq, i.itemoid, i.itemtype, 'Master' reqrabflag_m, i.itemcode, i.itemdesc, 0.0 reqrabqty, i.itemunitoid reqrabunitoid, g.gndesc reqrabunit, 0.0 reqrabprice, 0.0 reqrabdtlamt, 10.0 reqrabtaxvalue, 0.0 reqrabtaxamt, 1.5 reqrabpphvalue, 0.0 reqrabpphamt, 0.0 reqrabdtlnetto FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE i.activeflag='ACTIVE' AND i.itemtype IN ('Barang', 'Rakitan', 'Ongkir', 'Jasa') ORDER BY itemcode ";
            var tbl = db.Database.SqlQuery<reqrabdtl>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailManualData(string itemtype)
        {
            sSql = $"SELECT 0 reqrabseq, 0 itemoid, '{itemtype}' itemtype, 'Manual' reqrabflag_m, '' itemcode, '' itemdesc, 0.0 reqrabqty, 0 reqrabunitoid, '' reqrabunit, 0.0 reqrabprice, 0.0 reqrabdtlamt, 10.0 reqrabtaxvalue, 0.0 reqrabtaxamt, 1.5 reqrabpphvalue, 0.0 reqrabpphamt, 0.0 reqrabdtlnetto";
            var tbl = db.Database.SqlQuery<reqrabdtl>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<reqrabdtl>("SELECT rd.reqrabseq, rd.itemoid, rd.itemtype, rd.reqrabflag_m, ISNULL(i.itemcode,'') itemcode, rd.itemdesc, rd.reqrabqty, rd.reqrabunitoid, ISNULL(g.gndesc,'') reqrabunit, rd.reqrabprice, rd.reqrabdtlamt, rd.reqrabtaxvalue, rd.reqrabtaxamt, rd.reqrabdtlnetto, rd.reqrabpphvalue, rd.reqrabpphamt FROM QL_trnreqrabdtl rd LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemoid LEFT JOIN QL_m05GN g ON g.gnoid=rd.reqrabunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.reqrabmstoid=" + id + " ORDER BY rd.reqrabseq").ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetPartnerData()
        {
            List<QL_mstpartner> tbl = new List<QL_mstpartner>();

            sSql = "SELECT * FROM QL_mstpartner WHERE activeflag='ACTIVE' ORDER BY partnername";
            tbl = db.Database.SqlQuery<QL_mstpartner>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class mstcust : QL_mstcust
        {
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            sSql = "SELECT custoid, custcode, custname, custaddr, custprovinceoid, custcityoid, custpaymentoid FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            var tbl = db.Database.SqlQuery<mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }

        // GET/POST: PRAB
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            return View();
        }

        [HttpPost]
        public ActionResult getListDataTable(string ddlstatus, string periodfrom, string periodto)
        {

            sSql = "SELECT * FROM( Select pr.reqrabmstoid, pr.reqrabno, pr.reqrabdate, pr.reqrabpkm, pr.reqrabtype, pr.projectname, c.custname, pr.reqrabgrandtotalamt, pr.reqrabmststatus, CASE WHEN pr.reqrabmststatus = 'Revised' THEN pr.revisereason WHEN pr.reqrabmststatus = 'Rejected' THEN pr.rejectreason ELSE '' END reason FROM QL_trnreqrabmst pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(periodfrom) && !string.IsNullOrEmpty(periodto))
            {
                sSql += " AND t.reqrabdate >='" + periodfrom + " 00:00:00' AND t.reqrabdate <='" + periodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(ddlstatus))
            {
                sSql += " AND t.reqrabmststatus " + ddlstatus;
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRAB/Form
        public ActionResult Form(int? id, int? idcopy)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnreqrabmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnreqrabmst();
                tblmst.reqrabmststatus = "In Process";
                tblmst.reqrabno = "";//generateNo(ClassFunction.GetServerTime());
                tblmst.cmpcode = CompnyCode;
                tblmst.custoid = 0;
                tblmst.reqrabmstoid_revisi = 0;
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.upduser = Session["UserID"].ToString();

                ViewBag.reqrabdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.reqrabestdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else if (idcopy != null)
            {
                tblmst = db.QL_trnreqrabmst.Find(Session["CompnyCode"].ToString(), id);

                var refrab = db.QL_trnreqrabmst.Where(x => x.reqrabmstoid == idcopy).FirstOrDefault().reqrabno;
                string sNo = ClassFunction.Left(refrab, 16) + "-R";
                sSql = "SELECT ISNULL(MAX(CAST(REPLACE(reqrabno, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnreqrabmst WHERE cmpcode='" + CompnyCode + "' AND reqrabno LIKE '" + sNo + "%'";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                tblmst.reqrabno = sNo + sCounter;

                tblmst.reqrabmstoid_revisi = idcopy;
                tblmst.reqrabmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.upduser = Session["UserID"].ToString();

                ViewBag.reqrabdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.reqrabestdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnreqrabmst.Find(Session["CompnyCode"].ToString(), id);

                ViewBag.reqrabdate = tblmst.reqrabdate.Value.ToString("dd/MM/yyyy");
                ViewBag.reqrabestdate = tblmst.reqrabestdate.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tblmst.updtime.ToString("dd/MM/yyyy");
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst, action);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnreqrabmst tblmst, List<reqrabdtl> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            //Is Input Valid?
            if (tblmst.custoid == 0)
                msg += "- Pilih Customer Terlebih Dahulu!!<br>";
            if (string.IsNullOrEmpty(tblmst.projectname))
                msg += "- Input Project Terlebih Dahulu!!<br>";
            if (string.IsNullOrEmpty(tblmst.salesoid))
                msg += "- Pilih Sales Terlebih Dahulu!!<br>";
            if (string.IsNullOrEmpty(tblmst.salesadminoid))
                msg += "- Pilih Sales Admin Terlebih Dahulu!!<br>";
            if (string.IsNullOrEmpty(tblmst.reqrabmstnote))
                tblmst.reqrabmstnote = "";
            if (tblmst.reqrabmststatus.ToUpper() == "REVISED")
            {
                tblmst.reqrabmststatus = "In Process";
            }
            tblmst.rate2oid = 0;

            //is Input Detail Valid
            if (dtDtl == null)
                msg += "- Please fill detail data!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].itemdesc))
                            msg += "- Deskripsi Belum Diisi !!<br>";
                        if (dtDtl[i].reqrabqty == 0)
                        {
                            msg += "- Qty " + dtDtl[i].itemdesc + " Belum Diisi !!<br>";
                        }
                        if (dtDtl[i].reqrabqty == 0)
                        {
                            msg += "- Harga " + dtDtl[i].itemdesc + " Belum Diisi !!<br>";
                        }
                    }
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tblmst.reqrabmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    msg += "- Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.reqrabdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                tblmst.reqrabmststatus = "In Process";
            }
            if (tblmst.reqrabmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                    tblmst.reqrabmststatus = "In Process";
                }
            }

            if (string.IsNullOrEmpty(tblmst.reqrabno))
            {
                if (tblmst.reqrabmststatus == "Post")
                {
                    tblmst.reqrabno = generateNo(tblmst.reqrabdate.Value);
                }
                else
                {
                    tblmst.reqrabno = "";
                }
            }

            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_trnreqrabmst");
                var dtloid = ClassFunction.GenerateID("QL_trnreqrabdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.reqrabmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnreqrabmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnreqrabmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnreqrabdtl.Where(a => a.reqrabmstoid == tblmst.reqrabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnreqrabdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        if (tblmst.reqrabmstoid_revisi != 0)
                        {
                            sSql = "UPDATE QL_trnreqrabmst SET reqrabmststatus='Closed' WHERE reqrabmstoid="+ tblmst.reqrabmstoid_revisi + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var tbldtl = new List<QL_trnreqrabdtl>();
                        foreach (var item in dtDtl)
                        {
                            var new_dtl = (QL_trnreqrabdtl)ClassFunction.MappingTable(new QL_trnreqrabdtl(), item);
                            if (string.IsNullOrEmpty(item.reqrabdtlstatus)) new_dtl.reqrabdtlstatus = "";
                            new_dtl.cmpcode = tblmst.cmpcode;
                            new_dtl.reqrabdtloid = dtloid++;
                            new_dtl.reqrabmstoid = tblmst.reqrabmstoid;
                            new_dtl.upduser = tblmst.upduser;
                            new_dtl.updtime = tblmst.updtime;
                            tbldtl.Add(new_dtl);
                        }
                        db.QL_trnreqrabdtl.AddRange(tbldtl);

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnreqrabdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tblmst.reqrabmststatus == "In Approval")
                        {
                            tblapp.cmpcode = tblmst.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tblmst.reqrabmstoid;
                            tblapp.requser = tblmst.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnreqrabmst";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }        

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_trnreqrabmst tblmst = db.QL_trnreqrabmst.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_trnreqrabmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPRAB.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.reqrabmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PRABReport.pdf");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}