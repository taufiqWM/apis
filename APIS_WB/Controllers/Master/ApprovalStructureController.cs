﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class ApprovalStructureController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public ApprovalStructureController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listapprovalstructure
        {
            public int asoid { get; set; }
            public int stoid_req { get; set; }
            public string req { get; set; }
            public int stoid_app { get; set; }
            public string app { get; set; }
            public int mnoid { get; set; }
            public string mnname { get; set; }
        }

        // GET/POST : ApprovalStructure
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = " SELECT a.cmpcode, a.asoid, a.stoid_req, r.stname req, ap.stname app, a.mnoid, m.mnname  FROM QL_m08AS a INNER JOIN QL_m07ST r ON r.cmpcode=a.cmpcode AND r.stoid=a.stoid_req INNER JOIN QL_m07ST ap ON ap.cmpcode=a.cmpcode AND ap.stoid=a.stoid_app INNER JOIN QL_m04MN m ON a.cmpcode=m.cmpcode AND a.mnoid=m.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by a.asoid desc ";
            List<listapprovalstructure> approvalstruc = db.Database.SqlQuery<listapprovalstructure>(sSql).ToList();
            return View(approvalstruc);
        }        

        // GET: ApprovalStructure/Edit/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m08AS QL_m08AS;
            string action = "Create";
            if (id == null)
            {
                QL_m08AS = new QL_m08AS();
                QL_m08AS.cmpcode = Session["CompnyCode"].ToString();
                //QL_m08AS.crtuser = Session["UserID"].ToString();
                //QL_m08AS.crttime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                QL_m08AS = db.QL_m08AS.Find(Session["CompnyCode"].ToString(), id);     
            }

            if (QL_m08AS == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(QL_m08AS);
            return View(QL_m08AS);
        }

        // POST: ApprovalStructure/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m08AS QL_m08AS, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (QL_m08AS.stoid_req == QL_m08AS.stoid_app)
                ModelState.AddModelError("", "Request dan Approval tidak boleh sama !!");
            if (db.QL_m08AS.Where(w => w.stoid_req == QL_m08AS.stoid_req & w.stoid_app == QL_m08AS.stoid_app & w.mnoid == QL_m08AS.mnoid & w.asoid != QL_m08AS.asoid).Count() > 0)
                ModelState.AddModelError("", "Data dengan kombinasi ini sudah ada !!");
        

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_m08AS");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            QL_m08AS.cmpcode = Session["CompnyCode"].ToString();
                            QL_m08AS.asoid = mstoid;
                            QL_m08AS.crttime = servertime;
                            QL_m08AS.crtuser = Session["UserID"].ToString();
                            QL_m08AS.updtime = servertime;
                            QL_m08AS.upduser = Session["UserID"].ToString();
                            db.QL_m08AS.Add(QL_m08AS);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m08AS'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            QL_m08AS.updtime = servertime;
                            QL_m08AS.upduser = Session["UserID"].ToString();
                            db.Entry(QL_m08AS).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(QL_m08AS);
            return View(QL_m08AS);
        }

        private void setViewBag(QL_m08AS QL_m08AS)
        {
            sSql = " Select * from QL_m07ST where stflag='ACTIVE'";
            ViewBag.stoid_req = new SelectList(db.Database.SqlQuery<QL_m07ST>(sSql), "stoid", "stname", QL_m08AS.stoid_req);
            //ViewBag.stoid_req = new SelectList(db.QL_m07ST.Where (w => w.stoid == QL_m08AS.stoid_req).ToList(), "stoid", "stname", QL_m08AS.stoid_req);
            sSql = " Select * from QL_m07ST where stflag='ACTIVE'";
            ViewBag.stoid_app = new SelectList(db.Database.SqlQuery<QL_m07ST>(sSql), "stoid", "stname", QL_m08AS.stoid_app);
            sSql = " Select * from QL_m04MN where appflag=1 AND mnflag='ACTIVE'";
            ViewBag.mnoid = new SelectList(db.Database.SqlQuery<QL_m04MN>(sSql), "mnoid", "mnname", QL_m08AS.mnoid);
        }


        // POST: ApprovalStructure/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_m08AS QL_m08AS = db.QL_m08AS.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_m08AS.Remove(QL_m08AS);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult getApprovalStructure(int? asoid, int? stoid_req, int? stoid_app, int? mnoid)
        {
            var result = "sukses";
            var msg = "";
            List<QL_m08AS> QL_m08AS = new List<QL_m08AS>();
            if (Session["UserID"] == null)
            {
                result = "failed";
                msg = "Session Null";
            }
            else
            {
                var sWhere = "";
                if (asoid != null)
                {
                    sWhere += " AND a.asoid = " + asoid;
                }
                if (stoid_req != null)
                {
                    sWhere += " AND stoid_req = '" + stoid_req + "'";
                }

                if (stoid_app != null)
                {
                    sWhere += " OR stoid_app = " + stoid_app;
                }

                if (mnoid != null)
                {
                    sWhere += " OR a.mnoid = " + mnoid;
                }
                sSql = "SELECT a.cmpcode, a.asoid, a.stoid_req, r.stname req, ap.stname app, a.mnoid, m.mnname  FROM QL_m08AS a INNER JOIN QL_m07ST r ON r.cmpcode=a.cmpcode AND r.stoid=a.stoid_req INNER JOIN QL_m07ST ap ON ap.cmpcode=a.cmpcode AND ap.stoid=a.stoid_app INNER JOIN QL_m04MN m ON a.cmpcode=m.cmpcode AND a.mnoid=m.mnoid WHERE ( a.cmpcode = '" + Session["CompnyCode"].ToString() + "' " + sWhere + ")" ;
                QL_m08AS = db.Database.SqlQuery<QL_m08AS>(sSql).ToList();
            }

            return Json(new { result, msg, QL_m08AS }, JsonRequestBehavior.AllowGet);
        }
    }
}
