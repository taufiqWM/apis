﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class SOJasaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public SOJasaController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listsoitem
        {
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
            public string rabno { get; set; }
            public string soitemmststatus { get; set; }
        }

        public class listsoitemdtl
        {
            public int soitemdtlseq { get; set; }
            public int rabdtloid { get; set; }
            public int jasaoid { get; set; }
            public string jasacode { get; set; }
            public string jasadesc { get; set; }
            public decimal soitemqty { get; set; }
            public int soitemunitoid { get; set; }
            public string soitemunit { get; set; }
            public decimal soitemprice { get; set; }
            public decimal soitemdtlamt { get; set; }
            public decimal soitemdtltaxvalue { get; set; }
            public decimal soitemdtltaxamt { get; set; }
            public decimal soitemdtlpphvalue { get; set; }
            public decimal soitemdtlpphamt { get; set; }
            public decimal soitemdtlnetto { get; set; }           
            public string soitemdtlnote { get; set; }
            public decimal soitemdtlongkiramt { get; set; }
            public DateTime soitemdtletd { get; set; }
            public string soitemdtletdstr { get; set; }
            public int soitemdtllocoid { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public string reqrabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public int custoid { get; set; }
            public int custpaymentoid { get; set; }
            public string rabmstnote { get; set; }
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public string salesoid { get; set; }
            public int deptoid { get; set; }            
            public string departement { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listmat
        {
            public int jasaseq { get; set; }
            public int jasaoid { get; set; }
            public string jasacode { get; set; }
            public string jasadesc { get; set; }
            public int jasaunitoid { get; set; }
            public string jasaunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listsoitemdtl> dtDtl)
        {
            Session["QL_trnsoitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsoitemdtl"] == null)
            {
                Session["QL_trnsoitemdtl"] = new List<listsoitemdtl>();
            }

            List<listsoitemdtl> dataDtl = (List<listsoitemdtl>)Session["QL_trnsoitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SOJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(soitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnsoitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND soitemno LIKE '" + sNo + "%' AND revsoitemtype='Baru'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }
        private void InitDDL(QL_trnsoitemmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", "deptoid");
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 as int)";
            var soitempaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.soitempaytypeoid);
            ViewBag.soitempaytypeoid = soitempaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            //sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PROVINSI' AND gnflag='ACTIVE'";
            //var rabprovoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabprovoid);
            //ViewBag.rabprovoid = rabprovoid;

            //sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' AND gnflag='ACTIVE'";
            //var rabcityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabcityoid);
            //ViewBag.rabcityoid = rabcityoid;
        }

        private void FillAdditionalField(QL_trnsoitemmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.salesoid = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tblmst.salesoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 a WHERE a.custdtl2oid ='" + tblmst.alamatoid + "'").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst a WHERE a.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT reqrabno FROM QL_trnreqrabmst r WHERE r.reqrabmstoid IN (SELECT reqrabmstoid FROM QL_trnrabmst where rabmstoid ='" + tblmst.rabmstoid + "')").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT DISTINCT rm.rabmstoid, rm.rabno, rm.rabdate, CONVERT(CHAR(10), rm.rabdate, 103) rabdatestr, c.custname, c.custoid, c.custpaymentoid, isnull(rm.projectname, '') projectname, req.reqrabno, rm.rabmstnote, rm.alamatoid, c2.custdtl2addr alamat, rm.salesoid, rm.deptoid, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid = rm.deptoid),'') departement FROM QL_trnrabmst rm LEFT JOIN QL_trnreqrabmst req ON req.reqrabmstoid = rm.reqrabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_mstcustdtl2 c2 ON c2.custdtl2oid = rm.alamatoid INNER JOIN QL_trnrabdtl3 rd3 ON rd3.rabmstoid = rm.rabmstoid WHERE rm.cmpcode = '" + Session["CompnyCode"].ToString() + "' AND rabmststatus = 'Approved' AND ISNULL(statusjualjasa,'') = '' ORDER BY rabdate DESC";

            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailData(int rabmstoid)
        {
            List<listsoitemdtl> tbl = new List<listsoitemdtl>();
            sSql = "select rabdtl3seq soitemdtlseq, rabdtl3oid rabdtloid, i.jasaoid, i.jasacode, i.jasadesc, rd.rabdtl3qty soitemqty, rd.rabdtl3unitoid soitemunitoid, gn.gndesc soitemunit, rd.rabdtl3price soitemprice, rd.rabdtl3amt soitemdtlamt,rd.rabdtl3taxvalue soitemdtltaxvalue, rd.rabdtl3taxamt soitemdtltaxamt, rd.rabdtl3pphvalue soitemdtlpphvalue, rd.rabdtl3pphamt soitemdtlpphamt, rd.rabdtl3netto soitemdtlnetto, rd.rabdtl3etd soitemdtletd, CONVERT(CHAR(10),rd.rabdtl3etd,103) soitemdtletdstr, rd.rabdtl3locoid soitemdtllocoid, rd.rabdtl3note soitemdtlnote from ql_trnrabdtl3 rd INNER JOIN QL_mstjasa i ON i.jasaoid = rd.itemdtl3oid INNER JOIN QL_m05gn gn ON gn.gnoid = rd.rabdtl3unitoid INNER JOIN QL_m05gn gn1 ON gn1.gnoid = rd.rabdtl3locoid where rd.rabmstoid = " + rabmstoid +"";

                tbl = db.Database.SqlQuery<listsoitemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY jasacode) AS INT) jasaseq, jasaoid, jasacode, jasadesc, jasaunitoid, g.gndesc jasaunit FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE activeflag='ACTIVE' ORDER BY jasacode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: SOItem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            string sfilter = "";
            sSql = "Select pr.soitemmstoid, pr.soitemno, pr.soitemdate, c.custname, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') rabno, pr.soitemmststatus FROM QL_trnsoitemmst pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid WHERE pr.cmpcode='" + Session["CompnyCode"].ToString() + "' AND pr.soitemtype IN ('Jasa') " + sfilter + "  ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND pr.soitemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND pr.soitemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND pr.soitemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY pr.soitemdate";
            List<listsoitem> tblmst = db.Database.SqlQuery<listsoitem>(sSql).ToList();
            return View(tblmst);
        }


        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnsoitemmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.soitemno = generateNo(ClassFunction.GetServerTime());
                tblmst.soitemdate = ClassFunction.GetServerTime();
                tblmst.soitemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_trnsoitemdtl"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnsoitemmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT rd.soitemdtlseq, rd.rabdtloid, rd.itemoid jasaoid, i.jasacode, i.jasadesc, rd.soitemqty, rd.soitemunitoid, g.gndesc soitemunit, rd.soitemprice, rd.soitemdtlamt, rd.soitemdtltaxvalue, rd.soitemdtltaxamt, rd.soitemdtlnetto, rd.soitemdtlpphvalue, rd.soitemdtlpphamt, rd.soitemdtlnote, rd.soitemdtlongkiramt, rd.soitemdtletd, CONVERT(CHAR(10),rd.soitemdtletd,103) soitemdtletdstr, rd.soitemdtllocoid FROM QL_trnsoitemdtl rd INNER JOIN QL_trnsoitemmst sm ON sm.soitemmstoid = rd.soitemmstoid AND sm.soitemtype = 'Jasa' INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.soitemunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.soitemmstoid=" + id + " ORDER BY rd.soitemdtlseq";
                Session["QL_trnsoitemdtl"] = db.Database.SqlQuery<listsoitemdtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsoitemmst tblmst, string action, string tglmst, string tglpodate, string tglpodate2)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tblmst.custoid == null)
                ModelState.AddModelError("custoid", "Pilih Customer Dulu!!");
            try
            {
                tblmst.soitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.soitemcustpodate = DateTime.Parse(ClassFunction.toDate(tglpodate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soitemcustpodate", "Format Tanggal PO Customer Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.soitemcustpodate2 = DateTime.Parse(ClassFunction.toDate(tglpodate2));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("soitemcustpodate2", "Format Tanggal Penyelesaian Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tblmst.soitemmstnote))
                tblmst.soitemmstnote = "";            
            if (string.IsNullOrEmpty(tblmst.soitemrefno))
                tblmst.soitemrefno = "";
            if (string.IsNullOrEmpty(tblmst.soitemcustpo))
                tblmst.soitemcustpo = "";
            if (string.IsNullOrEmpty(tblmst.soitemcustdoc))
                tblmst.soitemcustdoc = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstres1))
                tblmst.soitemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstres2))
                tblmst.soitemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.soitemmstres3))
                tblmst.soitemmstres3 = "";
            if (tblmst.soitemmststatus == "Post")
            {
                tblmst.soitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tblmst.soitemno = "";
            }

            //is Input Detail Valid
            List<listsoitemdtl> dtDtl = (List<listsoitemdtl>)Session["QL_trnsoitemdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].soitemdtlnote))
                            dtDtl[i].soitemdtlnote = "";
                        if (dtDtl[i].soitemqty == 0)
                        {
                            ModelState.AddModelError("", "Qty " + dtDtl[i].jasadesc + " Belum Diisi !!");
                        }
                        if (dtDtl[i].soitemprice == 0)
                        {
                            ModelState.AddModelError("", "Harga " + dtDtl[i].jasadesc + " Belum Diisi !!");
                        }
                        try
                        {
                            dtDtl[i].soitemdtletd = DateTime.Parse(ClassFunction.toDate(dtDtl[i].soitemdtletdstr));
                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("", "Format Tanggal etd Tidak Valid!!" + ex.ToString());
                        }
                        sSql = "SELECT (rabdtl3qty - ISNULL((SELECT SUM(soitemqty) FROM QL_trnsoitemdtl sd INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.soitemmstoid=sd.soitemmstoid WHERE sd.cmpcode=dod.cmpcode AND soitemmststatus<>'Rejected' AND sm.soitemtype = 'Jasa' AND sd.rabdtloid=dod.rabdtl3oid AND sd.soitemmstoid<>" + tblmst.soitemmstoid + "), 0.0)) AS soitemqty FROM QL_trnrabdtl3 dod WHERE dod.cmpcode='" + CompnyCode + "' AND rabdtl3oid=" + dtDtl[i].rabdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].soitemqty)
                            dtDtl[i].soitemqty = dQty;
                        if (dQty < dtDtl[i].soitemqty)
                            ModelState.AddModelError("", "Qty Jasa " + dtDtl[i].jasadesc + " RAB telah diupdate oleh user yg lain. Silahkan cek setiap Qty Harus kurang dari RAB QTY!");

                    }
                }
            }

            var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();
            if (tblmst.soitemmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid.Value, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
            }

            if (ModelState.IsValid)
            {
                tblmst.rate2oid = cRate.GetRateMonthlyOid;
                tblmst.soitemrate2toidr = cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtotalamtidr = tblmst.soitemtotalamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtotaltaxamtidr = tblmst.soitemtotaltaxamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtotalpphamtidr = tblmst.soitemtotalpphamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemgrandtotalamtidr = tblmst.soitemgrandtotalamt * cRate.GetRateMonthlyIDRValue;
                tblmst.soitemtype = "Jasa";
                tblmst.rateoid = 0;
                tblmst.soitemtotaldiscamt = 0;
                tblmst.soitemtotaldiscamtidr = 0;
                tblmst.rate2oid = 1;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    var mstoid = ClassFunction.GenerateID("QL_trnsoitemmst");
                    var dtloid = ClassFunction.GenerateID("QL_trnsoitemdtl");

                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.soitemmstoid = mstoid;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnsoitemmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnsoitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == tblmst.soitemmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnsoitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnsoitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsoitemdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.soitemdtloid = dtloid++;
                            tbldtl.soitemmstoid = tblmst.soitemmstoid;
                            tbldtl.soitemdtlseq = i + 1;
                            tbldtl.rabdtloid = dtDtl[i].rabdtloid;
                            tbldtl.itemoid = dtDtl[i].jasaoid;
                            tbldtl.soitemqty = dtDtl[i].soitemqty;
                            tbldtl.closeqty = 0;
                            tbldtl.soitemprice = dtDtl[i].soitemprice;
                            tbldtl.soitempriceidr = dtDtl[i].soitemprice;
                            tbldtl.soitemunitoid = dtDtl[i].soitemunitoid;
                            tbldtl.soitemdtlamt = dtDtl[i].soitemdtlamt;
                            tbldtl.soitemdtlamtidr = dtDtl[i].soitemdtlamt;
                            tbldtl.soitemdtltaxvalue = dtDtl[i].soitemdtltaxvalue;
                            tbldtl.soitemdtltaxamt = dtDtl[i].soitemdtltaxamt;
                            tbldtl.soitemdtltaxamtidr = dtDtl[i].soitemdtltaxamt;
                            tbldtl.soitemdtlpphvalue = dtDtl[i].soitemdtlpphvalue;
                            tbldtl.soitemdtlpphamt = dtDtl[i].soitemdtlpphamt;
                            tbldtl.soitemdtlpphamtidr = dtDtl[i].soitemdtlpphamt;
                            tbldtl.soitemdtlnetto = dtDtl[i].soitemdtlnetto;
                            tbldtl.soitemdtlnettoidr = dtDtl[i].soitemdtlnetto;
                            tbldtl.soitemdtlstatus = "";
                            tbldtl.soitemdtlnote = dtDtl[i].soitemdtlnote;
                            tbldtl.soitemdtlongkiramt = dtDtl[i].soitemdtlongkiramt;
                            tbldtl.soitemdtletd = dtDtl[i].soitemdtletd;
                            tbldtl.soitemdtllocoid = dtDtl[i].soitemdtllocoid;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.soitemdtldisctype = "";
                            tbldtl.soitemdtldiscamt = 0;
                            tbldtl.soitemdtldiscamtidr = 0;
                            tbldtl.soitemdtldiscvalue = 0;
                            tbldtl.soitemdtlres1 = "";
                            tbldtl.soitemdtlres2 = "";
                            tbldtl.soitemdtlres3 = "";

                            db.QL_trnsoitemdtl.Add(tbldtl);
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnsoitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tblmst.soitemmststatus == "Post")
                        {
                            sSql = "UPDATE QL_trnrabmst SET statusjual='SO Jasa' WHERE rabmstoid="+ tblmst.rabmstoid +"";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                            // Oh we are here, looks like everything is fine - save all the data permanently
                            objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.soitemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.soitemmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsoitemmst tblmst = db.QL_trnsoitemmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnsoitemdtl.Where(a => a.soitemmstoid == id);
                        db.QL_trnsoitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnsoitemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSOJasa.rpt"));           

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usoid FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.soitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptSOJasa.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}