﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class DivisiController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public DivisiController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listdivisi
        {
            public int groupoid { get; set; }
            public string groupcode { get; set; }
            public string groupdesc { get; set; }
            public string groupnote { get; set; }
        }

        public class listdivisidtl
        {
            public int groupdtlseq { get; set; }
            public int deptoid { get; set; }
            public string deptcode { get; set; }
            public string deptname { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "DEPT/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(groupcode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstdeptgroup WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND groupcode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetDataDetails()
        {
            List<listdivisidtl> tbl = new List<listdivisidtl>();

                sSql = "SELECT /*CAST(ROW_NUMBER() OVER(ORDER BY deptcode) AS INT)*/ 0 AS groupdtlseq, d.deptoid, d.deptcode, d.deptname FROM QL_mstdept d WHERE d.activeflag='ACTIVE' AND d.deptoid NOT IN(SELECT dgd.deptoid FROM QL_mstdeptgroupdtl dgd) ORDER BY deptname";
            tbl = db.Database.SqlQuery<listdivisidtl>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
            //return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listdivisidtl> dtDtl)
        {
            Session["QL_mstdeptgroupdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstdeptgroupdtl"] == null)
            {
                Session["QL_mstdeptgroupdtl"] = new List<listdivisidtl>();
            }

            List<listdivisidtl> dataDtl = (List<listdivisidtl>)Session["QL_mstdeptgroupdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: Divisi
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select d.groupoid, d.groupcode, d.groupdesc, d.groupnote FROM QL_mstdeptgroup d WHERE d.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by d.groupcode ";
            List<listdivisi> tblmst = db.Database.SqlQuery<listdivisi>(sSql).ToList();
            return View(tblmst);
        }


        // GET: Divisi/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdeptgroup tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_mstdeptgroup();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.groupcode = generateNo(ClassFunction.GetServerTime());
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_mstdeptgroupdtl"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_mstdeptgroup.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT a.groupdtlseq, a.deptoid, d.deptcode, d.deptname from QL_mstdeptgroupdtl a INNER JOIN QL_mstdept d ON d.deptoid=a.deptoid WHERE a.cmpcode='" + CompnyCode + "' AND a.groupoid=" + id + " ORDER BY a.groupdtlseq";
                Session["QL_mstdeptgroupdtl"] = db.Database.SqlQuery<listdivisidtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstdeptgroup tblmst, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tblmst.groupdesc))
                ModelState.AddModelError("groupdesc", "Please Fill Nama Departemen!!");
            else if (db.QL_mstdeptgroup.Where(w => w.groupdesc == tblmst.groupdesc & w.groupoid == tblmst.groupoid & w.groupoid != tblmst.groupoid).Count() > 0)
                ModelState.AddModelError("groupdesc", "Nama Departemen Sudah Dipakai!!");

            List<listdivisidtl> dtDtl = (List<listdivisidtl>)Session["QL_mstdeptgroupdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstdeptgroup");
            var dtloid = ClassFunction.GenerateID("QL_mstdeptgroupdtl");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.groupoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_mstdeptgroup.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstdeptgroup'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstdeptgroupdtl.Where(a => a.groupoid == tblmst.groupoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_mstdeptgroupdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_mstdeptgroupdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_mstdeptgroupdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.groupdtloid = dtloid++;
                            tbldtl.groupoid = tblmst.groupoid;
                            tbldtl.groupdtlseq = i + 1;
                            tbldtl.deptoid = dtDtl[i].deptoid;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            db.QL_mstdeptgroupdtl.Add(tbldtl);
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_mstdeptgroupdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_mstdeptgroup tblmst)
        {
            //ViewBag.deptoid = new SelectList(db.QL_mstdeptgroup.Where(w => w.activeflag == "ACTIVE" | w.groupoid == tblmst.groupoid).ToList(), "groupoid", "groupoid", tblmst.groupoid);
        }

        // POST: Departemen/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdeptgroup tblmst = db.QL_mstdeptgroup.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstdeptgroupdtl.Where(a => a.groupoid == id);
                        db.QL_mstdeptgroupdtl.RemoveRange(trndtl);
                        db.SaveChanges();
                        
                        db.QL_mstdeptgroup.Remove(tblmst);
                        db.SaveChanges();
                       
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
