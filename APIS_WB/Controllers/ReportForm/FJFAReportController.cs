﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FJFAReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public FJFAReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetARData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomorAR, string DDLARNO)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, arm.arassetno, arm.arassetdate, c.custname FROM QL_trnarassetmst arm INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.arassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextNomorAR != "")
                {
                    string[] arr = TextNomorAR.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLARNO + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY arm.arassetno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblAR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT rm.custoid FROM QL_trnarassetmst rm WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND arm.arassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLARNO, string TextNomorAR, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnarassetdtl ard ON ard.assetmstoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid = ard.arassetunitoid INNER JOIN QL_trnarassetmst arm ON arm.arassetmstoid = ard.arassetmstoid INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.arassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomorAR != "")
                {
                    string[] arr = TextNomorAR.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLARNO + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        // GET: FJFAReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost] 
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorAR, string DDLARNO)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptARFASumXls.rpt";
                else
                    rptfile = "rptARFASumPdf.rpt";
                rptname = "FJFA_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptARFADtlXls.rpt";
                else
                {
                    rptfile = "rptARFADtlPdf.rpt";
                }
                rptname = "FJFA_DETAIL";
            }
            var Slct = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Slct += ", ard.arassetdtloid [AR Dtl Oid], ard.arassetdtlseq [Seq], ard.assetmstoid [Mat Oid], m.itemcode [Mat Code] , m.itemdesc [Mat Longdesc], arassetqty [Qty] , g2.gndesc [Unit], (arassetdtlamt / (arassetqty + 0.00)) [Price], arassetdtlamt [Detail Amount], arassetdtldisctype [Disc Type], arassetdtldiscvalue [Disc Value], arassetdtldiscamt [Disc Amount], '' [Disc Type 2], 0.00 [Disc Value 2], 0.00 [Disc Amount 2], arassetdtlnetto [Dtl Netto], arassetdtlnote [AR Detail Note], 1 AS [Convert Qty], ((arassetqty + 0.00) * 1) AS [Qty Dtl], '' AS [Unit Dtl] ";

                Join += " INNER JOIN QL_trnarassetdtl ard ON arm.cmpcode=ard.cmpcode AND arm.arassetmstoid=ard.arassetmstoid INNER JOIN QL_mstitem m ON m.itemoid=ard.assetmstoid INNER JOIN QL_m05GN g2 ON g2.gnoid=ard.arassetunitoid ";

                Slct += ", shm.shipmentassetmstoid [Shipment Oid],shipmentassetno [Shipment No.], shipmentassetdate [Shipment Date], shipmentassetvalueidr [PriceSJIDR], shipmentassetvalueusd [PriceSJUSD], '' [Shipment Container No.], '' [Shipment Driver],'' [Shipment Vehicle], '' [Shipment Police No.], shm.shipmentassetmststatus [Shipment Status], arm.arassetfaktur [Shipment SJ Pajak], '' [Shipment Seal No.], shipmentassetmstnote[Shipment Header Note], shm.createuser[Shipment Create User], shm.createtime[Shipment Create Datetime], '' AS[Shipment User Approval] , CURRENT_TIMESTAMP AS[Shipment ApprovalDate] ";

                Join += " INNER JOIN QL_trnshipmentassetmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentassetmstoid=ard.shipmentassetmstoid INNER JOIN QL_trnshipmentassetdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentassetdtloid=ard.shipmentassetdtloid ";

                Slct += ", 'APIS' AS [Divisi] ,som.soassetmstoid [SO ID], soassetno [SO No.], soassetdate [SO Date], ((SELECT gndesc FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnoid=soassetpaytypeoid)) [SO Payment Type] , '' [SO Price Type], som.soassettype [SO Local/Exp], '' AS [SO Direct Cust PO No.], 0 AS [Payment Method], CURRENT_TIMESTAMP [SO ETD], '' [Invoice Value],'' [SO Account No.], '' [Port Of Ship], '' [Port Of Dicharge], soassetmstnote [SO Header Note], soassetmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], CURRENT_TIMESTAMP AS [App User SO], CURRENT_TIMESTAMP AS [App Date SO]";

                Join += " INNER JOIN QL_trnsoassetmst som ON som.cmpcode=arm.cmpcode AND som.soassetmstoid=shd.soassetmstoid ";
            }

            sSql = "SELECT arm.arassetmstoid AS [armstoid], (CASE arm.arassetno WHEN '' THEN CONVERT(VARCHAR(10), arm.arassetmstoid) ELSE arm.arassetno END) AS [Draft No.], arm.arassetno AS [AR No.], arm.arassetdate AS [AR Date], CURRENT_TIMESTAMP [AR Date Take Giro], arm.arassetmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], arm.arassetratetoidr [Rate IDR], cast(arm.arassetratetousd as Float) [Rate USD], arm.arassettotalamt [AR Total Amt], arm.arassettotaldisc [AR Total Disc], arm.arassettotalnetto [Total Netto], arm.arassettaxtype [Tax Type], arm.arassettaxvalue [AR Tax Value], arm.arassettaxamt [AR Tax Amount], arm.arassetgrandtotal [AR Grand Total], arm.cmpcode AS [BU Name], arm.arassetmstnote AS [AR Header Note], ISNULL(arm.arassetmstres3,'') AS [Invoice No], arm.createuser [AR Create User], arm.createtime [AR Create Time], '' AS [AR User Approval], CURRENT_TIMESTAMP AS [AR ApprovalDate], arassetfaktur [No. Faktur] " + Slct + " FROM QL_trnarassetmst arm " + Join + " INNER JOIN QL_mstcust c ON c.custoid = arm.custoid INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid AND g.gngroup = 'PAYMENT TYPE' INNER JOIN QL_mstcurr curr ON curr.curroid = arm.curroid WHERE arm.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND arm.arassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorAR != "")
            {
                string[] arr = TextNomorAR.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLARNO + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            } 

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}