﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class FJAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public FJAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class arassetmst
        {
            public string cmpcode { get; set; }
            public int arassetmstoid { get; set; }
            public string arassetno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime? arassetdate { get; set; }
            public string invoiceno { get; set; }
            public string custname { get; set; }
            public string arassetmststatus { get; set; }
            public string arassetmstnote { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal arassetgrandtotal { get; set; }
            public string divname { get; set; }
        }

        public class arassetdtl
        {
            public int arassetdtlseq { get; set; }
            public int shipmentassetmstoid { get; set; }
            public string shipmentassetno { get; set; }
            public DateTime shipmentassetetd { get; set; }
            public int shipmentassetdtloid { get; set; }
            public int matrefoid { get; set; }
            public string matrefcode { get; set; }
            public string matreflongdesc { get; set; }
            public string matrefnote { get; set; }
            public decimal arassetqty { get; set; }
            public int arassetunitoid { get; set; }
            public string arassetunit { get; set; }
            public decimal arassetprice { get; set; }
            public decimal arassetdtlamt { get; set; }
            public string arassetdtldisctype { get; set; }
            public decimal arassetdtldiscvalue { get; set; }
            public decimal arassetdtldiscamt { get; set; }
            public decimal arassetdtlnetto { get; set; }
            public string arassetdtlnote { get; set; }
        }

        public class shipmentasset
        {
            public int shipmentassetmstoid { get; set; }
            public string shipmentassetno { get; set; }
            public DateTime shipmentassetdate { get; set; }
            public DateTime shipmentassetetd { get; set; }
            public string shipmentassetmstnote { get; set; }
            public string soassetno { get; set; }
            public string soassetcustpono { get; set; }
        }

        public class Rate
        {
            public int rateoid { get; set; }
            public decimal rateidrvalue { get; set; }
            public decimal rateusdvalue { get; set; }
            public int rate2oid { get; set; }
            public decimal rate2idrvalue { get; set; }
            public decimal rate2usdvalue { get; set; }
        }

        public class listso
        {
            public int soassetmstoid { get; set; }
            public DateTime soassetdate { get; set; }
            public string soassetno { get; set; }
            public string soassetmstnote { get; set; }
        }

        public class listfa
        {
            public int faoid { get; set; }
            public string fakturno { get; set; }
        }

        private void InitDDL(QL_trnarassetmst tbl, String action)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='PAYMENT TERM' AND gnflag='ACTIVE'";
            var arassetpaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.arassetpaytypeoid);
            ViewBag.arassetpaytypeoid = arassetpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            string sPlus = "";
            if (action == "Create")
            {
                sPlus = " WHERE /*faoid NOT IN (SELECT faoid FROM QL_trnaritemmst) AND*/ status = 'ACTIVE'";
            }
            sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  select faoid, fakturno from QL_mstFA  " + sPlus + "";
            var faoid = new SelectList(db.Database.SqlQuery<listfa>(sSql).ToList(), "faoid", "fakturno", tbl.faoid);
            ViewBag.faoid = faoid;
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FJA-" + tanggal.ToString("yy") + "-" + tanggal.ToString("MM") + "-";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(arassetno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnarassetmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND arassetno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        //[HttpPost]
        //public ActionResult InitDDLAppUser(string cmp)
        //{
        //    var result = "sukses";
        //    var msg = "";
        //    List<QL_APPperson> tbl = new List<QL_APPperson>();
        //    sSql = "SELECT * FROM QL_APPperson WHERE tablename='QL_trnarassetmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
        //    tbl = db.Database.SqlQuery<QL_APPperson>(sSql).ToList();

        //    return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult GetFAData(string action)
        {
            var result = "sukses";
            var msg = "";
            string sPlus = "";
            List<listfa> tbl = new List<listfa>();
            if (action == "Create")
            {
                sPlus = " WHERE /*faoid NOT IN (SELECT faoid FROM QL_trnaritemmst) AND*/ status = 'ACTIVE'";
            }
            sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  SELECT faoid, fakturno FROM QL_mstFA " + sPlus + "";
            tbl = db.Database.SqlQuery<listfa>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData(string cmp)
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();
            sSql = "SELECT * FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND s.activeflag='ACTIVE' AND s.custoid IN (SELECT custoid FROM QL_trnshipmentassetmst WHERE cmpcode='" + CompnyCode + "' AND shipmentassetmststatus='Post' AND ISNULL(shipmentassetmstres1,'')<>'Closed') ORDER BY s.custname";

            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSOData(string cmp, int custoid)
        {
            List<listso> tbl = new List<listso>();
            sSql = "SELECT som.soassetmstoid, som.soassetdate, som.soassetno, ISNULL(som.soassetmstnote,'') soassetmstnote FROM QL_trnsoassetmst som WHERE som.cmpcode='"+ CompnyCode +"' AND som.custoid="+ custoid + " AND som.soassetmststatus IN('Post','Closed') ORDER BY som.soassetno";

            tbl = db.Database.SqlQuery<listso>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShipmentData(string cmp, int custoid, int curroid, int somstoid)
        {
            List<shipmentasset> tbl = new List<shipmentasset>();
            sSql = "SELECT DISTINCT sm.shipmentassetmstoid, shipmentassetno, shipmentassetdate, shipmentassetmstnote, soassetno, '' soassetcustpono, shipmentassetetd FROM QL_trnshipmentassetmst sm INNER JOIN QL_trnshipmentassetdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentassetmstoid=sm.shipmentassetmstoid INNER JOIN QL_trnsoassetmst dom ON dom.cmpcode=sd.cmpcode AND dom.soassetmstoid=sd.soassetmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.custoid=" + custoid + " AND sm.soassetmstoid="+ somstoid +" AND shipmentassetmststatus='Post' AND ISNULL(shipmentassetmstres1,'')<>'Closed' AND sm.curroid=" + curroid + " ORDER BY shipmentassetdate DESC, sm.shipmentassetmstoid DESC";

            tbl = db.Database.SqlQuery<shipmentasset>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRateValue(int curroid, DateTime sDate)
        {
            var cRate = new ClassRate();
            Rate RateValue = new Rate();
            var result = "sukses";
            var msg = "";
            if (curroid != 0)
            {
                cRate.SetRateValue(curroid, sDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg = cRate.GetRateDailyLastError;
                if (msg == "")
                {
                    if (cRate.GetRateMonthlyLastError != "")
                        msg = cRate.GetRateMonthlyLastError;
                }

                if (msg == "")
                {
                    RateValue.rateoid = cRate.GetRateDailyOid;
                    RateValue.rateidrvalue = cRate.GetRateDailyIDRValue;
                    RateValue.rateusdvalue = cRate.GetRateDailyUSDValue;
                    RateValue.rate2oid = cRate.GetRateMonthlyOid;
                    RateValue.rate2idrvalue = cRate.GetRateMonthlyIDRValue;
                    RateValue.rate2usdvalue = cRate.GetRateMonthlyUSDValue;
                }
                else
                {
                    result = "failed";
                }
            }
            return Json(new { result, msg, RateValue }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int shipmentassetmstoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<arassetdtl> tbl = new List<arassetdtl>();
            sSql = "SELECT (SELECT shipmentassetno FROM QL_trnshipmentassetmst where shipmentassetmstoid = sd.shipmentassetmstoid) AS shipmentassetno, sd.shipmentassetmstoid, shipmentassetdtloid, shipmentassetdtlseq, sd.assetmstoid AS matrefoid, assetno, (SELECT itemcode FROM QL_mstitem m WHERE itemoid=am.refoid) AS matrefcode, (SELECT itemdesc FROM QL_mstitem m WHERE itemoid=am.refoid) AS matreflongdesc, (shipmentassetqty - ISNULL((SELECT SUM(sretassetqty) FROM QL_trnsretassetdtl sretd INNER JOIN QL_trnsretassetmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretassetmstoid=sretd.sretassetmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentassetdtloid=sd.shipmentassetdtloid AND sretassetmststatus<>'Rejected'), 0.0)) AS arassetqty, shipmentassetunitoid arassetunitoid, gndesc AS arassetunit, soassetprice AS arassetprice, soassetdtldisctype AS arassetdtldisctype, soassetdtldiscvalue AS arassetdtldiscvalue FROM QL_trnshipmentassetdtl sd INNER JOIN QL_assetmst am ON am.cmpcode = sd.cmpcode AND am.assetmstoid = sd.assetmstoid INNER JOIN QL_m05GN g ON gnoid = shipmentassetunitoid INNER JOIN QL_trnsoassetdtl dod ON dod.cmpcode = sd.cmpcode AND dod.soassetdtloid = sd.soassetdtloid WHERE sd.cmpcode = '" + CompnyCode + "' AND sd.shipmentassetmstoid = " + shipmentassetmstoid + " AND shipmentassetdtlstatus = '' AND(shipmentassetqty - ISNULL((SELECT SUM(sretassetqty) FROM QL_trnsretassetdtl sretd INNER JOIN QL_trnsretassetmst sretm ON sretm.cmpcode = sretd.cmpcode AND sretm.sretassetmstoid = sretd.sretassetmstoid WHERE sretd.cmpcode = sd.cmpcode AND sretd.shipmentassetdtloid = sd.shipmentassetdtloid AND sretassetmststatus <> 'Rejected'), 0.0)) > 0 ORDER BY shipmentassetdtlseq";

            tbl = db.Database.SqlQuery<arassetdtl>(sSql).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<arassetdtl> dtDtl)
        {
            Session["QL_trnarassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnarassetdtl"] == null)
            {
                Session["QL_trnarassetdtl"] = new List<arassetdtl>();
            }

            List<arassetdtl> dataDtl = (List<arassetdtl>)Session["QL_trnarassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnarassetmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid='" + tbl.custoid + "'").FirstOrDefault();
            ViewBag.soassetno = db.Database.SqlQuery<string>("SELECT soassetno FROM QL_trnsoassetmst WHERE cmpcode='" + CompnyCode + "' AND soassetmstoid='" + tbl.somstoid + "'").FirstOrDefault();
        }

        // GET: arassetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT arm.cmpcode, arm.arassetmstoid, arm.arassetno, arassetdate, s.custname, ISNULL(arm.arassetmstres3,'') AS invoiceno, arm.arassetmststatus, arm.arassetmstnote,arassetgrandtotal, arassetdatetakegiro, DATEADD(day,(CASE g1.gnother1 WHEN 0 THEN 0 ELSE CAST(g1.gnother1 AS INT) END), arassetdate) duedate FROM QL_trnarassetmst arm INNER JOIN QL_mstcust s ON arm.custoid=s.custoid INNER JOIN QL_m05GN g1 ON g1.gnoid=arassetpaytypeoid AND g1.gngroup='PAYMENT TERM' WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "arm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "arm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND arassetdate>=CAST('" + modfil.filterperiodfrom + " 00:00:00' AS DATETIME) AND arassetdate<=CAST('" + modfil.filterperiodto + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND arassetmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND arm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            List<arassetmst> dt = db.Database.SqlQuery<arassetmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: arassetMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarassetmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnarassetmst();
                tbl.cmpcode = CompnyCode;
                tbl.arassetmstoid = ClassFunction.GenerateID("QL_trnarassetmst");
                tbl.arassetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.arassetmststatus = "In Process";
                tbl.arassettaxvalue = 10;
                tbl.arassettaxamt = 0;
                
                Session["QL_trnarassetdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnarassetmst.Find(CompnyCode, id);

                sSql = "SELECT ard.arassetdtlseq,ard.shipmentassetmstoid,shm.shipmentassetno,CONVERT(VARCHAR(10),shm.shipmentassetetd,101) AS shipmentassetdate, ard.shipmentassetdtloid,ard.assetmstoid matrefoid,m.assetno, (SELECT itemcode FROM QL_mstitem i WHERE i.itemoid=ard.assetmstoid) matrefcode, (SELECT itemdesc FROM QL_mstitem i WHERE i.itemoid=ard.assetmstoid) matreflongdesc, ard.arassetqty, ard.arassetunitoid, g.gndesc AS arassetunit,ard.arassetprice, ard.arassetdtlamt, ard.arassetdtldisctype, ard.arassetdtldiscvalue, ard.arassetdtldiscamt, ard.arassetdtlnetto, ard.arassetdtlnote  FROM QL_trnarassetdtl ard INNER JOIN QL_assetmst m ON m.assetmstoid=ard.assetmstoid INNER JOIN QL_m05GN g ON g.gnoid=ard.arassetunitoid INNER JOIN QL_trnshipmentassetdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentassetdtloid=ard.shipmentassetdtloid INNER JOIN QL_trnshipmentassetmst shm ON shm.cmpcode=shd.cmpcode AND shm.shipmentassetmstoid=shd.shipmentassetmstoid WHERE ard.arassetmstoid=" + id + " ORDER BY ard.arassetdtlseq";
                Session["QL_trnarassetdtl"] = db.Database.SqlQuery<arassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: arassetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnarassetmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.arassetno == null)
                tbl.arassetno = "";

            try
            {
                tbl.arassetdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("arassetdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            List<arassetdtl> dtDtl = (List<arassetdtl>)Session["QL_trnarassetdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            var cRate = new ClassRate();
            var msg = "";
            cRate.SetRateValue(tbl.curroid, tbl.arassetdate.ToString("MM/dd/yyyy"));
            if (cRate.GetRateDailyLastError != "")
                msg = cRate.GetRateDailyLastError;
            if (msg == "")
            {
                if (cRate.GetRateMonthlyLastError != "")
                    msg = cRate.GetRateMonthlyLastError;
            }
            if (msg != "")
                ModelState.AddModelError("", msg);

            if (tbl.arassettaxtype == "TAX")
            {
                if (tbl.arassettaxvalue > 0)
                {
                    if (tbl.arassettaxvalue < 0 || tbl.arassettaxvalue > 100)
                    {
                        ModelState.AddModelError("", "Tax value must be between 0 and 100.");
                    }
                }
            }
            
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].arassetprice <= 0)
                        {
                            ModelState.AddModelError("", "Price "+ dtDtl[i].matrefcode +" must be greater than 0.");
                        }
                        if (dtDtl[i].arassetdtlamt <= 0)
                        {
                            ModelState.AddModelError("", "Detail Netto " + dtDtl[i].matrefcode + " must be greater than 0.");
                        }
                        sSql = "SELECT COUNT(*) FROM QL_trnshipmentassetmst sd WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + dtDtl[i].shipmentassetmstoid + " AND shipmentassetmststatus='Closed'";
                        if (action == "Edit")
                        {
                            sSql += " AND shipmentassetmstoid NOT IN (SELECT ard.shipmentassetmstoid FROM QL_trnarassetdtl ard WHERE ard.cmpcode=sd.cmpcode AND arassetmstoid=" + tbl.arassetmstoid + ")";
                        }
                        var iCek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCek > 0)
                            ModelState.AddModelError("", "Shipment No. " + dtDtl[i].shipmentassetno + " has been used by another data. Please cancel this transaction or use another Shipment No.!");
                    }
                }
            }

            ////Variable Send Approval
            //string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            //var appoid = ClassFunction.GenerateID("QL_APP");
            //sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            //var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            //if (tbl.arassetmststatus == "In Approval")
            //{
            //    if (stoid_app <= 0)
            //    {
            //        ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
            //    }
            //}
            //// END VAR UTK APPROVAL

            if (tbl.arassettaxamt > 0)
            {
                if (string.IsNullOrEmpty(tbl.arassetfaktur))
                {
                    ModelState.AddModelError("arassetfaktur", "Silahkan Isi kode faktur!!");
                }
                if (tbl.faoid == 0)
                {
                    ModelState.AddModelError("arassetfaktur", "Silahkan Pilih No. faktur!!");
                }
            }

            // Interface Validation
            if (!ClassFunction.IsInterfaceExists("VAR_AR", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AR"));
            if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
            if (!ClassFunction.IsInterfaceExists("VAR_PPN_OUT", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_OUT"));
            if (!ClassFunction.IsInterfaceExists("VAR_LR_JUAL_FA", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_LR_JUAL_FA"));

            sSql = "SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.custoid + "";
            string custname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            sSql = "SELECT fakturno FROM QL_mstfa WHERE faoid=" + tbl.faoid + "";
            string fakturno = tbl.arassetfaktur + '-' + db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (tbl.arassetmststatus == "Post")
            {
                if (string.IsNullOrEmpty(tbl.arassetno))
                {
                    tbl.arassetno = generateNo(ClassFunction.GetServerTime());
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.arassetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.arassetmststatus = "In Process";
            }
            if (tbl.arassetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.arassetmststatus = "In Process";
                }
            }

            if (tbl.arassetmststatus == "Revised")
                tbl.arassetmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.arassetmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnarassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnarassetdtl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var conaroid = ClassFunction.GenerateID("QL_conar");

                var servertime = ClassFunction.GetServerTime();
                tbl.arassetdatetakegiro = tbl.arassetdate;
                tbl.arassetmstdisctype = "A";
                tbl.arassetmstdiscvalue = 0;
                tbl.arassetmstdiscamt = 0;
                tbl.arassetdeliverycost = 0;
                tbl.arassetothercost = 0;
                tbl.arcloseuser = "";
                tbl.arclosetime = DateTime.Parse("1/1/1900 00:00:00");
                tbl.rateoid = 1;
                tbl.rate2oid = 1;
                tbl.arassetratetoidr = "1";
                tbl.arassetratetousd = "1";
                tbl.arassetrate2toidr = "1";
                tbl.arassetrate2tousd = "1";

                var iAcctgOidAR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR", CompnyCode));
                var iAcctgOidTrans = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", CompnyCode));
                var iAcctgOidLRJualFA = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_LR_JUAL_FA", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.arassetmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.arassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnarassetmst.Add(tbl);
                            db.SaveChanges();

                            //update status fa
                            sSql = "UPDATE QL_mstFA SET status = 'INACTIVE' WHERE faoid = " + tbl.faoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();


                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.arassetmstoid + " WHERE tablename='QL_trnarassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentassetdtl SET shipmentassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetdtloid IN (SELECT shipmentassetdtloid FROM QL_trnarassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arassetmstoid=" + tbl.arassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentassetmst SET shipmentassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid IN (SELECT shipmentassetmstoid FROM QL_trnarassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arassetmstoid=" + tbl.arassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnarassetdtl.Where(a => a.arassetmstoid == tbl.arassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnarassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnarassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnarassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.arassetdtloid = dtloid++;
                            tbldtl.arassetmstoid = tbl.arassetmstoid;
                            tbldtl.arassetdtlseq = i + 1;
                            tbldtl.shipmentassetdtloid = dtDtl[i].shipmentassetdtloid;
                            tbldtl.shipmentassetmstoid = dtDtl[i].shipmentassetmstoid;
                            tbldtl.assetmstoid = dtDtl[i].matrefoid;
                            tbldtl.arassetqty = dtDtl[i].arassetqty;
                            tbldtl.arassetunitoid = dtDtl[i].arassetunitoid;
                            tbldtl.arassetprice = dtDtl[i].arassetprice;
                            tbldtl.arassetdtlamt = dtDtl[i].arassetdtlamt;
                            tbldtl.arassetdtldisctype = dtDtl[i].arassetdtldisctype;
                            tbldtl.arassetdtldiscvalue = dtDtl[i].arassetdtldiscvalue;
                            tbldtl.arassetdtldiscamt = dtDtl[i].arassetdtldiscamt;
                            tbldtl.arassetdtlnetto = dtDtl[i].arassetdtlnetto;
                            tbldtl.arassetdtlstatus = "";
                            tbldtl.arassetdtlnote = (dtDtl[i].arassetdtlnote == null ? "" : dtDtl[i].arassetdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnarassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentassetdtl SET shipmentassetdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetdtloid=" + dtDtl[i].shipmentassetdtloid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnshipmentassetmst SET shipmentassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + dtDtl[i].shipmentassetmstoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnarassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.arassetmststatus == "Post")
                        {
                            decimal artaxamt = Convert.ToDecimal(tbl.arassettaxamt);
                            var shipmentdtl = (from sd in db.QL_trnshipmentassetdtl join ard in db.QL_trnarassetdtl on new { cmpcode = sd.cmpcode, shipmentassetdtloid = sd.shipmentassetdtloid } equals new { cmpcode = ard.cmpcode, shipmentassetdtloid = ard.shipmentassetdtloid } where ard.cmpcode == CompnyCode && ard.arassetmstoid == tbl.arassetmstoid select new { sd.shipmentassetvalueidr, sd.shipmentassetvalueusd, sd.shipmentassetqty, ard.arassetqty }).ToList();
                            decimal shipmentamtidr = 0;
                            shipmentamtidr = Math.Round(shipmentdtl.Sum(x => (x.shipmentassetvalueidr / x.shipmentassetqty) * x.arassetqty), 4);
                            decimal amtLR = (tbl.arassetgrandtotal - (shipmentamtidr + artaxamt));

                            //Insert QL_conar
                            db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, conaroid++, "QL_trnarassetmst", tbl.arassetmstoid, 0, tbl.custoid, iAcctgOidAR, "Post", "ARAS", tbl.arassetdate, ClassFunction.GetDateToPeriodAcctg(tbl.arassetdate), 0, DateTime.Parse("01/01/1900"), "", 0, DateTime.Parse("01/01/1900"), tbl.arassetgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tbl.arassetgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jual Asset|No. " + tbl.arassetno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            // Insert QL_trngldtl
                            // D : Piutang Usaha
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAR, "D", tbl.arassetgrandtotal, tbl.arassetno, custname + " |" + tbl.arassetno + " | " + tbl.arassetmstnote, "Post", Session["UserID"].ToString(), servertime, tbl.arassetgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarassetmst " + tbl.arassetmstoid, null, null, null, 0));
                            db.SaveChanges();

                            if (amtLR < 0)
                            {
                                amtLR = amtLR * (-1);
                                // D : LR FA
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidLRJualFA, "D", amtLR, tbl.arassetno, tbl.arassetno + " | " + custname + " | " + tbl.arassetmstnote, "Post", Session["UserID"].ToString(), servertime, amtLR * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarassetmst " + tbl.arassetmstoid, null, null, null, 0, fakturno));
                                db.SaveChanges();
                            }

                            if (amtLR > 0)
                            {
                                // C : LR FA
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidLRJualFA, "C", amtLR, tbl.arassetno, tbl.arassetno + " | " + custname + " | " + tbl.arassetmstnote, "Post", Session["UserID"].ToString(), servertime, amtLR * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarassetmst " + tbl.arassetmstoid, null, null, null, 0, fakturno));
                                db.SaveChanges();
                            }
                            // C : Piutang Suspend
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "C", shipmentamtidr, tbl.arassetno, custname +" |" + tbl.arassetno + " | " + tbl.arassetmstnote, "Post", Session["UserID"].ToString(), servertime, shipmentamtidr * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarassetmst " + tbl.arassetmstoid, null, null, null, 0));
                            db.SaveChanges();

                            if (artaxamt > 0)
                            {
                                // C : PPN KELUARAN
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "C", artaxamt, tbl.arassetno, tbl.arassetno + " | " + custname + " | " + tbl.arassetmstnote, "Post", Session["UserID"].ToString(), servertime, artaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnarassetmst " + tbl.arassetmstoid, null, null, null, 0, fakturno));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (conaroid - 1) + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //// INSERT APPROVAL
                        //if (tbl.arassetmststatus == "In Approval")
                        //{
                        //    QL_APP tblApp = new QL_APP();
                        //    tblApp.cmpcode = tbl.cmpcode;
                        //    tblApp.appoid = appoid;
                        //    tblApp.appform = ctrlname;
                        //    tblApp.appformoid = tbl.arassetmstoid;
                        //    tblApp.requser = tbl.upduser;
                        //    tblApp.reqdate = servertime;
                        //    tblApp.appstoid = stoid_app;
                        //    tblApp.tablename = "QL_trnpoassetmst";
                        //    db.QL_APP.Add(tblApp);
                        //    db.SaveChanges();

                        //    sSql = "UPDATE QL_mstoid SET lastoid=" + appoid + " WHERE tablename='QL_APP'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}
                        //// END INSERT APPROVAL

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.Message);
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl, action);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: arassetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnarassetmst tbl = db.QL_trnarassetmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //update status fa
                        sSql = "UPDATE QL_mstFA SET status = 'ACTIVE' WHERE faoid = " + tbl.faoid + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentassetdtl SET shipmentassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetdtloid IN (SELECT shipmentassetdtloid FROM QL_trnarassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arassetmstoid=" + tbl.arassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentassetmst SET shipmentassetmststatus='Post' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid IN (SELECT shipmentassetmstoid FROM QL_trnarassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND arassetmstoid=" + tbl.arassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnarassetdtl.Where(a => a.arassetmstoid == id && a.cmpcode == cmp);
                        db.QL_trnarassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnarassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var sReportName = "rptARAssetTrn.rpt";

            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.arassetmstoid IN (" + id + ")");

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ARFixedAssetsPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}