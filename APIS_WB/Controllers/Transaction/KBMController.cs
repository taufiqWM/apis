﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class KBMController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public KBMController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class cashbankgl
        {
            public int cashbankglseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctgdesc2 { get; set; }
            //[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankglamt { get; set; }
            public string cashbankglnote { get; set; }
            public decimal cashbankglamtidr { get; set; }
            public decimal cashbankglamtusd { get; set; }
            public int curroid { get; set; }
        }

        public class trndpar
        {
            public int dparoid { get; set; }
            public string dparno { get; set; }
            public DateTime dpardate { get; set; }
            public int dparacctgoid { get; set; }
            public string acctgdesc { get; set; }
            public string dparnote { get; set; }
            //[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dparamt { get; set; }
            public DateTime dpdateforcheck { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custtaxable { get; set; }
            public decimal custtaxvalue { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabtype { get; set; }
            public int deptoid { get; set; }
            public int curroid { get; set; }
            public string rabmstnote { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;            

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var groupoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.groupoid);
            ViewBag.groupoid = groupoid;
        }

        [HttpPost]
        public ActionResult InitDDLPerson()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m01US> tbl = new List<QL_m01US>();
            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usoid<>'admin' ORDER BY usname";
            tbl = db.Database.SqlQuery<QL_m01US>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLDiv()
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindCustomerData()
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT custoid, custcode, custname, custaddr, (CASE custtaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS custtaxable, 10.00 AS custtaxvalue FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY custcode";
            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabtype, rm.rabmstnote, rm.curroid, rm.deptoid FROM QL_trnrabmst rm WHERE rm.rabmststatus='Approved'";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindDPData(int custoid, int curroid)
        {
            List<trndpar> tbl = new List<trndpar>();
            sSql = "SELECT dp.dparoid, dp.dparno, dpardate, dp.acctgoid AS dparacctgoid, a.acctgdesc, dp.dparnote, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) AS dparamt, (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid";

            tbl = db.Database.SqlQuery<trndpar>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindTotalDP(string action, int curroid, int custoid, int cashbankoid)
        {
            decimal totaldpamt = 0;
            if (action == "Edit")
            {
                sSql = " SELECT ISNULL((SELECT SUM(dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt), 0.0) + cashbankamt FROM QL_trncashbankmst cb WHERE cb.cashbankoid=" + cashbankoid;
            }
            else
            {
                sSql = " SELECT ISNULL(SUM(dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)), 0.0) AS dparamt FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt ";
            }
            totaldpamt = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            return Json(totaldpamt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<cashbankgl> dtDtl, Guid? uid)
        {
            Session["QL_trncashbankglrec"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trncashbankglrec"+ uid] == null)
            {
                Session["QL_trncashbankglrec"+ uid] = new List<cashbankgl>();
            }

            List<cashbankgl> dataDtl = (List<cashbankgl>)Session["QL_trncashbankglrec"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.cashbankrefno = db.Database.SqlQuery<string>("SELECT cashbankrefno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();
            ViewBag.dparno = "";
            ViewBag.totaldpamt = 0;
            ViewBag.cashbankresamt = 0;
            ViewBag.dpdateforcheck = DateTime.Parse("1/1/1900 00:00:00");
            if (tbl.cashbanktype == "BLM")
            {
                ViewBag.dparno = db.Database.SqlQuery<string>("SELECT dparno FROM QL_trndpar WHERE dparoid=" + tbl.giroacctgoid).FirstOrDefault();
                ViewBag.totaldpamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)), 0.0) FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparstatus='Post' AND dp.custoid=" + tbl.refsuppoid + " AND dp.curroid=" + tbl.curroid + " AND dp.dparamt > dp.dparaccumamt").FirstOrDefault() + tbl.cashbankamt;
                ViewBag.cashbankresamt = db.Database.SqlQuery<decimal>("SELECT ISNULL(dparamt - ISNULL(dp.dparaccumamt, 0.0), 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault() + tbl.cashbankamt;
                ViewBag.dpdateforcheck = db.Database.SqlQuery<DateTime>("SELECT (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault();
            }
        }

        private string GenerateReceiptNo2(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateReceiptNo(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No KB" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cb.cashbankoid, cb.cashbankno, cb.cashbankdate, (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BLM' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, p.usname personname, cb.cashbankstatus, cb.cashbanknote, ISNULL(custname, '') AS custname, '' AS CBType,cashbankamt FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_m01us p ON p.usoid=cb.personoid LEFT JOIN QL_mstcust s ON s.custoid=cb.refsuppoid WHERE cb.cashbankgroup='RECEIPT' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                Session["QL_trncashbankglrec"+ ViewBag.uid] = null;
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                Session["lastcashbanktype"+ ViewBag.uid] = tbl.cashbanktype;
                Session["lastdparoid"+ ViewBag.uid] = tbl.giroacctgoid;
                Session["lastdparamt"+ ViewBag.uid] = tbl.cashbankamt;

                sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND cbgl.cmpcode='" + CompnyCode + "' ORDER BY cashbankgloid";
                var tbldtl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].cashbankglseq = i + 1;
                        }
                    }
                }
                Session["QL_trncashbankglrec"+ ViewBag.uid] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string tglmst, string tglduedate, string tgltakegiro, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.cashbankdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.cashbankduedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankduedate", "Format Tanggal Duedate Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.cashbanktakegiro = DateTime.Parse(ClassFunction.toDate(tgltakegiro));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbanktakegiro", "Format Tanggal Take Giro Tidak Valid!!" + ex.ToString());
            }

            tbl.cmpcode = CompnyCode;
            if (tbl.cashbankno == null)
                tbl.cashbankno = "";

            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankglrec"+ uid];
            string sErrReply = "";
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
                    if (tbl.cashbankrefno == "" || tbl.cashbankrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than RECEIPT DATE");
            }
            else if (tbl.cashbanktype == "BLM")
            {
                if (tbl.giroacctgoid == 0)
                    ModelState.AddModelError("", "Please select DP NO. field!");
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault())
                    ModelState.AddModelError("", "PAYMENT DATE must be more than DP DATE!");
            }
            if (tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbanktakegiro > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be less or equal than DUE DATE!");
                if (tbl.cashbankdate > tbl.cashbanktakegiro)
                    ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than RECEIPT DATE!");
                if (tbl.refsuppoid == 0)
                    ModelState.AddModelError("", "Please select CUSTOMER field!");
            }
            if (tbl.personoid.ToString() == "0")
                ModelState.AddModelError("", "Please select PIC field!");
            if (!ClassFunction.isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", tbl.cashbanktaxamt, ref sErrReply))
                ModelState.AddModelError("", "OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" + sErrReply + ") allowed stored in database!");
            if (tbl.cashbankamt < 0)
            {
                ModelState.AddModelError("", "GRAND TOTAL must be more than 0!");
            }
            else
            {
                if (tbl.cashbanktype == "BLM")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        ModelState.AddModelError("", "GRAND TOTAL must be less than DP AMOUNT!");
                    }
                    else
                    {
                        sSql = "SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (action == "Edit")
                            dNewDPBalance += tbl.cashbankamt;
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt)
                            ModelState.AddModelError("", "DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!");
                    }
                }
            }
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply))
                ModelState.AddModelError("", "GRAND TOTAL must be less than MAX GRAND TOTAL (" + sErrReply + ") allowed stored in database!");

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].cashbankglamt == 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be not equal to 0!");
                        }
                        else
                        {
                            if (!ClassFunction.isLengthAccepted("cashbankglamt", "QL_trncashbankgl", dtDtl[i].cashbankglamt, ref sErrReply))
                            {
                                ModelState.AddModelError("", "AMOUNT must be less than MAX AMOUNT (" + sErrReply + ") allowed stored in database!");
                            }
                        }                        
                    }
                }
            }

            //var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKM" || tbl.cashbanktype == "BLM")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = tbl.cashbankduedate;
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBM")
                sDate = tbl.cashbankduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iTaxAcctgOid = 0;
            int iOtherTaxAcctgOid = 0;
            int iGiroAcctgOid = tbl.giroacctgoid;

            if (tbl.cashbankstatus == "Post")
            {
                //if (tbl.cashbanktype == "BLM")
                //    cRate.SetRateValue(db.Database.SqlQuery<int>("SELECT rateoid FROM QL_trndpar WHERE dparoid=" + tbl.giroacctgoid).FirstOrDefault(), (db.Database.SqlQuery<int>("SELECT rate2oid FROM QL_trndpar WHERE dparoid=" + tbl.giroacctgoid).FirstOrDefault()));
                //else
                //    cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cashbankstatus = "In Process";
                //}
                //if (cRate.GetRateMonthlyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cashbankstatus = "In Process";
                //}
                string sVarErr = "";
                if (tbl.cashbanktaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_PPN_OUT", tbl.cmpcode))
                        sVarErr = "VAR_PPN_OUT";
                    else
                        iTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", tbl.cmpcode));
                }
                if (tbl.cashbankothertaxamt > 0)
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_OTHER_TAX_RECEIPT", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_OTHER_TAX_RECEIPT";
                    else
                        iOtherTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_OTHER_TAX_RECEIPT", tbl.cmpcode));
                }
                if (tbl.cashbanktype == "BGM")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO_IN", tbl.cmpcode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO_IN";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", tbl.cmpcode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.cashbankstatus = "In Process";
                }
            }

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.cashbankstatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, sDueDate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.cashbankstatus = "In Process";
            }
            if (tbl.cashbankstatus == "Post")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.cashbankstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                tbl.cmpcode = CompnyCode;
                

                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbankgroup = "RECEIPT";
                //tbl.cashbanktakegiro = (tbl.cashbanktype == "BGM" ? tbl.cashbanktakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.refsuppoid).FirstOrDefault();

                if (tbl.cashbankstatus == "Post")
                    tbl.cashbankno = GenerateReceiptNo2(CompnyCode, tglmst, tbl.cashbanktype, tbl.acctgoid);

                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.cashbankoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (Session["lastcashbanktype"+ uid] != null && Session["lastcashbanktype"+ uid].ToString() == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " + Session["lastdparamt"+ uid] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + Session["lastdparoid"+ uid];
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            if (tbl.cashbanktype == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " + tbl.cashbankamt + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + tbl.giroacctgoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trncashbankgl();
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglgiroflag = "";

                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamt = dtDtl[i].cashbankglamt;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.cashbankglamtusd = 0;
                            tbldtl.cashbankglduedate = (tbl.cashbanktype != "BKM" ? tbl.cashbankduedate : DateTime.Parse("1/1/1900 00:00:00"));
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.groupoid = 0;

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_id SET lastoid=" + dtloid + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Penerimaan Cash/Bank | " + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt < 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", -dtDtl[i].cashbankglamt, tbl.cashbankno, "Penerimaan Cash/Bank | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            if (tbl.cashbanktype == "BGM")
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iGiroAcctgOid, "D", tbl.cashbankamt, tbl.cashbankno, "Penerimaan Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            else
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", tbl.cashbankamt, tbl.cashbankno, "Penerimaan Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].cashbankglamt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "C", dtDtl[i].cashbankglamt, tbl.cashbankno, "Penerimaan Cash/Bank | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iSeq += 1;
                                    gldtloid += 1;
                                }
                            }

                            //if (tbl.cashbanktaxamt > 0)
                            //{
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iTaxAcctgOid, "C", tbl.cashbanktaxamt, tbl.cashbankno, "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbanktaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Tax C/B Receipt No. " + tbl.cashbanknote + "", "M", 0));
                            //    db.SaveChanges();
                            //    iSeq += 1;
                            //    gldtloid += 1;
                            //}

                            if (tbl.cashbankothertaxamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iOtherTaxAcctgOid, "C", tbl.cashbankothertaxamt, tbl.cashbankno, "Penerimaan Cash/Bank | " + tbl.cashbanknote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankothertaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Other Tax C/B Receipt No. " + tbl.cashbanknote + "", "M", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();                        
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dparMaterial/Return/5/11
        [HttpPost, ActionName("ReturGiro")]
        [ValidateAntiForgeryToken]
        public ActionResult ReturGiroConfirmed(int id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            else
            {
                if (tbl.cashbanktype == "BGM")
                {
                    var cekbg = db.QL_trncashbankmst.Where(s => s.cashbankoid == tbl.cashbankoid).FirstOrDefault().cashbankres1;
                    if (cekbg == "Closed")
                    {
                        result = "failed";
                        msg = "- BG Sudah Di cairkan!";
                    }
                }
            }
            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankglrec" + uid];
            if (dtDtl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            else if (dtDtl.Count <= 0)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                //DateTime sDate = tbl.cashbankdate;
                //if (tbl.cashbanktype == "BBM")
                //    sDate = tbl.cashbankduedate;
                //string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
                //DateTime sDueDate = new DateTime();
                //if (tbl.cashbanktype == "BKM" || tbl.cashbanktype == "BLM")
                //    sDueDate = tbl.cashbankdate;
                //else
                //    sDueDate = tbl.cashbankduedate;
                DateTime sDate = ClassFunction.GetServerTime();
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
                var cRate = new ClassRate();
                cRate.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                var iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", tbl.cmpcode));
                var iOtherTaxAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_OTHER_TAX_RECEIPT", tbl.cmpcode));
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl.cashbanktype == "BGM")
                        {
                            sSql = "UPDATE QL_trncashbankmst SET cashbankrefno = cashbankrefno +' Batal', cashbankres1 = 'Closed', updtime='" + servertime + "'  WHERE cashbankoid=" + tbl.cashbankoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //Insert Into GL Mst
                        db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Pembatalan Giro | " + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                        db.SaveChanges();

                        int iSeq = 1;
                        for (var i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].cashbankglamt < 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "C", -dtDtl[i].cashbankglamt, tbl.cashbankno, "Pembatalan Giro | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, -dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                        }

                        if (tbl.cashbanktype == "BGM")
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iGiroAcctgOid, "C", tbl.cashbankamt, tbl.cashbankno, "Pembatalan Giro | " + tbl.cashbanknote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        else
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt, tbl.cashbankno, "Pembatalan Giro | " + tbl.cashbanknote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }

                        for (var i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].cashbankglamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", dtDtl[i].cashbankglamt, tbl.cashbankno, "Pembatalan Giro | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                        }

                        //if (tbl.cashbanktaxamt > 0)
                        //{
                        //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iTaxAcctgOid, "C", tbl.cashbanktaxamt, tbl.cashbankno, "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbanktaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Tax C/B Receipt No. " + tbl.cashbanknote + "", "M", 0));
                        //    db.SaveChanges();
                        //    iSeq += 1;
                        //    gldtloid += 1;
                        //}

                        if (tbl.cashbankothertaxamt > 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iOtherTaxAcctgOid, "D", tbl.cashbankothertaxamt, tbl.cashbankno, "Pembatalan Giro | " + tbl.cashbanknote + " | " + ViewBag.custname + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankothertaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Other Tax C/B Receipt No. " + tbl.cashbanknote + "", "M", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }

                        sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (Session["lastcashbanktype"+ uid] != null && Session["lastcashbanktype" +uid].ToString() == "BLM")
                        {
                            sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " + Session["lastdparamt"+ uid] + " WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + Session["lastdparoid"+ uid];
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == CompnyCode);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptReceipt.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", " WHERE cashbankgroup='RECEIPT' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid IN (" + id + ")");
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ReceiptPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}