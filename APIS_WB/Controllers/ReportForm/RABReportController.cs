﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class RABReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string sWhere = "";

        public RABReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT rm.custoid FROM QL_trnrabmst rm INNER JOIN QL_trnsoitemmst som ON ISNULL(som.rabmstoid,0)=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] status, string custcode, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null; //a
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, rm.rabmstoid [No. Draft], rm.rabno [No. RAB], CONVERT(varchar(10),rm.rabdate,103) [Tgl RAB], rm.projectname [Nama Project], rm.rabmstnote [Note] FROM QL_trnrabmst rm INNER JOIN QL_trnsoitemmst som ON ISNULL(som.rabmstoid,0)=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (custcode != "")
                {
                    string[] arr = custcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY rm.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string ddlnomor, string rabno, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode Barang], m.itemdesc [Nama Barang], g.gndesc [Satuan] FROM QL_mstitem m INNER JOIN QL_trnrabdtl rd ON rd.itemoid=m.itemoid INNER JOIN QL_trnrabmst rm ON rm.cmpcode=rd.cmpcode AND rm.rabmstoid=rd.rabmstoid INNER JOIN QL_trnsoitemmst som ON ISNULL(som.rabmstoid,0)=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabunitoid WHERE rd.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (custcode != "")
                {
                    string[] arr = custcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (rabno != "")
                {
                    string[] arr = rabno.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetSalesData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, s.usoid [User], s.usname [Nama], g.gndesc [Jabatan], s.usaddress [Alamat] FROM QL_m01US s INNER JOIN QL_trnsoitemmst som ON som.salesoid=s.usoid INNER JOIN QL_m05GN g ON gnoid=s.Jabatanoid AND gngroup='JABATAN' Where som.cmpcode='" + CompnyCode + "' AND som.rabmstoid IN(SELECT rm.rabmstoid FROM QL_trnrabmst rm INNER JOIN QL_trnsoitemmst som ON ISNULL(som.rabmstoid,0)=rm.rabmstoid WHERE rm.cmpcode=som.cmpcode ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY s.usname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSales");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string TextSales, string DDLTypeSO, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptRABSumPdf.rpt";
                else
                    rptfile = "rptRABSumPdf.rpt";
                rptname = "RAB_SUMMARY";
            }
            else if (DDLType == "Summary2")
            {
                if (reporttype == "XLS")
                    rptfile = "rptRABRealisasiSumXls.rpt";
                else
                    rptfile = "rptRABRealisasiSumPdf.rpt";
                rptname = "RABRealisasi_SUMMARY";
            }
            else if (DDLType == "SummaryBiaya")
            {
                if (reporttype == "XLS")
                    rptfile = "rptRABSumBiayaXls.rpt";
                else
                    rptfile = "rptRABSumBiayaPdf.rpt";
                rptname = "RABBiayaPenjualan_SUMMARY";
            }
            else if (DDLType == "DetailBiaya")
            {
                if (reporttype == "XLS")
                    rptfile = "rptRABDtlBiayaXls.rpt";
                else
                    rptfile = "rptRABDtlBiayaPdf.rpt";
                rptname = "RABBiayaPenjualan_DETAIL";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptRABDtlPdf.rpt";
                else
                {
                    rptfile = "rptRABDtlPdf.rpt";
                }
                rptname = "RAB_DETAIL";
            }

            var sDtl = " ";
            var sGroup = "";
            var sJoin = "";
            var Dtl = "";
            var Join = "";
            var Dtl2 = "";
            var Join2 = "";
            //var Dtl3 = "";
            //var Join3 = "";
            var Dtl4 = "";
            var Join4 = "";
            var Dtl5 = "";
            var Join5 = "";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sWhere += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sWhere += " AND rm.rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            string sWhereMat = "";
            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhereMat = " AND i.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sWhere += " AND rm.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (DDLType == "Summary2")
            {
                if (reporttype == "XLS")
                {
                    if (DDLTypeSO != "NONE")
                    {
                        if (DDLTypeSO == "LB0")
                        {
                            sWhere += " AND (ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)) > 0 ";
                        }
                        else
                        {
                            sWhere += " AND (ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)) <= 0 ";
                        }
                    }

                    sSql = "SELECT rm.cmpcode, CAST(YEAR(rm.approvaldatetime) AS VARCHAR(30))+'-'+CAST(MONTH(rm.approvaldatetime) AS VARCHAR(30))  periode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=rm.cmpcode) bu, rm.projectname, som.soitemno, us.usname sales, ISNULL((SELECT x.usname FROM QL_m01US x WHERE x.usoid=rm.salesadminoid),'') salesadmin, ISNULL((SELECT x.usname FROM QL_m01US x WHERE x.usoid=rm.creatoroid),'') creator, rm.rabtype, CASE WHEN ISNULL(rm.rabmstres1,'')='' THEN som.soitemtotalamt ELSE 0.0 END sobudget, ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0) soreal, 0.0 AS penerimaan, ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.rabdtl2amt) ELSE 0.0 END FROM QL_trnrabdtl2 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.rabdtl4amt) ELSE 0.0 END FROM QL_trnrabdtl4 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.valueidr * arm.qtyout) ELSE 0.0 END FROM QL_matbooking arm WHERE arm.formoid = rm.rabmstoid),0.0) pobudget, ISNULL((SELECT SUM(CASE WHEN ISNULL(arm.apitemmstres1, '') = '' THEN (arm.apitemtotalamt-arm.apitemtotaldisc) ELSE 0.0 END) FROM QL_trnapitemmst arm INNER JOIN QL_trnpoitemmst px ON px.poitemmstoid=arm.poitemmstoid WHERE ISNULL((px.poitemmstres1),'')='' AND arm.rabmstoid = rm.rabmstoid AND arm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(ard.shipmentitemvalueidr) ELSE 0.0 END FROM QL_trnshipmentitemmst arm INNER JOIN QL_trnshipmentitemdtl ard ON ard.shipmentitemmstoid = arm.shipmentitemmstoid WHERE arm.rabmstoid = rm.rabmstoid AND ISNULL(ard.shipmentitemdtlres2, '') = 'Booking'),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(ard.transformdtl1valueidr) ELSE 0.0 END FROM QL_trntransformmst arm INNER JOIN QL_trntransformdtl1 ard ON ard.transformmstoid = arm.transformmstoid WHERE arm.rabmstoid = rm.rabmstoid AND ISNULL(ard.transformdtl1res2, '') = 'Booking'),0.0)+ISNULL((SELECT SUM(CASE WHEN ISNULL(arm.apassetmstres1, '') = '' THEN (xd.rabdtl2amt) ELSE 0.0 END) FROM QL_trnapassetmst arm INNER JOIN QL_trnapassetdtl ard ON ard.apassetmstoid=arm.poassetmstoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.mrassetdtloid=ard.mrassetdtloid INNER JOIN QL_trnrabdtl2 xd ON ISNULL(xd.poassetmstoid,0)=arm.poassetmstoid AND ISNULL(xd.poassetdtloid,0)=mrd.poassetdtloid WHERE ISNULL(xd.rabmstoid,0) = rm.rabmstoid AND arm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0) poreal, CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN rm.rabgrandtotalcostamt ELSE 0.0 END biayabudget, ISNULL((SELECT SUM(amt) FROM(SELECT CASE WHEN ISNULL((SELECT SUM(armx.kasbon2amt) FROM QL_trnkasbon2mst armx WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group = 'REALISASI' AND armx.kasbon2refoid = arm.kasbon2mstoid AND armx.rabmstoid = arm.rabmstoid), 0.0) = 0 THEN(arm.kasbon2amt + ABS(arm.pphcreditamt)) ELSE 0.0 END amt FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group = 'KASBON')AS tb),0.0) kasbon, ISNULL((SELECT SUM(total) total FROM(SELECT arm.apdirtotalnetto + ABS(arm.pphamt) total FROM QL_trnapdirmst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') UNION ALL  SELECT(arm.kasbon2amt /*+ ABS(arm.pphdebetamt)*/) FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group = 'REALISASI' UNION ALL  SELECT SUM(cb.cashbankamt) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'EXPENSE' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid  UNION ALL  SELECT SUM(cb.cashbankdpp) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'RABREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid  UNION ALL  SELECT SUM(amt) FROM (SELECT CASE ISNULL(cb.cost_type,'') WHEN 'KASBON' THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid),0)>0 THEN /*ISNULL((SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0.0)*/ 0.0 ELSE SUM(gl.cashbankglamt) END) ELSE SUM(gl.cashbankglamt) END amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type,'')<>'KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rm.rabmstoid GROUP BY cb.cashbankoid, cb.cost_type) AS tb  UNION ALL  SELECT SUM(gl.cashbankglamt)amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid = cb.cashbankoid WHERE cb.cashbankgroup = 'RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type, '') = 'KASBON REAL' AND ISNULL(gl.rabmstoid, 0) = rm.rabmstoid AND ISNULL(cb.rabmstoid, 0) = 0) AS kball),0.0) biayareal, CASE WHEN rm.rabtype = 'NON WAPU' THEN rm.rabtotalmargin ELSE 0.0 END margin, CASE WHEN rm.rabtype = 'WAPU' THEN rm.rabtotalmargin ELSE 0.0 END marginwapu, rm.rabpersenmargin persenmargin, ISNULL((SELECT TOP 1 CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN (CASE arm.rabdtl2taxvalue WHEN 0 THEN 0 ELSE (arm.rabdtl2taxvalue/100) END) ELSE 0.0 END FROM QL_trnrabdtl2 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0) ppnpobudget, ISNULL((SELECT TOP 1 CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN (CASE arm.apitemtaxvalue WHEN 0 THEN 0 ELSE (arm.apitemtaxvalue/100) END) ELSE 0.0 END FROM QL_trnapitemmst arm WHERE arm.rabmstoid = rm.rabmstoid),0.0) ppnporeal FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_m01US us ON us.usoid = rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid = rm.rabmstoid WHERE rm.cmpcode = '" + CompnyCode + "' /*AND rm.rabmststatus IN('Post', 'Approved', 'Closed')*/ " + sWhere + " ORDER BY som.soitemno ";
                }
                else
                {
                    if (DDLTypeSO != "NONE")
                    {
                        if (DDLTypeSO == "LB0")
                        {
                            sWhere += " AND (CASE WHEN ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)>0 THEN ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)/1000 ELSE 0.0 END) > 0 ";
                        }
                        else
                        {
                            sWhere += " AND (CASE WHEN ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)>0 THEN ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)/1000 ELSE 0.0 END) <= 0 ";
                        }
                    }

                    sSql = "SELECT rm.cmpcode, CAST(YEAR(rm.approvaldatetime) AS VARCHAR(30))+'-'+CAST(MONTH(rm.approvaldatetime) AS VARCHAR(30))  periode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=rm.cmpcode) bu, rm.projectname, som.soitemno, us.usname sales, ISNULL((SELECT x.usname FROM QL_m01US x WHERE x.usoid=rm.salesadminoid),'') salesadmin, ISNULL((SELECT x.usname FROM QL_m01US x WHERE x.usoid=rm.creatoroid),'') creator, rm.rabtype, CASE WHEN ISNULL(rm.rabmstres1,'')='' THEN (CASE WHEN som.soitemtotalamt>0 THEN som.soitemtotalamt/1000 ELSE 0.0 END) ELSE 0.0 END sobudget, CASE WHEN ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)>0 THEN ISNULL((SELECT SUM(arm.aritemtotalamt) FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed') AND ISNULL(arm.aritemmstres1,'')='' GROUP BY arm.somstoid, arm.aritemmstres1),0.0)/1000 ELSE 0.0 END soreal, 0.0 AS penerimaan, CASE WHEN ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.rabdtl2amt) ELSE 0.0 END FROM QL_trnrabdtl2 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0)+ISNULL((SELECT  CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.rabdtl4amt) ELSE 0.0 END FROM QL_trnrabdtl4 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.valueidr * arm.qtyout) ELSE 0.0 END FROM QL_matbooking arm WHERE arm.formoid = rm.rabmstoid),0.0)> 0 THEN(ISNULL((SELECT  CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.rabdtl2amt) ELSE 0.0 END FROM QL_trnrabdtl2 arm WHERE arm.rabmstoid = rm.rabmstoid), 0.0) + ISNULL((SELECT  CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.rabdtl4amt) ELSE 0.0 END FROM QL_trnrabdtl4 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(arm.valueidr * arm.qtyout) ELSE 0.0 END FROM QL_matbooking arm WHERE arm.formoid = rm.rabmstoid),0.0))/ 1000 ELSE 0.0 END pobudget, CASE WHEN ISNULL((SELECT SUM(CASE WHEN ISNULL(arm.apitemmstres1, '') = '' THEN (arm.apitemtotalamt-arm.apitemtotaldisc) ELSE 0.0 END) FROM QL_trnapitemmst arm INNER JOIN QL_trnpoitemmst px ON px.poitemmstoid=arm.poitemmstoid WHERE ISNULL((px.poitemmstres1),'')='' AND arm.rabmstoid = rm.rabmstoid AND arm.apitemmststatus IN('Post', 'Approved', 'Closed')),0.0)+ ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(ard.shipmentitemvalueidr) ELSE 0.0 END FROM QL_trnshipmentitemmst arm INNER JOIN QL_trnshipmentitemdtl ard ON ard.shipmentitemmstoid = arm.shipmentitemmstoid WHERE arm.rabmstoid = rm.rabmstoid AND ISNULL(ard.shipmentitemdtlres2, '') = 'Booking'),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(ard.transformdtl1valueidr) ELSE 0.0 END FROM QL_trntransformmst arm INNER JOIN QL_trntransformdtl1 ard ON ard.transformmstoid = arm.transformmstoid WHERE arm.rabmstoid = rm.rabmstoid AND ISNULL(ard.transformdtl1res2, '') = 'Booking'),0.0)+ISNULL((SELECT SUM(CASE WHEN ISNULL(arm.apassetmstres1, '') = '' THEN (xd.rabdtl2amt) ELSE 0.0 END) FROM QL_trnapassetmst arm INNER JOIN QL_trnapassetdtl ard ON ard.apassetmstoid=arm.poassetmstoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.mrassetdtloid=ard.mrassetdtloid INNER JOIN QL_trnrabdtl2 xd ON ISNULL(xd.poassetmstoid,0)=arm.poassetmstoid AND ISNULL(xd.poassetdtloid,0)=mrd.poassetdtloid WHERE ISNULL(xd.rabmstoid,0) = rm.rabmstoid AND arm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0)> 0 THEN(ISNULL((SELECT SUM(CASE WHEN ISNULL(arm.apitemmstres1, '') = '' THEN (arm.apitemtotalamt-arm.apitemtotaldisc) ELSE 0.0 END) FROM QL_trnapitemmst arm INNER JOIN QL_trnpoitemmst px ON px.poitemmstoid=arm.poitemmstoid WHERE ISNULL((px.poitemmstres1),'')='' AND arm.rabmstoid = rm.rabmstoid AND arm.apitemmststatus IN('Post', 'Approved', 'Closed')), 0.0) + ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(ard.shipmentitemvalueidr) ELSE 0.0 END FROM QL_trnshipmentitemmst arm INNER JOIN QL_trnshipmentitemdtl ard ON ard.shipmentitemmstoid = arm.shipmentitemmstoid WHERE arm.rabmstoid = rm.rabmstoid AND ISNULL(ard.shipmentitemdtlres2, '') = 'Booking'),0.0)+ISNULL((SELECT CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN SUM(ard.transformdtl1valueidr) ELSE 0.0 END FROM QL_trntransformmst arm INNER JOIN QL_trntransformdtl1 ard ON ard.transformmstoid = arm.transformmstoid WHERE arm.rabmstoid = rm.rabmstoid AND ISNULL(ard.transformdtl1res2, '') = 'Booking'),0.0)+ISNULL((SELECT SUM(CASE WHEN ISNULL(arm.apassetmstres1, '') = '' THEN (xd.rabdtl2amt) ELSE 0.0 END) FROM QL_trnapassetmst arm INNER JOIN QL_trnapassetdtl ard ON ard.apassetmstoid=arm.poassetmstoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.mrassetdtloid=ard.mrassetdtloid INNER JOIN QL_trnrabdtl2 xd ON ISNULL(xd.poassetmstoid,0)=arm.poassetmstoid AND ISNULL(xd.poassetdtloid,0)=mrd.poassetdtloid WHERE ISNULL(xd.rabmstoid,0) = rm.rabmstoid AND arm.apassetmststatus IN('Post', 'Approved', 'Closed')),0.0))/ 1000 ELSE 0.0 END poreal, CASE WHEN rm.rabgrandtotalcostamt > 0 THEN (CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN rm.rabgrandtotalcostamt / 1000 ELSE 0.0 END) ELSE 0.0 END biayabudget, CASE WHEN ISNULL((SELECT SUM(amt) FROM(SELECT CASE WHEN ISNULL((SELECT SUM(armx.kasbon2amt) FROM QL_trnkasbon2mst armx WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group = 'REALISASI' AND armx.kasbon2refoid = arm.kasbon2mstoid AND armx.rabmstoid = arm.rabmstoid), 0.0) = 0 THEN arm.kasbon2amt ELSE 0.0 END amt FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group = 'KASBON')AS tb),0.0)> 0 THEN ISNULL((SELECT SUM(amt) FROM(SELECT CASE WHEN ISNULL((SELECT SUM(armx.kasbon2amt) FROM QL_trnkasbon2mst armx WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group = 'REALISASI' AND armx.kasbon2refoid = arm.kasbon2mstoid AND armx.rabmstoid = arm.rabmstoid), 0.0) = 0 THEN(arm.kasbon2amt + ABS(arm.pphcreditamt)) ELSE 0.0 END amt FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group = 'KASBON')AS tb),0.0)/ 1000 ELSE 0.0 END kasbon, CASE WHEN ISNULL((SELECT SUM(total) total FROM(SELECT arm.apdirtotalnetto + ABS(arm.pphamt) total FROM QL_trnapdirmst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') UNION ALL  SELECT(arm.kasbon2amt /*+ ABS(arm.pphdebetamt)*/) FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group = 'REALISASI' UNION ALL  SELECT SUM(cb.cashbankamt) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'EXPENSE' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid  UNION ALL  SELECT SUM(cb.cashbankdpp) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'RABREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid  UNION ALL  SELECT SUM(amt) FROM (SELECT CASE ISNULL(cb.cost_type,'') WHEN 'KASBON' THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid),0)>0 THEN /*ISNULL((SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0.0)*/ 0.0 ELSE SUM(gl.cashbankglamt) END) ELSE SUM(gl.cashbankglamt) END amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type,'')<>'KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rm.rabmstoid GROUP BY cb.cashbankoid, cb.cost_type) AS tb  UNION ALL  SELECT SUM(gl.cashbankglamt)amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid = cb.cashbankoid WHERE cb.cashbankgroup = 'RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type, '') = 'KASBON REAL' AND ISNULL(gl.rabmstoid, 0) = rm.rabmstoid AND ISNULL(cb.rabmstoid, 0) = 0) AS kball),0.0)> 0 THEN ISNULL((SELECT SUM(total) total FROM(SELECT arm.apdirtotalnetto + ABS(arm.pphamt) total FROM QL_trnapdirmst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') UNION ALL  SELECT(arm.kasbon2amt /*+ ABS(arm.pphdebetamt)*/) FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group = 'REALISASI' UNION ALL  SELECT SUM(cb.cashbankamt) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'EXPENSE' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid  UNION ALL  SELECT SUM(cb.cashbankdpp) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'RABREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid  UNION ALL  SELECT SUM(amt) FROM (SELECT CASE ISNULL(cb.cost_type,'') WHEN 'KASBON' THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid),0)>0 THEN /*ISNULL((SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0.0)*/ 0.0 ELSE SUM(gl.cashbankglamt) END) ELSE SUM(gl.cashbankglamt) END amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type,'')<>'KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rm.rabmstoid GROUP BY cb.cashbankoid, cb.cost_type) AS tb  UNION ALL  SELECT SUM(gl.cashbankglamt)amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid = cb.cashbankoid WHERE cb.cashbankgroup = 'RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type, '') = 'KASBON REAL' AND ISNULL(gl.rabmstoid, 0) = rm.rabmstoid AND ISNULL(cb.rabmstoid, 0) = 0) AS kball),0.0)/ 1000 ELSE 0.0 END biayareal, CASE WHEN rm.rabtype = 'NON WAPU' THEN(CASE WHEN rm.rabtotalmargin > 0 THEN rm.rabtotalmargin / 1000 ELSE 0.0 END) ELSE 0.0 END margin, CASE WHEN rm.rabtype = 'WAPU' THEN(CASE WHEN rm.rabtotalmargin > 0 THEN rm.rabtotalmargin / 1000 ELSE 0.0 END) ELSE 0.0 END marginwapu, rm.rabpersenmargin persenmargin, ISNULL((SELECT TOP 1 CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN (CASE arm.rabdtl2taxvalue WHEN 0 THEN 0 ELSE (arm.rabdtl2taxvalue/100) END) ELSE 0.0 END FROM QL_trnrabdtl2 arm WHERE arm.rabmstoid = rm.rabmstoid),0.0) ppnpobudget, ISNULL((SELECT TOP 1 CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN (CASE arm.apitemtaxvalue WHEN 0 THEN 0 ELSE (arm.apitemtaxvalue/100) END) ELSE 0.0 END FROM QL_trnapitemmst arm WHERE arm.rabmstoid = rm.rabmstoid),0.0) ppnporeal FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_m01US us ON us.usoid = rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid = rm.rabmstoid WHERE rm.cmpcode = '" + CompnyCode + "' /*AND rm.rabmststatus IN('Post', 'Approved', 'Closed')*/ " + sWhere + " ORDER BY som.soitemno ";
                }
            }
            else if (DDLType == "SummaryBiaya")
            {
                sSql = "SELECT rm.rabmstoid, rm.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=rm.cmpcode) bu, rm.projectname, som.soitemno, us.usname sales, rm.rabtype, som.soitemtotalamt sobudget, ISNULL((SELECT arm.aritemtotalamt FROM QL_trnaritemmst arm WHERE arm.somstoid=som.soitemmstoid AND arm.rabmstoid=rm.rabmstoid AND arm.aritemmststatus IN('Post','Approved','Closed')),0.0) soreal, 0.0 AS penerimaan, ISNULL((SELECT SUM(arm.rabdtl2amt) FROM QL_trnrabdtl2 arm WHERE arm.rabmstoid=rm.rabmstoid),0.0)+ISNULL((SELECT SUM(arm.rabdtl4amt) FROM QL_trnrabdtl4 arm WHERE arm.rabmstoid=rm.rabmstoid),0.0) pobudget, ISNULL((SELECT SUM(arm.apitemtotalamt) FROM QL_trnapitemmst arm WHERE arm.rabmstoid=rm.rabmstoid AND arm.apitemmststatus IN('Post','Approved','Closed')),0.0) poreal, CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN rm.rabgrandtotalcostamt ELSE 0.0 END biayabudget, ISNULL((SELECT SUM(amt) FROM(SELECT CASE WHEN ISNULL((SELECT SUM(armx.kasbon2amt) FROM QL_trnkasbon2mst armx WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.kasbon2refoid=arm.kasbon2mstoid AND armx.rabmstoid=arm.rabmstoid),0.0)=0 THEN (arm.kasbon2amt + ABS(arm.pphcreditamt)) ELSE 0.0 END amt FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON')AS tb),0.0) kasbon, ISNULL((SELECT SUM(total) total FROM(SELECT arm.apdirtotalnetto + ABS(arm.pphamt) total FROM QL_trnapdirmst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') UNION ALL  SELECT ISNULL((SELECT SUM(cd.kasbon2dtlamt) FROM QL_trnkasbon2dtl cd WHERE cd.kasbon2mstoid=arm.kasbon2mstoid),0.0) FROM QL_trnkasbon2mst arm WHERE arm.rabmstoid = rm.rabmstoid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='REALISASI' UNION ALL  SELECT SUM(cb.cashbankamt) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup='EXPENSE' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid,0)=rm.rabmstoid  UNION ALL  SELECT SUM(cb.cashbankdpp) FROM QL_trncashbankmst cb WHERE cb.cashbankgroup = 'RABREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.rabmstoid, 0) = rm.rabmstoid UNION ALL  SELECT SUM(amt) FROM (SELECT CASE ISNULL(cb.cost_type,'') WHEN 'KASBON' THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=cb.cashbankoid),0)>0 THEN /*ISNULL((SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(kasbon_ref_id,0)=cb.cashbankoid),0.0)*/ 0.0 ELSE SUM(gl.cashbankglamt) END) ELSE SUM(gl.cashbankglamt) END amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type,'')<>'KASBON REAL' AND ISNULL(gl.rabmstoid,0)=rm.rabmstoid GROUP BY cb.cashbankoid, cb.cost_type) AS tb  UNION ALL  SELECT SUM(gl.cashbankglamt)amt FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid = cb.cashbankoid WHERE cb.cashbankgroup = 'RABMULTIREAL' AND cb.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(cb.cost_type, '') = 'KASBON REAL' AND ISNULL(gl.rabmstoid, 0) = rm.rabmstoid AND /*ISNULL(cb.rabmstoid, 0) = 0*/) AS kball),0.0) biayareal, CASE WHEN rm.rabtype = 'NON WAPU' THEN rm.rabtotalmargin ELSE 0.0 END margin, CASE WHEN rm.rabtype = 'WAPU' THEN rm.rabtotalmargin ELSE 0.0 END marginwapu, rm.rabpersenmargin persenmargin FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_m01US us ON us.usoid = rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid = rm.rabmstoid WHERE rm.cmpcode = '" + CompnyCode + "' /*AND rm.rabmststatus IN('Post', 'Approved', 'Closed')*/ " + sWhere + " ORDER BY  som.soitemno ";
            }
            else if (DDLType == "DetailBiaya")
            {
                sSql = "SELECT rm.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=rm.cmpcode) bu, rm.rabmstoid, rm.projectname, ISNULL(som.soitemno,'') soitemno, us.usname sales, rm.rabtype, CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN rm.rabgrandtotalcostamt ELSE 0.0 END rabgrandtotalcostamt, CASE WHEN ISNULL(rm.rabmstres1, '') = '' THEN rd.rabdtl5netto ELSE 0.0 END netto, ISNULL(transno,'') transno, ISNULL(CAST(DAY(transdate) AS VARCHAR(10))+' '+ (CASE MONTH(transdate) WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8 THEN 'Agustus' WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'Nopember' ELSE 'Desember' END) +' '+ CAST(YEAR(transdate) AS VARCHAR(10)),'') transdate, ISNULL(total,0.0) total, ISNULL(tipe,'') tipe, a.acctgdesc akun, ISNULL(notedtl,'') notedtl FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_m01US us ON us.usoid = rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid = rm.rabmstoid INNER JOIN QL_trnrabdtl5 rd ON rd.rabmstoid=rm.rabmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=rd.acctgoid LEFT JOIN( SELECT arm.rabmstoid rabmstoid, arm.apdirno transno, arm.apdirdate transdate, ard.apdirprice total, 'AP DIRECT' tipe, ard.apdirdtlnote notedtl, ard.acctgoid acctgoid, ard.acctgoid acctgoid2 FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.apdirmststatus IN('Post', 'Approved', 'Closed')  UNION ALL  SELECT arm.rabmstoid, CASE WHEN ISNULL(re.realno,'')='' THEN arm.kasbon2no ELSE realno END transno, CASE WHEN ISNULL(re.realno,'')='' THEN arm.kasbon2date ELSE realdate END transdate, CASE WHEN ISNULL(re.realno,'')='' THEN  ard.kasbon2dtlamt ELSE realamt END total, CASE WHEN ISNULL(re.realno,'')='' THEN arm.kasbon2group ELSE realtipe END tipe, CASE WHEN ISNULL(re.realno,'')='' THEN ard.kasbon2dtlnote ELSE realnotedtl END notedtl, ard.acctgoid, ard.acctgoid acctgoid2 FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid = arm.kasbon2mstoid LEFT JOIN( SELECT arm2.rabmstoid realrab, arm2.kasbon2refoid, arm2.kasbon2no realno, arm2.kasbon2date realdate, ard2.kasbon2dtlamt realamt, arm2.kasbon2group realtipe, ard2.kasbon2dtlnote realnotedtl FROM QL_trnkasbon2mst arm2 INNER JOIN QL_trnkasbon2dtl ard2 ON ard2.kasbon2mstoid = arm2.kasbon2mstoid WHERE arm2.kasbon2mststatus IN('Post', 'Approved', 'Closed')) AS re ON re.kasbon2refoid = arm.kasbon2mstoid AND re.realrab = arm.rabmstoid WHERE arm.kasbon2mststatus IN('Post', 'Approved', 'Closed')  AND arm.kasbon2group = 'KASBON' UNION ALL  SELECT arm.rabmstoid rabmstoid, arm.cashbankno transno, arm.cashbankdate transdate, ard.cashbankglamt total, 'KAS/BANK' tipe, ard.cashbankglnote notedtl, ard.acctgoid acctgoid, ard.acctgoid acctgoid2 FROM QL_trncashbankmst arm INNER JOIN QL_trncashbankgl ard ON ard.cashbankoid=arm.cashbankoid WHERE arm.cashbankgroup='EXPENSE' AND arm.cashbankstatus IN('Post', 'Approved', 'Closed')  UNION ALL  SELECT arm.rabmstoid rabmstoid, arm.cashbankno transno, arm.cashbankdate transdate, ard.cashbankglamt total, 'RAB Realisasi' tipe, ard.cashbankglnote notedtl, ard.acctgoid acctgoid, rd.acctgoid acctgoid2 FROM QL_trncashbankmst arm INNER JOIN QL_trncashbankgl ard ON ard.cashbankoid=arm.cashbankoid INNER JOIN QL_trnrabdtl5 rd ON rd.rabdtl5oid=ISNULL(arm.rabdtl5oid,0) WHERE arm.cashbankgroup='RABREAL' AND arm.cashbankstatus IN('Post', 'Approved', 'Closed') UNION ALL  SELECT ard.rabmstoid rabmstoid, arm.cashbankno transno, CASE WHEN (SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)>0 THEN /*(SELECT TOP 1 x.cashbankdate FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)*/ '1900-01-01' ELSE arm.cashbankdate END transdate, CASE WHEN (SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)>0 THEN /*(SELECT SUM(xd.cashbankglamt) FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)*/ 0.0 ELSE ard.cashbankglamt END total, 'Multi RAB' tipe, CASE WHEN (SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)>0 THEN /*(SELECT TOP 1 xd.cashbankglnote FROM QL_trncashbankmst x INNER JOIN QL_trncashbankgl xd ON xd.cashbankoid=x.cashbankoid WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)*/ '' ELSE ard.cashbankglnote END notedtl, ard.acctgoid acctgoid, rd.acctgoid acctgoid2 FROM QL_trncashbankmst arm INNER JOIN QL_trncashbankgl ard ON ard.cashbankoid=arm.cashbankoid INNER JOIN QL_trnrabdtl5 rd ON rd.rabdtl5oid=ISNULL(ard.rabdtl5oid,0) WHERE arm.cashbankgroup='RABMULTIREAL' AND arm.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(arm.cost_type,'') NOT IN('KASBON REAL') AND (SELECT COUNT(*) FROM QL_trncashbankmst x WHERE ISNULL(x.kasbon_ref_id,0)=arm.cashbankoid)=0  UNION ALL  SELECT ard.rabmstoid rabmstoid, arm.cashbankno transno, arm.cashbankdate transdate, ard.cashbankglamt total, 'Multi RAB' tipe, ard.cashbankglnote notedtl, ard.acctgoid acctgoid, rd.acctgoid acctgoid2 FROM QL_trncashbankmst arm INNER JOIN QL_trncashbankgl ard ON ard.cashbankoid = arm.cashbankoid INNER JOIN QL_trnrabdtl5 rd ON rd.rabdtl5oid = ISNULL(ard.rabdtl5oid, 0) AND rd.rabmstoid = ISNULL(ard.rabmstoid, 0) WHERE arm.cashbankgroup = 'RABMULTIREAL' AND arm.cashbankstatus IN('Post', 'Approved', 'Closed') AND ISNULL(arm.cost_type, '') IN('KASBON REAL') /*AND ISNULL(arm.rabmstoid, 0)= 0*/ )AS bp ON bp.rabmstoid = rm.rabmstoid AND bp.acctgoid2 = a.acctgoid WHERE rm.cmpcode = '" + CompnyCode + "' /*AND rm.rabmststatus IN('Post', 'Approved', 'Closed')*/ " + sWhere + " ORDER BY som.soitemno ";
            }
            else
            {
                if (DDLType == "Detail")
                {
                    sDtl = " *, CASE WHEN ram.rabtype='WAPU' THEN (ram.rabtotaljualamt - ram.rabtotaljualpphamt) ELSE (ram.rabtotaljualamt + ram.rabtotaljualtaxamt) END [Grand Total Jual], CASE WHEN ram.rabtype='WAPU' THEN (ram.rabtotaljualamt - ram.rabtotaljualpphamt) ELSE (ram.rabtotaljualamt) END [Grand Total Jual DPP], ram.rabtotalbeliamt + ram.rabtotalbelitaxamt AS [Grand Total Beli], ram.rabtotalbeliamt AS [Grand Total Beli DPP], ram.rabgrandtotaljualjasaamt AS [Grand Total Jual Jasa], ram.rabgrandtotalbelijasaamt + ram.rabtotalbelijasataxamt [Grand Total Beli Jasa], ram.rabgrandtotalbelijasaamt [Grand Total Beli Jasa DPP], ram.rabgrandtotalcostamt AS [Grand Total Biaya], ram.rabpersenmargin AS [Persen Margin], ram.rabtotalmargin AS [Total Margin] ";
                    Dtl = " ,rd.rabseq [Seq], i.itemcode [Kode], i.itemdesc [Deskripsi], rd.rabqty [Qty], g.gndesc [Satuan], rd.rabprice [Harga], rd.rabdtlamt [DPP], rd.rabtaxamt [PPN], rd.rabdtlamt + rd.rabtaxamt [DPP + PPN], rd.rabpphamt [PPH], rd.rabdtlamt - rd.rabpphamt [NETT], rd.rabetd [ETA/ETD], rd.rabdtlnote [Dtl Note], c.custname suppname, '' paytype ";
                    Join = " INNER JOIN QL_trnrabdtl rd ON rd.rabmstoid = rm.rabmstoid INNER JOIN QL_mstitem i ON i.itemoid = rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid ";
                    Dtl2 = " , rd2.rabdtl2seq [Seq],i.itemcode [Kode], i.itemdesc [Deskripsi], CASE WHEN rd2.rabdtl2qty = 0  THEN ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=rd2.itemdtl2oid AND mb.formoid=rd2.rabmstoid),0.0) ELSE rd2.rabdtl2qty END [Qty], g2.gndesc [Satuan], CASE WHEN rd2.rabdtl2qty = 0  THEN  ISNULL((SELECT TOP 1 mb.valueidr FROM QL_matbooking mb WHERE mb.refoid=rd2.itemdtl2oid AND mb.formoid=rd2.rabmstoid),0.0) ELSE rd2.rabdtl2price END [Harga], CASE WHEN rd2.rabdtl2qty = 0  THEN ISNULL((SELECT SUM(mb.valueidr * mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=rd2.itemdtl2oid AND mb.formoid=rd2.rabmstoid),0.0) ELSE rd2.rabdtl2amt END [DPP], CASE WHEN rd2.rabdtl2qty = 0  THEN  ISNULL((SELECT SUM(mb.taxamt) FROM QL_matbooking mb WHERE mb.refoid=rd2.itemdtl2oid AND mb.formoid=rd2.rabmstoid),0.0) ELSE rd2.rabdtl2taxamt END [PPN], CASE WHEN rd2.rabdtl2qty = 0  THEN  ISNULL((SELECT SUM(mb.amtnetto) FROM QL_matbooking mb WHERE mb.refoid=rd2.itemdtl2oid AND mb.formoid=rd2.rabmstoid),0.0) ELSE (rd2.rabdtl2amt + rabdtl2taxamt) END [DPP + PPN], 0.0 [PPH], 0.0 [NETT], rd2.rabdtl2eta [ETA/ETD], rd2.rabdtl2note [Dtl Note], s.suppname, gn.gndesc paytype ";
                    Join2 = " INNER JOIN QL_trnrabdtl2 rd2 ON rd2.rabmstoid = rm.rabmstoid INNER JOIN QL_mstitem i ON i.itemoid = rd2.itemdtl2oid INNER JOIN QL_m05GN g2 ON g2.gnoid = i.itemunitoid INNER JOIN QL_mstsupp s ON s.suppoid = rd2.suppdtl2oid INNER JOIN QL_m05gn gn ON gn.gnoid = s.supppaymentoid";
                    //Dtl3 = " ,rd.rabdtl3seq, i.jasacode, i.jasadesc, rd.rabdtl3qty, g.gndesc itemunit, rd.rabdtl3price, rd.rabdtl3amt, rd.rabdtl3taxamt, rd.rabdtl3amt + rd.rabdtl3taxamt, rd.rabdtl3etd, rd.rabdtl3note, c.custname suppname, '' paytype ";
                    //Join3 = " INNER JOIN QL_trnrabdtl3 rd ON rd.rabmstoid = rm.rabmstoid INNER JOIN QL_mstjasa i ON i.jasaoid = rd.itemdtl3oid INNER JOIN QL_m05GN g ON g.gnoid = i.jasaunitoid ";
                    Dtl4 = " ,rd2.rabdtl4seq,i2.itemcode, i2.itemdesc, rd2.rabdtl4qty, g2.gndesc itemunit2, rd2.rabdtl4price, rd2.rabdtl4amt, rd2.rabdtl4taxamt, rd2.rabdtl4amt + rd2.rabdtl4taxamt, 0.0 [PPH], 0.0 [NETT], rd2.rabdtl4eta, rd2.rabdtl4note, '' suppname, '' paytype ";
                    Join4 = " INNER JOIN QL_trnrabdtl4 rd2 ON rd2.rabmstoid = rm.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = rd2.suppdtl4oid INNER JOIN QL_mstitem i2 ON i2.itemoid = rd2.itemdtl4oid INNER JOIN QL_m05GN g2 ON g2.gnoid = i2.itemunitoid ";
                    Dtl5 = " ,rd5.rabdtl5seq,a.acctgcode, a.acctgdesc, rd5.rabdtl5qty, '' itemunit2, rd5.rabdtl5price, rd5.rabdtl5netto rabdtl5amt, 0 rabdtl5taxamt, rd5.rabdtl5netto, 0.0 [PPH], 0.0 [NETT], '' rabdtl5eta, rd5.rabdtl5note, '' suppname, '' paytype ";
                    Join5 = " INNER JOIN QL_trnrabdtl5 rd5 ON rd5.rabmstoid = rm.rabmstoid INNER JOIN QL_mstacctg a ON a.acctgoid = rd5.acctgoid ";
                }
                else
                {
                    sDtl = " DISTINCT ID, [Draft/RAB No], [RAB No.], [RAB Date], [RAB Type], [RAB Type2], [Trans Date], BU, Project, [Cust Name], [TOP], [RAB Status], [Cmp Code], [Header Note], [App User], [App Date], [Create User], [Create Date], SUM([Grand Total Jual]) [Grand Total Jual], SUM([Grand Total Beli]) AS [Grand Total Beli], SUM([Grand Total Jual Jasa]) AS [Grand Total Jual Jasa], SUM([Grand Total Beli Jasa]) [Grand Total Beli Jasa], SUM([Grand Total Biaya]) AS [Grand Total Biaya] ";
                    sGroup = " GROUP BY ID, [Draft/RAB No], [RAB No.], [RAB Date], [RAB Type], [RAB Type2], [Trans Date], BU, Project, [Cust Name], [TOP], [RAB Status], [Cmp Code], [Header Note], [App User], [App Date], [Create User], [Create Date] ";
                    sDtl = " DISTINCT ID, [Draft/RAB No], [RAB No.], [RAB Date], [RAB Type], [RAB Type2], [Trans Date], BU, Project, [Cust Name], [TOP], [RAB Status], [Cmp Code], [Header Note], [App User], [App Date], [Create User], [Create Date], ram.rabgrandtotaljualamt [Grand Total Jual], ram.rabgrandtotalbeliamt AS [Grand Total Beli], ram.rabgrandtotaljualjasaamt AS [Grand Total Jual Jasa], ram.rabgrandtotalbelijasaamt [Grand Total Beli Jasa], ram.rabgrandtotalcostamt AS [Grand Total Biaya], ram.rabpersenmargin AS [Persen Margin], ram.rabtotalmargin AS [Total Margin] ";
                    sGroup = "";
                }
                sJoin = " INNER JOIN QL_trnrabmst ram ON ram.rabmstoid = rab.ID ";
                sSql = "SELECT " + sDtl + " FROM (";
                sSql += "SELECT 1 raburut,'PENJUALAN' rabtype, rm.rabmstoid AS [ID], (CASE rm.rabno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), rm.rabmstoid) ELSE rm.rabno END) AS [Draft/RAB No], rm.rabno AS [RAB No.], rm.rabdate AS [RAB Date], rm.rabtype AS [RAB Type], rm.rabtype2 [RAB Type2], rm.updtime AS [Trans Date], '" + CompnyName + "' AS [BU], rm.projectname AS [Project], c.custcode+' - '+c.custname AS [Cust Name], g1.gndesc AS [TOP], rm.rabmststatus AS [RAB Status], rm.cmpcode [Cmp Code], rm.rabmstnote AS [Header Note], rm.approvaluser AS [App User], rm.approvaldatetime AS [App Date], rm.createuser AS [Create User], rm.createtime AS [Create Date]/*,rm.rabtotaljualamt AS [Total Jual], rabtotaljualtaxamt AS [Total Jual Tax], rabtotaljualpphamt [Total Jual Pph], rm.rabgrandtotaljualamt AS [Grand Total Jual], 0.0 AS [Total Beli], 0.0 AS [Total Beli Tax], 0 [Total Beli Pph], 0.0 AS [Grand Total Beli], 0.0 AS [Total Jual Jasa], 0.0 AS [Total Jual Jasa Tax], 0.0 [Total Jual Jasa Pph], 0.0 AS [Grand Total Jual Jasa], 0.0 AS [Total Beli Jasa], 0.0 AS [Total Beli Jasa Tax], 0.0 [Total Beli Jasa Pph], 0.0 AS [Grand Total Beli Jasa], 0.0 AS [Total Biaya], 0 AS [Total Biaya Tax], 0 [Total Biaya Pph], 0.0 AS [Grand Total Biaya], rm.rabpersenmargin AS [Persen Margin], rm.rabtotalmargin AS [Total Margin]*/ " + Dtl + " FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.custpaymentoid " + Join + " " + sWhere + " " + sWhereMat + "";
                sSql += " UNION ALL";
                sSql += " SELECT 2 raburut, 'PEMBELIAN' rabtype, rm.rabmstoid AS [ID], (CASE rm.rabno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), rm.rabmstoid) ELSE rm.rabno END) AS [Draft/RAB No], rm.rabno AS [RAB No.], rm.rabdate AS [RAB Date], rm.rabtype AS [RAB Type], rm.rabtype2 [RAB Type2], rm.updtime AS [Trans Date], '" + CompnyName + "' AS [BU], rm.projectname AS [Project], c.custcode+' - '+c.custname AS [Cust Name], g1.gndesc AS [TOP], rm.rabmststatus AS [RAB Status], rm.cmpcode [Cmp Code], rm.rabmstnote AS [Header Note], rm.approvaluser AS [App User], rm.approvaldatetime AS [App Date], rm.createuser AS [Create User], rm.createtime AS [Create Date]/*, 0.0 AS [Total Jual], 0.0 AS [Total Jual Tax], 0.0 [Total Jual Pph], 0.0 AS [Grand Total Jual], rm.rabtotalbeliamt AS [Total Beli], rabtotalbelitaxamt AS [Total Beli Tax], 0 [Total Beli Pph], rm.rabgrandtotalbeliamt AS [Grand Total Beli], 0.0 AS [Total Jual Jasa], 0.0 AS [Total Jual Jasa Tax], 0.0 [Total Jual Jasa Pph], 0.0 AS [Grand Total Jual Jasa], 0.0 AS [Total Beli Jasa], 0.0 AS [Total Beli Jasa Tax], 0 [Total Beli Jasa Pph], 0.0 AS [Grand Total Beli Jasa], 0.0 AS [Total Biaya], 0 AS [Total Biaya Tax], 0 [Total Biaya Pph], 0.0 AS [Grand Total Biaya], rm.rabpersenmargin AS [Persen Margin], rm.rabtotalmargin AS [Total Margin]*/ " + Dtl2 + " FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON rm.custoid = c.custoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.custpaymentoid " + Join2 + " " + sWhere + " " + sWhereMat + "";
                //sSql += " UNION ALL";
                //sSql += " SELECT 3 raburut,'PENJUALAN JASA' rabtype, rm.rabmstoid AS [ID], (CASE rm.rabno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), rm.rabmstoid) ELSE rm.rabno END) AS [Draft/RAB No], rm.rabno AS [RAB No.], rm.rabdate AS [RAB Date], rm.rabtype AS [RAB Type], rm.rabtype2 [RAB Type2], rm.updtime AS [Trans Date], '" + CompnyName + "' AS [BU], rm.projectname AS [Project], c.custcode+' - '+c.custname AS [Cust Name], g1.gndesc AS [TOP], rm.rabmststatus AS [RAB Status], rm.cmpcode [Cmp Code], rm.rabmstnote AS [Header Note], rm.approvaluser AS [App User], rm.approvaldatetime AS [App Date], rm.createuser AS [Create User], rm.createtime AS [Create Date]/*, 0.0 AS [Total Jual], 0.0 AS [Total Jual Tax], 0.0 [Total Jual Pph], 0.0 AS [Grand Total Jual], 0.0 AS [Total Beli], 0.0 AS [Total Beli Tax], 0 [Total Beli Pph], 0.0 AS [Grand Total Beli], rm.rabtotaljualjasaamt AS [Total Jual Jasa], rabtotaljualjasataxamt AS [Total Jual Jasa Tax], rabtotaljualjasapphamt [Total Jual Jasa Pph], rm.rabgrandtotaljualjasaamt AS [Grand Total Jual Jasa], 0.0 AS [Total Beli Jasa], 0.0 AS [Total Beli Jasa Tax], 0 [Total Beli Jasa Pph], 0.0 AS [Grand Total Beli Jasa], 0.0 AS [Total Biaya], 0 AS [Total Biaya Tax], 0 [Total Biaya Pph], 0.0 AS [Grand Total Biaya], rm.rabpersenmargin AS [Persen Margin], rm.rabtotalmargin AS [Total Margin]*/ " + Dtl3 + " FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid = rm.custoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.custpaymentoid " + Join3 + " " + sWhere + "";
                sSql += " UNION ALL";
                sSql += " SELECT 4 raburut, 'PEMBELIAN JASA' rabtype, rm.rabmstoid AS [ID], (CASE rm.rabno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), rm.rabmstoid) ELSE rm.rabno END) AS [Draft/RAB No], rm.rabno AS [RAB No.], rm.rabdate AS [RAB Date], rm.rabtype AS [RAB Type], rm.rabtype2 [RAB Type2], rm.updtime AS [Trans Date], '" + CompnyName + "' AS [BU], rm.projectname AS [Project], c.custcode+' - '+c.custname AS [Cust Name], g1.gndesc AS [TOP], rm.rabmststatus AS [RAB Status], rm.cmpcode [Cmp Code], rm.rabmstnote AS [Header Note], rm.approvaluser AS [App User], rm.approvaldatetime AS [App Date], rm.createuser AS [Create User], rm.createtime AS [Create Date]/*, 0.0 AS [Total Jual], 0.0 AS [Total Jual Tax], 0.0 [Total Jual Pph], 0.0 AS [Grand Total Jual], 0.0 AS [Total Beli], 0.0 AS [Total Beli Tax], 0 [Total Beli Pph], 0.0 AS [Grand Total Beli], 0.0 AS [Total Jual Jasa], 0.0 AS [Total Jual Jasa Tax], 0.0 [Total Jual Jasa Pph], 0.0 AS [Grand Total Jual Jasa],rm.rabtotalbelijasaamt AS [Total Beli Jasa], rabtotalbelijasataxamt AS [Total Beli Jasa Tax], 0 [Total Beli Jasa Pph], rm.rabgrandtotalbelijasaamt AS [Grand Total Beli Jasa], 0.0 AS [Total Biaya], 0 AS [Total Biaya Tax], 0 [Total Biaya Pph], rm.rabgrandtotalcostamt AS [Grand Total Biaya], rm.rabpersenmargin AS [Persen Margin], rm.rabtotalmargin AS [Total Margin]*/ " + Dtl4 + " FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON rm.custoid = c.custoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.custpaymentoid " + Join4 + " " + sWhere + "";
                sSql += " UNION ALL";
                sSql += " SELECT 5 raburut, 'BIAYA' rabtype, rm.rabmstoid AS [ID], (CASE rm.rabno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), rm.rabmstoid) ELSE rm.rabno END) AS [Draft/RAB No], rm.rabno AS [RAB No.], rm.rabdate AS [RAB Date], rm.rabtype AS [RAB Type], rm.rabtype2 [RAB Type2], rm.updtime AS [Trans Date], '" + CompnyName + "' AS [BU], rm.projectname AS [Project], c.custcode+' - '+c.custname AS [Cust Name], g1.gndesc AS [TOP], rm.rabmststatus AS [RAB Status], rm.cmpcode [Cmp Code], rm.rabmstnote AS [Header Note], rm.approvaluser AS [App User], rm.approvaldatetime AS [App Date], rm.createuser AS [Create User], rm.createtime AS [Create Date]/*, 0.0 AS [Total Jual], 0.0 AS [Total Jual Tax], 0.0 [Total Jual Pph], 0.0 AS [Grand Total Jual], 0.0 AS [Total Beli], 0.0 AS [Total Beli Tax], 0 [Total Beli Pph], 0.0 AS [Grand Total Beli], 0.0 AS [Total Jual Jasa], 0.0 AS [Total Jual Jasa Tax], 0.0 [Total Jual Jasa Pph], 0.0 AS [Grand Total Jual Jasa], 0.0 AS [Total Beli Jasa], 0.0 AS [Total Beli Jasa Tax], 0 [Total Beli Jasa Pph], 0.0 AS [Grand Total Beli Jasa],rm.rabtotalcostamt AS [Total Biaya], 0 AS [Total Biaya Tax], 0 [Total Biaya Pph], rm.rabgrandtotalcostamt AS [Grand Total Biaya], rm.rabpersenmargin AS [Persen Margin], rm.rabtotalmargin AS [Total Margin]*/ " + Dtl5 + " FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON rm.custoid = c.custoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.custpaymentoid " + Join5 + " " + sWhere + "";
                sSql += " ) RAB " + sGroup + " " + sJoin + "";

                if (DDLType == "Detail")
                {
                    sSql += " ORDER BY " + DDLSorting + ", raburut " + DDLSortDir + "";
                }
                else
                {
                    sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
                }
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail" || DDLType == "SummaryBiaya")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else if (DDLType == "DetailBiaya")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }

                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail" || DDLType == "SummaryBiaya")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else if (DDLType == "DetailBiaya")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }

        public class ReportFilter
        {
            public string ddltype { get; set; }
            public string[] ddlstatus { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filtersupp { get; set; }
            public string filterpo { get; set; }
            public string filterrab { get; set; }
            public string filtermat { get; set; }
        }

        [HttpPost]
        public ActionResult GetModalData(ReportFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                if (modaltype.ToLower() == "supp") sSql = $"SELECT DISTINCT 0 seq, suppcode [Kode], suppname [Nama], suppaddr [Alamat]";
                else if (modaltype.ToLower() == "po") sSql = $"SELECT DISTINCT 0 seq, poassetno [No. PO], poassetmstoid [No. Draft], FORMAT(poassetdate, 'dd/MM/yyyy') [Tgl. PO], poassetmststatus [Status], poassetmstnote [Catatan]";
                else if (modaltype.ToLower() == "mat") sSql = $"SELECT DISTINCT 0 seq, itemcode [Kode], itemdesc [Deskripsi]";

                sSql += $" FROM QL_v_report_po_asset WHERE 1=1";
                sSql += $" AND poassetdate >= CAST('{param.periodstart} 00:00:00' AS DATETIME) AND poassetdate <= CAST('{param.periodend} 23:59:59' AS DATETIME)";
                if (param.ddlstatus != null)
                {
                    if (param.ddlstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < param.ddlstatus.Count(); i++) stsval += "'" + param.ddlstatus[i] + "',";
                        sSql += " AND poassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(param.filtersupp) && modaltype.ToLower() != "supp")
                {
                    string[] arr = param.filtersupp.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (!string.IsNullOrEmpty(param.filterpo) && modaltype.ToLower() != "po")
                {
                    string[] arr = param.filterpo.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND poassetno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (!string.IsNullOrEmpty(param.filtermat) && modaltype.ToLower() != "mat")
                {
                    string[] arr = param.filtermat.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
    }
}