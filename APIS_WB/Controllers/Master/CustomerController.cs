﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class CustomerController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        string custktppath = "~/Images/CustKTP";
        private string custnpwppath = "~/Images/CustNPWP";
        private string imgtemppath = "~/Images/ImagesTemps";

        public CustomerController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class custmst
        {
            public string cmpcode { get; set; }
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custaddr { get; set; }
            public string custphone1 { get; set; }
            public string custemail { get; set; }
            public String custname { get; set; }
            public string activeflag { get; set; }
        }

        public class custdtl1
        {
            public int custdtl1oid { get; set; }
            public int custdtl1seq { get; set; }
            public string custdtl1picname { get; set; }
            public string custdtl1phone1 { get; set; }
            public string custdtl1phone2 { get; set; }
            public string custdtl1email { get; set; }
            public string custdtl1jabatan { get; set; }
        }

        public class custdtl2
        {
            public int custdtl2oid { get; set; }
            public int custdtl2seq { get; set; }
            public string custdtl2loc { get; set; }
            public string custdtl2addr { get; set; }
            public int custdtl2cityoid { get; set; }            
            public string custdtl2postcode { get; set; }
            public string custdtl2picitem { get; set; }
            public string custdtl2phone1 { get; set; }
            public string custdtl2phone2 { get; set; }
            public string custdtl2email { get; set; }            
        }

        private void InitDDL(QL_mstcust tbl)
        {
            sSql = "select * from ql_m05gn where gngroup='PAYMENT TERM' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS INTEGER)";
            var custpaymentoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.custpaymentoid);
            ViewBag.custpaymentoid = custpaymentoid;

            sSql = "select * from ql_m05gn where gngroup='KATEGORI CUSTOMER' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var catid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.catid = catid;

            sSql = "select * from ql_m05gn where gngroup='PROVINSI' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var custprovinceoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.custprovinceoid);
            ViewBag.custprovinceoid = custprovinceoid;

            if (tbl.custprovinceoid > 0)
            {
                sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + tbl.cmpcode + "' AND gnother2='" + tbl.custprovinceoid + "' AND gnflag='ACTIVE'";
                var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.custcityoid);
                ViewBag.custcityoid = cityoid;
            }
            else
            {
                sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + tbl.cmpcode + "' AND gnother2='" + custprovinceoid.First().Value + "' AND gnflag='ACTIVE'";
                var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.custcityoid);
                ViewBag.custcityoid = cityoid;
            }
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }

        [HttpPost]
        public ActionResult InitDDLCity()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'KOTA' and gnflag = 'ACTIVE' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData1()
        {
            if (Session["QL_mstcustdtl1"] == null)
            {
                Session["QL_mstcustdtl1"] = new List<custdtl1>();
            }

            List<custdtl1> dataDtl1 = (List<custdtl1>)Session["QL_mstcustdtl1"];
            return Json(dataDtl1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2()
        {
            if (Session["QL_mstcustdtl2"] == null)
            {
                Session["QL_mstcustdtl2"] = new List<custdtl2>();
            }

            List<custdtl2> dataDtl2 = (List<custdtl2>)Session["QL_mstcustdtl2"];
            return Json(dataDtl2, JsonRequestBehavior.AllowGet);
        }

        // GET: Customer
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT * FROM QL_mstcust WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY custoid DESC";
            List<custmst> vbag = db.Database.SqlQuery<custmst>(sSql).ToList();
            return View(vbag);
        }

        [HttpPost]
        public ActionResult GetDataList()
        {
            var msg = "";
            sSql = "SELECT c.custoid [oid], c.custcode [Kode], c.custname [Nama Customer], c.custaddr [Alamat], c.custphone1 [No. Telp], c.custemail [Email], ISNULL((SELECT TOP 1 c1.custdtl1picname FROM QL_mstcustdtl1 c1 WHERE c1.custoid=c.custoid ORDER BY c1.custdtl1oid),'') [PIC], c.activeflag [Status] FROM QL_mstcust c ORDER BY c.custoid DESC";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, "QL_mstcust");

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Kode")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + dr["oid"].ToString(), "Customer") + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    JsonResult js = Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                    return js;
                }
                else
                    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: Customer/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcust tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstcust();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.custcode = generateCode();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                Session["QL_mstcustdtl1"] = null;
                Session["QL_mstcustdtl2"] = null;

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstcust.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT sd1.custdtl1oid, sd1.custdtl1seq,sd1.custdtl1picname, sd1.custdtl1phone1, sd1.custdtl1phone2, sd1.custdtl1email, sd1.custdtl1jabatan FROM QL_mstcustdtl1 sd1 where custoid =" + tbl.custoid + " order by sd1.custdtl1seq";
                Session["QL_mstcustdtl1"] = db.Database.SqlQuery<custdtl1>(sSql).ToList();

                 sSql = "SELECT sd2.custdtl2oid, sd2.custdtl2seq, sd2.custdtl2loc, sd2.custdtl2addr, sd2.custdtl2cityoid, ISNULL((SELECT gndesc FROM QL_m05gn where gnoid = sd2.custdtl2cityoid and gngroup = 'KOTA' AND gnflag = 'ACTIVE'),'') custdtl2citydesc, sd2.custdtl2postcode, sd2.custdtl2picitem, sd2.custdtl2phone1, sd2.custdtl2phone2, custdtl2email FROM QL_mstcustdtl2 sd2 where custoid =" + tbl.custoid + " order by sd2.custdtl2seq";
                Session["QL_mstcustdtl2"] = db.Database.SqlQuery<custdtl2>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Customer/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcust tbl, string action, string[] catid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<custdtl1> dt1dtl = (List<custdtl1>)Session["QL_mstcustdtl1"];
            //if (dt1dtl == null)
            //    ModelState.AddModelError("", "Silahkan isi Detail PIC");
            //else if (dt1dtl.Count <= 0)
            //    ModelState.AddModelError("", "Silahkan isi Detail PIC");

            List<custdtl2> dt2dtl = (List<custdtl2>)Session["QL_mstcustdtl2"];
            //if (dt2dtl == null)
            //    ModelState.AddModelError("", "Silahkan isi Detail Pengiriman");
            //else if (dt2dtl.Count <= 0)
            //    ModelState.AddModelError("", "Silahkan isi Detail Pengiriman");

            //Is Input Valid?
            //if (string.IsNullOrEmpty(tbl.custcode))
            //    ModelState.AddModelError("custcode", "Silahkan isi Kode!");
            //else if (db.QL_mstcust.Where(w => w.custcode == tbl.custcode & w.custoid != tbl.custoid).Count() > 0)
            //    ModelState.AddModelError("custcode", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            //if (string.IsNullOrEmpty(tbl.custname))
            //    ModelState.AddModelError("custname", "Silahkan isi Nama!");
            //else if (db.QL_mstcust.Where(w => w.custname == tbl.custname & w.custoid != tbl.custoid).Count() > 0)
            //    ModelState.AddModelError("custname", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");

            if (string.IsNullOrEmpty(tbl.custname))
                ModelState.AddModelError("custname", "Silahkan isi Nama!");
            if (string.IsNullOrEmpty(tbl.custaddr))
                ModelState.AddModelError("custaddr", "Silahkan isi Alamat!");
            if (string.IsNullOrEmpty(tbl.custphone1))
                ModelState.AddModelError("custphone1", "Silahkan isi No Telp 1!");

            if (action == "New Data")
            {
                tbl.custcode = generateCode();
            }
            //if (string.IsNullOrEmpty(tbl.custaddr))
            //    tbl.custaddr = "";
            if (string.IsNullOrEmpty(tbl.custdeptname))
                tbl.custdeptname = "";
            if (string.IsNullOrEmpty(tbl.custpostcode))
                tbl.custpostcode = "";
            //if (string.IsNullOrEmpty(tbl.custphone1))
            //    tbl.custphone1 = "";
            if (string.IsNullOrEmpty(tbl.custphone2))
                tbl.custphone2 = "";
            if (string.IsNullOrEmpty(tbl.custfax1))
                tbl.custfax1 = "";
            if (string.IsNullOrEmpty(tbl.custemail))
                tbl.custemail = "";
                //ModelState.AddModelError("custemail", "Silahkan isi Email!");
            if (string.IsNullOrEmpty(tbl.custnpwpno))
                tbl.custnpwpno = "";
            if (string.IsNullOrEmpty(tbl.custnpwp))
                tbl.custnpwp = "";
            if (string.IsNullOrEmpty(tbl.custnpwpaddr))
                tbl.custnpwpaddr = "";
            if (string.IsNullOrEmpty(tbl.custktpimg))
                tbl.custktpimg = "";
            if (string.IsNullOrEmpty(tbl.custnpwpimg))
                tbl.custnpwpimg = "";
            if (string.IsNullOrEmpty(tbl.custnote))
                tbl.custnote = "";
            if (string.IsNullOrEmpty(tbl.custktpimg))
                tbl.custktpimg = "~/Images/";
            if (string.IsNullOrEmpty(tbl.custnpwpimg))
                tbl.custnpwpimg = "~/Images/";

            if (catid != null)
            {
                if (catid.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < catid.Count(); i++)
                    {
                        stsval += "" + catid[i] + ",";
                    }
                    tbl.custcategoryid =  ClassFunction.Left(stsval, stsval.Length - 1);
                }
                else
                {
                    tbl.custcategoryid = "";
                }
            }
            else
            {
                tbl.custcategoryid = "";
            }

            var cCdtl1 = 0;
            if (action != "New Data")
            {
                sSql = "SELECT ISNULL((SELECT COUNT(*) FROM QL_mstcustdtl1 WHERE custoid="+ tbl.custoid +" AND ISNULL(custdtl1flag,'')='"+ tbl.custcategory2 +"'),0) AS t";
                cCdtl1 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }

            if (dt1dtl != null)
            {
                if (dt1dtl.Count > 0)
                {
                    for (int i = 0; i < dt1dtl.Count(); i++)
                    {
                        if (dt1dtl[i].custdtl1picname == "" | string.IsNullOrEmpty(dt1dtl[i].custdtl1picname))
                        {
                            ModelState.AddModelError("", "Detail PIC No. " + dt1dtl[i].custdtl1seq + " - Nama PIC tidak boleh kosong");
                        }
                        if (dt1dtl[i].custdtl1phone1 == "" | string.IsNullOrEmpty(dt1dtl[i].custdtl1phone1))
                        {
                            ModelState.AddModelError("", "Detail PIC No. " + dt1dtl[i].custdtl1seq + " - No. HP 1 tidak boleh kosong");
                        }
                        if (string.IsNullOrEmpty(dt1dtl[i].custdtl1picname))
                            dt1dtl[i].custdtl1picname = "";
                        //if (string.IsNullOrEmpty(dt1dtl[i].custdtl1phone1))
                        //    dt1dtl[i].custdtl1phone1 = "";
                        if (string.IsNullOrEmpty(dt1dtl[i].custdtl1phone2))
                            dt1dtl[i].custdtl1phone2 = "";
                        if (string.IsNullOrEmpty(dt1dtl[i].custdtl1email))
                            dt1dtl[i].custdtl1email = "";
                        if (string.IsNullOrEmpty(dt1dtl[i].custdtl1jabatan))
                            dt1dtl[i].custdtl1jabatan = "";
                    }
                }
            }

            if (dt2dtl != null)
            {
                if (dt2dtl.Count > 0)
                {
                    for (int i = 0; i < dt2dtl.Count(); i++)
                    {
                        if (dt2dtl[i].custdtl2loc == "" | string.IsNullOrEmpty(dt2dtl[i].custdtl2loc))
                        {
                            ModelState.AddModelError("", "Detail Pengiriman No. " + dt2dtl[i].custdtl2seq + " - Nama Lokasi tidak boleh kosong");
                        }
                        if (dt2dtl[i].custdtl2addr == "" | string.IsNullOrEmpty(dt2dtl[i].custdtl2addr))
                        {
                            ModelState.AddModelError("", "Detail Pengiriman No. " + dt2dtl[i].custdtl2seq + " - Alamat tidak boleh kosong");
                        }
                        //if (dt2dtl[i].custdtl2picitem == "" | string.IsNullOrEmpty(dt2dtl[i].custdtl2picitem))
                        //{
                        //    ModelState.AddModelError("", "Detail Pengiriman No. " + dt2dtl[i].custdtl2seq + " - PIC Penerima Barang tidak boleh kosong");
                        //}
                        //if (dt2dtl[i].custdtl2phone1 == "" | string.IsNullOrEmpty(dt2dtl[i].custdtl2phone1))
                        //{
                        //    ModelState.AddModelError("", "Detail Pengiriman No. " + dt2dtl[i].custdtl2seq + " - No. HP 1 tidak boleh kosong");
                        //}
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2loc))
                            dt2dtl[i].custdtl2loc = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2addr))
                            dt2dtl[i].custdtl2addr = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2postcode))
                            dt2dtl[i].custdtl2postcode = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2picitem))
                            dt2dtl[i].custdtl2picitem = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2phone1))
                            dt2dtl[i].custdtl2phone1 = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2phone2))
                            dt2dtl[i].custdtl2phone2 = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].custdtl2email))
                            dt2dtl[i].custdtl2email = "";
                    }
                }
            }

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcust");

            if (ModelState.IsValid)
            {
                var custdtl1oid = ClassFunction.GenerateID("QL_mstcustdtl1");
                var custdtl2oid = ClassFunction.GenerateID("QL_mstcustdtl2");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(tbl.custktpimg)))
                        {
                            var sExt = Path.GetExtension(tbl.custktpimg);
                            var sdir = Server.MapPath(custktppath);
                            var sfilename = "FOTO_KTP_" + (action == "New Data" ? mstoid : tbl.custoid) + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.custktpimg.ToLower() != (custktppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.custktpimg), sdir + "/" + sfilename);
                            }
                            tbl.custktpimg = custktppath + "/" + sfilename;
                        }
                        if (System.IO.File.Exists(Server.MapPath(tbl.custnpwpimg)))
                        {
                            var sExt = Path.GetExtension(tbl.custnpwpimg);
                            var sdir = Server.MapPath(custnpwppath);
                            var sfilename = "FOTO_NPWP_" + (action == "New Data" ? mstoid : tbl.custoid) + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.custnpwpimg.ToLower() != (custnpwppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.custnpwpimg), sdir + "/" + sfilename);
                            }
                            tbl.custnpwpimg = custnpwppath + "/" + sfilename;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.custoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcust.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcust'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Cat Detail
                            sSql = "DELETE FROM QL_mstcatdtl WHERE catdtltype='QL_mstcust' AND catdtlrefoid="+ tbl.custoid +"";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tbl.custoid && a.catdtltype == "QL_mstcust");
                            db.QL_mstcatdtl.RemoveRange(trncat);
                            db.SaveChanges();

                            ////delete detail
                            //var kntrdtl = db.QL_mstcustdtl1.Where(a => a.custoid == tbl.custoid);
                            //var kirimdtl = db.QL_mstcustdtl2.Where(a => a.custoid == tbl.custoid);

                            //db.QL_mstcustdtl1.RemoveRange(kntrdtl);
                            //db.SaveChanges();
                            //db.QL_mstcustdtl2.RemoveRange(kirimdtl);
                            //db.SaveChanges();
                        }
                        
                        //insert cat detail
                        if (catid != null)
                        {
                            if (catid.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < catid.Count(); i++)
                                {
                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstcust";
                                    tblcatdtl.catdtlrefoid = tbl.custoid;
                                    tblcatdtl.catoid = int.Parse(catid[i]);
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        //insert detail
                        QL_mstcustdtl1 tbldtl1; var seqdtl1 = 0;
                        if (dt1dtl != null)
                        {
                            for (int i = 0; i < dt1dtl.Count(); i++)
                            {
                                seqdtl1 = i + 1;
                                if (dt1dtl[i].custdtl1oid == 0)
                                {
                                    tbldtl1 = new QL_mstcustdtl1();
                                    tbldtl1.custdtl1oid = custdtl1oid++;
                                }
                                else
                                {
                                    tbldtl1 = db.QL_mstcustdtl1.Find(CompnyCode, dt1dtl[i].custdtl1oid);
                                }
                                tbldtl1.cmpcode = CompnyCode;
                                tbldtl1.custdtl1seq = i + 1;
                                tbldtl1.custoid = tbl.custoid;
                                tbldtl1.custdtl1picname = dt1dtl[i].custdtl1picname;
                                tbldtl1.custdtl1phone1 = dt1dtl[i].custdtl1phone1;
                                tbldtl1.custdtl1phone2 = dt1dtl[i].custdtl1phone2;
                                tbldtl1.custdtl1email = dt1dtl[i].custdtl1email;
                                tbldtl1.custdtl1jabatan = dt1dtl[i].custdtl1jabatan;
                                tbldtl1.updtime = tbl.updtime;
                                tbldtl1.upduser = tbl.upduser;
                                if (dt1dtl[i].custdtl1oid == 0)
                                {
                                    db.QL_mstcustdtl1.Add(tbldtl1);
                                }
                                else
                                {
                                    db.Entry(tbldtl1).State = EntityState.Modified;
                                }
                            }
                            db.SaveChanges();
                            
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (custdtl1oid - 1) + " Where tablename = 'QL_mstcustdtl1'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //insert detail tipe Individual
                        if (tbl.custcategory2 == "Individual")
                        {
                            if (cCdtl1 == 0)
                            {
                                if (seqdtl1 == 0)
                                {
                                    seqdtl1 = 1;
                                }
                                tbldtl1 = new QL_mstcustdtl1();
                                tbldtl1.custdtl1oid = custdtl1oid++;
                                tbldtl1.cmpcode = CompnyCode;
                                tbldtl1.custdtl1seq = seqdtl1;
                                tbldtl1.custoid = tbl.custoid;
                                tbldtl1.custdtl1picname = tbl.custname;
                                tbldtl1.custdtl1phone1 = tbl.custphone1;
                                tbldtl1.custdtl1phone2 = "";
                                tbldtl1.custdtl1email = "";
                                tbldtl1.custdtl1jabatan = "";
                                tbldtl1.custdtl1flag = tbl.custcategory2;
                                tbldtl1.updtime = tbl.updtime;
                                tbldtl1.upduser = tbl.upduser;
                                db.QL_mstcustdtl1.Add(tbldtl1);
                                db.SaveChanges();

                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (custdtl1oid - 1) + " Where tablename = 'QL_mstcustdtl1'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        //insert detail
                        if (dt2dtl != null)
                        {
                            QL_mstcustdtl2 tbldtl2;
                            for (int i = 0; i < dt2dtl.Count(); i++)
                            {
                                if (dt2dtl[i].custdtl2oid == 0)
                                {
                                    tbldtl2 = new QL_mstcustdtl2();
                                    tbldtl2.custdtl2oid = custdtl2oid++;
                                }
                                else
                                {
                                    tbldtl2 = db.QL_mstcustdtl2.Find(CompnyCode, dt2dtl[i].custdtl2oid);
                                }
                                tbldtl2.cmpcode = CompnyCode;
                                tbldtl2.custdtl2seq = i + 1;
                                tbldtl2.custoid = tbl.custoid;
                                tbldtl2.custdtl2loc = dt2dtl[i].custdtl2loc;
                                tbldtl2.custdtl2addr = dt2dtl[i].custdtl2addr;
                                tbldtl2.custdtl2cityoid = dt2dtl[i].custdtl2cityoid;
                                tbldtl2.custdtl2postcode = dt2dtl[i].custdtl2postcode;
                                tbldtl2.custdtl2picitem = dt2dtl[i].custdtl2picitem;
                                tbldtl2.custdtl2phone1 = dt2dtl[i].custdtl2phone1;
                                tbldtl2.custdtl2phone2 = dt2dtl[i].custdtl2phone2;
                                tbldtl2.custdtl2email = dt2dtl[i].custdtl2email;
                                tbldtl2.updtime = tbl.updtime;
                                tbldtl2.upduser = tbl.upduser;
                                if (dt2dtl[i].custdtl2oid == 0)
                                {
                                    db.QL_mstcustdtl2.Add(tbldtl2);
                                }
                                else
                                {
                                    db.Entry(tbldtl2).State = EntityState.Modified;
                                }
                            }
                            db.SaveChanges();
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (custdtl2oid - 1) + " Where tablename = 'QL_mstcustdtl2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
            //ViewBag(QL_mstcustdtl1);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcust tbl = db.QL_mstcust.Find(Session["CompnyCode"].ToString(),id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (tbl != null)
                        {
                            System.IO.File.Delete(Server.MapPath(tbl.custktpimg));
                            System.IO.File.Delete(Server.MapPath(tbl.custnpwpimg));
                        }

                        var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tbl.custoid && a.catdtltype == "QL_mstcust");
                        db.QL_mstcatdtl.RemoveRange(trncat);
                        db.SaveChanges();

                        var trndtl1 = db.QL_mstcustdtl1.Where(a => a.custoid == id);
                        db.QL_mstcustdtl1.RemoveRange(trndtl1);
                        db.SaveChanges();

                        var trndtl2 = db.QL_mstcustdtl2.Where(a => a.custoid == id);
                        db.QL_mstcustdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        db.QL_mstcust.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<custdtl1> dtDtl, List<custdtl2> dtDtl2)
        {
            Session["QL_mstcustdtl1"] = dtDtl;
            Session["QL_mstcustdtl2"] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setDataDtl(List<custdtl1> mdataDtl)
        {
            Session["QL_mstcustdtl1"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setDataDtl2(List<custdtl2> mdataDtl)
        {
            Session["QL_mstcustdtl2"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string generateCode()
        {
            string sCode = "CUST-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(custCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstcust WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND custCode LIKE '" + sCode + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintXlsReport(string typeexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (typeexp == "Customer")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCustXls.rpt"));
            }
            else if (typeexp == "PIC")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCustPICXls.rpt"));
            }
            else if (typeexp == "Alamat")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCustAlamatXls.rpt"));
            }
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "ExportCustomer_"+ typeexp +".xls");
        }
    }
}