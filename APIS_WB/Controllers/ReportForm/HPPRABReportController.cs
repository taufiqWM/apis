﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class HPPRABReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public HPPRABReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;
            //if (DDLBusinessUnit.Count() > 0)
            //{
            sSql = "SELECT * FROM QL_mstcurr WHERE currcode IN ('IDR', 'USD') order by currcode";
            var DDLCurrency = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "currcode", "currcode");
            ViewBag.DDLCurrency = DDLCurrency;
            //}

            var DDLCOA = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_BANK")).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;
        }

        [HttpPost]
        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_BANK");
                tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("HPPRABReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLType, string StartDate, string EndDate, string DDLPeriod, string DDLCurrency, string DDLOrderBy, string ReportType, string TextNomor, string DDLNomor, string DDLCOA)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "rptHPPRABPdf.rpt";
            var rptname = "HPPReport";

            sSql = "SELECT arm.cmpcode, arm.aritemmstoid, arm.aritemno, c.custname, arm.aritemdate, arm.aritemgrandtotal, ISNULL((SELECT SUM(con.amtbayar) from QL_conar con WHERE con.reftype='QL_trnaritemmst' AND con.refoid=arm.aritemmstoid AND con.payrefoid<>0),0.0) amtbayar, som.soitemmstoid, som.soitemno, som.soitemdate, rm.rabmstoid, rm.rabno, rm.projectname, rm.rabdate, ISNULL(STUFF((SELECT ', '+ft.shipmentitemno FROM (SELECT DISTINCT CAST (cbx.shipmentitemno AS VARCHAR) shipmentitemno FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') sjno, CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnsretitemmst srm INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid=srm.shipmentitemmstoid WHERE shm.soitemmstoid=som.soitemmstoid),0)>0 OR ISNULL((SELECT COUNT(*) FROM QL_trnarretitemmst srm INNER JOIN QL_trnaritemmst shm ON shm.aritemmstoid=srm.aritemmstoid WHERE shm.somstoid=som.soitemmstoid),0)>0 THEN 0.0 ELSE ISNULL((SELECT SUM(pod.shipmentitemvalueidr) FROM QL_trnshipmentitemdtl pod INNER JOIN QL_mstitem i ON i.itemoid=pod.itemoid INNER JOIN QL_trnshipmentitemmst pom ON pom.shipmentitemmstoid=pod.shipmentitemmstoid WHERE pom.soitemmstoid=som.soitemmstoid AND i.itemtype IN('Barang','Rakitan')),0.0) /*+ ISNULL((SELECT SUM(rd.valueidr * rd.qtyout)  amt FROM QL_matbooking rd WHERE rd.formoid=rm.rabmstoid),0.0)*/ END hppapi, /*ISNULL((SELECT SUM((pod.poitemprice * pod.poitemqty) - pod.poitemdtldiscamt) FROM QL_trnpoitemdtl pod INNER JOIN QL_mstitem i ON i.itemoid=pod.itemoid INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid=pod.poitemmstoid WHERE pom.somstoid=som.soitemmstoid AND i.itemtype IN('Ongkir') AND pom.poitemtype<>'Jasa'),0.0)*/ 0.0 hppongkir, CASE WHEN ISNULL((SELECT COUNT(*) FROM QL_trnpretitemmst srm WHERE srm.rabmstoid=som.rabmstoid AND ISNULL(srm.pretitemtype,'')='Jasa'),0)>0 OR ISNULL((SELECT COUNT(*) FROM QL_trnapretitemmst srm INNER JOIN QL_trnapitemmst shm ON shm.apitemmstoid=srm.apitemmstoid WHERE shm.rabmstoid=som.rabmstoid AND ISNULL(shm.apitemtype,'')='Jasa'),0)>0 THEN 0.0 ELSE ISNULL((SELECT SUM((pod.poitemprice * pod.poitemqty) - pod.poitemdtldiscamt) FROM QL_trnpoitemdtl pod INNER JOIN QL_mstitem i ON i.itemoid=pod.itemoid INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid=pod.poitemmstoid WHERE pom.rabmstoid=som.rabmstoid AND  pom.poitemtype='Jasa'),0.0) END hppjasa, CASE WHEN ISNULL(rm.rabmstres1,'')='' THEN ISNULL((SELECT SUM(amt) FROM( SELECT SUM((rd.rabdtl2price * rd.rabdtl2qty) - rd.rabdtl2discamt)  amt FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid=rm.rabmstoid  UNION ALL  SELECT SUM((rd.rabdtl4price * rd.rabdtl4qty) - rd.rabdtl4discamt)  amt FROM QL_trnrabdtl4 rd WHERE rd.rabmstoid=rm.rabmstoid UNION ALL  SELECT SUM(rd.valueidr * rd.qtyout)  amt FROM QL_matbooking rd WHERE rd.formoid=rm.rabmstoid )AS tbl),0.0) ELSE 0.0 END hpprab, ISNULL((SELECT SUM(sod.soitemdtlamt) FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid INNER JOIN QL_trnshipmentitemdtl sd ON sd.shipmentitemmstoid=ardx.shipmentitemmstoid AND sd.shipmentitemdtloid=ardx.shipmentitemdtloid INNER JOIN QL_trnsoitemdtl sod ON sod.soitemdtloid=sd.soitemdtloid INNER JOIN QL_mstitem i ON i.itemoid=sd.itemoid WHERE ardx.aritemmstoid=arm.aritemmstoid AND i.itemtype IN('Ongkir','Jasa')),0.0) piutangsuspend, arm.aritemmstnote, '' jurnalno FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid AND som.soitemtype='' INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmststatus IN('Post','Approved','Closed') ";
            if (StartDate != "" && EndDate != "")
            {
                sSql += " AND "+ DDLPeriod + ">=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND "+ DDLPeriod + "<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            }
            sSql += " ORDER BY arm.aritemno, arm.aritemdate";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartDate) + " - " + ClassFunction.toDate(EndDate));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable(); ;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}