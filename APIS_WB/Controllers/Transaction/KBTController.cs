﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class KBTController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public KBTController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class cashbankgl
        {
            public int cashbankglseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public string acctgdesc2 { get; set; }
            public decimal cashbankglamt { get; set; }
            public string cashbankglnote { get; set; }
            public string cashbankno { get; set; }
            public string cashbanktype { get; set; }
            public int addacctgoiddtl1 { get; set; }
            public decimal addacctgamtdtl1 { get; set; }
            public string addacctgdescdtl1 { get; set; }
            public int addacctgoiddtl2 { get; set; }
            public decimal addacctgamtdtl2 { get; set; }
            public string addacctgdescdtl2 { get; set; }
            public int addacctgoiddtl3 { get; set; }
            public decimal addacctgamtdtl3 { get; set; }
            public string addacctgdescdtl3 { get; set; }
            public string cashbanknocost { get; set; }
            public string cashbanktypecost { get; set; }
            public string cashbankno_in { get; set; }
            public int curroid { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {

            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
            var curroid_to = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid_to);
            ViewBag.curroid_to = curroid_to;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE activeflag='ACTIVE' AND cmpcode='" + CompnyCode + "' ORDER BY acctgcode";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;

            var addacctgoiddtl1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.addacctgoiddtl1 = addacctgoiddtl1;
            var addacctgoiddtl2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.addacctgoiddtl2 = addacctgoiddtl2;
            var addacctgoiddtl3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.addacctgoiddtl3 = addacctgoiddtl3;
        }

        [HttpPost]
        public ActionResult InitDDLPerson()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m01US> tbl = new List<QL_m01US>();
            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' ORDER BY usname";
            tbl = db.Database.SqlQuery<QL_m01US>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOADtl(string[] sVar, string sFilter)
        {
            List<cashbankgl> tbl = new List<cashbankgl>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode, sFilter);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc2, 0.0 cashbankglamt, '' cashbankglnote, 0 groupoid, '' groupdesc, curroid, '' cashbankno_in, 0 addacctgoiddtl1, 0.0 addacctgamtdtl1, 0 addacctgoiddtl2, 0.0 addacctgamtdtl2, 0 addacctgoiddtl3, 0.0 addacctgamtdtl3, '' addacctgdescdtl1, '' addacctgdescdtl2, '' addacctgdescdtl3 FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<cashbankgl> dtDtl)
        {
            Session["QL_trncashbankglmut"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trncashbankglmut"] == null)
            {
                Session["QL_trncashbankglmut"] = new List<cashbankgl>();
            }

            List<cashbankgl> dataDtl = (List<cashbankgl>)Session["QL_trncashbankglmut"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.addcost = true;
            decimal addacctgamtdtl1 = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(cbgl.addacctgamt1), 0.0) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode='" + tbl.cmpcode + "' AND cbgl.cashbankoid=" + tbl.cashbankoid).FirstOrDefault();
            decimal addacctgamtdtl2 = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(cbgl.addacctgamt2), 0.0) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode='" + tbl.cmpcode + "' AND cbgl.cashbankoid=" + tbl.cashbankoid).FirstOrDefault();
            decimal addacctgamtdtl3 = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(cbgl.addacctgamt3), 0.0) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode='" + tbl.cmpcode + "' AND cbgl.cashbankoid=" + tbl.cashbankoid).FirstOrDefault();
            if ((addacctgamtdtl1 + addacctgamtdtl2 + addacctgamtdtl3) > 0)
            {
                ViewBag.addcost = false;
            }
        }

        private string GenerateMutationNo2(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (cmp != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateMutationNo(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (cmp != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Profile");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No KB" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode,cb.cashbankoid, cb.cashbankno, cashbankdate, (CASE cb.cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'BANK' ELSE 'GIRO' END) AS cashbanktype, (a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, p.usname personname, cb.cashbankstatus, cb.cashbanknote, 'False' AS checkvalue, cgl.cashbankglamt cashbankamt, (ax.acctgcode + ' - ' + ax.acctgdesc) AS acctgdesc_in , ISNULL(cbx.cashbankno, '') cashbankno_in  FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_m01us p ON p.cmpcode=cb.cmpcode AND p.usoid=cb.personoid INNER JOIN QL_trncashbankgl cgl ON cgl.cmpcode=cb.cmpcode AND cgl.cashbankoid=cb.cashbankoid INNER JOIN QL_mstacctg ax ON ax.acctgoid=cgl.acctgoid LEFT JOIN (SELECT cglx.cmpcode,ISNULL(CAST(cglx.cashbankglres1 AS INT), 0) glres1, ISNULL(cbx.cashbankno, '') cashbankno, cbx.cashbankamtidr,cbx.cashbanknote FROM QL_trncashbankgl cglx INNER JOIN QL_trncashbankmst cbx ON cbx.cmpcode=cglx.cmpcode AND cbx.cashbankoid=cglx.cashbankoid AND cbx.cashbankgroup='MUTATIONTO') as cbx  ON cbx.cmpcode=cb.cmpcode AND cbx.glres1=cb.cashbankoid AND cbx.cashbankamtidr=cgl.cashbankglamtidr  AND cbx.cashbanknote=cgl.cashbankglnote WHERE cb.cashbankgroup='MUTATION' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();
                tbl.cashbankresamt = 1;
                tbl.cashbankresamt2 = 1;
                tbl.cashbankdpp = 0;
                ViewBag.addcost = true;

                Session["QL_trncashbankglmut"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                Session["lastcashbanktype"] = tbl.cashbanktype;
                Session["lastdpapoid"] = tbl.giroacctgoid;
                Session["lastdpapamt"] = tbl.cashbankamt;

                sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, '' AS cashbankno, '' AS cashbanktype, cbgl.addacctgoid1 addacctgoiddtl1, cbgl.addacctgamt1 addacctgamtdtl1, cbgl.addacctgoid2 addacctgoiddtl2, cbgl.addacctgamt2 addacctgamtdtl2, cbgl.addacctgoid3 addacctgoiddtl3, cbgl.addacctgamt3 addacctgamtdtl3, '' AS cashbanknocost, '' AS cashbanktypecost  , ISNULL((SELECT cashbankno FROM QL_trncashbankgl cbglx INNER JOIN QL_trncashbankmst cbmx ON cbmx.cmpcode=cbglx.cmpcode AND cbmx.cashbankoid=cbglx.cashbankoid WHERE cbglx.cmpcode=cbgl.cmpcode AND ISNULL(CAST(cbglx.cashbankglres1 AS INT), 0)=cbgl.cashbankoid AND cbmx.cashbankamtidr=cbgl.cashbankglamtidr AND cbmx.cashbanknote=cbgl.cashbankglnote), '') AS cashbankno_in, ISNULL((SELECT '(' + a.acctgcode + ') ' + a.acctgdesc acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cbgl.addacctgoid1), '') addacctgdescdtl1, ISNULL((SELECT '(' + a.acctgcode + ') ' + a.acctgdesc acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cbgl.addacctgoid2), '') addacctgdescdtl2, ISNULL((SELECT '(' + a.acctgcode + ') ' + a.acctgdesc acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cbgl.addacctgoid3), '') addacctgdescdtl3 FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" + id + " AND cbgl.cmpcode='" + CompnyCode + "'";
                var tbldtl = db.Database.SqlQuery<cashbankgl>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].cashbankglseq = i + 1;
                        }
                    }
                }
                Session["QL_trncashbankglmut"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string tglmst, string tglduedate)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                tbl.cashbankdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.cashbankduedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankduedate", "Format Tanggal Due date Tidak Valid!!" + ex.ToString());
            }

            if (tbl.cashbankno == null)
                tbl.cashbankno = "";

            List<cashbankgl> dtDtl = (List<cashbankgl>)Session["QL_trncashbankglmut"];
            if (tbl.cashbankresamt <= 0 || tbl.cashbankresamt2 <= 0)
            {
                ModelState.AddModelError("", "Please fill RATE field!");
            }
            if (tbl.acctgoid == 0)
                ModelState.AddModelError("", "Please select MUTATION TO ACCOUNT field!");
            if (tbl.cashbanktype != "BKK")
            {
                if (tbl.cashbankdate > tbl.cashbankduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than EXPENSE DATE");
            }
            if (tbl.personoid.ToString() == "0")
                ModelState.AddModelError("", "Please select PIC field!");
            if (tbl.addacctgoid1 != 0)
                if (tbl.addacctgamt1 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid2)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                }
                if (tbl.addacctgoid2 != 0)
                {
                    if (tbl.addacctgoid2 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                }
            }
            if (tbl.curroid != 1 && tbl.curroid_to != 1)
            {
                ModelState.AddModelError("", "Please select One of Currency From/To must be in IDR!");
            }

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].acctgoid == tbl.acctgoid)
                            ModelState.AddModelError("", "Every Overbooking To COA must be different with Overbooking From COA!");
                        if (dtDtl[i].cashbankglamt <= 0)
                        {
                            ModelState.AddModelError("", "AMOUNT must be more than 0!");
                        }
                        if (dtDtl[i].addacctgoiddtl1 != 0)
                            if (dtDtl[i].addacctgamtdtl1 == 0)
                                ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
                        if (dtDtl[i].addacctgoiddtl2 != 0)
                        {
                            if (dtDtl[i].addacctgamtdtl2 == 0)
                                ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                            if (dtDtl[i].addacctgoiddtl1 != 0)
                            {
                                if (dtDtl[i].addacctgoiddtl1 == dtDtl[i].addacctgoiddtl2)
                                    ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                            }
                        }
                        if (dtDtl[i].addacctgoiddtl3 != 0)
                        {
                            if (dtDtl[i].addacctgamtdtl3 == 0)
                                ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                            if (dtDtl[i].addacctgoiddtl1 != 0)
                            {
                                if (dtDtl[i].addacctgoiddtl1 == dtDtl[i].addacctgoiddtl3)
                                    ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                            }
                            if (dtDtl[i].addacctgoiddtl2 != 0)
                            {
                                if (dtDtl[i].addacctgoiddtl2 == dtDtl[i].addacctgoiddtl3)
                                    ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                            }
                        }
                    }
                }
            }

            //var cRate = new ClassRate(); var cRateTo = new ClassRate(); var cRateCost = new ClassRate(); var cRateCostTo = new ClassRate();
            int iSelisihAcctgOid = 0, iAyatSilangAcctgOid = 0;
            DateTime sDueDate = new DateTime();
            if (tbl.cashbanktype == "BKK")
                sDueDate = tbl.cashbankdate;
            else
                sDueDate = tbl.cashbankduedate;
            DateTime sDate = tbl.cashbankdate;
            if (tbl.cashbanktype == "BBK")
                sDate = tbl.cashbankduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            decimal manualratetoidr = 1; decimal manualratetoidr_glout = 1; decimal manualratetoidr_glin = 1;
            if (tbl.cashbankstatus == "Post")
            {
                //cRate.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "") ModelState.AddModelError("", cRate.GetRateDailyLastError);
                //if (cRate.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRate.GetRateMonthlyLastError);

                //cRateTo.SetRateValue(tbl.curroid_to, sDate.ToString("MM/dd/yyyy"));
                //if (cRateTo.GetRateDailyLastError != "") ModelState.AddModelError("", cRateTo.GetRateDailyLastError);
                //if (cRateTo.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRateTo.GetRateMonthlyLastError);

                //cRateCost.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                //if (cRateCost.GetRateDailyLastError != "") ModelState.AddModelError("", cRateCost.GetRateDailyLastError);
                //if (cRateCost.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRateCost.GetRateMonthlyLastError);

                //cRateCostTo.SetRateValue(tbl.curroid_to, sDate.ToString("MM/dd/yyyy"));
                //if (cRateCostTo.GetRateDailyLastError != "") ModelState.AddModelError("", cRateCostTo.GetRateDailyLastError);
                //if (cRateCostTo.GetRateMonthlyLastError != "") ModelState.AddModelError("", cRateCostTo.GetRateMonthlyLastError);

                string sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_AYAT_SILANG", CompnyCode))
                    sVarErr = "VAR_AYAT_SILANG";
                else
                    iAyatSilangAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AYAT_SILANG", CompnyCode));
                if (!ClassFunction.IsInterfaceExists("VAR_DIFF_CURR_IDR", CompnyCode))
                    sVarErr = "VAR_DIFF_CURR_IDR";
                else
                    iSelisihAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DIFF_CURR_IDR", CompnyCode));

                if (sVarErr != "") ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr));

                if (tbl.curroid != tbl.curroid_to)
                {
                    if (tbl.cashbankresamt > 0 && tbl.cashbankresamt2 > 0)
                    {
                        string sCurr = db.Database.SqlQuery<string>("SELECT currcode FROM QL_mstcurr WHERE curroid=" + tbl.curroid).FirstOrDefault();
                        string sCurrTo = db.Database.SqlQuery<string>("SELECT currcode FROM QL_mstcurr WHERE curroid=" + tbl.curroid_to).FirstOrDefault();

                        manualratetoidr = (tbl.curroid == 1 ? tbl.cashbankresamt : tbl.cashbankresamt2);
                        manualratetoidr_glout = (tbl.curroid == 1 ? tbl.cashbankresamt : 1);
                        manualratetoidr_glin = (tbl.curroid_to == 1 ? tbl.cashbankresamt2 : 1);

                        decimal dRate = tbl.cashbankresamt / tbl.cashbankresamt2;
                        if (sCurr.ToUpper() == "IDR")
                        {
                            if (sCurrTo.ToUpper() == "USD")
                            {
                                //cRate.SetRateMonthlyUSDValue(dRate);
                                //cRateTo.SetRateMonthlyIDRValue(1);
                                //cRateTo.SetRateMonthlyUSDValue(dRate);
                            }
                            else
                            {
                                //cRateTo.SetRateMonthlyIDRValue(1);
                                //cRateTo.SetRateMonthlyUSDValue(cRate.GetRateMonthlyUSDValue);
                            }
                        }
                        if (sCurr.ToUpper() == "USD")
                        {
                            if (sCurrTo.ToUpper() == "IDR")
                            {
                                //cRate.SetRateMonthlyIDRValue(dRate);
                                //cRateTo.SetRateMonthlyIDRValue(dRate);
                                //cRateTo.SetRateMonthlyUSDValue(1);
                            }
                            else
                            {
                            //    cRateTo.SetRateMonthlyIDRValue(cRate.GetRateMonthlyIDRValue);
                            //    cRateTo.SetRateMonthlyUSDValue(1);
                            }
                        }
                        if (sCurr.ToUpper() != "IDR" && sCurr.ToUpper() != "USD")
                        {
                            if (sCurrTo.ToUpper() == "IDR")
                            {
                                //cRate.SetRateMonthlyIDRValue(dRate);
                                //cRateTo.SetRateMonthlyIDRValue(dRate);
                                //cRateTo.SetRateMonthlyUSDValue(cRate.GetRateMonthlyUSDValue);
                            }
                            else if (sCurrTo.ToUpper() == "USD")
                            {
                                //cRate.SetRateMonthlyUSDValue(dRate);
                                //cRateTo.SetRateMonthlyIDRValue(1);
                                //cRateTo.SetRateMonthlyUSDValue(dRate);
                            }
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Silahkan Isi Rate Dahulu!!");
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.cashbankstatus = "In Process";
            }
            if (tbl.cashbankstatus == "Post")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.cashbankstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                int iCashBankOid = 0;
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateMutationNo2(CompnyCode, tglmst, tbl.cashbanktype, tbl.acctgoid);
                    }
                    iCashBankOid = mstoid + 1;
                }
                else
                {
                    iCashBankOid = ClassFunction.GenerateID("QL_trncashbankmst");
                }
                var dtloid = ClassFunction.GenerateID("QL_trncashbankgl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cashbankamtidr = tbl.cashbankamt * manualratetoidr;
                tbl.cashbankamtusd = tbl.cashbankamt * 1;
                tbl.cashbankgroup = "MUTATION";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = (tbl.cashbanktype == "BGK" ? tbl.cashbanktakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.cashbankduedate = sDueDate;
                tbl.cashbanktakegiroreal = (tbl.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbanktakegiroreal);
                tbl.cashbankgiroreal = (tbl.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tbl.cashbankgiroreal);
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.refsuppoid).FirstOrDefault();
                decimal cashbankamt = tbl.cashbankamt - (tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3);

                if (tbl.cashbankstatus == "Post")
                {
                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        string sType = "BKM", sTypeCost = "BKK";
                        string sVar_Bank = ClassFunction.GetDataAcctgOid("VAR_BANK", CompnyCode, sFilter: " AND acctgoid=" + dtDtl[i].acctgoid);
                        if (sVar_Bank != "0")
                        {
                            sType = "BBM";
                            sTypeCost = "BBK";
                        }

                        string sNo = sType + "/" + tbl.cashbankdate.ToString("yy/MM") + "/";
                        string cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>("SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + dtDtl[i].acctgoid + "*/").FirstOrDefault(), DefaultCounter);
                        string sNoCost = sTypeCost + "/" + tbl.cashbankdate.ToString("yy/MM") + "/";
                        string cashbanknocost = sNoCost + ClassFunction.GenNumberString(db.Database.SqlQuery<int>("SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNoCost + "%' /*AND acctgoid=" + dtDtl[i].acctgoid + "*/").FirstOrDefault(), DefaultCounter);

                        dtDtl[i].cashbanktype = sType;
                        dtDtl[i].cashbankno = cashbankno;
                        dtDtl[i].cashbanktypecost = sTypeCost;
                        dtDtl[i].cashbanknocost = cashbanknocost;
                    }
                }
                tbl.cmpcode = CompnyCode;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(CompnyCode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trncashbankgl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncashbankgl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            dtDtl[i].cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);

                            tbldtl = new QL_trncashbankgl();
                            tbldtl.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                            tbldtl.cashbankglgiroflag = "";

                            tbldtl.cmpcode = CompnyCode;
                            tbldtl.cashbankgloid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.cashbankglseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.cashbankglamt = dtDtl[i].cashbankglamt;
                            tbldtl.cashbankglamtidr = dtDtl[i].cashbankglamt * manualratetoidr;
                            tbldtl.cashbankglamtusd = dtDtl[i].cashbankglamt * 1;
                            tbldtl.cashbankglduedate = (tbl.cashbanktype != "BKK" ? tbl.cashbankduedate : DateTime.Parse("1/1/1900 00:00:00"));
                            tbldtl.cashbankglrefno = tbl.cashbankrefno;
                            tbldtl.cashbankglstatus = tbl.cashbankstatus;
                            tbldtl.cashbankglnote = dtDtl[i].cashbankglnote;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.addacctgoid1 = dtDtl[i].addacctgoiddtl1;
                            tbldtl.addacctgoid2 = dtDtl[i].addacctgoiddtl2;
                            tbldtl.addacctgoid3 = dtDtl[i].addacctgoiddtl3;
                            tbldtl.addacctgamt1 = dtDtl[i].addacctgamtdtl1;
                            tbldtl.addacctgamt2 = dtDtl[i].addacctgamtdtl2;
                            tbldtl.addacctgamt3 = dtDtl[i].addacctgamtdtl3;

                            db.QL_trncashbankgl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_id SET lastoid=" + dtloid + " WHERE tablename='QL_trncashbankgl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            decimal dTotalOutInIDR = 0;
                            decimal dTotalOutInUSD = 0
        ;
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Pindah Buku | " + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Ayat Silang Debet
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iAyatSilangAcctgOid, "D", cashbankamt * manualratetoidr, tbl.cashbankno, "Pindah Buku | " + tbl.cashbanknote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, cashbankamt * manualratetoidr, cashbankamt * manualratetoidr, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                            db.SaveChanges();
                            gldtloid += 1;
                            iSeq += 1;
                            dTotalOutInIDR += cashbankamt * manualratetoidr;
                            dTotalOutInUSD += cashbankamt * manualratetoidr_glout;

                            //Additional Cost 1+/-
                            if (tbl.addacctgamt1 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt1) * manualratetoidr_glout, tbl.cashbankno, "Pindah Buku | " + tbl.cashbanknote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * manualratetoidr, Math.Abs(tbl.addacctgamt1) * manualratetoidr_glout, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Additional Cost 1 - C/B Overbooking|No=" + tbl.cashbankno + "", (tbl.addacctgamt1 > 0 ? "K" : "M"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                                dTotalOutInIDR += tbl.addacctgamt1 * manualratetoidr;
                                dTotalOutInUSD += tbl.addacctgamt1 * manualratetoidr_glout;
                            }
                            //Additional Cost 2+/-
                            if (tbl.addacctgamt2 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt2) * manualratetoidr_glout, tbl.cashbankno, "Pindah Buku | " + tbl.cashbanknote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * manualratetoidr, Math.Abs(tbl.addacctgamt2) * manualratetoidr_glout, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Additional Cost 2 - C/B Overbooking|No=" + tbl.cashbankno + "", (tbl.addacctgamt1 > 0 ? "K" : "M"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                                dTotalOutInIDR += tbl.addacctgamt2 * manualratetoidr;
                                dTotalOutInUSD += tbl.addacctgamt2 * manualratetoidr_glout;
                            }
                            //Additional Cost 3+/-
                            if (tbl.addacctgamt3 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt3) * manualratetoidr_glout, tbl.cashbankno, "Pindah Buku | " + tbl.cashbanknote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * manualratetoidr, Math.Abs(tbl.addacctgamt3) * manualratetoidr_glout, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "Additional Cost 3 - C/B Overbooking|No=" + tbl.cashbankno + "", (tbl.addacctgamt3 > 0 ? "K" : "M"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                                dTotalOutInIDR += tbl.addacctgamt3 * manualratetoidr;
                                dTotalOutInUSD += tbl.addacctgamt3 * manualratetoidr_glout;
                            }

                            //CREDIT
                            //Kas/Bank Keluar
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.cashbankamt * manualratetoidr_glout, tbl.cashbankno, "Pindah Buku | " + tbl.cashbanknote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dTotalOutInIDR, dTotalOutInUSD, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                            db.SaveChanges();
                            gldtloid += 1;
                            iSeq += 1;
                            glmstoid += 1;

                            decimal dRate = tbl.cashbankresamt / tbl.cashbankresamt2;
                            for (var i = 0; i < dtDtl.Count(); i++)
                            {
                                dtDtl[i].cashbankglnote = (dtDtl[i].cashbankglnote == null ? "" : dtDtl[i].cashbankglnote);

                                QL_trncashbankmst tblcashbank = new QL_trncashbankmst();
                                tblcashbank.cmpcode = CompnyCode;
                                tblcashbank.cashbankoid = iCashBankOid;
                                tblcashbank.cashbankno = dtDtl[i].cashbankno;
                                tblcashbank.cashbankdate = tbl.cashbankdate;
                                tblcashbank.cashbanktype = dtDtl[i].cashbanktype;
                                tblcashbank.cashbankgroup = "MUTATIONTO";
                                tblcashbank.acctgoid = dtDtl[i].acctgoid;
                                tblcashbank.curroid = tbl.curroid_to;
                                tblcashbank.cashbankamt = dtDtl[i].cashbankglamt;
                                tblcashbank.cashbankamtidr = dtDtl[i].cashbankglamt * manualratetoidr;
                                tblcashbank.cashbankamtusd = dtDtl[i].cashbankglamt * 1;
                                tblcashbank.personoid = tbl.personoid;
                                tblcashbank.cashbankduedate = (tbl.cashbanktype == "BKK" ? tbl.cashbankdate : tbl.cashbankduedate);
                                tblcashbank.cashbankrefno = tbl.cashbankrefno;
                                tblcashbank.cashbanknote = dtDtl[i].cashbankglnote;
                                tblcashbank.cashbankstatus = tbl.cashbankstatus;
                                tblcashbank.createuser = tbl.createuser;
                                tblcashbank.createtime = tbl.createtime;
                                tblcashbank.cashbanktakegiro = DateTime.Parse("1/1/1900 00:00:00");
                                tblcashbank.addacctgoid1 = dtDtl[i].addacctgoiddtl1;
                                tblcashbank.addacctgamt1 = dtDtl[i].addacctgamtdtl1;
                                tblcashbank.addacctgoid2 = dtDtl[i].addacctgoiddtl2;
                                tblcashbank.addacctgamt2 = dtDtl[i].addacctgamtdtl2;
                                tblcashbank.addacctgoid3 = dtDtl[i].addacctgoiddtl3;
                                tblcashbank.addacctgamt3 = dtDtl[i].addacctgamtdtl3;
                                tblcashbank.curroid_to = tbl.curroid;
                                tblcashbank.cashbankresamt = tbl.cashbankresamt2;
                                tblcashbank.cashbankresamt2 = tbl.cashbankresamt;

                                tblcashbank.cashbanktakegiroreal = (tblcashbank.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbanktakegiroreal);
                                tblcashbank.cashbankgiroreal = (tblcashbank.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbankgiroreal);
                                tblcashbank.cashbankaptype = (tblcashbank.cashbankaptype == null ? "" : tblcashbank.cashbankaptype);
                                tblcashbank.cashbanktaxtype = "NON TAX";
                                tblcashbank.periodacctg = sPeriod;
                                tblcashbank.upduser = tbl.createuser;
                                tblcashbank.updtime = tbl.createtime;

                                db.QL_trncashbankmst.Add(tblcashbank);
                                db.SaveChanges();

                                QL_trncashbankgl tbldtl2 = new QL_trncashbankgl();
                                tbldtl2.cashbankgltakegiro = DateTime.Parse("1/1/1900 00:00:00");
                                tbldtl2.cashbankgltakegiroreal = DateTime.Parse("1/1/1900 00:00:00");
                                tbldtl2.cashbankglgiroflag = "";

                                tbldtl2.cmpcode = tbl.cmpcode;
                                tbldtl2.cashbankgloid = dtloid++;
                                tbldtl2.cashbankoid = iCashBankOid;
                                tbldtl2.cashbankglseq = i + 1;
                                tbldtl2.acctgoid = tbl.acctgoid;
                                tbldtl2.cashbankglamt = dtDtl[i].cashbankglamt;
                                tbldtl2.cashbankglamtidr = dtDtl[i].cashbankglamt * manualratetoidr;
                                tbldtl2.cashbankglamtusd = dtDtl[i].cashbankglamt * 1;
                                tbldtl2.cashbankglduedate = (tbl.cashbanktype != "BKK" ? tbl.cashbankdate : tbl.cashbankduedate);
                                tbldtl2.cashbankglrefno = tbl.cashbankrefno;
                                tbldtl2.cashbankglstatus = tbl.cashbankstatus;
                                tbldtl2.cashbankglnote = tbl.cashbanknote;
                                tbldtl2.createuser = tbl.createuser;
                                tbldtl2.createtime = tbl.createtime;
                                tbldtl2.upduser = tbl.upduser;
                                tbldtl2.updtime = tbl.updtime;
                                tbldtl2.cashbankglres1 = tbl.cashbankoid.ToString();

                                db.QL_trncashbankgl.Add(tbldtl2);
                                db.SaveChanges();

                                //JURNAL BANK MASUK
                                iSeq = 1;
                                //Insert Into GLMst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Cash/Bank Overbooking|No=" + dtDtl[i].cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();
                                //Insert Into GLDtl
                                //Kas/Bank Masuk
                                decimal dTotCostPerLine = dtDtl[i].addacctgamtdtl1 + dtDtl[i].addacctgamtdtl2 + dtDtl[i].addacctgamtdtl3;
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].acctgoid, "D", (dtDtl[i].cashbankglamt * manualratetoidr_glin) - (dTotCostPerLine * manualratetoidr_glin), dtDtl[i].cashbankno, "Pindah Buku | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, (dtDtl[i].cashbankglamt * manualratetoidr) - (dTotCostPerLine * manualratetoidr), (dtDtl[i].cashbankglamt * manualratetoidr_glin) - (dTotCostPerLine * manualratetoidr_glin), "QL_trncashbankmst " + iCashBankOid + "", "", "", "", 0));
                                db.SaveChanges();
                                gldtloid += 1;
                                iSeq += 1;
                                if (dTotCostPerLine > 0)
                                {
                                    //Additional Cost 1+/-
                                    if (dtDtl[i].addacctgamtdtl1 != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].addacctgoiddtl1, (dtDtl[i].addacctgamtdtl1 > 0 ? "D" : "C"), Math.Abs(dtDtl[i].addacctgamtdtl1) * manualratetoidr_glin, dtDtl[i].cashbankno, "Pindah Buku | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(dtDtl[i].addacctgamtdtl1) * manualratetoidr, Math.Abs(dtDtl[i].addacctgamtdtl1) * manualratetoidr_glin, "QL_trncashbankmst " + iCashBankOid + "", "", "Cash/Bank Overbooking Additional Cost 1 on Detail|No=" + dtDtl[i].cashbankno + "", (dtDtl[i].addacctgamtdtl1 > 0 ? "K" : "M"), 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                    //Additional Cost 2+/-
                                    if (dtDtl[i].addacctgamtdtl2 != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].addacctgoiddtl2, (dtDtl[i].addacctgamtdtl2 > 0 ? "D" : "C"), Math.Abs(dtDtl[i].addacctgamtdtl2) * manualratetoidr_glin, dtDtl[i].cashbankno, "Pindah Buku | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(dtDtl[i].addacctgamtdtl2) * manualratetoidr, Math.Abs(dtDtl[i].addacctgamtdtl2) * manualratetoidr_glin, "QL_trncashbankmst " + iCashBankOid + "", "", "Cash/Bank Overbooking Additional Cost 2 on Detail|No=" + dtDtl[i].cashbankno + "", (dtDtl[i].addacctgamtdtl2 > 0 ? "K" : "M"), 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                    //Additional Cost 3+/-
                                    if (dtDtl[i].addacctgamtdtl3 != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].addacctgoiddtl3, (dtDtl[i].addacctgamtdtl3 > 0 ? "D" : "C"), Math.Abs(dtDtl[i].addacctgamtdtl3) * manualratetoidr_glin, dtDtl[i].cashbankno, "Pindah Buku | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, Math.Abs(dtDtl[i].addacctgamtdtl3) * manualratetoidr, Math.Abs(dtDtl[i].addacctgamtdtl3) * manualratetoidr_glin, "QL_trncashbankmst " + iCashBankOid + "", "", "Cash/Bank Overbooking Additional Cost 3 on Detail|No=" + dtDtl[i].cashbankno + "", (dtDtl[i].addacctgamtdtl3 > 0 ? "K" : "M"), 0));
                                        db.SaveChanges();
                                        iSeq += 1;
                                        gldtloid += 1;
                                    }
                                }
                                //Ayat Silang
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, iAyatSilangAcctgOid, "C", dtDtl[i].cashbankglamt * manualratetoidr, dtDtl[i].cashbankno, "Pindah Buku | " + tbl.cashbanknote + " | " + dtDtl[i].cashbankglnote + "", tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].cashbankglamt * manualratetoidr, dtDtl[i].cashbankglamt * manualratetoidr, "QL_trncashbankmst " + iCashBankOid + "", "", "", "", 0));
                                db.SaveChanges();
                                gldtloid += 1;
                                glmstoid += 1;
                                iCashBankOid += 1;
                                dtloid = dtloid + 1;
                            }

                            sSql = "UPDATE QL_id SET lastoid=" + dtloid + " WHERE tablename='QL_TRNCASHBANKGL'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (iCashBankOid - 1) + " WHERE tablename='QL_TRNCASHBANKMST'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (glmstoid - 1) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trncashbankgl.Where(a => a.cashbankoid == id && a.cmpcode == CompnyCode);
                        db.QL_trncashbankgl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string printtype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            string sWhere = " WHERE cb.cashbankgroup='MUTATION' AND cb.cashbankoid IN (" + id + ")";
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMutation.rpt"));
            if (printtype == "In")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptMutation2.rpt"));
                sWhere = " WHERE cb.cashbankgroup='MUTATIONTO' AND cb.cashbankoid IN (SELECT glx.cashbankoid FROM QL_trncashbankgl glx WHERE glx.cashbankglres1 IN (" + id + "))";
            }

            report.SetParameterValue("sWhere", sWhere);
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "OverbookingPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}