//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trncreditnote
    {
        public string cmpcode { get; set; }
        public int cnoid { get; set; }
        public string cnno { get; set; }
        public System.DateTime cndate { get; set; }
        public string reftype { get; set; }
        public int refoid { get; set; }
        public int suppcustoid { get; set; }
        public int dbacctgoid { get; set; }
        public int cracctgoid { get; set; }
        public int curroid { get; set; }
        public int rateoid { get; set; }
        public int rate2oid { get; set; }
        public decimal cnamt { get; set; }
        public decimal cnamtidr { get; set; }
        public decimal cnamtusd { get; set; }
        public string cntaxtype { get; set; }
        public decimal cntaxamt { get; set; }
        public decimal cntaxamtidr { get; set; }
        public decimal cntaxamtusd { get; set; }
        public decimal cntotal { get; set; }
        public decimal cntotalidr { get; set; }
        public decimal cntotalusd { get; set; }
        public string cnnote { get; set; }
        public string cnstatus { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public string approvaluser { get; set; }
        public Nullable<System.DateTime> approvaldatetime { get; set; }
        public string closeuser { get; set; }
        public Nullable<System.DateTime> closetime { get; set; }
        public string closereason { get; set; }
        public string reviseuser { get; set; }
        public Nullable<System.DateTime> revisedatetime { get; set; }
        public string revisereason { get; set; }
        public string rejectuser { get; set; }
        public Nullable<System.DateTime> rejectdatetime { get; set; }
        public string rejectreason { get; set; }
    }
}
