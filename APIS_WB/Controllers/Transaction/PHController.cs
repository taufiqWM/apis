﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class PHController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public PHController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usname<>'admin' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            var sVar = "VAR_CASH";
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
                sVar = "VAR_BANK";
            else if (tbl.cashbanktype == "BLK")
                sVar = "VAR_DP_AP";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = GetQueryBindListCOA("VAR_ADD_COST");
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
            var addacctgoid4 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid4);
            ViewBag.addacctgoid4 = addacctgoid4;
            var addacctgoid5 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid5);
            ViewBag.addacctgoid5 = addacctgoid5;

            ViewBag.addacctgoid1_temp = (tbl.addacctgoid1.ToString() == null ? '0' : tbl.addacctgoid1);
            ViewBag.addacctgoid2_temp = (tbl.addacctgoid2.ToString() == null ? '0' : tbl.addacctgoid2);
            ViewBag.addacctgoid3_temp = (tbl.addacctgoid3.ToString() == null ? '0' : tbl.addacctgoid3);
            ViewBag.addacctgoid4_temp = (tbl.addacctgoid4.ToString() == null ? '0' : tbl.addacctgoid4);
            ViewBag.addacctgoid5_temp = (tbl.addacctgoid5.ToString() == null ? '0' : tbl.addacctgoid5);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<int>("SELECT supppaymentoid FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<string>("SELECT gndesc FROM QL_mstsupp s INNER JOIN QL_m05GN g ON gnoid=supppaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.email = db.Database.SqlQuery<string>("SELECT suppemail FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.refsuppoid + "").FirstOrDefault();
            if (tbl.cashbanktype == "BLK")
            {
                ViewBag.dpno = db.Database.SqlQuery<string>("SELECT dpapno FROM QL_trndpap dp WHERE dp.cmpcode='" + CompnyCode + "' AND dpapoid=" + tbl.giroacctgoid + "").FirstOrDefault();
                ViewBag.cashbankresamt = db.Database.SqlQuery<decimal>("SELECT dpapamt FROM QL_trndpap dp WHERE dp.cmpcode='" + CompnyCode + "' AND dpapoid=" + tbl.giroacctgoid + "").FirstOrDefault();
            }
        }

        private string GenerateCashBankNo(string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
            cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);

            return cashbankno;
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            tbl = db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupplierData(string action, int id, int curroid)
        {
            JsonResult js = null;
            try
            {
                List<QL_mstsupp> tbl = new List<QL_mstsupp>();
                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 's.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstsupp') AND name NOT IN ('suppnote') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT DISTINCT " + cols + ", suppnote FROM( SELECT " + cols + ", ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),0) AS suppnote FROM QL_mstsupp s WHERE suppoid IN(SELECT DISTINCT con.suppoid FROM QL_trnapitemmst ap INNER JOIN QL_conap con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.apitemmstoid AND con.suppoid=ap.suppoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apitemmststatus='Post' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0)";
                sSql += " UNION ALL  SELECT " + cols + ", ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),0) AS suppnote FROM QL_mstsupp s WHERE suppoid IN(SELECT DISTINCT con.suppoid FROM QL_trnapdirmst ap INNER JOIN QL_conap con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.apdirmstoid AND con.suppoid=ap.suppoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apdirmststatus='Post' AND con.reftype='QL_trnapdirmst' AND con.payrefoid=0)";
                sSql += " UNION ALL  SELECT " + cols + ", ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),0) AS suppnote FROM QL_mstsupp s WHERE suppoid IN(SELECT DISTINCT con.suppoid FROM QL_trnkasbon2mst ap INNER JOIN QL_conap con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.kasbon2mstoid AND con.suppoid=ap.suppoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.kasbon2mststatus='Post' AND con.reftype='QL_trnkasbon2mst' AND con.payrefoid=0)";
                sSql += " UNION ALL  SELECT " + cols + ", ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),0) AS suppnote FROM QL_mstsupp s WHERE suppoid IN(SELECT DISTINCT con.suppoid FROM QL_trnapassetmst ap INNER JOIN QL_conap con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.apassetmstoid AND con.suppoid=ap.suppoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.apassetmststatus='Post' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0) )AS s ";
                tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDPData(int curroid, int suppoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_trndpap> tbl = new List<QL_trndpap>();

                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'dp.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_trndpap') AND name NOT IN ('dpappayrefno', 'dpapduedate', 'dpapamt') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT " + cols + ", acctgdesc dpappayrefno, (dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) dpapamt, (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpapduedate FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapstatus='Post' AND dp.suppoid=" + suppoid + " AND dp.curroid=" + curroid + " AND dp.dpapamt > dp.dpapaccumamt ORDER BY dp.dpapoid";
                tbl = db.Database.SqlQuery<QL_trndpap>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class payap : QL_trnpayap
        {
            public DateTime transdate { get; set; }
            public string acctgdesc { get; set; }
            public decimal amttrans { get; set; }
            public decimal amtpaid { get; set; }
            public decimal amtbalance { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int suppoid, int curroid, int dpapoid)
        {
            JsonResult js = null;
            string sPlus = ""; string sPlus2 = "";
            string sPlus3 = ""; string sPlus4 = "";
            if (dpapoid > 0)
            {
                sPlus = " AND ap.poitemmstoid IN(SELECT pomstoid FROM QL_trndpap WHERE dpapoid=" + dpapoid + " AND poreftype='QL_trnpoitemmst' AND dpapstatus IN('Post','Closed'))";
                sPlus2 = " AND ap.apdirmstoid=0";
                sPlus3 = " AND ap.kasbon2mstoid=0";
                sPlus4 = " AND ap.poassetmstoid IN(SELECT pomstoid FROM QL_trndpap WHERE dpapoid=" + dpapoid + " AND poreftype='QL_trnpoassetmst' AND dpapstatus IN('Post','Closed'))";
            }

            try
            {
                List<payap> tbl = new List<payap>();

                sSql = "SELECT *, 0 payapseq, (amttrans - amtpaid) amtbalance, 0.0000 payapamt FROM (";
                sSql += " SELECT con.refoid refoid, ap.apitemno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON ap.cmpcode=con.cmpcode AND ap.apitemmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.apdirno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conap con INNER JOIN QL_trnapdirmst ap ON ap.cmpcode=con.cmpcode AND ap.apdirmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapdirmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus2 + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.kasbon2no payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conap con INNER JOIN QL_trnkasbon2mst ap ON ap.cmpcode=con.cmpcode AND ap.kasbon2mstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnkasbon2mst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus3 + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.apassetno payaprefno, con.trnapdate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.0000) amtpaid, curr.currcode currcode, '' payapnote, con.reftype reftype, '' AS projectname, '' AS departemen FROM QL_conap con INNER JOIN QL_trnapassetmst ap ON ap.cmpcode=con.cmpcode AND ap.apassetmstoid=con.refoid AND ap.suppoid=con.suppoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0 AND con.suppoid=" + suppoid + " AND ap.curroid=" + curroid + " " + sPlus4 + " ";
                sSql += ") con WHERE amttrans>amtpaid";
                tbl = db.Database.SqlQuery<payap>(sSql).ToList();

                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<payap>();

            try
            {
                sSql = "DECLARE @oid as INTEGER; DECLARE @cmpcode as VARCHAR(30) SET @oid = " + id + "; SET @cmpcode = '" + CompnyCode + "' ";
                sSql += " SELECT pay.payapoid, pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apitemno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apitemgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP', 'APRET', 'PAYDP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP', 'APRET', 'PAYDP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=apm.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=apm.rabmstoid),'') departemen FROM QL_trnpayap pay INNER JOIN QL_trnapitemmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apitemmstoid AND pay.reftype = 'QL_trnapitemmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapoid, pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apdirno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apdirgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP', 'APRET', 'PAYDP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP', 'APRET', 'PAYDP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=apm.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=apm.rabmstoid),'') departemen FROM QL_trnpayap pay INNER JOIN QL_trnapdirmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apdirmstoid AND pay.reftype = 'QL_trnapdirmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapoid, pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.kasbon2no payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, CASE WHEN apm.kasbon2group='KASBON' THEN apm.kasbon2amt ELSE ABS(apm.kasbon2amtselisih) END amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP', 'APRET', 'PAYDP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP', 'APRET', 'PAYDP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance, '' AS projectname, '' AS departemen FROM QL_trnpayap pay INNER JOIN QL_trnkasbon2mst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.kasbon2mstoid AND pay.reftype = 'QL_trnkasbon2mst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payapoid, pay.payapseq, pay.suppoid, pay.reftype, pay.refoid refoid, apm.apassetno payaprefno, ISNULL((SELECT ap2.trnapdate FROM QL_conap ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.apassetgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payapoid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payapres1, '') <> 'Lebih Bayar' AND ap.trnaptype NOT IN('DNAP', 'CNAP', 'APRET', 'PAYDP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnaptype IN('DNAP', 'CNAP', 'APRET', 'PAYDP')),0.0)) AS amtpaid, pay.payapamt, apm.curroid AS curroid, currcode AS currcode, pay.payapnote, 0.0 AS amtbalance, '' AS projectname, '' AS departemen FROM QL_trnpayap pay INNER JOIN QL_trnapassetmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.apassetmstoid AND pay.reftype = 'QL_trnapassetmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payapres1, '')= ''";
                dtl = db.Database.SqlQuery<payap>(sSql).ToList();
                if (dtl.Count > 0)
                {
                    for (int i = 0; i < dtl.Count(); i++)
                    {
                        dtl[i].amtbalance = dtl[i].amttrans - dtl[i].amtpaid;
                    }
                }
                else
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl = dtl.OrderBy(o => o.payapseq) }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: APPayment
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Pelunasan",["Supplier"] = "Supplier" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cashbankoid, cashbankno, cashbankdate, suppname, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BGK' THEN 'GIRO/CHEQUE' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE '' END) AS cashbanktype, cashbankstatus, cashbanknote, '' divname, cashbankamt, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=cb.createuser),'') createuser FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=cb.refsuppoid WHERE cb.cashbankgroup='AP' AND cb.cashbanktype NOT IN('BLK','BLM') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.cashbankgroup = "AP";
                tbl.cashbankresamt = 0;
                tbl.giroacctgoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();
                tbl.upduser = Session["UserID"].ToString();

                ViewBag.cashbankdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbanktakegiro = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                ViewBag.cashbankdate = tbl.cashbankdate.ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = tbl.cashbankduedate.ToString("dd/MM/yyyy");
                ViewBag.cashbanktakegiro = tbl.cashbanktakegiro.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, List<payap> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            if (string.IsNullOrEmpty(tbl.cashbankno)) tbl.cashbankno = "";
            if (string.IsNullOrEmpty(tbl.cashbanknote)) tbl.cashbanknote = "";
            if (string.IsNullOrEmpty(tbl.cashbankrefno)) tbl.cashbankrefno = "";
            if (string.IsNullOrEmpty(tbl.cashbankres1)) tbl.cashbankres1 = "";

            string sErrReply = "";
            if (tbl.cashbanktype == "BBK" || tbl.cashbanktype == "BGK")
            {
                if (tbl.cashbanktype == "BBK")
                {
                    if (string.IsNullOrEmpty(tbl.cashbankrefno)) msg += "- Please fill REF. NO. field!<br>";
                    if (tbl.cashbankdate > tbl.cashbankduedate) msg += "- DUE DATE must be more or equal than Payment DATE!<br>";
                }
            }
            else if (tbl.cashbanktype == "BLK")
            {
                if (tbl.giroacctgoid == 0) msg += "- Please select DP NO. field!<br>";
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dpappaytype WHEN 'BBK' THEN dpapduedate ELSE dpapdate END) dpdateforcheck FROM QL_trndpap dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dpapoid=" + tbl.giroacctgoid).FirstOrDefault()) msg += "- PAYMENT DATE must be more than DP DATE!<br>";
            }

            if (tbl.cashbanktype == "BGK")
            {
                if (string.IsNullOrEmpty(tbl.cashbankrefno)) msg += "- Please fill REF. NO. field!<br>";
                if (tbl.cashbanktakegiro > tbl.cashbankduedate) msg += "- DATE TAKE GIRO must be less or equal than DUE DATE!<br>";
                if (tbl.cashbankdate > tbl.cashbanktakegiro) msg += "- DATE TAKE GIRO must be more or equal than Payment DATE!<br>";
                if (tbl.refsuppoid == 0) msg += "- Please select SUPPLIER field!<br>";
            }

            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankdpp, ref sErrReply)) msg += "- TOTAL PAYMENT must be less than MAX TOTAL PAYMENT allowed stored in database!<br>";
            if (tbl.cashbankdpp <= 0) msg += "- TOTAL PAYMENT must be more than 0!<br>";
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply)) msg += "- AMOUNT must be less than MAX AMOUNT allowed stored in database!<br>";
            if (tbl.cashbankamt <= 0)
            {
                if (tbl.cashbanktype != "BKK") msg += "- GRAND TOTAL must be more than 0!<br>";
            }
            else
            {
                if (tbl.cashbanktype == "BLK")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        msg += "- GRAND TOTAL must be less than DP AMOUNT!<br>";
                    }
                    else
                    {
                        sSql = "SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dpapoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (tbl.cashbankresamt != dNewDPBalance) tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt) msg += "- DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!<br>";
                    }
                }
            }

            if (tbl.addacctgoid1 != 0)
            {
                if (tbl.addacctgamt1 == 0) msg += "- Amount ADD Cost 1 must be more than 0!<br>";
            }
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0) msg += "- Amount ADD Cost 2 must be more than 0!<br>";
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0) msg += "- Amount ADD Cost 3 must be more than 0!<br>";
            }
            
            if (dtDtl == null) msg += "- Silahkan isi detail data !!<br>";
            else if (dtDtl.Count <= 0) msg += "- Silahkan isi detail data !!<br>";
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        dtDtl[i].suppoid = tbl.refsuppoid;
                        if (string.IsNullOrEmpty(dtDtl[i].payapnote)) dtDtl[i].payapnote = "";
                        if (dtDtl[i].payapamt <= 0) msg += "- Payment Amount " + dtDtl[i].payaprefno + " Belum Diisi !!<br>";
                        if (dtDtl[i].transdate > tbl.cashbankdate) msg += "- PAYMENT DATE tidak boleh lebih keci dari Invoice DATE " + dtDtl[i].payaprefno + " !!<br>";
                        if (dtDtl[i].reftype == "QL_trnapitemmst")
                        {
                            sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.00)) AS amtbalance FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON ap.cmpcode=con.cmpcode AND ap.apitemmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapitemmst' AND con.payrefoid=0 AND ap.apitemmstoid=" + dtDtl[i].refoid + " ";
                        }
                        else
                        {
                            sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conap conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.suppoid=con.suppoid AND conx.payrefoid<>0 AND conx.conflag<>'Lebih Bayar'), 0.00)) AS amtbalance FROM QL_conap con INNER JOIN QL_trnapassetmst ap ON ap.cmpcode=con.cmpcode AND ap.apassetmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnapassetmst' AND con.payrefoid=0 AND ap.apassetmstoid=" + dtDtl[i].refoid + " ";
                        }
                        dtDtl[i].amtbalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dtDtl[i].payapamt > dtDtl[i].amtbalance) msg += "- Payment Amount " + dtDtl[i].payaprefno + " Melebihi AP Balance !!<br>";
                    }
                }
            }

            if (tbl.cashbankstatus == "Post")
            {
                // Interface Validation
                //if (!ClassFunction.IsInterfaceExists("VAR_AP", CompnyCode))
                //    msg += ClassFunction.GetInterfaceWarning("VAR_AP");
                if (!ClassFunction.IsInterfaceExists("VAR_GIRO", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_GIRO");
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AP", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_DP_AP");
            }
            
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.cashbankstatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    msg += cRate.GetRateMonthlyLastError;
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            sSql = "SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.refsuppoid + "";
            string suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            DateTime sDate = tbl.cashbankdate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            DateTime sDateGL = new DateTime();
            if (tbl.cashbanktype == "BKK" || tbl.cashbanktype == "BLK")
            {
                tbl.cashbankduedate = tbl.cashbankdate;
                sDateGL = tbl.cashbankdate;
            }
            else if (tbl.cashbanktype == "BGK")
            {
                sDateGL = tbl.cashbankdate;
            }
            else
            {
                sDateGL = tbl.cashbankduedate;
            }
            string sPeriodGL = ClassFunction.GetDateToPeriodAcctg(sDateGL);

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            if (tbl.cashbankstatus == "Post")
            {
                cekClosingDate = sDateGL;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }

            if (msg == "")
            {
                tbl.cmpcode = CompnyCode;
                tbl.periodacctg = sPeriod;
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbanktaxtype = "";
                tbl.cashbanktaxpct = 0;
                tbl.cashbanktaxamt = 0;
                tbl.cashbankothertaxamt = 0;
                tbl.cashbankaptype = "";
                tbl.cashbankapoid = 0;
                tbl.deptoid = 0;
                tbl.curroid_to = 0;
                tbl.cashbankresamt2 = 0;
                tbl.cashbanksuppaccoid = 0;
                tbl.groupoid = 0;
                tbl.cashbankgiroreal = DateTime.Parse("01/01/1900");
                tbl.cashbanktakegiroreal = DateTime.Parse("01/01/1900");

                //var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP", CompnyCode));
                var iAcctgOidCB = tbl.acctgoid;
                if (tbl.cashbanktype == "BGK")
                {
                    iAcctgOidCB = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", CompnyCode));
                    tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO", CompnyCode));
                }

                if (tbl.cashbankstatus == "Post")
                    tbl.cashbankno = GenerateCashBankNo(tbl.cashbankdate.ToString("dd/MM/yyyy"), tbl.cashbanktype, tbl.acctgoid);

                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpayap");
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;

                            tbl.cashbankres1 = "";
                            tbl.cashbankres2 = "";
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Invoice
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].reftype == "QL_trnapitemmst")
                                {
                                    sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapdirmst")
                                {
                                    sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnkasbon2mst")
                                {
                                    sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapassetmst")
                                {
                                    sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            //Delete Conap
                            sSql = "DELETE FROM QL_conap WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYAP' AND payrefoid IN (SELECT DISTINCT payapoid FROM QL_trnpayap WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpayap.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpayap.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpayap tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnpayap)ClassFunction.MappingTable(new QL_trnpayap(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.payapoid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.payapduedate = tbl.cashbankduedate;
                            tbldtl.payapamtidr = dtDtl[i].payapamt;
                            tbldtl.payapamtusd = 0;
                            tbldtl.payapres1 = "";
                            tbldtl.payapres2 = "";
                            tbldtl.payapres3 = "";
                            tbldtl.payapstatus = "";
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.payapseq = i + 1;
                            tbldtl.giroacctgoid = 0;
                            tbldtl.payaptakegiro = DateTime.Parse("01/01/1900");
                            tbldtl.payaptakegiroreal = DateTime.Parse("01/01/1900");
                            tbldtl.payapgiroflag = "";
                            tbldtl.payapgironote = "";

                            db.QL_trnpayap.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].payapamt >= dtDtl[i].amtbalance)
                            {
                                //Update Invoice
                                if (dtDtl[i].reftype == "QL_trnapitemmst")
                                {
                                    sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapdirmst")
                                {
                                    sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnkasbon2mst")
                                {
                                    sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnapassetmst")
                                {
                                    sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            if (tbl.cashbankstatus == "Post")
                            {
                                // Insert QL_conap                        
                                db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, dtDtl[i].reftype, dtDtl[i].refoid, tbldtl.payapoid, dtDtl[i].suppoid, dtDtl[i].acctgoid, "Post", "PAYAP", servertime, sPeriod, tbl.acctgoid, tbl.cashbankdate, "", 0, tbl.cashbankduedate, 0, dtDtl[i].payapamt, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, dtDtl[i].payapamt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, ""));
                                db.SaveChanges();
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpayap'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLK")
                            {
                                sSql = "UPDATE QL_trndpap SET dpapaccumamt = (dpapaccumamt + " + tbl.cashbankresamt + ") WHERE cmpcode = '" + CompnyCode + "' AND dpapoid = " + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            //var dNetAmt = dtDtl.Sum(m => m.payapamt);
                            //dNetAmt = dNetAmt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3 + tbl.addacctgamt4.Value + tbl.addacctgamt5.Value;

                            var glseq = 1;
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(sDateGL.ToString("MM/dd/yyyy")), sPeriodGL, tbl.cashbankno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "D", dtDtl[i].payapamt, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + dtDtl[i].payapnote + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].payapamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            //Add Cost +
                            if (tbl.addacctgoid1 != 0 && tbl.addacctgamt1 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "D", tbl.addacctgamt1, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt1 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid2 != 0 && tbl.addacctgamt2 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "D", tbl.addacctgamt2, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt2 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid3 != 0 && tbl.addacctgamt3 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "D", tbl.addacctgamt3, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt3 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid4 != 0 && tbl.addacctgamt4 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid4.Value, "D", tbl.addacctgamt4.Value, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt4.Value * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid5 != 0 && tbl.addacctgamt5 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid5.Value, "D", tbl.addacctgamt5.Value, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt5.Value * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCB, "C", tbl.cashbankamt, tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                            db.SaveChanges();

                            //Add Cost -
                            if (tbl.addacctgoid1 != 0 && tbl.addacctgamt1 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "C", (tbl.addacctgamt1 * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt1 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid2 != 0 && tbl.addacctgamt2 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "C", (tbl.addacctgamt2 * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt2 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid3 != 0 && tbl.addacctgamt3 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "C", (tbl.addacctgamt3 * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt3 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid4 != 0 && tbl.addacctgamt4 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid4.Value, "C", (tbl.addacctgamt4.Value * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt4.Value * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid5 != 0 && tbl.addacctgamt5 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid5.Value, "C", (tbl.addacctgamt5.Value * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + suppname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt5.Value * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.cashbankoid.ToString();
                        sReturnNo = "No. " + tbl.cashbankno;
                        if (tbl.cashbankstatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: payap/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var dtDtl = db.QL_trnpayap.Where(x=> x.cashbankoid == id).ToList();
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            if (dtDtl == null)
                msg = "Silahkan isi detail data !!";
            else if (dtDtl.Count <= 0)
                msg = "Silahkan isi detail data !!";

            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Update Invoice
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].reftype == "QL_trnapitemmst")
                            {
                                sSql = "UPDATE QL_trnapitemmst SET apitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apitemmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapdirmst")
                            {
                                sSql = "UPDATE QL_trnapdirmst SET apdirmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnkasbon2mst")
                            {
                                sSql = "UPDATE QL_trnkasbon2mst SET kasbon2mststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND kasbon2mstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnapassetmst")
                            {
                                sSql = "UPDATE QL_trnapassetmst SET apassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND apassetmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        //Delete Conap
                        sSql = "DELETE FROM QL_conap WHERE cmpcode='" + CompnyCode + "' AND trnaptype='PAYAP' AND payrefoid IN (SELECT payapoid FROM QL_trnpayap WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpayap.Where(a => a.cashbankoid == id);
                        db.QL_trnpayap.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, bool isbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPaymentAPAll.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", "WHERE cashbankgroup='AP' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid=" + id + "");
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "APPaymentAllPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}