﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SOStatusReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public SOStatusReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnsoitemmst som WHERE som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetSOData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomorRAB)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, som.soitemmstoid [No. Draft],som.soitemno [No. Sales Order], CONVERT(varchar(10),som.soitemdate,103) [Tgl. SO],c.custname [Customer], som.soitemmststatus [Status], som.soitemmstnote [Note] FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY som.soitemdate DESC, som.soitemmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, rm.rabmstoid [No. Draft], rm.rabno [No. RAB], CONVERT(VARCHAR(10), rm.rabdate, 101) AS [Tgl. RAB], rm.projectname [Nama Project], CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], rm.rabmststatus [Status], rm.rabmstnote [Note] /*, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereq*/ FROM QL_trnrabmst rm WHERE rm.rabmstoid IN (SELECT som.rabmstoid from QL_trnsoitemmst som INNER JOIN QL_mstcust c ON som.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " )";
                sSql += " ORDER BY rm.rabdate DESC, rm.rabmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomorPO, string TextNomorSO, string TextNomorRAB, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=sod.soitemunitoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=sod.soitemmstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid INNER JOIN QL_mstcust c ON C.custoid = som.custoid WHERE som.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorSO != "")
                {
                    string[] arr = TextNomorSO.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomorPO + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorSO, string DDLSoNo, string TextNomorRAB)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            var sPeriode = "";
            var sDDLSono = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusSumXls.rpt";
                else
                    rptfile = "rptSOStatusSumPdf.rpt";
                rptname = "SOStatus_SUMMARY";
            }
            else if (DDLType == "Summary SO")
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusDtlSumXls.rpt";
                else
                    rptfile = "rptSOStatusDtlSumPdf.rpt";
                rptname = "SOStatus_SUMMARY_SO";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusDtlXls.rpt";
                else
                {
                    rptfile = "rptSOStatusDtlPdf.rpt";
                }
                rptname = "SOStatus_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail" || DDLType == "Summary SO")
            {
                //Detail Shipment
                Dtl = " , ISNULL([Shipment No.],'') [Shipment No.], ISNULL([Shipment Date],'') [Shipment Date], ISNULL([Shipment Qty],0.0) [Shipment Qty],ISNULL([SRETQty],0.0) [SRETQty], ISNULL([SJ Ref No.],'') AS [SJ Ref No.], ISNULL([PO No],'') [PO No], ISNULL([Supplier],'') [Supplier],ISNULL([PO Qty],0.0) [PO Qty],ISNULL([PO Price],0.0) [PO Price], ISNULL([MR No],'') [MR No], ISNULL([MR Qty],0.0) [MR Qty], 0.0 [PID], ISNULL([Shipment Value],0.0) [HPP] ";                
                Join = " LEFT JOIN ";
                Join += " (SELECT shd.cmpcode, shm.soitemmstoid, shd.soitemdtloid, shd.itemoid, ISNULL (shm.shipmentitemno,'') AS [Shipment No.],ISNULL (CONVERT(CHAR(10),shm.shipmentitemdate,103),'') AS [Shipment Date],  (ISNULL(shd.shipmentitemqty,0.0)) AS [Shipment Qty], CAST(CASE WHEN ISNULL(shd.shipmentitemvalueidr,0.0) = 0 THEN 0 ELSE(shd.shipmentitemvalueidr / shd.shipmentitemqty) END AS DECIMAL(18,4)) AS [Shipment Value], (SELECT SUM(sretd.sretitemqty) FROM QL_trnsretitemdtl sretd WHERE sretd.cmpcode=sretd.cmpcode AND sretd.shipmentitemdtloid=shd.shipmentitemdtloid AND sretd.sretitemmstoid IN (SELECT sretitemmstoid FROM QL_trnsretitemmst srt WHERE srt.cmpcode=shd.cmpcode AND srt.sretitemmststatus='APPROVED')) AS [SretQty], shd.refno AS [SJ Ref No.] from QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=shd.cmpcode AND shd.shipmentitemmstoid=shm.shipmentitemmstoid AND shm.shipmentitemmststatus IN ('Post','Closed')) AS tblShipment ON tblShipment.cmpcode=tbldtl.cmpcode AND tblShipment.soitemdtloid=tbldtl.sodtloid AND tblShipment.itemoid=tbldtl.itemoid AND tblShipment.soitemmstoid=tbldtl.soitemmstoid ";
                Join += " LEFT JOIN (SELECT shd.cmpcode, shm.somstoid, s.suppname [Supplier], shm.poitemno [PO No], shd.itemoid, ISNULL(SUM(shd.poitemqty), 0.0)[PO Qty], CAST(((shd.poitemprice * SUM(shd.poitemqty)) - SUM(shd.poitemdtldiscamt)) / SUM(shd.poitemqty) AS DECIMAL(18,4)) [PO Price], ISNULL(STUFF((SELECT ', '+ft.mritemno FROM (SELECT DISTINCT CAST (cbx.mritemno AS VARCHAR) mritemno FROM QL_trnmritemmst cbx INNER JOIN QL_trnmritemdtl ardx ON ardx.mritemmstoid=cbx.mritemmstoid WHERE ardx.podtloid=shd.poitemdtloid) ft FOR XML PATH('')), 1, 1, ''),'') [MR No], ISNULL((SELECT SUM(ardx.mritemqty) FROM QL_trnmritemmst cbx INNER JOIN QL_trnmritemdtl ardx ON ardx.mritemmstoid=cbx.mritemmstoid WHERE ardx.podtloid=shd.poitemdtloid),0.0) [MR Qty] FROM QL_trnpoitemdtl shd INNER JOIN QL_trnpoitemmst shm ON shm.poitemmstoid = shd.poitemmstoid INNER JOIN QL_mstsupp s ON s.suppoid=shm.suppoid Group by shd.cmpcode, shm.somstoid, s.suppname, shd.poitemdtloid, shd.itemoid, shm.poitemno, shd.poitemprice) AS tblSO ON tblSO.cmpcode = tbldtl.cmpcode AND tblSO.somstoid = tbldtl.soitemmstoid AND tblSO.itemoid = tbldtl.itemoid";
                //Join += " LEFT JOIN (SELECT shd.cmpcode, shm.somstoid, mrm.mritemno[MR No], shd.itemoid, ISNULL(SUM(mrd.mritemqty), 0.0)[MR Qty], mrd.refno[PID], CASE WHEN mrd.mritemvalueidr = 0 THEN 0 ELSE(mrd.mritemvalueidr / SUM(mrd.mritemqty)) END [HPP] FROM QL_trnpoitemdtl shd INNER JOIN QL_trnpoitemmst shm ON shm.poitemmstoid = shd.poitemmstoid INNER JOIN QL_trnmritemdtl mrd ON mrd.podtloid = shd.poitemdtloid AND mrd.itemoid = shd.itemoid INNER JOIN QL_trnmritemmst mrm ON mrm.mritemmstoid = mrd.mritemmstoid Group by shd.cmpcode, shm.somstoid, shd.itemoid, mrm.mritemno, mrd.refno, mrd.mritemvalueidr) AS tblMR ON tblMR.cmpcode = tbldtl.cmpcode AND tblMR.somstoid = tbldtl.soitemmstoid AND tblMR.itemoid = tbldtl.itemoid AND ISNULL(tblShipment.[SJ Ref No.],'')=ISNULL(tblMR.PID,'')";
            }
            else
            {
                //Detail Shipment
                Dtl = " ,  ISNULL([Shipment Qty],0.0) [Shipment Qty],  ISNULL([Shipment Value],0.0) [Shipment Value],ISNULL([SRETQty],0.0) [SRETQty],ISNULL([PO Qty],0.0) [PO Qty],ISNULL([MR Qty],0.0) [MR Qty]";
                //Detail Shipment Return
                Join = " LEFT JOIN";
                Join += " ( SELECT shd.cmpcode, shm.soitemmstoid, shd.soitemdtloid, shd.itemoid, SUM (ISNULL(shd.shipmentitemqty,0.0)) [Shipment Qty], SUM (ISNULL(shd.shipmentitemvalueidr,0.0)) [Shipment Value], SUM (ISNULL(SRETQty,0.0)) [SRETQty] FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid = shd.shipmentitemmstoid AND shd.shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnshipmentitemmst shm WHERE shm.shipmentitemmstoid = shd.shipmentitemmstoid AND shm.shipmentitemmststatus IN('Post', 'Closed')) LEFT JOIN(SELECT sretd.cmpcode, sretd.shipmentitemdtloid, SUM(Sretitemqty) SRETQty FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretd.cmpcode = sretm.cmpcode AND sretd.sretitemmstoid = sretm.sretitemmstoid GROUP BY sretd.cmpcode, sretd.shipmentitemdtloid) tblSret ON tblSret.cmpcode = shd.cmpcode AND tblSret.shipmentitemdtloid = shd.shipmentitemdtloid Group by shd.cmpcode,soitemmstoid,soitemdtloid, shd.itemoid) AS tblShipment ON tblShipment.cmpcode = tbldtl.cmpcode AND tblShipment.soitemmstoid = tbldtl.soitemmstoid AND tblShipment.soitemdtloid = tbldtl.soitemdtloid AND tblShipment.itemoid = tbldtl.itemoid";
                Join += " LEFT JOIN (SELECT shd.cmpcode, shm.somstoid, shd.itemoid, SUM(ISNULL(shd.poitemqty, 0.0))[PO Qty] FROM QL_trnpoitemdtl shd INNER JOIN QL_trnpoitemmst shm ON shm.poitemmstoid = shd.poitemmstoid Group by shd.cmpcode, shm.somstoid, shd.itemoid) AS tblSO ON tblSO.cmpcode = tbldtl.cmpcode AND tblSO.somstoid = tbldtl.soitemmstoid AND tblSO.itemoid = tbldtl.itemoid";
                Join += " LEFT JOIN (SELECT shd.cmpcode, shm.somstoid, shd.itemoid, SUM(ISNULL(mrd.mritemqty, 0.0))[MR Qty] FROM QL_trnpoitemdtl shd INNER JOIN QL_trnpoitemmst shm ON shm.poitemmstoid = shd.poitemmstoid INNER JOIN QL_trnmritemdtl mrd ON mrd.podtloid = shd.poitemdtloid AND mrd.itemoid = shd.itemoid Group by shd.cmpcode, shm.somstoid, shd.itemoid) AS tblMR ON tblMR.cmpcode = tbldtl.cmpcode AND tblMR.somstoid = tbldtl.soitemmstoid AND tblMR.itemoid = tbldtl.itemoid";
            }
            sSql = "SELECT tblmst.cmpcode, tblmst.sssflag ,tblmst.groupoid, [Division], divname, tblmst.somstoid, draftno, sono, rabno, sodate,transdate, somststatus, [Customer PO], custoid, custcode, custname, somstnote, approvaluser,  approvaldatetime, createuser, createtime AS createdate  , [Close Reason], [Close Date], [Close User], matcode, matlongdesc, sounit, ISNULL(SOClosingQty,0.0) SOClosingQty, ISNULL(SOQtyAwal,0.0) SOQtyAwal, ISNULL([SO Price],0.0) [SO Price], sodtloid, sodtlnote, 0.0 AS BalanceShipmentQty, soitemcustref, Marketing " + Dtl + " ";
            sSql += " FROM";
            sSql += " (SELECT som.cmpcode, '' sssflag , 0 groupoid, '' [Division], '' AS divname, som.soitemmstoid AS somstoid, (CONVERT(VARCHAR(10), som.soitemmstoid)) AS draftno, som.soitemno AS sono, rm.rabno, som.soitemdate AS sodate, som.updtime AS transdate, CASE WHEN ISNULL(som.soitemmstres1,'')='' THEN som.soitemmststatus ELSE ISNULL(som.soitemmstres1,'') END AS somststatus, som.soitemcustpo [Customer PO], som.custoid, c.custcode, c.custname, som.soitemmstnote AS somstnote, '' approvaluser, getdate() approvaldatetime, som.createuser, som.createtime, som.closereason [Close Reason], som.closetime [Close Date], som.closeuser [Close User], '' AS soitemcustref, som.salesoid AS Marketing FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid ) AS tblmst";
            sSql += " INNER JOIN";
            sSql += " (SELECT sod.cmpcode, sod.soitemmstoid, sod.soitemdtloid, sod.itemoid, m.itemcode AS matcode, m.itemdesc AS matlongdesc, 0.0 AS SOClosingQty, ISNULL(sod.soitemqty,0) AS SOQtyAwal, sod.soitemprice [SO Price], sod.soitemdtloid AS sodtloid, g.gndesc AS sounit, sod.soitemdtlnote AS sodtlnote FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON  m.itemoid=sod.itemoid INNER JOIN QL_m05gn g ON g.gnoid=sod.soitemunitoid) as tbldtl ON tbldtl.cmpcode=tblmst.cmpcode AND tbldtl.soitemmstoid=tblmst.somstoid " + Join + "";
            sSql += " WHERE tblmst.cmpcode LIKE '"+ CompnyCode +"'";
            
            if (StartPeriod != "" && EndPeriod != "")
            {
                if (DDLPeriod == "som.updtime")
                {
                    sPeriode = "transdate";
                }
                else
                {
                    sPeriode = "sodate";
                }
                sSql += " AND " + sPeriode + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + sPeriode + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND somststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorSO != "")
            {
                if (DDLSoNo == "som.soitemno")
                {
                    sDDLSono = "sono";
                }
                else
                {
                    sDDLSono = "draftno";
                }
                string[] arr = TextNomorSO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + sDDLSono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorRAB != "")
            {
                string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND matcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";


            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            
            for (int C1 = 0; C1 < dtRpt.Rows.Count; C1++)
            {
                dtRpt.Rows[C1]["BalanceShipmentQty"] = (decimal)dtRpt.Rows[C1]["SOQtyAwal"] - (decimal)dtRpt.Rows[C1]["SOClosingQty"] - ((decimal)dtRpt.Compute("SUM([Shipment Qty])", "sodtloid=" + dtRpt.Rows[C1]["sodtloid"]) - (decimal)dtRpt.Compute("SUM([SRETQty])", "sodtloid=" + dtRpt.Rows[C1]["sodtloid"]));
            }                    
                       

            ////For Status SO Closed (In Process Register)            
            //for (int C2 = 0; C2 < dtRpt.Rows.Count; C2++)
            //{
            //    if ((string)dtRpt.Rows[C2]["somststatus"] == "Closed" && (decimal)dtRpt.Rows[C2]["BalanceShipmentQty"] > 0)
            //    {
            //        dtRpt.Rows[C2]["somststatus"] = "Approved";
            //    }
            //}

            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }            
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}