﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class NilaiTukarController : Controller
    {
        // GET: NilaiTukar
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public NilaiTukarController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listrate
        {
            public int rate2oid { get; set; }
            public string currcode { get; set; }
            public DateTime rate2date { get; set; }
            public Decimal rate2idrvalue { get; set; }
            public Decimal rate2usdvalue { get; set; }
            public string activeflag { get; set; }
            public string rate2note { get; set; }
        }

        // GET/POST: Departemen
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select r.rate2oid, r.rate2date, c.currcode, r.rate2usdvalue, r.rate2idrvalue, r.activeflag, r.rate2note FROM QL_mstrate2 r INNER JOIN QL_mstcurr c ON c.curroid=r.curroid WHERE r.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by r.rate2date ";
            List<listrate> tblmst = db.Database.SqlQuery<listrate>(sSql).ToList();
            return View(tblmst);
        }


        // GET: Departemen/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstrate2 tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_mstrate2();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.rate2oid = ClassFunction.GenerateID("QL_mstrate2");
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.rate2date = ClassFunction.GetServerTime();
                tblmst.rate2usdvalue = 1;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_mstrate2.Find(Session["CompnyCode"].ToString(), id);
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstrate2 tblmst, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            try
            {
                tblmst.rate2date = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("rate2date", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tblmst.rate2note))
                tblmst.rate2note = "";

            tblmst.rate2month = tblmst.rate2date.Month;
            tblmst.rate2year = tblmst.rate2date.Year;
            tblmst.rate2res1 = "";
            tblmst.rate2res2 = "";
            tblmst.rate2res3 = "";

            if (ModelState.IsValid)
            {
                var servertime = ClassFunction.GetServerTime();
                var mstoid = ClassFunction.GenerateID("QL_mstrate2");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.rate2oid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_mstrate2.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstrate2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_mstrate2 tblmst)
        {
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>("SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE' AND currcode<>'IDR'").ToList(), "curroid", "currcode", tblmst.curroid);
            ViewBag.curroid = curroid;
        }

        // POST: Departemen/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstrate2 tblmst = db.QL_mstrate2.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstrate2.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}