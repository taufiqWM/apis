//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnstockadj2
    {
        public string cmpcode { get; set; }
        public int stockadj2oid { get; set; }
        public int stockadjoid { get; set; }
        public string periodacctg { get; set; }
        public string refname { get; set; }
        public int refoid { get; set; }
        public int mtrwhoid { get; set; }
        public decimal stockadjqty { get; set; }
        public decimal stockadjamtidr { get; set; }
        public decimal stockadjamtusd { get; set; }
        public string stockadjstatus { get; set; }
        public int resfield1 { get; set; }
        public string refno { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public string serialnumber { get; set; }
    }
}
