﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class PPController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public PPController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m01US WHERE usflag='ACTIVE' AND cmpcode='" + CompnyCode + "' AND usname<>'admin' ORDER BY usname";
            var personoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.personoid);
            ViewBag.personoid = personoid;

            var sVar = "VAR_CASH";
            if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
                sVar = "VAR_BANK";
            else if (tbl.cashbanktype == "BLM")
                sVar = "VAR_DP_AR";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = GetQueryBindListCOA("VAR_ADD_COST");
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
            var addacctgoid4 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid4);
            ViewBag.addacctgoid4 = addacctgoid4;
            var addacctgoid5 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid5);
            ViewBag.addacctgoid5 = addacctgoid5;

            ViewBag.addacctgoid1_temp = (tbl.addacctgoid1.ToString() == null ? '0' : tbl.addacctgoid1);
            ViewBag.addacctgoid2_temp = (tbl.addacctgoid2.ToString() == null ? '0' : tbl.addacctgoid2);
            ViewBag.addacctgoid3_temp = (tbl.addacctgoid3.ToString() == null ? '0' : tbl.addacctgoid3);
            ViewBag.addacctgoid4_temp = (tbl.addacctgoid4.ToString() == null ? '0' : tbl.addacctgoid4);
            ViewBag.addacctgoid5_temp = (tbl.addacctgoid5.ToString() == null ? '0' : tbl.addacctgoid5);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<int>("SELECT custpaymentoid FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.paytype = db.Database.SqlQuery<string>("SELECT gndesc FROM QL_mstcust s INNER JOIN QL_m05GN g ON gnoid=custpaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            ViewBag.email = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust s WHERE s.cmpcode='" + CompnyCode + "' AND custoid=" + tbl.refsuppoid + "").FirstOrDefault();
            if (tbl.cashbanktype == "BLM")
            {
                ViewBag.dpno = db.Database.SqlQuery<string>("SELECT dparno FROM QL_trndpar dp WHERE dp.cmpcode='" + CompnyCode + "' AND dparoid=" + tbl.giroacctgoid + "").FirstOrDefault();
                ViewBag.cashbankresamt = db.Database.SqlQuery<decimal>("SELECT dparamt FROM QL_trndpar dp WHERE dp.cmpcode='" + CompnyCode + "' AND dparoid=" + tbl.giroacctgoid + "").FirstOrDefault();
            }
        }

        private string GenerateCashBankNo(string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
            cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);

            return cashbankno;
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            tbl = db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList();
            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCusttomerData(string action, int id, int curroid)
        {
            JsonResult js = null;
            try
            {
                List<QL_mstcust> tbl = new List<QL_mstcust>();
                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 's.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstcust') AND name NOT IN ('custnote') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT DISTINCT " + cols + ", custnote FROM( SELECT " + cols + ", ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.custpaymentoid),0) AS custnote FROM QL_mstcust s WHERE custoid IN(SELECT DISTINCT con.custoid FROM QL_trnaritemmst ap INNER JOIN QL_conar con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.aritemmstoid AND con.custoid=ap.custoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.aritemmststatus='Post' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0)";
                sSql += " UNION ALL  SELECT " + cols + ", ISNULL((SELECT ISNULL(g.gnother1,'') FROM QL_m05GN g WHERE g.gnoid=s.custpaymentoid),0) AS custnote FROM QL_mstcust s WHERE custoid IN(SELECT DISTINCT con.custoid FROM QL_trnarassetmst ap INNER JOIN QL_conar con ON con.cmpcode=ap.cmpcode AND con.refoid=ap.arassetmstoid AND con.custoid=ap.custoid WHERE ap.cmpcode='" + CompnyCode + "' AND ap.arassetmststatus='Post' AND con.reftype='QL_trnarassetmst' AND con.payrefoid=0) )AS s ";
                tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetDPData(int curroid, int custoid)
        {
            JsonResult js = null;
            try
            {
                List<QL_trndpar> tbl = new List<QL_trndpar>();

                var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + 'dp.' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_trndpar') AND name NOT IN ('dparpayrefno', 'dparduedate', 'dparamt') FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
                sSql = "SELECT " + cols + ", acctgdesc dparpayrefno, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) dparamt, (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dparduedate FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparstatus='Post' AND dp.custoid=" + custoid + " AND dp.curroid=" + curroid + " AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid";
                tbl = db.Database.SqlQuery<QL_trndpar>(sSql).ToList();

                js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(e.ToString(), JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class payar : QL_trnpayar
        {
            public DateTime transdate { get; set; }
            public string acctgdesc { get; set; }
            public decimal amttrans { get; set; }
            public decimal amtpaid { get; set; }
            public decimal amtbalance { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int custoid, int curroid, int dparoid)
        {
            JsonResult js = null;
            string sPlus = ""; string sPlus2 = "";
            if (dparoid > 0)
            {
                sPlus = " AND ap.somstoid IN(SELECT somstoid FROM QL_trndpar WHERE dparoid=" + dparoid + " AND soreftype='QL_trnsoitemmst' AND dparstatus IN('Post','Closed'))";
                sPlus2 = " AND ap.somstoid IN(SELECT somstoid FROM QL_trndpar WHERE dparoid=" + dparoid + " AND soreftype='QL_trnsoassetmst' AND dparstatus IN('Post','Closed'))";
            }

            try
            {
                List<payar> tbl = new List<payar>();

                sSql = "SELECT *, 0 payarseq, (amttrans - amtpaid) amtbalance, 0.0000 payaramt FROM (";
                sSql += " SELECT con.refoid refoid, ap.aritemno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid  AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_conar con INNER JOIN QL_trnaritemmst ap ON ap.cmpcode=con.cmpcode AND ap.aritemmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0 AND con.custoid=" + custoid + " AND ap.curroid=" + curroid + " " + sPlus + " ";
                sSql += " UNION ALL  SELECT con.refoid refoid, ap.arassetno payarrefno, con.trnardate transdate, con.acctgoid, getdate() transdtg, (acctgcode + '-' + acctgdesc) acctgdesc, con.amttrans amttrans, ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.0000) amtpaid, curr.currcode currcode, '' payarnote, con.reftype reftype, '' AS projectname, '' AS departemen FROM QL_conar con INNER JOIN QL_trnarassetmst ap ON ap.cmpcode=con.cmpcode AND ap.arassetmstoid=con.refoid AND ap.custoid=con.custoid INNER JOIN QL_mstcurr curr ON curr.curroid=ap.curroid INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarassetmst' AND con.payrefoid=0 AND con.custoid=" + custoid + " AND ap.curroid=" + curroid + " " + sPlus2 + " ";
                sSql += ") con WHERE amttrans>amtpaid";
                tbl = db.Database.SqlQuery<payar>(sSql).ToList();

                js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<payar>();

            try
            {
                sSql = "DECLARE @oid as INTEGER; DECLARE @cmpcode as VARCHAR(30) SET @oid = " + id + "; SET @cmpcode = '" + CompnyCode + "' ";
                sSql += " SELECT pay.payaroid, pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.aritemno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, /*(apm.aritemgrandtotal-apm.pphamt-apm.lbppnamt)*/ ISNULL((SELECT ap2.amttrans FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),0.0) amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR', 'ARRET', 'PAYDP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAP', 'CNAP', 'ARRET', 'PAYDP')),0.0)) AS amtpaid, pay.payaramt, apm.curroid AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=apm.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=apm.rabmstoid),'') departemen FROM QL_trnpayar pay INNER JOIN QL_trnaritemmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.aritemmstoid AND pay.reftype = 'QL_trnaritemmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";
                sSql += " UNION ALL  SELECT pay.payaroid, pay.payarseq, pay.custoid, pay.reftype, pay.refoid refoid, apm.arassetno payarrefno, ISNULL((SELECT ap2.trnardate FROM QL_conar ap2 WHERE ap2.payrefoid = 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid),GETDATE()) AS transdate, pay.acctgoid AS acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, apm.arassetgrandtotal amttrans, (ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conar ap INNER JOIN QL_trnpayar pay2 ON ap.cmpcode = pay2.cmpcode AND ap.payrefoid = pay2.payaroid AND pay2.cashbankoid <> pay.cashbankoid WHERE ap.payrefoid <> 0 AND ap.cmpcode = pay.cmpcode AND ap.reftype = pay.reftype AND ap.refoid = pay.refoid AND ISNULL(pay2.payarres1, '') <> 'Lebih Bayar' AND ap.trnartype NOT IN('DNAR', 'CNAR', 'ARRET', 'PAYDP')), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conar ap2 WHERE ap2.payrefoid <> 0 AND pay.cmpcode = ap2.cmpcode AND pay.reftype = ap2.reftype AND pay.refoid = ap2.refoid AND ap2.trnartype IN('DNAP', 'CNAP', 'ARRET', 'PAYDP')),0.0)) AS amtpaid, pay.payaramt, apm.curroid AS curroid, currcode AS currcode, pay.payarnote, 0.0 AS amtbalance, '' AS projectname, '' AS departemen FROM QL_trnpayar pay INNER JOIN QL_trnarassetmst apm ON pay.cmpcode = apm.cmpcode AND pay.refoid = apm.arassetmstoid AND pay.reftype = 'QL_trnarassetmst' INNER JOIN QL_mstacctg a ON a.acctgoid = pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid = apm.curroid WHERE pay.cashbankoid = @oid AND pay.cmpcode = @cmpcode AND ISNULL(pay.payarres1, '')= ''";
                dtl = db.Database.SqlQuery<payar>(sSql).ToList();
                if (dtl.Count > 0)
                {
                    for (int i = 0; i < dtl.Count(); i++)
                    {
                        dtl[i].amtbalance = dtl[i].amttrans - dtl[i].amtpaid;
                    }
                }
                else
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl = dtl.OrderBy(o => o.payarseq) }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: APPayment
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No Pelunasan",["Customer"] = "Customer" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT cb.cmpcode, cashbankoid, cashbankno, cashbankdate, custname, (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BGM' THEN 'GIRO/CHEQUE' WHEN 'BLM' THEN 'DOWN PAYMENT' ELSE '' END) AS cashbanktype, cashbankstatus, cashbanknote, '' divname, cashbankamt, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=cb.createuser),'') createuser FROM QL_trncashbankmst cb INNER JOIN QL_mstcust s ON custoid=cb.refsuppoid WHERE cb.cashbankgroup='AR' AND cb.cashbanktype NOT IN('BLK','BLM') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.cashbankdate >='" + param.filterperiodfrom + " 00:00:00' AND t.cashbankdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.cashbankoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.cashbankno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Customer") sSql += " AND t.custname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.cashbankstatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.cashbankstatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.cashbankstatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.cashbankstatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl.cashbankgroup = "AR";
                tbl.cashbankresamt = 0;
                tbl.giroacctgoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbankduedate = ClassFunction.GetServerTime();
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                ViewBag.cashbankdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.cashbanktakegiro = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
                ViewBag.cashbankdate = tbl.cashbankdate.ToString("dd/MM/yyyy");
                ViewBag.cashbankduedate = tbl.cashbankduedate.ToString("dd/MM/yyyy");
                ViewBag.cashbanktakegiro = tbl.cashbanktakegiro.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, List<payar> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            if (string.IsNullOrEmpty(tbl.cashbankno)) tbl.cashbankno = "";
            if (string.IsNullOrEmpty(tbl.cashbanknote)) tbl.cashbanknote = "";
            if (string.IsNullOrEmpty(tbl.cashbankrefno)) tbl.cashbankrefno = "";
            if (string.IsNullOrEmpty(tbl.cashbankres1)) tbl.cashbankres1 = "";

            string sErrReply = "";
            if (tbl.cashbanktype == "BBM" || tbl.cashbanktype == "BGM")
            {
                if (tbl.cashbanktype == "BBM")
                {
                    if (string.IsNullOrEmpty(tbl.cashbankrefno)) msg += "- Please fill REF. NO. field!<br>";
                    if (tbl.cashbankdate > tbl.cashbankduedate) msg += "- DUE DATE must be more or equal than Payment DATE!<br>";
                }
            }
            else if (tbl.cashbanktype == "BLM")
            {
                if (tbl.giroacctgoid == 0) msg += "- Please select DP NO. field!<br>";
                if (tbl.cashbankdate < db.Database.SqlQuery<DateTime>("SELECT (CASE dparpaytype WHEN 'BBM' THEN dparduedate ELSE dpardate END) dpdateforcheck FROM QL_trndpar dp WHERE dp.cmpcode='" + tbl.cmpcode + "' AND dp.dparoid=" + tbl.giroacctgoid).FirstOrDefault()) msg += "- PAYMENT DATE must be more than DP DATE!<br>";
            }
            if (tbl.cashbanktype == "BGM")
            {
                if (string.IsNullOrEmpty(tbl.cashbankrefno)) msg += "- Please fill REF. NO. field!<br>";
                if (tbl.cashbanktakegiro > tbl.cashbankduedate) msg += "- DATE TAKE GIRO must be less or equal than DUE DATE!<br>";
                if (tbl.cashbankdate > tbl.cashbanktakegiro) msg += "- DATE TAKE GIRO must be more or equal than Payment DATE!<br>";
                if (tbl.refsuppoid == 0) msg += "- Please select SUPPLIER field!<br>";
            }
            if (!ClassFunction.isLengthAccepted("cashbankdpp", "QL_trncashbankmst", tbl.cashbankdpp, ref sErrReply)) msg += "- TOTAL PAYMENT must be less than MAX TOTAL PAYMENT allowed stored in database!<br>";
            if (tbl.cashbankdpp <= 0) msg += "- TOTAL PAYMENT must be more than 0!<br>";
            if (!ClassFunction.isLengthAccepted("cashbankamt", "QL_trncashbankmst", tbl.cashbankamt, ref sErrReply)) msg += "- AMOUNT must be less than MAX AMOUNT allowed stored in database!<br>";
            if (tbl.cashbankamt <= 0)
            {
                if (tbl.cashbanktype != "BKM") msg += "- GRAND TOTAL must be more than 0!<br>";
            }
            else
            {
                if (tbl.cashbanktype == "BLM")
                {
                    if (tbl.cashbankamt > tbl.cashbankresamt)
                    {
                        msg += "- GRAND TOTAL must be less than DP AMOUNT!<br>";
                    }
                    else
                    {
                        sSql = "SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparoid=" + tbl.giroacctgoid;
                        decimal dNewDPBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (tbl.cashbankresamt != dNewDPBalance)
                            tbl.cashbankresamt = dNewDPBalance;
                        if (tbl.cashbankamt > tbl.cashbankresamt) msg += "- DP AMOUNT has been updated by another user. GRAND TOTAL must be less than DP AMOUNT!<br>";
                    }
                }
            }

            if (tbl.addacctgoid1 != 0)
            {
                if (tbl.addacctgamt1 == 0) msg += "- Amount ADD Cost 1 must be more than 0!<br>";
            }
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0) msg += "- Amount ADD Cost 2 must be more than 0!<br>";
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0) msg += "- Amount ADD Cost 3 must be more than 0!<br>";
            }

            //is Input Detail Valid
            if (dtDtl == null) msg += "- Silahkan isi detail data !!<br>";
            else if (dtDtl.Count <= 0) msg += "- Silahkan isi detail data !!<br>";
            else
            {
                for (int i = 0; i < dtDtl.Count(); i++)
                {
                    dtDtl[i].custoid = tbl.refsuppoid;
                    if (string.IsNullOrEmpty(dtDtl[i].payarnote)) dtDtl[i].payarnote = "";
                    if (dtDtl[i].payaramt <= 0) msg += "- Payment Amount " + dtDtl[i].payarrefno + " Belum Diisi !!<br>";
                    if (dtDtl[i].transdate > tbl.cashbankdate) msg += "- PAYMENT DATE tidak boleh lebih keci dari Invoice DATE " + dtDtl[i].payarrefno + " !!<br>";
                    if (dtDtl[i].reftype == "QL_trnaritemmst")
                    {
                        sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.00)) AS amtbalance FROM QL_conar con INNER JOIN QL_trnaritemmst ar ON ar.cmpcode=con.cmpcode AND ar.aritemmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnaritemmst' AND con.payrefoid=0 AND ar.aritemmstoid=" + dtDtl[i].refoid + " ";
                    }
                    else
                    {
                        sSql = "SELECT (con.amttrans - ISNULL((SELECT SUM(conx.amtbayar) FROM QL_conar conx WHERE conx.cmpcode=con.cmpcode AND conx.reftype=con.reftype AND conx.refoid=con.refoid AND conx.custoid=con.custoid AND conx.payrefoid<>0), 0.00)) AS amtbalance FROM QL_conar con INNER JOIN QL_trnarassetmst ar ON ar.cmpcode=con.cmpcode AND ar.arassetmstoid=con.refoid WHERE con.cmpcode='" + CompnyCode + "' AND con.reftype='QL_trnarassetmst' AND con.payrefoid=0 AND ar.arassetmstoid=" + dtDtl[i].refoid + " ";
                    }
                    dtDtl[i].amtbalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                    if (dtDtl[i].payaramt > dtDtl[i].amtbalance) msg += "- Payment Amount " + dtDtl[i].payarrefno + " Melebihi AR Balance !!<br>";
                }
            }

            if (tbl.cashbankstatus == "Post")
            {
                // Interface Validation
                //if (!ClassFunction.IsInterfaceExists("VAR_AR", CompnyCode))
                //     msg += ClassFunction.GetInterfaceWarning("VAR_AR"));
                if (!ClassFunction.IsInterfaceExists("VAR_GIRO_IN", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_GIRO_IN");
                if (!ClassFunction.IsInterfaceExists("VAR_DP_AR", CompnyCode))
                    msg += ClassFunction.GetInterfaceWarning("VAR_DP_AR");
            }
            
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.cashbankstatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    msg += cRate.GetRateMonthlyLastError;
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            sSql = "SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.refsuppoid + "";
            string custname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            DateTime sDate = tbl.cashbankdate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            DateTime sDateGL = new DateTime();
            if (tbl.cashbanktype == "BKM" || tbl.cashbanktype == "BLM")
            {
                tbl.cashbankduedate = tbl.cashbankdate;
                sDateGL = tbl.cashbankdate;
            }
            else if (tbl.cashbanktype == "BGM")
            {
                sDateGL = tbl.cashbankdate;
            }
            else
            {
                sDateGL = tbl.cashbankduedate;
            }
            string sPeriodGL = ClassFunction.GetDateToPeriodAcctg(sDateGL);

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            if (tbl.cashbankstatus == "Post")
            {
                cekClosingDate = sDateGL;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                }
            }

            if (msg == "")
            {
                tbl.cmpcode = CompnyCode;
                tbl.periodacctg = sPeriod;
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.cashbankamtusd = 0;
                tbl.cashbanktaxtype = "";
                tbl.cashbanktaxpct = 0;
                tbl.cashbanktaxamt = 0;
                tbl.cashbankothertaxamt = 0;
                tbl.cashbankaptype = "";
                tbl.cashbankapoid = 0;
                tbl.deptoid = 0;
                tbl.curroid_to = 0;
                tbl.cashbankresamt2 = 0;
                tbl.cashbanksuppaccoid = 0;
                tbl.groupoid = 0;
                tbl.cashbankgiroreal = DateTime.Parse("01/01/1900");
                tbl.cashbanktakegiroreal = DateTime.Parse("01/01/1900");

                //var iAcctgOidAR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR", CompnyCode));
                var iAcctgOidCB = tbl.acctgoid;
                if (tbl.cashbanktype == "BGM")
                {
                    iAcctgOidCB = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", CompnyCode));
                    tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", CompnyCode));
                }

                if (tbl.cashbankstatus == "Post")
                    tbl.cashbankno = GenerateCashBankNo(tbl.cashbankdate.ToString("dd/MM/yyyy"), tbl.cashbanktype, tbl.acctgoid);

                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpayar");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;

                            tbl.cashbankres1 = "";
                            tbl.cashbankres2 = "";
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Invoice
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                if (dtDtl[i].reftype == "QL_trnaritemmst")
                                {
                                    sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarassetmst")
                                {
                                    sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            //Delete Conar
                            sSql = "DELETE FROM QL_conar WHERE cmpcode='" + CompnyCode + "' AND trnartype='PAYAR' AND payrefoid IN (SELECT DISTINCT payaroid FROM QL_trnpayar WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + tbl.cashbankoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpayar.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpayar.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpayar tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnpayar)ClassFunction.MappingTable(new QL_trnpayar(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.payaroid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.payarduedate = tbl.cashbankduedate;
                            tbldtl.payaramtidr = dtDtl[i].payaramt;
                            tbldtl.payaramtusd = 0;
                            tbldtl.payarres1 = "";
                            tbldtl.payarres2 = "";
                            tbldtl.payarres3 = "";
                            tbldtl.payarstatus = "";
                            tbldtl.createtime = tbl.createtime;
                            tbldtl.createuser = tbl.createuser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.payarseq = i + 1;
                            tbldtl.giroacctgoid = 0;
                            tbldtl.payartakegiro = DateTime.Parse("01/01/1900");
                            tbldtl.payartakegiroreal = DateTime.Parse("01/01/1900");
                            tbldtl.payargiroflag = "";
                            tbldtl.payargironote = "";

                            db.QL_trnpayar.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].payaramt >= dtDtl[i].amtbalance)
                            {
                                //Update Invoice
                                if (dtDtl[i].reftype == "QL_trnaritemmst")
                                {
                                    sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                else if (dtDtl[i].reftype == "QL_trnarassetmst")
                                {
                                    sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + dtDtl[i].refoid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            if (tbl.cashbankstatus == "Post")
                            {
                                // Insert QL_conar                      
                                db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, conaroid++, dtDtl[i].reftype, dtDtl[i].refoid, tbldtl.payaroid, dtDtl[i].custoid, dtDtl[i].acctgoid, "Post", "PAYAR", servertime, sPeriod, tbl.acctgoid, tbl.cashbankdate, "", 0, tbl.cashbankduedate, 0, dtDtl[i].payaramt, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, dtDtl[i].payaramt * cRate.GetRateMonthlyIDRValue, 0, 0, 0, ""));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpayar'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + (conaroid - 1) + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.cashbanktype == "BLM")
                            {
                                sSql = "UPDATE QL_trndpar SET dparaccumamt = (dparaccumamt + " + tbl.cashbankresamt + ") WHERE cmpcode = '" + CompnyCode + "' AND dparoid = " + tbl.giroacctgoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            //var dNetAmt = dtDtl.Sum(m => m.payaramt);
                            //dNetAmt = dNetAmt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3 + tbl.addacctgamt4.Value + tbl.addacctgamt5.Value;

                            var glseq = 1;
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(sDateGL.ToString("MM/dd/yyyy")), sPeriodGL, tbl.cashbankno + " | " + custname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidCB, "D", tbl.cashbankamt, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.cashbankamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                            db.SaveChanges();

                            //Add Cost -
                            if (tbl.addacctgoid1 != 0 && tbl.addacctgamt1 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "D", (tbl.addacctgamt1 * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt1 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid2 != 0 && tbl.addacctgamt2 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "D", (tbl.addacctgamt2 * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt2 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid3 != 0 && tbl.addacctgamt3 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "D", (tbl.addacctgamt3 * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt3 * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid4 != 0 && tbl.addacctgamt4 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid4.Value, "D", (tbl.addacctgamt4.Value * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt4.Value * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid5 != 0 && tbl.addacctgamt5 < 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid5.Value, "D", (tbl.addacctgamt5.Value * (-1)), tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, (tbl.addacctgamt5.Value * (-1)) * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "K", 0));
                                db.SaveChanges();
                            }

                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "C", dtDtl[i].payaramt, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + dtDtl[i].payarnote + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, dtDtl[i].payaramt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            //Add Cost +
                            if (tbl.addacctgoid1 != 0 && tbl.addacctgamt1 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid1, "C", tbl.addacctgamt1, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt1 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid2 != 0 && tbl.addacctgamt2 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid2, "C", tbl.addacctgamt2, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt2 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid3 != 0 && tbl.addacctgamt3 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid3, "C", tbl.addacctgamt3, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt3 * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid4 != 0 && tbl.addacctgamt4 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid4.Value, "C", tbl.addacctgamt4.Value, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt4.Value * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }
                            if (tbl.addacctgoid5 != 0 && tbl.addacctgamt5 > 0)
                            {
                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.addacctgoid5.Value, "C", tbl.addacctgamt5.Value, tbl.cashbankno, tbl.cashbankno + " | " + custname + " | " + tbl.cashbanknote, "Post", Session["UserID"].ToString(), servertime, tbl.addacctgamt5.Value * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid, null, null, "M", 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.cashbankoid.ToString();
                        sReturnNo = "No. " + tbl.cashbankno;
                        if (tbl.cashbankstatus == "Post")
                        {
                            sReturnState = "Posted";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var dtDtl = db.QL_trnpayar.Where(x => x.cashbankoid == id).ToList();
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Update Invoice
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (dtDtl[i].reftype == "QL_trnaritemmst")
                            {
                                sSql = "UPDATE QL_trnaritemmst SET aritemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND aritemmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else if (dtDtl[i].reftype == "QL_trnarassetmst")
                            {
                                sSql = "UPDATE QL_trnarassetmst SET arassetmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND arassetmstoid=" + dtDtl[i].refoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        //Delete Conar
                        sSql = "DELETE FROM QL_conar WHERE cmpcode='" + CompnyCode + "' AND trnartype='PAYAR' AND payrefoid IN (SELECT payaroid FROM QL_trnpayar WHERE cmpcode='" + CompnyCode + "' AND cashbankoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpayar.Where(a => a.cashbankoid == id);
                        db.QL_trnpayar.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, bool isbbk)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPaymentARAll.rpt"));
            ClassProcedure.SetDBLogonForReport(report);
            report.SetParameterValue("sWhere", "WHERE cashbankgroup='AR' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid=" + id + "");
            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "ARPaymentAllPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}