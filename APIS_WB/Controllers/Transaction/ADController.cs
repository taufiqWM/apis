﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class ADController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultFormatCounterLong"];
        private string sSql = "";

        public ADController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class assetmst
        {
            public string cmpcode { get; set; }
            public int seq { get; set; }
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public string reftype { get; set; }
            public string assetdesc { get; set; }
            public decimal assetvalue { get; set; }
            public decimal assetvalueidr { get; set; }
            public decimal assetvalueusd { get; set; }
            public decimal assetaccumdep { get; set; }
            public decimal assetaccumdepidr { get; set; }
            public decimal assetaccumdepusd { get; set; }
            public string assetmstnote { get; set; }
            public int assetacctgoid { get; set; }
            public int accumdepacctgoid { get; set; }
            public int rateoid { get; set; }
            public int rate2oid { get; set; }
        }

        public class mstacctg
        {
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }

        private void InitDDL()
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", "cmpcode");
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", null);
            ViewBag.acctgoid = acctgoid;

            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(GetQueryInitDDLDepartment()).ToList(), "deptoid", "deptname", "deptoid");
            ViewBag.deptoid = deptoid;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            List<QL_mstdept> tbl = new List<QL_mstdept>();
            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            tbl = db.Database.SqlQuery<QL_mstdept>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private string GetQueryInitDDLDepartment()
        {
            var result = "SELECT * FROM QL_mstdept WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY deptname";
            return result;
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<mstacctg> tbl = new List<mstacctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, cmp);
            sSql = "SELECT acctgoid, acctgcode, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GenerateData(string cmpcode, int deptoid, string assettype)
        {
            List<assetmst> tbl = new List<assetmst>();
            sSql = "SELECT DISTINCT 0 AS seq, 'False' AS checkvalue, fam.assetmstoid, fam.assetno, fam.reftype, (SELECT x.itemdesc FROM QL_mstitem x WHERE x.itemoid=fam.refoid) AS assetdesc, fam.assetvalue, fam.assetvalueidr, fam.assetvalueusd, ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl fad WHERE fad.cmpcode=fam.cmpcode AND fad.assetmstoid=fam.assetmstoid AND assetdtlstatus='Complete'), 0.0 ) assetaccumdep, ISNULL((SELECT SUM(assetperiodvalueidr) FROM QL_assetdtl fad WHERE fad.cmpcode=fam.cmpcode AND fad.assetmstoid=fam.assetmstoid AND assetdtlstatus='Complete'), 0.0 ) assetaccumdepidr, ISNULL((SELECT SUM(assetperiodvalueusd) FROM QL_assetdtl fad WHERE fad.cmpcode=fam.cmpcode AND fad.assetmstoid=fam.assetmstoid AND assetdtlstatus='Complete'), 0.0 ) assetaccumdepusd, fam.assetmstnote, assetacctgoid, accumdepacctgoid, fam.rateoid, fam.rate2oid FROM QL_assetmst fam WHERE fam.cmpcode='" + CompnyCode + "' AND fam.assetmststatus NOT IN ('In Process', 'Disposed') AND fam.assetdepvalue<>0 AND fam.assetmstflag=''";
            if (deptoid != 0)
            {
                sSql += "AND fam.deptoid='" + deptoid + "'";
            }
            if (assettype != "All")
            {
                sSql += "AND fam.reftype='" + assettype + "'";
            }
            sSql += "ORDER BY fam.assetno";
            //Rejournal
            //sSql = "SELECT DISTINCT 0 AS seq, fam.assetmstoid, fam.assetno, fam.reftype, (CASE fam.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=fam.refoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=fam.refoid) ELSE '' END) AS assetdesc, fam.assetvalue, fam.assetvalueidr, fam.assetvalueusd, ISNULL((SELECT SUM(assetperiodvalue) FROM QL_assetdtl fad WHERE fad.cmpcode=fam.cmpcode AND fad.assetmstoid=fam.assetmstoid AND assetdtlstatus='Complete'), 0.0 ) assetaccumdep, ISNULL((SELECT SUM(assetperiodvalueidr) FROM QL_assetdtl fad WHERE fad.cmpcode=fam.cmpcode AND fad.assetmstoid=fam.assetmstoid AND assetdtlstatus='Complete'), 0.0 ) assetaccumdepidr, ISNULL((SELECT SUM(assetperiodvalueusd) FROM QL_assetdtl fad WHERE fad.cmpcode=fam.cmpcode AND fad.assetmstoid=fam.assetmstoid AND assetdtlstatus='Complete'), 0.0 ) assetaccumdepusd, fam.assetmstnote, assetacctgoid, accumdepacctgoid, fam.rateoid, fam.rate2oid, fam.disposeuser, fam.disposetime, fam.disposeacctgoid FROM QL_assetmst fam WHERE fam.cmpcode='" + cmpcode + "' AND fam.assetmststatus IN ('Disposed') ORDER BY fam.assetno";


            tbl = db.Database.SqlQuery<assetmst>(sSql).ToList();
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].seq = i + 1;
                    }
                }
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<assetmst> dtDtl)
        {
            Session["QL_assetmst"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_assetmst"] == null)
            {
                Session["QL_assetmst"] = new List<assetmst>();
            }

            List<assetmst> dataDtl = (List<assetmst>)Session["QL_assetmst"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string cmpcode, string acctgoid)
        {
            ViewBag.tbDate = ClassFunction.GetServerTime();
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<assetmst> dtDtl = (List<assetmst>)Session["QL_assetmst"];
            int disposeacctgoid = 0;
            if (acctgoid != null)
            {
                disposeacctgoid = Convert.ToInt32(acctgoid);
            }
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "There is no Fixed Assets data that can be disposed for this period!");
            //if (disposeacctgoid == 0)
            //    ModelState.AddModelError("", "Please select COA Cost first!");
            if (dtDtl != null)
            {
                if (dtDtl.Count() <= 0)
                {
                    ModelState.AddModelError("", "Please select Fixed Assets data that will be disposed!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = ClassFunction.GetServerTime();//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
            }

            if (dtDtl != null)
            {
                if (dtDtl.Count() > 0)
                {
                    if (ModelState.IsValid)
                    {
                        var servertime = ClassFunction.GetServerTime();
                        int iGlMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                        int iGlDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                        string sDate = servertime.ToString("MM/dd/yyyy");
                        string sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
                        var cRate = new ClassRate();

                        using (var objTrans = db.Database.BeginTransaction())
                        {
                            try
                            {
                                for (var i = 0; i < dtDtl.Count(); i++)
                                {
                                    string sDisposeUser = Session["UserID"].ToString();
                                    DateTime dDisposeTime = ClassFunction.GetServerTime();

                                    //var dtData = db.QL_assetdtl.Where(x => x.cmpcode == cmpcode && x.assetmstoid == dtDtl[i].assetmstoid && Convert.ToInt32(x.assetperiod) <= Convert.ToInt32(sPeriod) && x.assetdtlstatus != "Complete").ToList();
                                    var dtData = db.Database.SqlQuery<QL_assetdtl>("SELECT * FROM QL_assetdtl WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + dtDtl[i].assetmstoid + " AND assetdtlstatus<>'Complete' AND assetperiod<='" + sPeriod + "'").ToList();
                                    decimal dPostDep = 0, dPostDepIDR = 0, dPostDepUSD = 0;
                                    if (dtData != null)
                                    {
                                        if (dtData.Count() > 0)
                                        {
                                            for (var j = 0; j < dtData.Count(); j++)
                                            {
                                                sSql = "UPDATE QL_assetdtl SET assetdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + dtData[j].assetmstoid + " AND assetdtloid=" + dtData[j].assetdtloid + "";
                                                db.Database.ExecuteSqlCommand(sSql);
                                                db.SaveChanges();
                                                if (dtData[j].assetperiodvalue > 0)
                                                {
                                                    dPostDep += dtData[j].assetperiodvalue;
                                                    dPostDepIDR += dtData[j].assetperiodvalueidr;
                                                    dPostDepUSD += dtData[j].assetperiodvalueusd;
                                                    var sPostDate = sDate;
                                                    var sPostPeriod = sPeriod;
                                                    if (dtData[j].assetperiodvalue != Convert.ToDecimal(sPeriod))
                                                    {
                                                        if (dtData[j].assetperiod == sPeriod)
                                                        {
                                                            sPostDate = dDisposeTime.ToString("MM/dd/yyyy");
                                                            sPostPeriod = dtData[j].assetperiod;
                                                        }else
                                                        {
                                                            sPostDate = new DateTime(Convert.ToInt32(ClassFunction.Left(dtData[j].assetperiod, 4)), Convert.ToInt32(ClassFunction.Right(dtData[j].assetperiod, 2)), DateTime.DaysInMonth(Convert.ToInt32(ClassFunction.Left(dtData[j].assetperiod, 4)), Convert.ToInt32(ClassFunction.Right(dtData[j].assetperiod, 2)))).ToString("MM/dd/yyyy");
                                                            sPostPeriod = dtData[j].assetperiod;
                                                        }
                                                    }
                                                    //Insert GLMST
                                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, iGlMstOid, Convert.ToDateTime(sPostDate), sPostPeriod, "Penyusutan Asset|No=" + dtDtl[i].assetno + "", "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, dtDtl[i].rateoid, dtDtl[i].rate2oid, 0, 0, 0, 0));
                                                    db.SaveChanges();
                                                    //Insert GLDTL
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, 1, iGlMstOid, dtData[j].periodacctgoid, "D", dtData[j].assetperiodvalue, dtDtl[i].assetno, "Penyusutan Asset|No=" + dtDtl[i].assetno + "", "Post", Session["UserID"].ToString(), servertime, dtData[j].assetperiodvalueidr, dtData[j].assetperiodvalueusd, "QL_assetdtl " + dtData[j].assetdtloid + "", "", "", "", 0));
                                                    db.SaveChanges();
                                                    iGlDtlOid += 1;
                                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, 2, iGlMstOid, dtData[j].perioddepacctgoid, "C", dtData[j].assetperiodvalue, dtDtl[i].assetno, "Penyusutan Asset|No=" + dtDtl[i].assetno + "", "Post", Session["UserID"].ToString(), servertime, dtData[j].assetperiodvalueidr, dtData[j].assetperiodvalueusd, "QL_assetdtl " + dtData[j].assetdtloid + "", "", "", "", 0));
                                                    db.SaveChanges();
                                                    iGlDtlOid += 1;
                                                    iGlMstOid += 1;
                                                }
                                            }
                                        }
                                    }

                                    var sDisposeAcctgOid = disposeacctgoid;
                                    sSql = "UPDATE QL_assetdtl SET assetdtlstatus='Disposed' WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + dtDtl[i].assetmstoid + " AND assetdtlstatus<>'Complete'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    sSql = "UPDATE QL_assetmst SET assetmststatus='Disposed', disposeuser='" + sDisposeUser + "', disposetime='" + dDisposeTime + "', disposeacctgoid=" + sDisposeAcctgOid + " WHERE cmpcode='" + CompnyCode + "' AND assetmstoid=" + dtDtl[i].assetmstoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    cRate.SetRateValue(dtDtl[i].rateoid, dtDtl[i].rate2oid.ToString());

                                    //Insert GLMST
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, iGlMstOid, Convert.ToDateTime(sDate), sPeriod, "FA Disposal|No=" + dtDtl[i].assetno + "", "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue));
                                    db.SaveChanges();

                                    decimal dAccum = dtDtl[i].assetaccumdep + dPostDep;
                                    decimal dAccumIDR = dtDtl[i].assetaccumdepidr + dPostDepIDR;
                                    decimal dAccumUSD = dtDtl[i].assetaccumdepusd + dPostDepUSD;

                                    decimal dAsset = dtDtl[i].assetvalue;
                                    decimal dAssetIDR = dtDtl[i].assetvalueidr;
                                    decimal dAssetUSD = dtDtl[i].assetvalueusd;

                                    decimal dCost = dAsset - dAccum;
                                    decimal dCostIDR = dAssetIDR - dAccumIDR;
                                    decimal dCostUSD = dAssetUSD - dAccumUSD;

                                    //Insert GLDTL
                                    // Akumulasi Penyusutan (D)                                    
                                    int iSeq = 1;
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, iSeq, iGlMstOid, dtDtl[i].accumdepacctgoid, "D", dAccum, dtDtl[i].assetno, "FA Disposal|No=" + dtDtl[i].assetno + "", "Post", sDisposeUser, servertime, dAccumIDR, dAccumUSD, "QL_assetmst_dis " + dtDtl[i].assetmstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iGlDtlOid += 1;
                                    iSeq += 1;
                                    //- Biaya (D)
                                    if (dCost != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, iSeq, iGlMstOid, sDisposeAcctgOid, (dCost < 0 ? "C" : "D"), Math.Abs(dCost), dtDtl[i].assetno, "FA Disposal|No=" + dtDtl[i].assetno + "", "Post", sDisposeUser, servertime, 0, 0, "QL_assetmst_dis " + dtDtl[i].assetmstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iGlDtlOid += 1;
                                        iSeq += 1;
                                    }

                                    if (dCostIDR != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, iSeq, iGlMstOid, sDisposeAcctgOid, (dCostIDR < 0 ? "C" : "D"), 0, dtDtl[i].assetno, "FA Disposal|No=" + dtDtl[i].assetno + "", "Post", sDisposeUser, servertime, Math.Abs(dCostIDR), 0, "QL_assetmst_dis " + dtDtl[i].assetmstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iGlDtlOid += 1;
                                        iSeq += 1;
                                    }
                                    if (dCostUSD != 0)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, iSeq, iGlMstOid, sDisposeAcctgOid, (dCostUSD < 0 ? "C" : "D"), 0, dtDtl[i].assetno, "FA Disposal|No=" + dtDtl[i].assetno + "", "Post", sDisposeUser, servertime, 0, Math.Abs(dCostUSD), "QL_assetmst_dis " + dtDtl[i].assetmstoid + "", "", "", "", 0));
                                        db.SaveChanges();
                                        iGlDtlOid += 1;
                                        iSeq += 1;
                                    }
                                    //- Fixed Assets (C)
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, iSeq, iGlMstOid, dtDtl[i].assetacctgoid, "C", dAsset, dtDtl[i].assetno, "FA Disposal|No=" + dtDtl[i].assetno + "", "Post", sDisposeUser, servertime, dAssetIDR, dAssetUSD, "QL_assetmst_dis " + dtDtl[i].assetmstoid + "", "", "", "", 0));
                                    db.SaveChanges();
                                    iGlDtlOid += 1;
                                    iGlMstOid += 1;
                                }

                                sSql = "UPDATE QL_id SET lastoid=" + (iGlMstOid - 1) + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + (iGlDtlOid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                objTrans.Commit();
                                Session["QL_assetmst"] = null;
                                return RedirectToAction("Index");
                            }
                            catch (Exception ex)
                            {
                                objTrans.Rollback();
                                return View(ex.ToString());
                            }
                        }
                    }
                }
            }
            InitDDL();
            Session["QL_assetmst"] = null;
            return View(dtDtl);
        }
    }
}