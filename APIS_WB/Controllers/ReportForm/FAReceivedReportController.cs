﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FAReceivedReportController : Controller
    {
        // GET: FAReceivedReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public FAReceivedReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listMR
        {
            public string mrassetno { get; set; }
            public DateTime mrassetdate { get; set; }
            public string suppname { get; set; }
        }

        [HttpPost]
        public ActionResult GetMRData(string[] status, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT 0 seq, sjm.mrassetmstoid, sjm.mrassetno, mrassetdate, c.suppname FROM QL_trnmrassetmst sjm INNER JOIN QL_mstsupp c ON c.suppoid=sjm.suppoid WHERE sjm.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND sjm.mrassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += "ORDER BY sjm.mrassetno";

            List<listMR> tbl = new List<listMR>();
            tbl = db.Database.SqlQuery<listMR>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        public class listSupp
        {
            public int seq { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
        }

        [HttpPost]
        public ActionResult GetSuppData(string[] status, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT 0 seq, c.suppcode, c.suppname, c.suppaddr FROM QL_mstsupp c WHERE c.suppoid IN (SELECT suppoid from QL_trnmrassetmst sjm WHERE sjm.cmpcode='" + CompnyCode + "')";

            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND sjm.mrassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            sSql += ") ORDER BY c.suppcode";

            List<listSupp> tbl = new List<listSupp>();
            tbl = db.Database.SqlQuery<listSupp>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string suppcode, string TextSO, string startdate, string enddate, string fdate)
        {
            sSql = "SELECT DISTINCT m.itemcode, m.itemdesc, m.itemtype, g.gndesc unit FROM QL_mstitem m INNER JOIN QL_trnmrassetdtl sjd ON sjd.mrassetrefoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid = m.itemunitoid WHERE sjd.cmpcode='"+ CompnyCode + "' AND sjd.mrassetmstoid IN (Select sjm.mrassetmstoid From QL_trnmrassetmst sjm INNER JOIN QL_mstsupp c ON c.suppoid = sjm.suppoid WHERE sjm.cmpcode = '" + CompnyCode + "' ";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND sjm.mrassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (suppcode != "")
            {
                string[] arr = suppcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }
            
            if (TextSO != "")
            {
                string[] arr = TextSO.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND sjm.mrassetno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += ") ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("FAReceivedReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string mrassetno)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                //rptfile = "rptSOAssetSumPdf.rpt";
                if (reporttype == "XLS")
                    rptfile = "rptMRAssetSumXls.rpt";
                else
                    rptfile = "rptMRAssetSumPdf.rpt";
                rptname = "MRA_SUMMARY";
            }
            else
            { 
                if (reporttype == "XLS")
                    rptfile = "rptMRAssetDtlXls.rpt";
                else
                {
                    rptfile = "rptMRAssetDtlPdf.rpt";
                }
                rptname = "MRA_DETAIL";
            }

            var Dtl = ""; var Join = "";
            if (DDLType == "Detail")
            {
                Dtl += ", m.itemcode AS [Code], m.itemdesc AS [Material], sjd.mrassetqty AS [MR Qty], g.gndesc AS [Unit], sjd.mrassetdtlnote AS [Detail Note], pod.poassetprice AS [Price] ";

                Join += " INNER JOIN QL_trnmrassetdtl sjd ON sjd.cmpcode=sjm.cmpcode AND sjd.mrassetmstoid=sjm.mrassetmstoid INNER JOIN QL_mstitem m ON m.itemoid = sjd.mrassetrefoid INNER JOIN QL_m05GN g ON g.gnoid = sjd.mrassetunitoid INNER JOIN QL_trnpoassetdtl pod ON pod.poassetdtloid=sjd.poassetdtloid AND pod.poassetrefoid=sjd.mrassetrefoid";
            }

            sSql = "SELECT sjm.mrassetmstoid AS [OID], CONVERT(VARCHAR(10), sjm.mrassetmstoid) [Draft No.], sjm.mrassetno AS [MR No.], sjm.mrassetdate AS [MR Date], sjm.mrassetmststatus AS [Status], sjm.cmpcode AS [CmpCode], sjm.suppoid [Supplier Oid], c.suppcode [Supplier Code], c.suppname [Supplier Name], 'ANUGRAH PRATAMA' AS [BU Name], sjm.mrassetmstnote AS [MR Note], sjm.createuser [Create User], sjm.createtime [Create Date], (CASE sjm.mrassetmststatus WHEN 'In Process' THEN '' ELSE sjm.upduser END) AS [MRec UserPost] , (CASE sjm.mrassetmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE sjm.updtime END) AS [MRec PostDate], grirposttime AS [GRIR Post Date], g1.gnoid [WH Oid], g1.gndesc [Warehouse], (CASE mrassetmststatus WHEN 'In Process' THEN '' ELSE UPPER(sjm.upduser) END) [Posting User], (CASE mrassetmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE sjm.updtime END) [Posting Date], pom.poassetno AS[PO No.], pom.poassetdate AS[PO Date], pom.approvaldatetime[PO App Date], (SELECT [usname] FROM QL_m01US p2 WHERE pom.approvaluser = p2.usname) AS[PO UserApp], ISNULL((SELECT SUM(sjx.mrassetqty) FROM QL_trnmrassetdtl sjx WHERE sjx.mrassetmstoid=sjm.mrassetmstoid),0.0) [Total Qty], ISNULL((SELECT SUM(mrdx.mrassetqty * podx.poassetprice) FROM QL_trnmrassetdtl mrdx INNER JOIN QL_trnpoassetdtl podx ON podx.poassetdtloid=mrdx.poassetdtloid AND podx.poassetrefoid=mrdx.mrassetrefoid WHERE mrdx.mrassetmstoid=sjm.mrassetmstoid),0.0) [Total HPP] " + Dtl + " FROM QL_trnmrassetmst sjm INNER JOIN QL_trnpoassetmst pom ON pom.poassetmstoid=sjm.poassetmstoid INNER JOIN QL_mstsupp c ON c.suppoid=sjm.suppoid INNER JOIN QL_m05GN g1 ON g1.gnoid=mrassetwhoid " + Join + " WHERE sjm.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND sjm.mrassetmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (!string.IsNullOrEmpty(mrassetno))
            {
                string[] arr = mrassetno.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND sjm.mrassetno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (!string.IsNullOrEmpty(TextSupp))
            {
                string[] arr = TextSupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND c.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}