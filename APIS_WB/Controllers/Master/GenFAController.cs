﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class GenFAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public GenFAController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class FA
        {
            public string cmpcode { get; set; }
            public int faoid { get; set; }
            public string fano { get; set; }
            public string status { get; set; }

        }

        [HttpPost]
        public JsonResult SetFAData(List<FA> dtDtl)
        {
            Session["QL_FA"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilFAData()
        {
            if (Session["QL_FA"] == null)
            {
                Session["QL_FA"] = new List<FA>();
            }

            List<FA> dataDtl = (List<FA>)Session["QL_FA"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

    
            sSql = "SELECT cmpcode, faoid, fakturno as fano, status from ql_mstFA WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY faoid DESC";
            List<FA> vbag = db.Database.SqlQuery<FA>(sSql).ToList();
            return View(vbag);
        }

        // GET: FA/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstFA tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstFA();
                tbl.faoid = ClassFunction.GenerateID("QL_mstFA");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.updtime = tbl.createtime;
                tbl.upduser = tbl.createuser;

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstFA.Find(id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            return View(tbl);
        }

        // GET: FA/Form/5
        public ActionResult Form2(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstFA tbl;
            string action = "New Data";
            if (id == null)
            {
                return RedirectToAction("Form", "GenFA");
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstFA.Find(id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstFA tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

        
            List<FA> dtDtl = (List<FA>)Session["QL_FA"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Silahkan Generate data Dahulu!!!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Silahkan Generate data Dahulu!!!");

            var checkval = "true";
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                sSql = "select count(fakturno) from ql_mstFA where fakturno='" + dtDtl[i].fano + "'";
                int countfa = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                if (countfa > 0)
                {
                    checkval = "false";
                }
            }
            if (checkval == "false")
            {
                ModelState.AddModelError("", "Terdapat No. Faktur yang Telah Digunakan !!!");
            }

            var mstoid = ClassFunction.GenerateID("QL_mstFA");
            var servertime = ClassFunction.GetServerTime();
            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            QL_mstFA tbldtl;
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                tbldtl = new QL_mstFA();
                                tbldtl.cmpcode = tbl.cmpcode;
                                tbldtl.faoid = mstoid++;
                                tbldtl.fakturno = dtDtl[i].fano;
                                tbldtl.status = dtDtl[i].status;
                                tbldtl.createuser = tbl.createuser;
                                tbldtl.createtime = tbl.createtime;
                                tbldtl.upduser = tbl.upduser;
                                tbldtl.updtime = tbl.updtime;

                                db.QL_mstFA.Add(tbldtl);
                            }
                            mstoid = mstoid - 1;
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstFA'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                       

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            return View(tbl);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form2(QL_mstFA tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");


            var servertime = ClassFunction.GetServerTime();

            if (tbl.status == "Closed")
                ModelState.AddModelError("", "No. Faktur Telah Digunakan!!!");


            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            return RedirectToAction("Form", "GenFA");
                        }
                        else
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat4 tbl = db.QL_mstcat4.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat4.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
