﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class CheckTTController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public CheckTTController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listchecktt
        {
            public int checkttmstoid { get; set; }
            public string checkttno { get; set; }
            public string checktttype { get; set; }
            
            public DateTime checkttdate { get; set; }
            public string suppname { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal grandtotal { get; set; }
            public string checkttmststatus { get; set; }
            public string checkttnote { get; set; }
        }

        public class listcheckttdtl
        {
            public int checkttdtlseq { get; set; }
            public int checkttdtloid { get; set; }
            public string apitemno { get; set; }
            public decimal poitemgrandtotal { get; set; }
            public string checkttlengkap { get; set; }
            public string checkttfaktur { get; set; }
            public string checkttkwitansi { get; set; }
            public string checkttsj { get; set; }
            public int mrmstoid { get; set; }
            public string mrno { get; set; }
            public int poitemmstoid { get; set; }
            public string poitemno { get; set; }
            public string potype { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public string checkttdtlnote { get; set; }

        }        
        

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        public class poitem
        {
            public int mrmstoid { get; set; }
            public string mrno { get; set; }
            public string mrdate { get; set; }
            public int poitemmstoid { get; set; }
            public string poitemno { get; set; }
            public DateTime poitemdate { get; set; }           
            public string poitemdatestr { get; set; }
            public string checkttlengkap { get; set; }
            public string checkttfaktur { get; set; }
            public string checkttkwitansi { get; set; }
            public string checkttsj { get; set; }           
            public string poitemmstnote { get; set; }
            public string potype { get; set; }
        }

        public class listduedate
        {
            public DateTime duedate { get; set; }
            public string duedatestr { get; set; }
        }

        public class listfa
        {
            public int faoid { get; set; }
            public string fakturno { get; set; }
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public int gnother1 { get; set; }
            public string supppengakuan { get; set; }
            public DateTime duedate { get; set; }
        }

        public class listsuppsales
        {
            public int suppoid { get; set; }
            public int suppsalesoid { get; set; }
            public string suppsalesname { get; set; }
            public string suppsalesphone { get; set; }
            public string suppsalesemail { get; set; }
            public string suppsalesjabatan { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listcheckttdtl> dtDtl)
        {
            Session["QL_trncheckttdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData()
        {
            if (Session["QL_trncheckttdtl"] == null)
            {
                Session["QL_trncheckttdtl"] = new List<listcheckttdtl>();
            }

            List<listcheckttdtl> dataDtl = (List<listcheckttdtl>)Session["QL_trncheckttdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "CTT/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(checkttno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncheckttmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND checkttno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetSupplierData(int checkttmstoid)
        {
            List<listsupp> tbl = new List<listsupp>();

            sSql = "SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid, (SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = supppaymentoid) gnother1, CASE WHEN supppengakuan='TT' THEN 'Check TT' ELSE 'Penerimaan' END supppengakuan, DATEADD(DAY, ISNULL((SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = supppaymentoid),0), GETDATE()) duedate FROM QL_mstsupp s INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=s.cmpcode AND pom.suppoid=s.suppoid INNER JOIN QL_trnmritemmst mrm ON mrm.pomstoid=pom.poitemmstoid WHERE s.cmpcode='" + CompnyCode + "' /*AND s.supppengakuan = 'TT'*/ AND mrm.mritemmstoid NOT IN (SELECT DISTINCT mrmstoid FROM QL_trncheckttdtl apd INNER JOIN QL_trncheckttmst apm ON apm.cmpcode=apd.cmpcode AND apm.checkttmstoid=apd.checkttmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND potype<>'QL_trnpoassetmst' AND checkttmststatus<>'Cancel' AND apm.checkttmstoid<>" + checkttmstoid + ") ";
            sSql += " UNION ALL  SELECT DISTINCT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid, (SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = supppaymentoid) gnother1, CASE WHEN supppengakuan='TT' THEN 'Check TT' ELSE 'Penerimaan' END supppengakuan, DATEADD(DAY, ISNULL((SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = supppaymentoid),0), GETDATE()) duedate FROM QL_mstsupp s INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=s.cmpcode AND pom.suppoid=s.suppoid INNER JOIN QL_trnmrassetmst mrm ON mrm.poassetmstoid=pom.poassetmstoid WHERE s.cmpcode='" + CompnyCode + "' /*AND s.supppengakuan = 'TT'*/ AND mrm.mrassetmstoid NOT IN (SELECT DISTINCT mrmstoid FROM QL_trncheckttdtl apd INNER JOIN QL_trncheckttmst apm ON apm.cmpcode=apd.cmpcode AND apm.checkttmstoid=apd.checkttmstoid WHERE apd.cmpcode='" + CompnyCode + "' AND potype='QL_trnpoassetmst' AND checkttmststatus<>'Cancel' AND apm.checkttmstoid<>" + checkttmstoid + ") ";
            tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();           

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSuppSalesData(int suppoid)
        {
            List<listsuppsales> tbl = new List<listsuppsales>();

            sSql = "SELECT * FROM ( SELECT suppoid,suppdtl2oid suppsalesoid, suppdtl2picname suppsalesname, suppdtl2phone1 suppsalesphone, suppdtl2email suppsalesemail, suppdtl2jabatan suppsalesjabatan FROM QL_mstsuppdtl2 UNION ALL SELECT suppoid,suppdtl3oid suppsalesoid, suppdtl3picname suppsalesname, suppdtl3phone1 suppsalesphone, suppdtl3email suppsalesemail, suppdtl3jabatan suppsalesjabatan FROM QL_mstsuppdtl3 ) GG where suppoid = " + suppoid +" ";
            tbl = db.Database.SqlQuery<listsuppsales>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(int suppoid)
        {
            List<poitem> tbl = new List<poitem>();
            sSql = "SELECT 0 checkttdtlseq, mrm.mritemmstoid mrmstoid, mrm.mritemno mrno, CONVERT(varchar(20), mrm.mritemdate, 103) mrdate, pom.poitemmstoid, pom.poitemno, poitemdate, CONVERT(varchar(20), poitemdate, 103) poitemdatestr,'' checkttlengkap, '' checkttfaktur, '' checkttkwitansi, '' checkttsj, poitemmstnote, 'QL_trnpoitemmst' potype FROM QL_trnpoitemmst pom INNER JOIN QL_trnmritemmst mrm ON mrm.pomstoid=pom.poitemmstoid WHERE pom.cmpcode = '" + CompnyCode + "' AND pom.suppoid = " + suppoid + " AND pom.poitemmststatus IN ('Approved','Closed') /*AND (pom.poitemgrandtotalamt - ISNULL((SELECT SUM(cd.checkttdtlamt) FROM QL_trncheckttdtl cd WHERE cd.poitemmstoid=pom.poitemmstoid AND cd.potype<>'QL_trnpoassetmst'),0.0)) > 0*/ ";
            sSql += " UNION ALL  SELECT 0 checkttdtlseq, mrm.mrassetmstoid mrmstoid, mrm.mrassetno mrno, CONVERT(varchar(20), mrm.mrassetdate, 103) mrdate, pom.poassetmstoid poitemmstoid, pom.poassetno poitemno, poassetdate poitemdate, CONVERT(varchar(20), poassetdate, 103) poitemdatestr,'' checkttlengkap, '' checkttfaktur, '' checkttkwitansi, '' checkttsj, poassetmstnote poitemmstnote, 'QL_trnpoassetmst' potype FROM QL_trnpoassetmst pom INNER JOIN QL_trnmrassetmst mrm ON mrm.poassetmstoid=pom.poassetmstoid WHERE pom.cmpcode = '" + CompnyCode + "' AND pom.suppoid = " + suppoid + " AND pom.poassetmststatus IN ('Approved','Closed') /*AND (pom.poassetgrandtotalamt - ISNULL((SELECT SUM(cd.checkttdtlamt) FROM QL_trncheckttdtl cd WHERE cd.poitemmstoid=pom.poassetmstoid AND cd.potype='QL_trnpoassetmst'),0.0)) > 0*/ ";

            tbl = db.Database.SqlQuery<poitem>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private void InitDDL(QL_trncheckttmst tbl)
        {
           sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var checkttpaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.checkttpaytypeoid);
            ViewBag.checkttpaytypeoid = checkttpaytypeoid;   
        }

        private void FillAdditionalField(QL_trncheckttmst tblmst)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE suppoid ='" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.supppengakuan = db.Database.SqlQuery<string>("SELECT CASE WHEN supppengakuan='TT' THEN 'Check TT' ELSE 'Penerimaan' END supppengakuan FROM QL_mstsupp WHERE suppoid ='" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.suppsalesname = db.Database.SqlQuery<string>("SELECT suppsalesname FROM ( SELECT suppdtl2oid suppsalesoid ,suppdtl2picname suppsalesname FROM QL_mstsuppdtl2 UNION ALL SELECT suppdtl3oid suppsalesoid, suppdtl3picname suppsalesname FROM QL_mstsuppdtl3 ) GG where suppsalesoid = " + tblmst.suppsalesoid + "").FirstOrDefault();
            ViewBag.gnother1 = db.Database.SqlQuery<int>("SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid ='" + tblmst.checkttpaytypeoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
     
        [HttpPost]
        public ActionResult InitDueDate(int paytype, string tgldue)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select dateadd(D, " + paytype + ",'"+ DateTime.Parse(ClassFunction.toDate(tgldue)) + "') duedate,CONVERT(char(20),dateadd(D, " + paytype +",'"+ DateTime.Parse(ClassFunction.toDate(tgldue)) + "'),103) duedatestr ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
   

        [HttpPost]
        public ActionResult GetDetailData(int mrmstoid, int poitemmstoid, string potype)
        {
            List<listcheckttdtl> tbl = new List<listcheckttdtl>();
            if (potype == "QL_trnpoassetmst")
            {
                sSql += "SELECT 0 checkttdtlseq, 0 checkttdtloid, mrassetmstoid mrmstoid, mrassetno mrno, pom.poassetmstoid poitemmstoid, '' apitemno, (pom.poassetgrandtotalamt - ISNULL((SELECT SUM(cd.checkttdtlamt) FROM QL_trncheckttdtl cd WHERE cd.poitemmstoid=pom.poassetmstoid AND cd.potype='QL_trnpoassetmst'),0.0)) AS poitemgrandtotal, poassetdate poitemdate, CONVERT(varchar(20), poassetdate, 103) apitemdatestr,'' checkttlengkap, '' checkttfaktur, '' checkttkwitansi, '' checkttsj, pom.poassetmstoid poitemmstoid, pom.poassetno poitemno, 0 rabmstoid, '' rabno, 0 soitemmstoid, '' soitemno, poassetmstnote poitemmstnote, 'QL_trnpoassetmst' potype FROM QL_trnpoassetmst pom INNER JOIN QL_trnmrassetmst mrm ON mrm.poassetmstoid=pom.poassetmstoid WHERE pom.cmpcode = '" + CompnyCode + "' AND pom.poassetmstoid = " + poitemmstoid + " AND mrm.mrassetmstoid = " + mrmstoid + " /*AND (pom.poassetgrandtotalamt - ISNULL((SELECT SUM(cd.checkttdtlamt) FROM QL_trncheckttdtl cd WHERE cd.poitemmstoid=pom.poassetmstoid AND cd.potype='QL_trnpoassetmst'),0.0)) > 0*/ ";
            }
            else
            {
                sSql = "SELECT 0 checkttdtlseq, 0 checkttdtloid, mritemmstoid mrmstoid, mritemno mrno, pom.poitemmstoid, '' apitemno, (pom.poitemgrandtotalamt - ISNULL((SELECT SUM(cd.checkttdtlamt) FROM QL_trncheckttdtl cd WHERE cd.poitemmstoid=pom.poitemmstoid AND cd.potype<>'QL_trnpoassetmst'),0.0)) AS poitemgrandtotal, poitemdate, CONVERT(varchar(20), poitemdate, 103) apitemdatestr,'' checkttlengkap, '' checkttfaktur, '' checkttkwitansi, '' checkttsj, pom.poitemmstoid, pom.poitemno, ISNULL((SELECT som.rabmstoid FROM QL_trnrabmst som WHERE som.rabmstoid=pom.rabmstoid),0) rabmstoid, ISNULL((SELECT som.rabno FROM QL_trnrabmst som WHERE som.rabmstoid=pom.rabmstoid),'') rabno, ISNULL((SELECT som.soitemmstoid FROM QL_trnsoitemmst som WHERE som.soitemmstoid=pom.somstoid),0) soitemmstoid, ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.soitemmstoid=pom.somstoid),'') soitemno, poitemmstnote, 'QL_trnpoitemmst' potype FROM QL_trnpoitemmst pom INNER JOIN QL_trnmritemmst mrm ON mrm.pomstoid=pom.poitemmstoid WHERE pom.cmpcode = '" + CompnyCode + "' AND pom.poitemmstoid = " + poitemmstoid + " AND mrm.mritemmstoid = " + mrmstoid + " /*AND (pom.poitemgrandtotalamt - ISNULL((SELECT SUM(cd.checkttdtlamt) FROM QL_trncheckttdtl cd WHERE cd.poitemmstoid=pom.poitemmstoid AND cd.potype<>'QL_trnpoassetmst'),0.0)) > 0*/ ";
            }

            tbl = db.Database.SqlQuery<listcheckttdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
    

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
      
        // GET/POST: checktt
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            string sfilter = "";
            sSql = "select pr.checkttmstoid, pr.checkttno, pr.checkttdate, pr.checktttype, c.suppname, pr.checkttgrandtotal grandtotal, pr.checkttnote, pr.checkttmststatus FROM QL_trncheckttmst pr INNER JOIN QL_mstsupp c ON c.suppoid=pr.suppoid WHERE pr.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND pr.checkttdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND pr.checkttdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND pr.checkttmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY pr.checkttdate";
            List<listchecktt> tblmst = db.Database.SqlQuery<listchecktt>(sSql).ToList();
            return View(tblmst);
        }


        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncheckttmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trncheckttmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();                
                tblmst.checkttdate = ClassFunction.GetServerTime();                
                tblmst.checkttmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_trncheckttdtl"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trncheckttmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT cd.checkttdtlseq, cd.checkttdtloid, cd.apitemmstoid, cd.apitemno, cd.checkttdtlamt poitemgrandtotal, cd.checkttdtllengkap checkttlengkap, cd.checkttdtlfaktur checkttfaktur, cd.checkttdtlkwitansi checkttkwitansi, cd.checkttdtlsj checkttsj, cd.mrmstoid, CASE WHEN cd.potype='QL_trnpoassetmst' THEN (SELECT pom.mrassetno FROM QL_trnmrassetmst pom WHERE pom.mrassetmstoid=cd.mrmstoid) ELSE (SELECT pom.mritemno FROM QL_trnmritemmst pom WHERE pom.mritemmstoid=cd.mrmstoid) END mrno, cd.poitemmstoid, CASE WHEN cd.potype='QL_trnpoassetmst' THEN (SELECT pom.poassetno FROM QL_trnpoassetmst pom WHERE pom.poassetmstoid=cd.poitemmstoid) ELSE (SELECT pom.poitemno FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid=cd.poitemmstoid) END poitemno, cd.soitemmstoid, CASE WHEN cd.potype='QL_trnpoassetmst' THEN '' ELSE (SELECT pom.soitemno FROM QL_trnsoitemmst pom WHERE pom.soitemmstoid=cd.soitemmstoid) END soitemno, cd.rabmstoid, CASE WHEN cd.potype='QL_trnpoassetmst' THEN '' ELSE (SELECT pom.rabno FROM QL_trnrabmst pom WHERE pom.rabmstoid=cd.rabmstoid) END rabno, cd.checkttdtlnote, cd.potype FROM QL_trncheckttdtl cd WHERE cd.cmpcode = '" + CompnyCode +"' AND cd.checkttmstoid = "+ id +" ORDER BY cd.checkttdtlseq";
                Session["QL_trncheckttdtl"] = db.Database.SqlQuery<listcheckttdtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncheckttmst tblmst, string action, string tglmst, string tgldate2, string tgldue)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tblmst.checkttmststatus == "Post")
            {
                tblmst.checkttno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tblmst.checkttno = "";
            }            
            try
            {
                tblmst.checkttdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("checkttdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.checkttdate2 = DateTime.Parse(ClassFunction.toDate(tgldate2));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("checkttdate2", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.checkttduedate = DateTime.Parse(ClassFunction.toDate(tgldue));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("checkttduedate", "Format Tanggal Jatuh Tempo Tidak Valid!!" + ex.ToString());
            }           
           
            if (string.IsNullOrEmpty(tblmst.checkttnote))
                tblmst.checkttnote = "";           
            if (string.IsNullOrEmpty(tblmst.resfield1))
                tblmst.resfield1 = "";
            if (string.IsNullOrEmpty(tblmst.resfield2))
                tblmst.resfield2 = "";
            if (string.IsNullOrEmpty(tblmst.resfield3))
                tblmst.resfield3= "";           


            //is Input Detail Valid
            List<listcheckttdtl> dtDtl = (List<listcheckttdtl>)Session["QL_trncheckttdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].checkttdtlnote))
                            dtDtl[i].checkttdtlnote = "";
                        if (string.IsNullOrEmpty(dtDtl[i].apitemno))
                            ModelState.AddModelError("apitemno", "Silahkan isi No.Faktur!");
                        if ((dtDtl[i].poitemgrandtotal == 0 || string.IsNullOrEmpty(dtDtl[i].poitemgrandtotal.ToString())))
                            ModelState.AddModelError("poitemgrandtotal", "Nominal Faktur harus lebih dari 0!");
                    }
                    if (tblmst.checkttmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tblmst.checkttno))
                            ModelState.AddModelError("checkttno", "Silahkan isi No. Faktur!");
                        else if (db.QL_trncheckttmst.Where(w => w.checkttno == tblmst.checkttno & w.checkttmstoid != tblmst.checkttmstoid).Count() > 0)
                            ModelState.AddModelError("checkttno", "No. Faktur yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");                        
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.checkttdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.checkttmststatus = "In Process";
            }
            if (tblmst.checkttmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.checkttmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    var servertime = ClassFunction.GetServerTime();
                    var mstoid = ClassFunction.GenerateID("QL_trncheckttmst");
                    var dtloid = ClassFunction.GenerateID("QL_trncheckttdtl");
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.checkttmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trncheckttmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trncheckttmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();

                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trncheckttdtl.Where(a => a.checkttmstoid == tblmst.checkttmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trncheckttdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trncheckttdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++) 
                        {
                            tbldtl = new QL_trncheckttdtl();
                            tbldtl.cmpcode = CompnyCode;
                            tbldtl.checkttdtlseq = i + 1;
                            tbldtl.checkttdtloid = dtloid++;
                            tbldtl.checkttmstoid = tblmst.checkttmstoid;
                            tbldtl.apitemmstoid = 0;
                            tbldtl.apitemno = dtDtl[i].apitemno;
                            tbldtl.checkttdtllengkap = dtDtl[i].checkttlengkap;
                            tbldtl.checkttdtlfaktur = dtDtl[i].checkttfaktur;
                            tbldtl.checkttdtlkwitansi = dtDtl[i].checkttkwitansi;
                            tbldtl.checkttdtlsj = dtDtl[i].checkttsj;
                            tbldtl.checkttdtlamt = dtDtl[i].poitemgrandtotal;
                            tbldtl.poitemmstoid = dtDtl[i].poitemmstoid;
                            tbldtl.potype = dtDtl[i].potype;
                            tbldtl.soitemmstoid = dtDtl[i].soitemmstoid;
                            tbldtl.rabmstoid = dtDtl[i].rabmstoid;
                            tbldtl.checkttdtlnote = dtDtl[i].checkttdtlnote;
                            tbldtl.checkttdtlstatus = "";                            
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.mrmstoid = dtDtl[i].mrmstoid;

                            db.QL_trncheckttdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trncheckttdtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();                            
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.checkttmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.checkttmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncheckttmst tblmst = db.QL_trncheckttmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                    

                        var trndtl = db.QL_trncheckttdtl.Where(a => a.checkttmstoid == id);
                        db.QL_trncheckttdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncheckttmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        [HttpPost, ActionName("SaveRev")]
        [ValidateAntiForgeryToken]
        public ActionResult SavedConfirmed(int id, string type, string note)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncheckttmst tbl = db.QL_trncheckttmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();
            List<listcheckttdtl> dtDtl = (List<listcheckttdtl>)Session["QL_trncheckttdtl"];

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trncheckttmst SET checktttype = '"+ type + "', checkttnote = '" + ClassFunction.Tchar(note) + "' where checkttmstoid = " + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count() > 0)
                            {
                                QL_trncheckttdtl tbldtl;

                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    int dtloid = dtDtl[i].checkttdtloid;
                                    tbldtl = db.QL_trncheckttdtl.Where(x => x.checkttdtloid == dtloid).FirstOrDefault();
                                    if (tbldtl != null)
                                    {
                                        tbldtl.checkttdtllengkap = dtDtl[i].checkttlengkap;
                                        tbldtl.checkttdtlfaktur = dtDtl[i].checkttfaktur;
                                        tbldtl.checkttdtlkwitansi = dtDtl[i].checkttkwitansi;
                                        tbldtl.checkttdtlsj = dtDtl[i].checkttsj;
                                        db.Entry(tbldtl).State = EntityState.Modified;
                                        db.SaveChanges();
                                    } 
                                }
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptCheckTT.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE cm.cmpcode='" + CompnyCode + "' AND cm.checkttmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptCheckTT.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}