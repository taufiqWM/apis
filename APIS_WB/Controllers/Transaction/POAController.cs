﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class POAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public POAController()
        {
            db.Database.CommandTimeout = 0;
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "POA/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(poassetno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoassetmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND poassetno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private void InitDDL(QL_trnpoassetmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='PAYMENT TERM' AND gnflag='ACTIVE'";
            var poassetpaytype = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.poassetpaytypeoid);
            ViewBag.poassetpaytypeoid = poassetpaytype;

            if (tbl.poassetmstoid > 0)
            {
                sSql = "SELECT poassetdtllocoid FROM QL_trnpoassetdtl WHERE poassetmstoid='" + tbl.poassetmstoid + "'";
                ViewBag.kirimke = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='ALAMAT' AND gnflag='ACTIVE' ORDER BY gndesc";
            var kirimke = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.kirimke);
            ViewBag.kirimke = kirimke;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
        }

        private void FillAdditionalField(QL_trnpoassetmst tbl)
        {
            ViewBag.revpoassetno = db.Database.SqlQuery<string>("SELECT poassetno FROM QL_trnpoassetmst r WHERE r.poassetmstoid ='" + tbl.revpoassetmstoid + "'").FirstOrDefault();
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT suppdtl1addr FROM QL_mstsuppdtl1 a WHERE a.suppdtl1oid ='" + tbl.alamatoid + "'").FirstOrDefault();
            ViewBag.potype = tbl.poassettype;
            ViewBag.potaxtype = tbl.poassettaxtype;
        }

        [HttpPost]
        public ActionResult GetRevPOData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT pom.*, '' poassetnonew, s.suppname, ISNULL((SELECT suppdtl1addr FROM QL_mstsuppdtl1 a WHERE a.suppdtl1oid  = pom.alamatoid),'') alamat FROM QL_trnpoassetmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE poassetmststatus IN ('Closed') AND ISNULL(poassetmstres1,'')='Force Closed' ORDER BY poassetdate DESC";
                var tbl = new ClassConnection().GetDataTable(sSql, "tbl");
                if (tbl == null || tbl.Rows.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    foreach (DataRow row in tbl.Rows)
                    {
                        string sNo = ClassFunction.Left(row["poassetno"].ToString(), 15) + "-R";
                        sSql = "SELECT ISNULL(MAX(CAST(REPLACE(poassetno, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND poassetno LIKE '" + sNo + "%'";
                        string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                        sNo = sNo + sCounter;

                        row["poassetnonew"] = sNo;
                    }
                    js = Json(new { result = "", tbl = ClassFunction.toObject(tbl) }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT suppoid, suppcode, suppname,supppaymentoid, suppaddr, (CASE supppajak WHEN 1 THEN 'TAX' ELSE 'NON TAX' END) supptaxable, (CASE supppajak WHEN 1 THEN CAST(ISNULL((SELECT TOP 1 gndesc FROM QL_m05GN WHERE gngroup='DEFAULT TAX'),'10') AS DECIMAL(18,2)) ELSE 0.0 END) supptaxamt, gp.gndesc supppayment, 1 curroid FROM QL_mstsupp s INNER JOIN QL_m05GN gp ON gp.gnoid=s.supppaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND s.activeflag='ACTIVE' ORDER BY suppcode DESC";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatData(int suppoid)
        {
            JsonResult js = null;
            try
            {
                sSql = $"SELECT suppdtl1oid alamatoid, suppdtl1addr alamat, suppcityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi, c.suppdtl1phone phone FROM QL_mstsuppdtl1 c INNER JOIN QL_m05GN g ON g.gnoid=c.suppcityoid WHERE c.suppoid = " + suppoid + " ORDER BY alamatoid";
                var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }
        public class trnpoassetdtl : QL_trnpoassetdtl
        {
            public string poassetcode { get; set; }
            public string poassetdesc { get; set; }
            public string poassetunit { get; set; }
            public decimal lastpoprice { get; set; }
            public string itemtype { get; set; }
            public string prno { get; set; }
            public decimal prqty { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(string sType, int suppoid, string ddlpr, int poassetmstoid)
        {
            var result = "";
            List<trnpoassetdtl> tbl = new List<trnpoassetdtl>();
            var taxval = db.Database.SqlQuery<decimal>($"SELECT (CASE supppajak WHEN 1 THEN CAST(ISNULL((SELECT TOP 1 gndesc FROM QL_m05GN WHERE gngroup='DEFAULT TAX'),'10') AS DECIMAL(18,2)) ELSE 0.0 END) supptaxamt FROM QL_mstsupp WHERE suppoid={suppoid}").FirstOrDefault();

            string sPlus = "";
            if (sType == "Asset")
            {
                sPlus = " AND i.itemtype IN ('Asset')";
            }
            else
            {
                sPlus = " AND i.itemtype IN ('Barang','Rakitan')";
            }

            if (ddlpr == "pr")
            {
                sSql = $"SELECT 0 poassetdtlseq, itemoid AS poassetrefoid, itemcode AS poassetcode, itemdesc AS poassetdesc, (prqty - ISNULL((SELECT SUM(poassetqty) FROM QL_trnpoassetdtl x WHERE x.prdtloid=prd.prdtloid AND x.poassetmstoid<>{poassetmstoid}),0.0)) poassetqty, itemunitoid AS poassetunitoid, g.gndesc poassetunit, prprice poassetprice, prdtlamt poassetdtlamt, 'P' poassetdtldisctype, 0.0 poassetdtldiscvalue, 0.0 poassetdtldiscamt, prdtlamt poassetdtlnetto, GETDATE() poassetdtleta, 0 poassetdtllocoid, {taxval} poassetdtltaxvalue, 0.0 poassetdtltaxamt, i.itemtype +'/'+ i.pidtype itemtype, prd.prdtlnote poassetdtlnote, prd.prmstoid, prd.prdtloid, prm.prno, (prqty - ISNULL((SELECT SUM(poassetqty) FROM QL_trnpoassetdtl x WHERE x.prdtloid=prd.prdtloid AND x.poassetmstoid<>{poassetmstoid}),0.0)) prqty FROM QL_trnprmst prm INNER JOIN QL_trnprdtl prd ON prd.prmstoid=prm.prmstoid INNER JOIN QL_mstitem i ON i.itemoid=prd.prrefoid INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' AND prm.prmststatus IN('Post','Approved') AND prm.prtype='Barang' AND ISNULL(prd.prdtlstatus,'')='' {sPlus} ORDER BY itemcode";
            }
            else
            {
                sSql = $"SELECT 0 poassetdtlseq, itemoid AS poassetrefoid, itemcode AS poassetcode, itemdesc AS poassetdesc, 0.0 poassetqty, itemunitoid AS poassetunitoid, g.gndesc poassetunit, 0.0 poassetprice, 0.0 poassetdtlamt, 'P' poassetdtldisctype, 0.0 poassetdtldiscvalue, 0.0 poassetdtldiscamt, 0.0 poassetdtlnetto, GETDATE() poassetdtleta, 0 poassetdtllocoid, {taxval} poassetdtltaxvalue, 0.0 poassetdtltaxamt, i.itemtype +'/'+ i.pidtype itemtype, '' poassetdtlnote, 0 prmstoid, 0 prdtloid, '' prno, 0.0 prqty FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' {sPlus} ORDER BY itemcode";
            }
            tbl = db.Database.SqlQuery<trnpoassetdtl>(sSql).ToList();

            if (tbl != null && tbl.Count > 0)
            {
                foreach (var item in tbl)
                {
                    if (ddlpr == "pr")
                    {
                        if (taxval > 0)
                        {
                            item.poassetdtltaxamt = item.poassetdtlamt * (taxval / 100);
                            item.poassetdtlnetto = item.poassetdtlamt + item.poassetdtltaxamt;
                        }
                    }
                }
            }

            JsonResult js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<trnpoassetdtl>();

            try
            {
                sSql = $"SELECT pod.poassetdtlseq, pod.poassetreftype, pod.poassetrefoid, m.itemcode AS poassetcode, m.itemdesc AS poassetdesc, pod.poassetqty,  pod.poassetunitoid, g.gndesc AS poassetunit, pod.poassetprice, pod.poassetdtlamt, pod.poassetdtldisctype, pod.poassetdtldiscvalue, pod.poassetdtldiscamt, pod.poassetdtlnetto, ISNULL(pod.poassetdtlnote,'') poassetdtlnote, 0.0 AS lastpoprice, pod.poassetdtleta, pod.poassetdtllocoid, g2.gndesc poassetdtlloc, pod.poassetdtltaxvalue, pod.poassetdtltaxamt, m.itemtype +'/'+ m.pidtype itemtype, ISNULL(pod.prmstoid,0) prmstoid, ISNULL(pod.prdtloid,0) prdtloid, (ISNULL(prqty,0.0) - ISNULL((SELECT SUM(poassetqty) FROM QL_trnpoassetdtl x WHERE ISNULL(x.prdtloid,0)=ISNULL(pod.prdtloid,0) AND x.poassetmstoid<>pod.poassetmstoid),0.0)) prqty, ISNULL(prm.prno,'') prno FROM QL_trnpoassetdtl pod INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode = pod.cmpcode AND pom.poassetmstoid = pod.poassetmstoid INNER JOIN QL_mstitem m ON pod.poassetrefoid = m.itemoid AND m.itemtype<>'Ongkir' INNER JOIN QL_m05GN g ON pod.poassetunitoid = g.gnoid INNER JOIN QL_m05GN g2 ON pod.poassetdtllocoid = g2.gnoid LEFT JOIN QL_trnprdtl prd ON prd.prdtloid=ISNULL(pod.prdtloid,0) LEFT JOIN QL_trnprmst prm ON prm.prmstoid=ISNULL(pod.prmstoid,0) WHERE pod.cmpcode='{CompnyCode}' AND pod.poassetmstoid={id} ORDER BY pod.poassetdtlseq";
                dtl = db.Database.SqlQuery<trnpoassetdtl>(sSql).ToList();

                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: POFixedAssets
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No PO",["Supplier"] = "Supplier"}, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT pom.cmpcode, poassetmstoid, poassetno, poassetdate, tipebarang, suppname, poassetmststatus, poassetmstnote, CASE WHEN pom.poassetmststatus = 'Revised' THEN revisereason WHEN pom.poassetmststatus = 'Rejected' THEN rejectreason ELSE '' END reason, pom.poassetgrandtotalamt, ISNULL(pom.poassetsuppref,'') poassetsuppref, pom.pocustname FROM QL_trnpoassetmst pom INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE s.cmpcode='" + CompnyCode + "') AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.poassetdate >='" + param.filterperiodfrom + " 00:00:00' AND t.poassetdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.poassetmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.poassetno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.poassetmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.poassetmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.poassetmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.poassetmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: POFixedAssets/Form/5/11
        public ActionResult Form(int? id, int? revid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpoassetmst tbl;
            string action = "Create";
            ViewBag.adddisc = "";
            if (id == null && revid == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trnpoassetmst();
                tbl.poassetmstoid = ClassFunction.GenerateID("QL_trnpoassetmst");
                tbl.poassetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.poassetmststatus = "In Process";
                tbl.revpoassettype = "Baru";
                tbl.revpoassetmstoid = 0;
                tbl.suppoid = 0;
                tbl.alamatoid = 0;

                ViewBag.poassetdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else if (id != null && revid != null)//AddDiskon
            {
                action = "Create";
                ViewBag.uid = Guid.NewGuid();
                tbl = db.QL_trnpoassetmst.Find(CompnyCode, revid);

                string pono = db.Database.SqlQuery<string>("SELECT poassetno FROM QL_trnpoassetmst r WHERE r.poassetmstoid ='" + revid + "'").FirstOrDefault();

                string sNo = ClassFunction.Left(pono, 15) + "-R";
                sSql = "SELECT ISNULL(MAX(CAST(REPLACE(poassetno, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpoassetmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND poassetno LIKE '" + sNo + "%'";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                sNo = sNo + sCounter;

                tbl.poassetno = sNo;
                tbl.revpoassettype = "Revisi";
                tbl.revpoassetmstoid = revid.Value;
                tbl.poassetmststatus = "In Process";
                ViewBag.revpoassetno = pono;
                ViewBag.adddisc = "adddisc";
                ViewBag.poassetdate = tbl.poassetdate.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnpoassetmst.Find(CompnyCode, id);

                ViewBag.poassetdate = tbl.poassetdate.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: POFixedAssets/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpoassetmst tbl, List<trnpoassetdtl> dtDtl, string action, string adddisc)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            if (tbl.cmpcode == null)
                tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.poassetno))
                tbl.poassetno = "";
            if (string.IsNullOrEmpty(tbl.pocustname))
                tbl.pocustname = "";
            if (string.IsNullOrEmpty(tbl.poassetmstnote))
                tbl.poassetmstnote = "";

            tbl.poassettype = tbl.tipebarang;
            tbl.rateoid = 0;
            tbl.poassetratetoidr = 0;
            tbl.poassetratetoidrchar = "0";
            tbl.poassetratetousdchar = "0";
            tbl.poassetrate2toidrchar = "0";
            tbl.poassetrate2tousdchar = "0";

            if (dtDtl == null)
                msg += "- Please fill detail data!<br />";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br />";
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].poassetqty <= 0)
                        {
                            msg += "- PO QTY for Material " + dtDtl[i].poassetdesc + " must be more than 0!!<br />";
                        }

                        if (dtDtl[i].poassetprice <= 0)
                        {
                            msg += "- PO Price for Material " + dtDtl[i].poassetdesc + " must be more than 0!!<br />";
                        }

                        //cek PR
                        if (dtDtl[i].prdtloid != 0)
                        {
                            decimal AmtOS = db.Database.SqlQuery<decimal>($"SELECT (prqty - ISNULL((SELECT SUM(poassetqty) FROM QL_trnpoassetdtl x WHERE x.prdtloid=prd.prdtloid AND x.poassetmstoid<>{tbl.poassetmstoid}),0.0)) amt FROM QL_trnprmst prm INNER JOIN QL_trnprdtl prd ON prd.prmstoid=prm.prmstoid WHERE prd.prdtloid={dtDtl[i].prdtloid}").FirstOrDefault();
                            dtDtl[i].prqty = AmtOS;
                            if (dtDtl[i].poassetqty > dtDtl[i].prqty)
                            {
                                msg += "- Qty " + dtDtl[i].poassetdesc + " Tidak Boleh Melebihi Qty PR!! <br>";
                            }
                        }
                    }
                }
            }

            //Variable Send Approval
            string ctrlname = "POA";
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.poassetmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    msg += "- Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya! <br>";
                }
            }

            if (action == "Create")
            {
                if (tbl.poassetno != "")
                {
                    if (db.QL_trnpoassetmst.Where(x => x.poassetno == tbl.poassetno).Count() > 0)
                    {
                        msg += "- No PO Sudah Digunakan PO Lain, Silahkan Ganti No PO!<br>";
                    }
                }
            }
            else
            {

                if (tbl.poassetno != "")
                {
                    if (db.QL_trnpoassetmst.Where(x => x.poassetno == tbl.poassetno && x.poassetmstoid != tbl.poassetmstoid).Count() > 0)
                    {
                        msg += "- No PO Sudah Digunakan PO Lain, Silahkan Ganti No PO!<br>";
                    }
                }
            }

            if (adddisc == "adddisc")
            {
                decimal amtRevPO = db.Database.SqlQuery<decimal>("SELECT poassetgrandtotalamt FROM QL_trnpoassetmst WHERE poassetmstoid =" + tbl.revpoassetmstoid + "").FirstOrDefault();
                if (tbl.poassetgrandtotalamt >= amtRevPO)
                {
                    msg += "- Total PO Harus Lebih Kecil Dari Rev PO!!<br>";
                }
            }

            //var servertime = ClassFunction.GetServerTime();
            var cRate = new ClassRate();
            if (tbl.poassetmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.poassetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }
            if (tbl.poassetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                }
            }

            if (tbl.poassetmststatus == "Revised")
                tbl.poassetmststatus = "In Process";

            if (msg == "")
            {
                tbl.rate2oid = cRate.GetRateMonthlyOid;
                tbl.poassetrate2toidr = cRate.GetRateMonthlyIDRValue;
                tbl.poassettotalamtidr = tbl.poassettotalamt * cRate.GetRateMonthlyIDRValue;
                tbl.poassettotalnettoidr = tbl.poassettotalnetto * cRate.GetRateMonthlyIDRValue;
                tbl.poassetgrandtotalamtidr = tbl.poassetgrandtotalamt * cRate.GetRateMonthlyIDRValue;

                var mstoid = ClassFunction.GenerateID("QL_trnpoassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpoassetdtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trnpoassetmst.Find(tbl.cmpcode, tbl.poassetmstoid) != null)
                                tbl.poassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.poassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnpoassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.poassetmstoid + " WHERE tablename='QL_trnpoassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //--
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //update PR
                            sSql = "UPDATE QL_trnprdtl SET prdtlstatus='' WHERE prdtloid IN (SELECT ISNULL(prdtloid,0) FROM QL_trnpoassetdtl WHERE poassetmstoid=" + tbl.poassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnprmst SET prmststatus='Post' WHERE prmstoid IN (SELECT DISTINCT ISNULL(prmstoid,0) FROM QL_trnpoassetdtl WHERE poassetmstoid=" + tbl.poassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpoassetdtl.Where(a => a.poassetmstoid == tbl.poassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpoassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        if (tbl.revpoassettype == "Revisi")
                        {
                            sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Closed', poassetmstres1='Force Closed', closereason='" + (adddisc == "adddisc" ? "Add Diskon PO" : "Revisi PO") + "', closeuser='" + Session["UserID"].ToString() + "', closetime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid =" + tbl.revpoassetmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        QL_trnpoassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnpoassetdtl)ClassFunction.MappingTable(new QL_trnpoassetdtl(), dtDtl[i]);
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.poassetdtloid = dtloid++;
                            tbldtl.poassetmstoid = tbl.poassetmstoid;
                            tbldtl.poassetdtlseq = i + 1;
                            tbldtl.closeqty = 0;
                            tbldtl.poassetreftype = tbl.tipebarang;
                            tbldtl.poassetdtlstatus = "";
                            tbldtl.poassetdtldisctype = "P";
                            tbldtl.poassetpriceidr = dtDtl[i].poassetprice;
                            tbldtl.poassetdtlamtidr = dtDtl[i].poassetdtlamt;
                            tbldtl.poassetdtldiscamtidr = dtDtl[i].poassetdtldiscamt;
                            tbldtl.poassetdtlnettoidr = dtDtl[i].poassetdtlnetto;
                            tbldtl.poassetdtlres1 = "";
                            tbldtl.poassetdtlres2 = "";
                            tbldtl.poassetdtlres3 = "";
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnpoassetdtl.Add(tbldtl);
                            db.SaveChanges();

                            //update PR
                            if (dtDtl[i].prdtloid != 0)
                            {
                                if (dtDtl[i].poassetqty >= dtDtl[i].prqty)
                                {
                                    sSql = "UPDATE QL_trnprdtl SET prdtlstatus='Complete' WHERE prdtloid=" + dtDtl[i].prdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnprmst SET prmststatus='Closed' WHERE prmstoid=" + dtDtl[i].prmstoid + " AND (SELECT COUNT(prdtloid) FROM QL_trnprdtl WHERE prdtlstatus='' AND prmstoid=" + dtDtl[i].prmstoid + " AND prdtloid<>" + +dtDtl[i].prdtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        sSql = "UPDATE QL_id SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpoassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.poassetmststatus == "In Approval")
                        {
                            QL_APP tblApp = new QL_APP();
                            tblApp.cmpcode = tbl.cmpcode;
                            tblApp.appoid = appoid;
                            tblApp.appform = ctrlname;
                            tblApp.appformoid = tbl.poassetmstoid;
                            tblApp.requser = tbl.upduser;
                            tblApp.reqdate = servertime;
                            tblApp.appstoid = stoid_app;
                            tblApp.tablename = "QL_trnpoassetmst";
                            db.QL_APP.Add(tblApp);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + appoid + " WHERE tablename='QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.poassetmstoid.ToString();
                        sReturnNo = "No. " + tbl.poassetno;
                        if (tbl.poassetmststatus == "In Approval")
                        {
                            sReturnState = "Send Approval";
                        }
                        else
                        {
                            sReturnState = "Saved";
                        }
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/AddDiskon
        [HttpPost, ActionName("AddDiskon")]
        [ValidateAntiForgeryToken]
        public ActionResult AddDiskonConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            var cekPOPB = db.QL_trnmrassetmst.Where(m => m.poassetmstoid == id && m.mrassetmstres1 == "").Count();
            var cekPOFB = db.QL_trnapassetmst.Where(m => m.poassetmstoid == id && m.apassetmstres1 == "").Count();
            if (cekPOFB > 0 || cekPOPB > 0)
            {
                msg += "- PO Masih Belum Di buatkan Return! <BR>";
            }
            var cekRAB = (from rd in db.QL_trnrabdtl2 join rm in db.QL_trnrabmst on rd.rabmstoid equals rm.rabmstoid where rd.poassetmstoid == id && rm.rabmststatus != "Closed" select rd).Count();
            if (cekRAB > 0)
            {
                msg += "- PO Sudah di Tarik Di RAB! <BR>";
            }
            if (msg != "")
            {
                result = "failed";
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: PRAB/ChangeKrimKe
        [HttpPost, ActionName("ChangeKrimKe")]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeKrimKeConfirmed(int? id, int? idkirimke)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";

            if (msg != "")
            {
                result = "failed";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnpoassetdtl SET poassetdtllocoid=" + idkirimke + " WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + id + " ";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: POFixedAssets/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpoassetmst tbl = db.QL_trnpoassetmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            int porefid = 0;
            if (tbl.revpoassetmstoid > 0)
            {
                if (db.QL_trnpoassetmst.Where(x => x.poassetmstoid == tbl.revpoassetmstoid && x.closereason == "Add Diskon PO").Count() > 0)
                {
                    porefid = tbl.revpoassetmstoid;
                }
            }

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl.revpoassettype == "Revisi")
                        {
                            sSql = "UPDATE QL_trnpoassetmst SET poassetmstres1='', closereason='' WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + porefid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //update PR
                        sSql = "UPDATE QL_trnprdtl SET prdtlstatus='' WHERE prdtloid IN (SELECT ISNULL(prdtloid,0) FROM QL_trnpoassetdtl WHERE poassetmstoid=" + tbl.poassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnprmst SET prmststatus='Post' WHERE prmstoid IN (SELECT DISTINCT ISNULL(prmstoid,0) FROM QL_trnpoassetdtl WHERE poassetmstoid=" + tbl.poassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpoassetdtl.Where(a => a.poassetmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnpoassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpoassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.poassetno FROM QL_trnpoassetmst arm WHERE arm.poassetmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPOAssetItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.poassetmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptPOAssettem_" + sNo + ".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
