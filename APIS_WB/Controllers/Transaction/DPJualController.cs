﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class DPJualController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public DPJualController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class trndpar
        {
            public string cmpcode { get; set; }
            public int dparoid { get; set; }
            public string dparno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime dpardate { get; set; }
            public string custname { get; set; }
            public string sono { get; set; }
            public string acctgdesc { get; set; }
            public string dparpaytype { get; set; }
            public string dparstatus { get; set; }
            public string dparnote { get; set; }
            public string cashbankno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dparamt { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custtaxable { get; set; }
            public decimal custtaxvalue { get; set; }
        }

        public class listso
        {
            public int somstoid { get; set; }
            public string soreftype { get; set; }
            public string sono { get; set; }
            public DateTime sodate { get; set; }
            public string projectname { get; set; }
            public string rabno { get; set; }
            public string somstnote { get; set; }
            public int curroid { get; set; }
            public string departemen { get; set; }
            public Decimal sograndtotalamt { get; set; }
        }

        private void InitDDL(QL_trndpar tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";

            var dparpayacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dparpayacctgoid);
            ViewBag.dparpayacctgoid = dparpayacctgoid;
            var addacctgoid1 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid1);
            ViewBag.addacctgoid1 = addacctgoid1;
            var addacctgoid2 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid2);
            ViewBag.addacctgoid2 = addacctgoid2;
            var addacctgoid3 = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.addacctgoid3);
            ViewBag.addacctgoid3 = addacctgoid3;
        }

        [HttpPost]
        public ActionResult BindCustomerData()
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT custoid, custcode, custname, custaddr, (CASE custtaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS custtaxable, ISNULL((SELECT TOP 1 CAST (gndesc AS DECIMAL) FROM QL_m05GN WHERE cmpcode='"+ CompnyCode + "' AND activeflag='ACTIVE' AND gngroup='DEFAULT TAX' ORDER BY updtime DESC), 0) custtaxvalue FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY custcode";
            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(int custoid, decimal dparaccumamt, string dparstatus, string soreftype, int somstoid)
        {
            List<listso> tbl = new List<listso>();
            if (dparaccumamt == 0 && dparstatus == "Post" && soreftype == "QL_trnsoitemmst")
            {
                string projectname = db.Database.SqlQuery<string>("SELECT rm.projectname FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid WHERE som.soitemmstoid=" + somstoid).FirstOrDefault();

                sSql = "SELECT pm.soitemmstoid somstoid, 'QL_trnsoitemmst' soreftype, pm.soitemno sono, ISNULL((SELECT r. projectname FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') projectname, ISNULL((SELECT r. rabno FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') rabno, pm.soitemdate sodate,  pm.soitemmstnote somstnote, pm.curroid, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid=pm.rabmstoid),'') departemen, (pm.soitemgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=pm.soitemmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')=''),0.0)) sograndtotalamt FROM QL_trnsoitemmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.custoid= " + custoid + " AND soitemmststatus IN('Approved','Closed','Post') AND ISNULL(soitemmstres1,'')='' AND (pm.soitemgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=pm.soitemmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')=''),0.0)) > 0 AND ISNULL((SELECT r. projectname FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') = '" + projectname + "' ";
            }
            else
            {
                sSql = "SELECT pm.soitemmstoid somstoid, 'QL_trnsoitemmst' soreftype, pm.soitemno sono, ISNULL((SELECT r. projectname FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') projectname, ISNULL((SELECT r. rabno FROM QL_trnrabmst r WHERE r.rabmstoid=pm.rabmstoid),'') rabno, pm.soitemdate sodate,  pm.soitemmstnote somstnote, pm.curroid, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid=pm.rabmstoid),'') departemen, (pm.soitemgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=pm.soitemmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')=''),0.0)) sograndtotalamt FROM QL_trnsoitemmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.custoid= " + custoid + " AND soitemmststatus IN('Approved','Closed','Post') /*AND pm.soitemmstoid NOT IN(SELECT apm.somstoid FROM QL_trnaritemmst apm)*/ AND (pm.soitemgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=pm.soitemmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')=''),0.0)) > 0";
                sSql += " UNION ALL  SELECT pm.soassetmstoid somstoid, 'QL_trnsoassetmst' soreftype, pm.soassetno sono, '' projectname, '' rabno, pm.soassetdate podate,  pm.soassetmstnote somstnote, pm.curroid, '' departemen, (pm.soassetgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoassetmst' AND dp.somstoid=pm.soassetmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')=''),0.0)) sograndtotalamt FROM QL_trnsoassetmst pm WHERE pm.cmpcode = '" + CompnyCode + "' AND pm.custoid= " + custoid + " AND soassetmststatus IN('Approved','Closed','Post') AND pm.soassetmstoid NOT IN(SELECT apm.somstoid FROM QL_trnarassetmst apm) AND (pm.soassetgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoassetmst' AND dp.somstoid=pm.soassetmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')=''),0.0)) > 0";
            }
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            if (sVar == "NONE")
            {
                sSql = "SELECT 'APIS' cmpcode, 0 acctgoid, '-' acctgcode, 'NONE' acctgdesc, '' acctgdbcr, '' acctggrp1, '' acctggrp2, '' acctggrp3, '' acctgnote, ''acctgres1, ''	 acctgres2, '' acctgres3, '' activeflag, '' createuser, getdate() createtime, '' upduser, getdate() updtime, 0 curroid, '' acctgcodeseq";
            }
            else
            {
                sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, CompnyCode) + ") ORDER BY acctgcode";
            }            
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string dparno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(dparno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndpar tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.dparpayrefno = db.Database.SqlQuery<string>("SELECT dparpayrefno FROM QL_trndpar WHERE cmpcode='" + tbl.cmpcode + "' AND dparoid=" + tbl.dparoid + "").FirstOrDefault();
            ViewBag.cashbankno = db.Database.SqlQuery<string>("SELECT cashbankno FROM QL_trncashbankmst WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + "").FirstOrDefault();
            ViewBag.cashbankamt = tbl.dparamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;
            if (tbl.soreftype == "QL_trnsoitemmst")
            {
                ViewBag.sono = db.Database.SqlQuery<string>("SELECT pm.soitemno FROM QL_trnsoitemmst pm WHERE pm.soitemmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                ViewBag.sograndtotalamt = db.Database.SqlQuery<decimal>("SELECT (pm.soitemgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=pm.soitemmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')='' AND dp.dparoid<>'" + tbl.dparoid + "'),0.0)) FROM QL_trnsoitemmst pm WHERE pm.soitemmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r INNER JOIN QL_trnsoitemmst pm ON pm.rabmstoid=r.rabmstoid WHERE pm.soitemmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_trnsoitemmst pm ON pm.rabmstoid=r.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE pm.soitemmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
            }
            else
            {
                ViewBag.sono = db.Database.SqlQuery<string>("SELECT pm.soassetno FROM QL_trnsoassetmst pm WHERE pm.soassetmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                ViewBag.sograndtotalamt = db.Database.SqlQuery<decimal>("SELECT (pm.soassetgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoasetmst' AND dp.somstoid=pm.soassetmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')='' AND dp.dparoid<>'" + tbl.dparoid + "'),0.0)) FROM QL_trnsoassetmst pm WHERE pm.soassetmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                ViewBag.projectname = ""; ViewBag.departemen = "";
            }
        }

        private string GenerateNo(string dpardate)
        {
            var dparno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(dpardate));
            if (CompnyCode != "")
            {
                string sNo = "DPAR/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dparno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" + CompnyCode + "' AND dparno LIKE '%" + sNo + "%' ";
                dparno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return dparno;
        }

        private string GenerateExpenseNo2(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (cmp != "" && cashbanktype != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateExpenseNo(string cmp, string cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            DateTime sDate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            if (cmp != "" && cashbanktype != "")
            {
                string sNo = cashbanktype + "/" + sDate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        // GET: dparMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT dp.cmpcode, dp.dparoid, dparno, dpardate, custname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE dparpaytype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'BANK' ELSE 'GIRO' END) AS dparpaytype, dparstatus, dparnote, 'False' AS checkvalue, ISNULL(cashbankno,'') cashbankno, dparamt, CASE WHEN dp.soreftype='QL_trnsoitemmst' THEN ISNULL((SELECT pm.soitemno FROM QL_trnsoitemmst pm WHERE pm.soitemmstoid=dp.somstoid),'') ELSE ISNULL((SELECT pm.soassetno FROM QL_trnsoassetmst pm WHERE pm.soassetmstoid=dp.somstoid),'') END sono FROM QL_trndpar dp INNER JOIN QL_mstcust s ON s.custoid=dp.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='"+ CompnyCode + "' AND dp.dparpaytype<>'DPFK' ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND dpardate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND dpardate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND dparstatus IN ('In Process')";
            }

            sSql += " ORDER BY CONVERT(DATETIME, dp.dpardate) DESC, dp.dparoid DESC";

            List<trndpar> dt = db.Database.SqlQuery<trndpar>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: dparMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpar tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trndpar();
                tbl.dparoid = ClassFunction.GenerateID("QL_trndpar");
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.dpardate = ClassFunction.GetServerTime();
                tbl.dparduedate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.soreftype = "";
                tbl.somstoid = 0;
                tbl.dparaccumamt = 0;
                tbl.dparstatus = "In Process";
                tbl.dpartakegiro = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trndpar.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dparMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndpar tbl, string action, string cashbankno, string tglmst, string tglduedate, string tgltakegiro)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.dpardate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dpardate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.dparduedate = DateTime.Parse(ClassFunction.toDate(tglduedate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dpardate", "Format Tanggal Due Date Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tbl.dpartakegiro = DateTime.Parse(ClassFunction.toDate(tgltakegiro));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dpartakegiro", "Format Tanggal Giro Tidak Valid!!" + ex.ToString());
            }

            if (tbl.dparno == null)
                tbl.dparno = "";
            string sErrReply = "";
            if (tbl.custoid == 0)
                ModelState.AddModelError("", "Please select CUSTOMER field!");
            if (tbl.acctgoid == 0)
                ModelState.AddModelError("", "Please select DP ACCOUNT field");
            if (tbl.dparpaytype == "0")
                ModelState.AddModelError("", "Please select Payment Type field");
            if (tbl.dparpayacctgoid == 0)
                ModelState.AddModelError("", "Please select PAYMENT ACCOUNT field!");
            if (tbl.dparpaytype != "BKM")
            {
                if (tbl.dparpaytype == "BBM")
                {
                    if (tbl.dparpayrefno == "" || tbl.dparpayrefno == null)
                        ModelState.AddModelError("", "Please fill REF. NO. field!");
                }
                if (tbl.dpardate > tbl.dparduedate)
                    ModelState.AddModelError("", "DUE DATE must be more or equal than DP DATE");
                if (tbl.dparpaytype == "BGM")
                {
                    if (tbl.dpardate > tbl.dpartakegiro)
                        ModelState.AddModelError("", "DATE TAKE GIRO must be more or equal than DP DATE!");
                }
            }
            if (tbl.dparamt <= 0)
            {
                ModelState.AddModelError("", "DP AMOUNT must be more than 0!");
            }
            else
            {
                decimal dSOAmt = 0;
                if (tbl.soreftype == "QL_trnsoitemmst")
                {
                    dSOAmt = db.Database.SqlQuery<decimal>("SELECT (pm.soitemgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=pm.soitemmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')='' AND dp.dparoid<>'" + tbl.dparoid + "'),0.0)) FROM QL_trnsoitemmst pm WHERE pm.soitemmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                }
                else
                {
                    dSOAmt = db.Database.SqlQuery<decimal>("SELECT (pm.soassetgrandtotalamt - ISNULL((SELECT SUM(dparamt) FROM QL_trndpar dp WHERE dp.soreftype='QL_trnsoassetmst' AND dp.somstoid=pm.soassetmstoid AND dp.dparpaytype<>'DPFK' AND ISNULL(dp.dparres1,'')='' AND dp.dparoid<>'" + tbl.dparoid + "'),0.0)) FROM QL_trnsoassetmst pm WHERE pm.soassetmstoid ='" + tbl.somstoid + "'").FirstOrDefault();
                }
                if (tbl.dparamt > dSOAmt)
                {
                    ModelState.AddModelError("", "DP Amount > SO Amount!");
                }

                if (!ClassFunction.isLengthAccepted("dparamt", "QL_trndpar", tbl.dparamt, ref sErrReply))
                    ModelState.AddModelError("", "DP AMOUNT must be less than MAX DP AMOUNT (" + sErrReply + ") allowed stored in database!");
            }
            if (tbl.addacctgoid1 != 0)
                if (tbl.addacctgamt1 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 1 can't be equal to 0!");
            if (tbl.addacctgoid2 != 0)
            {
                if (tbl.addacctgamt2 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 2 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid2)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 2 must be in different account!");
                }
            }
            if (tbl.addacctgoid3 != 0)
            {
                if (tbl.addacctgamt3 == 0)
                    ModelState.AddModelError("", "Additional Cost Amount 3 can't be equal to 0!");
                if (tbl.addacctgoid1 != 0)
                {
                    if (tbl.addacctgoid1 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 1 and Additional Cost 3 must be in different account!");
                }
                if (tbl.addacctgoid2 != 0)
                {
                    if (tbl.addacctgoid2 == tbl.addacctgoid3)
                        ModelState.AddModelError("", "Additional Cost 2 and Additional Cost 3 must be in different account!");
                }
            }
            if (tbl.addacctgamt1 != 0)
            {
                if (tbl.addacctgoid1 == 0)
                {
                    ModelState.AddModelError("", "Additional Cost 1 please select account!");
                }
            }
            if (tbl.addacctgamt2 != 0)
            {
                if (tbl.addacctgoid2 == 0)
                {
                    ModelState.AddModelError("", "Additional Cost 2 please select account!");
                }
            }
            if (tbl.addacctgamt3 != 0)
            {
                if (tbl.addacctgoid3 == 0)
                {
                    ModelState.AddModelError("", "Additional Cost 3 please select account!");
                }
            }

            //var cRate = new ClassRate();
            DateTime sDueDate = new DateTime();
            if (tbl.dparpaytype == "BKM" || tbl.dparpaytype == "BLM")
                sDueDate = tbl.dpardate;
            else
                sDueDate = tbl.dparduedate;
            DateTime sDate = tbl.dpardate;
            if (tbl.dparpaytype == "BBM")
                sDate = tbl.dparduedate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            int iGiroAcctgOid = tbl.giroacctgoid;
            if (tbl.dparstatus == "Post")
            {
                //cRate.SetRateValue(tbl.curroid, sDate.ToString("MM/dd/yyyy"));
                //if (cRate.GetRateDailyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.dparstatus = "In Process";
                //}
                //if (cRate.GetRateMonthlyLastError != "")
                //{
                //    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.dparstatus = "In Process";
                //}
                string sVarErr = "";
                if (tbl.dparpaytype == "BGM")
                {
                    if (!ClassFunction.IsInterfaceExists("VAR_GIRO_IN", CompnyCode))
                        sVarErr += (sVarErr == "" ? "" : " AND ") + "VAR_GIRO_IN";
                    else
                        iGiroAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", CompnyCode));
                        tbl.giroacctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_GIRO_IN", CompnyCode));
                }
                if (sVarErr != "")
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning(sVarErr)); tbl.dparstatus = "In Process";
                }
            }

            var custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid='" + tbl.custoid + "'").FirstOrDefault();

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.dpardate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.dparstatus = "In Process";
            }
            if (tbl.dparstatus == "Post")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.dparstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.dparstatus = "In Process";

            if (ModelState.IsValid)
            {
                tbl.cmpcode = CompnyCode;
                tbl.dpardppamt = 0;
                tbl.dparamtidr = tbl.dparamt * 1;
                tbl.dparamtusd = tbl.dparamt * 1;
                tbl.dpartakegiro = (tbl.dparpaytype == "BGM" ? tbl.dpartakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                tbl.dparduedate = sDueDate;
                tbl.dparnote = (tbl.dparnote == null ? "" : ClassFunction.Tchar(tbl.dparnote));
                if (tbl.custoid != 0)
                    ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.custoid).FirstOrDefault();
                decimal cashbankamt = tbl.dparamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;

                if (cashbankno == null) cashbankno = "";
                if (tbl.dparstatus == "Post")
                {
                    tbl.dparno = GenerateNo(tglmst);
                    cashbankno = GenerateExpenseNo2(tbl.cmpcode, tglmst, tbl.dparpaytype, tbl.dparpayacctgoid);
                }

                var mstoid = ClassFunction.GenerateID("QL_trndpar");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_trncashbankmst tblcashbank;
                        if (action == "Create")
                        {
                            tblcashbank = new QL_trncashbankmst();
                        }
                        else
                        {
                            tblcashbank = new QL_APISEntities().QL_trncashbankmst.Where(s => s.cmpcode == tbl.cmpcode && s.cashbankoid == tbl.cashbankoid).FirstOrDefault();
                        }

                        tblcashbank.cmpcode = tbl.cmpcode;
                        tblcashbank.cashbankoid = tbl.cashbankoid;
                        tblcashbank.cashbankno = cashbankno;
                        tblcashbank.cashbankdate = tbl.dpardate;
                        tblcashbank.cashbanktype = tbl.dparpaytype;
                        tblcashbank.cashbankgroup = "DPAR";
                        tblcashbank.acctgoid = tbl.dparpayacctgoid;
                        tblcashbank.curroid = tbl.curroid;
                        tblcashbank.cashbankamt = cashbankamt;
                        tblcashbank.cashbankamtidr = cashbankamt * 1;
                        tblcashbank.cashbankamtusd = cashbankamt * 1;
                        tblcashbank.personoid = tbl.custoid.ToString();
                        tblcashbank.cashbankduedate = tbl.dparduedate;
                        tblcashbank.cashbankrefno = tbl.dparpayrefno;
                        tblcashbank.cashbanknote = tbl.dparnote;
                        tblcashbank.cashbankstatus = tbl.dparstatus;
                        tblcashbank.createuser = tbl.createuser;
                        tblcashbank.createtime = tbl.createtime;
                        tblcashbank.cashbanktakegiro = (tbl.dparpaytype == "BGM" ? tbl.dpartakegiro : DateTime.Parse("1/1/1900 00:00:00"));
                        tblcashbank.giroacctgoid = iGiroAcctgOid;
                        tblcashbank.addacctgoid1 = tbl.addacctgoid1;
                        tblcashbank.addacctgamt1 = tbl.addacctgamt1;
                        tblcashbank.addacctgoid2 = tbl.addacctgoid2;
                        tblcashbank.addacctgamt2 = tbl.addacctgamt2;
                        tblcashbank.addacctgoid3 = tbl.addacctgoid3;
                        tblcashbank.addacctgamt3 = tbl.addacctgamt3;

                        tblcashbank.cashbanktakegiroreal = (tblcashbank.cashbanktakegiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbanktakegiroreal);
                        tblcashbank.cashbankgiroreal = (tblcashbank.cashbankgiroreal == DateTime.Parse("1/1/0001 00:00:00") ? DateTime.Parse("1/1/1900 00:00:00") : tblcashbank.cashbankgiroreal);
                        tblcashbank.cashbankaptype = (tblcashbank.cashbankaptype == null ? "" : tblcashbank.cashbankaptype);
                        tblcashbank.cashbanktaxtype = "NON TAX";

                        if (action == "Create")
                        {
                            tblcashbank.cashbankoid = cashbankoid;
                            tblcashbank.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dpardate);
                            tblcashbank.upduser = tbl.createuser;
                            tblcashbank.updtime = tbl.createtime;
                            db.QL_trncashbankmst.Add(tblcashbank);
                            db.SaveChanges();
                            
                            tbl.dparoid = mstoid;
                            tbl.cashbankoid = cashbankoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dpardate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndpar.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + mstoid + " WHERE tablename='QL_trndpar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {           
                            tblcashbank.updtime = servertime;
                            tblcashbank.upduser = Session["UserID"].ToString();
                            db.Entry(tblcashbank).State = EntityState.Modified;
                            db.SaveChanges();

                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (tbl.dparstatus == "Post")
                        {
                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DP A/R|No=" + tbl.dparno + "", tbl.dparstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            //Cash/Bank/Giro
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, (tbl.dparpaytype == "BGM" ? iGiroAcctgOid : tbl.dparpayacctgoid), "D", cashbankamt, tbl.dparno, cashbankno + " | " + custname + " | "+ tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, cashbankamt * 1, cashbankamt * 1, "QL_trndpar " + tbl.dparoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                            //Additional Cost 1+/-
                            if (tbl.addacctgamt1 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt1), tbl.dparno, cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * 1, Math.Abs(tbl.addacctgamt1) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 1 - DP A/R|No=" + tbl.dparno + "", (tbl.addacctgamt1 > 0 ? "M" : "K"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Additional Cost 2+/-
                            if (tbl.addacctgamt2 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt2), tbl.dparno, cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * 1, Math.Abs(tbl.addacctgamt2) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 2 - DP A/R|No=" + tbl.dparno + "", (tbl.addacctgamt2 > 0 ? "M" : "K"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Additional Cost 3+/-
                            if (tbl.addacctgamt3 != 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "C" : "D"), Math.Abs(tbl.addacctgamt3), tbl.dparno, cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * 1, Math.Abs(tbl.addacctgamt3) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 3 - DP A/R|No=" + tbl.dparno + "", (tbl.addacctgamt3 > 0 ? "M" : "K"), 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }
                            //Uang Muka
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "C", tbl.dparamt, tbl.dparno, cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, tbl.dparamt * 1, tbl.dparamt * 1, "QL_trndpar " + tbl.dparoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        //if (string.IsNullOrEmpty(closing))
                        //    return RedirectToAction("Form/" + tbl.dparoid + "/" + tbl.cmpcode);
                        //else
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: dparMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpar tbl = db.QL_trndpar.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trncashbank = db.QL_trncashbankmst.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == CompnyCode);
                        db.QL_trncashbankmst.RemoveRange(trncashbank);
                        db.SaveChanges();

                        db.QL_trndpar.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: dparMaterial/Return/5/11
        [HttpPost, ActionName("ReturDP")]
        [ValidateAntiForgeryToken]
        public ActionResult ReturDPConfirmed(int id, int somstoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpar tbl = db.QL_trndpar.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }
            else
            {
                if (tbl.dparpaytype == "BGM")
                {
                    var cekbg = db.QL_trncashbankmst.Where(s => s.cashbankoid == tbl.cashbankoid).FirstOrDefault().cashbankres1;
                    if (cekbg == "Closed")
                    {
                        result = "failed";
                        msg = "- BG Sudah Di cairkan!";
                    }
                }
            }

            if (result == "success")
            {
                //DateTime sDate = tbl.dpardate;
                //if (tbl.dparpaytype == "BBM")
                //    sDate = tbl.dparduedate;
                DateTime sDate = ClassFunction.GetServerTime();
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
                var custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid='" + tbl.custoid + "'").FirstOrDefault();
                var cashbankno = db.Database.SqlQuery<string>("SELECT cashbankno FROM QL_trncashbankmst WHERE cashbankoid='" + tbl.cashbankoid + "'").FirstOrDefault();
                decimal cashbankamt = tbl.dparamt + tbl.addacctgamt1 + tbl.addacctgamt2 + tbl.addacctgamt3;
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trndpar SET dparres1='Batal', dparaccumamt=dparamt, updtime='"+ servertime +"'  WHERE dparoid=" + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        if (tbl.dparpaytype == "BGM")
                        {
                            sSql = "UPDATE QL_trncashbankmst SET cashbankrefno = cashbankrefno +' Batal', cashbankres1 = 'Batal', updtime='" + servertime + "'  WHERE cashbankoid=" + tbl.cashbankoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        //Insert Into GL Mst
                        db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DP A/R|No=" + tbl.dparno + "", tbl.dparstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                        db.SaveChanges();

                        int iSeq = 1;
                        //Insert Into GL Dtl
                        //Cash/Bank/Giro
                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, (tbl.dparpaytype == "BGM" ? tbl.giroacctgoid : tbl.dparpayacctgoid), "C", cashbankamt, tbl.dparno, "Pembatalan DP | " + cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, cashbankamt * 1, cashbankamt * 1, "QL_trndpar " + tbl.dparoid + "", "", "", "", 0));
                        db.SaveChanges();
                        iSeq += 1;
                        gldtloid += 1;
                        //Additional Cost 1+/-
                        if (tbl.addacctgamt1 != 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid1, (tbl.addacctgamt1 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt1), tbl.dparno, "Pembatalan DP | " +  cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt1) * 1, Math.Abs(tbl.addacctgamt1) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 1 - DP A/R|No=" + tbl.dparno + "", (tbl.addacctgamt1 > 0 ? "K" : "M"), 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        //Additional Cost 2+/-
                        if (tbl.addacctgamt2 != 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid2, (tbl.addacctgamt2 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt2), tbl.dparno, "Pembatalan DP | " +  cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt2) * 1, Math.Abs(tbl.addacctgamt2) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 2 - DP A/R|No=" + tbl.dparno + "", (tbl.addacctgamt2 > 0 ? "K" : "M"), 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        //Additional Cost 3+/-
                        if (tbl.addacctgamt3 != 0)
                        {
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.addacctgoid3, (tbl.addacctgamt3 > 0 ? "D" : "C"), Math.Abs(tbl.addacctgamt3), tbl.dparno, "Pembatalan DP | " +  cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, Math.Abs(tbl.addacctgamt3) * 1, Math.Abs(tbl.addacctgamt3) * 1, "QL_trndpar " + tbl.dparoid + "", "", "Additional Cost 3 - DP A/R|No=" + tbl.dparno + "", (tbl.addacctgamt3 > 0 ? "K" : "M"), 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;
                        }
                        //Uang Muka
                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", tbl.dparamt, tbl.dparno, "Pembatalan DP | " +  cashbankno + " | " + custname + " | " + tbl.dparnote, tbl.dparstatus, tbl.upduser, tbl.updtime, tbl.dparamt * 1, tbl.dparamt * 1, "QL_trndpar " + tbl.dparoid + "", "", "", "", 0));
                        db.SaveChanges();
                        iSeq += 1;

                        sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: dparMaterial/Revisi/5/11
        [HttpPost, ActionName("RevisiSO")]
        [ValidateAntiForgeryToken]
        public ActionResult RevisiSOConfirmed(int id, int somstoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndpar tbl = db.QL_trndpar.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trndpar SET somstoid=" + somstoid + " WHERE dparoid="+ id +"";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDPAR.rpt"));
            report.SetParameterValue("sWhere", " WHERE dp.cmpcode='" + CompnyCode + "' AND dp.dparoid IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "DownPaymentARPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}