﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class DNController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = System.Configuration.ConfigurationManager.AppSettings["CompnyName"];
        private string DefaultFormatCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public DNController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class trndebetnote
        {
            public string cmpcode { get; set; }
            public int dnoid { get; set; }
            public string dnno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime dndate { get; set; }
            public string tipe { get; set; }
            public string suppcustname { get; set; }
            public string aparno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal dnamt { get; set; }
            public string dnstatus { get; set; }
            public string dnnote { get; set; }
            public string divname { get; set; }
            public string reason { get; set; }
        }

        public class apardata
        {
            public string cmpcode { get; set; }
            public string reftype { get; set; }
            public int refoid { get; set; }
            public int acctgoid { get; set; }
            public string acctgaccount { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public string transcheckdate { get; set; }
            public int curroid { get; set; }
            public string currcode { get; set; }
            public decimal amttrans { get; set; }
            public decimal amttransidr { get; set; }
            public decimal amttransusd { get; set; }
            public decimal amtbayar { get; set; }
            public decimal amtbayaridr { get; set; }
            public decimal amtbayarusd { get; set; }
            public decimal amtbalance { get; set; }
            public decimal amtbalanceidr { get; set; }
            public decimal amtbalanceusd { get; set; }
            public decimal aparpaidamt { get; set; }
            public decimal aparpaidamtidr { get; set; }
            public decimal aparpaidamtusd { get; set; }
            public decimal aparbalance { get; set; }
            public decimal aparbalanceidr { get; set; }
            public decimal aparbalanceusd { get; set; }
            public decimal aparamt { get; set; }
            public decimal aparamtidr { get; set; }
            public decimal aparamtusd { get; set; }
            public decimal amtdncn { get; set; }
            public decimal amtdncnidr { get; set; }
            public decimal amtdncnusd { get; set; }
            public DateTime apardate { get; set; }
            public DateTime aparcheckdate { get; set; }
            public string dntype { get; set; }
            public string aparno { get; set; }
            public string suppcustname { get; set; }
            public string dbacctgdesc { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
        }

        public class supplier
        {
            public int oid { get; set; }
            public string code { get; set; }
            public string name { get; set; }
        }

        private void InitDDL(QL_trndebetnote tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var dbacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.dbacctgoid);
            ViewBag.dbacctgoid = dbacctgoid;
            var cracctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.cracctgoid);
            ViewBag.cracctgoid = cracctgoid;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindSupplierData(string reftype)
        {
            List<supplier> tbl = new List<supplier>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT DISTINCT a.suppoid AS oid,a.suppcode AS code,a.suppname AS name FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,(ap.amttrans+ap.amttransidr+ap.amttransusd) apamt," +
                "ap.trnapdate, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 And ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid = ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS appaidamt," +
                " 0.0 AS apdncnamt " +
                "FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + CompnyCode + "' AND ISNULL(ap.payrefoid,0)=0 " +
                ") AS a WHERE ROUND(a.apamt, 4)<>ROUND(a.appaidamt+a.apdncnamt, 4) " +
                "ORDER BY a.suppcode ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT DISTINCT a.custoid AS oid,a.custcode AS code,a.custname AS name FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,(ar.amttrans+ar.amttransidr+ar.amttransusd) aramt," +
                "ar.trnardate, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS arpaidamt," +
                " 0.0 AS ardncnamt " +
                "FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + CompnyCode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.trnartype NOT LIKE 'ARRET%' " +
                ") AS a WHERE ROUND(a.aramt, 4)<>ROUND(a.arpaidamt+a.ardncnamt, 4) " +
                "ORDER BY a.custcode ";
            }
            tbl = db.Database.SqlQuery<supplier>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataAPAR(string reftype, string suppcustoid)
        {
            List<apardata> tbl = new List<apardata>();
            if (reftype.ToUpper() == "AP")
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode, a.projectname, a.departemen," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,s.suppcode,s.suppname,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,ap.amttransidr,ap.amttransusd,vap.transno,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transcheckdate,vap.currency, vap.projectname, vap.departemen," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd " +
                "FROM QL_conap ap INNER JOIN (SELECT con.cmpcode, conapoid, vapno transno, ap.* FROM QL_conap con CROSS APPLY ( SELECT apitemdate vapdate, apitemno vapno, apitempaytypeoid vappaytypeoid,c.currcode currency,  ap.curroid vapcurr, ap.rateoid vaprate, ap.curroid, ap.rate2oid vaprate2, apitemtotalamt vapamt, apitemtaxamt vaptax, apitemmstnote vapnote, apitemmststatus vapstatus, ap.createuser vapcrtuser, ap.createtime vapcrtdate, ap.upduser vapupduser, ap.updtime vapupddate, getdate() vapdatetakegiro, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_trnapitemmst ap INNER JOIN QL_mstcurr c ON c.curroid = ap.curroid WHERE con.reftype='QL_trnapitemmst' AND apitemmstoid=con.refoid AND ap.cmpcode=con.cmpcode UNION ALL  SELECT apdirdate vapdate, apdirno vapno, apdirpaytypeoid vappaytypeoid,c.currcode currency,  ap.curroid vapcurr, ap.rateoid vaprate, ap.curroid, ap.rate2oid vaprate2, apdirtotalamt vapamt, apdirtaxamt vaptax, apdirmstnote vapnote, apdirmststatus vapstatus, ap.createuser vapcrtuser, ap.createtime vapcrtdate, ap.upduser vapupduser, ap.updtime vapupddate, getdate() vapdatetakegiro, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ap.rabmstoid),'') projectname, ISNULL((SELECT de.groupdesc FROM QL_trnrabmst rm INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE rm.rabmstoid=ap.rabmstoid),'') departemen FROM QL_trnapdirmst ap INNER JOIN QL_mstcurr c ON c.curroid = ap.curroid WHERE con.reftype='QL_trnapdirmst' AND apdirmstoid=con.refoid AND ap.cmpcode=con.cmpcode UNION ALL  SELECT apassetdate vapdate, apassetno vapno, apassetpaytypeoid vappaytypeoid,c.currcode currency,  ap.curroid vapcurr, ap.rateoid vaprate, ap.curroid, ap.rate2oid vaprate2, apassettotalamt vapamt, apassettotaltax vaptax, apassetmstnote vapnote, apassetmststatus vapstatus, ap.createuser vapcrtuser, ap.createtime vapcrtdate, ap.upduser vapupduser, ap.updtime vapupddate, getdate() vapdatetakegiro, '' AS projectname, '' AS departemen FROM QL_trnapassetmst ap INNER JOIN QL_mstcurr c ON c.curroid = ap.curroid WHERE con.reftype='QL_trnapassetmst' AND apassetmstoid=con.refoid AND ap.cmpcode=con.cmpcode ) ap WHERE con.payrefoid=0) vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid " +
                "WHERE ap.cmpcode='" + CompnyCode + "' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" + suppcustoid + "" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno ";
            }
            else if (reftype.ToUpper() == "AR")
            {
                sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.transcheckdate,ISNULL((SELECT c.curroid FROM QL_mstcurr c WHERE c.currcode=a.currency), 0) curroid," +
                "a.currency currcode, a.projectname, a.departemen," +
                "a.amttrans,a.amttransidr,a.amttransusd,a.amtbayar+a.amtdncn AS amtbayar,a.amtbayaridr+a.amtdncnidr AS amtbayaridr,a.amtbayarusd+a.amtdncnusd AS amtbayarusd,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance,a.amttransidr-(a.amtbayaridr+a.amtdncnidr) AS amtbalanceidr,a.amttransusd-(a.amtbayarusd+a.amtdncnusd) AS amtbalanceusd " +
                "FROM (" +
                "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans,ar.amttransidr,ar.amttransusd,var.transno,CONVERT(VARCHAR(10),ar.trnardate,101) AS transdate,CONVERT(VARCHAR(10),ar.trnardate,101) AS transcheckdate,var.currency, var.projectname, var.departemen," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd " +
                "FROM QL_conar ar INNER JOIN (SELECT cmpcode, conaroid, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemno FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetno FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmststatus FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmststatus FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmstnote FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmstnote FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), aritemdate) FROM QL_trnaritemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), arassetdate) FROM QL_trnarassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currcode FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currsymbol FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT rm.projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) projectname, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT de.groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) departemen FROM QL_conar AS con WHERE payrefoid = 0) var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid INNER JOIN QL_mstcust c ON ar.custoid=c.custoid " +
                "INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid " +
                "WHERE ar.cmpcode='" + CompnyCode + "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" + suppcustoid + " AND ar.trnartype NOT LIKE 'ARRET%'" +
                ") AS a WHERE (a.amttrans-(a.amtbayar+a.amtdncn)<>0 OR a.amttransidr-(a.amtbayaridr+a.amtdncnidr)<>0 OR a.amttransusd-(a.amtbayarusd+a.amtdncnusd)<>0) ORDER BY a.transno";
            }
            tbl = db.Database.SqlQuery<apardata>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetCOACurrency(int acctgoid)
        {
            return Json((db.Database.SqlQuery<int>("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid=" + acctgoid).FirstOrDefault()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, CompnyCode) + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trndebetnote tbl)
        {
            if (tbl.reftype != "")
            {
                ViewBag.suppcustname = Session["suppcustname"];
                ViewBag.aparno = Session["aparno"];
                ViewBag.aparamt = Session["aparamt"];
                ViewBag.aparamtidr = Session["aparamtidr"];
                ViewBag.aparamtusd = Session["aparamtusd"];
                ViewBag.aparpaidamt = Session["aparpaidamt"];
                ViewBag.aparbalance = Session["aparbalance"];
                ViewBag.aparbalanceidr = Session["aparbalanceidr"];
                ViewBag.aparbalanceusd = Session["aparbalanceusd"];
                ViewBag.aparbalanceafter = Session["aparbalanceafter"];
                ViewBag.aparcheckdate = Session["aparcheckdate"];
                ViewBag.apardate = Session["apardate"];
                ViewBag.dntype = Session["dntype"];
                ViewBag.dmaxamount = Session["dmaxamount"];
                ViewBag.dbacctgdesc = Session["dbacctgdesc"];
                ViewBag.projectname = Session["projectname"];
                ViewBag.departemen = Session["departemen"];
            }
        }

        private string GenerateNo()
        {
            var dnno = "";
            if (CompnyCode != "")
            {
                string sNo = "DN/" + ClassFunction.GetServerTime().ToString("yy") + "/" + ClassFunction.GetServerTime().ToString("MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dnno, "+ int.Parse(DefaultFormatCounter) + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndebetnote WHERE cmpcode='" + CompnyCode + "' AND dnno LIKE '%" + sNo + "%' ";
                var sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultFormatCounter));
                dnno = sNo + sCounter;
            }
            return dnno;
        }

        // GET: debetnoteMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT * FROM (" +
            "SELECT dn.cmpcode,'"+ CompnyName +"' divname,dn.dnoid,dn.dnno, dn.dnnote,dn.dndate,'FB' AS tipe,s.suppname AS suppcustname," +
            "CASE dn.reftype WHEN 'QL_trnapitemmst' THEN(SELECT apit.apitemno FROM QL_trnapitemmst apit WHERE apit.cmpcode = dn.cmpcode AND apit.apitemmstoid = dn.refoid) WHEN 'QL_trnapdirmst' THEN(SELECT apit.apdirno FROM QL_trnapdirmst apit WHERE apit.cmpcode = dn.cmpcode AND apit.apdirmstoid = dn.refoid) ELSE(SELECT apit.apassetno FROM QL_trnapassetmst apit WHERE apit.cmpcode = dn.cmpcode AND apit.apassetmstoid = dn.refoid)" +
            "END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue, CASE WHEN dnstatus = 'Revised' THEN revisereason WHEN dnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
            "FROM QL_trndebetnote dn " +
            "INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid " +
            "WHERE left(dn.reftype,8)='ql_trnap' " +
            "UNION ALL " +
            "SELECT dn.cmpcode,'" + CompnyName + "' divname,dn.dnoid,dn.dnno, dn.dnnote,dn.dndate,'FJ' AS tipe,c.custname AS suppcustname," +
            "CASE dn.reftype WHEN 'QL_trnaritemmst' THEN (SELECT arit.aritemno FROM QL_trnaritemmst arit WHERE arit.cmpcode=dn.cmpcode AND arit.aritemmstoid=dn.refoid) ELSE (SELECT arit.arassetno FROM QL_trnarassetmst arit WHERE arit.cmpcode=dn.cmpcode AND arit.arassetmstoid=dn.refoid)" +
            "END AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime, 'False' AS checkvalue, CASE WHEN dnstatus = 'Revised' THEN revisereason WHEN dnstatus = 'Rejected' THEN rejectreason ELSE '' END reason " +
            "FROM QL_trndebetnote dn " +
            "INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid " +
            "WHERE left(dn.reftype,8)='ql_trnar' AND left(dn.reftype,11)<>'ql_trnarret' " +
            ") AS a WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " a.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += " a.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND dndate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND dndate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND dnstatus IN ('In Process', 'Revised')";
            }

            sSql += " ORDER BY CONVERT(DATETIME, a.dndate) DESC, a.dnoid DESC";

            List<trndebetnote> dt = db.Database.SqlQuery<trndebetnote>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: debetnoteMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndebetnote tbl;
            string action = "New Data";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trndebetnote();
                tbl.dnoid = ClassFunction.GenerateID("QL_trndebetnote");
                tbl.dndate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.dnstatus = "In Process";
                tbl.reftype = "";
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trndebetnote.Find(CompnyCode, id);

                List<apardata> tbldtl = new List<apardata>();
                sSql = "SELECT * FROM (" +
                "SELECT dn.cmpcode, '"+ CompnyCode + "' divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,s.suppname AS suppcustname,dn.reftype tipe,dn.refoid,vap.transno AS aparno, vap.projectname, vap.departemen," +
                "dn.curroid,dn.rateoid,dn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,ap.amttransidr aparamtidr,ap.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ap2.amtbayaridr) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ap2.amtbayarusd) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnapstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd," +
                "dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ap.trnapdate aparcheckdate " +
                "FROM QL_trndebetnote dn " +
                "INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
                "INNER JOIN QL_conap ap ON ap.cmpcode=dn.cmpcode AND ap.reftype=dn.reftype AND ap.refoid=dn.refoid AND ap.payrefoid=0 " +
                "INNER JOIN (SELECT cmpcode, conapoid, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemno FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirno FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetno FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmststatus FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmststatus FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmststatus FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT apitemmstnote FROM QL_trnapitemmst arm WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT apdirmstnote FROM QL_trnapdirmst arm WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT apassetmstnote FROM QL_trnapassetmst arm WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apitemdate) FROM QL_trnapitemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apitempaytypeoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apdirdate) FROM QL_trnapdirmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apdirpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), apassetdate) FROM QL_trnapassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.apassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currcode FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currcode FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT currsymbol FROM QL_trnapitemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT currsymbol FROM QL_trnapdirmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnapassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND apassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT rm.projectname FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT rm.projectname FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS projectname, (CASE WHEN con.reftype='QL_trnapitemmst' THEN (SELECT de.groupdesc FROM QL_trnapitemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apitemmstoid = con.refoid) WHEN con.reftype='QL_trnapdirmst' THEN (SELECT de.groupdesc FROM QL_trnapdirmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND apdirmstoid = con.refoid) ELSE '' END) AS departemen FROM QL_conap AS con WHERE payrefoid = 0) vap ON vap.cmpcode=ap.cmpcode AND vap.conapoid=ap.conapoid " +
                "WHERE left(dn.reftype,8)='ql_trnap' " +
                "UNION ALL " +
                "SELECT dn.cmpcode,'" + CompnyName + "' divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,c.custname AS suppcustname,dn.reftype AS tipe,dn.refoid,var.transno AS aparno, var.projectname, var.departemen," +
                "dn.curroid,dn.rateoid,dn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,ar.amttransidr aparamtidr,ar.amttransusd aparamtusd," +
                "(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayar," +
                "(ISNULL((SELECT SUM(ar2.amtbayaridr) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayaridr," +
                "(ISNULL((SELECT SUM(ar2.amtbayarusd) FROM QL_conar ar2 WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnarstatus='Post'),0.0)) AS amtbayarusd," +
                " 0.0 AS amtdncn," +
                " 0.0 AS amtdncnidr," +
                " 0.0 AS amtdncnusd," +
                "dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime,ar.trnardate aparcheckdate " +
                "FROM QL_trndebetnote dn " +
                "INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid " +
                "INNER JOIN QL_conar ar ON ar.cmpcode=dn.cmpcode AND ar.reftype=dn.reftype AND ar.refoid=dn.refoid AND ar.payrefoid=0 " +
                "INNER JOIN (SELECT cmpcode, conaroid, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemno FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetno FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transno, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmststatus FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmststatus FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS transstatus, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT aritemmstnote FROM QL_trnaritemmst arm WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT arassetmstnote FROM QL_trnarassetmst arm WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transnote, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), aritemdate) FROM QL_trnaritemmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT DATEADD(day, (CASE ISNUMERIC(gnother1) WHEN 0 THEN 0 ELSE CAST(gnother1 AS INT) END), arassetdate) FROM QL_trnarassetmst arm INNER JOIN QL_m05GN g ON g.gnoid = arm.arassetpaytypeoid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS transduedate, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currcode FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currcode FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END) AS currency, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT currsymbol FROM QL_trnaritemmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE (SELECT currsymbol FROM QL_trnarassetmst arm INNER JOIN QL_mstcurr c ON c.curroid = arm.curroid WHERE arm.cmpcode = con.cmpcode AND arassetmstoid = con.refoid) END)  AS currencysymbol, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT rm.projectname FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) projectname, (CASE WHEN con.reftype='QL_trnaritemmst' THEN (SELECT de.groupdesc FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = arm.rabmstoid INNER JOIN QL_mstdeptgroup de ON groupoid=rm.deptoid WHERE arm.cmpcode = con.cmpcode AND aritemmstoid = con.refoid) ELSE '' END) departemen FROM QL_conar con WHERE payrefoid = 0) var ON var.cmpcode=ar.cmpcode AND var.conaroid=ar.conaroid " +
                "WHERE left(dn.reftype,8)='ql_trnar' AND left(dn.reftype,11)<>'ql_trnarret' " +
                ") AS a WHERE a.dnoid=" + id;
                tbldtl = db.Database.SqlQuery<apardata>(sSql).ToList();

                Session["aparno"] = tbldtl[0].aparno;
                Session["apardate"] = tbldtl[0].apardate;
                Session["aparcheckdate"] = tbldtl[0].aparcheckdate;
                if (ClassFunction.Left(tbl.reftype.ToUpper(), 8) == "QL_TRNAP")
                {
                    Session["dntype"] = "AP";
                }
                else
                {
                    Session["dntype"] = "AR";
                }
                Session["dmaxamount"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["suppcustname"] = tbldtl[0].suppcustname;

                Session["aparbalance"] = tbldtl[0].aparamt - (tbldtl[0].amtbayar + tbldtl[0].amtdncn);
                Session["aparbalanceidr"] = tbldtl[0].aparamtidr - (tbldtl[0].amtbayaridr + tbldtl[0].amtdncnidr);
                Session["aparbalanceusd"] = tbldtl[0].aparamtusd - (tbldtl[0].amtbayarusd + tbldtl[0].amtdncnusd);
                Session["aparamt"] = tbldtl[0].aparamt;
                Session["aparamtidr"] = tbldtl[0].aparamtidr;
                Session["aparamtusd"] = tbldtl[0].aparamtusd;
                Session["aparpaidamt"] = tbldtl[0].amtbayar + tbldtl[0].amtdncn;
                Session["aparbalanceafter"] = Convert.ToDecimal(Session["aparbalance"]) + (tbl.dnamt * (Session["dntype"].ToString() != "AR" ? -1 : 1));
                Session["dbacctgdesc"] = tbldtl[0].dbacctgdesc;
                Session["projectname"] = tbldtl[0].projectname;
                Session["departemen"] = tbldtl[0].departemen;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: debetnoteMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trndebetnote tbl, string action, string tglmst, string aparno, string apardate, string aparcheckdate, string dntype, decimal aparbalanceafter, decimal aparbalanceidr, decimal aparbalanceusd, decimal aparbalance, decimal dmaxamount, string suppcustname, decimal aparamt, decimal aparamtidr, decimal aparamtusd, decimal aparpaidamt, string dbacctgdesc, string projectname, string departemen)
        {
            Session["aparno"] = aparno;
            Session["apardate"] = apardate;
            Session["aparcheckdate"] = aparcheckdate;
            Session["dntype"] = dntype;
            Session["aparbalanceafter"] = aparbalanceafter;
            Session["aparbalanceidr"] = aparbalanceidr;
            Session["aparbalanceusd"] = aparbalanceusd;
            Session["aparbalance"] = aparbalance;
            Session["dmaxamount"] = dmaxamount;
            Session["suppcustname"] = suppcustname;
            Session["aparamt"] = aparamt;
            Session["aparamtidr"] = aparamtidr;
            Session["aparamtusd"] = aparamtusd;
            Session["aparpaidamt"] = aparpaidamt;
            Session["dbacctgdesc"] = dbacctgdesc;
            Session["projectname"] = projectname;
            Session["departemen"] = departemen;

            try
            {
                tbl.dndate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("dndate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            var reftype = dntype;
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            if (tbl.cmpcode == null)
                tbl.cmpcode = CompnyCode;
            if (tbl.dnno == null)
                tbl.dnno = "";
            string sErrReply = "";
            if (Convert.ToDateTime(tbl.dndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(apardate) || Convert.ToDateTime(tbl.dndate.ToString("MM/dd/yyyy")) < Convert.ToDateTime(aparcheckdate))
            {
                ModelState.AddModelError("", "Tanggal DN Harus Lebih dari Tanggal FB/FJ");
            }
            if (tbl.refoid == 0)
            {
                ModelState.AddModelError("", "Silahkan pilih " + reftype + " terlebih dahulu.");
            }
            else
            {
                if (tbl.dnamt <= 0 && aparbalance > 0)
                {
                    ModelState.AddModelError("", "Debet Note amount harus lebih dari 0.");
                }
                else
                {
                    if (reftype == "AP")
                    {
                        if (tbl.dnamt > dmaxamount)
                        {
                            ModelState.AddModelError("", "Maximum Debet Note amount adalah " + dmaxamount + "");
                        }
                        else
                        {
                            if (reftype == "AP")
                            {
                                sSql = "SELECT ISNULL(SUM(amttrans - amtbayar), 0) FROM QL_conap con WHERE con.cmpcode='" + CompnyCode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            }
                            else
                            {
                                sSql = "SELECT ISNULL(SUM(amttrans - amtbayar), 0) FROM QL_conar con WHERE con.cmpcode='" + CompnyCode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            }
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.dnoid;
                            }
                            decimal dNewBalance = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (reftype == "AP")
                            {
                                sSql = "SELECT ISNULL(SUM(amttransidr - amtbayaridr), 0) FROM QL_conap con WHERE con.cmpcode='" + CompnyCode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            }
                            else
                            {
                                sSql = "SELECT ISNULL(SUM(amttransidr - amtbayaridr), 0) FROM QL_conar con WHERE con.cmpcode='" + CompnyCode + "' AND con.refoid=" + tbl.refoid + " AND con.reftype='" + tbl.reftype + "'";
                            }
                            if (action == "Update Data")
                            {
                                sSql += " AND payrefoid<>" + tbl.dnoid;
                            }
                            decimal dNewBalanceIDR = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            aparbalanceidr = dNewBalanceIDR;
                            
                            if (dmaxamount != dNewBalance)
                            {
                                dmaxamount = dNewBalance;
                                aparbalance = dNewBalance;
                            }
                            if (tbl.dnamt > dmaxamount)
                            {
                                ModelState.AddModelError("", "Maximum Debet Note sudah di update oleh user yang lain. Maximum Debet Note amount adalah " + dmaxamount + "");
                            }
                        }
                    }
                }
            }
            if (tbl.suppcustoid == 0)
                ModelState.AddModelError("", "Silahkan pilih kolom SUPPLIER/CUSTOMER!");
            if (tbl.dbacctgoid == 0)
                ModelState.AddModelError("", "Silahkan pilih kolom DEBET ACCOUNT");
            if (tbl.cracctgoid == 0)
                ModelState.AddModelError("", "Silahkan pilih kolom CREDIT ACCOUNT");
            if (tbl.dnamt <= 0)
            {
                ModelState.AddModelError("", "DN AMOUNT harus lebih dari 0!");
            }
            else
            {
                if (!ClassFunction.isLengthAccepted("dnamt", "QL_trndebetnote", tbl.dnamt, ref sErrReply))
                    ModelState.AddModelError("", "DN AMOUNT harus kurang dari MAX DN AMOUNT (" + sErrReply + ") allowed stored in database!");
            }
            if (tbl.dnstatus.ToUpper() == "REVISED")
            {
                tbl.dnstatus = "In Process";
            }

            var cRate = new ClassRate();
            if (tbl.dnstatus == "Post")
            {
                tbl.dnno = GenerateNo();
                cRate.SetRateValue(tbl.curroid, Convert.ToDateTime(apardate).ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.dnstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.dnstatus = "In Process";
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.dnstatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.dndate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.dnstatus = "In Process";
            }
            if (tbl.dnstatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.dnstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.dnstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trndebetnote");
                var cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "New Data")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trndebetnote WHERE dnoid=" + tbl.dnoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trndebetnote");
                    }
                }
                var conapoid = ClassFunction.GenerateID("QL_conap");
                var conaroid = ClassFunction.GenerateID("QL_conar");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cmpcode = CompnyCode;
                tbl.dntaxtype = "NON TAX";
                tbl.dntaxamt = 0;
                tbl.dnno = (tbl.dnno == null ? "" : tbl.dnno);
                tbl.dnnote = (tbl.dnnote == null ? "" : tbl.dnnote);

                tbl.dnamtidr = tbl.dnamt * cRate.GetRateMonthlyIDRValue;
                tbl.dnamtusd = tbl.dnamt * cRate.GetRateMonthlyUSDValue;
                tbl.dntaxamtidr = tbl.dntaxamt * cRate.GetRateMonthlyIDRValue;
                tbl.dntaxamtusd = tbl.dntaxamt * cRate.GetRateMonthlyUSDValue;
                var dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conar con WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnarstatus='Post'").FirstOrDefault();
                if (reftype.ToUpper() == "AP")
                {
                    dbalancepost = db.Database.SqlQuery<decimal>("SELECT ISNULL(SUM(amttrans - amtbayar), 0.0) amtbalance FROM QL_conap WHERE cmpcode='" + tbl.cmpcode + "' AND reftype='" + tbl.reftype + "' AND refoid=" + tbl.refoid + " AND trnapstatus='Post'").FirstOrDefault();
                }
                if (dbalancepost == 0)
                {
                    tbl.dnamtidr = aparbalanceidr;
                    tbl.dnamtusd = aparbalanceusd;
                    tbl.dntaxamtidr = 0;
                    tbl.dntaxamtusd = 0;
                }

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trndebetnote.Find(tbl.cmpcode, tbl.dnoid) != null)
                                tbl.dnoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trndebetnote.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.dnoid + " WHERE tablename='QL_trndebetnote'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Select OLD Data
                            var sOldType = tbl.reftype.ToUpper();
                            var sOldOid = tbl.refoid;
                            if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                            {
                                var conap = db.QL_conap.Where(x => x.payrefoid == tbl.dnoid && x.trnaptype == "DNAP");
                                db.QL_conap.RemoveRange(conap);
                                db.SaveChanges();
                            }
                            else
                            {
                                var conar = db.QL_conar.Where(x => x.payrefoid == tbl.dnoid && x.trnartype == "DNAR");
                                db.QL_conar.RemoveRange(conar);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tbl.dnstatus == "In Approval")
                        {
                            tblapp.cmpcode = tbl.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.dnoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trndebetnote";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tbl.dnstatus == "Post")
                        {
                            if (reftype.ToUpper() == "AP")
                            {
                                QL_conap conap = new QL_conap();
                                conap.cmpcode = tbl.cmpcode;
                                conap.conapoid = conapoid++;
                                conap.reftype = tbl.reftype;
                                conap.refoid = tbl.refoid;
                                conap.payrefoid = tbl.dnoid;
                                conap.suppoid = tbl.suppcustoid;
                                conap.acctgoid = tbl.dbacctgoid;
                                conap.trnapstatus = tbl.dnstatus;
                                conap.trnaptype = "DN" + reftype;
                                conap.trnapdate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                conap.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                                conap.paymentacctgoid = tbl.cracctgoid;
                                conap.paymentdate = tbl.dndate;
                                conap.payrefno = tbl.dnno;
                                conap.paybankoid = 0;
                                conap.payduedate = tbl.dndate;
                                conap.amttrans = 0;
                                conap.amtbayar = tbl.dnamt + tbl.dntaxamt;
                                conap.trnapnote = "DN No. " + tbl.dnno + " for "+ reftype +" No. " + aparno + "";
                                conap.createuser = tbl.createuser;
                                conap.createtime = tbl.createtime;
                                conap.upduser = tbl.upduser;
                                conap.updtime = tbl.updtime;
                                conap.amttransidr = 0;
                                conap.amtbayaridr = tbl.dnamtidr + tbl.dntaxamtidr;
                                conap.amttransusd = 0;
                                conap.amtbayarusd = tbl.dnamtusd + tbl.dntaxamtusd;
                                conap.amtbayarother = 0;
                                conap.conflag = "";

                                db.QL_conap.Add(conap);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + conap.conapoid + " WHERE tablename='QL_conap'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                QL_conar conar = new QL_conar();
                                conar.cmpcode = tbl.cmpcode;
                                conar.conaroid = conaroid++;
                                conar.reftype = tbl.reftype;
                                conar.refoid = tbl.refoid;
                                conar.payrefoid = tbl.dnoid;
                                conar.custoid = tbl.suppcustoid;
                                conar.acctgoid = tbl.dbacctgoid;
                                conar.trnarstatus = tbl.dnstatus;
                                conar.trnartype = "DN" + reftype;
                                conar.trnardate = (tbl.refoid < 0 ? Convert.ToDateTime("1/1/1900") : Convert.ToDateTime(apardate));
                                conar.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);
                                conar.paymentacctgoid = tbl.cracctgoid;
                                conar.paymentdate = tbl.dndate;
                                conar.payrefno = tbl.dnno;
                                conar.paybankoid = 0;
                                conar.payduedate = tbl.dndate;
                                conar.amttrans = 0;
                                conar.amtbayar = (tbl.dnamt + tbl.dntaxamt) * -1;
                                conar.trnarnote = "DN No. " + tbl.dnno + " for " + reftype + " No. " + aparno + "";
                                conar.createuser = tbl.createuser;
                                conar.createtime = tbl.createtime;
                                conar.upduser = tbl.upduser;
                                conar.updtime = tbl.updtime;
                                conar.amttransidr = 0;
                                conar.amtbayaridr = (tbl.dnamtidr + tbl.dntaxamtidr) * -1;
                                conar.amttransusd = 0;
                                conar.amtbayarusd = (tbl.dnamtusd + tbl.dntaxamtusd) * -1;
                                conar.amtbayarother = 0;

                                db.QL_conar.Add(conar);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + conar.conaroid + " WHERE tablename='QL_conar'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }

                            var sDate = tbl.dndate;
                            var sPeriod = ClassFunction.GetDateToPeriodAcctg(tbl.dndate);

                            //Insert Into GL Mst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue));
                            db.SaveChanges();

                            int iSeq = 1;
                            //Insert Into GL Dtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.dbacctgoid, "D", tbl.dnamt + tbl.dntaxamt, tbl.dnno, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.upduser, tbl.updtime, tbl.dnamtidr + tbl.dntaxamtidr, tbl.dnamtusd + tbl.dntaxamtusd, "QL_trndebetnote " + tbl.dnoid, "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;

                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.cracctgoid, "C", tbl.dnamt + tbl.dntaxamt, tbl.dnno, "DN No. " + tbl.dnno + " for " + reftype.ToUpper() + " No. " + aparno + "", tbl.dnstatus, tbl.upduser, tbl.updtime, tbl.dnamtidr + tbl.dntaxamtidr, tbl.dnamtusd + tbl.dntaxamtusd, "QL_trndebetnote " + tbl.dnoid, "", "", "", 0));
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + gldtloid + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        //ModelState.AddModelError("Error", ex.ToString());
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: debetnoteMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trndebetnote tbl = db.QL_trndebetnote.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Select OLD Data
                        var sOldType = tbl.reftype.ToUpper();
                        var sOldOid = tbl.refoid;
                        if (ClassFunction.Left(sOldType, 8) == "QL_TRNAP")
                        {
                            var conap = db.QL_conap.Where(x => x.payrefoid == tbl.dnoid && x.trnaptype == "DNAP");
                            db.QL_conap.RemoveRange(conap);
                            db.SaveChanges();
                        }
                        else
                        {
                            var conar = db.QL_conar.Where(x => x.payrefoid == tbl.dnoid && x.trnartype == "DNAR");
                            db.QL_conar.RemoveRange(conar);
                            db.SaveChanges();
                        }

                        db.QL_trndebetnote.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptDebetNote.rpt"));
            report.SetParameterValue("sWhere", " WHERE [CmpCode]='" + CompnyCode + "' AND [Oid] IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "DebetNotePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}