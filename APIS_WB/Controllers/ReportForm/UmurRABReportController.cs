﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class UmurRABReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public UmurRABReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ReportFilter
        {
            public string ddltype { get; set; }
            public string[] ddlstatus { get; set; }
            public string periodstart { get; set; }
            public string periodend { get; set; }
            public string filtercust { get; set; }
            public string filterrab { get; set; }
        }

        [HttpPost]
        public ActionResult GetModalData(ReportFilter param, string modaltype)
        {
            var result = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                if (modaltype.ToLower() == "cust") sSql = $"SELECT DISTINCT 0 seq, custcode [Kode], custname [Nama], custaddr [Alamat]";
                else if (modaltype.ToLower() == "rab") sSql = $"SELECT DISTINCT 0 seq, rabno [No. RAB], rabmstoid [No. Draft], FORMAT(rabdate, 'dd/MM/yyyy') [Tgl. RAB], rabmststatus [Status], rabmstnote [Catatan]";

                sSql += $" FROM QL_v_report_umur_rab WHERE 1=1";
                sSql += $" AND rabdate >= CAST('{param.periodstart} 00:00:00' AS DATETIME) AND rabdate <= CAST('{param.periodend} 23:59:59' AS DATETIME)";
                if (param.ddlstatus != null)
                {
                    if (param.ddlstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < param.ddlstatus.Count(); i++) stsval += "'" + param.ddlstatus[i] + "',";
                        sSql += " AND rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(param.filtercust) && modaltype.ToLower() != "cust")
                {
                    string[] arr = param.filtercust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (!string.IsNullOrEmpty(param.filterrab) && modaltype.ToLower() != "rab")
                {
                    string[] arr = param.filterrab.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                    sSql += " AND rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblModal" + modaltype);
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(ReportFilter param, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            var rptfile = $"rptUmurRAB{reporttype.Replace("View", "Pdf")}.rpt"; var rptname = $"LAPORAN_UMUR_TGL_BERAKHIR_SPK_TGL_{ClassFunction.toDate(param.periodstart)} - {ClassFunction.toDate(param.periodend)}";

            sSql = $"SELECT *, DATEDIFF(d, rabfinishdate, '{param.periodend}') rablate FROM QL_v_report_umur_rab WHERE 1=1";
            if (param.periodstart != "" && param.periodend != "") sSql += $" AND rabdate >= CAST('{param.periodstart} 00:00:00' AS DATETIME) AND rabdate <= CAST('{param.periodend} 23:59:59' AS DATETIME)";

            if (param.ddlstatus != null)
            {
                if (param.ddlstatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < param.ddlstatus.Count(); i++) stsval += "'" + param.ddlstatus[i] + "',";
                    sSql += " AND rabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (!string.IsNullOrEmpty(param.filtercust))
            {
                string[] arr = param.filtercust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                sSql += " AND custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (!string.IsNullOrEmpty(param.filterrab))
            {
                string[] arr = param.filterrab.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++) filterdata += "'" + arr[i] + "',";
                sSql += " AND rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            sSql += " ORDER BY rabno, rabdate";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(param.periodstart) + " - " + ClassFunction.toDate(param.periodend));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype.Replace("View", "") == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = null;//CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                //report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}