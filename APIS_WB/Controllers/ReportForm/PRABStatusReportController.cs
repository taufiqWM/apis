﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class PRABStatusReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public PRABStatusReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listcust
        {
            public int seq { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class listprab
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string projectname { get; set; }
            public string rabnote { get; set; }
        }

        public class listmat
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }
            public string unit { get; set; }
        }

        public class listso
        {
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string custname { get; set; }
        }

        public ActionResult GetSalesData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, s.usoid [Username], s.usname [Nama], g.gndesc [Jabatan], s.usaddress [Alamat] FROM QL_m01US s INNER JOIN QL_trnreqrabmst som ON som.salesoid=s.usoid INNER JOIN QL_m05GN g ON gnoid=s.Jabatanoid AND gngroup='JABATAN' Where som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.reqrabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY s.usname";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSales");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(string[] status, string startdate, string enddate, string fdate, string TextSales)
        {
            sSql = "SELECT 0 seq, som.soitemmstoid, som.soitemno, soitemdate, c.custname FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "' AND som.soitemtype<>'Jasa'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += "ORDER BY som.soitemno";

            List<listso> tbl = new List<listso>();
            tbl = db.Database.SqlQuery<listso>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] status, string startdate, string enddate, string fdate, string TextSales)
        {
            sSql = "SELECT 0 seq, c.custcode, c.custname, c.custaddr FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnreqrabmst som WHERE som.cmpcode='" + CompnyCode + "' ";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.reqrabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += ") ORDER BY c.custcode";

            List<listcust> tbl = new List<listcust>();
            tbl = db.Database.SqlQuery<listcust>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] status, string custcode, string startdate, string enddate, string fdate, string TextSales)
        {
            sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid, rm.rabno, rm.rabdate, rm.projectname, rm.rabmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_trnsoitemmst som ON som.rabmstoid=rm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "' AND som.soitemtype<>'Jasa' ";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }
            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += " ORDER BY rm.rabno";

            List<listprab> tbl = new List<listprab>();
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string custcode, string prabno, string startdate, string enddate, string fdate, string TextSales)
        {
            sSql = "SELECT DISTINCT m.itemcode, m.itemdesc, m.itemtype FROM QL_mstitem m INNER JOIN QL_trnrabdtl rd ON rd.itemoid=m.itemoid INNER JOIN QL_trnrabmst rm ON rm.cmpcode=rd.cmpcode AND rm.rabmstoid=rd.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabunitoid INNER JOIN QL_trnsoitemmst som ON rm.rabmstoid=som.rabmstoid WHERE rd.cmpcode='" + CompnyCode + "'";
            if (startdate != "" && enddate != "")
            {
                sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
            }

            if (status != null)
            {
                if (status.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < status.Count(); i++)
                    {
                        stsval += "'" + status[i] + "',";
                    }
                    sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (custcode != "")
            {
                string[] arr = custcode.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (prabno != "")
            {
                string[] arr = prabno.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            sSql += " ORDER BY m.itemcode";

            List<listmat> tbl = new List<listmat>();
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();
            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("SOReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string soitemno, string TextSales)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "rptPRABStatusPdf.rpt";
            var rptname = "PRABStatus";

            //var Dtl = "";
            //var Join = "";
            if (DDLType == "Detail")
            {
                //Dtl = ",sod.soitemdtletd [ETD DTL], soitemdtlseq [No.], itemcode [Code], m.itemdesc [Material], itemnote [SKU], soitemqty [Qty], g2.gndesc [Unit], soitemprice [Price], soitemdtlamt [Detail Amount], soitemdtldisctype [Disc Dtl Type], soitemdtldiscvalue [Disc Dtl Value], soitemdtldiscamt [Disc Dtl Amt], soitemdtlnetto [Detail Netto], soitemdtlnote [Detail Note], 0.00 [Pack Volume FG]";
                //Join = " INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode=som.cmpcode AND sod.soitemmstoid=som.soitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = sod.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid = soitemunitoid";
            }
            sSql = "SELECT som.cmpcode, som.reqrabmstoid, reqrabno, som.reqrabdate, ISNULL((SELECT rm.rabno FROM QL_trnrabmst_draft rm WHERE rm.reqrabmstoid=som.reqrabmstoid),'') rabno_draft, ISNULL((SELECT rm2.rabno FROM QL_trnrabmst rm2 INNER JOIN QL_trnrabmst_draft rm ON rm.rabmstoid=ISNULL(rm2.rabmstoid_draft,0) WHERE rm.reqrabmstoid=som.reqrabmstoid),'') rabno, som.reqrabtype, som.projectname, us.usname sales, us2.usname salesadmin, som.reqrabmststatus, som.reqrabmstnote, CASE WHEN ISNULL((SELECT rm.rabno FROM QL_trnrabmst_draft rm WHERE rm.reqrabmstoid=som.reqrabmstoid),'')<>'' AND ISNULL((SELECT rm2.rabno FROM QL_trnrabmst rm2 INNER JOIN QL_trnrabmst_draft rm ON rm.rabmstoid=ISNULL(rm2.rabmstoid_draft,0) WHERE rm.reqrabmstoid=som.reqrabmstoid),'')<>'' THEN 'Selesai' ELSE 'Pending' END reqstatus, C.CUSTNAME FROM QL_trnreqrabmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_m01US us ON us.usoid=som.salesoid INNER JOIN QL_m01US us2 ON us2.usoid=som.salesadminoid WHERE som.cmpcode='"+ CompnyCode +"'";
            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND som.reqrabmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (soitemno != "")
            {
                string[] arr = soitemno.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND som.soitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND rm.rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextSales != "")
            {
                string[] arr = TextSales.Split(';'); string datafilter = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    datafilter += "'" + arr[i] + "',";
                }
                sSql += " AND som.salesoid IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            //sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                if (DDLType == "Summary Status Amount")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary Status Amount")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}