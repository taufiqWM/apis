﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class RoleController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public RoleController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET/POST: Role
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select * FROM QL_m02RL WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter;
            List<QL_m02RL> m02RL = db.Database.SqlQuery<QL_m02RL>(sSql).ToList();
            return View(m02RL);
        }

        // GET: Role/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m02RL qL_m02RL;
            string action = "Create";
            if (id == null)
            {
                qL_m02RL = new QL_m02RL();
                qL_m02RL.cmpcode = Session["CompnyCode"].ToString();
                Session["QL_m02RLdtl"] = null;
            }
            else
            {
                action = "Edit";
                qL_m02RL = db.QL_m02RL.Find(Session["CompnyCode"].ToString(), id);
                string sSql = "select * from QL_m04MN where mnoid in (select x.mnoid from QL_m02Rldtl x where x.rloid="+ id + ")" ;
                Session["QL_m02RLdtl"] = db.Database.SqlQuery<roledtl>(sSql).ToList();
            }

            if (qL_m02RL == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(qL_m02RL);
            return View(qL_m02RL);
        }
        // POST: Role/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m02RL qL_m02RL, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<roledtl> list = (List<roledtl>)Session["QL_m02RLdtl"];
            if (list == null)
                ModelState.AddModelError("", "Please fill Role Detail");
            else if (list.Count <= 0)
                ModelState.AddModelError("", "Please fill Role Detail");

            var mstoid = ClassFunction.GenerateID("QL_m02RL");
            var dtloid = ClassFunction.GenerateID("QL_m02RLdtl");
            var servertime = ClassFunction.GetServerTime();


            if (String.IsNullOrEmpty(qL_m02RL.rlname))
                ModelState.AddModelError("rolename", "Please fill Role Name");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            qL_m02RL.rloid = mstoid;
                            qL_m02RL.crttime = servertime;
                            qL_m02RL.crtuser = Session["UserID"].ToString();
                            qL_m02RL.updtime = servertime;
                            qL_m02RL.upduser = Session["UserID"].ToString();
                            db.QL_m02RL.Add(qL_m02RL);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m02RL'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            qL_m02RL.updtime = servertime;
                            qL_m02RL.upduser = Session["UserID"].ToString();
                            db.Entry(qL_m02RL).State = EntityState.Modified;
                            db.SaveChanges();

                            //delete detail
                            var trndtl = db.QL_m02RLdtl.Where(a => a.rloid == qL_m02RL.rloid);
                            db.QL_m02RLdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                        }

                        //insert detail
                        QL_m02RLdtl ql_m02RLdtl;
                        for (int i = 0; i < list.Count(); i++)
                        {
                            ql_m02RLdtl = new QL_m02RLdtl();
                            ql_m02RLdtl.cmpcode = qL_m02RL.cmpcode;
                            ql_m02RLdtl.rldtloid = dtloid++;
                            ql_m02RLdtl.rloid = qL_m02RL.rloid;
                            ql_m02RLdtl.mnoid = list[i].mnoid;
                            ql_m02RLdtl.updtime = servertime;
                            ql_m02RLdtl.upduser = Session["UserID"].ToString();
                            db.QL_m02RLdtl.Add(ql_m02RLdtl);
                        }
                        db.SaveChanges();

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + dtloid + " Where tablename = 'QL_m02RLdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            setViewBag(qL_m02RL);
            return View(qL_m02RL);
        }
        
        private void setViewBag(QL_m02RL qL_m02RL)
        {
            sSql = "Select distinct mnmodule Value, mnmodule Text from QL_m04MN ";
            ViewBag.mnmodule = new SelectList(db.Database.SqlQuery<SelectListItem>(sSql).ToList(), "Value", "Text");
            sSql = "Select distinct mntype Value, mntype Text from QL_m04MN ";
            ViewBag.mntype = new SelectList(db.Database.SqlQuery<SelectListItem>(sSql).ToList(), "Value", "Text");
        }

        public ActionResult getQL_m02RLdtl()
        {
            if (Session["QL_m02RLdtl"] == null)
            {
                Session["QL_m02RLdtl"] = new List<roledtl>();
            }

            List<roledtl> dataDtl = (List<roledtl>)Session["QL_m02RLdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_m02RLdtl(List<roledtl> mdataDtl)
        {
            Session["QL_m02RLdtl"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }


        // POST: Role/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (db.QL_m03UR.Where(w => w.rloid == id).Count() > 0)
            {
                result = "failed";
                msg += "this Role already used by User";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_m02RLdtl.Where(a => a.rloid == id);
                        db.QL_m02RLdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        //db.Database.ExecuteSqlCommand("delete from QL_mstworkdtl where workoid = {0}", id);

                        QL_m02RL qL_m02RL = db.QL_m02RL.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_m02RL.Remove(qL_m02RL);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }
            

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult GetMenu(string mnmodule, string mntype)
        {
            List<QL_m04MN> mstmenu = new List<QL_m04MN>();
            mstmenu = db.QL_m04MN.Where(w => w.mnflag == "ACTIVE" & w.mntype == mntype & w.mnmodule == mnmodule).ToList();
            SelectList menu = new SelectList(mstmenu, "mnoid", "mnname", 0);
            return Json(menu);
        }

        public class roledtl
        {
            public string mnmodule { get; set; }
            public string mntype { get; set; }
            public string mnname { get; set; }
            public int mnoid { get; set; }

        }
    }
}