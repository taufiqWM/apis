﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Other
{
    public class WaitingAction2Controller : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public WaitingAction2Controller()
        {
            db.Database.CommandTimeout = 0;
        }

        public class waitactmodel
        {
            public int mstoid { get; set; }
            public string cmpcode { get; set; }
            public string tblname { get; set; }
        }

        public class modelforapp
        {
            public string caption { get; set; }
            public string value { get; set; }
        }

        public class listappfinal
        {
            public string stoid_app { get; set; }
        }

        private string CekStatusApp(string menu, string req_user, string app_user)
        {
            sSql = "SELECT a.asflag FROM QL_m08AS2 a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.usoid=stoid_app INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + CompnyCode + "' AND mnfileloc='" + menu + "' AND a.stoid_req='" + req_user + "'AND a.stoid_app='" + app_user + "'";
            string stts = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            return stts;
        }

        private List<listappfinal> BindAppFinal(string menu, string user)
        {
            sSql = "SELECT stoid_app FROM QL_m08AS2 a INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + CompnyCode + "' AND mnfileloc='" + menu + "' AND a.stoid_req='"+ user +"' AND ISNULL(a.asflag,'')='Final'";
            var app = db.Database.SqlQuery<listappfinal>(sSql).ToList();

            return app;
        }

        [HttpPost]
        public ActionResult setrevisednote(string rv)
        {
            Session["rvnote"] = rv;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult setrejectednote(string rj)
        {
            Session["rjnote"] = rj;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // GET: WaitingAction
        public ActionResult Index(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            ViewBag.TransName = db.Database.SqlQuery<string>("SELECT DISTINCT mn.mnname FROM QL_APP2 ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform WHERE ap.tablename='" + id + "'").FirstOrDefault();
            ViewBag.tblname = id;
            return View();
        }

        [HttpPost]
        public ActionResult GetDataList(string tblname)
        {
            var msg = "";

            if (tblname == "QL_trnperdinmst")
            {
                sSql = "SELECT DISTINCT '"+ CompnyCode + "' cmpcode, h.perdinid [Draft No.], CONVERT(VARCHAR(10), h.perdindate, 103) [Tgl Perdin], isnull((select x.usname from QL_m01US x where x.usoid=h.pemohon),'') [Pemohon], perdinmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnperdinmst h INNER JOIN QL_APP2 a ON  a.appformoid=h.perdinid INNER JOIN QL_m01US us ON us.usoid=a.appuser WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY perdindate";

            }
            else if (tblname == "QL_trnperdinrealmst")
            {
                sSql = "SELECT DISTINCT '" + CompnyCode + "' cmpcode, h.perdinid [Draft No.], CONVERT(VARCHAR(10), h.perdindate, 103) [Tgl Perdin], isnull((select x.usname from QL_m01US x where x.usoid=h.pemohon),'') [Pemohon], perdinmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnperdinmst h INNER JOIN QL_APP2 a ON  a.appformoid=h.perdinid INNER JOIN QL_m01US us ON us.usoid=a.appuser WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "' --ORDER BY perdindate";

            }
            else if (tblname == "QL_trnprmst")
            {
                sSql = "SELECT DISTINCT '" + CompnyCode + "' cmpcode, h.prmstoid [Draft No.], CONVERT(VARCHAR(10), h.prdate, 103) [Tgl Perdin], isnull((select x.usname from QL_m01US x where x.usoid=h.usoid),'') [Pemohon], prmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnprmst h INNER JOIN QL_APP2 a ON  a.appformoid=h.prmstoid INNER JOIN QL_m01US us ON us.usoid=a.appuser WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND us.usoid='" + Session["UserID"].ToString() + "'";

            }
            else
                msg = "Approval action haven't be set";

            if (msg == "")
            {
                DataTable tbl = new ClassConnection().GetDataTable(sSql, tblname);

                if (tbl.Rows.Count > 0)
                {
                    List<string> colname = new List<string>();

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "Draft No.")
                            {
                                item = "<a class='text-primary' href='" + Url.Action("Form/" + item + "", "WaitingAction2") + "?tblname=" + tblname + "'>" + item + "</a>";
                            }
                            row.Add(col.ColumnName, item);
                            if (!colname.Contains(col.ColumnName))
                                colname.Add(col.ColumnName);
                        }
                        rows.Add(row);
                    }

                    return Json(new { msg, colname, rows }, JsonRequestBehavior.AllowGet);
                }
                //else
                //    msg = "Data Not Found";
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // GET: WaitingAction/Form/id/cmp
        public ActionResult Form(int id, string cmp, string tblname)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            ViewBag.TransName = db.Database.SqlQuery<string>("SELECT DISTINCT mn.mnname FROM QL_APP2 ap INNER JOIN QL_m04MN mn ON mn.mnfileloc=ap.appform WHERE ap.tablename='" + tblname + "'").FirstOrDefault();
            ViewBag.BusinessUnit = CompnyName;
            ViewBag.namaTabel = tblname;
            //db.Database.SqlQuery<string>("SELECT divname FROM QL_mstdivision WHERE cmpcode='" + cmp + "'").FirstOrDefault();

            waitactmodel tbl = new waitactmodel();
            tbl.mstoid = id;
            tbl.cmpcode = cmp;
            tbl.tblname = tblname;

            return View(tbl);
        }

        [HttpPost]
        public ActionResult GetDataApproval(int id, string cmp, string tblname)
        {
            var msg = "";
            var sSqlHdr = "";
            var info = "";
            var sSqlDtl = "";
            var sSqlDtl2 = "";
            var sSqlDtl3 = "";
            var sSqlDtl4 = "";
            var sSqlDtl5 = "";
            var sSqlFtr = "";

            if (tblname == "QL_trnperdinmst")
            {
                sSqlHdr = "SELECT DISTINCT '" + CompnyCode + "' cmpcode, h.perdinid [Draft No.], CONVERT(VARCHAR(10), h.perdindate, 103) [Tgl Perdin], isnull((select x.usname from QL_m01US x where x.usoid=h.pemohon),'') [Pemohon], isnull((select x.gndesc from QL_m05GN x where x.gnoid=h.jabatanid),'') [Jabatan], isnull((select x.groupdesc from QL_mstdeptgroup x where x.groupoid=h.deptid),'') [Departemen], isnull((select x.gndesc from QL_m05GN x where x.gnoid=h.kotaid),'') [Kota Tujuan], FORMAT(h.tglberangkat,'dd/MM/yyyy') [Tgl Berangkat], FORMAT(h.tglpulang,'dd/MM/yyyy') [Tgl Pulang], perdinmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnperdinmst h INNER JOIN QL_APP2 a ON  a.appformoid=h.perdinid INNER JOIN QL_m01US us ON us.usoid=a.appuser WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.perdinid=" + id + " --ORDER BY perdindate";

                sSqlDtl = "SELECT rd.perdindtlseq [No], rd.klasifikasi [Klasifikasi], rd.perdindtlamt [DEC_Nilai] FROM QL_trnperdindtl rd WHERE rd.perdinid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.perdindtlamt) [Total] FROM QL_trnperdindtl rd WHERE rd.perdinid=" + id + "";

            }
            else if (tblname == "QL_trnperdinrealmst")
            {
                sSqlHdr = "SELECT DISTINCT '" + CompnyCode + "' cmpcode, h.perdinid [Draft No.], CONVERT(VARCHAR(10), h.perdindate, 103) [Tgl Perdin], isnull((select x.usname from QL_m01US x where x.usoid=h.pemohon),'') [Pemohon], isnull((select x.gndesc from QL_m05GN x where x.gnoid=h.jabatanid),'') [Jabatan], isnull((select x.groupdesc from QL_mstdeptgroup x where x.groupoid=h.deptid),'') [Departemen], isnull((select x.gndesc from QL_m05GN x where x.gnoid=h.kotaid),'') [Kota Tujuan], FORMAT(h.tglberangkat,'dd/MM/yyyy') [Tgl Berangkat], FORMAT(h.tglpulang,'dd/MM/yyyy') [Tgl Pulang], perdinmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnperdinmst h INNER JOIN QL_APP2 a ON  a.appformoid=h.perdinid INNER JOIN QL_m01US us ON us.usoid=a.appuser WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.perdinid=" + id + " --ORDER BY perdindate";

                sSqlDtl = "SELECT rd.perdindtlseq [No], rd.klasifikasi [Klasifikasi], isnull(rd.perdindtlnote,'') [Keterangan], rd.perdindtlamt [DEC_Nilai] FROM QL_trnperdindtl rd WHERE rd.perdinid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.perdindtlamt) [Total] FROM QL_trnperdindtl rd WHERE rd.perdinid=" + id + "";

            }
            else if (tblname == "QL_trnprmst")
            {
                sSqlHdr = "SELECT DISTINCT '" + CompnyCode + "' cmpcode, h.prmstoid [Draft No.], CONVERT(VARCHAR(10), h.prdate, 103) [Tgl Perdin], isnull((select x.usname from QL_m01US x where x.usoid=h.usoid),'') [Pemohon], prmstnote [Note], h.createuser [Create User], a.requser [Request User], CONVERT(VARCHAR(10), a.reqdate, 103) [Request Date] FROM QL_trnprmst h INNER JOIN QL_APP2 a ON  a.appformoid=h.prmstoid INNER JOIN QL_m01US us ON us.usoid=a.appuser WHERE ISNULL(a.appstatus, '')= '' AND a.tablename = '" + tblname + "' AND h.prmstoid=" + id + " --ORDER BY prdate";

                sSqlDtl = "SELECT rd.prdtlseq [No], CASE rd.prreftype WHEN 'QL_mstitem' THEN ISNULL((SELECT x.itemdesc FROM QL_mstitem x where x.itemoid=rd.prrefoid),'') ELSE ISNULL((SELECT x.jasadesc FROM QL_mstjasa x where x.jasaoid=rd.prrefoid),'') END [Nama Barang], rd.prqty [DEC_Qty], rd.prprice [DEC_Harga], rd.prdtlamt [DEC_Nilai], isnull(rd.prdtlnote,'') [Keterangan] FROM QL_trnprdtl rd  WHERE rd.prmstoid=" + id + "";

                sSqlFtr = "SELECT SUM(rd.prdtlamt) [Total] FROM QL_trnprdtl rd WHERE rd.prmstoid=" + id + "";

            }
            else
                msg = "Approval action haven't be set";

            if (msg == "")
            {
                // Process Data for Header
                DataTable tblhdr = new ClassConnection().GetDataTable(sSqlHdr, tblname + "_hdr");
                List<modelforapp> rowshdr = new List<modelforapp>();
                if (tblhdr.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblhdr.Rows)
                    {
                        foreach (DataColumn col in tblhdr.Columns)
                        {
                            var item = dr[col].ToString();
                            rowshdr.Add(new modelforapp()
                            {
                                caption = col.ColumnName,
                                value = item
                            });
                        }
                    }
                }
                else
                    msg = "Data Header Not Found";

                // Process Data for Detail
                DataTable tbldtl = new ClassConnection().GetDataTable(sSqlDtl, tblname + "_dtl");
                List<string> colsdtl = new List<string>();
                List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
                if (msg == "" && tbldtl.Rows.Count > 0)
                {
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbldtl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        Int32 i = 0;
                        foreach (DataColumn col in tbldtl.Columns)
                        {
                            i++;
                            var item = dr[col].ToString();
                            decimal decval = 0;
                            try
                            {
                                //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                if (i != 1 && Decimal.TryParse(item, out decval))
                                {
                                    if (decval == 0)
                                        item = "0.00";
                                    else
                                        item = decval.ToString("#,##0.00");
                                }
                            }
                            catch
                            {
                                item = dr[col].ToString();
                            }
                            row.Add(col.ColumnName.Replace("DEC_", ""), item);
                            if (!colsdtl.Contains(col.ColumnName.Replace("DEC_", "")))
                                colsdtl.Add(col.ColumnName.Replace("DEC_", ""));
                        }
                        rowsdtl.Add(row);
                    }
                }
                else
                    msg = "Data Detail Not Found";

                // Process Data for Detail 2
                DataTable tbldtl2 = null;
                List<string> colsdtl2 = null;
                List<Dictionary<string, object>> rowsdtl2 = null;

                if (sSqlDtl2 != "")
                {
                    tbldtl2 = new ClassConnection().GetDataTable(sSqlDtl2, tblname + "_dtl2");
                    colsdtl2 = new List<string>();
                    rowsdtl2 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl2.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl2.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl2.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl2.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl2.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl2.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Detail 3
                DataTable tbldtl3 = null;
                List<string> colsdtl3 = null;
                List<Dictionary<string, object>> rowsdtl3 = null;

                if (sSqlDtl3 != "")
                {
                    tbldtl3 = new ClassConnection().GetDataTable(sSqlDtl3, tblname + "_dtl3");
                    colsdtl3 = new List<string>();
                    rowsdtl3 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl3.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl3.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl3.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl3.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl3.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl3.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Detail 4
                DataTable tbldtl4 = null;
                List<string> colsdtl4 = null;
                List<Dictionary<string, object>> rowsdtl4 = null;

                if (sSqlDtl4 != "")
                {
                    tbldtl4 = new ClassConnection().GetDataTable(sSqlDtl4, tblname + "_dtl4");
                    colsdtl4 = new List<string>();
                    rowsdtl4 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl4.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl4.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl4.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl4.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl4.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl4.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Detail 5
                DataTable tbldtl5 = null;
                List<string> colsdtl5 = null;
                List<Dictionary<string, object>> rowsdtl5 = null;

                if (sSqlDtl5 != "")
                {
                    tbldtl5 = new ClassConnection().GetDataTable(sSqlDtl5, tblname + "_dtl5");
                    colsdtl5 = new List<string>();
                    rowsdtl5 = new List<Dictionary<string, object>>();
                    if (msg == "" && tbldtl5.Rows.Count > 0)
                    {
                        Dictionary<string, object> row;
                        foreach (DataRow dr in tbldtl5.Rows)
                        {
                            row = new Dictionary<string, object>();
                            Int32 i = 0;
                            foreach (DataColumn col in tbldtl5.Columns)
                            {
                                i++;
                                var item = dr[col].ToString();
                                decimal decval = 0;
                                try
                                {
                                    //if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                    if (i != 1 && Decimal.TryParse(item, out decval))
                                    {
                                        if (decval == 0)
                                            item = "0.00";
                                        else
                                            item = decval.ToString("#,##0.00");
                                    }
                                }
                                catch
                                {
                                    item = dr[col].ToString();
                                }
                                row.Add(col.ColumnName.Replace("DEC_", ""), item);
                                if (!colsdtl5.Contains(col.ColumnName.Replace("DEC_", "")))
                                    colsdtl5.Add(col.ColumnName.Replace("DEC_", ""));
                            }
                            rowsdtl5.Add(row);
                        }
                    }
                    //else
                    //    msg = "Data Detail Not Found";
                }

                // Process Data for Footer
                DataTable tblftr = new ClassConnection().GetDataTable(sSqlFtr, tblname + "_ftr");
                List<modelforapp> rowsftr = new List<modelforapp>();
                if (msg == "" && tblftr.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblftr.Rows)
                    {
                        foreach (DataColumn col in tblftr.Columns)
                        {
                            var item = dr[col].ToString();
                            decimal decval = 0;
                            if (!string.IsNullOrEmpty(item) && Decimal.TryParse(item, out decval))
                            {
                                if (Decimal.Parse(item) > 999)
                                {
                                    item = decval.ToString("0,0.00");
                                }
                                else if (Decimal.Parse(item) < (-999))
                                {
                                    item = decval.ToString("0,0.00");
                                }
                                else
                                {
                                    item = decval.ToString("0.00");
                                }
                            }
                            if (col.ColumnName != "No Footer")
                            {
                                rowsftr.Add(new modelforapp()
                                {
                                    caption = col.ColumnName,
                                    value = item
                                });
                            }
                        }
                    }
                }

                if (msg == "")
                    return Json(new { msg, info, rowshdr, colsdtl, rowsdtl, colsdtl2, rowsdtl2, colsdtl3, rowsdtl3, colsdtl4, rowsdtl4, colsdtl5, rowsdtl5, rowsftr }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: WaitingAction/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(waitactmodel tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var transno = "";
            if (action == "Approved" && string.IsNullOrEmpty(transno))
            {
                var prefix = ""; string transnofld = "";
                if (tbl.tblname == "QL_trnperdinmst")
                {
                    prefix = "SPD"; transnofld = "perdinno";
                }
                else if (tbl.tblname == "QL_trnperdinrealmst")
                {
                    prefix = "RPD"; transnofld = "perdinno";
                }
                else if (tbl.tblname == "QL_trnprmst")
                {
                    prefix = "FPB"; transnofld = "prno";
                }
                transno = GenerateTransNo(prefix, CompnyCode, (tbl.tblname == "QL_trnprmst" ? "QL_trnprmst" : "QL_trnperdinmst"), transnofld);
            }

            var error = "";
            if (tbl.tblname == "QL_trnperdinmst")
            {
                ApprovalPerdin(tbl.mstoid, CompnyCode, transno, action, out error);
            }
            else if (tbl.tblname == "QL_trnperdinrealmst")
            {
                ApprovalPerdinReal(tbl.mstoid, CompnyCode, transno, action, out error);
            }
            else if (tbl.tblname == "QL_trnprmst")
            {
                ApprovalPR(tbl.mstoid, CompnyCode, transno, action, out error);
            }

            if (string.IsNullOrEmpty(error))
                return RedirectToAction("Index/" + tbl.tblname);

            ModelState.AddModelError("", error);
            return View(tbl);
        }

        private int GenPID(DateTime tanggal)
        {
            string sNo = tanggal.ToString("yy") + "-" + tanggal.ToString("MM") + "-";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(refno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_conmat WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND refno LIKE '" + sNo + "%'";
            var counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            return counter;
        }

        private int GenPIDNew()
        {
            var str = ClassFunction.GetServerTime().ToString("yy") + "-"; int counter = 1;
            sSql = "SELECT COUNT(-1) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'' ) LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "'";
            var cek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (cek > 0)
            {
                sSql = "SELECT ISNULL((SELECT TOP 1 CAST(REPLACE(refno,'" + str + "','') AS INTEGER) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'')<>'' AND ISNULL(refno,'') LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "' ORDER BY CAST(REPLACE(refno,'" + str + "','') AS INTEGER) DESC),0) + 1 AS IDNEW";
                counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            return counter;
        }

        private string GenerateTransNo(string prefix, string cmp, string tblname, string transnofld)
        {
            string sWhere = "";
            if (tblname == "QL_trnpojasamst")
            {
                tblname = "QL_trnpoitemmst";
            }
            if (tblname == "QL_trnrabmst")
            {
                sWhere = " AND revrabtype='Baru' ";
            }
            if (tblname == "QL_trnpoitemmst")
            {
                sWhere = " AND revpoitemtype='Baru' ";
            }
            int formatCounter = Convert.ToInt32(DefaultCounter);
            DateTime tanggal = ClassFunction.GetServerTime();
            var sNo = prefix + "/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            var ln = sNo.Length + formatCounter;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(" + transnofld + "," + ln + "), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM " + tblname + " WHERE " + transnofld + " LIKE '" + sNo + "%' " + sWhere + "";
            sNo = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            return sNo;
        }

        #region Approval List
        private void ApprovalPerdin(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "PerjalananDinas";
            var appoid = ClassFunction.GenerateID("QL_APP2");
            List<listappfinal> appfinal = new List<listappfinal>();
            var user_akses = Session["UserID"].ToString();
            var req_user = db.QL_APP2.FirstOrDefault(x => x.appform == ctrlname && x.appformoid == id && x.appuser == user_akses).requser; 
            string appStatus = CekStatusApp(ctrlname, req_user, user_akses);
            if (appStatus != "Final")
            {
                appfinal = BindAppFinal(ctrlname, Session["UserID"].ToString());
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnperdinmst tbl = db.QL_trnperdinmst.Find(id);

            string errDup = "";
            if (db.QL_trnperdinmst.Where(x => x.perdinid == tbl.perdinid && x.perdinmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP2 SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnperdinmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnperdinmst SET perdinmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE perdinmststatus='In Approval' AND perdinid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus != "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnperdinmst SET approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE perdinmststatus='In Approval' AND perdinid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                QL_APP2 tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP2();
                                    tblapp.cmpcode = CompnyCode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.perdinid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appuser = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnperdinmst";
                                    db.QL_APP2.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnperdinmst SET perdinmststatus='" + action + "', approvaluser1='" + Session["UserID"].ToString() + "', approvaldatetime1='" + servertime + "' " + sqlPlus + " WHERE perdinmststatus='In Approval' AND perdinid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalPerdinReal(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "RealPerjalananDinas/Form";
            var appoid = ClassFunction.GenerateID("QL_APP2");
            List<listappfinal> appfinal = new List<listappfinal>();
            var user_akses = Session["UserID"].ToString();
            var req_user = db.QL_APP2.FirstOrDefault(x => x.appform == ctrlname && x.appformoid == id && x.appuser == user_akses).requser;
            string appStatus = CekStatusApp(ctrlname, req_user, user_akses);
            if (appStatus != "Final")
            {
                appfinal = BindAppFinal(ctrlname, Session["UserID"].ToString());
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnperdinmst tbl = db.QL_trnperdinmst.Find(id);

            string errDup = "";
            if (db.QL_trnperdinmst.Where(x => x.perdinid == tbl.perdinid && x.perdinmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP2 SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnperdinrealmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnperdinmst SET perdinmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE perdinmststatus='In Approval' AND perdinid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus != "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnperdinmst SET approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE perdinmststatus='In Approval' AND perdinid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                QL_APP2 tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP2();
                                    tblapp.cmpcode = CompnyCode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.perdinid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appuser = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnperdinrealmst";
                                    db.QL_APP2.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnperdinmst SET perdinmststatus='" + action + "', approvaluser1='" + Session["UserID"].ToString() + "', approvaldatetime1='" + servertime + "' " + sqlPlus + " WHERE perdinmststatus='In Approval' AND perdinid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }

        private void ApprovalPR(int id, string cmp, string transno, string action, out string error)
        {
            var servertime = ClassFunction.GetServerTime();
            var sqlPlus = "";
            error = "";

            string ctrlname = "PRBJ";
            var appoid = ClassFunction.GenerateID("QL_APP2");
            List<listappfinal> appfinal = new List<listappfinal>();
            var user_akses = Session["UserID"].ToString();
            var req_user = db.QL_APP2.FirstOrDefault(x => x.appform == ctrlname && x.appformoid == id && x.appuser == user_akses).requser;
            string appStatus = CekStatusApp(ctrlname, req_user, user_akses);
            if (appStatus != "Final")
            {
                appfinal = BindAppFinal(ctrlname, Session["UserID"].ToString());
                if (appfinal.Count() <= 0)
                {
                    error = "Silahkab Setting Approval Final Dulu!!";
                }
            }

            QL_trnprmst tbl = db.QL_trnprmst.Find(id);

            string errDup = "";
            if (db.QL_trnprmst.Where(x => x.prmstoid == tbl.prmstoid && x.prmststatus == "Approved").Count() > 0)
            {
                errDup = "Duplicate Data!";
            }

            if (error == "" && errDup == "")
            {

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Revised")
                        {
                            sqlPlus = " ,revisereason='" + Session["rvnote"].ToString() + "', revisedatetime='" + servertime + "', reviseuser='" + Session["UserID"].ToString() + "'";
                        }
                        else if (action == "Rejected")
                        {
                            sqlPlus = " ,rejectreason='" + Session["rjnote"].ToString() + "', rejectdatetime='" + servertime + "', rejectuser='" + Session["UserID"].ToString() + "'";
                        }

                        sSql = "UPDATE QL_APP2 SET appstatus='" + action + "', appuser='" + Session["UserID"].ToString() + "', appdate='" + servertime + "' WHERE ISNULL(appstatus,'')='' AND tablename='QL_trnprmst' AND appformoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (action == "Revised" || action == "Rejected")
                        {
                            sSql = "UPDATE QL_trnprmst SET prmststatus='" + action + "', approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE prmststatus='In Approval' AND prmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (appStatus != "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnprmst SET approvaluser='" + Session["UserID"].ToString() + "', approvaldatetime='" + servertime + "' " + sqlPlus + " WHERE prmststatus='In Approval' AND prmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                QL_APP2 tblapp;
                                for (int i = 0; i < appfinal.Count(); i++)
                                {
                                    tblapp = new QL_APP2();
                                    tblapp.cmpcode = CompnyCode;
                                    tblapp.appoid = appoid++;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tbl.prmstoid;
                                    tblapp.requser = Session["UserID"].ToString();
                                    tblapp.reqdate = servertime;
                                    tblapp.appuser = appfinal[i].stoid_app;
                                    tblapp.tablename = "QL_trnprmst";
                                    db.QL_APP2.Add(tblapp);
                                    db.SaveChanges();
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (appoid - 1) + " Where tablename = 'QL_APP2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (appStatus == "Final")
                        {
                            if (action == "Approved")
                            {
                                sSql = "UPDATE QL_trnprmst SET prno='" + transno + "', prmststatus='" + action + "', approvaluser1='" + Session["UserID"].ToString() + "', approvaldatetime1='" + servertime + "' " + sqlPlus + " WHERE prmststatus='In Approval' AND prmstoid=" + id;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        error = ex.ToString();
                    }
                }
            }
        }
        #endregion

    }
}