﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Transaction
{
    public class PSvcController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public PSvcController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class mrservicemst
        {
            public string cmpcode { get; set; }
            public int mrservicemstoid { get; set; }
            public string mrserviceno { get; set; }
            public DateTime mrservicedate { get; set; }
            public string itemdesc { get; set; }            
            public string custname { get; set; }
            public string mrservicemststatus { get; set; }
            public string mrservicemstnote { get; set; }           
        }        

        public class cust
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custphone { get; set; }
        }

        public class listmat
        {          
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string refno { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }           
        }

        private void InitDDL(QL_trnmrservicemst tbl)
        {
           
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, i.itemoid, itemcode, '' refno, i.itemdesc, itemunitoid, g.gndesc AS itemunit FROM QL_mstitem i INNER JOIN QL_m05gn g ON gnoid=itemunitoid WHERE i.cmpcode='"+ CompnyCode +"' AND activeflag = 'ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
      
        [HttpPost]
        public ActionResult GetCustData()
        {
            List<cust> tbl = new List<cust>();
            sSql = "SELECT custoid, custname, custcode, custaddr, custphone1 custphone FROM QL_mstcust where activeflag = 'ACTIVE' ORDER BY custcode, custname";

            tbl = db.Database.SqlQuery<cust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
      

        private void FillAdditionalField(QL_trnmrservicemst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid='" + tbl.custoid + "'").FirstOrDefault();

            ViewBag.itemdesc = db.Database.SqlQuery<string>("SELECT itemdesc FROM QL_mstitem WHERE cmpcode='" + CompnyCode + "' AND itemoid='" + tbl.itemoid + "'").FirstOrDefault();                      
        }

        // GET: mrserviceMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("PSvc", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT som.cmpcode, som.mrservicemstoid, som.mrserviceno, som.mrservicedate, som.itemoid, (SELECT itemdesc FROM QL_mstitem where itemoid = som.itemoid) itemdesc, c.custname, som.mrservicemststatus, som.mrservicemstnote FROM QL_TRNmrserviceMST som INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode = '" + CompnyCode + "'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND som.mrservicedate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND som.mrservicedate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND som.mrservicemststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY som.mrservicedate";

            List<mrservicemst> dt = db.Database.SqlQuery<mrservicemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: mrserviceMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrservicemst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnmrservicemst();
                tbl.cmpcode = CompnyCode;
                tbl.mrservicedate = ClassFunction.GetServerTime();               
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mrservicemststatus = "In Process";
                
                Session["QL_trnmrservicedtl"] = null;
            }
            else
            {
                tbl = db.QL_trnmrservicemst.Find(CompnyCode, id);
                action = "Edit";
            }
            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: mrserviceMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmrservicemst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.mrservicedate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("mrservicedate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.mrservicemststatus == "Post")
            {
                tbl.mrserviceno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.mrserviceno = "";
            }
            if (string.IsNullOrEmpty(tbl.mrserviceres1))
                tbl.mrserviceres1 = "";
            if (string.IsNullOrEmpty(tbl.mrserviceres2))
                tbl.mrserviceres2 = "";
            if (string.IsNullOrEmpty(tbl.mrserviceres3))
                tbl.mrserviceres3 = "";
            if (string.IsNullOrEmpty(tbl.mrservicereq))
                tbl.mrservicereq = "";
            if (string.IsNullOrEmpty(tbl.mrservicecomplaint))
                tbl.mrservicecomplaint = "";
            if (string.IsNullOrEmpty(tbl.mrservicemstnote))
                tbl.mrservicemstnote = "";

            if (tbl.mrservicereq == "")
            {
                ModelState.AddModelError("mrservicereq", "Silahkan Isi Kelengkapan!!");
            }
            if (tbl.mrservicecomplaint == "")
            {
                ModelState.AddModelError("mrservicecomplaint", "Silahkan Isi Keluhan!!");
            }

            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.mrservicedate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.mrservicemststatus = "In Process";
            }
            if (tbl.mrservicemststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.mrservicemststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmrservicemst");
                var dtloid = ClassFunction.GenerateID("QL_trnmrservicedtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var servertime = ClassFunction.GetServerTime();               
                          

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                     

                        if (action == "Create")
                        {
                            
                            tbl.mrservicemstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());                            
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnmrservicemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.mrservicemstoid + " WHERE tablename='QL_trnmrservicemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();                           
                        }                             

                        objTrans.Commit();                       
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.mrservicemststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.mrservicemststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        
        }
        

        // POST: mrserviceMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrservicemst tbl = db.QL_trnmrservicemst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                       

                        db.QL_trnmrservicemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SVC/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrserviceno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrservicemst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND mrserviceno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}