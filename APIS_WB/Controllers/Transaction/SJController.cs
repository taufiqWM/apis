﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;


namespace APIS_WB.Controllers.Transaction
{
    public class SJController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public SJController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class shipmentitemdtl
        {
            public int shipmentitemdtlseq { get; set; }
            public int soitemdtloid { get; set; }
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }            
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public int shipmentitemunitoid { get; set; }
            public string shipmentitemunit { get; set; }
            public decimal qtystok { get; set; }
            public decimal soitemqty { get; set; }
            public decimal shipmentitemqty { get; set; }            
            public string shipmentitemdtlnote { get; set; }
            public decimal shipmentitemvalue { get; set; }
            public decimal shipmentitemvalueidr { get; set; }
            public string shipmentitemdtlres2 { get; set; }
            public string serialnumber { get; set; }
        }

        public class shipmentexp
        {
            public int shipmentitemexpseq { get; set; }
            public string shipmentitemexpdate { get; set; }
            public int expedisioid { get; set; }
            public string expedisi { get; set; }
            public string shipmentitemexpnote { get; set; }
            public string shipmentitemexptype { get; set; }
            public string expedisiname { get; set; }
        }

        public class soitem
        {
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string soitemdatestr { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public int rabmstoid { get; set; }
            public int rabmstoid_awal { get; set; }
            public string rabno { get; set; }
            public int curroid { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
            public string soitemmstnote { get; set; }
        }

        public class warehouse
        {
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
        }

        public class listmat
        {
            public int soitemdtloid { get; set; }
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string refno { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal qtystok { get; set; }
            public Decimal soitemqty { get; set; }
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
            public string shipmentitemdtlres2 { get; set; }
            public string serialnumber { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        public class listnopol
        {
            public string nopol { get; set; }
        }

        public class listpid
        {
            public string refno { get; set; }
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + itemoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listnopol> tbl = new List<listnopol>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listnopol>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnshipmentitemmst tbl)
        {
            sSql = "SELECT * FROM QL_mstexpedisi where activeflag = 'ACTIVE'";
            var ekspedisioid = new SelectList(db.Database.SqlQuery<QL_mstexpedisi>(sSql).ToList(), "expoid", "expname", tbl.ekspedisioid);
            ViewBag.ekspedisioid = ekspedisioid;

            sSql = "SELECT us.* FROM QL_m01us us INNER JOIN QL_m05gn gn ON gn.gnoid = us.jabatanoid where usflag = 'ACTIVE' AND gn.gndesc = 'DRIVER'";
            var driveroid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.driveroid);
            ViewBag.driveroid = driveroid;

            sSql = "select * from QL_mstvehicle where activeflag = 'ACTIVE'";
            var vhcoid = new SelectList(db.Database.SqlQuery<QL_mstvehicle>(sSql).ToList(), "vhcoid", "vhcdesc", tbl.armadaoid);
            ViewBag.armadaoid = vhcoid;

            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var shipmentitemwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.shipmentitemwhoid = shipmentitemwhoid;

            //sSql = "SELECT refno FROM QL_conmat con GROUP BY con.refno HAVING SUM(con.qtyin - con.qtyout)>0";
            //var pidstart = new SelectList(db.Database.SqlQuery<listpid>(sSql).ToList(), "refno", "refno", ViewBag.pidstart);
            //ViewBag.pidstart = pidstart;

            //sSql = "SELECT refno FROM QL_conmat con GROUP BY con.refno HAVING SUM(con.qtyin - con.qtyout)>0";
            //var pidend = new SelectList(db.Database.SqlQuery<listpid>(sSql).ToList(), "refno", "refno", ViewBag.pidend);
            //ViewBag.pidend = pidend;

        }

        [HttpPost]
        public ActionResult GetDataExpDetail(string shipmentitemtype)
        {
            List<shipmentexp> tbl = new List<shipmentexp>();

            sSql = "SELECT 0 AS shipmentitemexpseq, CONVERT(CHAR(10),GETDATE(),103) shipmentitemexpdate, vhcoid AS expedisioid, vhcno AS expedisi, '' shipmentitemexpnote, '"+ shipmentitemtype + "' AS shipmentitemtype, '' AS expedisiname FROM QL_mstvehicle WHERE activeflag = 'ACTIVE'";

            //if (shipmentitemtype == "MANDIRI")
            //{
            //    sSql = "SELECT 0 AS shipmentitemexpseq, CONVERT(CHAR(10),GETDATE(),103) shipmentitemexpdate, vhcoid AS expedisioid, vhcdesc AS expedisi, '' shipmentitemexpnote FROM QL_mstvehicle WHERE activeflag = 'ACTIVE'";
            //}
            //else
            //{
            //    sSql = "SELECT 0 AS shipmentitemexpseq, CONVERT(CHAR(10),GETDATE(),103) shipmentitemexpdate, expoid AS expedisioid, expname AS expedisi, '' shipmentitemexpnote FROM QL_mstexpedisi WHERE activeflag = 'ACTIVE'";
            //}
            tbl = db.Database.SqlQuery<shipmentexp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetItemBySO(int soitemmstoid, int rabmstoid)
        {
            List<shipmentitemdtl> tbl = new List<shipmentitemdtl>();

            sSql = "select 0 shipmentitemdtlseq, sd.soitemdtloid, i.itemoid, i.itemcode, '' refno, '' serialnumber, itemdesc, soitemdtllocoid shipmentitemwhoid, gn2.gndesc shipmentitemwh, soitemqty AS soitemqty, soitemqty shipmentitemqty, soitemunitoid shipmentitemunitoid, gn.gndesc shipmentitemunit, '' shipmentitemdtlnote, 0.0 qtystok FROM QL_trnsoitemdtl sd INNER JOIN QL_trnsoitemmst sm ON sm.soitemmstoid=sd.soitemmstoid INNER JOIN QL_m05GN gn ON gn.gnoid = sd.soitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_m05GN gn2 ON gn2.gnoid = (CASE ISNULL(sd.soitemdtllocoid,0) WHEN 0 THEN 16 ELSE sd.soitemdtllocoid END) and gn2.gngroup = 'GUDANG' INNER JOIN QL_mstitem i ON i.itemoid = sd.itemoid WHERE sd.cmpcode='" + CompnyCode +"' AND sm.soitemmstoid="+ soitemmstoid +" AND sm.rabmstoid="+ rabmstoid +" ORDER BY i.itemcode";
            tbl = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail(int whoid, int soitemmstoid, int shipmentitemmstoid, string filteritem, string pidstart, string pidend, int rabmstoid, int rabmstoid_awal)
        {
            List<listmat> tbl = new List<listmat>();

            var sPlus = ""; var sPlus2 = ""; var sPlus3 = "";
            if (!string.IsNullOrEmpty(filteritem))
            {
                sPlus += " AND i.itemdesc LIKE '%" + filteritem + "%' ";
                sPlus2 += " AND i.itemdesc LIKE '%" + filteritem + "%' ";
            }
            if (!string.IsNullOrEmpty(pidstart) && !string.IsNullOrEmpty(pidend))
            {
                sPlus += " AND CAST(RIGHT(c.refno,4) AS INTEGER) BETWEEN '" + pidstart +"' AND '"+ pidend +"' ";
                sPlus2 += " AND CAST(RIGHT(c.refno,4) AS INTEGER) BETWEEN '" + pidstart + "' AND '" + pidend + "' ";
            }
            if(rabmstoid != 0)
            {
                sPlus += " AND ISNULL(c.rabmstoid,0)=" + rabmstoid_awal + " ";
                sPlus2 += " AND ISNULL(c.formoid,0)=" + rabmstoid + " ";
                sPlus3 += " AND ISNULL(rd2.rabmstoid,0)=" + rabmstoid + " ";
            }

            sSql = "SELECT * FROM( ";
            sSql += " SELECT itemseq, soitemdtloid, itemoid, itemcode, refno, serialnumber, itemdesc, itemunitoid, itemunit, qtystok, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.soitemdtloid=st.soitemdtloid AND shipmentitemmststatus<>'Rejected' AND  sd.shipmentitemmstoid <> " + shipmentitemmstoid + "), 0.0)) AS soitemqty, shipmentitemwh, shipmentitemwhoid, shipmentitemdtlres2, 0 AS numb FROM( SELECT 0 itemseq, sod.soitemdtloid, i.itemoid, itemcode, c.refno, ISNULL(c.serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, SUM(c.qtyin-c.qtyout) qtystok, soitemqty AS soitemqty, g1.gndesc shipmentitemwh, g1.gnoid shipmentitemwhoid, '' shipmentitemdtlres2 FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid AND gngroup = 'SATUAN' INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = i.itemoid INNER JOIN QL_conmat c ON c.refoid = i.itemoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.mtrwhoid AND g1.gngroup = 'GUDANG' INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = sod.soitemmstoid WHERE sod.soitemmstoid = " + soitemmstoid + " "+ sPlus +" GROUP BY  sod.soitemdtloid, i.itemoid, itemcode, c.refno, ISNULL(c.serialnumber, ''), itemdesc, itemunitoid, g.gndesc, g1.gndesc, g1.gnoid, sod.cmpcode, sod.soitemqty HAVING SUM(c.qtyin - c.qtyout) > 0 )AS st WHERE (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.soitemdtloid=st.soitemdtloid AND shipmentitemmststatus<>'Rejected' AND  sd.shipmentitemmstoid <> " + shipmentitemmstoid + "), 0.0)) > 0 ";
            sSql += " UNION ALL  SELECT DISTINCT 0 itemseq, sod.soitemdtloid, i.itemoid, itemcode, '' refno, '' serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + shipmentitemmstoid + "), 0.0)) qtystok, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + shipmentitemmstoid + "), 0.0)) AS soitemqty, '' shipmentitemwh, 0 shipmentitemwhoid, '' shipmentitemdtlres2, 0 AS numb FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid AND gngroup = 'SATUAN' INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = i.itemoid INNER JOIN QL_trnsoitemmst c ON c.soitemmstoid=sod.soitemmstoid WHERE sod.soitemmstoid = " + soitemmstoid + " /*AND sod.soitemdtlstatus = ''*/ AND i.itemtype IN('Ongkir', 'Jasa') AND (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + shipmentitemmstoid + "), 0.0))>0 ";
            sSql += "  UNION ALL  SELECT * FROM( SELECT DISTINCT 0 itemseq, sod.soitemdtloid, i.itemoid, itemcode, c.refno, ISNULL(c.serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, c.qtyout qtystok, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + shipmentitemmstoid + "), 0.0)) AS soitemqty, g1.gndesc shipmentitemwh, g1.gnoid shipmentitemwhoid, 'Booking' shipmentitemdtlres2, ROW_NUMBER() OVER (PARTITION BY i.itemoid ORDER BY sod.soitemdtloid DESC) AS numb FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid AND gngroup = 'SATUAN' INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = i.itemoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=sod.soitemmstoid INNER JOIN QL_matbooking c ON c.refoid = i.itemoid AND som.rabmstoid=c.formoid INNER JOIN QL_m05GN g1 ON g1.gnoid = c.mtrwhoid AND g1.gngroup = 'GUDANG' WHERE sod.soitemmstoid = " + soitemmstoid + " AND c.flag='Post' " + sPlus2 + " AND (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + shipmentitemmstoid + "), 0.0))>0 )AS t /*WHERE t.numb<=(CASE WHEN ISNULL(t.refno, '')='' AND ISNULL(t.refno, '')='' THEN 1 ELSE t.soitemqty END)*/ ";
            sSql += "  UNION ALL  SELECT itemseq, soitemdtloid, itemoid, itemcode, refno, serialnumber, itemdesc, itemunitoid, itemunit, (qtystok - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid = poa.itemoid AND mb.mtrwhoid = poa.shipmentitemwhoid AND ISNULL(mb.refno, '') = ISNULL(poa.refno, '') AND ISNULL(mb.serialnumber, '') = ISNULL(poa.serialnumber, '') AND mb.flag = 'Post'),0.0)) qtystok, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode = sd.cmpcode AND sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sd.soitemdtloid = poa.soitemdtloid AND shipmentitemmststatus <> 'Rejected' AND  sd.shipmentitemmstoid <> " + shipmentitemmstoid + "), 0.0)) soitemqty, shipmentitemwh, shipmentitemwhoid, shipmentitemdtlres2, 0 AS numb FROM( SELECT 0 itemseq, sod.soitemmstoid, sod.soitemdtloid, i.itemoid, itemcode, ISNULL(mrd.refno,'') refno, ISNULL(mrd.serialnumber, '') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, SUM(con.qtyin - con.qtyout) qtystok, soitemqty, g1.gndesc shipmentitemwh, g1.gnoid shipmentitemwhoid, 'poasset' shipmentitemdtlres2 FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid AND gngroup = 'SATUAN' INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = i.itemoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = sod.soitemmstoid INNER JOIN QL_trnmrassetdtl mrd ON mrd.mrassetrefoid = sod.itemoid INNER JOIN QL_trnmrassetmst mrm ON mrm.mrassetmstoid = mrd.mrassetmstoid INNER JOIN QL_conmat con ON con.refoid=mrd.mrassetrefoid AND ISNULL(con.refno,'')=ISNULL(mrd.refno,'') AND ISNULL(con.serialnumber,'')=ISNULL(mrd.serialnumber,'') INNER JOIN QL_m05GN g1 ON g1.gnoid = con.mtrwhoid AND g1.gngroup = 'GUDANG' INNER JOIN QL_trnrabdtl2 rd2 ON rd2.rabmstoid = som.rabmstoid AND ISNULL(rd2.poassetdtloid, 0)= mrd.poassetdtloid AND rd2.itemdtl2oid = mrd.mrassetrefoid WHERE mrm.mrassetmststatus IN('Post','Approved','Closed') AND con.mtrwhoid IN(SELECT gnoid FROM QL_m05GN WHERE gngroup='gudang' and isnull(gnother1,'')='FPB') AND sod.soitemmstoid = " + soitemmstoid + " " + sPlus3 + " GROUP BY sod.soitemmstoid, sod.soitemdtloid, i.itemoid, itemcode, ISNULL(mrd.refno,''), ISNULL(mrd.serialnumber, ''), itemdesc, itemunitoid, g.gndesc, soitemqty, g1.gndesc, g1.gnoid HAVING SUM(con.qtyin - con.qtyout) > 0)AS poa WHERE (qtystok - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid = poa.itemoid AND mb.mtrwhoid = poa.shipmentitemwhoid AND ISNULL(mb.refno, '') = ISNULL(poa.refno, '') AND ISNULL(mb.serialnumber, '') = ISNULL(poa.serialnumber, '') AND mb.flag = 'Post'),0.0)) > 0 AND (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode = sd.cmpcode AND sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sd.soitemdtloid = poa.soitemdtloid AND shipmentitemmststatus <> 'Rejected' AND  sd.shipmentitemmstoid <> " + shipmentitemmstoid + "), 0.0)) > 0 ";
            sSql += " )AS tbl ORDER BY tbl.itemcode ";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();
            sSql = "SELECT * FROM QL_mstcust c WHERE c.custoid IN(SELECT DISTINCT som.custoid FROM QL_trnsoitemmst som WHERE som.soitemmststatus = 'Post' AND soitemtype<>'Jasa')";

            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSOData(int custoid)
        {
            List<soitem> tbl = new List<soitem>();
            var sPlus = "";
            if (custoid > 0)
            {
                sPlus = " AND som.custoid="+ custoid +"";
            }
            sSql = "SELECT soitemmstoid, soitemno, soitemdate,CONVERT(CHAR(10),som.soitemdate,103) soitemdatestr, c.custoid, c.custname, (SELECT custdtl2addr FROM QL_mstcustdtl2 where custdtl2oid = som.alamatoid) custaddr, rm.rabmstoid, CASE WHEN rm.revrabtype='Baru' THEN rm.rabmstoid ELSE rm.revrabmstoid END rabmstoid_awal, rm.rabno, som.curroid, rm.projectname, de.groupdesc departemen, soitemmstnote FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid INNER JOIN QL_mstcust c ON c.custoid = som.custoid where som.soitemmststatus = 'Post' AND som.soitemtype<>'Jasa' " + sPlus +" order by soitemno";

            tbl = db.Database.SqlQuery<soitem>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<shipmentitemdtl> dtDtl, List<shipmentexp> dtDtl2, Guid? uid)
        {
            Session["QL_trnshipmentitemdtl"+ uid] = dtDtl;
            Session["QL_trnshipmentitemexp"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<shipmentitemdtl> dtDtl, Guid? uid)
        {
            Session["QL_trnshipmentitemdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trnshipmentitemdtl"+ uid] == null)
            {
                Session["QL_trnshipmentitemdtl"+ uid] = new List<shipmentitemdtl>();
            }

            List<shipmentitemdtl> dataDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<shipmentexp> dtDtl, Guid? uid)
        {
            Session["QL_trnshipmentitemexp"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["QL_trnshipmentitemexp"+ uid] == null)
            {
                Session["QL_trnshipmentitemexp"+ uid] = new List<shipmentexp>();
            }

            List<shipmentexp> dataDtl = (List<shipmentexp>)Session["QL_trnshipmentitemexp"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnshipmentitemmst tbl)
        {
            ViewBag.soitemno = db.Database.SqlQuery<string>("SELECT soitemno FROM QL_trnsoitemmst WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid=" + tbl.soitemmstoid + "").FirstOrDefault();

            ViewBag.soitemdate = db.Database.SqlQuery<string>("SELECT CONVERT(CHAR(10),soitemdate,103) FROM QL_trnsoitemmst WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid=" + tbl.soitemmstoid + "").FirstOrDefault();

            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();

            ViewBag.custaddr = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 cd INNER JOIN QL_trnsoitemmst som ON som.alamatoid=cd.custdtl2oid WHERE som.cmpcode='" + CompnyCode + "' AND som.soitemmstoid=" + tbl.soitemmstoid + "").FirstOrDefault();

            ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
        }

        // GET: shipmentitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No SJ",["Customer"] = "Customer",["Project"] = "Project",["NomorSO"] = "No SO" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT som.cmpcode, som.shipmentitemmstoid, shipmentitemtype, som.shipmentitemno, som.shipmentitemdate, som.soitemmstoid, ISNULL(rm.projectname,'') projectname, sm.soitemno, c.custname, som.shipmentitemmststatus, som.shipmentitemmstnote FROM QL_trnshipmentitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_trnsoitemmst sm ON ISNULL(sm.rabmstoid,0) = ISNULL(som.rabmstoid,0) INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=ISNULL(som.rabmstoid,0) WHERE som.shipmentitemtype NOT IN ('JASA')) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.shipmentitemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.shipmentitemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.shipmentitemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.shipmentitemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Customer") sSql += " AND t.custname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "NomorSO") sSql += " AND t.soitemno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.shipmentitemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.shipmentitemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.shipmentitemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.shipmentitemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: shipmentitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl;
            string action = "Create";
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tbl = new QL_trnshipmentitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.shipmentitemmstoid = 0;
                tbl.shipmentitemno = generateNo(ClassFunction.GetServerTime());
                tbl.shipmentitemdate = ClassFunction.GetServerTime();               
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.shipmentitemmststatus = "In Process";
                tbl.custoid = 0;
                tbl.rabmstoid = 0;
                
                Session["QL_trnshipmentitemdtl"+ ViewBag.uid] = null;
                Session["QL_trnshipmentitemexp"+ ViewBag.uid] = null;
            }
            else
            {
                ViewBag.uid = Guid.NewGuid();
                action = "Edit";
                tbl = db.QL_trnshipmentitemmst.Find(CompnyCode, id);  
                              
                sSql = "select shd.soitemdtloid, shipmentitemdtlseq, i.itemoid, i.itemcode, refno, itemdesc, shipmentitemwhoid, ISNULL((SELECT gn2.gndesc FROM QL_m05GN gn2 WHERE gn2.gnoid = shd.shipmentitemwhoid and gn2.gngroup = 'GUDANG'),'') shipmentitemwh, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND sm.shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>shd.shipmentitemmstoid), 0.0) + ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl srd INNER JOIN QL_trnshipmentitemdtl sd ON sd.shipmentitemdtloid=srd.shipmentitemdtloid INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND sm.shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>shd.shipmentitemmstoid), 0.0) + ISNULL((SELECT SUM(arretitemqty) FROM QL_trnarretitemdtl srd INNER JOIN QL_trnaritemdtl ard ON ard.aritemdtloid=srd.aritemdtloid INNER JOIN QL_trnshipmentitemdtl sd ON sd.shipmentitemdtloid=ard.shipmentitemdtloid INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND sm.shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>shd.shipmentitemmstoid), 0.0)) AS soitemqty, shipmentitemqty, shipmentitemunitoid, gn.gndesc shipmentitemunit, shipmentitemdtlnote, 0.0 qtystok, ISNULL(shipmentitemdtlres2,'') shipmentitemdtlres2, ISNULL(shd.serialnumber,'') serialnumber from QL_trnshipmentitemdtl shd INNER JOIN QL_trnsoitemdtl sod ON sod.soitemdtloid = shd.soitemdtloid INNER JOIN QL_m05GN gn ON gn.gnoid = shd.shipmentitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_mstitem i ON i.itemoid = sod.itemoid WHERE shd.cmpcode='" + CompnyCode + "' AND shd.shipmentitemmstoid='" + id + "' ORDER BY shd.shipmentitemdtlseq";
                Session["QL_trnshipmentitemdtl"+ ViewBag.uid] = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();

                sSql = "SELECT shipmentitemexpseq, shipmentitemexptype, CONVERT(CHAR(10), shipmentitemexpdate,103) shipmentitemexpdate, expedisioid, CASE WHEN ISNULL(sm.shipmentitemmstres3,'')='MANDIRI' THEN (SELECT v.vhcno FROM QL_mstvehicle v WHERE v.vhcoid=sd.expedisioid) ELSE (SELECT v.expname FROM QL_mstexpedisi v WHERE v.expoid=sd.expedisioid) END expedisi, shipmentitemexpnote, expedisiname FROM QL_trnshipmentitemexp sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.shipmentitemmstoid=" + id + " ORDER BY sd.shipmentitemexpseq";
                Session["QL_trnshipmentitemexp"+ ViewBag.uid] = db.Database.SqlQuery<shipmentexp>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnshipmentitemmst tbl, string action, string tglmst, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.shipmentitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("shipmentitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            if (string.IsNullOrEmpty(tbl.shipmentitemno))
            {
                tbl.shipmentitemno = generateNo(tbl.shipmentitemdate);
            }

            if (string.IsNullOrEmpty(tbl.shipmentitemmstres1))
                tbl.shipmentitemmstres1 = "";
            if (string.IsNullOrEmpty(tbl.shipmentitemmstres2))
                tbl.shipmentitemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.shipmentitemtype))
                tbl.shipmentitemtype = "";
            if (string.IsNullOrEmpty(tbl.shipmentitemmstnote))
                tbl.shipmentitemmstnote = "";
            if (string.IsNullOrEmpty(tbl.ekspedisinoresi))
                tbl.ekspedisinoresi = "";

            List<shipmentitemdtl> dtDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"+ uid];
            List<shipmentexp> dtDtl2 = (List<shipmentexp>)Session["QL_trnshipmentitemexp"+ uid];

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {                
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].shipmentitemdtlnote))
                            dtDtl[i].shipmentitemdtlnote = "";
                        if (string.IsNullOrEmpty(dtDtl[i].refno))
                            dtDtl[i].refno = "";
                        if (string.IsNullOrEmpty(dtDtl[i].serialnumber))
                            dtDtl[i].serialnumber = "";

                        if (tbl.shipmentitemmststatus == "Post")
                        {
                            var a = dtDtl.Where(x => x.itemoid == dtDtl[i].itemoid && x.soitemdtloid == dtDtl[i].soitemdtloid).GroupBy(x => x.itemoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.shipmentitemqty) }).ToList();
                            if (dtDtl[i].shipmentitemqty <= 0)
                            {
                                ModelState.AddModelError("", "QTY SJ item " + dtDtl[i].itemdesc + "  tidak boleh 0!");
                            }
                            else
                            {
                                sSql = "select (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0) + ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl srd INNER JOIN QL_trnshipmentitemdtl sd ON sd.shipmentitemdtloid=srd.shipmentitemdtloid INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0) + ISNULL((SELECT SUM(arretitemqty) FROM QL_trnarretitemdtl srd INNER JOIN QL_trnaritemdtl ard ON ard.aritemdtloid=srd.aritemdtloid INNER JOIN QL_trnshipmentitemdtl sd ON sd.shipmentitemdtloid=ard.shipmentitemdtloid INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0)) AS soitemqty from QL_trnsoitemdtl sod INNER JOIN QL_mstitem i ON i.itemoid = sod.itemoid WHERE sod.cmpcode='" + CompnyCode + "' AND sod.soitemdtloid='" + dtDtl[i].soitemdtloid + "'";
                                var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                                if (dQty != dtDtl[i].soitemqty)
                                {
                                    dtDtl[i].soitemqty = dQty;
                                }  

                                for (int j = 0; j < a.Count(); j++)
                                {
                                    if (a[j].Qty > dtDtl[i].soitemqty)
                                    {
                                        ModelState.AddModelError("", "Qty SJ item " + dtDtl[i].itemdesc + "  tidak boleh lebih dari Qty SO!");
                                    }
                                }
                            }

                            sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                            var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                            if (type == "Barang" || type == "Rakitan")
                            {
                                var spr = ""; var rabmstoid_temp = tbl.rabmstoid_awal;
                                if (dtDtl[i].shipmentitemdtlres2 != "Booking")
                                {
                                    if (dtDtl[i].shipmentitemdtlres2 == "poasset")
                                    {
                                        rabmstoid_temp = 0;
                                    }
                                    if (!ClassFunction.IsStockAvailable(Session["CompnyCode"].ToString(), ClassFunction.GetServerTime(), dtDtl[i].itemoid, dtDtl[i].shipmentitemwhoid, dtDtl[i].shipmentitemqty, dtDtl[i].refno, rabmstoid_temp, dtDtl[i].serialnumber))
                                    {
                                        if (dtDtl[i].refno != "")
                                        {
                                            spr = " - ";
                                        }
                                        ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + spr + dtDtl[i].refno + spr + dtDtl[i].serialnumber + " Qty SJ tidak boleh lebih dari Stock Qty");
                                    }
                                }
                                else
                                {
                                    if (!ClassFunction.IsStockBookingAvailable(Session["CompnyCode"].ToString(), ClassFunction.GetServerTime(), dtDtl[i].itemoid, dtDtl[i].shipmentitemwhoid, dtDtl[i].shipmentitemqty, dtDtl[i].refno, 0, dtDtl[i].serialnumber))
                                    {
                                        if (dtDtl[i].refno != "")
                                        {
                                            spr = " - ";
                                        }
                                        ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + spr + dtDtl[i].refno + spr + dtDtl[i].serialnumber + " Qty SJ dari Booking tidak boleh lebih dari Stock Qty");
                                    }
                                }
                                //Stock Value
                                decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].itemoid);
                                decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dtDtl[i].shipmentitemqty, sValue, "OUT");
                                dtDtl[i].shipmentitemvalue = sAvgValue;
                                dtDtl[i].shipmentitemvalueidr = sAvgValue;
                            }
                            else
                            {
                                dtDtl[i].shipmentitemvalue = 0;
                                dtDtl[i].shipmentitemvalueidr = 0;
                            }
                        }
                        else
                        {
                            dtDtl[i].shipmentitemvalue = 0;
                            dtDtl[i].shipmentitemvalueidr = 0;
                        }
                    }
                    if (tbl.shipmentitemmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tbl.shipmentitemno))
                            ModelState.AddModelError("shipmentitemno", "Silahkan isi No. SJ!");
                        else if (db.QL_trnshipmentitemmst.Where(w => w.shipmentitemno == tbl.shipmentitemno & w.shipmentitemmstoid != tbl.shipmentitemmstoid).Count() > 0)
                            ModelState.AddModelError("shipmentitemno", "No. SJ yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");
                    }                        
                }
            }

            if (tbl.shipmentitemmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
                if (!ClassFunction.IsInterfaceExists("VAR_HPP_JUAL", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_HPP_JUAL"));
                if (tbl.shipmentitemtype2 == "Draft")
                {
                    ModelState.AddModelError("shipmentitemtype2", "- Tipe Transaksi Draft, Silahkan Ganti Tipe Transaksi Menjadi Complete Jika Barang Sudah Lengkap!!");
                }
            }

            sSql = "SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.custoid + "";
            string custname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.shipmentitemmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.shipmentitemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.shipmentitemmststatus = "In Process";
            }
            if (tbl.shipmentitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.shipmentitemmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnshipmentitemdtl");
                var expoid = ClassFunction.GenerateID("QL_trnshipmentitemexp");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                var iAcctgOidTrans = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode));
                var iAcctgOidHPP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_HPP_JUAL", CompnyCode));

                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.shipmentitemdate);
                tbl.rate2oid = rate2oid;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            
                            tbl.shipmentitemmstoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnshipmentitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.shipmentitemmstoid + " WHERE tablename='QL_trnshipmentitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid IN (SELECT sm.soitemmstoid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_matbooking SET flag='Post' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tbl.rabmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == tbl.shipmentitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnshipmentitemexp.Where(a => a.shipmentitemmstoid == tbl.shipmentitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentitemexp.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        QL_trnshipmentitemdtl tbldtl;

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnshipmentitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.shipmentitemdtloid = dtloid++;
                            tbldtl.shipmentitemmstoid = tbl.shipmentitemmstoid;
                            tbldtl.shipmentitemdtlseq = i + 1;
                            tbldtl.soitemdtloid = dtDtl[i].soitemdtloid;
                            tbldtl.refno = (dtDtl[i].refno == null ? "" : dtDtl[i].refno);
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.shipmentitemqty = dtDtl[i].shipmentitemqty;
                            tbldtl.shipmentitemunitoid = dtDtl[i].shipmentitemunitoid;
                            tbldtl.shipmentitemwhoid = dtDtl[i].shipmentitemwhoid;
                            tbldtl.shipmentitemdtlstatus = "";
                            tbldtl.shipmentitemdtlnote = (dtDtl[i].shipmentitemdtlnote == null ? "" : dtDtl[i].shipmentitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.shipmentitemdtlres1 = "";
                            tbldtl.shipmentitemdtlres2 = dtDtl[i].shipmentitemdtlres2;
                            tbldtl.shipmentitemdtlres3 = "";
                            tbldtl.shipmentitemvalueidr = dtDtl[i].shipmentitemvalueidr;
                            tbldtl.shipmentitemvalueusd = 0;
                            tbldtl.shipmentitemvalue = dtDtl[i].shipmentitemvalue;
                            tbldtl.serialnumber = dtDtl[i].serialnumber;

                            db.QL_trnshipmentitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.shipmentitemtype2 == "Complete")
                            {
                                if (dtDtl[i].shipmentitemdtlres2 == "Booking")
                                {
                                    sSql = "UPDATE QL_matbooking SET flag='Closed' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tbl.rabmstoid + " AND refoid=" + dtDtl[i].itemoid + " AND ISNULL(refno,'')='" + (dtDtl[i].refno ?? "") + "' AND ISNULL(serialnumber,'')='" + (dtDtl[i].serialnumber ?? "") + "' ";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }

                            if (tbl.shipmentitemmststatus == "Post" )
                            {
                                sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                                var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                                if (type == "Barang" || type == "Rakitan")
                                {
                                    int rab = tbl.rabmstoid_awal;
                                    if (dtDtl[i].shipmentitemdtlres2 == "Booking" || dtDtl[i].shipmentitemdtlres2 == "poasset")
                                    {
                                        //rab = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT rabmstoid FROM QL_matbooking WHERE formoid="+ tbl.rabmstoid_awal +" AND refoid="+ dtDtl[i].itemoid + " AND mtrwhoid="+ dtDtl[i].shipmentitemwhoid + " AND ISNULL(refno,'')='"+ dtDtl[i].refno + "' AND ISNULL(serialnumber,'')='" + dtDtl[i].serialnumber + "'),0) AS tbl").FirstOrDefault();
                                        rab = 0;
                                    }

                                    // Insert QL_conmat
                                    db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "SJFG", "QL_trnshipmentitemdtl", tbl.shipmentitemmstoid, dtDtl[i].itemoid, "FINISH GOOD", dtDtl[i].shipmentitemwhoid, dtDtl[i].shipmentitemqty * -1, "Surat Jalan", tbl.shipmentitemno + " | " + custname, Session["UserID"].ToString(), dtDtl[i].refno, dtDtl[i].shipmentitemvalueidr, 0, 0, null, tbldtl.shipmentitemdtloid, rab, dtDtl[i].shipmentitemvalueidr, dtDtl[i].serialnumber));
                                    db.SaveChanges();
                                }                                
                            }

                            var z = dtDtl.Where(x => x.itemoid == dtDtl[i].itemoid).GroupBy(x => x.itemoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.shipmentitemqty) }).ToList();

                            for (int j = 0; j < z.Count(); j++)
                            {
                                sSql = "SELECT (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=dod.soitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0)) AS soitemqty FROM QL_trnsoitemdtl dod WHERE dod.cmpcode='" + CompnyCode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid;
                                var qtyOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

                                if (z[j].Qty >= qtyOs)
                                {
                                    sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnrabmst SET statusjual='SJ PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst mr ON mr.soitemmstoid = pm.soitemmstoid where mr.shipmentitemmstoid = " + tbl.shipmentitemmstoid + ")";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid=" + tbl.soitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnsoitemdtl WHERE cmpcode='" + CompnyCode + "' AND soitemdtlstatus='' AND soitemmstoid=" + tbl.soitemmstoid + " AND soitemdtloid<>" + dtDtl[i].soitemdtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnrabmst SET statusjual='SJ' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst mr ON mr.soitemmstoid = pm.soitemmstoid where mr.shipmentitemmstoid = " + tbl.shipmentitemmstoid + ") AND (SELECT COUNT(*) FROM QL_trnsoitemdtl pod INNER JOIN QL_trnsoitemmst pom ON pom.soitemmstoid = pod.soitemmstoid WHERE pom.cmpcode='" + CompnyCode + "' AND soitemdtlstatus='' AND pom.rabmstoid IN (SELECT rabmstoid FROM QL_trnsoitemmst where soitemmstoid = " + tbl.soitemmstoid + ") AND pod.soitemdtloid<>" + dtDtl[i].soitemdtloid + ")=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnshipmentitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count() > 0)
                            {
                                QL_trnshipmentitemexp tbldtl2;

                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnshipmentitemexp();
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.shipmentitemexpoid = expoid++;
                                    tbldtl2.shipmentitemmstoid = tbl.shipmentitemmstoid;
                                    tbldtl2.shipmentitemexpseq = i + 1;
                                    tbldtl2.shipmentitemexpdate = DateTime.Parse(ClassFunction.toDate(dtDtl2[i].shipmentitemexpdate));
                                    tbldtl2.expedisioid = dtDtl2[i].expedisioid;
                                    tbldtl2.shipmentitemexpnote = dtDtl2[i].shipmentitemexpnote;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.shipmentitemexptype = dtDtl2[i].shipmentitemexptype;
                                    tbldtl2.expedisiname = dtDtl2[i].expedisiname;

                                    db.QL_trnshipmentitemexp.Add(tbldtl2);
                                    db.SaveChanges();
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (expoid - 1) + " WHERE tablename='QL_trnshipmentitemexp'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (tbl.shipmentitemmststatus == "Post")
                        {
                            decimal glamt = dtDtl.Sum(x => x.shipmentitemvalue);
                            decimal glamtIDR = dtDtl.Sum(x => x.shipmentitemvalueidr);
                            
                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.shipmentitemno + " | " + custname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;

                            // Insert QL_trngldtl
                            // D : Persediaan Barang Dlm Perjalanan
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "D", glamt, tbl.shipmentitemno, tbl.shipmentitemno + " | " + custname + " | "+ tbl.shipmentitemmstnote, "Post", Session["UserID"].ToString(), servertime, glamtIDR, 0, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0));
                            // C : Persediaan Barang
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidStock, "C", glamt, tbl.shipmentitemno, tbl.shipmentitemno + " | " + custname + " | " + tbl.shipmentitemmstnote, "Post", Session["UserID"].ToString(), servertime, glamtIDR, 0, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0));

                            //glmstoid += 1;
                            //// Insert QL_trnglmst
                            //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.shipmentitemno + " | " + custname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            //db.SaveChanges();

                            //glseq = 1;

                            //// Insert QL_trngldtl
                            //// D : HPP
                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidHPP, "D", glamt, tbl.shipmentitemno, tbl.shipmentitemno + " | " + custname + " | " + tbl.shipmentitemmstnote, "Post", Session["UserID"].ToString(), servertime, glamtIDR, 0, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0));
                            //// C : Persediaan Barang Dlm Perjalanan
                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "C", glamt, tbl.shipmentitemno, tbl.shipmentitemno + " | " + custname + " | " + tbl.shipmentitemmstnote, "Post", Session["UserID"].ToString(), servertime, glamtIDR, 0, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0));


                            sSql = "UPDATE QL_ID SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();                       
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.shipmentitemmststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.shipmentitemmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        
        }
        

        // POST: shipmentitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl = db.QL_trnshipmentitemmst.Find(CompnyCode, id);
            List<QL_trnshipmentitemdtl> dtDtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == id && a.cmpcode == CompnyCode).ToList();
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid IN (SELECT sm.soitemmstoid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='SJ PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst rm ON rm.soitemmstoid = pm.soitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND rm.shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst rm ON rm.soitemmstoid = pm.soitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND rm.shipmentitemmstoid=" + id + ") AND (SELECT COUNT(*) FROM QL_trnsoitemdtl pod INNER JOIN QL_trnsoitemmst pom ON pom.soitemmstoid = pod.soitemmstoid WHERE pom.cmpcode='" + CompnyCode + "' AND soitemdtlstatus='' AND pom.rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnshipmentitemmst mm INNER JOIN QL_trnsoitemmst pm ON pm.soitemmstoid = mm.soitemmstoid where mm.shipmentitemmstoid <> " + id + "))=0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (tbl.shipmentitemtype2 == "Complete")
                            {
                                if (dtDtl[i].shipmentitemdtlres2 == "Booking")
                                {
                                    sSql = "UPDATE QL_matbooking SET flag='Post' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + tbl.rabmstoid + " AND refoid=" + dtDtl[i].itemoid + " AND ISNULL(refno,'')='" + (dtDtl[i].refno ?? "") + "' AND ISNULL(serialnumber,'')='" + (dtDtl[i].serialnumber ?? "") + "' ";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }    
                        }   

                        var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trnshipmentitemexp.Where(a => a.shipmentitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnshipmentitemexp.RemoveRange(trndtl2);
                        db.SaveChanges();

                        db.QL_trnshipmentitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: shipmentitemMaterial/SaveRev/5/11
        [HttpPost, ActionName("SaveRev")]
        [ValidateAntiForgeryToken]
        public ActionResult SavedConfirmed(int id, string note, string tgl, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl = db.QL_trnshipmentitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();
            List<shipmentexp> dtDtl2 = (List<shipmentexp>)Session["QL_trnshipmentitemexp" + uid];
            var expoid = ClassFunction.GenerateID("QL_trnshipmentitemexp");
            DateTime tanggal = DateTime.Parse(ClassFunction.toDate(tgl));

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemdate = '" + tanggal.ToString("yyyy-MM-dd") + "', shipmentitemmstnote  = '" + ClassFunction.Tchar(note) + "' where shipmentitemmstoid = " + id + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trnshipmentitemexp.Where(a => a.shipmentitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnshipmentitemexp.RemoveRange(trndtl2);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count() > 0)
                            {
                                QL_trnshipmentitemexp tbldtl2;

                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnshipmentitemexp();
                                    tbldtl2.cmpcode = tbl.cmpcode;
                                    tbldtl2.shipmentitemexpoid = expoid++;
                                    tbldtl2.shipmentitemmstoid = tbl.shipmentitemmstoid;
                                    tbldtl2.shipmentitemexpseq = i + 1;
                                    tbldtl2.shipmentitemexpdate = DateTime.Parse(ClassFunction.toDate(dtDtl2[i].shipmentitemexpdate));
                                    tbldtl2.expedisioid = dtDtl2[i].expedisioid;
                                    tbldtl2.shipmentitemexpnote = dtDtl2[i].shipmentitemexpnote;
                                    tbldtl2.upduser = tbl.upduser;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.shipmentitemexptype = dtDtl2[i].shipmentitemexptype;
                                    tbldtl2.expedisiname = dtDtl2[i].expedisiname;

                                    db.QL_trnshipmentitemexp.Add(tbldtl2);
                                    db.SaveChanges();
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (expoid - 1) + " WHERE tablename='QL_trnshipmentitemexp'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(shipmentitemno, 14), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND shipmentitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult generateNo2(string tgl, int oid)
        {
            DateTime tanggal = DateTime.Parse(ClassFunction.toDate(tgl));
            string mt = ""; string nomor = "";

            if (oid != 0)
            {
                sSql = "SELECT FORMAT(shipmentitemdate,'MM') as mt FROM QL_trnshipmentitemmst WHERE shipmentitemmstoid="+ oid +"";
                mt = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                sSql = "SELECT shipmentitemno as nomor FROM QL_trnshipmentitemmst WHERE shipmentitemmstoid=" + oid + "";
                nomor = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            }
            
            string sNo = "SJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(shipmentitemno, 14), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND shipmentitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            if (tanggal.ToString("MM") != mt)
            {
                sNo = sNo + sCounter;
            }
            else
            {
                sNo = nomor;
            }

            return Json(sNo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSJItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SJReport.pdf");
        }

        public ActionResult PrintReportLabel(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSJItemLabel.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SJReportLabel.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}