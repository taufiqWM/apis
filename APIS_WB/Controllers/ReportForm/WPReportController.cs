﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class WPReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public WPReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;
            //if (DDLBusinessUnit.Count() > 0)
            //{
            sSql = "SELECT * FROM QL_mstcurr WHERE currcode IN ('IDR', 'USD') order by currcode";
            var DDLCurrency = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "currcode", "currcode");
            ViewBag.DDLCurrency = DDLCurrency;
            //}

            var DDLCOA = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_BANK")).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;
        }

        [HttpPost]
        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_BANK");
                tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("WPReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLType, string StartDate, string EndDate, string DDLPeriod, string DDLCurrency, string DDLOrderBy, string ReportType, string TextNomor, string DDLNomor, string DDLCOA)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "rptWeeklyPembayaran.rpt";
            var rptname = "WeeklyPembayaranReport";

            string kasOid = ClassFunction.GetDataAcctgOid("VAR_CASH", CompnyCode);
            string bankOid = ClassFunction.GetDataAcctgOid("VAR_BANK", CompnyCode);

            sSql = "DECLARE @tgl AS VARCHAR(20) SET @tgl = '"+ EndDate + "'; SELECT *, ISNULL((SELECT SUM(debet - credit) amt FROM(SELECT CASE WHEN gld.gldbcr = 'D' THEN glamt ELSE 0.0 END debet, CASE WHEN gld.gldbcr = 'C' THEN glamt ELSE 0.0 END credit FROM QL_trnglmst glm INNER JOIN QL_trngldtl gld ON gld.glmstoid = glm.glmstoid WHERE glm.gldate <= CAST(@tgl + ' 00:00:00' AS DATETIME) AND gld.acctgoid IN(" + kasOid + "))AS dt),0.0) + ISNULL((SELECT SUM(debet - credit) amt FROM(SELECT CASE WHEN gld.gldbcr = 'D' THEN glamt ELSE 0.0 END debet, CASE WHEN gld.gldbcr = 'C' THEN glamt ELSE 0.0 END credit FROM QL_trnglmst glm INNER JOIN QL_trngldtl gld ON gld.glmstoid = glm.glmstoid WHERE glm.gldate <= CAST(@tgl + ' 00:00:00' AS DATETIME) AND gld.acctgoid IN("+ bankOid + "))AS dt),0.0) kasbank, CASE WHEN [Tipe]='B. RENCARANA PENGELUARAN' THEN ([Grand Total] * (-1)) ELSE [Grand Total] END totaldana FROM( /*AR ITEM*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.aritemno AS[Nomor], c.custname AS[SuppCust], som.soitemno AS[POSO], arm.aritemduedate[Jatuh Tempo], arm.aritemgrandtotal AS[Grand Total], 'A. RENCARANA PENERIMAAN' AS[Tipe], 'A1. Piutang Jatuh Tempo' AS[SubTipe] FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid = arm.custoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = arm.somstoid WHERE arm.aritemmststatus = 'Post' AND DATEDIFF(DAY, arm.aritemduedate, CAST(@tgl + ' 00:00:00' AS DATETIME)) > 0  UNION ALL /*AR ASSET*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.arassetno AS[Nomor], c.custname AS[SuppCust], som.soassetno AS[POSO], arm.arassetdate[Jatuh Tempo], arm.arassetgrandtotal AS[Grand Total], 'A. RENCARANA PENERIMAAN' AS[Tipe], 'A1. Piutang Jatuh Tempo' AS[SubTipe] FROM QL_trnarassetmst arm INNER JOIN QL_mstcust c ON c.custoid = arm.custoid INNER JOIN QL_trnsoassetmst som ON som.soassetmstoid = arm.somstoid WHERE arm.arassetmststatus = 'Post' AND DATEDIFF(DAY, arm.arassetdate, CAST(@tgl + ' 00:00:00' AS DATETIME)) > 0  UNION ALL /*AR GIRO*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = cb.cmpcode) AS[BU Name], cb.cmpcode AS[cmpcode], cb.cashbankno AS[Nomor], ISNULL((SELECT c.custname FROM QL_mstcust c WHERE c.custoid = cb.refsuppoid), '') AS[SuppCust], '' AS[POSO], cb.cashbankduedate[Jatuh Tempo], cb.cashbankamt AS[Grand Total], 'A. RENCARANA PENERIMAAN' AS[Tipe], 'A2. Piutang Giro Jatuh Tempo' AS[SubTipe] FROM QL_trncashbankmst cb WHERE cb.cashbankstatus = 'Post' AND cb.cashbanktype = 'BGM' AND ISNULL(cb.cashbankres1, '')= '' AND cb.cashbanktakegiroreal <> '1900-01-01' AND DATEDIFF(DAY, cb.cashbankduedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*PO ITEM CASH*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.poitemno AS[Nomor], c.suppname AS[SuppCust], arm.poitemno AS[POSO], arm.poitemdate[Jatuh Tempo], arm.poitemgrandtotalamt AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B1. PO CASH' AS[SubTipe] FROM QL_trnpoitemmst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.suppoid INNER JOIN QL_m05GN g ON g.gnoid = arm.poitempaytypeoid AND gngroup = 'PAYMENT TERM' WHERE arm.poitemmststatus IN('Post', 'Closed') AND CAST(g.gnother1 AS INT)= 0 AND arm.poitemmstoid NOT IN(SELECT dp.pomstoid FROM QL_trndpap dp WHERE dp.pomstoid = arm.poitemmstoid AND dp.poreftype = 'QL_trnpoitemmst') AND DATEDIFF(DAY, arm.poitemdate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*PO ASSET CASH*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.poassetno AS[Nomor], c.suppname AS[SuppCust], arm.poassetno AS[POSO], arm.poassetdate[Jatuh Tempo], arm.poassetgrandtotalamt AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B1. PO CASH' AS[SubTipe] FROM QL_trnpoassetmst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.suppoid INNER JOIN QL_m05GN g ON g.gnoid = arm.poassetpaytypeoid AND gngroup = 'PAYMENT TERM' WHERE arm.poassetmststatus IN('Post', 'Closed') AND CAST(g.gnother1 AS INT)= 0 AND arm.poassetmstoid NOT IN(SELECT dp.pomstoid FROM QL_trndpap dp WHERE dp.pomstoid = arm.poassetmstoid AND dp.poreftype = 'QL_trnpoassetmst') AND DATEDIFF(DAY, arm.poassetdate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*AP ITEM*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.apitemno AS[Nomor], c.suppname AS[SuppCust], som.poitemno AS[POSO], arm.apitemduedate[Jatuh Tempo], arm.apitemgrandtotal AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B2. Hutang Jatuh Tempo' AS[SubTipe] FROM QL_trnapitemmst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.suppoid INNER JOIN QL_trnpoitemmst som ON som.poitemmstoid = arm.poitemmstoid INNER JOIN QL_m05GN g ON g.gnoid = arm.apitempaytypeoid AND gngroup = 'PAYMENT TERM' WHERE arm.apitemmststatus = 'Post' AND CAST(g.gnother1 AS INT)> 0 AND DATEDIFF(DAY, arm.apitemduedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*AP ASSET*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.apassetno AS[Nomor], c.suppname AS[SuppCust], som.poassetno AS[POSO], arm.apassetduedate[Jatuh Tempo], arm.apassetgrandtotal AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B2. Hutang Jatuh Tempo' AS[SubTipe] FROM QL_trnapassetmst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.suppoid INNER JOIN QL_trnpoassetmst som ON som.poassetmstoid = arm.poassetmstoid INNER JOIN QL_m05GN g ON g.gnoid = arm.apassetpaytypeoid AND gngroup = 'PAYMENT TERM' WHERE arm.apassetmststatus = 'Post' AND CAST(g.gnother1 AS INT)> 0 AND DATEDIFF(DAY, arm.apassetduedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL  /*AP GIRO*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = cb.cmpcode) AS[BU Name], cb.cmpcode AS[cmpcode], cb.cashbankno AS[Nomor], ISNULL((SELECT c.suppname FROM QL_mstsupp c WHERE c.suppoid = cb.refsuppoid),'') AS[SuppCust], ISNULL(STUFF((SELECT ', ' + ft.poitemno FROM(SELECT DISTINCT CAST(pom.poitemno AS VARCHAR) poitemno FROM QL_trnpayap cbx INNER JOIN QL_trnapitemmst ardx ON ardx.apitemmstoid = cbx.refoid AND cbx.reftype = 'QL_trnapitemmst' INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = ardx.poitemmstoid WHERE cbx.cashbankoid = cb.cashbankoid) ft FOR XML PATH('')), 1, 1, ''),'') AS[POSO], cb.cashbankduedate[Jatuh Tempo], cb.cashbankamt AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B3. Hutang Giro Jatuh Tempo' AS[SubTipe] FROM QL_trncashbankmst cb WHERE cb.cashbankstatus = 'Post' AND cb.cashbanktype = 'BGK' AND ISNULL(cb.cashbankres1, '')= 'Closed' AND cb.cashbanktakegiroreal <> '1900-01-01' AND DATEDIFF(DAY, cb.cashbankduedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*AP DIRECT*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.apdirno AS[Nomor], arm.apdirmstnote AS[SuppCust], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid = arm.rabmstoid AND som.soitemtype = ''),'') AS[POSO], arm.apdirduedate[Jatuh Tempo], arm.apdirgrandtotal AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B4. Hutang Lainnya Jatuh Tempo' AS[SubTipe] FROM QL_trnapdirmst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.apdirmstoid WHERE arm.apdirmststatus = 'Post' AND DATEDIFF(DAY, arm.apdirduedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*AP KASBON*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.kasbon2no AS[Nomor], arm.kasbon2note AS[SuppCust], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid = arm.rabmstoid AND som.soitemtype = ''),'') AS[POSO], arm.kasbon2duedate[Jatuh Tempo], arm.kasbon2amt AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B4. Hutang Lainnya Jatuh Tempo' AS[SubTipe] FROM QL_trnkasbon2mst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.kasbon2mstoid WHERE arm.kasbon2mststatus = 'Post' AND kasbon2group = 'KASBON' AND DATEDIFF(DAY, arm.kasbon2duedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0  UNION ALL /*AP KASBON REAL*/ SELECT(SELECT div.divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode) AS[BU Name], arm.cmpcode AS[cmpcode], arm.kasbon2no AS[Nomor], arm.kasbon2note AS[SuppCust], ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid = arm.rabmstoid AND som.soitemtype = ''),'') AS[POSO], arm.kasbon2duedate[Jatuh Tempo], ABS(arm.kasbon2amtselisih) AS[Grand Total], 'B. RENCARANA PENGELUARAN' AS[Tipe], 'B4. Hutang Lainnya Jatuh Tempo' AS[SubTipe] FROM QL_trnkasbon2mst arm INNER JOIN QL_mstsupp c ON c.suppoid = arm.kasbon2mstoid WHERE arm.kasbon2mststatus = 'Post' AND kasbon2group = 'REALISASI' AND arm.kasbon2amtselisih < 0 AND DATEDIFF(DAY, arm.kasbon2duedate, CAST(@tgl + ' 00:00:00' AS DATETIME))> 0 )AS dt  WHERE[cmpcode]='" + CompnyCode +"'";
            //if (StartDate != "" && EndDate != "")
            //{
            //    sSql += " AND [Jatuh Tempo]>=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND [Jatuh Tempo]<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            //}
            sSql += " ORDER BY [Tipe], [SubTipe]";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartDate) + " - " + ClassFunction.toDate(EndDate));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable(); ;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}