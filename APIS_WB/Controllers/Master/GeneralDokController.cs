﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class GeneralDokController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public GeneralDokController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listgeneral
        {
            public int gnoid { get; set; }
            public string gndesc { get; set; }
            public string gngroup { get; set; }
            public string gnflag { get; set; }

        }
        public class listddl
        {
            public string gnoid { get; set; }
            public string gndesc { get; set; }
        }

        // GET/POST: General
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select * FROM QL_m05GN gn WHERE gn.cmpcode='" + CompnyCode + "' AND gn.gngroup='DOKUMEN' " + sfilter + " order by gn.gndesc desc ";
            List<listgeneral> generals = db.Database.SqlQuery<listgeneral>(sSql).ToList();
            return View(generals);
        }
        private void InitDDL(QL_m05GN tbl)
        {
            sSql = "SELECT CAST(gnoid AS VARCHAR(30)) AS gnoid, gndesc FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='NEGARA' ORDER BY gndesc";
            var ddlgnother1 = new SelectList(db.Database.SqlQuery<listddl>(sSql).ToList(), "gnoid", "gndesc", tbl.gnother1);
            ViewBag.ddl1 = ddlgnother1;

            sSql = "SELECT CAST(gnoid AS VARCHAR(30)) AS gnoid, gndesc FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='PROVINSI' ORDER BY gndesc";
            var ddlgnother2 = new SelectList(db.Database.SqlQuery<listddl>(sSql).ToList(), "gnoid", "gndesc", tbl.gnother2);
            ViewBag.ddl2 = ddlgnother2;
        }

        // GET: General/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m05GN qL_m05GN;
            string action = "Create";
            if (id == null)
            {
                qL_m05GN = new QL_m05GN();
                qL_m05GN.cmpcode = Session["CompnyCode"].ToString();
                qL_m05GN.crtuser = Session["UserID"].ToString();
                qL_m05GN.crttime = ClassFunction.GetServerTime();
            }
            else
            {
                qL_m05GN = db.QL_m05GN.Find(Session["CompnyCode"].ToString(), id);
                action = "Edit";
            }

            if (qL_m05GN == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(qL_m05GN);
            FillAdditionalField(qL_m05GN);
            return View(qL_m05GN);
        }

        // POST: General/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m05GN qL_m05GN, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(qL_m05GN.gndesc))
                ModelState.AddModelError("gndesc", "Please Fill Description!!");
            else if (db.QL_m05GN.Where(w => w.gndesc == qL_m05GN.gndesc & w.gngroup == qL_m05GN.gngroup & w.gnoid != qL_m05GN.gnoid).Count() > 0)
                ModelState.AddModelError("gndesc", "This Description already use, Please Fill Another Description!!");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_m05GN");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            qL_m05GN.gnoid = mstoid;
                            qL_m05GN.crttime = servertime;
                            qL_m05GN.crtuser = Session["UserID"].ToString();
                            qL_m05GN.updtime = servertime;
                            qL_m05GN.upduser = Session["UserID"].ToString();
                            db.QL_m05GN.Add(qL_m05GN);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m05GN'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            qL_m05GN.updtime = servertime;
                            qL_m05GN.upduser = Session["UserID"].ToString();
                            db.Entry(qL_m05GN).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(qL_m05GN);
            FillAdditionalField(qL_m05GN);
            return View(qL_m05GN);
        }

        private void FillAdditionalField(QL_m05GN tbl)
        {
            ViewBag.tbdecimal = db.Database.SqlQuery<string>("SELECT ISNULL(gnother1,'') FROM QL_m05GN WHERE gnoid=" + tbl.gnoid + "").FirstOrDefault();
            ViewBag.optype = db.Database.SqlQuery<string>("SELECT ISNULL(gnother2,'') FROM QL_m05GN WHERE gnoid=" + tbl.gnoid + "").FirstOrDefault();
            ViewBag.optype2 = db.Database.SqlQuery<string>("SELECT ISNULL(gnother1,'') FROM QL_m05GN WHERE gnoid=" + tbl.gnoid + "").FirstOrDefault();
        }

        // POST: General/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_m05GN qL_m05GN = db.QL_m05GN.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_m05GN.Remove(qL_m05GN);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult getGeneral(int? groid, string grname, string grflag, int? gnoid, int? orgnoid)
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> qL_m05GN = new List<QL_m05GN>();
            if (Session["UserID"] == null)
            {
                result = "failed";
                msg = "Session Null";
            }
            else
            {
                var sWhere = "";
                if (groid != null)
                {
                    sWhere += " AND gn.groid = " + groid;
                }
                if (grname != null)
                {
                    sWhere += " AND grname = '" + grname + "'";
                }
                if (grflag != null)
                {
                    sWhere += " AND grflag = '" + grflag + "'";
                }
                if (gnoid != null)
                {
                    sWhere += " AND gnoid = '" + gnoid + "'";
                }
                var sElse = "";
                if (orgnoid != null)
                {
                    sElse += " OR gnoid = " + orgnoid;
                }

                sSql = "Select gn.* from QL_m05GN gn INNER JOIN QL_m06GR gr ON gr.groid=gn.groid WHERE ( gn.cmpcode = '" + Session["CompnyCode"].ToString() + "' " + sWhere + ")" + sElse;
                qL_m05GN = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();
            }

            return Json(new { result, msg, qL_m05GN }, JsonRequestBehavior.AllowGet);
        }
    }
}