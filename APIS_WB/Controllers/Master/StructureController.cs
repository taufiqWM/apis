﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class StructureController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public StructureController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET/POST: Structure
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select * FROM QL_m07ST WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter;
            List<QL_m07ST> mstuser = db.Database.SqlQuery<QL_m07ST>(sSql).ToList();
            return View(db.QL_m07ST.ToList());
        }

        // GET: Structure/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m07ST qL_m07ST;
            string action = "Create";
            if (id == null)
            {
                qL_m07ST = new QL_m07ST();
                qL_m07ST.cmpcode = Session["CompnyCode"].ToString();
            }
            else
            {
                action = "Edit";
                qL_m07ST = db.QL_m07ST.Find(Session["CompnyCode"].ToString(), id);
            }
            
            if (qL_m07ST == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            return View(qL_m07ST);
        }

        // POST: Structure/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m07ST tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.stname))
                ModelState.AddModelError("usoid", "Please Fill Structure Name!!");
            else if (db.QL_m07ST.Where(w => w.stname == tbl.stname & w.stoid != tbl.stoid).Count() > 0)
                ModelState.AddModelError("usoid", "This Structure Name already exist, Please Fill Another Structure Name!!");
            if (string.IsNullOrEmpty(tbl.stdesc))
                tbl.stdesc = "";

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_m07ST");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.stoid = mstoid;
                            tbl.crttime = servertime;
                            tbl.crtuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_m07ST.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_m07ST'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            return View(tbl);
        }
        

        // POST: Structure/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            string result = "sukses";
            string msg = "";
            if (db.QL_m01US.Where(w => w.stoid == id).Count() > 0)
            {
                result = "failed";
                msg += "this Structure already used in Another Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_m07ST qL_m07ST = db.QL_m07ST.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_m07ST.Remove(qL_m07ST);
                        db.SaveChanges();
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        //return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
