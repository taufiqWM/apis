﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APIS_WB.Models.ViewModel
{

    public class mdFilter
    {
        public string ddlfilter { get; set; }
        public string txtfilter { get; set; }
        public string ddlstatus { get; set; }
        public bool cbfilterperiod { get; set; }
        public string filterperiod1 { get; set; }
        public string filterperiod2 { get; set; }
    }

    public class ModelFilter
    {
        public string filterperiodfrom { get; set; }
        public string filterperiodto { get; set; }
        //public string filterstatus { get; set; }
    }

    public class mdFilterList
    {
        public string filterddl { get; set; }
        public string filtertext { get; set; }
        public string filterstatus { get; set; }
        public string filterperiodfrom { get; set; }
        public string filterperiodto { get; set; }
    }

    public class SendEmailModel
    {
        [DataType(DataType.EmailAddress), Display(Name = "To")]
        [Required]
        public string ToEmail { get; set; }
        [Display(Name = "Body")]
        [DataType(DataType.MultilineText)]
        public string EMailBody { get; set; }
        [Display(Name = "Subject")]
        public string EmailSubject { get; set; }
        [DataType(DataType.EmailAddress)]
        [Display(Name = "CC")]
        public string EmailCC { get; set; }
        [DataType(DataType.EmailAddress)]
        [Display(Name = "BCC")]
        public string EmailBCC { get; set; }
    }

    public class FilterHPE
    {
        //public IEnumerable<APIS_WB.Models.DB.QL_t05HPE> HPE { get; set; }
        //public mdFilter HPEfil { get; set; }
    }

    public class dtAppHeader
    {
        public string caption { get; set; }
        public string sept { get; set; }
        public string value { get; set; }
    }

    public class dtFiles
    {
        public int seq { get; set; }
        public string filename { get; set; }
        public string fileurl { get; set; }
        public string fileflag { get; set; }
    }


    public class dtUpm
    {
        //Header
        public string cmpcode { get; set; }
        public int upmoid { get; set; }
        public string upmno { get; set; }
        public string upmwono { get; set; }
        public DateTime upmdate { get; set; }
        public int upmtypeoid { get; set; }
        public string upmtype { get; set; }
        public string upmmstnote { get; set; }
        public string crtuser { get; set; }
        public DateTime crttime { get; set; }
        public string appuser1 { get; set; }
        public string appuser2 { get; set; }
        public string appuser3 { get; set; }
        public string crtstname { get; set; }
        public string appstname1 { get; set; }
        public string appstname2 { get; set; }
        public string appstname3 { get; set; }
        public string crtpath { get; set; }
        public string apppath1 { get; set; }
        public string apppath2 { get; set; }
        public string apppath3 { get; set; }
        public Byte[] crtttd { get; set; }
        public Byte[] appttd1 { get; set; }
        public Byte[] appttd2 { get; set; }
        public Byte[] appttd3 { get; set; }
        //Detail
        public int upmdtloid { get; set; }
        public string upmreftype { get; set; }
        public int upmrefoid { get; set; }
        public string upmrefno { get; set; }
        public string upmrefname { get; set; }
        public string upmrefdesc { get; set; }
        public decimal upmqty { get; set; }
        public string upmunit { get; set; }
        public int upmunitoid { get; set; }
        public DateTime upmdateneed { get; set; }
        public string upmreason { get; set; }
        public string upmdtlnote { get; set; }
    }

    public class ReportModels
    {
        public class DDLBusinessUnitModel
        {
            public string divcode { get; set; }
            public string divname { get; set; }
        }

        public class DDLDepartmentModel
        {
            public int deptoid { get; set; }
            public string deptname { get; set; }
        }

        public class DDLAccountModel
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
        }

        public class DDLSingleField
        {
            public string sfield { get; set; }
        }

        public class DDLDoubleField
        {
            public int ifield { get; set; }
            public string sfield { get; set; }
        }

        public class DDLReasonModel
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }

        public class DDLDivisionModel
        {
            public int groupoid { get; set; }
            public string groupcode { get; set; }
        }

        public class DDLWarehouseModel
        {
            public int genoid { get; set; }
            public string gendesc { get; set; }
        }

        public class DDLGroupModel
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
        }

        public class DDLUserModel
        {
            public string a { get; set; }

            public string b { get; set; }
        }

        public class DDLBAReturnUserModel
        {
            public string createuser { get; set; }

            public string createuser1 { get; set; }
        }

        public class DDLSupModel
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
        }

        public class FullFormType
        {
            public string formtype { get; set; }
            public string formtitle { get; set; }
            public string reftype { get; set; }
            public string mattype { get; set; }
            public string flagtype { get; set; }
            public string formref { get; set; }

            public FullFormType()
            {
                this.formtype = "";
                this.formtitle = "";
                this.reftype = "";
                this.mattype = "";
                this.flagtype = "";
                this.formref = "";
            }

            public FullFormType(string formparam)
            {
                if (formparam.Replace(" ", "").ToUpper() == "RAWMATERIAL")
                {
                    this.formtype = "RawMaterial";
                    this.formtitle = "Raw Material";
                    this.reftype = "raw";
                    this.mattype = "matraw";
                    this.flagtype = "A";
                    this.formref = "Raw";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "GENERALMATERIAL")
                {
                    this.formtype = "GeneralMaterial";
                    this.formtitle = "General Material";
                    this.reftype = "gen";
                    this.mattype = "matgen";
                    this.flagtype = "A";
                    this.formref = "General";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "SPAREPART")
                {
                    this.formtype = "SparePart";
                    this.formtitle = "Spare Part";
                    this.reftype = "sp";
                    this.mattype = "sparepart";
                    this.flagtype = "A";
                    this.formref = "Spare Part";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "FINISHGOOD")
                {
                    this.formtype = "FinishGood";
                    this.formtitle = "Finish Good";
                    this.reftype = "item";
                    this.mattype = "item";
                    this.flagtype = "A";
                    this.formref = "Finish Good";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "FIXEDASSETS")
                {
                    this.formtype = "FixedAssets";
                    this.formtitle = "Fixed Assets";
                    this.reftype = "asset";
                    this.mattype = "";
                    this.flagtype = "B";
                    this.formref = "Asset";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "LOG")
                {
                    this.formtype = "Log";
                    this.formtitle = "Log";
                    this.reftype = "wip";
                    this.mattype = "log";
                    this.flagtype = "C";
                    this.formref = "Log";
                }
                else if (formparam.Replace(" ", "").ToUpper() == "SAWNTIMBER")
                {
                    this.formtype = "SawnTimber";
                    this.formtitle = "Sawn Timber";
                    this.reftype = "sawn";
                    this.mattype = "pallet";
                    this.flagtype = "C";
                    this.formref = "Sawn";
                }
            }
        }
    }

    public class DataTableAjaxPostModel
    {
        // properties are not capital due to json mapping
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }
    }

    public class Column
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public Search search { get; set; }
    }

    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }

    public class Order
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
}