﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class APDController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public APDController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listapdirdtl
        {
            public int apdirdtlseq { get; set; }            
            public int rabdtl5oid { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }            
            public decimal priceOs { get; set; }           
            public decimal apdirprice { get; set; }
            public string apdirdtlnote { get; set; }                      
        }

        public class listapitemdtl2
        {
            public int apitemdtl2seq { get; set; }
            public int fabelioid { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public decimal fabeliaccumqty { get; set; }
            public decimal apitemdtl2amt { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }            
            public int reqrabmstoid { get; set; }
            public string reqrabno { get; set; }            
            public string rabmstnote { get; set; }
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public int curroid { get; set; }
            public int deptoid { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listacctg
        {            
            public int rabdtl5oid { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }           
            public decimal priceOs { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        public class mritem
        {
            public int mritemmstoid { get; set; }
            public string mritemno { get; set; }
            public DateTime mritemdate { get; set; }           
            public string mritemdatestr { get; set; }           
            public string mritemmstnote { get; set; }
            public string poitemno { get; set; }
        }

        public class listduedate
        {
            public DateTime duedate { get; set; }
            public string duedatestr { get; set; }
        }

        public class listfa
        {
            public int faoid { get; set; }
            public string fakturno { get; set; }
        }

        public class listacctg2
        {
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
            public int gnother1 { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<listapdirdtl> dtDtl, List<listapitemdtl2> dtDtl2, Guid? uid)
        {
            Session["ql_trnapdirdtl"+ uid] = dtDtl;
            Session["ql_trnapdirdtl2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listapdirdtl> dtDtl, Guid? uid)
        {
            Session["ql_trnapdirdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["ql_trnapdirdtl"+ uid] == null)
            {
                Session["ql_trnapdirdtl"+ uid] = new List<listapdirdtl>();
            }

            List<listapdirdtl> dataDtl = (List<listapdirdtl>)Session["ql_trnapdirdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listapitemdtl2> dtDtl, Guid? uid)
        {
            Session["ql_trnapdirdtl2"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["ql_trnapdirdtl2"+ uid] == null)
            {
                Session["ql_trnapdirdtl2"+ uid] = new List<listapitemdtl2>();
            }

            List<listapitemdtl2> dataDtl = (List<listapitemdtl2>)Session["ql_trnapdirdtl2"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "APD/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(apdirno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnapdirmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND apdirno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            List<listsupp> tbl = new List<listsupp>();

            sSql = "SELECT s.suppoid, suppcode, suppname, suppaddr, supppaymentoid, CAST(g.gnother1 AS int) gnother1 FROM QL_mstsupp s INNER JOIN QL_m05GN g ON g.gnoid=s.supppaymentoid WHERE s.cmpcode='" + CompnyCode + "' AND g.gngroup='PAYMENT TERM' AND s.activeflag = 'ACTIVE' AND s.supptype='Other' ORDER BY suppcode";
            tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();           

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListBiaya(int rabmstoid)
        {
            List<listacctg> tbl = new List<listacctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid("VAR_COST_AP_DIRECT", CompnyCode);
            var sDtl = " , 0 AS rabdtl5oid, 0.0 priceOs ";
            var sJoin = "";
            var sWhere = " AND acctgoid IN (" + acctgoid + ")";

            if (rabmstoid > 0)
            {
                sDtl = " ,rd5.rabdtl5oid, (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0)) priceOs";
                sJoin = " INNER JOIN QL_trnrabdtl5 rd5 ON rd5.acctgoid = a.acctgoid ";
                sWhere = " AND rd5.rabmstoid = " + rabmstoid + " AND (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed')),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0)) > 0 ";
            }            
            sSql = "SELECT a.acctgoid, acctgcode, acctgdesc "+ sDtl +" FROM QL_mstacctg a "+ sJoin +"  WHERE a.cmpcode='"+ CompnyCode +"' AND a.activeflag='ACTIVE' "+ sWhere + " ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<listacctg>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private void InitDDL(QL_trnapdirmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var apdirpaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.apdirpaytypeoid);
            ViewBag.apdirpaytypeoid = apdirpaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE activeflag='ACTIVE'";
            var acctgpphoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgoid", tbl.acctgpphoid);
            ViewBag.acctgpphoid = acctgpphoid;
        }

        private void FillAdditionalField(QL_trnapdirmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("select (SELECT reqrabno FROM QL_trnreqrabmst where reqrabmstoid = rb.reqrabmstoid) from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid +"").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("select projectname from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.poitemno = db.Database.SqlQuery<string>("select poitemno from QL_trnpoitemmst rb where poitemmstoid = " + tblmst.poitemmstoid + "").FirstOrDefault();
            ViewBag.suppsales = db.Database.SqlQuery<string>("SELECT suppdtl2picname FROM QL_mstsuppdtl2 where suppdtl2oid = '"+ tblmst.suppsalesoid +"'").FirstOrDefault();
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp a WHERE a.suppoid ='" + tblmst.suppoid + "'").FirstOrDefault();
            ViewBag.gnother1 = db.Database.SqlQuery<int>("SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid ='" + tblmst.apdirpaytypeoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar)
        {
            List<listacctg2> tbl = new List<listacctg2>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<listacctg2>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }       

        [HttpPost]
        public ActionResult GetFAData()
        {
            var result = "sukses";
            var msg = "";
            List<listfa> tbl = new List<listfa>();

            sSql = "select faoid, right(fakturno, 11) fakturno from QL_mstFA where status = 'ACTIVE'";
            tbl = db.Database.SqlQuery<listfa>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDueDate(int paytype, string tgldue, int paytypeoid)
        {
            var result = "sukses";
            var msg = "";
            sSql = "SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid =" + paytypeoid + "";
            paytype = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select dateadd(D, " + paytype + ",'"+ DateTime.Parse(ClassFunction.toDate(tgldue)) + "') duedate,CONVERT(char(20),dateadd(D, " + paytype +",'"+ DateTime.Parse(ClassFunction.toDate(tgldue)) + "'),103) duedatestr ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT DISTINCT rm.rabmstoid, rm.rabno, rm.rabdate, CONVERT(CHAR(10),rm.rabdate,103) rabdatestr, isnull(req.reqrabmstoid,0) reqrabmstoid, ISNULL(reqrabno,'') reqrabno, ISNULL(rm.projectname,'') projectname, ISNULL(rm.salesoid,'') salesoid, rm.rabmstnote, rm.alamatoid, rm.curroid, rm.deptoid FROM QL_trnrabmst rm INNER JOIN QL_trnrabdtl5 rd5 ON rd5.rabmstoid = rm.rabmstoid LEFT JOIN QL_trnreqrabmst req ON req.reqrabmstoid = rm.reqrabmstoid  WHERE rm.cmpcode = '" + CompnyCode + "' AND rabmststatus='Approved' AND rabdtl5status='' ORDER BY rabdate DESC";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailData(int mritemmstoid)
        {
            List<listapdirdtl> tbl = new List<listapdirdtl>();
            sSql = "SELECT sod.poitemdtlseq apdirdtlseq, sm.mritemno, rabdtl2oid, m.acctgoid, m.itemcode, m.itemdesc, sd.refno, (sd.mritemqty /*- ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.mritemdtloid=sd.mritemdtloid AND sretitemmststatus<>'Rejected'), 0.0)*/) AS apdirqty, sod.poitemunitoid apdirunitoid, gn.gndesc apdirunit, sod.poitemprice apdirprice,sod.poitemdtltaxvalue apdirdtltaxvalue, '' apdirdtlnote, sd.mritemmstoid, sd.mritemdtloid FROM QL_trnmritemdtl sd INNER JOIN QL_trnmritemmst sm ON sm.mritemmstoid = sd.mritemmstoid INNER JOIN ql_mstacctg m ON m.acctgoid = sd.acctgoid INNER JOIN QL_m05gn gn ON gnoid = mritemunitoid INNER JOIN QL_trnpoitemmst som ON som.cmpcode = sm.cmpcode AND som.poitemmstoid = sm.pomstoid INNER JOIN QL_trnpoitemdtl sod ON sod.cmpcode = sd.cmpcode AND sod.poitemmstoid = sm.pomstoid AND sod.poitemdtloid = sd.podtloid WHERE sd.cmpcode = '"+ CompnyCode +"' AND sm.mritemmstoid = " + mritemmstoid + " AND mritemdtlstatus = '' AND (mritemqty /*- ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode = sretd.cmpcode AND sretm.sretitemmstoid = sretd.sretitemmstoid WHERE sretd.cmpcode = sd.cmpcode AND sretd.mritemdtloid = sd.mritemdtloid AND sretitemmststatus <> 'Rejected'), 0.0)*/) > 0 ORDER BY mritemdtlseq";

                tbl = db.Database.SqlQuery<listapdirdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();
            //tblmst.apdirduedate = ClassFunction.GetServerTime().AddDays(7);
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetFakturData(int suppoid)
        {
            List<listapitemdtl2> tbl = new List<listapitemdtl2>();

            sSql = "SELECT 0 AS apitemdtl2seq, fa.fabelioid, fa.fabelicode, fa.fakturno, (fa.fabeliqty-fa.fabeliaccumqty) fabeliaccumqty, 0.0 AS apitemdtl2amt FROM QL_mstfabeli fa WHERE fa.activeflag='ACTIVE' AND fa.suppoid=" + suppoid + " AND (fabeliqty-fabeliaccumqty)>0 ORDER BY fa.fakturno";
            tbl = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listacctg> tbl = new List<listacctg>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, acctgoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM ql_mstacctg i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listacctg>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string apdirno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(apdirno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        // GET/POST: apdir
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No APD",["Supplier"] = "Supplier",["Project"] = "Project" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select pr.apdirmstoid, pr.apdirno, pr.apdirdate, c.suppname, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') projectname, ISNULL((SELECT rm.poitemno FROM QL_trnpoitemmst rm WHERE rm.poitemmstoid=pr.poitemmstoid),'') poitemno, pr.apdirgrandtotal grandtotal, pr.apdirmststatus, pr.apdirmstnote, CASE WHEN pr.apdirmststatus = 'Revised' THEN revisereason WHEN pr.apdirmststatus = 'Rejected' THEN rejectreason ELSE '' END reason FROM QL_trnapdirmst pr INNER JOIN QL_mstsupp c ON c.suppoid=pr.suppoid WHERE pr.cmpcode='" + CompnyCode + "' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.apdirdate >='" + param.filterperiodfrom + " 00:00:00' AND t.apdirdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.apdirmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.apdirno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.apdirmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.apdirmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.apdirmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.apdirmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapdirmst tblmst;
            string action = "Create";
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = new QL_trnapdirmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();                
                tblmst.apdirdate = ClassFunction.GetServerTime();
                tblmst.apdirduedate = ClassFunction.GetServerTime();
                tblmst.apdirsuppfjdate = ClassFunction.GetServerTime();
                tblmst.rabmstoid = 0;
                tblmst.pphamt = 0;
                tblmst.apdirtaxamt = 0;
                tblmst.apdirmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["ql_trnapdirdtl"+ ViewBag.uid] = null;
                Session["ql_trnapdirdtl2"+ ViewBag.uid] = null;
            }
            else
            {
                action = "Edit";
                ViewBag.uid = Guid.NewGuid();
                tblmst = db.QL_trnapdirmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT rd.rabdtl5oid, rd.apdirdtlseq, rd.acctgoid, i.acctgcode, i.acctgdesc, ISNULL(rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') AND arm.apdirmstoid <> " + tblmst.apdirmstoid + "),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0),0.0) priceOs, rd.apdirprice, rd.apdirdtlnote FROM ql_trnapdirdtl rd LEFT JOIN QL_trnrabdtl5 rd5 ON rd5.rabdtl5oid = rd.rabdtl5oid INNER JOIN ql_mstacctg i ON i.acctgoid=rd.acctgoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apdirmstoid=" + id + " ORDER BY rd.apdirdtlseq";
                Session["ql_trnapdirdtl"+ ViewBag.uid] = db.Database.SqlQuery<listapdirdtl>(sSql).ToList();

                sSql = "SELECT rd.apitemdtl2seq, rd.fabelioid, sm.fabelicode, sm.fakturno, (sm.fabeliqty-sm.fabeliaccumqty) fabeliaccumqty, rd.apitemdtl2amt FROM QL_trnapitemdtl2 rd INNER JOIN QL_mstfabeli sm ON sm.fabelioid = rd.fabelioid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.apitemmstoid=" + id + " AND rd.apitemdtl2type='QL_trnapdirmst' ORDER BY rd.apitemdtl2seq";
                Session["ql_trnapdirdtl2"+ ViewBag.uid] = db.Database.SqlQuery<listapitemdtl2>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnapdirmst tblmst, string action, string tglmst, string tgldue, string tglinv, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tblmst.apdirmststatus == "Post")
            {
                tblmst.apdirno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tblmst.apdirno = "";
            }            
            try
            {
                tblmst.apdirdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apdirdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            //try
            //{
            //    tblmst.apdirduedate = DateTime.Parse(ClassFunction.toDate(tgldue));
            //}
            //catch (Exception ex)
            //{
            //    ModelState.AddModelError("apdirduedate", "Format Tanggal Jatuh Tempo Tidak Valid!!" + ex.ToString());
            //}           
            try
            {
                tblmst.apdirsuppfjdate = DateTime.Parse(ClassFunction.toDate(tglinv));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("apdirsuppfjdate", "Format Tanggal Inv Supplier Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tblmst.apdirmstnote))
                tblmst.apdirmstnote = "";           
            if (string.IsNullOrEmpty(tblmst.apdirmstres1))
                tblmst.apdirmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.apdirmstres2))
                tblmst.apdirmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.apdirmstres3))
                tblmst.apdirmstres3 = "";
            if (tblmst.apdirmststatus.ToUpper() == "REVISED")
            {
                tblmst.apdirmststatus = "In Process";
            }

            DateTime duedate = db.Database.SqlQuery<DateTime>("Select DATEADD(DAY, (CASE g.gnother1 WHEN 0 THEN 0 ELSE CAST(g.gnother1 AS INT) END), '" + tblmst.apdirdate + "') AS [Due Date] FROM QL_m05gn g WHERE g.gnoid=" + tblmst.apdirpaytypeoid + " AND g.gngroup='PAYMENT TERM'").FirstOrDefault();
            tblmst.apdirduedate = duedate;

            //is Input Detail Valid
            List<listapdirdtl> dtDtl = (List<listapdirdtl>)Session["ql_trnapdirdtl"+ uid];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["ql_trnapdirdtl2"+ uid];

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].apdirdtlnote))
                            dtDtl[i].apdirdtlnote = "";
                        if (dtDtl[i].apdirprice == 0)
                        {
                            ModelState.AddModelError("", "Amount " + dtDtl[i].acctgdesc + " Belum Diisi !!");
                        }
                        if (tblmst.rabmstoid > 0)
                        {
                            if (dtDtl[i].apdirprice > dtDtl[i].priceOs)
                            {
                                ModelState.AddModelError("", "Amount " + dtDtl[i].acctgdesc + " Tidak Boleh Lebih Dari Price O/S !!");
                            }
                        }
                        if (tblmst.rabmstoid > 0) {
                            sSql = "SELECT (rd5.rabdtl5price - ISNULL((SELECT SUM(ard.apdirprice) total FROM QL_trnapdirmst arm INNER JOIN QL_trnapdirdtl ard ON ard.apdirmstoid=arm.apdirmstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.apdirmststatus IN('Post', 'Approved', 'Closed') AND arm.apdirmstoid <> " + tblmst.apdirmstoid + "),0.0) - CASE WHEN ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0)=0 THEN ISNULL((SELECT SUM(ard.kasbon2dtlamt) amt FROM QL_trnkasbon2mst arm INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE arm.rabmstoid = rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid AND arm.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND arm.kasbon2group='KASBON'),0.0) ELSE ISNULL((SELECT SUM(ardx.kasbon2dtlamt) FROM QL_trnkasbon2mst armx INNER JOIN QL_trnkasbon2dtl ardx ON ardx.kasbon2mstoid=armx.kasbon2mstoid INNER JOIN QL_trnkasbon2mst arm ON arm.kasbon2mstoid=armx.kasbon2refoid INNER JOIN QL_trnkasbon2dtl ard ON ard.kasbon2mstoid=arm.kasbon2mstoid WHERE armx.kasbon2mststatus IN('Post', 'Approved', 'Closed') AND armx.kasbon2group='REALISASI' AND armx.rabmstoid=rd5.rabmstoid AND ard.rabdtl5oid=rd5.rabdtl5oid),0.0) END - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='EXPENSE' AND cb.rabmstoid=rd5.rabmstoid AND gl.acctgoid=rd5.acctgoid),0.0) - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankmst cb INNER JOIN QL_trncashbankgl gl ON gl.cashbankoid=cb.cashbankoid WHERE cb.cashbankgroup='RABREAL' AND cb.rabmstoid=rd5.rabmstoid AND cb.rabdtl5oid=rd5.rabmstoid),0.0)) AS soitemqty FROM QL_trnrabdtl5 rd5 WHERE rd5.cmpcode='" + CompnyCode + "' AND rabdtl5oid= " + dtDtl[i].rabdtl5oid + "";
                            var PriceOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (PriceOs < dtDtl[i].apdirprice)
                                ModelState.AddModelError("", "Amount O/S " + dtDtl[i].acctgdesc + " Sudah di update, Silahkan refresh terlebih dahulu!");
                            if (dtDtl[i].apdirprice > PriceOs)
                                ModelState.AddModelError("", "Amount " + dtDtl[i].acctgdesc + " AP Direct tidak boleh lebih dari Amount O/S (" + PriceOs + ")");
                        }                        
                    }
                    if (tblmst.apdirmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tblmst.apdirno))
                            ModelState.AddModelError("apdirno", "Silahkan isi No. Faktur!");
                        else if (db.QL_trnapdirmst.Where(w => w.apdirno == tblmst.apdirno & w.apdirmstoid != tblmst.apdirmstoid).Count() > 0)
                            ModelState.AddModelError("apdirno", "No. Faktur yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");                        
                    }
                }
            }

            string apitemdtl2type = "QL_trnapdirmst";
            var suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp s WHERE s.cmpcode='" + CompnyCode + "' AND s.suppoid=" + tblmst.suppoid).FirstOrDefault();
            string fakturnoGL = ""; decimal totaltaxhdr = 0;
            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        totaltaxhdr += dtDtl2[i].apitemdtl2amt;
                        fakturnoGL += dtDtl2[i].fakturno + ",";
                    }
                    fakturnoGL = ClassFunction.Left(fakturnoGL, fakturnoGL.Length - 1);
                }
            }

            if (tblmst.apdirmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_AP_DIRECT", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AP_DIRECT"));
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_IN", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_IN"));
            }

            var servertime = ClassFunction.GetServerTime();
            DateTime sDate = tblmst.apdirdate.Value;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tblmst.apdirmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tblmst.apdirmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.apdirdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.apdirmststatus = "In Process";
            }
            if (tblmst.apdirmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.apdirmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var conapoid = ClassFunction.GenerateID("QL_CONAP");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var mstoid = ClassFunction.GenerateID("QL_trnapdirmst");
                var dtloid = ClassFunction.GenerateID("QL_trnapdirdtl");
                var dtl2oid = ClassFunction.GenerateID("QL_trnapitemdtl2");

                var iAcctgOidAP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AP_DIRECT", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_IN", CompnyCode));

                tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                tblmst.rateoid = 1;
                tblmst.rate2oid = rate2oid;
                tblmst.apdirmstdiscamt = 0;
                tblmst.apdirmstdisctype = "";
                tblmst.apdirmstdiscvalue = 0;
                tblmst.apdirothercost = 0;
                tblmst.apdirratetoidr = "1";
                tblmst.apdirratetousd = "0";
                tblmst.apdirrate2toidr = rate2toidr;
                tblmst.apdirrate2tousd = "0";
                tblmst.apdirtaxtype = "TAX";
                tblmst.apdirtaxvalue = 10;
                tblmst.apdirtotaldisc = 0;
                tblmst.apdirtotaldiscdtl = 0;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.apdirmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            
                            db.QL_trnapdirmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnapdirmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                          
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN(SELECT rabdtl5oid FROM QL_trnapdirdtl WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid="+ tblmst.apdirmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnapdirdtl.Where(a => a.apdirmstoid == tblmst.apdirmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnapdirdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tblmst.apdirmstoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();
                        }

                        QL_trnapdirdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnapdirdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.apdirdtloid = dtloid++;
                            tbldtl.rabdtl5oid = dtDtl[i].rabdtl5oid;
                            tbldtl.apdirmstoid = tblmst.apdirmstoid;
                            tbldtl.apdirdtlseq = i + 1;                            
                            tbldtl.acctgoid = dtDtl[i].acctgoid;                                                      
                            tbldtl.apdirprice = dtDtl[i].apdirprice;
                            tbldtl.apdirdtlstatus = "";
                            tbldtl.apdirdtlnote = dtDtl[i].apdirdtlnote;                            
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.apdirdtlres1 = "";
                            tbldtl.apdirdtlres2 = "";
                            tbldtl.apdirdtlres3 = "";

                            db.QL_trnapdirdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (dtDtl[i].apdirprice >= dtDtl[i].priceOs)
                            {
                                sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='Complete' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid=" + dtDtl[i].rabdtl5oid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='ql_trnapdirdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnapitemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnapitemdtl2();
                                    tbldtl2.cmpcode = tblmst.cmpcode;
                                    tbldtl2.apitemdtl2oid = dtl2oid++;
                                    tbldtl2.apitemmstoid = tblmst.apdirmstoid;
                                    tbldtl2.apitemdtl2seq = i + 1;
                                    tbldtl2.fabelioid = dtDtl2[i].fabelioid;
                                    tbldtl2.apitemdtl2amt = dtDtl2[i].apitemdtl2amt;
                                    tbldtl2.createuser = tblmst.upduser;
                                    tbldtl2.createtime = tblmst.updtime;
                                    tbldtl2.upduser = tblmst.upduser;
                                    tbldtl2.updtime = tblmst.updtime;
                                    tbldtl2.apitemdtl2type = apitemdtl2type;

                                    db.QL_trnapitemdtl2.Add(tbldtl2);
                                    db.SaveChanges();

                                    if (tblmst.apdirmststatus == "Post")
                                    {
                                        if (dtDtl2[i].fabeliaccumqty > 1)
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='ACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            //update fa beli
                                            sSql = "UPDATE QL_mstfabeli SET activeflag='INACTIVE', fabeliaccumqty=(fabeliaccumqty + 1) WHERE fabelioid=" + dtDtl2[i].fabelioid + "";
                                            db.Database.ExecuteSqlCommand(sSql);
                                            db.SaveChanges();
                                        }
                                    }

                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnapitemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tblmst.apdirmststatus == "In Approval")
                        {
                            tblapp.cmpcode = tblmst.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tblmst.apdirmstoid;
                            tblapp.requser = tblmst.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnapdirmst";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tblmst.apdirmststatus == "Post")
                        {
                            // Insert QL_conap                        
                            db.QL_conap.Add(ClassFunction.InsertConAP(CompnyCode, conapoid++, "QL_trnapdirmst", tblmst.apdirmstoid, 0, tblmst.suppoid, iAcctgOidAP, "Post", "APDIR", sDate, sPeriod, 0, new DateTime(1900, 01, 01), "", 0, duedate, tblmst.apdirgrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, tblmst.apdirgrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, sDate, sPeriod, tblmst.apdirno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            //decimal totaltax = Convert.ToDecimal(tblmst.apdirtaxamt);
                            //if (tblmst.apdirtaxamt > 0)
                            //{
                            //    // Insert QL_trngldtl
                            //    // PPN Masukan (D)
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tblmst.apdirno, "AP Direct| Note. PPN ", "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapitemmst " + tblmst.apdirmstoid, null, null, null, 0));
                            //    db.SaveChanges();
                            //}

                            // Insert QL_trngldtl
                            // Biaya Lain - Lain (D)
                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                var amt = dtDtl[i].apdirprice;
                                if (amt > 0)
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "D", amt, tblmst.apdirno, tblmst.apdirno + " | " + tblmst.apdirmstnote + " | " + dtDtl[i].apdirdtlnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tblmst.apdirmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                                else if (amt < 0)
                                {
                                    amt = dtDtl[i].apdirprice * (-1);
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].acctgoid, "C", amt, tblmst.apdirno, tblmst.apdirno + " | " + tblmst.apdirmstnote + " | " + dtDtl[i].apdirdtlnote + "", "Post", Session["UserID"].ToString(), servertime, amt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tblmst.apdirmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                            }

                            decimal totaltax = Convert.ToDecimal(totaltaxhdr);
                            if (totaltax > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "D", totaltax, tblmst.apdirno, tblmst.apdirno + " | " + suppname + " | " + tblmst.apdirmstnote, "Post", Session["UserID"].ToString(), servertime, totaltax * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tblmst.apdirmstoid, null, null, null, 0, fakturnoGL));
                                db.SaveChanges();
                            }

                            // Insert QL_trngldtl
                            // Hutang Dagang Lainnya (C)
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAP, "C", tblmst.apdirgrandtotal, tblmst.apdirno, tblmst.apdirno + " | " + tblmst.apdirmstnote, "Post", Session["UserID"].ToString(), servertime, tblmst.apdirgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tblmst.apdirmstoid, null, null, null, 0));
                            db.SaveChanges();

                            decimal totalpph = Convert.ToDecimal(tblmst.pphamt);
                            if (tblmst.pphamt > 0)
                            {
                                // Insert QL_trngldtl
                                // PPH (C)
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tblmst.acctgpphoid, "C", totalpph, tblmst.apdirno, tblmst.apdirno + " | " + tblmst.apdirmstnote, "Post", Session["UserID"].ToString(), servertime, totalpph * cRate.GetRateMonthlyIDRValue, 0, "QL_trnapdirmst " + tblmst.apdirmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (conapoid - 1) + " WHERE tablename='QL_conap'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.apdirmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.apdirmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnapdirmst tblmst = db.QL_trnapdirmst.Find(Session["CompnyCode"].ToString(), id);
            List<listapitemdtl2> dtDtl2 = (List<listapitemdtl2>)Session["ql_trnapdirdtl2"+ uid];
            var servertime = ClassFunction.GetServerTime();
            string apitemdtl2type = "QL_trnapdirmst";

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnrabdtl5 SET rabdtl5status ='' WHERE cmpcode='" + CompnyCode + "' AND rabdtl5oid IN (SELECT rabdtl5oid FROM QL_trnapdirdtl WHERE cmpcode='" + CompnyCode + "' AND apdirmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmritemmst SET mritemmststatus = 'Post' WHERE mritemmstoid IN (SELECT mritemmstoid FROM ql_trnapdirdtl where apdirmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnmritemdtl SET mritemdtlstatus = '' WHERE mritemdtloid IN (SELECT mritemdtloid FROM ql_trnapdirdtl where apdirmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusbeli='APD PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnapdirmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.apdirmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusbeli='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnapdirmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.apdirmstoid=" + id + ") AND (SELECT COUNT(*) FROM QL_trnmritemdtl md INNER JOIN QL_trnmritemmst mm ON mm.mritemmstoid = md.mritemmstoid INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid = mm.pomstoid WHERE mm.cmpcode='" + CompnyCode + "' AND mritemdtlstatus='' AND pom.rabmstoid IN (SELECT rabmstoid from QL_trnapdirmst where apdirmstoid <> " + id + "))=0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trnapitemdtl2.Where(a => a.apitemmstoid == tblmst.apdirmstoid && a.apitemdtl2type == apitemdtl2type && a.cmpcode == tblmst.cmpcode);
                        db.QL_trnapitemdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl = db.QL_trnapdirdtl.Where(a => a.apdirmstoid == id);
                        db.QL_trnapdirdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnapdirmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
		
		public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptAPDirTrn.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE apm.cmpcode='" + CompnyCode + "' AND apm.apdirmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "APDirectReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}