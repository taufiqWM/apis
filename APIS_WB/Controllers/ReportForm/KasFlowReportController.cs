﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class KasFlowReportController : Controller
    {
        // GET: ISReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public KasFlowReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }


        // GET: PReturnReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("ISReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLCurrency, string ReportType, string DDLType, string DDLMonth, string DDLYear)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "crKasFlow.rpt"; var rptname = "KASFLOW";

            decimal lr_tahun = 0; decimal penyusutan_at = 0; decimal amortisasi = 0;
            string acctgoidLR = ClassFunction.GetDataAcctgOid("VAR_CF_LRTAHUN", CompnyCode);
            string acctgoidPenyusutanAT = ClassFunction.GetDataAcctgOid("VAR_CF_PENYUSUTAN_AT", CompnyCode);
            string acctgoidAmor = ClassFunction.GetDataAcctgOid("VAR_CF_AMOR", CompnyCode);
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidLR + ")";
            lr_tahun = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPenyusutanAT + ")";
            penyusutan_at = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidAmor + ")";
            amortisasi = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;

            decimal piutang = 0; decimal piutang_gm = 0; decimal pajak_dimuka = 0;
            decimal piutang_lain = 0; decimal persediaan = 0; decimal uang_muka = 0;
            decimal biaya_dimuka = 0;
            string acctgoidPiutang = ClassFunction.GetDataAcctgOid("VAR_CF_PIUTANG", CompnyCode);
            string acctgoidPiutang_gm = ClassFunction.GetDataAcctgOid("VAR_CF_PIUTANG_GM", CompnyCode);
            string acctgoidPajakDimuka = ClassFunction.GetDataAcctgOid("VAR_CF_PAJAK_DIMUKA", CompnyCode);
            string acctgoidPiutangLain = ClassFunction.GetDataAcctgOid("VAR_CF_PIUTANG_LAIN", CompnyCode);
            string acctgoidPersediaan = ClassFunction.GetDataAcctgOid("VAR_CF_PERSEDIAAN", CompnyCode);
            string acctgoidUangMuka = ClassFunction.GetDataAcctgOid("VAR_CF_UM", CompnyCode);
            string acctgoidBiayaDimuka = ClassFunction.GetDataAcctgOid("VAR_CF_BIAYA_DIMUKA", CompnyCode);
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPiutang + ")";
            piutang = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPiutang_gm + ")";
            piutang_gm = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPajakDimuka + ")";
            pajak_dimuka = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPiutangLain + ")";
            piutang_lain = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPersediaan + ")";
            persediaan = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidUangMuka + ")";
            uang_muka = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidBiayaDimuka + ")";
            biaya_dimuka = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;

            decimal hutang = 0; decimal hutang_gk = 0; decimal hutang_pajak = 0;
            decimal pendapatan_dimuka = 0; decimal hutang_lain = 0;
            string acctgoidHutang = ClassFunction.GetDataAcctgOid("VAR_CF_HUTANG", CompnyCode);
            string acctgoidHutangGK = ClassFunction.GetDataAcctgOid("VAR_CF_HUTANG_GK", CompnyCode);
            string acctgoidHutangPajak = ClassFunction.GetDataAcctgOid("VAR_CF_HUTANG_PAJAK", CompnyCode);
            string acctgoidPendDimka = ClassFunction.GetDataAcctgOid("VAR_CF_PENDAPATAN DIMUKA", CompnyCode);
            string acctgoidHutangLain = ClassFunction.GetDataAcctgOid("VAR_CF_HUTANG_LAIN", CompnyCode);
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidHutang + ")";
            hutang = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidHutangGK + ")";
            hutang_gk = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidHutangPajak + ")";
            hutang_pajak = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPendDimka + ")";
            pendapatan_dimuka = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidHutangLain + ")";
            hutang_lain = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;

            decimal aktiva_tetap = 0; decimal aktiva_lain = 0;
            string acctgoidAT = ClassFunction.GetDataAcctgOid("VAR_CF_AT", CompnyCode);
            string acctgoidATL = ClassFunction.GetDataAcctgOid("VAR_CF_AT_LAIN", CompnyCode);
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidAT + ")";
            aktiva_tetap = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidATL + ")";
            aktiva_lain = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;

            decimal modal = 0; decimal hutang_panjang = 0; decimal laba_ditahan = 0; decimal laba_tahun = 0; decimal laba_bulan = 0; decimal prive = 0;
            string acctgoidModal = ClassFunction.GetDataAcctgOid("VAR_MODAL", CompnyCode);
            string acctgoidHP = ClassFunction.GetDataAcctgOid("VAR_HUTANG_JP", CompnyCode);
            string acctgoidLabaDitahan = ClassFunction.GetDataAcctgOid("VAR_LR_ONHAND", CompnyCode);
            string acctgoidLabaTahun = ClassFunction.GetDataAcctgOid("VAR_LR_YEAR", CompnyCode);
            string acctgoidLabaBulan = ClassFunction.GetDataAcctgOid("VAR_LR_MONTH", CompnyCode);
            string acctgoidPrive = ClassFunction.GetDataAcctgOid("VAR_PRIVE", CompnyCode);
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidModal + ")";
            modal = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidHP + ")";
            hutang_panjang = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidLabaDitahan + ")";
            laba_ditahan = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidLabaTahun + ")";
            laba_tahun = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidLabaBulan + ")";
            laba_bulan = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidPrive + ")";
            prive = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;

            decimal saldoAkhirKas = 0; decimal saldoAwalKas = 0; decimal saldoAkhirBank = 0; decimal mutasiKas = 0;
            string acctgoidKas = ClassFunction.GetDataAcctgOid("VAR_CASH_DASH", CompnyCode);
            string acctgoidBank = ClassFunction.GetDataAcctgOid("VAR_BANK_DASH", CompnyCode);
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidKas + ")";
            saldoAkhirKas = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtopen) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidKas + "," + acctgoidBank + ")";
            saldoAwalKas = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtbalance) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidBank + ")";
            saldoAkhirBank = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;
            sSql = "SELECT SUM(c.amtdebit-c.amtcredit) saldo FROM QL_mstacctg a INNER JOIN QL_crdgl c ON c.acctgoid=a.acctgoid WHERE c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "' AND  a.acctgoid IN (" + acctgoidKas + "," + acctgoidBank + ")";
            mutasiKas = db.Database.SqlQuery<decimal?>(sSql).FirstOrDefault() ?? 0;

            string bulandesc = "";
            if (int.Parse(DDLMonth) == 1)
            {
                bulandesc = "Januari";
            }
            else if (int.Parse(DDLMonth) == 2)
            {
                bulandesc = "Februari";
            }
            else if (int.Parse(DDLMonth) == 3)
            {
                bulandesc = "Maret";
            }
            else if (int.Parse(DDLMonth) == 4)
            {
                bulandesc = "April";
            }
            else if (int.Parse(DDLMonth) == 5)
            {
                bulandesc = "Mei";
            }
            else if (int.Parse(DDLMonth) == 6)
            {
                bulandesc = "Juni";
            }
            else if (int.Parse(DDLMonth) == 7)
            {
                bulandesc = "Juli";
            }
            else if (int.Parse(DDLMonth) == 8)
            {
                bulandesc = "Agustus";
            }
            else if (int.Parse(DDLMonth) == 9)
            {
                bulandesc = "September";
            }
            else if (int.Parse(DDLMonth) == 10)
            {
                bulandesc = "Oktober";
            }
            else if (int.Parse(DDLMonth) == 11)
            {
                bulandesc = "Nopember";
            }
            else if (int.Parse(DDLMonth) == 12)
            {
                bulandesc = "Desember";
            }
            bulandesc = bulandesc + " " + DDLYear;

            string acctgoid1 = ClassFunction.GetDataAcctgOid("VAR_CASH_DASH", CompnyCode);

            sSql = "SELECT * FROM( SELECT a.acctgoid, a.acctgcode, a.acctgdesc, ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='"+ ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo, 'ARUS KAS DARI KEGIATAN USAHA ' utama, '' subutama, 'a' gb, 1 seq FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoid1 + "))AS dt ORDER BY dt.seq";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", bulandesc);

            rptparam.Add("lr_tahun", (lr_tahun != 0 ? (lr_tahun * (-1)) : lr_tahun));
            rptparam.Add("penyusutan_at", (penyusutan_at != 0 ? (penyusutan_at * (-1)) : penyusutan_at));
            rptparam.Add("amortisasi", (amortisasi != 0 ? (amortisasi * (-1)) : amortisasi));

            rptparam.Add("piutang", piutang);
            rptparam.Add("piutang_gm", piutang_gm);
            rptparam.Add("pajak_dimuka", pajak_dimuka);
            rptparam.Add("piutang_lain", piutang_lain);
            rptparam.Add("persediaan", persediaan);
            rptparam.Add("uang_muka", uang_muka);
            rptparam.Add("biaya_dimuka", biaya_dimuka);

            rptparam.Add("hutang", hutang);
            rptparam.Add("hutang_gk", hutang_gk);
            rptparam.Add("hutang_pajak", hutang_pajak);
            rptparam.Add("pendapatan_dimuka", pendapatan_dimuka);
            rptparam.Add("hutang_lain", hutang_lain);

            rptparam.Add("aktiva_tetap", aktiva_tetap);
            rptparam.Add("aktiva_lain", aktiva_lain);

            rptparam.Add("modal", (modal != 0 ? (modal * (-1)) : modal));
            rptparam.Add("hutang_panjang", (hutang_panjang != 0 ? (hutang_panjang * (-1)) : hutang_panjang));
            rptparam.Add("laba_ditahan", (laba_ditahan != 0 ? (laba_ditahan * (-1)) : laba_ditahan));
            rptparam.Add("laba_tahun", (laba_tahun != 0 ? (laba_tahun * (-1)) : laba_tahun));
            rptparam.Add("laba_bulan", (laba_bulan != 0 ? (laba_bulan * (-1)) : laba_bulan));
            rptparam.Add("prive", (prive != 0 ? (prive * (-1)) : prive));

            rptparam.Add("saldoAkhirKas", saldoAkhirKas);
            rptparam.Add("saldoAwalKas", saldoAwalKas);
            rptparam.Add("saldoAkhirBank", saldoAkhirBank);
            rptparam.Add("mutasiKas", mutasiKas);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                ClassProcedure.SetDBLogonForReport(report);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}