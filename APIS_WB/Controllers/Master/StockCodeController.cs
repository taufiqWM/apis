﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class StockCodeController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public StockCodeController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET: StockCode
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT * FROM QL_m10SC WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY scoid DESC";
            List<ListStockCode> vbag = db.Database.SqlQuery<ListStockCode>(sSql).ToList();
            return View(vbag);
        }

        // GET: StockCode/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m10SC tbl;
            if (id == null)
            {
                tbl = new QL_m10SC();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                ViewBag.action = "Create";
            }
            else
            {
                tbl = db.QL_m10SC.Find(Session["CompnyCode"].ToString(), id);
                ViewBag.action = "Edit";
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            return View(tbl);
        }

        // POST: StockCode/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m10SC tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tbl.sccode))
                ModelState.AddModelError("sccode", "Silahkan isi Kode!");
            else if (db.QL_m10SC.Where(w => w.sccode == tbl.sccode & w.scoid != tbl.scoid).Count() > 0)
                ModelState.AddModelError("sccode", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (string.IsNullOrEmpty(tbl.scname))
                ModelState.AddModelError("scname", "Silahkan isi Nama!");
            else if (db.QL_m10SC.Where(w => w.scname == tbl.scname & w.scoid != tbl.scoid).Count() > 0)
                ModelState.AddModelError("scname", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            if (string.IsNullOrEmpty(tbl.scuoi))
                ModelState.AddModelError("scuoi", "Silahkan isi UOI!");
            if (string.IsNullOrEmpty(tbl.sctype))
                ModelState.AddModelError("sctype", "Silahkan isi Tipe!");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_m10SC");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.scoid = mstoid;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_m10SC.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_m10SC'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            return View(tbl);
        }

        // POST: StockCode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_m10SC tbl = db.QL_m10SC.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_m10SC.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class ListStockCode
        {
            public int scoid { get; set; }
            public string sccode { get; set; }
            public string scname { get; set; }
            public string scdesc1 { get; set; }
            public string scdesc2 { get; set; }
            public string scdesc3 { get; set; }
            public string scdesc4 { get; set; }
            public string scextdesc { get; set; }
            public string scflag { get; set; }
        }
    }
}
