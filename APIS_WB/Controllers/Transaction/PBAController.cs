﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace APIS_WB.Controllers.Transaction
{
    public class PBAController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public PBAController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trnmrassetmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0";
            var mrassetwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.mrassetwhoid = mrassetwhoid;
        }

        [HttpPost]
        public ActionResult InitDDLWH(string tipebarang, string action)
        {
            JsonResult js = null;
            try
            {
                sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0";
                if (tipebarang == "FPB") sSql += " AND ISNULL(gnother1,'')='FPB'";
                var tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class mrassetdtl : QL_trnmrassetdtl
        {
            public string poassetno { get; set; }
            public decimal poassetqty { get; set; }
            public decimal poassetprice { get; set; }
            public string mrassetrefcode { get; set; }
            public string mrassetreflongdesc { get; set; }
            public string mrassetunit { get; set; }
            public decimal mrassetamt { get; set; }
            public string pidtype { get; set; }
            public int assetacctgoid { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(int poassetmstoid, int suppoid, int mrassetmstoid)
        {
            List<mrassetdtl> tbl_ori = new List<mrassetdtl>();


            sSql = "SELECT 0 AS mrassetdtlseq, pod.poassetdtloid, poassetno, pod.poassetrefoid AS mrassetrefoid, pod.poassetreftype AS mrassetreftype, (SELECT itemcode FROM QL_mstitem where itemoid = pod.poassetrefoid) AS mrassetrefcode, (SELECT itemdesc FROM QL_mstitem where itemoid = pod.poassetrefoid) AS mrassetreflongdesc, (((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty)  poassetprice, (poassetqty - ISNULL((SELECT SUM(mrassetqty) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm2 ON mrm2.cmpcode = mrd.cmpcode AND mrm2.mrassetmstoid = mrd.mrassetmstoid WHERE mrd.cmpcode = pod.cmpcode AND mrd.poassetdtloid = pod.poassetdtloid AND mrm2.poassetmstoid = pom.poassetmstoid AND mrd.mrassetmstoid<>" + mrassetmstoid + "), 0)) AS poassetqty, 0.0 AS mrassetqty, poassetunitoid AS mrassetunitoid, gndesc AS mrassetunit, '' AS mrassetdtlnote, (((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) AS mrassetvalue, pom.poassetmstoid, '' refno, '' serialnumber, (SELECT pidtype FROM QL_mstitem where itemoid = pod.poassetrefoid) AS pidtype, (SELECT assetacctgoid FROM QL_mstitem where itemoid = pod.poassetrefoid) AS assetacctgoid FROM QL_trnpoassetdtl pod INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode = pod.cmpcode AND pod.poassetmstoid = pom.poassetmstoid INNER JOIN QL_m05GN g ON gnoid = poassetunitoid WHERE pod.cmpcode = '" + CompnyCode + "' AND pod.poassetmstoid = " + poassetmstoid + " AND poassetdtlstatus = '' AND ISNULL(poassetdtlres2, '')<> 'Complete' ORDER BY poassetdtlseq";

            tbl_ori = db.Database.SqlQuery<mrassetdtl>(sSql).ToList();

            var tbl = new List<mrassetdtl>();
            foreach (var item in tbl_ori)
            {
                if (item.pidtype == "PID")
                {
                    var maxloop = (int)item.poassetqty;
                    for (int i = 0; i < maxloop; i++)
                    {
                        item.mrassetqty = 1;
                        tbl.Add(item);
                    }
                }
                else
                {
                    item.mrassetqty = item.poassetqty;
                    tbl.Add(item);
                }
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id)
        {
            JsonResult js = null;
            try
            {
                var tbl = db.Database.SqlQuery<mrassetdtl>("SELECT mrassetdtlseq, mrd.poassetdtloid, poassetno, mrd.mrassetrefoid, mrd.mrassetreftype, (SELECT itemcode FROM QL_mstitem where itemoid = mrd.mrassetrefoid) AS mrassetrefcode, (SELECT itemdesc FROM QL_mstitem where itemoid = mrd.mrassetrefoid) AS mrassetreflongdesc, (poassetqty - ISNULL((SELECT SUM(x.mrassetqty) FROM QL_trnmrassetdtl x INNER JOIN QL_trnmrassetmst y ON y.cmpcode=x.cmpcode AND y.mrassetmstoid=x.mrassetmstoid WHERE x.cmpcode=mrd.cmpcode AND x.poassetdtloid=mrd.poassetdtloid AND pod.poassetmstoid=y.poassetmstoid AND x.mrassetmstoid<>mrd.mrassetmstoid), 0) ) AS poassetqty, (((pod.poassetqty * pod.poassetprice) - pod.poassetdtldiscamt) / pod.poassetqty) poassetprice, (mrassetqty + mrassetbonusqty) AS mrqty, mrassetqty, mrassetbonusqty, mrassetunitoid, gndesc AS mrassetunit, mrassetdtlnote, mrassetvalue, (mrassetvalue * mrassetqty) AS mrassetamt, ISNULL(mrd.refno,'') AS refno, ISNULL(mrd.serialnumber,'') AS serialnumber, (SELECT pidtype FROM QL_mstitem where itemoid = mrd.mrassetrefoid) AS pidtype, (SELECT assetacctgoid FROM QL_mstitem where itemoid = mrd.mrassetrefoid) AS assetacctgoid FROM QL_trnmrassetdtl mrd INNER JOIN QL_m05GN g ON gnoid = mrassetunitoid INNER JOIN QL_trnpoassetdtl pod ON pod.cmpcode = mrd.cmpcode AND pod.poassetdtloid = mrd.poassetdtloid AND poassetmstoid IN(SELECT poassetmstoid FROM QL_trnmrassetmst mrm WHERE mrd.cmpcode = mrm.cmpcode AND mrm.mrassetmstoid = mrd.mrassetmstoid) INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode = pod.cmpcode AND pom.poassetmstoid = pod.poassetmstoid WHERE mrassetmstoid = " + id + " AND mrd.cmpcode = '" + CompnyCode + "' ORDER BY mrassetdtlseq").ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        private void FillAdditionalField(QL_trnmrassetmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();
            ViewBag.poassetno = db.Database.SqlQuery<string>("SELECT poassetno FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + "").FirstOrDefault();
        }

        public class mstsupp : QL_mstsupp
        {
        }

        [HttpPost]
        public ActionResult GetSuppData(string sType)
        {
            List<mstsupp> tbl = new List<mstsupp>();
            sSql = "SELECT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND poassetmststatus='Approved' /*AND tipebarang='"+ sType +"'*/) ORDER BY suppcode, suppname";
            tbl = db.Database.SqlQuery<mstsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        public class poassetmst : QL_trnpoassetmst
        {
        }

        [HttpPost]
        public ActionResult GetPOData(int suppoid, string sType)
        {
            List<poassetmst> tbl = new List<poassetmst>();
            sSql = "SELECT poassettype, poassetmstoid, poassetno, poassetdate, poassetmstnote, curroid, poassettype FROM QL_trnpoassetmst WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + suppoid + " AND poassetmststatus='Approved' /*AND tipebarang='" + sType + "'*/ ORDER BY poassetmstoid";

            tbl = db.Database.SqlQuery<poassetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MR
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No PB",["Supplier"] = "Supplier",["NomorPO"] = "No PO" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( SELECT mrm.cmpcode, mrassetmstoid, mrassetno, mrassetdate, suppname, pom.poassetmstoid, poassetno, mrassetmststatus, mrassetmstnote FROM QL_trnmrassetmst mrm INNER JOIN QL_trnpoassetmst pom ON pom.cmpcode=mrm.cmpcode AND pom.poassetmstoid=mrm.poassetmstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.mrassetdate >='" + param.filterperiodfrom + " 00:00:00' AND t.mrassetdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.mrassetmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.mrassetno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Supplier") sSql += " AND t.suppname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "NomorPO") sSql += " AND t.poassetno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.mrassetmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.mrassetmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.mrassetmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.mrassetmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: MR/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrassetmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnmrassetmst();
                tbl.mrassetmstoid = ClassFunction.GenerateID("QL_trnmrassetmst");
                tbl.mrassetno = generateNo(ClassFunction.GetServerTime(), "Stock");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.mrassetmststatus = "In Process";

                ViewBag.mrassetdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnmrassetmst.Find(CompnyCode, id);

                ViewBag.mrassetdate = tbl.mrassetdate.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        private Boolean IsPOAssetWillBeClosed(int poassetmstoid)
        {
            List<mrassetdtl> dtDtl = (List<mrassetdtl>)Session["QL_trnmrassetdtl"];
            var sOid = "";
            int bRet = 0;
            for (int i = 0; i < dtDtl.Count(); i++)
            {
                if (dtDtl[i].mrassetqty >= dtDtl[i].poassetqty)
                {
                    sOid += dtDtl[i].poassetdtloid + ",";
                }
            }
            if (sOid != "")
            {
                sOid = ClassFunction.Left(sOid, sOid.Length - 1);
                sSql = "SELECT COUNT(*) FROM QL_trnpoassetdtl WHERE cmpcode='" + CompnyCode + "' AND poassetmstoid=" + poassetmstoid + " AND poassetdtlstatus='' AND poassetdtloid NOT IN (" + sOid + ")";
                bRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            if (bRet > 0)
                return true;
            else
                return false;
        }   

        private string generateNo(DateTime tanggal, string sType)
        {
            var prefix = "PBA";
            if (sType == "Konsinyasi")
            {
                prefix = "PBK";
            }
            string sNo = prefix + "/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(mrassetno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmrassetmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND mrassetno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private int GenPIDNew()
        {
            var str = ClassFunction.GetServerTime().ToString("yy") + "-"; int counter = 1;
            sSql = "SELECT COUNT(-1) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='" + ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'' ) LIKE'" + str + "%' AND cmpcode ='" + CompnyCode + "'";
            var cek = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (cek > 0)
            {
                sSql = "SELECT ISNULL((SELECT TOP 1 CAST(REPLACE(refno,'"+ str +"','') AS INTEGER) AS IDNEW FROM QL_conmat WHERE YEAR(updtime)='"+ ClassFunction.GetServerTime().ToString("yyyy") + "' AND ISNULL(refno,'')<>'' AND ISNULL(refno,'') LIKE'"+ str +"%' AND cmpcode ='"+ CompnyCode +"' ORDER BY CAST(REPLACE(refno,'"+ str +"','') AS INTEGER) DESC),0) + 1 AS IDNEW";
                counter = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            }
            return counter;
        }

        // POST: MR/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmrassetmst tbl, List<mrassetdtl> dtDtl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed"; var isdoubleinput = false; var isdoublepost = false;
            var servertime = ClassFunction.GetServerTime();

            if (tbl.cmpcode == null)
                tbl.cmpcode = CompnyCode;

            if (string.IsNullOrEmpty(tbl.mrassetno))
            {
                tbl.mrassetno = generateNo(ClassFunction.GetServerTime(), tbl.tipebarang);
            }

            sSql = "SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.suppoid + "";
            string suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.mrassetmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            if (dtDtl == null)
                msg += "- Please fill detail data!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br>";
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].mrassetqty <= 0)
                        {
                            msg += "- MR QTY for Material " + dtDtl[i].mrassetreflongdesc + " must be more than 0!<br>";
                        }

                        decimal dQty = 0;
                        if (dtDtl[i].pidtype == "PID")
                        {
                            var a = dtDtl.Where(x => x.mrassetrefoid == dtDtl[i].mrassetrefoid && x.poassetdtloid == dtDtl[i].poassetdtloid).GroupBy(x => x.mrassetrefoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.mrassetqty) }).ToList();
                            for (int j = 0; j < a.Count(); j++)
                            {
                                dQty = a[j].Qty;
                            }
                        }
                        else
                        {
                            dQty = dtDtl[i].mrassetqty;
                        }

                        sSql = "SELECT (poassetqty - ISNULL((SELECT SUM(mrassetqty) FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm2 ON mrm2.cmpcode=mrd.cmpcode AND mrm2.mrassetmstoid=mrd.mrassetmstoid WHERE mrd.cmpcode=regd.cmpcode AND mrd.poassetdtloid=regd.poassetdtloid AND mrm2.poassetmstoid=regd.poassetmstoid  AND mrd.mrassetmstoid<>"+ tbl.mrassetmstoid +"), 0) ) AS poassetqty FROM QL_trnpoassetdtl regd WHERE regd.cmpcode='" + CompnyCode + "' AND regd.poassetdtloid=" + dtDtl[i].poassetdtloid + " AND regd.poassetmstoid=" + tbl.poassetmstoid;
                        var dQtyOS = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQtyOS != dtDtl[i].poassetqty)
                            dtDtl[i].poassetqty = dQtyOS;
                        if (dQtyOS < dQty)
                            msg += "- Some PO Qty has been updated by another user. Please check that every Qty must be less than PO Qty!<br>";
                        if (tbl.mrassetmststatus == "Post")
                        {
                            //set coa asset
                            if (tbl.tipebarang == "Asset")
                                if (dtDtl[i].assetacctgoid == 0)
                                    msg += "- Silahkan Set COA Asset di Master Item!<br>";

                            decimal sValue = dtDtl[i].poassetprice * cRate.GetRateMonthlyIDRValue;
                            decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].mrassetrefoid, dQty, sValue, "IN");
                            if (dtDtl[i].pidtype == "PID")
                            {
                                dtDtl[i].mrassetvalue = Math.Round((sAvgValue > 0 ? (sAvgValue / dQty) : sAvgValue), 4);
                                dtDtl[i].mrassetvalueidr = Math.Round((sAvgValue > 0 ? (sAvgValue / dQty) : sAvgValue), 4);
                            }
                            else
                            {
                                dtDtl[i].mrassetvalue = Math.Round(sAvgValue, 4);
                                dtDtl[i].mrassetvalueidr = Math.Round(sAvgValue, 4);
                            }
                            dtDtl[i].mrassetamt = dtDtl[i].poassetprice * dtDtl[i].mrassetqty;
                        }
                        else
                        {
                            dtDtl[i].mrassetvalue = 0;
                            dtDtl[i].mrassetvalueidr = 0;
                            dtDtl[i].mrassetamt = 0;
                        }
                    }
                }
            }

            if (tbl.mrassetmststatus.ToUpper() == "POST")
            {
                var sVarErr = "";
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", tbl.cmpcode))
                {
                    sVarErr = "VAR_STOCK";
                    msg += ClassFunction.GetInterfaceWarning(sVarErr);
                }
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", tbl.cmpcode))
                {
                    sVarErr = "VAR_PURC_RECEIVED";
                    msg += ClassFunction.GetInterfaceWarning(sVarErr);
                }
                var podate = db.Database.SqlQuery<DateTime>("SELECT poassetdate FROM QL_trnpoassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid).FirstOrDefault();

                cRate.SetRateValue(tbl.curroid, podate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                    msg += cRate.GetRateDailyLastError;
                if (cRate.GetRateMonthlyLastError != "")
                    msg += cRate.GetRateMonthlyLastError;
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.mrassetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                tbl.mrassetmststatus = "In Process";
            }
            if (tbl.mrassetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                    tbl.mrassetmststatus = "In Process";
                }
            }

            if (msg == "")
            {
                int PIDNew = 0;
                if (tbl.mrassetmststatus.ToUpper() == "POST")
                {
                    PIDNew = GenPIDNew();
                }

                var mstoid = ClassFunction.GenerateID("QL_trnmrassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnmrassetdtl");

                decimal dSumMR = 0; decimal dSumMR_IDR = 0;
                var iConMatOid = ClassFunction.GenerateID("QL_CONMAT");
                var iGLMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                var iGLDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                var iStockAcctgOid = 0;
                var iRecAcctgOid = 0;
                var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                var sDateGRIR = "";

                if (tbl.mrassetmststatus.ToUpper() == "POST")
                {
                    iStockAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tbl.cmpcode));
                    iRecAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", tbl.cmpcode));
                    sDateGRIR = ClassFunction.GetServerTime().ToString("MM/dd/yyyy  hh:mm:ss");
                    tbl.grirposttime = ClassFunction.GetServerTime();

                    tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                }
                var tipeStock = new List<string> { "Stock", "FPB" };

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.Database.ExecuteSqlCommand($"UPDATE QL_trnmrassetmst SET createtime='{tbl.createtime}' WHERE createtime='{tbl.createtime}'") > 0) isdoubleinput = true;
                            else
                            {
                                if (db.QL_trnmrassetmst.Find(tbl.cmpcode, tbl.mrassetmstoid) != null)
                                    tbl.mrassetmstoid = mstoid;

                                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.mrassetdate);
                                tbl.updtime = tbl.createtime;
                                tbl.upduser = tbl.createuser;
                                tbl.grirposttime = new DateTime(1900, 01, 01);
                                db.QL_trnmrassetmst.Add(tbl);
                                db.SaveChanges();

                                sSql = "UPDATE QL_id SET lastoid=" + tbl.mrassetmstoid + " WHERE tablename='QL_trnmrassetmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            if (db.QL_trnmrassetmst.AsNoTracking().FirstOrDefault(t => t.cmpcode == tbl.cmpcode && t.mrassetmstoid == tbl.mrassetmstoid).mrassetmststatus.ToUpper() == "IN PROCESS")
                            {
                                tbl.updtime = servertime;
                                tbl.upduser = Session["UserID"].ToString();
                                db.Entry(tbl).State = EntityState.Modified;
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND poassetdtloid IN (SELECT poassetdtloid FROM QL_trnmrassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + tbl.mrassetmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                var trndtl = db.QL_trnmrassetdtl.Where(a => a.mrassetmstoid == tbl.mrassetmstoid && a.cmpcode == tbl.cmpcode);
                                db.QL_trnmrassetdtl.RemoveRange(trndtl);
                                db.SaveChanges();
                            }
                            else if (tbl.mrassetmststatus.ToUpper() == "POST") isdoublepost = true;
                        }

                        if (!isdoubleinput && !isdoublepost)
                        {
                            var tbldtl = new List<QL_trnmrassetdtl>();
                            foreach (var item in dtDtl)
                            {
                                sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + item.mrassetrefoid + "";
                                var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                                var new_dtl = (QL_trnmrassetdtl)ClassFunction.MappingTable(new QL_trnmrassetdtl(), item);
                                if (string.IsNullOrEmpty(item.mrassetdtlstatus)) new_dtl.mrassetdtlstatus = "";
                                if (string.IsNullOrEmpty(item.mrassetdtlnote)) new_dtl.mrassetdtlnote = "";
                                new_dtl.cmpcode = tbl.cmpcode;
                                new_dtl.mrassetdtloid = dtloid++;
                                new_dtl.mrassetmstoid = tbl.mrassetmstoid;
                                new_dtl.mrassetbonusqty = 0;
                                new_dtl.mrassetvalueusd = 1;
                                new_dtl.refno = (item.pidtype == "PID" ? ClassFunction.GetServerTime().ToString("yy") + "-" + PIDNew.ToString() : "");
                                new_dtl.serialnumber = item.serialnumber ?? "";
                                new_dtl.upduser = tbl.upduser;
                                new_dtl.updtime = tbl.updtime;
                                tbldtl.Add(new_dtl);

                                if (tbl.mrassetmststatus == "Post")
                                {
                                    PIDNew = (item.pidtype == "PID" ? (PIDNew + 1) : PIDNew);
                                }

                                if (tbl.mrassetmststatus == "Post")
                                {
                                    if (type != "Ongkir")
                                    {
                                        if (tipeStock.Contains(tbl.tipebarang))
                                        {
                                            // Insert QL_conmat
                                            db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, iConMatOid++, "MRA", "QL_trnmrassetdtl", tbl.mrassetmstoid, item.mrassetrefoid, "FIXED ASSET", tbl.mrassetwhoid, item.mrassetqty, "Penerimaan Barang " + tbl.tipebarang + "", tbl.mrassetno + " | " + suppname, Session["UserID"].ToString(), new_dtl.refno, item.mrassetvalue, 0, 0, null, new_dtl.mrassetdtloid, 0, item.mrassetamt, new_dtl.serialnumber));
                                        }
                                        dSumMR += item.mrassetamt;
                                        dSumMR_IDR += item.mrassetamt;
                                    }
                                }

                                decimal dQty = 0;
                                if (item.pidtype == "PID")
                                {
                                    var a = dtDtl.Where(x => x.mrassetrefoid == item.mrassetrefoid && x.poassetdtloid == item.poassetdtloid).GroupBy(x => x.mrassetrefoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.mrassetqty) }).ToList();
                                    for (int j = 0; j < a.Count(); j++)
                                    {
                                        dQty = a[j].Qty;
                                    }
                                }
                                else
                                {
                                    dQty = item.mrassetqty;
                                }

                                if (dQty >= item.poassetqty)
                                {
                                    sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetdtloid=" + item.poassetdtloid + " AND poassetmstoid=" + tbl.poassetmstoid;
                                    db.Database.ExecuteSqlCommand(sSql);

                                    sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND (SELECT COUNT(*) FROM QL_trnpoassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND poassetdtloid<>" + item.poassetdtloid + " AND poassetdtlstatus='')=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                }
                            }
                            db.QL_trnmrassetdtl.AddRange(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnmrassetdtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            if (tbl.mrassetmststatus == "Post")
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, iGLMstOid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.mrassetno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, cRate.GetRateDailyOid, cRate.GetRateMonthlyOid, cRate.GetRateDailyIDRValue, cRate.GetRateMonthlyIDRValue, cRate.GetRateDailyUSDValue, cRate.GetRateMonthlyUSDValue));
                                db.SaveChanges();

                                if (tbl.tipebarang == "Asset")
                                {
                                    var listGLAsset = dtDtl.GroupBy(x => x.assetacctgoid).Select(x => new { acctgoid = x.Key, amtIDR = x.Sum(y => y.mrassetamt) }).ToList();
                                    foreach (var item in listGLAsset)
                                    {
                                        // Insert QL_trngldtl
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 1, iGLMstOid, item.acctgoid, "D", item.amtIDR, tbl.mrassetno, tbl.mrassetno + " | " + suppname + " | " + tbl.mrassetmstnote, "Post", Session["UserID"].ToString(), servertime, item.amtIDR, 0, "QL_trnmrassetmst " + tbl.mrassetmstoid.ToString(), null, null, null, 0));
                                    }
                                    db.SaveChanges();
                                }
                                else
                                {
                                    // Insert QL_trngldtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 1, iGLMstOid, iStockAcctgOid, "D", dSumMR, tbl.mrassetno, tbl.mrassetno + " | " + suppname + " | " + tbl.mrassetmstnote, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, 0, "QL_trnmrassetmst " + tbl.mrassetmstoid.ToString(), null, null, null, 0));
                                    db.SaveChanges();
                                }

                                // Insert QL_trngldtl
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, iGLDtlOid++, 2, iGLMstOid, iRecAcctgOid, "C", dSumMR, tbl.mrassetno, tbl.mrassetno + " | " + suppname + " | " + tbl.mrassetmstnote, "Post", Session["UserID"].ToString(), servertime, dSumMR_IDR, 0, "QL_trnmrassetmst " + tbl.mrassetmstoid.ToString(), null, null, null, 0));
                                db.SaveChanges();

                                sSql = " UPDATE QL_id SET lastoid=" + (iConMatOid - 1) + " WHERE tablename='QL_CONMAT'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + iGLMstOid + " WHERE tablename='QL_trnglmst'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_ID SET lastoid=" + (iGLDtlOid - 1) + " WHERE tablename='QL_trngldtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: MR/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmrassetmst tbl = db.QL_trnmrassetmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        sSql = "UPDATE QL_trnpoassetdtl SET poassetdtlstatus='' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid + " AND poassetdtloid IN (SELECT poassetdtloid FROM QL_trnmrassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND mrassetmstoid=" + tbl.mrassetmstoid + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnpoassetmst SET poassetmststatus='Approved' WHERE cmpcode='" + tbl.cmpcode + "' AND poassetmstoid=" + tbl.poassetmstoid;
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnmrassetdtl.Where(a => a.mrassetmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnmrassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmrassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT mrassetno FROM QL_trnmrassetmst WHERE mrassetmstoid=" + id + "";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPBAssetItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mrassetmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptPBAssetItem_" + sNo + ".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}