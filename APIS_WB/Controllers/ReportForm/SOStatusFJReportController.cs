﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class SOStatusFJReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public SOStatusFJReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT som.custoid FROM QL_trnsoitemmst som WHERE som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetSOData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomorRAB)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, som.soitemmstoid [No. Draft],som.soitemno [No. Sales Order], CONVERT(varchar(10),som.soitemdate,103) [Tgl. SO],c.custname [Customer], som.soitemmststatus [Status], som.soitemmstnote [Note] FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=som.custoid WHERE som.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY som.soitemdate DESC, som.soitemmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string TextCust, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, rm.rabmstoid [No. Draft], rm.rabno [No. RAB], CONVERT(VARCHAR(10), rm.rabdate, 101) AS [Tgl. RAB], rm.projectname [Nama Project], CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], rm.rabmststatus [Status], rm.rabmstnote [Note] /*, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereq*/ FROM QL_trnrabmst rm WHERE rm.rabmstoid IN (SELECT som.rabmstoid from QL_trnsoitemmst som INNER JOIN QL_mstcust c ON som.custoid=c.custoid AND c.activeflag='ACTIVE' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " )";
                sSql += " ORDER BY rm.rabdate DESC, rm.rabmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomorPO, string TextNomorSO, string TextNomorRAB, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=sod.soitemunitoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=sod.soitemmstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=som.rabmstoid INNER JOIN QL_mstcust c ON C.custoid = som.custoid WHERE som.cmpcode='" + CompnyCode + "' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND som.soitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorSO != "")
                {
                    string[] arr = TextNomorSO.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomorPO + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorSO, string DDLSoNo, string TextNomorRAB)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            var sPeriode = "";
            var sDDLSono = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusFJSumXls.rpt";
                else
                    rptfile = "rptSOStatusFJSumPdf.rpt";
                rptname = "SOStatusFJ_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptSOStatusFJDtlXls.rpt";
                else
                {
                    rptfile = "rptSOStatusFJDtlPdf.rpt";
                }
                rptname = "SOStatusFJ_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Detail")
            {
                //Detail Shipment
                Dtl = " , ISNULL([Shipment No.],'') [Shipment No.],  ISNULL([Shipment Qty],0.0) [Shipment Qty],ISNULL([SRETQty],0.0) [SRETQty], ISNULL([SJ Ref No.],'') AS [SJ Ref No.] ";
                Join = " LEFT JOIN ";
                Join += " ( SELECT shd.cmpcode, shm.soitemmstoid, shd.soitemdtloid, shd.shipmentitemdtloid, shd.itemoid,ISNULL (shm.shipmentitemno,'') AS [Shipment No.],  (ISNULL(shd.shipmentitemqty,0.0)) AS [Shipment Qty], (SELECT SUM(sretd.sretitemqty) FROM QL_trnsretitemdtl sretd WHERE sretd.cmpcode=sretd.cmpcode AND sretd.shipmentitemdtloid=shd.shipmentitemdtloid AND sretd.sretitemmstoid IN (SELECT sretitemmstoid FROM QL_trnsretitemmst srt WHERE srt.cmpcode=shd.cmpcode AND srt.sretitemmststatus IN('Post','Approved','Closed'))) AS [SretQty], '' AS [SJ Ref No.] from QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=shd.cmpcode AND shd.shipmentitemmstoid=shm.shipmentitemmstoid AND shm.shipmentitemmststatus IN('Post','Approved','Closed')) AS tblShipment ON tblShipment.cmpcode=tbldtl.cmpcode AND tblShipment.soitemdtloid=tbldtl.sodtloid AND tblShipment.itemoid=tbldtl.itemoid ";
                //Detail FJ
                Dtl += " , ISNULL([FJ No.],'') [FJ No.], ISNULL([FJ Qty],0.0) [FJ Qty], ISNULL([FJ Return Qty],0.0) [FJ Return Qty] ";
                //Join += " LEFT JOIN ";
                //Join += "  ( SELECT shd.shipmentitemdtloid, shd.shipmentitemmstoid FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid = shd.shipmentitemmstoid AND shd.shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnshipmentitemmst shm WHERE shm.shipmentitemmstoid = shd.shipmentitemmstoid AND shm.shipmentitemmststatus IN('Post', 'Closed'))) AS tblSJ ON tblShipment.cmpcode = tbldtl.cmpcode AND tblShipment.soitemmstoid = tbldtl.soitemmstoid AND tblShipment.soitemdtloid = tbldtl.soitemdtloid AND tblShipment.itemoid = tbldtl.itemoid ";
                Join += " LEFT JOIN ";
                Join += " ( SELECT arm.somstoid, shd.soitemdtloid, ard.shipmentitemmstoid, ard.shipmentitemdtloid, arm.aritemno [FJ No.],ard.aritemmstoid, ard.aritemdtloid, ard.itemoid, m.itemcode AS matcode, m.itemdesc AS matlongdsc, 0.0 AS POClosingQty, ISNULL(ard.aritemqty,0) AS [FJ Qty], ISNULL((SELECT SUM(rtd.arretitemqty) FROM QL_trnarretitemmst rtm INNER JOIN QL_trnarretitemdtl rtd ON rtd.arretitemmstoid=rtm.arretitemmstoid WHERE rtm.arretitemmststatus IN('Post','Approved','Closed') AND rtd.aritemdtloid=ard.aritemdtloid AND rtm.aritemmstoid=ard.aritemmstoid),0.0) [FJ Return Qty] FROM QL_trnaritemdtl ard INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid = ard.aritemmstoid INNER JOIN QL_mstitem m ON m.itemoid = ard.itemoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.shipmentitemdtloid=ard.shipmentitemdtloid AND shd.shipmentitemmstoid=ard.shipmentitemmstoid AND shd.itemoid=ard.itemoid INNER JOIN QL_m05gn g ON g.gnoid=ard.aritemunitoid WHERE ard.aritemmstoid IN (SELECT aritemmstoid FROM QL_trnaritemmst arm WHERE arm.aritemmstoid = ard.aritemmstoid AND arm.aritemmststatus IN('Post','Approved','Closed'))) AS ardtl ON ardtl.somstoid = tblShipment.soitemmstoid AND tblShipment.soitemdtloid=ardtl.soitemdtloid AND ardtl.shipmentitemdtloid = tblShipment.shipmentitemdtloid AND ardtl.itemoid = tblShipment.itemoid ";
            }
            else // Summary
            {
                //Detail Shipment
                Dtl = " ,  ISNULL([Shipment Qty],0.0) [Shipment Qty],ISNULL([SRETQty],0.0) [SRETQty], ISNULL([FJ Qty],0.0) [FJ Qty], ISNULL([FJ Return Qty],0.0) [FJ Return Qty]";
                //Detail Shipment, SJ Return, FJ, FJ Return
                Join = " LEFT JOIN";
                Join += " ( SELECT shd.cmpcode, shm.soitemmstoid, shd.soitemdtloid, shd.itemoid, SUM (ISNULL(shd.shipmentitemqty,0.0)) [Shipment Qty], SUM (ISNULL(SRETQty,0.0)) [SRETQty], SUM (ISNULL([FJ Qty],0.0)) [FJ Qty], SUM (ISNULL([FJ Return Qty],0.0)) [FJ Return Qty] FROM QL_trnshipmentitemdtl shd INNER JOIN QL_trnshipmentitemmst shm ON shm.shipmentitemmstoid = shd.shipmentitemmstoid AND shd.shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnshipmentitemmst shm WHERE shm.shipmentitemmstoid = shd.shipmentitemmstoid AND shm.shipmentitemmststatus IN('Post','Approved', 'Closed')) LEFT JOIN( SELECT sretd.cmpcode, sretd.shipmentitemdtloid, SUM(Sretitemqty) SRETQty FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretd.cmpcode = sretm.cmpcode AND sretd.sretitemmstoid = sretm.sretitemmstoid WHERE sretm.sretitemmststatus IN('Post','Approved','Closed') GROUP BY sretd.cmpcode, sretd.shipmentitemdtloid) tblSret ON tblSret.cmpcode = shd.cmpcode AND tblSret.shipmentitemdtloid = shd.shipmentitemdtloid LEFT JOIN( SELECT ard.cmpcode, ard.shipmentitemdtloid, SUM(aritemqty) [FJ Qty] FROM QL_trnaritemdtl ard INNER JOIN QL_trnaritemmst arm ON ard.cmpcode = arm.cmpcode AND ard.aritemmstoid = arm.aritemmstoid WHERE arm.aritemmststatus IN('Post','Approved','Closed') GROUP BY ard.cmpcode, ard.shipmentitemdtloid) tblFJ ON tblFJ.cmpcode = shd.cmpcode AND tblFJ.shipmentitemdtloid = shd.shipmentitemdtloid LEFT JOIN( SELECT ard.cmpcode, ard.shipmentitemdtloid, SUM(arretitemqty) [FJ Return Qty] FROM QL_trnaritemdtl ard INNER JOIN QL_trnaritemmst arm ON ard.cmpcode = arm.cmpcode AND ard.aritemmstoid = arm.aritemmstoid INNER JOIN QL_trnarretitemdtl rtd ON rtd.aritemdtloid=ard.aritemdtloid AND rtd.itemoid=ard.itemoid INNER JOIN QL_trnarretitemmst rtm ON rtm.arretitemmstoid=rtd.arretitemmstoid  WHERE rtm.arretitemmststatus IN('Post','Approved','Closed') GROUP BY ard.cmpcode, ard.shipmentitemdtloid) tblFJRet ON tblFJ.cmpcode = shd.cmpcode AND tblFJRet.shipmentitemdtloid = shd.shipmentitemdtloid GROUP BY shd.cmpcode, shm.soitemmstoid, shd.soitemdtloid, shd.itemoid) AS tblShipment ON tblShipment.cmpcode = tbldtl.cmpcode AND tblShipment.soitemmstoid = tbldtl.soitemmstoid AND tblShipment.soitemdtloid = tbldtl.soitemdtloid AND tblShipment.itemoid = tbldtl.itemoid";
                ////Detail FJ
                //Dtl += ", ISNULL([FJ Qty],0.0) [FJ Qty]";                
                //Join += " LEFT JOIN ";
                //Join += " ( SELECT arm.somstoid, ard.shipmentitemdtloid, ard.itemoid, SUM(ISNULL(ard.aritemqty,0)) AS [FJ Qty] FROM QL_trnaritemdtl ard INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid = ard.aritemmstoid INNER JOIN QL_mstitem m ON m.itemoid = ard.itemoid INNER JOIN QL_m05gn g ON g.gnoid=ard.aritemunitoid AND ard.aritemmstoid IN (SELECT aritemmstoid FROM QL_trnaritemmst arm WHERE arm.aritemmstoid = ard.aritemmstoid AND arm.aritemmststatus IN('Post', 'Closed')) Group BY arm.somstoid, ard.shipmentitemdtloid, ard.itemoid) AS ardtl ON  ardtl.somstoid = tblShipment.soitemmstoid AND ardtl.itemoid = tblShipment.itemoid ";
            }
            sSql = "SELECT tblmst.cmpcode, tblmst.sssflag ,tblmst.groupoid, [Division], divname, tblmst.somstoid, draftno, sono, rabno, sodate,transdate, somststatus, [Customer PO], custoid, custcode, custname, somstnote, approvaluser,  approvaldatetime, createuser, createtime AS createdate  , [Close Reason], [Close Date], [Close User], tbldtl.matcode, matlongdesc, sounit, ISNULL(SOClosingQty,0.0) SOClosingQty, ISNULL(SOQtyAwal,0.0) SOQtyAwal, sodtloid, sodtlnote, 0.0 AS BalanceShipmentQty, soitemcustref, Marketing " + Dtl + " ";
            sSql += " FROM";
            sSql += " ( SELECT som.cmpcode, '' sssflag , 0 groupoid, '' [Division], '' AS divname, som.soitemmstoid AS somstoid, (CONVERT(VARCHAR(10), som.soitemmstoid)) AS draftno, som.soitemno AS sono, rm.rabno, som.soitemdate AS sodate, som.updtime AS transdate, CASE WHEN ISNULL(som.soitemmstres1,'')='' THEN som.soitemmststatus ELSE ISNULL(som.soitemmstres1,'') END AS somststatus, som.soitemcustpo [Customer PO], som.custoid, c.custcode, c.custname, som.soitemmstnote AS somstnote, '' approvaluser, getdate() approvaldatetime, som.createuser, som.createtime, som.closereason [Close Reason], som.closetime [Close Date], som.closeuser [Close User], '' AS soitemcustref, som.salesoid AS Marketing FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid ) AS tblmst";
            sSql += " INNER JOIN";
            sSql += " ( SELECT sod.cmpcode, sod.soitemmstoid, sod.soitemdtloid, sod.itemoid, m.itemcode AS matcode, m.itemdesc AS matlongdesc, 0.0 AS SOClosingQty, ISNULL(sod.soitemqty,0) AS SOQtyAwal, sod.soitemdtloid AS sodtloid, g.gndesc AS sounit, sod.soitemdtlnote AS sodtlnote FROM QL_trnsoitemdtl sod INNER JOIN QL_mstitem m ON  m.itemoid=sod.itemoid INNER JOIN QL_m05gn g ON g.gnoid=sod.soitemunitoid) as tbldtl ON tbldtl.cmpcode=tblmst.cmpcode AND tbldtl.soitemmstoid=tblmst.somstoid " + Join + "";
            sSql += " WHERE tblmst.cmpcode LIKE '" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                if (DDLPeriod == "som.updtime")
                {
                    sPeriode = "transdate";
                }
                else
                {
                    sPeriode = "sodate";
                }
                sSql += " AND " + sPeriode + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + sPeriode + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND somststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextCust != "")
            {
                string[] arr = TextCust.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorSO != "")
            {
                if (DDLSoNo == "som.soitemno")
                {
                    sDDLSono = "sono";
                }
                else
                {
                    sDDLSono = "draftno";
                }
                string[] arr = TextNomorSO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + sDDLSono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorRAB != "")
            {
                string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND matcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";


            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);

            for (int C1 = 0; C1 < dtRpt.Rows.Count; C1++)
            {
                dtRpt.Rows[C1]["BalanceShipmentQty"] = (decimal)dtRpt.Rows[C1]["SOQtyAwal"] - (decimal)dtRpt.Rows[C1]["SOClosingQty"] - ((decimal)dtRpt.Compute("SUM([Shipment Qty])", "sodtloid=" + dtRpt.Rows[C1]["sodtloid"]) - (decimal)dtRpt.Compute("SUM([SRETQty])", "sodtloid=" + dtRpt.Rows[C1]["sodtloid"]));
            }


            ////For Status SO Closed (In Process Register)            
            //for (int C2 = 0; C2 < dtRpt.Rows.Count; C2++)
            //{
            //    if ((string)dtRpt.Rows[C2]["somststatus"] == "Closed" && (decimal)dtRpt.Rows[C2]["BalanceShipmentQty"] > 0)
            //    {
            //        dtRpt.Rows[C2]["somststatus"] = "Approved";
            //    }
            //}

            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                    
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}