﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class EkspedisiController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public EkspedisiController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listExp
        {
            public int expoid { get; set; }
            public string expcode { get; set; }
            public string expaddr { get; set; }
            public string expname { get; set; }
            public string gndesc { get; set; }
            public string expphone1 { get; set; }
            public string expphone2 { get; set; }
            public string expemail { get; set; }
            public string activeflag { get; set; }
            public int expcityoid { get; set; }
        }

        public class dataDtl
        {
            public int seq { get; set; }
            public string picname { get; set; }
            public string phone1 { get; set; }
            public string phone2 { get; set; }
            public string email { get; set; }
            public string jabatan { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "EXP/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(expcode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstexpedisi WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND expcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;
            return sNo;
        }

        public JsonResult SetDataDetails(List<dataDtl> dtDtl)
        {
            Session["QL_mstexpedisidtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstexpedisidtl"] == null)
            {
                Session["QL_mstexpedisidtl"] = new List<dataDtl>();
            }

            List<dataDtl> dataDtl = (List<dataDtl>)Session["QL_mstexpedisidtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: Driver
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            sSql = "SELECT *,gndesc FROM QL_mstexpedisi ex INNER JOIN QL_m05GN kt ON kt.gnoid=ex.expcityoid AND gngroup='KOTA' WHERE ex.cmpcode='" + Session["CompnyCode"].ToString() + "' order by expoid ASC";
            List<listExp> ExpList = db.Database.SqlQuery<listExp>(sSql).ToList();
            return View(ExpList);
        }

        // GET: Driver/Form/5
        //pageload
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstexpedisi TblExp;
            string action = "Create";
            if (id == null)
            {
                TblExp = new QL_mstexpedisi();
                TblExp.expcode = generateNo(ClassFunction.GetServerTime());
                TblExp.cmpcode = Session["CompnyCode"].ToString();
                TblExp.createuser = Session["UserID"].ToString();
                TblExp.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                TblExp = db.QL_mstexpedisi.Find(Session["CompnyCode"].ToString(), id);
                sSql = "SELECT seq, picname, jabatan, email, phone1, phone2 FROM QL_mstexpedisidtl WHERE expoid=" + id + " ORDER BY seq";
                Session["QL_mstexpedisidtl"] = db.Database.SqlQuery<dataDtl>(sSql).ToList();
            }

            if (TblExp == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(TblExp);
            return View(TblExp);
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstexpedisi Tbldata, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            Tbldata.expphone1 = "";
            Tbldata.expphone2 = "";

            List<dataDtl> dtDtl = (List<dataDtl>)Session["QL_mstexpedisidtl"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Maaf Tolong isi data detail,,!!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Maaf Tolong isi data detail,,!!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].phone1))
                            dtDtl[i].phone1 = "";
                        if (string.IsNullOrEmpty(dtDtl[i].phone2))
                            dtDtl[i].phone2 = "";
                        if (string.IsNullOrEmpty(dtDtl[i].jabatan))
                            dtDtl[i].jabatan = "";
                        if (string.IsNullOrEmpty(dtDtl[i].email))
                            dtDtl[i].email = "";
                        if (string.IsNullOrEmpty(dtDtl[i].picname))
                        {
                            dtDtl[i].picname = "";
                            ModelState.AddModelError("", "Silahkan Isi PIC!!");
                        }                            
                    }   
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("ql_mstexpedisi");
                var dtloid = ClassFunction.GenerateID("QL_mstexpedisidtl");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            Tbldata.expoid = mstoid;
                            Tbldata.createtime = servertime;
                            Tbldata.createuser = Session["UserID"].ToString();
                            Tbldata.updtime = servertime;
                            Tbldata.upduser = Session["UserID"].ToString();
                            db.QL_mstexpedisi.Add(Tbldata);
                            db.SaveChanges();
                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstexpedisi'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            Tbldata.updtime = servertime;
                            Tbldata.upduser = Session["UserID"].ToString();
                            db.Entry(Tbldata).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstexpedisidtl.Where(a => a.expoid == Tbldata.expoid);
                            db.QL_mstexpedisidtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }
                      
                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                QL_mstexpedisidtl tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = new QL_mstexpedisidtl();
                                    tbldtl.cmpcode = Tbldata.cmpcode;
                                    tbldtl.expdtloid = dtloid++;
                                    tbldtl.expoid = Tbldata.expoid;
                                    tbldtl.seq = i + 1;
                                    tbldtl.picname = dtDtl[i].picname;
                                    tbldtl.phone1 = dtDtl[i].phone1;
                                    tbldtl.phone2 = dtDtl[i].phone2;
                                    tbldtl.email = dtDtl[i].email;
                                    tbldtl.jabatan = dtDtl[i].jabatan;
                                    tbldtl.upduser = Tbldata.upduser;
                                    tbldtl.updtime = Tbldata.updtime;
                                    db.QL_mstexpedisidtl.Add(tbldtl);
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_mstexpedisidtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(Tbldata);
            /*FillAdditionalField(QL_mstacctg);*/
            return View(Tbldata);
        }

        private void InitDDL(QL_mstexpedisi tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' ORDER BY gnoid";
            var gnoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc");
            ViewBag.expcityoid = gnoid;
        }

        private void FillAdditionalField(QL_mstexpedisi tbl)
        {
            /* ViewBag.acctgcodeind = db.Database.SqlQuery<string>("SELECT acctgcode FROM QL_mstacctg a WHERE a.acctgoid =" + tbl.acctgoid1 + "").FirstOrDefault();*/
        }


        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstexpedisi tblmst = db.QL_mstexpedisi.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstexpedisidtl.Where(a => a.expoid == id);
                        db.QL_mstexpedisidtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_mstexpedisi.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult PrintXlsReport(string typeexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (typeexp == "Expedisi")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptExpedisiXls.rpt"));
            }
            else if (typeexp == "PIC")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptExpedisiPICXls.rpt"));
            }
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "ExportExpedisi_" + typeexp + ".xls");
        }
    }
}
