﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class PGKController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public PGKController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class trncashbankmst
        {
            public string cmpcode { get; set; }
            public int nmr { get; set; }
            public int cashbankoid { get; set; }
            public string cashbankno { get; set; }
            public string cashbankrefno { get; set; }
            public string suppname { get; set; }
            public string cashbankdate { get; set; }
            public string cashbanktakegiro { get; set; }
            public string cashbankduedate { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamt { get; set; }
            public string createuser { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamtidr { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal cashbankamtusd { get; set; }
            public int suppoid { get; set; }
            public string cashbanknote { get; set; }
            public int giroacctgoid { get; set; }
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
            public int curroid { get; set; }
            public string giroflagnote { get; set; }
            public string tblname { get; set; }
            public string girostatus { get; set; }
            public string postingcashbankno { get; set; }
            public string fieldoid { get; set; }
            public string fieldstatus { get; set; }
            public string fieldnote { get; set; }
            public string fieldtakegiro { get; set; }
        }

        private void InitDDL()
        {

        }

        [HttpPost]
        public ActionResult BindGiroData(string tbDate)
        {
            DateTime cbdate = ClassFunction.GetServerTime();
            try
            {
                cbdate = DateTime.Parse(ClassFunction.toDate(tbDate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());

            }
            List<trncashbankmst> tbl = new List<trncashbankmst>();
            sSql = "SELECT 0 nmr, * FROM (";
            sSql += "SELECT 0 AS nmr, cb.cashbankoid, cb.cashbankno, cb.cashbankrefno, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN '' WHEN 'EXPENSE' THEN ISNULL((select s.suppname from QL_mstsupp s WHERE s.suppoid=cb.refsuppoid),'') ELSE ISNULL((select s.suppname from QL_mstsupp s WHERE s.suppoid=cb.refsuppoid),'') END) AS suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cb.cashbanktakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) AS cashbankduedate, cashbankamt, cb.createuser, cb.cashbankamtidr, cb.cashbankamtusd, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN cb.refsuppoid WHEN 'EXPENSE' THEN cb.refsuppoid ELSE cb.refsuppoid END) AS suppoid, cb.cashbanknote, cb.giroacctgoid, cb.acctgoid, ISNULL((SELECT a.acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cb.acctgoid),'') acctgdesc, cb.curroid, '' AS giroflagnote, 'QL_trncashbankmst' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbankres2' AS fieldnote, 'cashbanktakegiroreal' AS fieldtakegiro FROM QL_trncashbankmst cb WHERE cb.cmpcode='"+ CompnyCode +"' AND cb.cashbanktype='BGK' AND cb.cashbankgroup<>'DPAP' AND cashbankgroup NOT IN ('EXPENSE GIRO', 'PAYAP GIRO') AND cb.cashbankstatus='Post' AND ISNULL(cb.cashbankres1, '')='' AND cb.cashbankdate<=CONVERT(DATETIME, '" + cbdate.ToString("MM/dd/yyyy") + " 23:59:59')  AND cashbankrefno IS NOT NULL AND cashbankrefno<>'' AND cb.cashbankoid NOT IN(SELECT cashbankoid FROM QL_trndpap WHERE ISNULL(dpapres1,'')='Batal') ";
            sSql += " UNION ALL ";
            sSql += "SELECT 0 AS nmr, cb.cashbankoid, cb.cashbankno, ISNULL(dp.dpappayrefno,'') cashbankrefno, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN '' WHEN 'EXPENSE' THEN ISNULL((select s.suppname from QL_mstsupp s WHERE s.suppoid=cb.refsuppoid),'') ELSE ISNULL((select s.suppname from QL_mstsupp s WHERE s.suppoid=cb.personoid),'') END) AS suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cb.cashbanktakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) AS cashbankduedate, cashbankamt, cb.createuser, cb.cashbankamtidr, cb.cashbankamtusd, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN cb.personoid WHEN 'EXPENSE' THEN cb.refsuppoid ELSE cb.personoid END) AS suppoid, cb.cashbanknote, cb.giroacctgoid, cb.acctgoid, ISNULL((SELECT a.acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cb.acctgoid),'') acctgdesc, cb.curroid, '' AS giroflagnote, 'QL_trncashbankmst' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbankres2' AS fieldnote, 'cashbanktakegiroreal' AS fieldtakegiro FROM QL_trncashbankmst cb INNER JOIN QL_trndpap dp ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE cb.cmpcode='" + CompnyCode + "' AND cb.cashbanktype='BGK' AND cb.cashbankgroup='DPAP' AND cashbankgroup NOT IN ('EXPENSE GIRO', 'PAYAP GIRO') AND cb.cashbankstatus='Post' AND ISNULL(cb.cashbankres1, '')='' AND cb.cashbankdate<=CONVERT(DATETIME, '" + cbdate.ToString("MM/dd/yyyy") + " 23:59:59')  /*AND cashbankrefno IS NOT NULL AND dp.dpappayrefno<>''*/ AND cb.cashbankoid NOT IN(SELECT cashbankoid FROM QL_trndpap WHERE ISNULL(dpapres1,'')='Batal') ";
            sSql += " UNION ALL ";
            sSql += "SELECT 0 AS nmr, cashbankgloid AS cashbankoid, cb.cashbankno, cashbankglrefno AS cashbankrefno, suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cashbankgltakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cashbankglduedate, 101) AS cashbankduedate, cashbankglamt AS cashbankamt, cb.createuser, cashbankglamtidr AS cashbankamtidr, cashbankglamtusd AS cashbankamtusd, refsuppoid AS suppoid, cashbankglnote AS cashbanknote, gl.giroacctgoid, cb.acctgoid, ISNULL((SELECT a.acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cb.acctgoid),'') acctgdesc, cb.curroid, '' AS giroflagnote, 'QL_trncashbankgl' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankgloid' AS fieldoid, 'cashbankglgiroflag' AS fieldstatus, 'cashbankglgironote' AS fieldnote, 'cashbankgltakegiroreal' AS fieldtakegiro FROM QL_trncashbankgl gl INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=gl.cmpcode AND cb.cashbankoid=gl.cashbankoid INNER JOIN QL_mstsupp s ON s.suppoid=refsuppoid WHERE gl.cmpcode='" + CompnyCode + "' AND cb.cashbanktype='BGK' AND cashbankgroup='EXPENSE GIRO' AND cb.cashbankstatus='Post' AND cashbankglgiroflag='' AND cb.cashbankdate<=CONVERT(DATETIME, '" + cbdate.ToString("MM/dd/yyyy") + " 23:59:59')  AND cashbankglrefno IS NOT NULL AND cashbankglrefno<>'' ";
            sSql += " UNION ALL ";
            sSql += "SELECT 0 AS nmr, payapoid AS cashbankoid, cb.cashbankno, payaprefno AS cashbankrefno, suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), payaptakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), payapduedate, 101) AS cashbankduedate, payapamt AS cashbankamt, cb.createuser, payapamtidr AS cashbankamtidr, payapamtusd AS cashbankamtusd, cb.personoid AS suppoid, payapnote AS cashbanknote, pay.giroacctgoid, cb.acctgoid, ISNULL((SELECT a.acctgdesc FROM QL_mstacctg a WHERE a.acctgoid=cb.acctgoid),'') acctgdesc, cb.curroid, '' AS giroflagnote, 'QL_trnpayap' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'payapoid' AS fieldoid, 'payapgiroflag' AS fieldstatus, 'payapgironote' AS fieldnote, 'payaptakegiroreal' AS fieldtakegiro FROM QL_trnpayap pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstsupp s ON s.suppoid=cb.personoid WHERE pay.cmpcode='" + CompnyCode + "' AND cb.cashbanktype='BGK' AND cashbankgroup='PAYAP GIRO' AND cb.cashbankstatus='Post' AND payapgiroflag='' AND cb.cashbankdate<=CONVERT(DATETIME, '" + cbdate.ToString("MM/dd/yyyy") + " 23:59:59')  AND payaprefno IS NOT NULL AND payaprefno<>'' ";
            sSql += ") AS QL_girodetail ORDER BY CONVERT(DATETIME, cashbanktakegiro), cashbankno";

            tbl = db.Database.SqlQuery<trncashbankmst>(sSql).ToList();
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].nmr = i + 1;
                        tbl[i].cmpcode = CompnyCode;
                    }
                }
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trncashbankmst> dtDtl)
        {
            Session["QL_trncashbankmst_gk"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trncashbankmst_gk"] == null)
            {
                Session["QL_trncashbankmst_gk"] = new List<trncashbankmst>();
            }

            List<trncashbankmst> dataDtl = (List<trncashbankmst>)Session["QL_trncashbankmst_gk"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET: PRRawMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string tbDate)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (string.IsNullOrEmpty(tbDate))
            {
                ViewBag.waktusrv = ClassFunction.GetServerTime();
            }
            List<trncashbankmst> dtDtl = (List<trncashbankmst>)Session["QL_trncashbankmst_gk"];

            if (dtDtl != null)
            {
                if (dtDtl.Count() > 0)
                {
                    for (var i = 0; i < dtDtl.Count(); i++)
                    {
                        if (!string.IsNullOrEmpty(tbDate))
                        {
                            if (Convert.ToDateTime(dtDtl[i].cashbanktakegiro) > Convert.ToDateTime(ClassFunction.toDate(tbDate)))
                            {
                                ModelState.AddModelError("", "Date Take Giro for Outgoing Giro Data No. " + dtDtl[i].cashbankno + " must be less or equal than Filter Date!");
                            }
                            if (Convert.ToDateTime(dtDtl[i].cashbankduedate) <= Convert.ToDateTime(ClassFunction.toDate(tbDate)))
                            {
                                dtDtl[i].girostatus = "Post";
                            }
                            else
                            {
                                ModelState.AddModelError("", "Due Date for Outgoing Giro Data No. " + dtDtl[i].cashbankno + " must be less or equal than Filter Date!");
                            }
                        } 
                    }

                    //Generate cashbank
                    string sNo = "BBK/" + Convert.ToDateTime(ClassFunction.toDate(tbDate)).ToString("yy/MM") + "/";
                    for (var a = 0; a < dtDtl.Count(); a++)
                    {
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%'";

                        if (dtDtl[a].girostatus == "Post")
                        {
                            int numb = (db.Database.SqlQuery<int>(sSql).FirstOrDefault() + a);
                            dtDtl[a].postingcashbankno = sNo + ClassFunction.GenNumberString(numb, DefaultCounter);
                        }
                    }
                }

                //Cek Tanggal Closing
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                DateTime cekClosingDate = Convert.ToDateTime(ClassFunction.toDate(tbDate));//Tanggal Dokumen
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                }

                if (!string.IsNullOrEmpty(tbDate))
                {
                    if (ModelState.IsValid)
                    {
                        var dt = dtDtl.Where(x => x.girostatus == "Post" && x.acctgoid != 0).GroupBy(a => a.acctgoid).Select(x => new { acctgoid = x.Key });
                        if (dt != null)
                        {
                            if (dt.Count() > 0)
                            {
                                //string sNo = "BBK/" + Convert.ToDateTime(ClassFunction.toDate(tbDate)).ToString("yy/MM") + "/";
                                //foreach (var i in dt)
                                //{
                                //    for (var a = 0; a < dtDtl.Count(); a++)
                                //    {
                                //        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, " + DefaultCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + i.acctgoid + "*/";

                                //        //if (dtDtl[a].acctgoid == i.acctgoid && dtDtl[a].girostatus == "Post")
                                //        if (dtDtl[a].girostatus == "Post")
                                //        {
                                //            dtDtl[a].postingcashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
                                //        }
                                //    }
                                //}
                            }
                        }

                        var servertime = ClassFunction.GetServerTime();
                        int cashbankoid = ClassFunction.GenerateID("QL_TRNCASHBANKMST");
                        int payapgirooid = ClassFunction.GenerateID("QL_TRNPAYAPGIRO");
                        int iGlMstOid = ClassFunction.GenerateID("QL_TRNGLMST");
                        int iGlDtlOid = ClassFunction.GenerateID("QL_TRNGLDTL");
                        string sDate = ClassFunction.toDate(tbDate);
                        string sPeriod = ClassFunction.GetDateToPeriodAcctg(Convert.ToDateTime(sDate));

                        using (var objTrans = db.Database.BeginTransaction())
                        {
                            try
                            {
                                var dt2 = dtDtl.Where(x => x.girostatus == "").ToList();
                                for (var i = 0; i < dt2.Count(); i++)
                                {
                                    sSql = "UPDATE " + dt2[i].tblname + " SET " + dt2[i].fieldstatus + "='Post', " + dt2[i].fieldnote + "='" + dt2[i].giroflagnote + "', " + dt2[i].fieldtakegiro + "='" + sDate + "' WHERE cmpcode='" + CompnyCode + "' AND " + dt2[i].fieldoid + "=" + dt2[i].cashbankoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                                var dt3 = dtDtl.Where(x => x.girostatus == "Post").ToList();
                                for (var i = 0; i < dt3.Count(); i++)
                                {
                                    // Update BGK Status
                                    sSql = "UPDATE " + dt3[i].tblname + " SET " + dt3[i].fieldstatus + "='Closed', " + dt3[i].fieldnote + "='" + dt3[i].giroflagnote + "', " + dt3[i].fieldtakegiro + "='" + sDate + "' WHERE cmpcode='" + CompnyCode + "' AND " + dt3[i].fieldoid + "=" + dt3[i].cashbankoid;
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    var note_cashbank = "Pencairan Cash/Bank No. " + dt3[i].cashbankno + " | Ref. No. " + dt3[i].cashbankrefno + " | Note. " + dt3[i].cashbanknote + "";

                                    // Insert BBK (Cashbank)
                                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime) VALUES ('" + CompnyCode + "', " + cashbankoid + ", '" + sPeriod + "', '" + dt3[i].postingcashbankno + "', '" + sDate + "', 'BBK', 'GIRO OUT', " + dt3[i].acctgoid + ", " + dt3[i].curroid + ", " + dt3[i].cashbankamt + ", " + dt3[i].cashbankamtidr + ", " + dt3[i].cashbankamtusd + ", 0, '" + sDate + "', '', '"+ ClassFunction.Left(note_cashbank, 300) +"', 'Post', '" + Session["UserID"].ToString() + "', '" + servertime + "', '" + Session["UserID"].ToString() + "', '" + servertime + "')";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    // Insert BBK (Pay Giro)
                                    sSql = "INSERT INTO QL_trnpayapgiro (cmpcode, payapgirooid, cashbankoid, suppoid, reftype, refoid, refacctgoid, payapgirorefno, payapgiroduedate, payapgiroamt, payapgiroamtidr, payapgiroamtusd, payapgironote, payapgirostatus, upduser, updtime) VALUES ('" + CompnyCode + "', " + payapgirooid + ", " + cashbankoid + ", " + dt3[i].suppoid + ", '" + dt3[i].tblname + "', " + dt3[i].cashbankoid + ", " + dt3[i].giroacctgoid + ", '" + dt3[i].cashbankrefno + "', '" + dt3[i].cashbankduedate + "', " + dt3[i].cashbankamt + ", " + dt3[i].cashbankamtidr + ", " + dt3[i].cashbankamtusd + ", '" + ClassFunction.Left(note_cashbank, 300) + "', 'Post', '" + Session["UserID"].ToString() + "', '" + servertime + "')";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                    // Insert Into GL Mst
                                    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, iGlMstOid, Convert.ToDateTime(sDate), sPeriod, "Giro Out|No=" + dt3[i].postingcashbankno + "", "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 0, 0, 0, 0));
                                    // Insert Into GL Dtl
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, 1, iGlMstOid, dt3[i].giroacctgoid, "D", dt3[i].cashbankamt, dt3[i].postingcashbankno, "Pencairan Cash/Bank No. " + dt3[i].cashbankno + " | Refno. " + dt3[i].cashbankrefno + "", "Post", Session["UserID"].ToString(), servertime, dt3[i].cashbankamtidr, dt3[i].cashbankamtusd, "QL_trncashbankmst " + cashbankoid + "", "", "", "", 0));
                                    iGlDtlOid += 1;
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, iGlDtlOid, 2, iGlMstOid, dt3[i].acctgoid, "C", dt3[i].cashbankamt, dt3[i].postingcashbankno, "Pencairan Cash/Bank No. " + dt3[i].cashbankno + " | Refno. " + dt3[i].cashbankrefno + "", "Post", Session["UserID"].ToString(), servertime, dt3[i].cashbankamtidr, dt3[i].cashbankamtusd, "QL_trncashbankmst " + cashbankoid + "", "", "", "", 0));
                                    iGlDtlOid += 1;
                                    iGlMstOid += 1;
                                    payapgirooid += 1;
                                    cashbankoid += 1;
                                }
                                sSql = "UPDATE QL_ID SET lastoid=" + (cashbankoid - 1) + " WHERE tablename='QL_TRNCASHBANKMST'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_ID SET lastoid=" + (payapgirooid - 1) + " WHERE tablename='QL_TRNPAYAPGIRO'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_ID SET lastoid=" + (iGlMstOid - 1) + " WHERE tablename='QL_TRNGLMST'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                                sSql = "UPDATE QL_ID SET lastoid=" + (iGlDtlOid - 1) + " WHERE tablename='QL_TRNGLDTL'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                objTrans.Commit();
                                Session["QL_trncashbankmst_gk"] = null;
                                return RedirectToAction("Index");
                            }
                            catch (Exception ex)
                            {
                                objTrans.Rollback();
                                return View(ex.ToString());
                            }
                        }
                    }
                    else
                    {
                        ViewBag.waktusrv = ClassFunction.GetServerTime();
                    }
                }
                    
            }
            InitDDL();
            Session["QL_trncashbankmst_gk"] = null;
            return View(dtDtl);
        }
    }
}