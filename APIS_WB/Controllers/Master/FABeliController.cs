﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class FABeliController : Controller
    {
        // GET: FABeli
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public FABeliController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listfabeli
        {
            public int fabelioid { get; set; }
            public string suppname { get; set; }
            public string fabelicode { get; set; }
            public string fakturno { get; set; }
            public string activeflag { get; set; }
        }

        public class listsupp
        {
            public int suppoid { get; set; }
            public string suppname { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public int supppaymentoid { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "DIV/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(deptcode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstfabeli WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND deptcode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            List<listsupp> tbl = new List<listsupp>();

            sSql = "SELECT DISTINCT s.suppoid, s.suppname, s.suppaddr, s.suppcode, s.supppaymentoid FROM QL_mstsupp s WHERE s.activeflag = 'ACTIVE' ";
            tbl = db.Database.SqlQuery<listsupp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: Departemen
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select d.fabelioid, s.suppname, d.fabelicode, d.fakturno, d.activeflag FROM QL_mstfabeli d INNER JOIN QL_mstsupp s ON s.suppoid=d.suppoid WHERE d.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " order by d.fakturno ";
            List<listfabeli> tblmst = db.Database.SqlQuery<listfabeli>(sSql).ToList();
            return View(tblmst);
        }


        // GET: Departemen/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstfabeli tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_mstfabeli();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.fabeliqty = 1;
                tblmst.fabeliaccumqty = 0;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_mstfabeli.Find(Session["CompnyCode"].ToString(), id);
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        // POST: Departemen/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstfabeli tblmst, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(tblmst.fabelicode))
                ModelState.AddModelError("fabelicode", "Please Fill Kode!!");
            if (string.IsNullOrEmpty(tblmst.fakturno))
                ModelState.AddModelError("fakturno", "Please Fill No Faktur!!");
            if (tblmst.fabeliqty < 1)
                ModelState.AddModelError("fabeliqty", "Jumlah Pemakaian Minimum 1!!");

            if (ModelState.IsValid)
            {
                var servertime = ClassFunction.GetServerTime();
                var mstoid = ClassFunction.GenerateID("QL_mstfabeli");

                tblmst.updtime = servertime;
                tblmst.upduser = Session["UserID"].ToString();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.fabelioid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            db.QL_mstfabeli.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstfabeli'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tblmst);
            return View(tblmst);
        }

        private void setViewBag(QL_mstfabeli tblmst)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname from QL_mstsupp where suppoid = '" + tblmst.suppoid + "'").FirstOrDefault();
        }

        // POST: Departemen/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstfabeli tblmst = db.QL_mstfabeli.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstfabeli.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}