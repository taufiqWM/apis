﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class DriverController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public DriverController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listdriver
        {
            public string drivoid { get; set; }
            public string drivcode { get; set; }
            public string drivname { get; set; }
            public string drivlicno { get; set; }
            public string drivphone1 { get; set; }
            public string drivphone2 { get; set; }
            public string drivnote { get; set; }
            public string drivcardaddr { get; set; }
            public string drivres1 { get; set; }
            public string drivaddr { get; set; }
            public string activeflag { get; set; }
            public int drivcardcityoid { get; set; }
            public string drivcardno { get; set; }
            public int drivcityoid { get; set; }
            public DateTime drivlicexpdate { get; set; }
            public string simtype { get; set; }
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "DRV/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(drivcode, "+ formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstdriver WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND drivcode LIKE '" + sNo + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;
            return sNo;
        }

        // GET/POST: Driver
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            sSql = "SELECT *,CASE drivlicno WHEN 'B1U' THEN 'B1 UMUM' WHEN 'B2U' THEN 'B2 UMUM' ELSE drivlicno END simtype FROM QL_mstdriver WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' order by drivoid ASC";
            List<listdriver> driver = db.Database.SqlQuery<listdriver>(sSql).ToList();
            return View(driver);
        }

        // GET: Driver/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstdriver TblDriver;
            string action = "Create";
            if (id == null)
            {
                TblDriver = new QL_mstdriver();
                TblDriver.drivcode = generateNo(ClassFunction.GetServerTime());
                TblDriver.cmpcode = Session["CompnyCode"].ToString();
                TblDriver.createuser = Session["UserID"].ToString();
                TblDriver.createtime = ClassFunction.GetServerTime();               
            }
            else
            {
                action = "Edit";
                TblDriver = db.QL_mstdriver.Find(Session["CompnyCode"].ToString(), id);
            }

            if (TblDriver == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(TblDriver);
            FillAdditionalField(TblDriver);
            return View(TblDriver);
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstdriver Tbldata, string action, string tglexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                Tbldata.drivlicexpdate = DateTime.Parse(ClassFunction.toDate(tglexp));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("drivlicexpdate", "Tanggal tidak valid..!!" + ex.ToString());
            }

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstdriver");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            Tbldata.drivoid = mstoid;
                            Tbldata.createtime = servertime;
                            Tbldata.createuser = Session["UserID"].ToString();
                            Tbldata.updtime = servertime;
                            Tbldata.upduser = Session["UserID"].ToString();
                            db.QL_mstdriver.Add(Tbldata);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstdriver'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            Tbldata.updtime = servertime;
                            Tbldata.upduser = Session["UserID"].ToString(); 
                            db.Entry(Tbldata).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(Tbldata);
            FillAdditionalField(Tbldata);
            return View(Tbldata);
        }

        private void InitDDL(QL_mstdriver tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' ORDER BY gnoid";
            var gnoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.drivcityoid);
            ViewBag.drivcityoid = gnoid;
            var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.drivcardcityoid);
            ViewBag.drivcardcityoid = cityoid;
        }

        private void FillAdditionalField(QL_mstdriver tbl)
        {
           
        }

        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstdriver dtdriver = db.QL_mstdriver.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstdriver.Remove(dtdriver);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }                      
    }
}
