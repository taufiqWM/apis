﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class FJController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public FJController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listaritemdtl
        {
            public int aritemdtlseq { get; set; }            
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public decimal aritemqty { get; set; }
            public int aritemunitoid { get; set; }
            public string aritemunit { get; set; }
            public decimal aritemprice { get; set; }
            public decimal aritemdtlamt { get; set; }
            public decimal aritemdtltaxvalue { get; set; }
            public decimal aritemdtltaxamt { get; set; }            
            public decimal aritemdtlnetto { get; set; }           
            public string aritemdtlnote { get; set; }            
            public string shipmentitemno { get; set; }
            public int shipmentitemmstoid { get; set; }
            public int shipmentitemdtloid { get; set; }
        }

        public class listaritemdtl2
        {
            public int aritemdtl2seq { get; set; }
            public string aritemdtl2type { get; set; }
            public string aritemdtl2jenis { get; set; }
            public string ntpnno { get; set; }
            public string aritemdtl2date { get; set; }
            public decimal aritemdtl2amt { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public int rabmstoid_awal { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabtype { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public string soitemmstnote { get; set; }
            public int reqrabmstoid { get; set; }
            public string reqrabno { get; set; }
            public string custname { get; set; }
            public int custoid { get; set; }
            public string rabmstnote { get; set; }
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public string salesoid { get; set; }
            public int deptoid { get; set; }            
            public string departement { get; set; }
            public int rabpaytermoid { get; set; }
            public int gnother1 { get; set; }
            public int curroid { get; set; }
            public decimal dpfkamt { get; set; }
            public int dpfaoid { get; set; }
            public string dpfaktur { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        public class shipmentitem
        {
            public int shipmentitemmstoid { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }           
            public string shipmentitemdatestr { get; set; }           
            public string shipmentitemmstnote { get; set; }
            public string soitemno { get; set; }
            public string soitemcustpo { get; set; }
        }

        public class listduedate
        {
            public DateTime duedate { get; set; }
            public string duedatestr { get; set; }
        }

        public class listfa
        {
            public int faoid { get; set; }
            public string fakturno { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<listaritemdtl> dtDtl, List<listaritemdtl2> dtDtl2, Guid? uid)
        {
            Session["QL_trnaritemdtl"+ uid] = dtDtl;
            Session["QL_trnaritemdtl2"+ uid] = dtDtl2;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listaritemdtl> dtDtl, Guid? uid)
        {
            Session["QL_trnaritemdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trnaritemdtl"+ uid] == null)
            {
                Session["QL_trnaritemdtl"+ uid] = new List<listaritemdtl>();
            }

            List<listaritemdtl> dataDtl = (List<listaritemdtl>)Session["QL_trnaritemdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listaritemdtl2> dtDtl, Guid? uid)
        {
            Session["QL_trnaritemdtl2"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2(Guid? uid)
        {
            if (Session["QL_trnaritemdtl2"+ uid] == null)
            {
                Session["QL_trnaritemdtl2"+ uid] = new List<listaritemdtl2>();
            }

            List<listaritemdtl2> dataDtl = (List<listaritemdtl2>)Session["QL_trnaritemdtl2"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FJ-" + tanggal.ToString("yy") + "-" + tanggal.ToString("MM") + "-";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(LEFT(aritemno,14), " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnaritemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND aritemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetShipmentData(int soitemmstoid)
        {
            List<shipmentitem> tbl = new List<shipmentitem>();
            sSql = "SELECT DISTINCT sm.shipmentitemmstoid, shipmentitemno, shipmentitemdate,CONVERT(char(20),shipmentitemdate,103) shipmentitemdatestr, shipmentitemmstnote, soitemno, soitemcustpo FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentitemmstoid=sm.shipmentitemmstoid INNER JOIN QL_trnsoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.soitemmstoid=sm.soitemmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.soitemmstoid = "+ soitemmstoid +" AND shipmentitemmststatus='Post' AND ISNULL(shipmentitemmstres1,'')<>'Closed' ORDER BY shipmentitemdate DESC, sm.shipmentitemmstoid DESC";

            tbl = db.Database.SqlQuery<shipmentitem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }
        private void InitDDL(QL_trnaritemmst tbl, string action)
        {
           sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var aritempaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.aritempaytypeoid);
            ViewBag.aritempaytypeoid = aritempaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
            //string sPlus = "";
            if (action == "Create")
            {
                sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  select faoid, fakturno from QL_mstFA WHERE status = 'ACTIVE'";
                //sPlus = " WHERE /*faoid NOT IN (SELECT faoid FROM QL_trnaritemmst) AND*/ status = 'ACTIVE'";
            }
            else if (action == "Edit")
            {
                if (new List<string>{ "Approved", "Post", "Closed" }.Contains(tbl.aritemmststatus))
                {
                    sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  select faoid, fakturno from QL_mstFA WHERE faoid = '"+ tbl.faoid +"'";
                }
                else
                {
                    sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  select faoid, fakturno from QL_mstFA WHERE status = 'ACTIVE' UNION ALL  select faoid, fakturno from QL_mstFA WHERE faoid = '" + tbl.faoid + "'";
                }
            }
            //sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  select faoid, fakturno from QL_mstFA  " + sPlus + "";
            var faoid = new SelectList(db.Database.SqlQuery<listfa>(sSql).ToList(), "faoid", "fakturno", tbl.faoid);
            ViewBag.faoid = faoid;
        }

        private string BindListDDLCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return sSql;
        }

        private void FillAdditionalField(QL_trnaritemmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.rabtype = db.Database.SqlQuery<string>("SELECT rabtype FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("select (SELECT reqrabno FROM QL_trnreqrabmst where reqrabmstoid = rb.reqrabmstoid) from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid +"").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("select projectname from QL_trnrabmst where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.soitemno = db.Database.SqlQuery<string>("select soitemno from QL_trnsoitemmst rb where soitemmstoid = " + tblmst.somstoid + "").FirstOrDefault();
            ViewBag.salesoid = db.Database.SqlQuery<string>("SELECT (SELECT usname FROM QL_m01US a WHERE a.usoid = salesoid) FROM QL_trnsoitemmst where soitemmstoid = " + tblmst.somstoid +"").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.gnother1 = db.Database.SqlQuery<int>("SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid ='" + tblmst.aritempaytypeoid + "'").FirstOrDefault();
            //ViewBag.ardpamt = db.Database.SqlQuery<decimal>("SELECT ISNULL((SELECT SUM(dp.dparamt) FROM QL_trndpar dp WHERE dp.somstoid='"+ tblmst.somstoid + "' AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),0.0) dpfkamt").FirstOrDefault();
            ViewBag.dpfaoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 dp.faoid FROM QL_trndpar dp WHERE dp.somstoid='" + tblmst.somstoid + "' AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),0) dpfaoid").FirstOrDefault();
            ViewBag.dpfaktur = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 dp.dparfaktur FROM QL_trndpar dp WHERE dp.somstoid='" + tblmst.somstoid + "' AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),'') dpfaktur").FirstOrDefault();
            var pphacctgoid = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(BindListDDLCOA("VAR_PPH_22")).ToList(), "acctgoid", "acctgdesc", tblmst.pphacctgoid);
            ViewBag.pphacctgoid = pphacctgoid;
            var lbppnacctgoid = new SelectList(db.Database.SqlQuery<ReportModels.DDLAccountModel>(BindListDDLCOA("VAR_PPN_NTPN")).ToList(), "acctgoid", "acctgdesc", tblmst.lbppnacctgoid);
            ViewBag.lbppnacctgoid = lbppnacctgoid;
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }       

        [HttpPost]
        public ActionResult GetFAData(string action)
        {
            var result = "sukses";
            var msg = "";
            string sPlus = "";
            List<listfa> tbl = new List<listfa>();
            if (action == "Create")
            {
                sPlus = " WHERE /*faoid NOT IN (SELECT faoid FROM QL_trnaritemmst) AND*/ status = 'ACTIVE'";
            }
            sSql = "select 0 AS faoid, 'NONE' fakturno  UNION ALL  SELECT faoid, fakturno FROM QL_mstFA " + sPlus + "";
            tbl = db.Database.SqlQuery<listfa>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDueDate(int paytype, string tgldue)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select dateadd(D, " + paytype + ",'" + DateTime.Parse(ClassFunction.toDate(tgldue)) + "') duedate,CONVERT(char(20),dateadd(D, " + paytype + ",'" + DateTime.Parse(ClassFunction.toDate(tgldue)) + "'),103) duedatestr ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT rm.rabmstoid, rm.rabtype, CASE WHEN rm.revrabtype='Baru' THEN rm.rabmstoid ELSE rm.revrabmstoid END rabmstoid_awal, rm.rabpaytermoid, (SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = rm.rabpaytermoid) gnother1, rm.rabno, rm.rabdate, CONVERT(CHAR(10),rm.rabdate,103) rabdatestr, soitemmstoid, soitemno, rm.reqrabmstoid, rm.projectname, rm.salesoid, c.custname, c.custoid, rm.rabmstnote, rm.alamatoid, c2.custdtl2addr alamat, rm.salesoid, rm.deptoid, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=rm.deptoid),'') departement, rm.curroid, ISNULL((SELECT SUM(dp.dparamt-dp.dparaccumamt) FROM QL_trndpar dp WHERE dp.somstoid=sm.soitemmstoid AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),0.0) dpfkamt, ISNULL((SELECT TOP 1 dp.faoid FROM QL_trndpar dp WHERE dp.somstoid=sm.soitemmstoid AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),0) dpfaoid, ISNULL((SELECT TOP 1 dp.dparfaktur FROM QL_trndpar dp WHERE dp.somstoid=sm.soitemmstoid AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),0) dpfaktur, sm.soitemmstnote FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_mstcustdtl2 c2 ON c2.custdtl2oid = rm.alamatoid INNER JOIN QL_trnsoitemmst sm ON sm.rabmstoid = rm.rabmstoid WHERE rm.cmpcode = '" + CompnyCode + "' AND rm.rabmststatus='Approved' /*AND sm.soitemmstoid IN (SELECT soitemmstoid FROM QL_trnshipmentitemmst where shipmentitemmststatus = 'Post' AND ISNULL(shipmentitemmstres1,'')<>'Closed' AND shipmentitemtype NOT IN ('Jasa'))*/ ORDER BY rabdate DESC";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailData(int shipmentitemmstoid)
        {
            List<listaritemdtl> tbl = new List<listaritemdtl>();
            sSql = "SELECT sod.soitemdtlseq aritemdtlseq, sm.shipmentitemno, rabdtloid, m.itemoid, m.itemcode, m.itemdesc, sd.refno, (shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentitemdtloid=sd.shipmentitemdtloid AND sretitemmststatus<>'Rejected'), 0.0)) AS aritemqty, sod.soitemunitoid aritemunitoid, gn.gndesc aritemunit, sod.soitemprice aritemprice,sod.soitemdtltaxvalue aritemdtltaxvalue, '' aritemdtlnote, sd.shipmentitemmstoid, sd.shipmentitemdtloid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = sd.itemoid INNER JOIN QL_m05gn gn ON gnoid = shipmentitemunitoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode = sm.cmpcode AND som.soitemmstoid = sm.soitemmstoid INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode = sd.cmpcode AND sod.soitemmstoid = sm.soitemmstoid AND sod.soitemdtloid = sd.soitemdtloid WHERE sd.cmpcode = '" + CompnyCode + "' AND sm.shipmentitemmstoid = " + shipmentitemmstoid + " AND shipmentitemdtlstatus = '' AND(shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode = sretd.cmpcode AND sretm.sretitemmstoid = sretd.sretitemmstoid WHERE sretd.cmpcode = sd.cmpcode AND sretd.shipmentitemdtloid = sd.shipmentitemdtloid AND sretitemmststatus <> 'Rejected'), 0.0)) > 0 ORDER BY shipmentitemdtlseq";

                tbl = db.Database.SqlQuery<listaritemdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetItemBySO(int soitemmstoid)
        {
            List<listaritemdtl> tbl = new List<listaritemdtl>();

            sSql = "SELECT sod.soitemdtlseq aritemdtlseq, '' shipmentitemno, rabdtloid, m.itemoid, m.itemcode, m.itemdesc, '' refno, soitemqty AS aritemqty, sod.soitemunitoid aritemunitoid, gn.gndesc aritemunit, sod.soitemprice aritemprice,sod.soitemdtltaxvalue aritemdtltaxvalue, '' aritemdtlnote, 0 shipmentitemmstoid, 0 shipmentitemdtloid FROM QL_trnsoitemmst som INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode = sod.cmpcode AND sod.soitemmstoid = som.soitemmstoid INNER JOIN QL_mstitem m ON m.itemoid = sod.itemoid INNER JOIN QL_m05gn gn ON gnoid = soitemunitoid WHERE sod.cmpcode = '"+ CompnyCode +"' AND soitemmststatus IN ('Approved','Post','Closed') AND som.soitemmstoid="+ soitemmstoid +" ORDER BY soitemdtlseq";
            tbl = db.Database.SqlQuery<listaritemdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetail2Data(string aritemdtl2type, string aritemdtl2jenis, string aritemdtl2date, string ntpnno, int aritemmstoid)
        {
            int acctgoid = 0;
            if (aritemdtl2type == "PPN")
            {
                acctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_NTPN", CompnyCode));
            }
            else if (aritemdtl2type == "PPH 22")
            {
                acctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPH_22", CompnyCode));
            }
            else if (aritemdtl2type == "PPH 23")
            {
                acctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPH_23", CompnyCode));
            }

            List<listaritemdtl2> tbl = new List<listaritemdtl2>();
            sSql = "SELECT 0 aritemdtl2seq,'" + aritemdtl2type + "' aritemdtl2type,'" + aritemdtl2jenis + "' aritemdtl2jenis, '" + aritemdtl2date + "' aritemdtl2date, '" + ntpnno + "' ntpnno, ISNULL((SELECT TOP 1 (CASE WHEN cb.addacctgoid1=" + acctgoid + " THEN ABS(addacctgamt1) WHEN cb.addacctgoid2=" + acctgoid + " THEN ABS(addacctgamt2) WHEN cb.addacctgoid3=" + acctgoid + " THEN ABS(addacctgamt3) ELSE 0.0 END) aritemdtl2amt FROM QL_trnpayar pay INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid WHERE pay.reftype='QL_trnaritemmst' AND (cb.addacctgamt1<>0 OR cb.addacctgamt2<>0 OR cb.addacctgamt3<>0) AND pay.refoid=" + aritemmstoid + "),0.0) + ISNULL((SELECT ABS(pay.cashbankglamt) FROM QL_trncashbankgl pay INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid WHERE pay.acctgoid=" + acctgoid + " AND ISNULL(cb.aritemmstoid,0)=" + aritemmstoid + "),0.0) /*+ ISNULL((SELECT (CASE WHEN pay.addacctgoid1=" + acctgoid + " THEN ABS(addacctgamt1) WHEN pay.addacctgoid2=" + acctgoid + " THEN ABS(addacctgamt2) WHEN pay.addacctgoid3=" + acctgoid + " THEN ABS(addacctgamt3) ELSE 0.0 END) aritemdtl2amt FROM QL_trndpar pay INNER JOIN QL_trnaritemmst cb ON cb.somstoid=pay.somstoid WHERE pay.soreftype='QL_trnsoitemmst' AND cb.aritemmstoid=" + aritemmstoid + "),0.0)*/ aritemdtl2amt ";

            tbl = db.Database.SqlQuery<listaritemdtl2>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();
            //tblmst.aritemduedate = ClassFunction.GetServerTime().AddDays(7);
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
      
        // GET/POST: aritem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View(); 
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No FJ",["Customer"] = "Customer",["Project"] = "Project",["NomorSO"] = "No SO" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {

            sSql = "SELECT * FROM( Select pr.aritemmstoid, pr.aritemno, pr.aritemdate, c.custname, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') rabno, ISNULL((SELECT rm.rabtype2 FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') rabtype2, ISNULL((SELECT rm.soitemno FROM QL_trnsoitemmst rm WHERE rm.soitemmstoid=pr.somstoid),'') soitemno, pr.aritemgrandtotal grandtotal, pr.aritemmststatus, pr.aritemtype2, CASE WHEN pr.aritemmststatus='Closed' THEN 0 ELSE DATEDIFF(DAY,pr.aritemduedate,GETDATE()) END as countjt FROM QL_trnaritemmst pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid WHERE pr.cmpcode='" + CompnyCode + "' AND pr.aritemtype NOT IN ('JASA') ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.aritemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.aritemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.aritemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.aritemno LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Customer") sSql += " AND t.custname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Project") sSql += " AND t.projectname LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "NomorSO") sSql += " AND t.soitemno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.aritemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.aritemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.aritemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.aritemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnaritemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = new QL_trnaritemmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.aritemno = generateNo(ClassFunction.GetServerTime());
                tblmst.aritemdate = ClassFunction.GetServerTime();
                tblmst.pphamt = 0;
                tblmst.lbppnamt = 0;               
                tblmst.aritemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_trnaritemdtl"+ ViewBag.uid] = null;
            }
            else
            {
                ViewBag.uid = Guid.NewGuid();
                action = "Edit";
                tblmst = db.QL_trnaritemmst.Find(Session["CompnyCode"].ToString(), id);

                if (tblmst.aritemtype2 == "Draft")
                {
                    sSql = "SELECT rd.aritemdtlseq, '' shipmentitemno, rd.itemoid, i.itemcode, i.itemdesc, '' refno, rd.aritemqty, rd.aritemunitoid, g.gndesc aritemunit, rd.aritemprice, rd.aritemdtlamt, rd.aritemdtltaxvalue, rd.aritemdtltaxamt, rd.aritemdtlnetto, rd.aritemdtlnote, rd.shipmentitemdtloid, rd.shipmentitemmstoid FROM QL_trnaritemdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.aritemunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.aritemmstoid=" + id + " ORDER BY rd.aritemdtlseq";
                }
                else
                {
                    sSql = "SELECT rd.aritemdtlseq, sm.shipmentitemno, rd.itemoid, i.itemcode, i.itemdesc, (SELECT refno FROM QL_trnshipmentitemdtl where shipmentitemdtloid = rd.shipmentitemdtloid) refno, rd.aritemqty, rd.aritemunitoid, g.gndesc aritemunit, rd.aritemprice, rd.aritemdtlamt, rd.aritemdtltaxvalue, rd.aritemdtltaxamt, rd.aritemdtlnetto, rd.aritemdtlnote, rd.shipmentitemdtloid, rd.shipmentitemmstoid FROM QL_trnaritemdtl rd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = rd.shipmentitemmstoid INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.aritemunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.aritemmstoid=" + id + " ORDER BY rd.aritemdtlseq";
                }
                Session["QL_trnaritemdtl"+ ViewBag.uid] = db.Database.SqlQuery<listaritemdtl>(sSql).ToList();

                sSql = "SELECT rd.aritemdtl2seq, rd.aritemdtl2type, rd.aritemdtl2jenis, rd.ntpnno, CONVERT(CHAR(10),rd.aritemdtl2date,103) aritemdtl2date, rd.aritemdtl2amt FROM QL_trnaritemdtl2 rd WHERE rd.cmpcode='" + CompnyCode + "' AND rd.aritemmstoid=" + id + " ORDER BY rd.aritemdtl2seq";
                Session["QL_trnaritemdtl2"+ ViewBag.uid] = db.Database.SqlQuery<listaritemdtl2>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst, action);            
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnaritemmst tblmst, string action, string tglmst, string tgldue, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (string.IsNullOrEmpty(tblmst.aritemno))
            {
                tblmst.aritemno = generateNo(ClassFunction.GetServerTime());
            }         
            try
            {
                tblmst.aritemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("aritemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.aritemduedate = DateTime.Parse(ClassFunction.toDate(tgldue));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("aritemduedate", "Format Tanggal Jatuh Tempo Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tblmst.aritemmstnote))
                tblmst.aritemmstnote = "";           
            if (string.IsNullOrEmpty(tblmst.aritemmstres1))
                tblmst.aritemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.aritemmstres2))
                tblmst.aritemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.aritemmstres3))
                tblmst.aritemmstres3 = "";

            decimal dAmtOngkir = 0; decimal dAmtJasa = 0; decimal dAmtJasaLain = 0;
            //is Input Detail Valid
            List<listaritemdtl> dtDtl = (List<listaritemdtl>)Session["QL_trnaritemdtl"+ uid];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl != null)
                {
                    if (dtDtl.Count > 0)
                    {
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (string.IsNullOrEmpty(dtDtl[i].aritemdtlnote))
                                dtDtl[i].aritemdtlnote = "";
                            if (dtDtl[i].aritemqty == 0)
                            {
                                ModelState.AddModelError("", "Qty " + dtDtl[i].itemdesc + " Belum Diisi !!");
                            }
                            if (dtDtl[i].aritemprice == 0)
                            {
                                ModelState.AddModelError("", "Harga " + dtDtl[i].itemdesc + " Belum Diisi !!");
                            }

                            sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                            var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                            if (type == "Ongkir")
                            {
                                dAmtOngkir += dtDtl[i].aritemqty * dtDtl[i].aritemprice;
                            }
                            if (type == "Jasa")
                            {
                                sSql = "SELECT jasatype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                                var type2 = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
                                if (type2 == "PENDAPATAN")
                                {
                                    dAmtJasa += dtDtl[i].aritemqty * dtDtl[i].aritemprice;
                                }
                                else
                                {
                                    dAmtJasaLain += dtDtl[i].aritemqty * dtDtl[i].aritemprice;
                                }
                                
                            }
                        }
                        if (tblmst.aritemmststatus == "Post")
                        {
                            if (tblmst.aritemtype2 == "Draft")
                            {
                                ModelState.AddModelError("aritemtype2", "- Tipe Transaksi Draft, Silahkan Ganti Tipe Transaksi Menjadi Complete Jika Barang Sudah Lengkap!!");
                            }
                            if (string.IsNullOrEmpty(tblmst.aritemno))
                                ModelState.AddModelError("aritemno", "Silahkan isi No. Faktur!");
                            else if (db.QL_trnaritemmst.Where(w => w.aritemno == tblmst.aritemno & w.aritemmstoid != tblmst.aritemmstoid).Count() > 0)
                                ModelState.AddModelError("aritemno", "No. Faktur yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");                                                 
                        }
                    }
                }
            }

            // Interface Validation
            if (!ClassFunction.IsInterfaceExists("VAR_AR", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AR"));
            if (!ClassFunction.IsInterfaceExists("VAR_JUAL", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_JUAL"));
            if (!ClassFunction.IsInterfaceExists("VAR_HPP_JUAL", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_HPP_JUAL"));
            if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
            if (!ClassFunction.IsInterfaceExists("VAR_PPN_OUT", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_OUT"));
            if (!ClassFunction.IsInterfaceExists("VAR_ONGKIR_JUAL", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_ONGKIR_JUAL"));
            if (!ClassFunction.IsInterfaceExists("VAR_PENDAPATAN_JASA", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PENDAPATAN_JASA"));
            if (!ClassFunction.IsInterfaceExists("VAR_DPFAKTUR_K", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_DPFAKTUR_K"));
            if (!ClassFunction.IsInterfaceExists("VAR_AR_LAIN", CompnyCode))
                ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AR_LAIN"));

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tblmst.aritemmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            if (tblmst.aritemtaxamt > 1)
            {
                if (string.IsNullOrEmpty(tblmst.aritemfaktur))
                {
                    ModelState.AddModelError("aritemfaktur", "Silahkan Isi kode faktur!!");
                }
                if(tblmst.faoid == 0)
                {
                    ModelState.AddModelError("aritemfaktur", "Silahkan Pilih No. faktur!!");
                }
            }

            sSql = "SELECT custname FROM QL_mstcust WHERE custoid=" + tblmst.custoid + "";
            string custname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            sSql = "SELECT fakturno FROM QL_mstfa WHERE faoid="+ tblmst.faoid +"";
            string fakturno = tblmst.aritemfaktur + '-' + db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            sSql = "SELECT dp.dparfaktur+'-'+f.fakturno FROM QL_trndpar dp INNER JOIN QL_mstFA f ON f.faoid=ISNULL(dp.faoid,0) WHERE dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal' AND dp.soreftype='QL_trnsoitemmst' AND dp.somstoid=" + tblmst.somstoid +"";
            string fakturnodp = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            //decimal dpfkamt = db.Database.SqlQuery<decimal>("SELECT ISNULL((SELECT SUM(dp.dparamt) FROM QL_trndpar dp WHERE dp.somstoid='" + tblmst.somstoid + "' AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype='DPFK' AND ISNULL(dp.dparres1,'')<>'Batal'),0.0) dpfkamt").FirstOrDefault();
            //tblmst.ardpamt = dpfkamt;

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.aritemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.aritemmststatus = "In Process";
            }
            if (tblmst.aritemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.aritemmststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnaritemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnaritemdtl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var conaroid = ClassFunction.GenerateID("QL_conar");

                var iAcctgOidAR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR", CompnyCode));
                var iAcctgOidJual = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_JUAL", CompnyCode));
                var iAcctgOidHPP = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_HPP_JUAL", CompnyCode));
                var iAcctgOidTrans = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", CompnyCode));
                var iAcctgOidOngkir = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_ONGKIR_JUAL", CompnyCode));
                var iAcctgOidJasa = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PENDAPATAN_JASA", CompnyCode));
                var iAcctgOidDPFK = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_DPFAKTUR_K", CompnyCode));
                var iAcctgOidARLain = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR_LAIN", CompnyCode));

                var glseq = 1;

                tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                tblmst.rateoid = 1;
                tblmst.rate2oid = rate2oid;
                tblmst.aritemmstdiscamt = 0;
                tblmst.aritemmstdisctype = "";
                tblmst.aritemmstdiscvalue = 0;
                tblmst.aritemratetoidr = "1";
                tblmst.aritemratetousd = "0";
                tblmst.aritemrate2toidr = rate2toidr;
                tblmst.aritemrate2tousd = "0";
                tblmst.aritemtaxtype = "TAX";
                tblmst.aritemtaxvalue = 10;
                tblmst.aritemtotaldisc = 0;
                tblmst.aritemtotaldiscdtl = 0;
                tblmst.aritemtype = "";

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.aritemmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();                          
                            db.QL_trnaritemmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnaritemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            //update status fa
                            sSql = "UPDATE QL_mstFA SET status = 'INACTIVE' WHERE faoid = " + tblmst.faoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus = 'Post' WHERE shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl where aritemmstoid = " + tblmst.aritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus = '' WHERE shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl where aritemmstoid = " + tblmst.aritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnaritemdtl.Where(a => a.aritemmstoid == tblmst.aritemmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnaritemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnaritemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnaritemdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.aritemdtloid = dtloid++;
                            tbldtl.aritemmstoid = tblmst.aritemmstoid;
                            tbldtl.aritemdtlseq = i + 1;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.aritemqty = dtDtl[i].aritemqty;
                            tbldtl.aritemprice = dtDtl[i].aritemprice;
                            tbldtl.aritemunitoid = dtDtl[i].aritemunitoid;
                            tbldtl.aritemdtlamt = dtDtl[i].aritemdtlamt;
                            tbldtl.aritemdtlnetto = dtDtl[i].aritemdtlnetto;
                            tbldtl.aritemdtlstatus = "";
                            tbldtl.aritemdtlnote = dtDtl[i].aritemdtlnote;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.aritemdtldisctype = "";
                            tbldtl.aritemdtldiscamt = 0;
                            tbldtl.aritemdtldiscvalue = 0;
                            tbldtl.aritemdtlres1 = "";
                            tbldtl.aritemdtlres2 = "";
                            tbldtl.aritemdtlres3 = "";
                            tbldtl.shipmentitemmstoid = dtDtl[i].shipmentitemmstoid;
                            tbldtl.shipmentitemdtloid = dtDtl[i].shipmentitemdtloid;
                            tbldtl.aritemdtltaxvalue = dtDtl[i].aritemdtltaxvalue;
                            tbldtl.aritemdtltaxamt = dtDtl[i].aritemdtltaxamt;

                            db.QL_trnaritemdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus = 'Complete' WHERE shipmentitemdtloid = " + dtDtl[i].shipmentitemdtloid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusjual='FJ PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid = " + tblmst.rabmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + dtDtl[i].shipmentitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemdtlstatus='' AND shipmentitemmstoid=" + dtDtl[i].shipmentitemmstoid + " AND shipmentitemdtloid<>" + dtDtl[i].shipmentitemdtloid + ")=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusjual='FJ' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid = " + tblmst.rabmstoid + " AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl md INNER JOIN QL_trnshipmentitemmst mm ON mm.shipmentitemmstoid = md.shipmentitemmstoid WHERE mm.cmpcode='" + CompnyCode + "' AND shipmentitemdtlstatus='' AND mm.soitemmstoid IN (SELECT soitemmstoid from ql_trnsoitemmst where rabmstoid = " + tblmst.rabmstoid + ") AND md.shipmentitemdtloid<>" + dtDtl[i].shipmentitemdtloid + ")=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();                            
                        }

                        if (tblmst.ardpamt > 0)
                        {
                            //Update Accum DPFK
                            sSql = "UPDATE QL_trndpar SET dparaccumamt=dparamt WHERE somstoid='" + tblmst.somstoid + "' AND soreftype='QL_trnsoitemmst' AND dparpaytype='DPFK' AND ISNULL(dparres1,'')<>'Batal'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        if (tblmst.aritemmststatus == "Post")
                        {
                            decimal argrandtotal = Convert.ToDecimal(tblmst.aritemothercost);//nilai setelah di potong pph & ln ppn
                            decimal artotalamt = Convert.ToDecimal(tblmst.aritemtotalamt - tblmst.aritemtotaldiscdtl) - dAmtOngkir - dAmtJasa - dAmtJasaLain;
                            decimal artaxamt = Convert.ToDecimal(tblmst.aritemtaxamt);

                            var shipmentdtl = (from sd in db.QL_trnshipmentitemdtl join ard in db.QL_trnaritemdtl on new { cmpcode = sd.cmpcode, shipmentitemdtloid = sd.shipmentitemdtloid } equals new { cmpcode = ard.cmpcode, shipmentitemdtloid = ard.shipmentitemdtloid } where ard.cmpcode == CompnyCode && ard.aritemmstoid == tblmst.aritemmstoid select new { sd.shipmentitemvalueidr, sd.shipmentitemvalueusd, sd.shipmentitemqty, ard.aritemqty }).ToList();
                            decimal shipmentamtidr = 0;
                            shipmentamtidr = Math.Round(shipmentdtl.Sum(x => (x.shipmentitemvalueidr / x.shipmentitemqty) * x.aritemqty), 4);

                            //Insert QL_conar
                            db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, conaroid++, "QL_trnaritemmst", tblmst.aritemmstoid, 0, tblmst.custoid, iAcctgOidAR, "Post", "ARFG", tblmst.aritemdate, ClassFunction.GetDateToPeriodAcctg(tblmst.aritemdate), 0, DateTime.Parse("01/01/1900"), "", 0, DateTime.Parse("01/01/1900"), argrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));

                            if (shipmentamtidr > 0)
                            {
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jual|No. " + tblmst.aritemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                // Insert QL_trngldtl
                                // D : HPP Penjualan
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidHPP, "D", shipmentamtidr, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, shipmentamtidr, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                db.SaveChanges();

                                // C : Stock dalam perjalanan
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "C", shipmentamtidr, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, shipmentamtidr, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            if (argrandtotal > 0)
                            {
                                glmstoid += 1;
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.aritemno + " | " + custname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                // Insert QL_trngldtl
                                // D : Piutang Usaha
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAR, "D", argrandtotal, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                db.SaveChanges();

                                if (tblmst.pphamt > 0)
                                {
                                    // D : PPH
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tblmst.pphacctgoid.Value, "D", tblmst.pphamt, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, tblmst.pphamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                if (tblmst.lbppnamt > 0)
                                {
                                    // D : LB PPN
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tblmst.lbppnacctgoid.Value, "D", tblmst.lbppnamt, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, tblmst.lbppnamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                // C : Penjualan
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidJual, "C", artotalamt, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, artotalamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                db.SaveChanges();

                                if (dAmtOngkir > 0)
                                {
                                    // C : Ongkir
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidOngkir, "C", dAmtOngkir, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, dAmtOngkir * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                if (dAmtJasa > 0)
                                {
                                    // C : Jasa
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidJasa, "C", dAmtJasa, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, dAmtJasa * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                if (dAmtJasaLain > 0)
                                {
                                    // C : Jasa Lain(Penjualan Lain)
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidARLain, "C", dAmtJasaLain, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, dAmtJasaLain * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }

                                if (artaxamt > 0)
                                {
                                    // C : PPN KELUARAN
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "C", artaxamt, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, artaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0, fakturno));
                                    db.SaveChanges();
                                }

                                if (tblmst.ardpamt > 0)
                                {
                                    //glmstoid += 1;
                                    //// Insert QL_trnglmst
                                    //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                    //db.SaveChanges();

                                    //// Insert QL_trngldtl
                                    //// D : DPFK
                                    //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidDPFK, "D", dpfkamt, tblmst.aritemno, tblmst.aritemno + " | " + custname + " | " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, dpfkamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0, fakturnodp));
                                    //db.SaveChanges();

                                    // C : Piutang Suspend
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "C", tblmst.ardpamt, tblmst.aritemno, "Faktur Jual|No. " + tblmst.aritemno + "|Note. " + tblmst.aritemmstnote, "Post", Session["UserID"].ToString(), servertime, tblmst.ardpamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + (conaroid - 1) + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnaritemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");                        
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.aritemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.aritemmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst, action);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnaritemmst tblmst = db.QL_trnaritemmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //update status fa
                        sSql = "UPDATE QL_mstFA SET status = 'ACTIVE' WHERE faoid = " + tblmst.faoid + "";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus = 'Post' WHERE shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl where aritemmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus = '' WHERE shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl where aritemmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='FJ PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnaritemmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.aritemmstoid=" + id + " )";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnaritemmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.aritemmstoid=" + id + ") AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl md INNER JOIN QL_trnshipmentitemmst mm ON mm.shipmentitemmstoid = md.shipmentitemmstoid INNER JOIN QL_trnsoitemmst pom ON pom.soitemmstoid = mm.soitemmstoid WHERE mm.cmpcode='" + CompnyCode + "' AND shipmentitemdtlstatus='' AND pom.rabmstoid IN (SELECT rabmstoid from QL_trnaritemmst where aritemmstoid <> " + id + "))=0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tblmst.ardpamt > 0)
                        {
                            //Update Accum DPFK
                            sSql = "UPDATE QL_trndpar SET dparaccumamt=0 WHERE somstoid='" + tblmst.somstoid + "' AND soreftype='QL_trnsoitemmst' AND dparpaytype='DPFK' AND ISNULL(dparres1,'')<>'Batal'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        var trndtl = db.QL_trnaritemdtl.Where(a => a.aritemmstoid == id);
                        db.QL_trnaritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnaritemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("SaveNTPN")]
        [ValidateAntiForgeryToken]
        public ActionResult SaveConfirmed(int? id, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";

            QL_trnaritemmst tblmst = db.QL_trnaritemmst.Find(Session["CompnyCode"].ToString(), id);
            List<listaritemdtl2> dtDtl2 = (List<listaritemdtl2>)Session["QL_trnaritemdtl2"+ uid];
            var servertime = ClassFunction.GetServerTime();

            if (dtDtl2 == null)
                msg += "Please fill detail data!";
            else if (dtDtl2.Count <= 0)
                msg += "Please fill detail data!";

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl2[i].ntpnno))
                        {
                            msg += "Please fill No NTPN!";
                        }
                        if (dtDtl2[i].aritemdtl2amt <= 0)
                        {
                            msg += "Nominal Harus Lebih Besar 0!";
                        }
                    }
                }
            }

            if (msg != "")
            {
                result = "failed";
            }
            else
            {
                var dtl2oid = ClassFunction.GenerateID("QL_trnaritemdtl2");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnaritemdtl2.Where(a => a.aritemmstoid == id);
                        db.QL_trnaritemdtl2.RemoveRange(trndtl);
                        db.SaveChanges();

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                QL_trnaritemdtl2 tbldtl2;
                                for (int i = 0; i < dtDtl2.Count(); i++)
                                {
                                    tbldtl2 = new QL_trnaritemdtl2();
                                    tbldtl2.cmpcode = tblmst.cmpcode;
                                    tbldtl2.aritemdtl2oid = dtl2oid++;
                                    tbldtl2.aritemmstoid = tblmst.aritemmstoid;
                                    tbldtl2.aritemdtl2seq = i + 1;
                                    tbldtl2.aritemdtl2type = dtDtl2[i].aritemdtl2type;
                                    tbldtl2.aritemdtl2jenis = dtDtl2[i].aritemdtl2jenis;
                                    tbldtl2.aritemdtl2date = DateTime.Parse(ClassFunction.toDate(dtDtl2[i].aritemdtl2date));
                                    tbldtl2.ntpnno = dtDtl2[i].ntpnno;
                                    tbldtl2.aritemdtl2amt = dtDtl2[i].aritemdtl2amt;
                                    tbldtl2.createuser = tblmst.upduser;
                                    tbldtl2.createtime = tblmst.updtime;
                                    tbldtl2.upduser = tblmst.upduser;
                                    tbldtl2.updtime = tblmst.updtime;

                                    db.QL_trnaritemdtl2.Add(tbldtl2);
                                    db.SaveChanges();
                                }

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtl2oid - 1) + " WHERE tablename='QL_trnaritemdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id, string sRekening, string sBank, string sAtasNama)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var rabtype = "";
            sSql = "SELECT ISNULL((SELECT rm.rabtype FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid WHERE arm.aritemmstoid="+ id +"),'') dt";
            rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            if (rabtype == "WAPU")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARTrn2.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARTrn.rpt"));
            }  

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");
            report.SetParameterValue("sRekening", sRekening);
            report.SetParameterValue("sBank", sBank);
            report.SetParameterValue("sAtasNama", sAtasNama);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "FJItemReport_"+ sNo +".pdf");
        }

        public ActionResult PrintReportSPP(int id, string sRekening, string sBank, string sAtasNama)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSPP.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");
            report.SetParameterValue("sRekening", sRekening);
            report.SetParameterValue("sBank", sBank);
            report.SetParameterValue("sAtasNama", sAtasNama);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            //Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
            //stream.Seek(0, SeekOrigin.Begin);
            //report.Close(); report.Dispose();
            //return File(stream, "application/vnd.ms-word", "SPP_"+ sNo +".doc");
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SPP_" + sNo + ".pdf");
        }

        public ActionResult PrintReportKWT(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var rabtype = "";
            sSql = "SELECT ISNULL((SELECT rm.rabtype FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid WHERE arm.aritemmstoid=" + id + "),'') dt";
            rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            if (rabtype == "WAPU")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKWT2.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKWT.rpt"));
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            //Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
            //stream.Seek(0, SeekOrigin.Begin);
            //report.Close(); report.Dispose();
            //return File(stream, "application/vnd.ms-word", "KWT_" + sNo + ".doc");
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "KWT_" + sNo + ".pdf");
        }

        public ActionResult PrintReportBASTB(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptBASTB.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            //Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
            //stream.Seek(0, SeekOrigin.Begin);
            //report.Close(); report.Dispose();
            //return File(stream, "application/vnd.ms-word", "BASTB_" + sNo + ".doc");
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "BASTB_" + sNo + ".pdf");
        }

        public ActionResult PrintReportSKPBN(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSKPBN.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SKPBN_"+ sNo +".pdf");
        }

        public ActionResult PrintReportSurat(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var rabtype = "";
            sSql = "SELECT ISNULL((SELECT rm.rabtype FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid WHERE arm.aritemmstoid=" + id + "),'') dt";
            rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var seq = 0; string nomor = "";
            //sSql = "select ISNULL(CAST(aritemmstres3 AS INT),0) + 1 from QL_trnaritemmst WHERE aritemmstoid=" + id + "";
            //seq = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            nomor = "0" + seq;

            //sSql = "UPDATE QL_trnaritemmst SET aritemmstres3='" + seq + "' WHERE aritemmstoid=" + id + "";
            //db.Database.ExecuteSqlCommand(sSql);
            //db.SaveChanges();

            ReportDocument report = new ReportDocument();
            if (rabtype == "WAPU")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARSuratWapu.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARSurat.rpt"));
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");
            report.SetParameterValue("Nomor", nomor);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SuratReminder_" + sNo + ".pdf");
        }

        public ActionResult PrintReportGBN(int id, string sRekening, string sBank, string sAtasNama)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var rabtype = "";
            sSql = "SELECT ISNULL((SELECT rm.rabtype FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid WHERE arm.aritemmstoid=" + id + "),'') dt";
            rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            if (rabtype == "WAPU")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARGbnTrn2.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARGbnTrn.rpt"));
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usoid FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");
            report.SetParameterValue("sRekening", sRekening);
            report.SetParameterValue("sBank", sBank);
            report.SetParameterValue("sAtasNama", sAtasNama);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "FJItemReport_" + sNo + ".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}