﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PGController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public PGController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listpg
        {
            public string cmpcode { get; set; }
            public int transitemoid { get; set; }
            public string transitemno { get; set; }
            public string transitemdocrefno { get; set; }
            public DateTime transitemdate { get; set; }
            public string transdocdate { get; set; }
            public string transitemnote { get; set; }
            public String transitemstatus { get; set; }
            
        }

        public class listpgdtl
        {
            public int transitemdtlseq { get; set; }
            public int transitemfromwhoid { get; set; }
            public string mtrwhdesc { get; set; }
            public int transitemtowhoid { get; set; }
            public string mtrwhtodesc { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public Decimal qtystok { get; set; }
            public decimal transitemqty { get; set; }
            public int transitemunitoid { get; set; }
            public string transitemunit { get; set; }
            public string transitemdtlnote { get; set; }           
            public decimal transitemvalueidr { get; set; }
            public string transitemdtlres1 { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public string itemtype { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal qtystok { get; set; }
            public string mtrwhdesc { get; set; }
            public int transitemfromwhoid { get; set; }
            public string mtrwhtodesc { get; set; }
            public int transitemtowhoid { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
        }

        public class liststock
        {            
            public Decimal stokakhir { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listpgdtl> dtDtl, Guid? uid)
        {
            Session["QL_trntransitemdtl"+ uid] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public ActionResult InitDDLGudangFrom()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'GUDANG' and gnflag = 'ACTIVE' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLGudangTo(int whfrom)
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'GUDANG' and gnflag = 'ACTIVE' and gnoid <> " + whfrom + " ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = "+ whfrom +" and refoid = "+ itemoid +" and refno = '"+ refno +"' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trntransitemmst tbl)
        {
            sSql = "SELECT COUNT(*) FROM QL_trntransitemdtl WHERE transitemmstoid='" + tbl.transitemmstoid + "'";
            var cTrn = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (cTrn > 0)
            {
                sSql = "SELECT TOP 1 transitemfromwhoid FROM QL_trntransitemdtl WHERE transitemmstoid='" + tbl.transitemmstoid + "'";
                var transitemfromwhoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

                sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
                var mtrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", transitemfromwhoid);
                ViewBag.transitemfromwhoid = mtrwhoid;

                sSql = "SELECT TOP 1 transitemtowhoid FROM QL_trntransitemdtl WHERE transitemmstoid='" + tbl.transitemmstoid + "'";
                var transitemtowhoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

                sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
                var tomtrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", transitemtowhoid);
                ViewBag.transitemtowhoid = tomtrwhoid;
            }
            else
            {
                sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
                var mtrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
                ViewBag.transitemfromwhoid = mtrwhoid;

                sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
                var tomtrwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
                ViewBag.transitemtowhoid = tomtrwhoid;
            }
        }

        [HttpPost]
        public ActionResult InitDDLWHTo(int fromwhoid)
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='GUDANG' AND gnoid<>" + fromwhoid + " ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWH()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE gnflag='ACTIVE' AND gngroup='GUDANG' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetGudang(int whfrom)
        {
            List<QL_m05GN> objGudang = new List<QL_m05GN>();
            objGudang = db.QL_m05GN.Where(g => g.gngroup == "GUDANG" && g.gnflag.ToUpper() == "ACTIVE" && g.gnoid != whfrom).ToList();
            SelectList obgGudang = new SelectList(objGudang, "gnoid", "gndesc", 0);
            return Json(obgGudang);
        }

        [HttpPost]
        public ActionResult GetDataMatDetail(int whoid, int whtooid, string filteritem, string pidstart, string pidend)
        {
            List<listmat> tbl = new List<listmat>();

            var sPlus = "";
            if (!string.IsNullOrEmpty(filteritem))
            {
                sPlus += " AND i.itemdesc LIKE '%" + filteritem + "%' ";
            }
            if (!string.IsNullOrEmpty(pidstart) && !string.IsNullOrEmpty(pidend))
            {
                sPlus += " AND CAST(RIGHT(c.refno,4) AS INTEGER) BETWEEN '" + pidstart + "' AND '" + pidend + "' ";
            }

            sSql = "SELECT itemseq, itemoid, itemcode, refno, ISNULL(serialnumber,'') serialnumber, itemdesc, itemunitoid, itemunit, (qtystok - ISNULL((SELECT SUM(qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.itemoid AND mb.mtrwhoid=" + whoid + " AND ISNULL(mb.refno,'')=tbl.refno AND mb.flag='Post'),0.0)) qtystok, transitemfromwhoid, mtrwhdesc, transitemtowhoid, mtrwhtodesc, rabmstoid, projectname, itemtype FROM( ";
            sSql += " SELECT 0 itemseq, itemoid, itemcode, ISNULL(refno,'') refno, ISNULL(serialnumber,'') serialnumber, itemdesc, itemunitoid, g.gndesc itemunit, (SUM(qtyin-qtyout)) qtystok, (SELECT gndesc FROM QL_m05gn where gnoid = " + whoid + " and gngroup = 'GUDANG') mtrwhdesc, " + whoid + " transitemfromwhoid, (SELECT gndesc FROM QL_m05gn where gnoid = " + whtooid + " and gngroup = 'GUDANG') mtrwhtodesc, " + whtooid + " transitemtowhoid, ISNULL(c.rabmstoid,0) rabmstoid, ISNULL((SELECT r.projectname FROM QL_trnrabmst r WHERE r.rabmstoid=ISNULL(c.rabmstoid,0)),'') projectname, i.itemtype FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid INNER JOIN QL_conmat c ON c.refoid = i.itemoid WHERE i.itemtype IN('Barang','Rakitan','Asset') AND i.activeflag = 'ACTIVE' AND c.mtrwhoid="+ whoid +" " + sPlus + " GROUP BY itemoid, itemcode, ISNULL(refno,''), ISNULL(serialnumber,''), itemdesc, itemunitoid, g.gndesc, ISNULL(c.rabmstoid,0), i.itemtype /*HAVING (SUM(qtyin-qtyout))>0*/ ";
            sSql += " ) AS tbl WHERE (qtystok - ISNULL((SELECT SUM(qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.itemoid AND mb.mtrwhoid=" + whoid + " AND ISNULL(mb.refno,'')=tbl.refno AND mb.flag='Post'),0.0))>0 ORDER BY tbl.itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult FillDetailData(Guid? uid)
        {
            if (Session["QL_trntransitemdtl"+ uid] == null)
            {
                Session["QL_trntransitemdtl"+ uid] = new List<listpgdtl>();
            }

            List<listpgdtl> dataDtl = (List<listpgdtl>)Session["QL_trntransitemdtl"+ uid];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET: PG
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";
            InitAdvFilterIndex();
            return View();
        }

        private void InitAdvFilterIndex()
        {
            var filterddl = new SelectList(new Dictionary<string, string>() {["Draft"] = "No Draft",["Nomor"] = "No PG" }, "Key", "Value");
            ViewBag.filterddl = filterddl;

            ViewBag.filtertext = "";
            ViewBag.filterperiodfrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy");
            ViewBag.filterperiodto = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");

            var filterstatus = new SelectList(new Dictionary<string, string>() {["ALL"] = "ALL",["In Process"] = "In Process/Revised",["In Approval"] = "In Approval",["Post"] = "Post/Approved",["Closed"] = "Closed" }, "Key", "Value");
            ViewBag.filterstatus = filterstatus;
        }

        [HttpPost]
        public ActionResult getListDataTable(mdFilterList param)
        {
            sSql = "SELECT * FROM( Select tw.transitemmstoid, tw.transitemno, tw.transitemdate, tw.transitemmstnote, tw.transitemmststatus FROM ql_trntransitemmst tw WHERE tw.cmpcode='" + Session["CompnyCode"].ToString() + "' AND ISNULL(tw.transitemmstres1,'')<>'Reset') AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(param.filterperiodfrom) && !string.IsNullOrEmpty(param.filterperiodto))
            {
                sSql += " AND t.transitemdate >='" + param.filterperiodfrom + " 00:00:00' AND t.transitemdate <='" + param.filterperiodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(param.filtertext))
            {
                if (param.filterddl == "Draft") sSql += " AND t.transitemmstoid LIKE'%" + param.filtertext + "%'";
                else if (param.filterddl == "Nomor") sSql += " AND t.transitemno LIKE'%" + param.filtertext + "%'";
            }
            if (param.filterstatus != "ALL")
            {
                if (param.filterstatus == "In Process") sSql += " AND t.transitemmststatus IN('In Process','Revised')";
                else if (param.filterstatus == "In Approval") sSql += " AND t.transitemmststatus IN('In Approval')";
                else if (param.filterstatus == "Post") sSql += " AND t.transitemmststatus IN('Post','Approved')";
                else if (param.filterstatus == "Closed") sSql += " AND t.transitemmststatus IN('Closed')";
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PG/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string action = "";
            QL_trntransitemmst tblmst;
            if (id == null)
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = new QL_trntransitemmst();
                tblmst.cmpcode = CompnyCode;
                tblmst.transitemmstoid = ClassFunction.GenerateID("QL_trntransitemmst");
                tblmst.transitemno = generateCode();
                tblmst.transitemmststatus = "In Process"; 
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();
                tblmst.transitemdate = ClassFunction.GetServerTime();
                tblmst.transdocdate = ClassFunction.GetServerTime();
                tblmst.updtime = ClassFunction.GetServerTime();
                action = "Create";

                Session["QL_trntransitemdtl"+ ViewBag.uid] = null;
            }
            else
            {
                ViewBag.uid = Guid.NewGuid();
                tblmst = db.QL_trntransitemmst.Find(CompnyCode, id);
                action = "Edit";

                sSql = "SELECT rd.transitemdtlseq, rd.itemoid, i.itemcode, refno, ISNULL(serialnumber,'') serialnumber, i.itemdesc, rd.transitemqty, rd.transitemunitoid, g.gndesc transitemunit ,rd.transitemdtlnote, transitemfromwhoid, (SELECT gndesc FROM QL_m05GN where gnoid = transitemfromwhoid and gngroup = 'GUDANG') mtrwhdesc, transitemtowhoid, (SELECT gndesc FROM QL_m05GN where gnoid = transitemtowhoid and gngroup = 'GUDANG') mtrwhtodesc, ISNULL((SELECT SUM(con.qtyin-con.qtyout) FROM QL_conmat con WHERE con.refoid=rd.itemoid AND con.mtrwhoid=rd.transitemfromwhoid AND ISNULL(con.refno,'')=ISNULL(rd.refno,'') AND ISNULL(con.serialnumber,'')=ISNULL(rd.serialnumber,'')),0.0) - ISNULL((SELECT SUM(qtyout) FROM QL_matbooking mb WHERE mb.refoid=rd.itemoid AND mb.mtrwhoid=rd.transitemfromwhoid AND ISNULL(mb.refno,'')=rd.refno AND ISNULL(mb.serialnumber,'')=rd.serialnumber AND mb.flag='Post'),0.0) qtystok, ISNULL(rd.transitemdtlres1,'') transitemdtlres1, ISNULL(rd.rabmstoid,0) rabmstoid, ISNULL((SELECT r.projectname FROM QL_trnrabmst r WHERE r.rabmstoid=ISNULL(rd.rabmstoid,0)),'') projectname FROM QL_trntransitemdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.transitemunitoid AND g.gngroup = 'SATUAN' WHERE rd.cmpcode='" + CompnyCode + "' AND rd.transitemmstoid=" + id + " ORDER BY rd.transitemdtlseq";
                Session["QL_trntransitemdtl"+ ViewBag.uid] = db.Database.SqlQuery<listpgdtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            InitDDL(tblmst);
            ViewBag.action = action;
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trntransitemmst tblmst, string action, string tglmst, Guid? uid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tblmst.transitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("transitemdate", "Format Tanggal Tidak Valid!!" + ex.ToString());                
            }           

            List<listpgdtl> dtDtl = (List<listpgdtl>)Session["QL_trntransitemdtl"+ uid];
            if (dtDtl == null)
                ModelState.AddModelError("", "Silahkan isi detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Silahkan isi detail data!");

            //Is Input Valid?                   
            if (string.IsNullOrEmpty(tblmst.transitemdocrefno))
                tblmst.transitemdocrefno = "";
            if (string.IsNullOrEmpty(tblmst.transitemmstnote))
                tblmst.transitemmstnote = "";
            if (string.IsNullOrEmpty(tblmst.transitemmstres1))
                tblmst.transitemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.transitemmstres2))
                tblmst.transitemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.transitemmstres3))
                tblmst.transitemmstres3 = "";
            if (string.IsNullOrEmpty(tblmst.transitemno))
                tblmst.transitemno = generateCode(); 
                         
            var servertime = ClassFunction.GetServerTime();
            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                       
                        if (dtDtl[i].transitemqty == 0)
                        {
                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transfer tidak boleh 0");
                        }
                        if (dtDtl[i].transitemqty.ToString() == "")
                        {
                            ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transfer tidak boleh Kosong");
                        }
                        
                        if (string.IsNullOrEmpty(dtDtl[i].transitemdtlnote))
                            dtDtl[i].transitemdtlnote = "";
                        if (string.IsNullOrEmpty(dtDtl[i].refno))
                            dtDtl[i].refno = "";
                        if (string.IsNullOrEmpty(dtDtl[i].serialnumber))
                            dtDtl[i].serialnumber = "";
                        if (string.IsNullOrEmpty(tblmst.transitemno))
                            tblmst.transitemno = "";
                        if (tblmst.transitemmststatus == "Post")
                        {
                            if (!ClassFunction.IsStockAvailable(CompnyCode, servertime, dtDtl[i].itemoid, dtDtl[i].transitemfromwhoid, dtDtl[i].transitemqty, dtDtl[i].refno, dtDtl[i].rabmstoid, dtDtl[i].serialnumber))
                            {
                                ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty Transfer tidak boleh lebih dari Stock Qty");
                            }
                            else
                            {                                
                                decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].itemoid);
                                dtDtl[i].transitemvalueidr = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dtDtl[i].transitemqty, sValue, "");
                            }
                        }
                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.transitemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tblmst.transitemmststatus = "In Process";
            }
            if (tblmst.transitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tblmst.transitemmststatus = "In Process";
                }
            }

            var mstoid = ClassFunction.GenerateID("QL_trntransitemmst");
            var dtloid = ClassFunction.GenerateID("QL_trntransitemdtl");
            var conmatoid = ClassFunction.GenerateID("QL_conmat");

            if (ModelState.IsValid)            
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.transitemmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trntransitemmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trntransitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trntransitemdtl.Where(a => a.transitemmstoid == tblmst.transitemmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trntransitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trntransitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            
                            tbldtl = new QL_trntransitemdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.transitemdtloid = dtloid++;
                            tbldtl.transitemmstoid = tblmst.transitemmstoid;
                            tbldtl.transitemdtlseq = i + 1;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.transitemfromwhoid = dtDtl[i].transitemfromwhoid;
                            tbldtl.transitemtowhoid = dtDtl[i].transitemtowhoid;
                            tbldtl.transitemqty = dtDtl[i].transitemqty;
                            tbldtl.transitemunitoid = dtDtl[i].transitemunitoid;
                            tbldtl.transitemdtlnote = dtDtl[i].transitemdtlnote;                           
                            tbldtl.refno = dtDtl[i].refno;
                            tbldtl.transitemvalueidr = dtDtl[i].transitemvalueidr;
                            tbldtl.transitemdtlres1 = dtDtl[i].transitemdtlres1;                           
                            tbldtl.transitemdtlres2 = "";                           
                            tbldtl.transitemdtlres3 = "";
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.rabmstoid = dtDtl[i].rabmstoid;
                            tbldtl.serialnumber = dtDtl[i].serialnumber;

                            db.QL_trntransitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tblmst.transitemmststatus == "Post")
                            {
                                var refname = "FINISH GOOD";
                                if (dtDtl[i].transitemdtlres1 == "Asset")
                                {
                                    refname = "FIXED ASSET";
                                }

                                // Insert QL_conmat OUT
                                db.QL_conmat.Add(ClassFunction.InsertConMat(tblmst.cmpcode, conmatoid++, "PGO", "QL_trntransitemdtl", tblmst.transitemmstoid, dtDtl[i].itemoid, refname, dtDtl[i].transitemfromwhoid, -dtDtl[i].transitemqty, "Pindah Gudang", "Pindah Gudang No. " + tblmst.transitemno, Session["UserID"].ToString(), dtDtl[i].refno, dtDtl[i].transitemvalueidr, 0, 0, null, tbldtl.transitemdtloid, dtDtl[i].rabmstoid, dtDtl[i].transitemvalueidr, dtDtl[i].serialnumber));
                                db.SaveChanges();

                                int rabmstoid_in = 0;
                                if (string.IsNullOrEmpty(tblmst.transitemmstres1))
                                {
                                    rabmstoid_in = dtDtl[i].rabmstoid;
                                }
                                // Insert QL_conmat IN
                                db.QL_conmat.Add(ClassFunction.InsertConMat(tblmst.cmpcode, conmatoid++, "PGI", "QL_trntransitemdtl", tblmst.transitemmstoid, dtDtl[i].itemoid, refname, dtDtl[i].transitemtowhoid, dtDtl[i].transitemqty, "Pindah Gudang", "Pindah Gudang No. " + tblmst.transitemno, Session["UserID"].ToString(), dtDtl[i].refno, dtDtl[i].transitemvalueidr, 0, 0, null, tbldtl.transitemdtloid, rabmstoid_in, dtDtl[i].transitemvalueidr, dtDtl[i].serialnumber));
                                db.SaveChanges();
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trntransitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_ID SET lastoid=" + conmatoid + " WHERE tablename='QL_conmat'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.transitemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.transitemmststatus = "In Process";
            }
            InitDDL(tblmst);
            ViewBag.action = action;
            return View(tblmst);
        }

        // POST: PG/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trntransitemmst tblmst = db.QL_trntransitemmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trntransitemdtl.Where(a => a.transitemmstoid == id);
                        db.QL_trntransitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trntransitemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        private string generateCode()
        {
            string sCode = "PAG/"+ ClassFunction.GetServerTime().ToString("yy") + "/" + ClassFunction.GetServerTime().ToString("MM") + "-";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(transitemno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trntransitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND transitemno LIKE '" + sCode + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT TOP 1 usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'";
            string profname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trntransitemmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            var rptname = "rptTransfer";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT * FROM (SELECT tm.cmpcode, '"+ CompnyName + "' [Business Unit], tm.transitemmstoid [ID Hdr], CAST(tm.transitemmstoid AS VARCHAR(10)) [Draft No.], transitemno [Transfer No.], transitemdate [Transfer Date], transitemdocrefno [Doc. Ref. No.], transitemmstnote [Header Note], transitemmststatus [Status], UPPER(tm.createuser) [Create User], tm.createtime [Create Time], (CASE transitemmststatus WHEN 'In Process' THEN '' ELSE UPPER(tm.upduser) END) [Post User], (CASE transitemmststatus WHEN 'In Process' THEN CAST('01/01/1900' AS DATETIME) ELSE UPPER(tm.updtime) END) [Post Time], transitemdtloid [ID Dtl], transitemdtlseq [No.], g1.gndesc [From WH], g2.gndesc [To WH], itemcode [Code], itemdesc [Description], transitemqty [Qty], g3.gndesc [Unit], transitemdtlnote [Detail Note], '' [Transfer To], '' [Address], 0 transitemmstcityOid, '' [City], td.refno [PID], ISNULL(td.serialnumber,'') [serialnumber] FROM QL_trntransitemmst tm INNER JOIN QL_trntransitemdtl td ON td.cmpcode=tm.cmpcode AND td.transitemmstoid=tm.transitemmstoid INNER JOIN QL_m05GN g1 ON g1.gnoid=transitemfromwhoid INNER JOIN QL_m05GN g2 ON g2.gnoid=transitemtowhoid INNER JOIN QL_mstitem m ON m.itemoid=td.itemoid INNER JOIN QL_m05GN g3 ON g3.gnoid=transitemunitoid WHERE tm.transitemmstoid IN(" + id +")) AS tblTransfer";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            report.SetParameterValue("MaterialType", "FINISH GOOD");
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PrintOutTransferBarang.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}