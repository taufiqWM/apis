﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class PBReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string sWhere = "";

        public PBReportController()
        {
            db.Database.CommandTimeout = 0;
        }


        [HttpPost]
        public ActionResult GetSuppData(string[] status, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, s.suppcode [Kode], s.suppname [Nama], s.suppaddr [Alamat] FROM QL_mstsupp s WHERE s.suppoid IN (SELECT mrm.suppoid FROM QL_trnmritemmst mrm WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mritemtype<>'Jasa'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND mrm.mritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY s.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string startdate, string enddate, string fdate, string suppcode)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, pom.poitemno [No. PO], CONVERT(varchar(20),pom.poitemdate, 103) [Tanggal PO], pom.poitemmstnote [Note] FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa'";                
                if (suppcode != "")
                {
                    string[] arr = suppcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY pom.poitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMRData(string[] status, string suppcode, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, mrm.mritemmstoid [No. Draft], mrm.mritemno [No. Penerimaan], CONVERT(varchar(20),mrm.mritemdate,103) [Tgl Penerimaan], mrm.nosjsupp [No. SJ Supp], CONVERT(varchar(10),mrm.tglsjsupp,103) [Tgl SJ Supp], mrm.mritemmstnote [Note] FROM QL_trnmritemmst mrm INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid WHERE mrm.cmpcode='" + CompnyCode + "' AND mrm.mritemtype<>'Jasa'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND mrm.mritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (suppcode != "")
                {
                    string[] arr = suppcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY mrm.mritemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string suppcode, string ddlnomor, string rabno, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode Barang], m.itemdesc [Nama Barang], g.gndesc [Satuan] FROM QL_mstitem m INNER JOIN QL_trnmritemdtl mrd ON mrd.itemoid=m.itemoid INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_m05GN g ON g.gnoid=mrd.mritemunitoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrm.mritemtype<>'Jasa'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND mrm.mritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (suppcode != "")
                {
                    string[] arr = suppcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (rabno != "")
                {
                    string[] arr = rabno.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string TextNomor, string DDLNomor, string TextPO, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptPBSumXls.rpt";
                else
                    rptfile = "rptPBSumPdf.rpt";
                rptname = "PB_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptPBDtlXls.rpt";
                else
                {
                    rptfile = "rptPBDtlPdf.rpt";
                }
                rptname = "PB_DETAIL";
            }
            
            var Dtl = "";
            var Join = "";

            sWhere = " WHERE mrm.cmpcode = '"+ CompnyCode +"' ";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sWhere += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sWhere += " AND mrm.mritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextSupp != "")
            {
                string[] arr = TextSupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND "+ DDLNomor +" IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }            
            if (DDLType == "Detail")
            {
                if (TextPO != "")
                {
                    string[] arr = TextPO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhere += " AND pom.poitemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhere += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (DDLType == "Detail")
            {
                //SELECT DETAIL
                Dtl += " , m.itemcode AS [Code], m.itemdesc AS [Material], mrd.mritemqty AS [MR Qty], mrd.refno AS [PID], ISNULL(mrd.serialnumber,'') AS [serialnumber], g.gndesc AS [Unit], mrd.mritemdtlnote AS [Detail Note], pod.poitemprice [Price], mrd.mritemqty*pod.poitemprice [Amount Netto] ";
                //JOIN DTL
                Join += " INNER JOIN QL_trnmritemdtl mrd ON mrd.cmpcode=mrm.cmpcode AND mrd.mritemmstoid=mrm.mritemmstoid INNER JOIN QL_trnpoitemdtl pod ON pod.poitemdtloid=mrd.podtloid INNER JOIN QL_mstitem m ON  m.itemoid=mrd.itemoid INNER JOIN QL_m05gn g ON g.gnoid=mrd.mritemunitoid ";
                //Select Pret
                Dtl += ", ISNULL(pretm.pretitemno,'') AS [PRet No.], ISNULL(pretm.pretitemdate,'') AS [PRet Date], pretitemqty AS [PRet Qty] ";
                //JOIN PRET
                Join += " LEFT JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode=mrd.cmpcode AND pretd.mritemmstoid=mrm.mritemmstoid AND pretd.mritemdtloid=mrd.mritemdtloid LEFT JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.pretitemmstoid IN (SELECT pretitemmstoid FROM QL_trnpretitemdtl pretd2 WHERE pretd2.cmpcode=mrd.cmpcode AND pretd2.mritemmstoid=mrm.mritemmstoid AND pretd2.mritemdtloid=mrd.mritemdtloid) ";
            }
            
            sSql = " SELECT mrm.mritemmstoid AS [OID], CONVERT(VARCHAR(10), mrm.mritemmstoid) [Draft No.], mrm.nosjsupp [No SJ], CONVERT(VARCHAR(10),mrm.tglsjsupp,103) [Tgl SJ], mrm.mritemno AS [MR No.], mrm.mritemdate AS [MR Date], mrm.mritemmststatus AS [Status], mrm.cmpcode AS [CmpCode], mrm.suppoid [Supplier Oid], s.suppcode [Supplier Code], s.suppname [Supplier Name], '" + CompnyCode + "' AS [BU Name], mrm.mritemmstnote AS [MR Note], mrm.createuser [Create User], mrm.createtime [Create Date], (CASE mrm.mritemmststatus WHEN 'In Process' THEN '' ELSE mrm.upduser END) AS [MRec UserPost] , (CASE mrm.mritemmststatus WHEN 'In Process' THEN CONVERT(DATETIME,'1/1/1900') ELSE mrm.updtime END) AS [MRec PostDate], (CASE mritemmststatus WHEN 'In Process' THEN '' ELSE UPPER(mrm.upduser) END) [Posting User], (CASE mritemmststatus WHEN 'In Process' THEN CONVERT(DATETIME, '01/01/1900') ELSE mrm.updtime END) [Posting Date], ISNULL((SELECT SUM(mrdx.mritemqty) FROM QL_trnmritemdtl mrdx WHERE mrdx.mritemmstoid=mrm.mritemmstoid),0.0) [Total Qty], ISNULL((SELECT SUM(mrdx.mritemqty * podx.poitemprice) FROM QL_trnmritemdtl mrdx INNER JOIN QL_trnpoitemdtl podx ON podx.poitemdtloid=mrdx.podtloid AND podx.itemoid=mrdx.itemoid WHERE mrdx.mritemmstoid=mrm.mritemmstoid),0.0) [Total HPP], pom.poitemno AS [PO No.], pom.poitemdate AS [PO Date], pom.approvaldatetime [PO App Date], '' AS [PO UserApp], (SELECT DISTINCT rabno FROM QL_trnrabmst where rabmstoid = pom.rabmstoid) [RAB No], (SELECT DISTINCT projectname FROM QL_trnrabmst where rabmstoid = pom.rabmstoid) [Project] " + Dtl + " FROM QL_trnmritemmst mrm INNER JOIN QL_mstsupp s ON s.suppoid=mrm.suppoid INNER JOIN QL_trnpoitemmst pom ON pom.cmpcode=mrm.cmpcode AND pom.poitemmstoid=mrm.pomstoid " + Join + " " + sWhere + " AND mrm.mritemtype<>'Jasa'";

            if (DDLType == "Detail")
            {
                sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }            
        }
    }
}