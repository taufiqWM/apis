﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class ISReportController : Controller
    {
        // GET: ISReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public ISReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }


        // GET: PReturnReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("ISReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLCurrency, string ReportType, string DDLType, string DDLMonth, string DDLYear)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "SUMMARY")
                rptfile = "crLabaRugi.rpt";
            else if (DDLType == "DETAIL")
                rptfile = "crLabaRugiDtl.rpt";
            else
                rptfile = "crLabaRugiXls.rpt";
            rptname = "INCOMESTATEMENT";

            if (DDLType == "TAHUNANSUM")
            {
                sSql = "DECLARE @cmpcode AS VARCHAR(10); DECLARE @tahun AS VARCHAR(10); DECLARE @currency AS VARCHAR(10); SET @cmpcode='" + CompnyCode + "'; SET @tahun='" + DDLYear + "'; SET @currency='" + DDLCurrency + "';";
                sSql += " SELECT grp1code, grp1desc, grp2code, grp2desc, acctgcode, acctgdesc, acctgdbcr, @currency currency, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr01)*(-1) ELSE SUM(amtbalanceusd01)*(-1) END Bulan_Jan, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr02)*(-1) ELSE SUM(amtbalanceusd02)*(-1) END Bulan_Feb, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr03)*(-1) ELSE SUM(amtbalanceusd03)*(-1) END Bulan_Mar, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr04)*(-1) ELSE SUM(amtbalanceusd04)*(-1) END Bulan_Apr, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr05)*(-1) ELSE SUM(amtbalanceusd05)*(-1) END Bulan_Mei, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr06)*(-1) ELSE SUM(amtbalanceusd06)*(-1) END Bulan_Jun, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr07)*(-1) ELSE SUM(amtbalanceusd07)*(-1) END Bulan_Jul, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr08)*(-1) ELSE SUM(amtbalanceusd08)*(-1) END Bulan_Ags, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr09)*(-1) ELSE SUM(amtbalanceusd09)*(-1) END Bulan_Sep, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr10)*(-1) ELSE SUM(amtbalanceusd10)*(-1) END Bulan_Okt, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr11)*(-1) ELSE SUM(amtbalanceusd11)*(-1) END Bulan_Nop, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr12)*(-1) ELSE SUM(amtbalanceusd12)*(-1) END Bulan_Des FROM(";
                sSql += " SELECT a.cmpcode, a.grp1code, a.grp1desc, a.grp2code, a.grp2desc, a.acctgcode, a.acctgdesc, a.acctgdbcr, CASE WHEN a.period='01' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr01, CASE WHEN a.period='01' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd01, CASE WHEN a.period='02' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr02, CASE WHEN a.period='02' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd02, CASE WHEN a.period='03' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr03, CASE WHEN a.period='03' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd03, CASE WHEN a.period='04' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr04, CASE WHEN a.period='04' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd04, CASE WHEN a.period='05' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr05, CASE WHEN a.period='05' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd05, CASE WHEN a.period='06' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr06, CASE WHEN a.period='06' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd06, CASE WHEN a.period='07' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr07, CASE WHEN a.period='07' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd07, CASE WHEN a.period='08' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr08, CASE WHEN a.period='08' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd08, CASE WHEN a.period='09' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr09, CASE WHEN a.period='09' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd09, CASE WHEN a.period='10' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr10, CASE WHEN a.period='10' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd10, CASE WHEN a.period='11' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr11, CASE WHEN a.period='11' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd11, CASE WHEN a.period='12' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr12, CASE WHEN a.period='12' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd12 FROM (";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname,'01' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'01' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '02' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'02' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '03' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'03' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '04' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'04' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '05' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'05' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '06' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'06' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '07' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'07' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '08' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'08' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '09' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'09' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '10' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'10' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '11' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'11' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
                sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '12' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'12' WHERE ISNULL(a1.acctgres1,'')='1'";
                sSql += " ) AS a ) AS b GROUP BY cmpcode, grp1code, grp1desc, grp2code, grp2desc, acctgcode, acctgdesc, acctgdbcr ORDER BY grp1code, grp2code, acctgcode";

                DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
                Dictionary<string, string> rptparam = new Dictionary<string, string>();
                rptparam.Add("BU", CompnyCode);
                rptparam.Add("Tahun", DDLYear);
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("ReportPath", "Accounting > Report > Income Statement > ");
                rptparam.Add("RptFile", rptfile);

                if (ReportType == "")
                {
                    this.HttpContext.Session["rptsource"] = dtRpt;
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    this.HttpContext.Session["rptparam"] = rptparam;
                    return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ReportDocument report = new ReportDocument();
                    report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                    report.SetDataSource(dtRpt);
                    if (rptparam.Count > 0)
                        foreach (var item in rptparam)
                            report.SetParameterValue(item.Key, item.Value);
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");

                }
            }
            else //Summary & Detail
            {
                System.Globalization.CultureInfo AppsCultureInfo = new System.Globalization.CultureInfo("en-US");
                var period = new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1).ToString("MMMM yyyy");
                Dictionary<string, object> rptparam = new Dictionary<string, object>();
                rptparam.Add("cmpcode", CompnyCode);
                rptparam.Add("periodawal", ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)));
                rptparam.Add("periodakhir", ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)));
                rptparam.Add("sPeriode", period);
                rptparam.Add("currency", DDLCurrency);
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("ReportPath", "Accounting > Report > Income Statement > ");
                rptparam.Add("RptFile", rptfile);

                if (ReportType == "")
                {
                    this.HttpContext.Session["rptsource"] = "";
                    this.HttpContext.Session["rptpaper"] = null;
                    this.HttpContext.Session["rptparam"] = rptparam;
                    return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ReportDocument report = new ReportDocument();
                    report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                    if (rptparam.Count > 0)
                        foreach (var item in rptparam)
                            report.SetParameterValue(item.Key, item.Value);
                    ClassProcedure.SetDBLogonForReport(report);
                    // report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    if (ReportType == "XLS")
                    {
                        Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                        stream.Seek(0, SeekOrigin.Begin);
                        report.Close(); report.Dispose();
                        return File(stream, "application/excel", rptname + ".xls");
                    }
                    else
                    {
                        Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                        stream.Seek(0, SeekOrigin.Begin);
                        report.Close(); report.Dispose();
                        return File(stream, "application/pdf", rptname + ".pdf");
                    }
                }
            }               
        }

        [HttpPost]
        public ActionResult GenerateExcel(string DDLBusinessUnit, string DDLCurrency, string ReportType, string DDLType, string DDLMonth, string DDLYear)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");

            var rptfile = ""; var rptname = "";
            rptfile = "crLabaRugiXls.rpt";
            rptname = "INCOMESTATEMENT";

            sSql = "DECLARE @cmpcode AS VARCHAR(10); DECLARE @tahun AS VARCHAR(10); DECLARE @currency AS VARCHAR(10); SET @cmpcode='" + CompnyCode + "'; SET @tahun='" + DDLYear + "'; SET @currency='" + DDLCurrency + "';";
            sSql += " SELECT grp1code, grp1desc, grp2code, grp2desc, acctgcode, acctgdesc, acctgdbcr, @currency currency, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr01)*(-1) ELSE SUM(amtbalanceusd01)*(-1) END Bulan_Jan, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr02)*(-1) ELSE SUM(amtbalanceusd02)*(-1) END Bulan_Feb, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr03)*(-1) ELSE SUM(amtbalanceusd03)*(-1) END Bulan_Mar, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr04)*(-1) ELSE SUM(amtbalanceusd04)*(-1) END Bulan_Apr, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr05)*(-1) ELSE SUM(amtbalanceusd05)*(-1) END Bulan_Mei, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr06)*(-1) ELSE SUM(amtbalanceusd06)*(-1) END Bulan_Jun, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr07)*(-1) ELSE SUM(amtbalanceusd07)*(-1) END Bulan_Jul, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr08)*(-1) ELSE SUM(amtbalanceusd08)*(-1) END Bulan_Ags, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr09)*(-1) ELSE SUM(amtbalanceusd09)*(-1) END Bulan_Sep, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr10)*(-1) ELSE SUM(amtbalanceusd10)*(-1) END Bulan_Okt, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr11)*(-1) ELSE SUM(amtbalanceusd11)*(-1) END Bulan_Nop, CASE WHEN @currency='IDR' THEN SUM(amtbalanceidr12)*(-1) ELSE SUM(amtbalanceusd12)*(-1) END Bulan_Des FROM(";
            sSql += " SELECT a.cmpcode, a.grp1code, a.grp1desc, a.grp2code, a.grp2desc, a.acctgcode, a.acctgdesc, a.acctgdbcr, CASE WHEN a.period='01' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr01, CASE WHEN a.period='01' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd01, CASE WHEN a.period='02' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr02, CASE WHEN a.period='02' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd02, CASE WHEN a.period='03' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr03, CASE WHEN a.period='03' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd03, CASE WHEN a.period='04' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr04, CASE WHEN a.period='04' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd04, CASE WHEN a.period='05' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr05, CASE WHEN a.period='05' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd05, CASE WHEN a.period='06' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr06, CASE WHEN a.period='06' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd06, CASE WHEN a.period='07' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr07, CASE WHEN a.period='07' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd07, CASE WHEN a.period='08' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr08, CASE WHEN a.period='08' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd08, CASE WHEN a.period='09' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr09, CASE WHEN a.period='09' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd09, CASE WHEN a.period='10' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr10, CASE WHEN a.period='10' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd10, CASE WHEN a.period='11' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr11, CASE WHEN a.period='11' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd11, CASE WHEN a.period='12' THEN a.amtbalanceidr ELSE 0.0 END amtbalanceidr12, CASE WHEN a.period='12' THEN a.amtbalanceusd ELSE 0.0 END amtbalanceusd12 FROM (";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname,'01' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'01' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '02' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'02' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '03' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'03' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '04' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'04' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '05' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'05' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '06' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'06' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '07' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'07' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '08' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'08' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '09' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'09' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '10' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'10' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '11' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'11' WHERE ISNULL(a1.acctgres1,'')='1' UNION ALL";
            sSql += " SELECT @cmpcode cmpcode, (CASE a1.acctgcode WHEN '8' THEN '9' WHEN '9' THEN '8' ELSE a1.acctgcode END) grp1code, a1.acctgdesc grp1desc, ISNULL(a2.acctgcode, a1.acctgcode) grp2code, ISNULL(a2.acctgdesc, a1.acctgdesc) grp2desc, ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) acctgoid, ISNULL(a3.acctgcode, ISNULL(a2.acctgcode, a1.acctgcode)) acctgcode, ISNULL(a3.acctgdesc, ISNULL(a2.acctgdesc, a1.acctgdesc)) acctgdesc, ISNULL(a3.acctgdbcr, ISNULL(a2.acctgdbcr, a1.acctgdbcr)) acctgdbcr, ISNULL(c.amtdebit - c.amtcredit, 0.0) amtbalanceidr, ISNULL(c.amtdebitusd - c.amtcreditusd, 0.0) amtbalanceusd, (SELECT dv.divname FROM QL_mstdivision dv WHERE dv.divcode=@cmpcode) cmpname, '12' period FROM QL_mstacctg a1 LEFT JOIN QL_mstacctg a2 ON a1.cmpcode=a2.cmpcode AND LEFT(a2.acctgcode, LEN(a1.acctgcode))=a1.acctgcode AND ISNULL(a2.acctgres2, '')='2' LEFT JOIN QL_mstacctg a3 ON a2.cmpcode=a3.cmpcode AND LEFT(a3.acctgcode, LEN(a2.acctgcode))=a2.acctgcode AND a3.acctgoid NOT IN (SELECT DISTINCT b.acctggrp3 FROM QL_mstacctg b WHERE b.acctggrp3 IS NOT NULL AND b.cmpcode=a3.cmpcode) LEFT JOIN QL_crdgl c ON c.cmpcode=@cmpcode AND c.acctgoid=ISNULL(a3.acctgoid, ISNULL(a2.acctgoid, a1.acctgoid)) AND c.periodacctg=@tahun+'12' WHERE ISNULL(a1.acctgres1,'')='1'";
            sSql += " ) AS a ) AS b GROUP BY cmpcode, grp1code, grp1desc, grp2code, grp2desc, acctgcode, acctgdesc, acctgdbcr ORDER BY grp1code, grp2code, acctgcode";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            Dictionary<string, string> rptparam = new Dictionary<string, string>();
            rptparam.Add("BU", CompnyCode);
            rptparam.Add("Tahun", DDLYear);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("ReportPath", "Accounting > Report > Income Statement > ");
            rptparam.Add("RptFile", rptfile);

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dtRpt;
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dtRpt);
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                stream.Seek(0, SeekOrigin.Begin);
                report.Close(); report.Dispose();
                return File(stream, "application/excel", rptname + ".xls");

            }
        }
    }
}