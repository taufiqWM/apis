//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnapitemmst
    {
        public string cmpcode { get; set; }
        public int apitemmstoid { get; set; }
        public string periodacctg { get; set; }
        public Nullable<System.DateTime> apitemdate { get; set; }
        public System.DateTime apitemduedate { get; set; }
        public Nullable<System.DateTime> apitemttdate { get; set; }
        public string apitemno { get; set; }
        public int suppoid { get; set; }
        public Nullable<int> suppsalesoid { get; set; }
        public int apitempaytypeoid { get; set; }
        public int rabmstoid { get; set; }
        public int poitemmstoid { get; set; }
        public string apitemsuppfjno { get; set; }
        public Nullable<System.DateTime> apitemsuppfjdate { get; set; }
        public string apitemfaktur { get; set; }
        public int curroid { get; set; }
        public int rateoid { get; set; }
        public string apitemratetoidr { get; set; }
        public string apitemratetousd { get; set; }
        public int rate2oid { get; set; }
        public string apitemrate2toidr { get; set; }
        public string apitemrate2tousd { get; set; }
        public decimal apitemtotalamt { get; set; }
        public Nullable<decimal> apitemtotaldiscdtl { get; set; }
        public string apitemmstdisctype { get; set; }
        public Nullable<decimal> apitemmstdiscvalue { get; set; }
        public Nullable<decimal> apitemmstdiscamt { get; set; }
        public Nullable<decimal> apitemtotaldisc { get; set; }
        public decimal apitemtotalnetto { get; set; }
        public string apitemtaxtype { get; set; }
        public Nullable<decimal> apitemtaxvalue { get; set; }
        public Nullable<decimal> apitemtaxamt { get; set; }
        public Nullable<decimal> apitemothercost { get; set; }
        public decimal apitemgrandtotal { get; set; }
        public string apitemmstnote { get; set; }
        public string apitemmststatus { get; set; }
        public string apitemmstres1 { get; set; }
        public string apitemmstres2 { get; set; }
        public string apitemmstres3 { get; set; }
        public string createuser { get; set; }
        public System.DateTime createtime { get; set; }
        public string upduser { get; set; }
        public System.DateTime updtime { get; set; }
        public decimal apdpamt { get; set; }
        public string apitemtype { get; set; }
        public int rabmstoid_awal { get; set; }
        public Nullable<int> projectoid { get; set; }
    }
}
