﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FJReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public FJReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY rm.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, arm.cmpcode, arm.aritemno [FJ No], CONVERT(CHAR(10),arm.aritemdate,103) [FJ Date], c.custname [Customer], arm.aritemmstnote [FJ Note] FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY arm.aritemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT rm.custoid FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
      
        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomor, string TextNomor, string StartPeriod, string EndPeriod, string DDLPeriod, string DDLPono, string TextNomorPO)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnaritemdtl ard ON ard.itemoid=m.itemoid INNER JOIN QL_trnaritemmst arm ON ard.aritemmstoid=arm.aritemmstoid INNER JOIN QL_trnrabmst rm ON rm.cmpcode=rd.cmpcode AND rm.rabmstoid=arm.rabmstoid INNER JOIN QL_m05GN g ON g.gnoid=ard.aritemunitunitoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    var filter = "arm.aritemdate";
                    if (DDLPeriod == "[Upd Date]")
                    {
                        filter = "arm.updtime";
                    }
                    sSql += " AND " + filter + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomorPO != "")
                {
                    var filter = "arm.aritemno";
                    if (DDLPono == "[armstoid]")
                    {
                        filter = "arm.aritemmstoid";
                    }
                    string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + filter + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO,string DDLPono)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptARSumPdf.rpt";
                rptname = "FJ_SUMMARY";
            }
            else if (DDLType == "Summary2")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptAR_ReportPPNKXls.rpt";
                }
                else
                {
                    rptfile = "rptAR_ReportPPNKPdf.rpt";
                }
                rptname = "PPNKELUARAN_SUMMARY";
            }
            else if (DDLType == "Summary3")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptAR_ReportBPPajakXls.rpt";
                }
                else
                {
                    rptfile = "rptAR_ReportBPPajakPdf.rpt";
                }
                rptname = "BPPajak_SUMMARY";
            }
            else if (DDLType == "Summary Status Amount")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptARStatusAmtSumXls.rpt";
                }
                else
                {
                    rptfile = "rptARStatusAmtSumPdf.rpt";
                }
                rptname = "LAP_PENJUALAN_SUMMARY";
            }
            else if (DDLType == "Summary Outstanding FJ")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptAROSSumXls.rpt";
                }
                else
                {
                    rptfile = "rptAROSSumPdf.rpt";
                }
                rptname = "LAP_PIUTANG_SUMMARY";
            }
            else if (DDLType == "CSV")
            {
                rptfile = "rptARTrn_CSV.rpt";
                rptname = "GenerateFakturCsvReport";
            }
            else
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptARDtlXls.rpt";
                }
                else
                {
                    rptfile = "rptARDtlPdf.rpt";
                }
                rptname = "FJ_DETAIL";
            }

            if (DDLType == "CSV")
            {
                sSql = "select m_company_id , cmpcode ,m_company_name ,m_depo_id ,m_depo_name ,armstoid ,draftno ,arno ,ardate ,armstnote ,armststatus,custoid ,paymenttype ,mstdisctype ,armstdiscvalue ,armstdiscamt ,artotalnetto ,artaxtype ,artaxvalue ,artaxamt ,ardeliverycost ,arothercost ,argrandtotal ,ardtloid ,aroid ,[Code],[Description] ,Qty,disc,dtlnetto ,totaldtlnetto ,[Disc Promo],[Disc Promo Per Item] ,[Netto Item] ,Netto ,dtlppn , PriceNew PriceNew  ,PriceOld ,alamatfaktur ,custfaktur ,npwpfaktur ,custname ,bulan_pajak ,tahun_pajak ,npwp ,namanpwp ,alamatnpwp ,zero ,kodejenis ,fgpengganti ,nofaktur ,dpamt ,dpamt_ppn ,case when PriceNew = 0  then 100 else  trndiscvalue end trndiscvalue ,referensi , case when owner_name = '' then case when custfaktur = '' then custname else custfaktur end else owner_name end owner_name ,owner_nik ,owner_street ,owner_blok ,owner_nome_no ,owner_rt ,owner_rw ,owner_kelurahan ,owner_kecamatan ,owner_kabupaten ,owner_propinsi ,owner_kode_pos ,owner_phone  from( SELECT 0 m_company_id, co.divcode AS cmpcode, co.divname m_company_name, 0 m_depo_id, '' m_depo_name, arm.aritemmstoid AS armstoid, arm.aritemmstoid AS draftno, arm.aritemno AS arno, arm.aritemdate AS ardate, arm.aritemmstnote AS armstnote, arm.aritemmststatus AS armststatus, arm.custoid AS custoid, p.gndesc AS paymenttype, '' mstdisctype, 0.0 AS armstdiscvalue, 0.0 AS armstdiscamt, arm.aritemtotalamt artotalnetto, '' artaxtype, ard.aritemdtltaxvalue AS artaxvalue, arm.aritemtaxamt AS artaxamt, 0.0 AS ardeliverycost, 0.0 AS arothercost, aritemgrandtotal argrandtotal, ard.aritemdtloid AS ardtloid, arm.aritemmstoid AS aroid, REPLACE(REPLACE(i.itemcode, '.', ''), '-', '') AS[Code], i.itemdesc AS[Description], ard.aritemqty AS[Qty], 0.0 AS[Disc], ROUND(ard.aritemdtlnetto, 0) AS dtlnetto, arm.aritemgrandtotal AS[totaldtlnetto]/*Diskon asli include tax*/, 0.0[Disc Promo]/*Dianggap diskon sebelum tax*/, 0.0 AS[Disc Promo Per Item]/*Nilai transaksi yg include tax*/, ard.aritemdtlnetto AS[Netto Item]/*DPP*/, ROUND(ard.aritemdtlnetto / 1.1, 2) AS[Netto]/*PPN*/, ROUND((round(ard.aritemdtlnetto / 1.1, 2) * 0.1), 2, 1) AS[dtlppn]/*Karena harga asli include tax, harga sebelum pajak dihitung dari (DPP+DISKON sblm tax)/Qty*/, ard.aritemprice  AS[PriceNew], ard.aritemprice AS[PriceOld], c.custaddr AS alamatfaktur, custnpwp AS custfaktur, REPLACE(REPLACE(c.custnpwpno, '.', ''), '-', '') AS npwpfaktur, c.custname custname, MONTH(arm.aritemdate)bulan_pajak, YEAR(arm.aritemdate) tahun_pajak, REPLACE(REPLACE(c.custnpwpno, '.', ''), '-', '') as npwp, c.custnpwp as namanpwp, c.custnpwpaddr as alamatnpwp, 0 as zero, CAST(LEFT(arm.aritemfaktur,2) AS VARCHAR(10))  AS kodejenis, '0' AS fgpengganti, REPLACE(REPLACE(fa.fakturno, '.', ''), '-', '') AS nofaktur, 0 AS dpamt, 0 AS dpamt_ppn, 0 AS trndiscvalue, arm.aritemno referensi/*Tambahan e Fahmi*/, c.custname owner_name, c.custcode owner_nik, c.custaddr owner_street, '' owner_blok, '' owner_nome_no, '' owner_rt, '' owner_rw, '' owner_kelurahan, '' owner_kecamatan, '' owner_kabupaten, '' owner_propinsi, c.custpostcode owner_kode_pos, c.custphone1 owner_phone  FROM QL_trnaritemmst arm INNER JOIN QL_trnaritemdtl ard ON ard.aritemmstoid = arm.aritemmstoid INNER JOIN QL_mstdivision co ON co.divcode = arm.cmpcode INNER JOIN QL_mstitem i ON i.itemoid = ard.itemoid INNER JOIN QL_m05GN p ON p.gnoid = arm.aritempaytypeoid AND p.gngroup = 'PAYMENT TERM' INNER JOIN QL_mstcust c ON c.custoid = arm.custoid INNER JOIN QL_mstfa fa ON fa.faoid = arm.faoid WHERE arm.aritemmststatus IN('Post', 'Approved', 'Closed') ";
                var filter = "arm.aritemdate";
                if (DDLPeriod == "[Upd Date]")
                {
                    filter = "arm.updtime";
                }
                sSql += " AND " + filter + " >=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + " <=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND arm.aritemno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += " ) dx";
            }
            else
            {
                var Slct = ""; var Join = ""; var Slct2 = ""; var Join2 = "";
                if (DDLType == "Summary Status Amount" || DDLType == "Summary Outstanding FJ")
                {
                    if (DDLType == "Summary Status Amount")
                    {
                        sSql = "SELECT * FROM ( SELECT som.cmpcode [cmpcode], som.soitemmstoid, som.soitemno, som.soitemdate , arm.arupdtime [Upd Date], som.soitemmststatus [Status], som.rabmstoid, rm.rabno [No. RAB], rm.projectname, rm.rabdate, rm.rabtype, rm.rabtype2, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.salesoid),'') sales, ISNULL((SELECT arm.aritemfaktur + '.' + us.fakturno FROM QL_mstfa us WHERE us.faoid=ISNULL(arm.faoid,'')),'') fakturno, c.custcode [Custcode], c.custname, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN som.soitemgrandtotalamt ELSE 0.0 END soitemgrandtotalamt, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN ISNULL(arm.aritemtaxamt, 0.0) ELSE 0.0 END soitemtotaltaxamt, ISNULL(arm.aritemmstoid,0) [armstoid], ISNULL(arm.aritemno, '') [AR No.], ISNULL(arm.aritemdate, '') aritemdate, ISNULL(arm.ardate, '') ardate, ISNULL(arm.arupdtime, '') arupdtime, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN ISNULL(arm.aritemgrandtotal, 0.0) ELSE 0.0 END aritemgrandtotal , ISNULL(shm.shipmentitemno, '') shipmentitemno, ISNULL(shm.shipmentitemdate, '') shipmentitemdate, ISNULL(cb.cashbankno, '') cashbankno, ISNULL(cb.cashbankdate, '') cashbankdate, ISNULL((SELECT TOP 1 CONVERT(CHAR(10),ard2.aritemdtl2date,103) FROM QL_trnaritemdtl2 ard2 WHERE ard2.aritemmstoid=arm.aritemmstoid AND ard2.aritemdtl2type='PPH 22'),'') tglpph, ISNULL((SELECT TOP 1 ard2.ntpnno FROM QL_trnaritemdtl2 ard2 WHERE ard2.aritemmstoid=arm.aritemmstoid AND ard2.aritemdtl2type='PPH 22'),'') ntpnno, ISNULL((SELECT TOP 1 ard2.aritemdtl2amt FROM QL_trnaritemdtl2 ard2 WHERE ard2.aritemmstoid=arm.aritemmstoid AND ard2.aritemdtl2type='PPH 22'),0.0) pphamt, isnull((select x.gndesc from QL_m05GN x where x.gnoid=isnull(arm.aritempaytypeoid,0)),'') tempo FROM QL_trnsoitemmst som LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = som.custoid LEFT JOIN( SELECT arm.cmpcode, arm.aritemmstoid, arm.aritemno, CONVERT(CHAR(10), arm.aritemdate, 103) aritemdate, arm.aritemdate ardate, arm.updtime arupdtime, arm.aritemtaxamt, arm.aritemgrandtotal, arm.somstoid, arm.rabmstoid, arm.faoid, arm.aritemmststatus, arm.aritemfaktur, arm.aritempaytypeoid FROM QL_trnaritemmst arm ) AS arm ON arm.somstoid = som.soitemmstoid AND arm.rabmstoid = rm.rabmstoid LEFT JOIN( SELECT shm.cmpcode, shm.shipmentitemmstoid, shm.shipmentitemno, CONVERT(CHAR(10), shm.shipmentitemdate, 103) shipmentitemdate, shm.soitemmstoid, shm.rabmstoid FROM QL_trnshipmentitemmst shm ) AS shm ON shm.soitemmstoid = som.soitemmstoid AND shm.rabmstoid = rm.rabmstoid LEFT JOIN( SELECT cb.cmpcode, cb.cashbankno, CONVERT(CHAR(10), cb.cashbankdate, 103) cashbankdate, arm.somstoid, arm.rabmstoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar py ON py.cashbankoid = cb.cashbankoid INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid = py.refoid WHERE py.reftype = 'QL_trnaritemmst' UNION ALL SELECT cb.cmpcode, cb.cashbankno, CONVERT(CHAR(10), cb.cashbankdate, 103) cashbankdate, arm.somstoid, arm.rabmstoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpaydp py ON py.cashbankoid = cb.cashbankoid INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid = py.refoid WHERE py.reftype = 'QL_trnaritemmst' ) AS cb ON cb.somstoid = som.soitemmstoid AND cb.rabmstoid = rm.rabmstoid WHERE som.cmpcode='" + CompnyCode + "')AS ar WHERE [cmpcode]='" + CompnyCode + "'";
                    }
                    if (DDLType == "Summary Outstanding FJ")
                    {
                        sSql = "SELECT * FROM ( SELECT arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], arm.updtime AS [Upd Date], CURRENT_TIMESTAMP [AR Date Take Giro], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], arm.aritemratetoidr [Rate IDR], cast(arm.aritemratetousd as Float) [Rate USD], arm.aritemtotalamt [AR Total Amt], arm.aritemtotaldisc [AR Total Disc], arm.aritemtotalnetto [Total Netto], arm.aritemtaxtype [Tax Type], arm.aritemtaxvalue [AR Tax Value], arm.aritemtaxamt [AR Tax Amount], CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=00 THEN ISNULL(arm.aritemgrandtotal, 0.0) ELSE 0.0 END [AR Grand Total], ISNULL((SELECT divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode),'') AS [BU Name], arm.aritemmstnote AS [AR Header Note], ISNULL(arm.aritemmstres3,'') AS [Invoice No], arm.createuser [AR Create User], arm.createtime [AR Create Time], '' AS [AR User Approval] , CURRENT_TIMESTAMP AS [AR ApprovalDate], aritemfaktur [No. Faktur], ISNULL(som.soitemdate,'') [Date SO], ISNULL(som.soitemno,'') [No SO], ISNULL(som.soitemgrandtotalamt,0.0) [Total SO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemno FROM (SELECT DISTINCT CAST (cbx.shipmentitemno AS VARCHAR) shipmentitemno FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ NO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.shipmentitemdate,103) shipmentitemdate FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ Date], ISNULL(STUFF((SELECT ', '+ft.cashbankno FROM (SELECT DISTINCT CAST (cbx.cashbankno AS VARCHAR) cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND CASE LEFT(cbx.cashbanktype,2) WHEN 'BB' THEN cbx.cashbankduedate ELSE cbx.cashbankdate END<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) UNION ALL SELECT DISTINCT CAST (cbx.cashbankno AS VARCHAR) cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpaydp ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbankdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)) ft FOR XML PATH('')), 1, 1, ''),'') [Bayar NO], ISNULL(STUFF((SELECT ', '+ft.cashbankdate FROM (SELECT DISTINCT CONVERT(CHAR(10),CASE LEFT(cbx.cashbanktype,2) WHEN 'BB' THEN cbx.cashbankduedate ELSE cbx.cashbankdate END,103) cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND CASE LEFT(cbx.cashbanktype,2) WHEN 'BB' THEN cbx.cashbankduedate ELSE cbx.cashbankdate END<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) UNION ALL  SELECT DISTINCT CONVERT(CHAR(10),cbx.cashbankdate,103) cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpaydp ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbankdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)) ft FOR XML PATH('')), 1, 1, ''),'') [Bayar Date], ISNULL((SELECT SUM(ardx.payaramt) FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbankstatus IN('Post','Closed') AND CASE LEFT(cbx.cashbanktype,2) WHEN 'BB' THEN cbx.cashbankduedate ELSE cbx.cashbankdate END<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)),0.0)+ISNULL((SELECT SUM(ardx.paydpamt) FROM QL_trncashbankmst cbx INNER JOIN QL_trnpaydp ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbankstatus IN('Post','Closed') AND cbx.cashbankdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)),0.0) [Bayar Amt], arm.aritemduedate [duedate], ISNULL(rabno,'') [No. RAB], ISNULL(rm.rabdate,'') [RAB Date], ISNULL(rm.projectname,'') [Project], ISNULL(us.usname,'') [Sales] FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid LEFT JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid WHERE arm.cmpcode='" + CompnyCode + "')AS ar WHERE [CmpCode]='" + CompnyCode + "'";
                    }
                }
                else if (DDLType == "Summary2") //PPN
                {
                    sSql = "SELECT * FROM ( SELECT (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode) [BU Name], arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], arm.updtime AS[Upd Date], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], som.soitemdate [Date SO], som.soitemno [No SO], rabno [No. RAB], rm.rabdate [RAB Date], rm.projectname [Project], us.usname [Sales], c.custnpwpno, arm.aritemfaktur+'-'+fa.fakturno fakturno, CASE WHEN arm.aritemtaxtype='TAX' THEN 'PPN' ELSE 'NON PPN' END keterangan, MONTH(arm.aritemdate) masa, LEFT(arm.aritemfaktur,2) kode, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.aritemtotalamt ELSE 0.0 END DPP, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.aritemtaxamt ELSE 0.0 END PPN, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtotalamt+arm.aritemtaxamt) ELSE 0.0 END Total, CASE MONTH(arm.aritemdate) WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END jan, CASE MONTH(arm.aritemdate) WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END feb, CASE MONTH(arm.aritemdate) WHEN 3 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END mar, CASE MONTH(arm.aritemdate) WHEN 4 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END apr, CASE MONTH(arm.aritemdate) WHEN 5 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END mei, CASE MONTH(arm.aritemdate) WHEN 6 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END jun, CASE MONTH(arm.aritemdate) WHEN 7 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END jul, CASE MONTH(arm.aritemdate) WHEN 8 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END ags, CASE MONTH(arm.aritemdate) WHEN 9 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END sep, CASE MONTH(arm.aritemdate) WHEN 10 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END okt, CASE MONTH(arm.aritemdate) WHEN 11 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END nop, CASE MONTH(arm.aritemdate) WHEN 12 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt WHERE arrt.aritemmstoid=ISNULL(arm.aritemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN (arm.aritemtaxamt) ELSE 0.0 END) ELSE 0.0 END desm FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid=rm.rabmstoid INNER JOIN QL_m01US us ON us.usoid=rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid INNER JOIN QL_mstfa fa ON fa.faoid=arm.faoid  ";
                    sSql += " UNION ALL  SELECT (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode) [BU Name], arm.dparoid AS [armstoid], (CASE arm.dparno WHEN '' THEN CONVERT(VARCHAR(10), arm.dparoid) ELSE arm.dparno END) AS [Draft No.], arm.dparno AS [AR No.], arm.dpardate AS [AR Date], arm.updtime AS[Upd Date], arm.dparstatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], som.soitemdate [Date SO], som.soitemno [No SO], rabno [No. RAB], rm.rabdate [RAB Date], rm.projectname [Project], us.usname [Sales], c.custnpwpno, arm.dparfaktur+'-'+fa.fakturno fakturno, CASE WHEN c.custtaxable=1 THEN 'PPN' ELSE 'NON PPN' END keterangan, MONTH(arm.dpardate) masa, LEFT(arm.dparfaktur,2) kode, 0 DPP, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END PPN, CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END Total, CASE MONTH(arm.dpardate) WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END jan, CASE MONTH(arm.dpardate) WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END feb, CASE MONTH(arm.dpardate) WHEN 3 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END mar, CASE MONTH(arm.dpardate) WHEN 4 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END apr, CASE MONTH(arm.dpardate) WHEN 5 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END mei, CASE MONTH(arm.dpardate) WHEN 6 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END jun, CASE MONTH(arm.dpardate) WHEN 7 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END jul, CASE MONTH(arm.dpardate) WHEN 8 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END ags, CASE MONTH(arm.dpardate) WHEN 9 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END sep, CASE MONTH(arm.dpardate) WHEN 10 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END okt, CASE MONTH(arm.dpardate) WHEN 11 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END nop, CASE MONTH(arm.dpardate) WHEN 12 THEN (CASE WHEN (SELECT COUNT(*) FROM QL_trnarretitemmst arrt INNER JOIN QL_trnaritemmst arx ON arx.aritemmstoid=arrt.aritemmstoid WHERE arx.somstoid=ISNULL(som.soitemmstoid,0) AND arrt.arretitemmststatus IN('Approved','Post','Closed'))<=0 THEN arm.dparamt ELSE 0 END) ELSE 0.0 END desm FROM QL_trndpar arm INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=c.custpaymentoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid AND arm.soreftype='QL_trnsoitemmst' INNER JOIN QL_mstfa fa ON fa.faoid=ISNULL(arm.faoid,0)  LEFT JOIN QL_trnrabmst rm ON som.rabmstoid=rm.rabmstoid INNER JOIN QL_m01US us ON us.usoid=rm.salesoid )AS ar WHERE [CmpCode]='" + CompnyCode + "'";
                }
                else if (DDLType == "Summary3") //Bukti Potong
                {
                    sSql = "SELECT * FROM ( SELECT (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode) [BU Name], arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], arm.updtime AS[Upd Date], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], som.soitemdate [Date SO], som.soitemno [No SO], rabno [No. RAB], rm.rabdate [RAB Date], rm.projectname [Project], us.usname [Sales], c.custnpwpno, arm.aritemfaktur+'-'+fa.fakturno fakturno, arm.aritemtotalamt DPP, arm.aritemtaxamt PPN, arm.aritemgrandtotal TOTAL, ISNULL((SELECT rd.aritemdtl2amt FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid=arm.aritemmstoid AND rd.aritemdtl2type='PPN'),0.0) AlokasiPPN, ISNULL((SELECT rd.aritemdtl2amt FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid=arm.aritemmstoid AND rd.aritemdtl2type='PPH 22'),0.0) AlokasiPPH22, ISNULL((SELECT rd.aritemdtl2amt FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid=arm.aritemmstoid AND rd.aritemdtl2type='PPH 23'),0.0) AlokasiPPH23, ISNULL((SELECT rd.ntpnno FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPN'),'') nomorPPN, ISNULL((SELECT rd.aritemdtl2jenis FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPN' AND rd.aritemdtl2jenis = 'ASLI'),'') asliPPN, ISNULL((SELECT rd.aritemdtl2jenis FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPN' AND rd.aritemdtl2jenis = 'COPY'),'') copyPPN, ISNULL((SELECT rd.ntpnno FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPH 22'),'') nomorPPH22, ISNULL((SELECT rd.aritemdtl2jenis FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPH 22' AND rd.aritemdtl2jenis = 'ASLI'),'') asliPPH22, ISNULL((SELECT rd.aritemdtl2jenis FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPH 22' AND rd.aritemdtl2jenis = 'COPY'),'') copyPPh22, ISNULL((SELECT rd.ntpnno FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPH 23'),'') nomorPPH23, ISNULL((SELECT rd.aritemdtl2jenis FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPH 23' AND rd.aritemdtl2jenis = 'ASLI'),'') asliPPH23, ISNULL((SELECT rd.aritemdtl2jenis FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid AND rd.aritemdtl2type = 'PPH 23' AND rd.aritemdtl2jenis = 'COPY'),'') copyPPh23, ISNULL((SELECT TOP 1 CONVERT(CHAR(10), rd.aritemdtl2date, 103) FROM QL_trnaritemdtl2 rd WHERE rd.aritemmstoid = arm.aritemmstoid ORDER BY rd.aritemdtl2date DESC),'') tglupd  FROM QL_trnaritemmst arm INNER JOIN QL_mstcust c ON c.custoid = arm.custoid INNER JOIN QL_m05GN g ON g.gnoid = arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid = arm.curroid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid = rm.rabmstoid INNER JOIN QL_m01US us ON us.usoid = rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = arm.somstoid INNER JOIN QL_mstfa fa ON fa.faoid = arm.faoid )AS ar WHERE [CmpCode]='" + CompnyCode + "'";
                }
                else
                {
                    if (DDLType == "Detail")
                    {
                        Slct += " ,ard.aritemdtloid [AR Dtl Oid], ard.aritemdtlseq [Seq], ard.itemoid [Mat Oid], m.itemcode [Mat Code] , m.itemdesc [Mat Longdesc], aritemqty [Qty] , g2.gndesc [Unit], aritemprice [Price], aritemdtlamt [Detail Amount], aritemdtldisctype [Disc Type], aritemdtldiscvalue [Disc Value], aritemdtldiscamt [Disc Amount], '' [Disc Type 2], 0.00 [Disc Value 2], 0.00 [Disc Amount 2], aritemdtlnetto [Dtl Netto], aritemdtlnote [AR Detail Note], 1 AS [Convert Qty], ((aritemqty + 0.00) * 1) AS [Qty Dtl], '' AS [Unit Dtl] ";
                        Join += " INNER JOIN QL_trnaritemdtl ard ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid INNER JOIN QL_mstitem m ON m.itemoid=ard.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid=ard.aritemunitoid ";

                        Slct += ", shm.shipmentitemmstoid [Shipment Oid],shipmentitemno [Shipment No.], shipmentitemdate [Shipment Date], shipmentitemvalueidr [PriceSJIDR], shipmentitemvalueusd [PriceSJUSD], '' [Shipment Container No.], ISNULL((SELECT usname FROM QL_m01US dr WHERE dr.usoid=shm.driveroid),'') [Shipment Driver], (SELECT vhcdesc FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Vehicle], (SELECT vhcno FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Police No.], shm.shipmentitemmststatus [Shipment Status], arm.aritemfaktur [Shipment SJ Pajak], '' [Shipment Seal No.], shipmentitemmstnote[Shipment Header Note], shm.createuser[Shipment Create User], shm.createtime[Shipment Create Datetime], '' AS[Shipment User Approval] , CURRENT_TIMESTAMP AS[Shipment ApprovalDate] ";

                        Join += " INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentitemmstoid=ard.shipmentitemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid ";

                        Slct += " , '' AS [Divisi] ,som.soitemmstoid [SO ID], soitemno [SO No.], soitemdate [SO Date], ((SELECT gndesc FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnoid=soitempaytypeoid)) [SO Payment Type] , '' [SO Price Type], som.soitemtype [SO Local/Exp], '' AS [SO Direct Cust PO No.], 0 AS [Payment Method], CURRENT_TIMESTAMP [SO ETD], '' [Invoice Value],'' [SO Account No.], '' [Port Of Ship], '' [Port Of Dicharge], soitemmstnote [SO Header Note], soitemmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], CURRENT_TIMESTAMP AS [App User SO], CURRENT_TIMESTAMP AS [App Date SO]";

                        Slct2 += " ,ard.aritemdtloid [AR Dtl Oid], ard.aritemdtlseq [Seq], ard.itemoid [Mat Oid], m.jasacode [Mat Code] , m.jasadesc [Mat Longdesc], aritemqty [Qty] , g2.gndesc [Unit], aritemprice [Price], aritemdtlamt [Detail Amount], aritemdtldisctype [Disc Type], aritemdtldiscvalue [Disc Value], aritemdtldiscamt [Disc Amount], '' [Disc Type 2], 0.00 [Disc Value 2], 0.00 [Disc Amount 2], aritemdtlnetto [Dtl Netto], aritemdtlnote [AR Detail Note], 1 AS [Convert Qty], aritemqty AS [Qty Dtl], '' AS [Unit Dtl] ";
                        Join2 += " INNER JOIN QL_trnaritemdtl ard ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid INNER JOIN QL_mstjasa m ON m.jasaoid=ard.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid=ard.aritemunitoid ";

                        Slct2 += ", shm.shipmentitemmstoid [Shipment Oid],shipmentitemno [Shipment No.], shipmentitemdate [Shipment Date], shipmentitemvalueidr [PriceSJIDR], shipmentitemvalueusd [PriceSJUSD], '' [Shipment Container No.], ISNULL((SELECT usname FROM QL_m01US dr WHERE dr.usoid=shm.driveroid),'') [Shipment Driver], (SELECT vhcdesc FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Vehicle], (SELECT vhcno FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Police No.], shm.shipmentitemmststatus [Shipment Status], arm.aritemfaktur [Shipment SJ Pajak], '' [Shipment Seal No.], shipmentitemmstnote[Shipment Header Note], shm.createuser[Shipment Create User], shm.createtime[Shipment Create Datetime], '' AS[Shipment User Approval] , CURRENT_TIMESTAMP AS[Shipment ApprovalDate] ";

                        Join2 += " INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentitemmstoid=ard.shipmentitemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid ";

                        Slct2 += " , '' AS [Divisi] ,som.soitemmstoid [SO ID], soitemno [SO No.], soitemdate [SO Date], ((SELECT gndesc FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnoid=soitempaytypeoid)) [SO Payment Type] , '' [SO Price Type], som.soitemtype [SO Local/Exp], '' AS [SO Direct Cust PO No.], 0 AS [Payment Method], CURRENT_TIMESTAMP [SO ETD], '' [Invoice Value],'' [SO Account No.], '' [Port Of Ship], '' [Port Of Dicharge], soitemmstnote [SO Header Note], soitemmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], CURRENT_TIMESTAMP AS [App User SO], CURRENT_TIMESTAMP AS [App Date SO]";
                    }

                    sSql = "SELECT * FROM ( SELECT arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], arm.updtime AS[Upd Date], CURRENT_TIMESTAMP [AR Date Take Giro], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], arm.aritemratetoidr [Rate IDR], cast(arm.aritemratetousd as Float) [Rate USD], arm.aritemtotalamt [AR Total Amt], arm.aritemtotaldisc [AR Total Disc], arm.aritemtotalnetto [Total Netto], arm.aritemtaxtype [Tax Type], arm.aritemtaxvalue [AR Tax Value], arm.aritemtaxamt [AR Tax Amount], arm.aritemgrandtotal [AR Grand Total], ISNULL((SELECT divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode),'') AS [BU Name], arm.aritemmstnote AS [AR Header Note], ISNULL(arm.aritemmstres3,'') AS [Invoice No], arm.createuser [AR Create User], arm.createtime [AR Create Time], '' AS [AR User Approval] , CURRENT_TIMESTAMP AS [AR ApprovalDate], aritemfaktur [No. Faktur], som.soitemdate [Date SO], som.soitemno [No SO], som.soitemgrandtotalamt [Total SO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemno FROM (SELECT DISTINCT CAST (cbx.shipmentitemno AS VARCHAR) shipmentitemno FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ NO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.shipmentitemdate,103) shipmentitemdate FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ Date], ISNULL(STUFF((SELECT ', '+ft.cashbankno FROM (SELECT DISTINCT CAST (cbx.cashbankno AS VARCHAR) cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar NO], ISNULL(STUFF((SELECT ', '+ft.cashbankdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.cashbankdate,103) cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar Date], ISNULL((SELECT SUM(cbx.cashbankamt) FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM'),0.0) [Bayar Amt] " + Slct + ", ISNULL(rabno,'') [No.RAB], ISNULL(rm.rabdate,'') [RAB Date], ISNULL(rm.projectname,'') [Project], ISNULL(us.usname,'') [Sales] FROM QL_trnaritemmst arm " + Join + " INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemtype<>'Jasa'";
                    sSql += " /*UNION ALL  SELECT arm.aritemmstoid AS[armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS[Draft No.], arm.aritemno AS[AR No.], arm.aritemdate AS[AR Date], arm.updtime AS[Upd Date], CURRENT_TIMESTAMP[AR Date Take Giro], arm.aritemmststatus AS[Status], arm.cmpcode AS[CmpCode], arm.custoid[Custoid], c.custcode [Custcode], c.custname[Customer], g.gndesc[Payment Type], curr.currcode[Currency], arm.aritemratetoidr[Rate IDR], cast(arm.aritemratetousd as Float)[Rate USD], arm.aritemtotalamt[AR Total Amt], arm.aritemtotaldisc[AR Total Disc], arm.aritemtotalnetto[Total Netto], arm.aritemtaxtype[Tax Type], arm.aritemtaxvalue[AR Tax Value], arm.aritemtaxamt[AR Tax Amount], arm.aritemgrandtotal[AR Grand Total], ISNULL((SELECT divname FROM QL_mstdivision div WHERE div.divcode = arm.cmpcode),'') AS[BU Name], arm.aritemmstnote AS[AR Header Note], ISNULL(arm.aritemmstres3, '') AS[Invoice No], arm.createuser[AR Create User], arm.createtime[AR Create Time], '' AS[AR User Approval] , CURRENT_TIMESTAMP AS[AR ApprovalDate], aritemfaktur[No.Faktur], som.soitemdate[Date SO], som.soitemno[No SO], som.soitemgrandtotalamt[Total SO], ISNULL(STUFF((SELECT ', ' + ft.shipmentitemno FROM(SELECT DISTINCT CAST(cbx.shipmentitemno AS VARCHAR) shipmentitemno FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid = cbx.shipmentitemmstoid WHERE ardx.aritemmstoid = arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ NO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.shipmentitemdate,103) shipmentitemdate FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid= arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ Date], ISNULL(STUFF((SELECT ', '+ft.cashbankno FROM (SELECT DISTINCT CAST (cbx.cashbankno AS VARCHAR) cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid= arm.aritemmstoid AND ardx.reftype= 'QL_trnaritemmst' AND cbx.cashbanktype= 'BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar NO], ISNULL(STUFF((SELECT ', '+ft.cashbankdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.cashbankdate,103) cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid= arm.aritemmstoid AND ardx.reftype= 'QL_trnaritemmst' AND cbx.cashbanktype= 'BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar Date], ISNULL((SELECT SUM(cbx.cashbankamt) FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype='BLM'),0.0) [Bayar Amt] " + Slct2 + ", ISNULL(rabno,'') [No.RAB], ISNULL(rm.rabdate,'') [RAB Date], ISNULL(rm.projectname,'') [Project], ISNULL(us.usname,'') [Sales] FROM QL_trnaritemmst arm " + Join2 + " INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid= arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid= arm.curroid LEFT JOIN QL_trnrabmst rm ON arm.rabmstoid= rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid= rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid= arm.somstoid WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemtype= 'Jasa'*/";
                    sSql += " ) AS ar WHERE [CmpCode]='" + CompnyCode + "'";
                }

                if (StartPeriod != "" && EndPeriod != "")
                {
                    if (DDLType == "Summary Status Amount")
                    {
                        var filter = "ar.ardate";
                        if (DDLPeriod == "[Upd Date]")
                        {
                            filter = "[Upd Date]";
                        }
                        sSql += " AND " + filter + " >=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + filter + " <=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    }
                    else if (DDLType == "Summary Outstanding FJ")
                    {
                        sSql += " AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) AND ISNULL((SELECT SUM(con.amttrans - amtbayar) AS amt FROM QL_conar con WHERE con.reftype='QL_trnaritemmst' AND con.refoid=[armstoid] AND CASE con.payrefoid WHEN 0 THEN con.trnardate ELSE con.paymentdate END<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)),0.0) > 0 ";
                    }
                    else
                    {
                        sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    }
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND [Status] IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND [Custcode] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomorPO != "")
                {
                    string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (DDLType == "Detail")
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND [Mat Code] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
            }
            //sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            if (DDLType != "CSV")
            {
                rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
                rptparam.Add("PrintUserID", Session["UserID"].ToString());
                rptparam.Add("RptFile", rptfile);
            }

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary Status Amount" || DDLType == "Summary2" || DDLType == "Summary3")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }  
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary Status Amount" || DDLType == "Summary2" || DDLType == "Summary3")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else if (DDLType == "Detail")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else if (DDLType == "CSV")
                {
                    
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else if (reporttype == "CSV")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".csv");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}