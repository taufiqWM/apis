﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class SJAReturnController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public SJAReturnController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class sretassetmst
        {
            public string cmpcode { get; set; }
            public int sretassetmstoid { get; set; }
            public string sretassetno { get; set; }
            //[DataType(DataType.Date)]
            public DateTime? sretassetdate { get; set; }
            public string custname { get; set; }
            public string shipmentassetno { get; set; }
            public string sretassetmstres2 { get; set; }
            public string sretassetmststatus { get; set; }
            public string sretassetmstnote { get; set; }
            public string divname { get; set; }
        }

        public class sretassetdtl
        {
            public int sretassetdtlseq { get; set; }
            public int sretassetwhoid { get; set; }
            public int shipmentassetdtloid { get; set; }
            public int matrefoid { get; set; }
            public string matrefcode { get; set; }
            public string matreflongdesc { get; set; }
            public decimal shipmentassetqty { get; set; }
            public decimal sretassetqty { get; set; }
            public int sretassetunitoid { get; set; }
            public string sretassetunit { get; set; }
            public string sretassetdtlnote { get; set; }
            public decimal matreflimitqty { get; set; }
        }

        public class customer
        {
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
        }

        public class warehouse
        {
            public int sretassetwhoid { get; set; }
            public string sretassetwh { get; set; }
        }

        private void InitDDL(QL_trnsretassetmst tbl)
        {

        }

        //[HttpPost]
        //public ActionResult InitDDLAppUser(string cmp)
        //{
        //    var result = "sukses";
        //    var msg = "";
        //    List<QL_APPperson> tbl = new List<QL_APPperson>();
        //    sSql = "SELECT * FROM QL_APPperson WHERE tablename='QL_trnsretassetmst' AND activeflag='ACTIVE' AND approvaluser<>'admin' AND cmpcode IN ('" + cmp + "', '" + CompnyCode + "') ORDER BY approvaluser";
        //    tbl = db.Database.SqlQuery<QL_APPperson>(sSql).ToList();

        //    return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult GetCustomerData(string cmp)
        {
            List<customer> tbl = new List<customer>();
            sSql = "SELECT DISTINCT custoid, custcode, custname, custaddr FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid IN (SELECT custoid FROM QL_trnshipmentassetmst WHERE cmpcode='" + cmp + "' AND shipmentassetmststatus='Approved' AND ISNULL(shipmentassetmstres1, '')<>'Closed' AND shipmentassetmstoid NOT IN (SELECT shipmentassetmstoid FROM QL_trnarassetdtl ard INNER JOIN QL_trnarassetmst arm ON arm.cmpcode=ard.cmpcode AND arm.arassetmstoid=ard.arassetmstoid WHERE ard.cmpcode='" + cmp + "' AND arassetmststatus<>'Rejected')) ORDER BY custcode, custname";

            tbl = db.Database.SqlQuery<customer>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShipmentData(string cmp, int custoid)
        {
            List<QL_trnshipmentassetmst> tbl = new List<QL_trnshipmentassetmst>();
            sSql = "SELECT * FROM QL_trnshipmentassetmst WHERE cmpcode='" + cmp + "' AND sssflag=0 AND custoid=" + custoid + " AND shipmentassetmststatus='Approved' AND ISNULL(shipmentassetmstres1, '')<>'Closed' AND shipmentassetmstoid NOT IN (SELECT shipmentassetmstoid FROM QL_trnarassetdtl ard INNER JOIN QL_trnarassetmst arm ON arm.cmpcode=ard.cmpcode AND arm.arassetmstoid=ard.arassetmstoid WHERE ard.cmpcode='" + cmp + "' AND arassetmststatus<>'Rejected') ORDER BY shipmentassetmstoid";

            tbl = db.Database.SqlQuery<QL_trnshipmentassetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmp, int custoid, int shipmentassetmstoid, int sretassetmstoid)
        {
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            List<sretassetdtl> tbl = new List<sretassetdtl>();
            sSql = "SELECT DISTINCT shipmentassetdtloid, shd.assetmstoid matrefoid, m.assetno matrefcode, (CASE m.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=shd.assetmstoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=shd.assetmstoid) ELSE '' END) matreflongdesc, (CASE m.reftype WHEN 'General' THEN (SELECT x.matgenlimitqty FROM QL_mstmatgen x WHERE x.matgenoid=shd.assetmstoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlimitqty FROM QL_mstsparepart x WHERE x.sparepartoid=shd.assetmstoid) ELSE 0.0 END) matreflimitqty, shipmentassetunitoid, g.gendesc AS shipmentassetunit, shipmentassetwhoid, g2.gendesc AS shipmentassetwh, (shipmentassetqty - ISNULL((SELECT SUM(sretassetqty) FROM QL_TRNSRETASSETDTL sretd WHERE sretd.cmpcode=shd.cmpcode AND sretd.shipmentassetdtloid=shd.shipmentassetdtloid AND sretd.sretassetmstoid<>" + sretassetmstoid + "), 0)) AS shipmentassetqty, 0.0 AS sretassetqty, '' AS sretassetdtlnote, shipmentassetdtlseq FROM QL_trnshipmentassetdtl shd INNER JOIN QL_assetmst m ON m.refoid=shd.assetmstoid INNER JOIN QL_mstgen g ON genoid=shipmentassetunitoid INNER JOIN QL_mstgen g2 ON g2.genoid=shipmentassetwhoid WHERE shd.cmpcode='" + cmp + "' AND shipmentassetmstoid=" + shipmentassetmstoid + " AND ISNULL(shipmentassetdtlres1, '')<>'Complete' ORDER BY shipmentassetdtlseq";

            tbl = db.Database.SqlQuery<sretassetdtl>(sSql).ToList();
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for(var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].sretassetqty = tbl[i].shipmentassetqty;
                    }
                }
            }
            return Json(tbl, JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<sretassetdtl> dtDtl)
        {
            Session["QL_trnsretassetdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnsretassetdtl"] == null)
            {
                Session["QL_trnsretassetdtl"] = new List<sretassetdtl>();
            }

            List<sretassetdtl> dataDtl = (List<sretassetdtl>)Session["QL_trnsretassetdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnsretassetmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();
            ViewBag.shipmentassetno = db.Database.SqlQuery<string>("SELECT shipmentassetno FROM QL_trnshipmentassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + tbl.shipmentassetmstoid + "").FirstOrDefault();
        }

        // GET: sretassetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT sretm.cmpcode, sretassetmstoid, sretassetno, sretassetdate, custname, shipmentassetno, sretassetmststatus, sretassetmstnote, ISNULL(sretassetmstres2, 'Retur Ganti Barang') AS sretassetmstres2 FROM QL_trnsretassetmst sretm INNER JOIN QL_trnshipmentassetmst shm ON shm.cmpcode=sretm.cmpcode AND shm.shipmentassetmstoid=sretm.shipmentassetmstoid INNER JOIN QL_mstcust c ON c.custoid=sretm.custoid WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "sretm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "sretm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND sretassetdate>=CAST('" + modfil.filterperiodfrom + " 00:00:00' AS DATETIME) AND sretassetdate<=CAST('" + modfil.filterperiodto + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND sretassetmststatus IN ('In Process', 'Revised')";
            }

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND sretm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";

            List<sretassetmst> dt = db.Database.SqlQuery<sretassetmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: sretassetMaterial/Form/5/11
        public ActionResult Form(int? id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsretassetmst tbl;
            string action = "New Data";
            if (id == null | cmp == null)
            {
                tbl = new QL_trnsretassetmst();
                tbl.sretassetmstoid = ClassFunction.GenerateID("QL_trnsretassetmst");
                tbl.sretassetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.sretassetmststatus = "In Process";
                
                Session["QL_trnsretassetdtl"] = null;
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_trnsretassetmst.Find(cmp, id);

                sSql = "SELECT sretassetdtlseq, sretassetwhoid, sretd.shipmentassetdtloid, sretd.assetmstoid matrefoid, m.assetno matrefcode, (CASE m.reftype WHEN 'General' THEN (SELECT x.matgenlongdesc FROM QL_mstmatgen x WHERE x.matgenoid=sretd.assetmstoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlongdesc FROM QL_mstsparepart x WHERE x.sparepartoid=sretd.assetmstoid) ELSE '' END) matreflongdesc, (CASE m.reftype WHEN 'General' THEN (SELECT x.matgenlimitqty FROM QL_mstmatgen x WHERE x.matgenoid=sretd.assetmstoid) WHEN 'Spare Part' THEN (SELECT x.sparepartlimitqty FROM QL_mstsparepart x WHERE x.sparepartoid=sretd.assetmstoid) ELSE 0.0 END) matreflimitqty, (shipmentassetqty - ISNULL((SELECT SUM(x.sretassetqty) FROM QL_TRNSRETASSETDTL x INNER JOIN QL_trnsretassetmst xm ON xm.cmpcode=x.cmpcode AND xm.sretassetmstoid=x.sretassetmstoid  WHERE x.cmpcode=sretd.cmpcode AND x.shipmentassetdtloid=sretd.shipmentassetdtloid AND x.sretassetmstoid<>sretd.sretassetmstoid AND sretassetmststatus<>'Rejected'), 0)) AS shipmentassetqty, sretassetqty, sretassetunitoid, gendesc AS sretassetunit, sretassetdtlnote FROM QL_TRNSRETASSETDTL sretd INNER JOIN QL_trnshipmentassetdtl shd ON shd.cmpcode=sretd.cmpcode AND shd.shipmentassetdtloid=sretd.shipmentassetdtloid INNER JOIN QL_trnshipmentassetmst shm ON shm.cmpcode=sretd.cmpcode AND shm.shipmentassetmstoid=shd.shipmentassetmstoid INNER JOIN QL_assetmst m ON m.refoid=sretd.assetmstoid INNER JOIN QL_mstgen g ON genoid=sretassetunitoid WHERE sretassetmstoid=" + id + " ORDER BY sretassetdtlseq";
                Session["QL_trnsretassetdtl"] = db.Database.SqlQuery<sretassetdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: sretassetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnsretassetmst tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.sretassetno == null)
                tbl.sretassetno = "";
            
            List<sretassetdtl> dtDtl = (List<sretassetdtl>)Session["QL_trnsretassetdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].sretassetqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY must be more than 0!");
                        }
                        else
                        {
                            if (dtDtl[i].sretassetqty > dtDtl[i].shipmentassetqty)
                            {
                                ModelState.AddModelError("", "QTY must be less than Shipment QTY!");
                            }
                            else
                            {
                                //if (dtDtl[i].matreflimitqty > 0)
                                //{
                                //    if (!ClassFunction.IsQtyRounded(dtDtl[i].sretassetqty, dtDtl[i].matreflimitqty))
                                //    {
                                //        ModelState.AddModelError("", "QTY must be rounded by ROUNDING QTY!");
                                //    }
                                //}
                            }
                        }

                        sSql = "SELECT (shipmentassetqty - ISNULL((SELECT SUM(sretassetqty) FROM QL_trnsretassetdtl sretd INNER JOIN QL_trnsretassetmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretassetmstoid=sretd.sretassetmstoid WHERE sretd.cmpcode=shd.cmpcode AND sretd.shipmentassetdtloid=shd.shipmentassetdtloid AND sretassetmststatus<>'Rejected' AND sretd.sretassetmstoid<>" + tbl.sretassetmstoid + "), 0)) AS shipmentassetqty FROM QL_trnshipmentassetdtl shd WHERE shd.cmpcode='" + tbl.cmpcode + "' AND shd.shipmentassetdtloid=" + dtDtl[i].shipmentassetdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].shipmentassetqty)
                            dtDtl[i].shipmentassetqty = dQty;
                        if (dQty < dtDtl[i].sretassetqty)
                            ModelState.AddModelError("", "Shipment QTY for some detail data has been updated by another user. Please check that every detail Shipment QTY must be more or equal than RETURN QTY!<BR>");
                    }
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tbl.sretassetmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
                }
            }
            // END VAR UTK APPROVAL

            if (tbl.sretassetmststatus == "Revised")
                tbl.sretassetmststatus = "In Process";
            if (!ModelState.IsValid)
                tbl.sretassetmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnsretassetmst");
                var dtloid = ClassFunction.GenerateID("QL_trnsretassetdtl");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            if (db.QL_trnsretassetmst.Find(tbl.cmpcode, tbl.sretassetmstoid) != null)
                                tbl.sretassetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.sretassetdate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnsretassetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_mstoid SET lastoid=" + tbl.sretassetmstoid + " WHERE tablename='QL_trnsretassetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentassetdtl SET shipmentassetdtlres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetdtloid IN (SELECT shipmentassetdtloid FROM QL_trnsretassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND sretassetmstoid=" + tbl.sretassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentassetmst SET shipmentassetmstres1='' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid IN (SELECT shipmentassetmstoid FROM QL_trnsretassetmst WHERE cmpcode='" + tbl.cmpcode + "' AND sretassetmstoid=" + tbl.sretassetmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnsretassetdtl.Where(a => a.sretassetmstoid == tbl.sretassetmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnsretassetdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnsretassetdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnsretassetdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.sretassetdtloid = dtloid++;
                            tbldtl.sretassetmstoid = tbl.sretassetmstoid;
                            tbldtl.sretassetdtlseq = i + 1;
                            tbldtl.shipmentassetdtloid = dtDtl[i].shipmentassetdtloid;
                            tbldtl.assetmstoid = dtDtl[i].matrefoid;
                            tbldtl.sretassetqty = dtDtl[i].sretassetqty;
                            tbldtl.sretassetunitoid = dtDtl[i].sretassetunitoid;
                            tbldtl.sretassetwhoid = dtDtl[i].sretassetwhoid;
                            tbldtl.sretassetdtlstatus = "";
                            tbldtl.sretassetdtlnote = dtDtl[i].sretassetdtlnote;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnsretassetdtl.Add(tbldtl);
                            db.SaveChanges();
                            if (dtDtl[i].sretassetqty >= dtDtl[i].shipmentassetqty)
                            {
                                sSql = "UPDATE QL_trnshipmentassetdtl SET shipmentassetdtlres1='Complete' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetdtloid=" + dtDtl[i].shipmentassetdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnshipmentassetmst SET shipmentassetmstres1='Closed' WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + tbl.shipmentassetmstoid + " AND (SELECT COUNT(*) FROM QL_trnshipmentassetdtl WHERE cmpcode='" + tbl.cmpcode + "' AND shipmentassetmstoid=" + tbl.shipmentassetmstoid + " AND shipmentassetdtloid<>" + dtDtl[i].shipmentassetdtloid + " AND ISNULL(shipmentassetdtlres1, '')<>'Complete')=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_mstoid SET lastoid=" + dtloid + " WHERE tablename='QL_trnsretassetdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // INSERT APPROVAL
                        if (tbl.sretassetmststatus == "In Approval")
                        {
                            QL_APP tblApp = new QL_APP();
                            tblApp.cmpcode = tbl.cmpcode;
                            tblApp.appoid = appoid;
                            tblApp.appform = ctrlname;
                            tblApp.appformoid = tbl.sretassetmstoid;
                            tblApp.requser = tbl.upduser;
                            tblApp.reqdate = servertime;
                            tblApp.appstoid = stoid_app;
                            tblApp.tablename = "QL_trnpoassetmst";
                            db.QL_APP.Add(tblApp);
                            db.SaveChanges();                            

                            sSql = "UPDATE QL_mstoid SET lastoid=" + appoid + " WHERE tablename='QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        // END INSERT APPROVAL

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: sretassetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnsretassetmst tbl = db.QL_trnsretassetmst.Find(cmp, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnshipmentassetdtl SET shipmentassetdtlres1='' WHERE cmpcode='" + cmp + "' AND shipmentassetdtloid IN (SELECT shipmentassetdtloid FROM QL_trnsretassetdtl WHERE cmpcode='" + cmp + "' AND sretassetmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentassetmst SET shipmentassetmstres1='' WHERE cmpcode='" + cmp + "' AND shipmentassetmstoid IN (SELECT shipmentassetmstoid FROM QL_trnsretassetmst WHERE cmpcode='" + cmp + "' AND sretassetmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnsretassetdtl.Where(a => a.sretassetmstoid == id && a.cmpcode == cmp);
                        db.QL_trnsretassetdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnsretassetmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var sReportName = "rptSRetAsset.rpt";
            report.Load(Path.Combine(Server.MapPath("~/Report"), sReportName));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT profname FROM QL_mstprof WHERE profoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sretm.cmpcode='"+ cmp +"' AND sretm.sretassetmstoid=" + id);
            ClassProcedure.SetDBLogonForReport(report);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SalesReturnAssetsPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}