﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Other
{
    public class Dashboard2Controller : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public Dashboard2Controller()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET: Dashboard2
        //public class listrab
        //{
        //    public int rabmstoid { get; set; }
        //    public string rabno { get; set; }
        //    public string rabdate { get; set; }
        //    public string custname { get; set; }
        //    public string projectname { get; set; }
        //    public string sales { get; set; }
        //    public decimal rabsoqty { get; set; }
        //    public decimal soqty { get; set; }
        //    public decimal sjqty { get; set; }
        //    public decimal siqty { get; set; }
        //    public decimal rabpoqty { get; set; }
        //    public decimal poqty { get; set; }
        //    public decimal bookingstock { get; set; }
        //    public decimal fpb { get; set; }
        //    public decimal mrqty { get; set; }
        //    public decimal piqty { get; set; }
        //    public string flag { get; set; }
        //    public decimal ar { get; set; }
        //    public decimal bayarar { get; set; }
        //    public string flagar { get; set; }
        //    public decimal ap { get; set; }
        //    public decimal bayarap { get; set; }
        //    public string flagap { get; set; }
        //}

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public string rabdate { get; set; }
            public string soitemno { get; set; }
            public string soitemdate { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string salesadmin { get; set; }
            public string sales { get; set; }
            public decimal amt { get; set; }
            public string lastuser { get; set; }

        }

        public class dbshipmentmodel
        {
            public string divname { get; set; }
            public decimal totalshipment { get; set; }
        }

        public ActionResult GetDataDashboardShipment()
        {
            var startperiod = ClassFunction.GetServerTime().ToString("01/01/yyyy") + " 00:00:00";
            var endperiod = ClassFunction.GetServerTime().ToString("MM/dd/yyyy") + " 23:59:59";

            sSql = "SELECT ardate divname, SUM(totalshipment) totalshipment FROM (";
            sSql += "SELECT arm.cmpcode, (SELECT divname FROM QL_mstdivision div WHERE div.cmpcode=arm.cmpcode) divname, (SELECT currcode FROM QL_mstcurr c WHERE c.curroid=arm.curroid) currcode, SUM(ard.aritemdtlnetto) totalshipment, CONVERT(VARCHAR(7),arm.aritemdate,120) ardate FROM QL_trnaritemmst arm INNER JOIN QL_trnaritemdtl ard ON ard.cmpcode=arm.cmpcode AND ard.aritemmstoid=arm.aritemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid INNER JOIN QL_trnshipmentitemmst shm ON shd.cmpcode=shm.cmpcode AND shd.shipmentitemmstoid=shm.shipmentitemmstoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=shd.cmpcode AND som.soitemmstoid=shm.soitemmstoid WHERE aritemmststatus IN ('Approved', 'Closed', 'Post') AND aritemdate <= CAST('" + endperiod + "' AS DATETIME) GROUP BY arm.cmpcode, arm.curroid, CONVERT(VARCHAR(7),arm.aritemdate,120)";
            sSql += ") QL_totalshipment GROUP BY ardate ORDER BY ardate";

            List<dbshipmentmodel> dataDtl = db.Database.SqlQuery<dbshipmentmodel>(sSql).ToList();

            JsonResult js = Json(dataDtl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            return View();
        }

        public class barang_draft
        {
            public int rabmstoid_draft { get; set; }
            public int rabdtloid_draft { get; set; }
            public int rabdtloid2_draft { get; set; }
            public string rabno { get; set; }
            public string rabtype { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string itemdesc { get; set; }
            public string itemtype { get; set; }

        }

        public class jasa_draft
        {
            public int rabmstoid_draft { get; set; }
            public int rabdtloid4_draft { get; set; }
            public string rabno { get; set; }
            public string rabtype { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string itemdesc { get; set; }

        }

        [HttpPost]
        public ActionResult GetRABQtyStatusData()
        {
            List<listrab> tbl = new List<listrab>();
            //sSql = "SELECT *, CASE WHEN rabsoqty <= soqty AND soqty <= sjqty AND sjqty <= siqty AND rabpoqty <= (poqty + bookingstock + fpb) AND poqty <= mrqty AND mrqty <= piqty THEN 'Done' ELSE 'On Progress' END flag, CASE WHEN ar=bayarar AND ar>0 THEN 'Lunas' ELSE 'Belum' END flagar, CASE WHEN ap=bayarap AND ap>0  THEN 'Lunas' ELSE 'Belum' END flagap FROM( SELECT rab.rabmstoid, ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=rab.rabmstoid AND ISNULL(som.soitemtype,'')=''),'') rabno,ISNULL((SELECT CONVERT(VARCHAR(10), som.soitemdate, 103) FROM QL_trnsoitemmst som WHERE som.rabmstoid=rab.rabmstoid AND ISNULL(som.soitemtype,'')=''),'') rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabsoqty, ISNULL((SELECT SUM(sod.soitemqty) FROM QL_trnsoitemdtl sod INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = sod.soitemmstoid WHERE som.soitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) soqty, ISNULL((SELECT SUM(sod.shipmentitemqty) FROM QL_trnshipmentitemdtl sod INNER JOIN QL_trnshipmentitemmst som ON som.shipmentitemmstoid = sod.shipmentitemmstoid WHERE som.shipmentitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) sjqty, ISNULL((SELECT SUM(sod.aritemqty) FROM QL_trnaritemdtl sod INNER JOIN QL_trnaritemmst som ON som.aritemmstoid = sod.aritemmstoid WHERE som.aritemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) siqty, ISNULL((SELECT SUM(rd.rabdtl2qty) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) +ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.formoid = rab.rabmstoid),0.0) rabpoqty, ISNULL((SELECT SUM(sod.poitemqty) FROM QL_trnpoitemdtl sod INNER JOIN QL_trnpoitemmst som ON som.poitemmstoid = sod.poitemmstoid WHERE som.poitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) poqty, ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.formoid = rab.rabmstoid),0.0) bookingstock, ISNULL((SELECT SUM(rd.rabdtl2qty) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid AND ISNULL(rd.poassetmstoid, 0) <> 0 AND ISNULL(rd.poassetdtloid, 0) <> 0),0.0) fpb, ISNULL((SELECT SUM(sod.mritemqty) FROM QL_trnmritemdtl sod INNER JOIN QL_trnmritemmst som ON som.mritemmstoid = sod.mritemmstoid WHERE som.mritemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) mrqty, ISNULL((SELECT SUM(sod.apitemqty) FROM QL_trnapitemdtl sod INNER JOIN QL_trnapitemmst som ON som.apitemmstoid = sod.apitemmstoid WHERE som.apitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) piqty, ISNULL((SELECT SUM(con.amttrans) FROM QL_trnaritemmst arm INNER JOIN QL_conar con ON con.reftype = 'QL_trnaritemmst' AND con.refoid = arm.aritemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) ar, ISNULL((SELECT SUM(con.amtbayar) FROM QL_trnaritemmst arm INNER JOIN QL_conar con ON con.reftype = 'QL_trnaritemmst' AND con.refoid = arm.aritemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) bayarar, ISNULL((SELECT SUM(con.amttrans) FROM QL_trnapitemmst arm INNER JOIN QL_conap con ON con.reftype = 'QL_trnapitemmst' AND con.refoid = arm.apitemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) ap, ISNULL((SELECT SUM(con.amtbayar) FROM QL_trnapitemmst arm INNER JOIN QL_conap con ON con.reftype = 'QL_trnapitemmst' AND con.refoid = arm.apitemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) bayarap FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' ) AS tb ORDER BY rabdate DESC";

            string sWhere = "";
            if (Session["Jabatan"] != null && Session["Jabatan"].ToString() == "SALES") sWhere = $" AND (rab.salesadminoid='{Session["UserID"].ToString()}' OR rab.salesoid='{Session["UserID"].ToString()}')";
            sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, som.soitemno, CONVERT(VARCHAR(10), som.soitemdate, 103) soitemdate, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),'') sales, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesadminoid),'') salesadmin, ROUND(som.soitemgrandtotalamt,0) amt, ISNULL((SELECT TOP 1 UPPER(tb.createuser) FROM( SELECT 1 seq, som.createuser, som.updtime FROM QL_trnsoitemmst som where som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid UNION ALL SELECT 2 seq, som.createuser, som.updtime FROM QL_trnpoitemmst som where som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid UNION ALL SELECT 3 seq, som.createuser, som.updtime FROM QL_trnmritemmst som where som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid UNION ALL SELECT 4 seq, som.createuser, som.updtime FROM QL_trnshipmentitemmst som where som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid UNION ALL SELECT 5 seq, som.createuser, som.updtime FROM QL_trnapitemmst som where som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid UNION ALL SELECT 6 seq, som.createuser, som.updtime FROM QL_trnaritemmst som where som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid )AS tb ORDER BY tb.seq DESC, tb.updtime DESC),'') lastuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode=rab.cmpcode AND som.rabmstoid=rab.rabmstoid AND ISNULL(som.soitemtype,'')='' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' " + sWhere +" ORDER BY rabdate DESC";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            List<barang_draft> tbl2 = new List<barang_draft>();
            sSql = "SELECT rabmstoid_draft, SUM(rabdtloid_draft) rabdtloid_draft, SUM(rabdtloid2_draft) rabdtloid2_draft, rabno, rabtype, projectname, custname, itemdesc, itemtype FROM( SELECT rd.rabmstoid rabmstoid_draft, rabdtloid_draft, 0 rabdtloid2_draft, rabno, rabtype, projectname, custname, rd.itemdtldesc itemdesc, ISNULL(rd.itemdtltype, 'Barang') itemtype FROM QL_trnrabmst_draft rm INNER JOIN QL_trnrabdtl_draft rd ON rd.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid WHERE rm.rabmststatus IN('Post', 'Approved') AND ISNULL(rd.itemoid, 0)= 0 AND ISNULL(rabdtlflag_m,'')= 'Manual'  UNION ALL SELECT rd.rabmstoid rabmstoid_draft, 0 rabdtloid_draft, rabdtloid2_draft, rabno, rabtype, projectname, custname, rd.itemdtl2desc itemdesc, 'Barang' itemtype FROM QL_trnrabmst_draft rm INNER JOIN QL_trnrabdtl2_draft rd ON rd.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = rm.custoid WHERE rm.rabmststatus IN('Post', 'Approved') AND ISNULL(rd.itemdtl2oid, 0)= 0 AND ISNULL(rabdtl2flag_m,'')= 'Manual' )AS tb GROUP BY rabmstoid_draft, rabno, rabtype, projectname, custname, itemdesc, itemtype";
            tbl2 = db.Database.SqlQuery<barang_draft>(sSql).ToList();

            List<jasa_draft> tbl3 = new List<jasa_draft>();
            sSql = "SELECT rd.rabmstoid rabmstoid_draft, rabdtloid4_draft, rabno, rabtype, projectname, custname, rd.itemdtl4desc itemdesc FROM QL_trnrabmst_draft rm INNER JOIN QL_trnrabdtl4_draft rd ON rd.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid WHERE rm.rabmststatus IN('Post','Approved') AND ISNULL(rd.itemdtl4oid,0)=0 AND ISNULL(rabdtl4flag_m,'')='Manual'";
            tbl3 = db.Database.SqlQuery<jasa_draft>(sSql).ToList();

            JsonResult js = Json(new { tbl, tbl2, tbl3 }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDetailRAB(string tipe)
        {
            var result = ""; string title_dash = ""; var id = int.Parse(tipe);
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                title_dash = "Data Detail RAB";
                sSql = "SELECT rabmstoid oid, /*rabno [RAB No], rabdate [RAB Date], custname [Customer],*/ projectname [Project], /*sales [Sales], amt [DEC_Amount],*/ transno [Trans No], transdate [Trans Date], rabqty [DEC_RAB Qty], transqty [DEC_Real Qty], transuser [User] FROM( SELECT 1 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(som.soitemgrandtotalamt, 0) amt, som.soitemno transno, CONVERT(VARCHAR(10), som.soitemdate, 103) transdate, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.soitemqty) FROM QL_trnsoitemdtl rd WHERE rd.soitemmstoid = som.soitemmstoid),0.0) transqty, UPPER(som.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.soitemtype, '')= '' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 2 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(rab.rabgrandtotalbeliamt, 0) amt, som.poitemno transno, CONVERT(VARCHAR(10), som.poitemdate, 103) transdate, ISNULL((SELECT SUM(rd.rabdtl2qty + rd.rabdtl2qtybooking) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.poitemqty) FROM QL_trnpoitemdtl rd WHERE rd.poitemmstoid = som.poitemmstoid),0.0) +ISNULL((SELECT SUM(rd.rabdtl2qtybooking) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) transqty, UPPER(som.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnpoitemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.poitemtype, '')= '' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 3 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(rab.rabgrandtotalbeliamt, 0) amt, som.mritemno transno, CONVERT(VARCHAR(10), som.mritemdate, 103) transdate, ISNULL((SELECT SUM(rd.rabdtl2qty) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.mritemqty) FROM QL_trnmritemdtl rd WHERE rd.mritemmstoid = som.mritemmstoid),0.0) transqty, UPPER(som.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnmritemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.mritemtype, '')= '' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 4 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(sm.soitemgrandtotalamt, 0) amt, som.shipmentitemno transno, CONVERT(VARCHAR(10), som.shipmentitemdate, 103) transdate, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.shipmentitemqty) FROM QL_trnshipmentitemdtl rd WHERE rd.shipmentitemmstoid = som.shipmentitemmstoid),0.0) transqty, UPPER(som.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode = rab.cmpcode AND sm.rabmstoid = rab.rabmstoid INNER JOIN QL_trnshipmentitemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.shipmentitemtype, '')= '' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(sm.soitemtype, '')= '' AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 5 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(rab.rabgrandtotalbeliamt, 0) amt, som.apitemno transno, CONVERT(VARCHAR(10), som.apitemdate, 103) transdate, ISNULL((SELECT SUM(rd.rabdtl2qty) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.apitemqty) FROM QL_trnapitemdtl rd WHERE rd.apitemmstoid = som.apitemmstoid),0.0) transqty, UPPER(som.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnapitemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.apitemtype, '')= '' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 6 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(sm.soitemgrandtotalamt, 0) amt, som.aritemno transno, CONVERT(VARCHAR(10), som.aritemdate, 103) transdate, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.aritemqty) FROM QL_trnaritemdtl rd WHERE rd.aritemmstoid = som.aritemmstoid),0.0) transqty, UPPER(som.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode = rab.cmpcode AND sm.rabmstoid = rab.rabmstoid INNER JOIN QL_trnaritemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.aritemtype, '')= '' WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(sm.soitemtype, '')= '' AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 7 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(sm.soitemgrandtotalamt, 0) amt, cb.cashbankno transno, CONVERT(VARCHAR(10), cb.cashbankdate, 103) transdate, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.aritemqty) FROM QL_trnaritemdtl rd WHERE rd.aritemmstoid = som.aritemmstoid),0.0) transqty, UPPER(cb.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode = rab.cmpcode AND sm.rabmstoid = rab.rabmstoid INNER JOIN QL_trnaritemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.aritemtype, '')= '' INNER JOIN QL_trnpayar pay ON pay.cmpcode=som.cmpcode AND pay.reftype='QL_trnaritemmst' AND pay.refoid=som.aritemmstoid INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=pay.cashbankoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(sm.soitemtype, '')= '' AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 7 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(sm.soitemgrandtotalamt, 0) amt, cb.cashbankno transno, CONVERT(VARCHAR(10), cb.cashbankdate, 103) transdate, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.aritemqty) FROM QL_trnaritemdtl rd WHERE rd.aritemmstoid = som.aritemmstoid),0.0) transqty, UPPER(cb.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode = rab.cmpcode AND sm.rabmstoid = rab.rabmstoid INNER JOIN QL_trnaritemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.aritemtype, '')= '' INNER JOIN QL_trnpaydp pay ON pay.cmpcode = som.cmpcode AND pay.reftype = 'QL_trnaritemmst' AND pay.refoid = som.aritemmstoid INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid = pay.cashbankoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(sm.soitemtype, '')= '' AND ISNULL(rab.rabmstres1, '')= '' UNION ALL SELECT 8 seq, rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ROUND(sm.soitemgrandtotalamt, 0) amt, a2.ntpnno transno, CONVERT(VARCHAR(10), a2.createtime, 103) transdate, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.aritemqty) FROM QL_trnaritemdtl rd WHERE rd.aritemmstoid = som.aritemmstoid),0.0) transqty, UPPER(a2.createuser) transuser FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid INNER JOIN QL_trnsoitemmst sm ON sm.cmpcode = rab.cmpcode AND sm.rabmstoid = rab.rabmstoid INNER JOIN QL_trnaritemmst som ON som.cmpcode = rab.cmpcode AND som.rabmstoid = rab.rabmstoid AND ISNULL(som.aritemtype, '')= '' INNER JOIN QL_trnaritemdtl2 a2 ON a2.cmpcode = som.cmpcode AND a2.aritemmstoid = som.aritemmstoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(sm.soitemtype, '')= '' AND ISNULL(rab.rabmstres1, '')= '')AS tb WHERE tb.rabmstoid = " + id +" ORDER BY tb.seq";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblDashBox");
                if (tbl.Rows.Count > 0)
                {
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            try
                            {
                                decimal decval = 0;
                                if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                {
                                    if (decval == 0)
                                        item = "0.00";
                                    else
                                        item = decval.ToString("#,##0.00");
                                }
                            }
                            catch
                            {
                                item = dr[col].ToString();
                            }
                            row.Add(col.ColumnName.Replace("DEC_", ""), item);
                            if (!tblcols.Contains(col.ColumnName.Replace("DEC_", "")))
                                tblcols.Add(col.ColumnName.Replace("DEC_", ""));
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows, title_dash }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();
            sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, rab.rabtotaljualamt bruto, rab.rabtotaljualtaxamt totaltax, rab.rabtotaljualpphamt totalpph, (rab.rabtotaljualamt + rab.rabtotaljualtaxamt) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved','Closed') ORDER BY rabdate DESC";

            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABDataStatus(string sts, int rabmstoid)
        {
            List<listrab> tbl = new List<listrab>();
            if (sts == "RAB")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, rab.rabtotaljualamt bruto, rab.rabtotaljualtaxamt totaltax, rab.rabtotaljualpphamt totalpph, (rab.rabtotaljualamt + rab.rabtotaljualtaxamt) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved','Closed') AND rab.rabmstoid="+ rabmstoid +"";
            }
            else if (sts == "SO")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL(som.soitemtotalamt,0.0) bruto, ISNULL(som.soitemtotaltaxamt,0.0) totaltax, 0.0 totalpph, (ISNULL(som.soitemtotalamt,0.0) + ISNULL(som.soitemtotaltaxamt,0.0)) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(sod.soitemqty) FROM QL_trnsoitemdtl sod WHERE sod.soitemmstoid=som.soitemmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid LEFT JOIN QL_trnsoitemmst som ON som.rabmstoid=rab.rabmstoid AND ISNULL(som.soitemmststatus, '') IN('Approved','Closed','Post') WHERE rab.rabmstoid=" + rabmstoid + "";
            }
            else if (sts == "PO")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL(som.poitemtotalamt, 0.0) bruto, ISNULL(som.poitemtotaltaxamt, 0.0) totaltax, 0.0 totalpph, (ISNULL(som.poitemtotalamt, 0.0) + ISNULL(som.poitemtotaltaxamt, 0.0)) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(sod.poitemqty) FROM QL_trnpoitemdtl sod WHERE sod.poitemmstoid = som.poitemmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid LEFT JOIN QL_trnpoitemmst som ON som.rabmstoid = rab.rabmstoid AND ISNULL(som.poitemmststatus, '') IN('Approved', 'Closed', 'Post') WHERE rab.rabmstoid=" + rabmstoid + "";
            }
            else if (sts == "PB")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL(som.poitemtotalamt,0.0) bruto, ISNULL(som.poitemtotaltaxamt,0.0) totaltax, 0.0 totalpph, (ISNULL(som.poitemtotalamt,0.0) + ISNULL(som.poitemtotaltaxamt,0.0)) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(sod.mritemqty) FROM QL_trnmritemdtl sod WHERE sod.mritemmstoid=mrm.mritemmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid LEFT JOIN QL_trnpoitemmst som ON som.rabmstoid=rab.rabmstoid AND ISNULL(som.poitemmststatus, '') IN('Approved','Closed','Post') LEFT JOIN QL_trnmritemmst mrm ON mrm.pomstoid=som.poitemmstoid AND ISNULL(mrm.mritemmststatus, '') IN('Approved','Closed','Post') WHERE rab.rabmstoid=" + rabmstoid + "";
            }
            else if (sts == "FB")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL(mrm.apitemtotalamt,0.0) bruto, ISNULL(mrm.apitemtaxamt,0.0) totaltax, 0.0 totalpph, (ISNULL(mrm.apitemtotalamt,0.0) + ISNULL(mrm.apitemtaxamt,0.0)) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(sod.apitemqty) FROM QL_trnapitemdtl sod WHERE sod.apitemmstoid=mrm.apitemmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid LEFT JOIN QL_trnpoitemmst som ON som.rabmstoid=rab.rabmstoid AND ISNULL(som.poitemmststatus, '') IN('Approved','Closed','Post') LEFT JOIN QL_trnapitemmst mrm ON mrm.poitemmstoid=som.poitemmstoid AND ISNULL(mrm.apitemmststatus, '') IN('Approved','Closed','Post') WHERE rab.rabmstoid=" + rabmstoid + "";
            }
            else if (sts == "SJ")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL(som.soitemtotalamt,0.0) bruto, ISNULL(som.soitemtotaltaxamt,0.0) totaltax, 0.0 totalpph, (ISNULL(som.soitemtotalamt,0.0) + ISNULL(som.soitemtotaltaxamt,0.0)) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(sod.shipmentitemqty) FROM QL_trnshipmentitemdtl sod WHERE sod.shipmentitemmstoid=mrm.shipmentitemmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid LEFT JOIN QL_trnsoitemmst som ON som.rabmstoid=rab.rabmstoid AND ISNULL(som.soitemmststatus, '') IN('Approved','Closed','Post') LEFT JOIN QL_trnshipmentitemmst mrm ON mrm.soitemmstoid=som.soitemmstoid AND ISNULL(mrm.shipmentitemmststatus, '') IN('Approved','Closed','Post') WHERE rab.rabmstoid=" + rabmstoid + "";
            }
            else if (sts == "FJ")
            {
                sSql = "SELECT rab.rabmstoid, rab.rabno, CONVERT(VARCHAR(10), rab.rabdate, 103) rabdate, c.custname, rab.projectname, ISNULL(mrm.aritemtotalamt,0.0) bruto, ISNULL(mrm.aritemtaxamt,0.0) totaltax, 0.0 totalpph, (ISNULL(mrm.aritemtotalamt,0.0) + ISNULL(mrm.aritemtaxamt,0.0)) netto, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid=rab.rabmstoid),0.0) rabqty, ISNULL((SELECT SUM(sod.aritemqty) FROM QL_trnaritemdtl sod WHERE sod.aritemmstoid=mrm.aritemmstoid),0.0) qty FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid=rab.custoid LEFT JOIN QL_trnsoitemmst som ON som.rabmstoid=rab.rabmstoid AND ISNULL(som.soitemmststatus, '') IN('Approved','Closed','Post') LEFT JOIN QL_trnaritemmst mrm ON mrm.somstoid=som.soitemmstoid AND ISNULL(mrm.aritemmststatus, '') IN('Approved','Closed','Post') WHERE rab.rabmstoid=" + rabmstoid + "";
            }
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataDashBox(string tipe)
        {
            var result = ""; string title_dash = "";
            JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                if (tipe == "kas_dash")
                {
                    string acctgoidKas = ClassFunction.GetDataAcctgOid("VAR_CASH_DASH", CompnyCode);
                    title_dash = "Data Kas";
                    sSql = "SELECT t.acctgoid iod, t.acctgcode [Kode], t.acctgdesc [Deskripsi], (ISNULL((SELECT SUM(crd.amtbalance) FROM QL_crdgl crd WHERE crd.acctgoid=t.acctgoid AND crd.crdgloid < 0),0.0) + t.saldo) [DEC_Saldo] FROM( SELECT tb.acctgoid, tb.acctgcode, acctgdesc, SUM(debet - credit) saldo FROM( SELECT gld.acctgoid, a.acctgcode, a.acctgdesc, CASE WHEN gld.gldbcr='D' THEN SUM(gld.glamtidr) ELSE 0.0 END debet, CASE WHEN gld.gldbcr='C' THEN SUM(gld.glamtidr) ELSE 0.0 END credit FROM Ql_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.glmstoid=gld.glmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.acctgoid IN(" + acctgoidKas + ") AND glm.glflag IN('Post','Closed') GROUP BY gld.gldbcr, gld.acctgoid, a.acctgcode, a.acctgdesc)AS tb GROUP BY tb.acctgoid, tb.acctgcode, acctgdesc) AS t";
                }
                else if (tipe == "bank_dash")
                {
                    string acctgoidBank = ClassFunction.GetDataAcctgOid("VAR_BANK_DASH", CompnyCode);
                    title_dash = "Data Bank";
                    sSql = "SELECT t.acctgoid iod, t.acctgcode [Kode], t.acctgdesc [Deskripsi], (ISNULL((SELECT SUM(crd.amtbalance) FROM QL_crdgl crd WHERE crd.acctgoid=t.acctgoid AND crd.crdgloid < 0),0.0) + t.saldo) [DEC_Saldo] FROM( SELECT tb.acctgoid, tb.acctgcode, acctgdesc, SUM(debet - credit) saldo FROM( SELECT gld.acctgoid, a.acctgcode, a.acctgdesc, CASE WHEN gld.gldbcr='D' THEN SUM(gld.glamtidr) ELSE 0.0 END debet, CASE WHEN gld.gldbcr='C' THEN SUM(gld.glamtidr) ELSE 0.0 END credit FROM Ql_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.glmstoid=gld.glmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.acctgoid IN(" + acctgoidBank + ") AND glm.glflag IN('Post','Closed') GROUP BY gld.gldbcr, gld.acctgoid, a.acctgcode, a.acctgdesc)AS tb GROUP BY tb.acctgoid, tb.acctgcode, acctgdesc) AS t";
                }
                else if (tipe == "hutang_dash")
                {
                    title_dash = "Data Hutang";
                    sSql = "SELECT con.suppoid oid, c.suppcode [Kode], c.suppname [Supplier], SUM(con.amttrans - con.amtbayar) [DEC_Saldo] FROM Ql_conap con INNER JOIN QL_mstsupp c ON c.suppoid=con.suppoid WHERE c.supptype='Hutang' GROUP BY con.suppoid, c.suppcode, c.suppname HAVING SUM(con.amttrans - con.amtbayar)>0.9";
                }
                else if (tipe == "hutang_lain_dash")
                {
                    title_dash = "Data Hutang Lain";
                    sSql = "SELECT con.suppoid oid, c.suppcode [Kode], c.suppname [Supplier], SUM(con.amttrans - con.amtbayar) [DEC_Saldo] FROM Ql_conap con INNER JOIN QL_mstsupp c ON c.suppoid=con.suppoid WHERE c.supptype='Other' GROUP BY con.suppoid, c.suppcode, c.suppname HAVING SUM(con.amttrans - con.amtbayar)>0.9";
                }
                else if (tipe == "piutang_dash")
                {
                    title_dash = "Data Piutang";
                    sSql = "SELECT con.custoid oid, c.custcode [Kode], c.custname [Customer], SUM(con.amttrans - con.amtbayar) [DEC_Saldo] FROM Ql_conar con INNER JOIN QL_mstcust c ON c.custoid=con.custoid GROUP BY con.reftype, con.refoid, con.custoid, c.custcode, c.custname HAVING SUM(con.amttrans - con.amtbayar)>0.9";
                }
                else if (tipe == "stock_dash")
                {
                    string acctgoidStock = ClassFunction.GetDataAcctgOid("VAR_STOCK", CompnyCode);
                    title_dash = "Data Persediaan";
                    sSql = "SELECT t.acctgoid oid, t.acctgcode [Kode], t.acctgdesc [Deskripsi], (ISNULL((SELECT SUM(crd.amtbalance) FROM QL_crdgl crd WHERE crd.acctgoid=t.acctgoid AND crd.crdgloid < 0),0.0) + t.saldo) [DEC_Saldo] FROM( SELECT tb.acctgoid, tb.acctgcode, acctgdesc, SUM(debet - credit) saldo FROM( SELECT gld.acctgoid, a.acctgcode, a.acctgdesc, CASE WHEN gld.gldbcr='D' THEN SUM(gld.glamtidr) ELSE 0.0 END debet, CASE WHEN gld.gldbcr='C' THEN SUM(gld.glamtidr) ELSE 0.0 END credit FROM Ql_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.glmstoid=gld.glmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE gld.acctgoid IN(" + acctgoidStock + ") AND glm.glflag IN('Post','Closed') GROUP BY gld.gldbcr, gld.acctgoid, a.acctgcode, a.acctgdesc)AS tb GROUP BY tb.acctgoid, tb.acctgcode, acctgdesc) AS t";
                }

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblDashBox");
                if (tbl.Rows.Count > 0)
                {
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            try
                            {
                                decimal decval = 0;
                                if (col.ColumnName.StartsWith("DEC_") && Decimal.TryParse(item, out decval))
                                {
                                    if (decval == 0)
                                        item = "0.00";
                                    else
                                        item = decval.ToString("#,##0.00");
                                }
                                if (col.ColumnName.StartsWith("Tgl Pembelian"))
                                {
                                    item = DateTime.Parse(item).ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                item = dr[col].ToString();
                            }
                            row.Add(col.ColumnName.Replace("DEC_", ""), item);
                            if (!tblcols.Contains(col.ColumnName.Replace("DEC_", "")))
                                tblcols.Add(col.ColumnName.Replace("DEC_", ""));
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblcols, tblrows, title_dash }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSaldoData()
        {
            decimal piutang = 0;
            sSql = "SELECT SUM(tb.saldo) saldo FROM( SELECT con.reftype, con.refoid, con.custoid, c.custname, SUM(con.amttrans - con.amtbayar) saldo FROM Ql_conar con INNER JOIN QL_mstcust c ON c.custoid=con.custoid GROUP BY con.reftype, con.refoid, con.custoid, c.custname) AS tb";
            piutang = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal hutang = 0;
            sSql = "SELECT SUM(tb.saldo) saldo FROM( SELECT con.reftype, con.refoid, con.suppoid, c.suppname, SUM(con.amttrans - con.amtbayar) saldo FROM Ql_conap con INNER JOIN QL_mstsupp c ON c.suppoid=con.suppoid WHERE c.supptype='Hutang' GROUP BY con.reftype, con.refoid, con.suppoid, c.suppname) AS tb";
            hutang = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            decimal hutangLain = 0;
            sSql = "SELECT SUM(tb.saldo) saldo FROM( SELECT con.reftype, con.refoid, con.suppoid, c.suppname, SUM(con.amttrans - con.amtbayar) saldo FROM Ql_conap con INNER JOIN QL_mstsupp c ON c.suppoid=con.suppoid WHERE c.supptype='Other' GROUP BY con.reftype, con.refoid, con.suppoid, c.suppname) AS tb";
            hutangLain = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            string acctgoidKas = ClassFunction.GetDataAcctgOid("VAR_CASH_DASH", CompnyCode);
            decimal kas = 0;
            sSql = "SELECT SUM(tx.saldo) saldo FROM( SELECT t.acctgoid, (ISNULL((SELECT SUM(crd.amtbalance) FROM QL_crdgl crd WHERE crd.acctgoid=t.acctgoid AND crd.crdgloid < 0),0.0) + t.saldo) saldo FROM( SELECT tb.acctgoid, SUM(debet - credit) saldo FROM( SELECT gld.acctgoid, CASE WHEN gld.gldbcr='D' THEN SUM(gld.glamtidr) ELSE 0.0 END debet, CASE WHEN gld.gldbcr='C' THEN SUM(gld.glamtidr) ELSE 0.0 END credit FROM Ql_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.glmstoid=gld.glmstoid WHERE gld.acctgoid IN(" + acctgoidKas + ") AND glm.glflag IN('Post','Closed') GROUP BY gld.gldbcr, gld.acctgoid)AS tb GROUP BY tb.acctgoid) AS t) AS tx";
            kas = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            string acctgoidBank = ClassFunction.GetDataAcctgOid("VAR_BANK_DASH", CompnyCode);
            decimal bank = 0;
            sSql = "SELECT SUM(tx.saldo) saldo FROM( SELECT t.acctgoid, (ISNULL((SELECT SUM(crd.amtbalance) FROM QL_crdgl crd WHERE crd.acctgoid=t.acctgoid AND crd.crdgloid < 0),0.0) + t.saldo) saldo FROM( SELECT tb.acctgoid, SUM(debet - credit) saldo FROM( SELECT gld.acctgoid, CASE WHEN gld.gldbcr='D' THEN SUM(gld.glamtidr) ELSE 0.0 END debet, CASE WHEN gld.gldbcr='C' THEN SUM(gld.glamtidr) ELSE 0.0 END credit FROM Ql_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.glmstoid=gld.glmstoid WHERE gld.acctgoid IN(" + acctgoidBank + ") AND glm.glflag IN('Post','Closed') GROUP BY gld.gldbcr, gld.acctgoid)AS tb GROUP BY tb.acctgoid) AS t) AS tx";
            bank = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            string acctgoidStock = ClassFunction.GetDataAcctgOid("VAR_STOCK", CompnyCode);
            decimal stock = 0;
            sSql = "SELECT SUM(tx.saldo) saldo FROM( SELECT t.acctgoid, (ISNULL((SELECT SUM(crd.amtbalance) FROM QL_crdgl crd WHERE crd.acctgoid=t.acctgoid AND crd.crdgloid < 0),0.0) + t.saldo) saldo FROM( SELECT tb.acctgoid, SUM(debet - credit) saldo FROM( SELECT gld.acctgoid, CASE WHEN gld.gldbcr='D' THEN SUM(gld.glamtidr) ELSE 0.0 END debet, CASE WHEN gld.gldbcr='C' THEN SUM(gld.glamtidr) ELSE 0.0 END credit FROM Ql_trngldtl gld INNER JOIN QL_trnglmst glm ON glm.glmstoid=gld.glmstoid WHERE gld.acctgoid IN(" + acctgoidStock + ") AND glm.glflag IN('Post','Closed') GROUP BY gld.gldbcr, gld.acctgoid)AS tb GROUP BY tb.acctgoid) AS t) AS tx";
            stock = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            JsonResult js = Json(new { piutang, hutang, hutangLain, kas, bank, stock }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAksesDash()
        {
            bool dashkotak = false;  bool dashtable = false; bool dashbarang = false;  bool dashchart = false;
            string Jabatan2 = ((Session["Jabatan"] == null) ? "-" : Session["Jabatan"].ToString());
            List<QL_m04MN> allmenus = new List<QL_m04MN>();

            if (Jabatan2 == "-" || Jabatan2 == "DIREKTUR" || Jabatan2 == "MANAGER")
            {
                dashkotak = true; dashtable = true; dashbarang = true;  dashchart = true;
            }
            else //selain direktur & manager
            {
                if (Session["Role"] != null)
                {
                    allmenus = (List<QL_m04MN>)Session["Role"];
                    allmenus = allmenus.ToList();
                    if (allmenus.Count() > 0)
                    {
                        foreach (var menu in allmenus)
                        {
                            if (menu.mnfileloc == "RAB" | menu.mnfileloc == "SO" | menu.mnfileloc == "SJ" | menu.mnfileloc == "FJ" | menu.mnfileloc == "PO" | menu.mnfileloc == "PB" | menu.mnfileloc == "FB" | menu.mnfileloc == "StockReport/Report")
                            {
                                dashtable = true;
                            }
                            if (menu.mnfileloc == "Barang" | menu.mnfileloc == "Jasa" | menu.mnfileloc == "RABDraft" | menu.mnfileloc == "RABWapu")
                            {
                                dashbarang = true;
                            }
                        }
                    }
                }
                if (Jabatan2 == "SALES")
                {
                    dashtable = true;
                }
            }

            JsonResult js = Json(new { dashkotak, dashtable, dashbarang, dashchart }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult PrintReport()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            string sWhere = "";
            if (Session["Jabatan"] != null && Session["Jabatan"].ToString() == "SALES") sWhere = $" AND (rab.salesadminoid='{Session["UserID"].ToString()}' OR rab.salesoid='{Session["UserID"].ToString()}')";
            sSql = "SELECT *, CASE WHEN rabsoqty <= soqty AND soqty <= sjqty AND sjqty <= siqty AND rabpoqty <= (poqty + bookingstock + fpb) AND poqty <= mrqty AND mrqty <= piqty THEN 'Done' ELSE 'On Progress' END flag, CASE WHEN ar=bayarar AND ar>0 THEN 'Lunas' ELSE 'Belum' END flagar, CASE WHEN ap=bayarap AND ap>0  THEN 'Lunas' ELSE 'Belum' END flagap FROM( SELECT rab.rabmstoid, ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid = rab.rabmstoid AND ISNULL(som.soitemtype, '') = ''),'') rabno, ISNULL((SELECT CONVERT(VARCHAR(10), som.soitemdate, 103) FROM QL_trnsoitemmst som WHERE som.rabmstoid = rab.rabmstoid AND ISNULL(som.soitemtype, '') = ''),'') rabdate, c.custname, rab.projectname, ISNULL((SELECT rd.usname FROM QL_m01US rd WHERE rd.usoid = rab.salesoid),0.0) sales, ISNULL((SELECT SUM(rd.rabqty) FROM QL_trnrabdtl rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) rabsoqty, ISNULL((SELECT SUM(sod.soitemqty) FROM QL_trnsoitemdtl sod INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid = sod.soitemmstoid WHERE som.soitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) soqty, ISNULL((SELECT SUM(sod.shipmentitemqty) FROM QL_trnshipmentitemdtl sod INNER JOIN QL_trnshipmentitemmst som ON som.shipmentitemmstoid = sod.shipmentitemmstoid WHERE som.shipmentitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) sjqty, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.shipmentitemno refno FROM QL_trnshipmentitemmst cbx WHERE cbx.rabmstoid = rab.rabmstoid) ft FOR XML PATH('')), 1, 1, ''),'') sjno, ISNULL((SELECT SUM(sod.aritemqty) FROM QL_trnaritemdtl sod INNER JOIN QL_trnaritemmst som ON som.aritemmstoid = sod.aritemmstoid WHERE som.aritemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) siqty, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.aritemno refno FROM QL_trnaritemmst cbx WHERE cbx.rabmstoid = rab.rabmstoid) ft FOR XML PATH('')), 1, 1, ''),'') sino, ISNULL((SELECT SUM(rd.rabdtl2qty) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid),0.0) +ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.formoid = rab.rabmstoid),0.0) rabpoqty, ISNULL((SELECT SUM(sod.poitemqty) FROM QL_trnpoitemdtl sod INNER JOIN QL_trnpoitemmst som ON som.poitemmstoid = sod.poitemmstoid WHERE som.poitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) poqty, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.poitemno refno FROM QL_trnpoitemmst cbx WHERE cbx.rabmstoid = rab.rabmstoid) ft FOR XML PATH('')), 1, 1, ''),'') pono, ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.formoid = rab.rabmstoid),0.0) bookingstock, ISNULL((SELECT SUM(rd.rabdtl2qty) FROM QL_trnrabdtl2 rd WHERE rd.rabmstoid = rab.rabmstoid AND ISNULL(rd.poassetmstoid, 0) <> 0 AND ISNULL(rd.poassetdtloid, 0) <> 0),0.0) fpb, ISNULL((SELECT SUM(sod.mritemqty) FROM QL_trnmritemdtl sod INNER JOIN QL_trnmritemmst som ON som.mritemmstoid = sod.mritemmstoid WHERE som.mritemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) mrqty, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.mritemno refno FROM QL_trnmritemmst cbx WHERE cbx.rabmstoid = rab.rabmstoid) ft FOR XML PATH('')), 1, 1, ''),'') mrno, ISNULL((SELECT SUM(sod.apitemqty) FROM QL_trnapitemdtl sod INNER JOIN QL_trnapitemmst som ON som.apitemmstoid = sod.apitemmstoid WHERE som.apitemmststatus IN('Approved', 'Post', 'Closed') AND som.rabmstoid = rab.rabmstoid),0.0) piqty, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.apitemno refno FROM QL_trnapitemmst cbx WHERE cbx.rabmstoid = rab.rabmstoid) ft FOR XML PATH('')), 1, 1, ''),'') pino, ISNULL((SELECT SUM(con.amttrans) FROM QL_trnaritemmst arm INNER JOIN QL_conar con ON con.reftype = 'QL_trnaritemmst' AND con.refoid = arm.aritemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) ar, ISNULL((SELECT SUM(con.amtbayar) FROM QL_trnaritemmst arm INNER JOIN QL_conar con ON con.reftype = 'QL_trnaritemmst' AND con.refoid = arm.aritemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) bayarar, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT refno FROM(SELECT DISTINCT c.cashbankno refno FROM QL_trnaritemmst cbx INNER JOIN QL_trnpayar con ON con.reftype = 'QL_trnaritemmst' AND con.refoid = cbx.aritemmstoid INNER JOIN QL_trncashbankmst c ON c.cashbankoid = con.cashbankoid WHERE cbx.rabmstoid = rab.rabmstoid UNION ALL  SELECT DISTINCT c.cashbankno refno FROM QL_trnaritemmst cbx INNER JOIN QL_trnpaydp con ON con.reftype = 'QL_trnaritemmst' AND con.refoid = cbx.aritemmstoid INNER JOIN QL_trncashbankmst c ON c.cashbankoid = con.cashbankoid WHERE cbx.rabmstoid = rab.rabmstoid) AS t) ft FOR XML PATH('')), 1, 1, ''),'') payarno, ISNULL((SELECT SUM(con.amttrans) FROM QL_trnapitemmst arm INNER JOIN QL_conap con ON con.reftype = 'QL_trnapitemmst' AND con.refoid = arm.apitemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) ap, ISNULL((SELECT SUM(con.amtbayar) FROM QL_trnapitemmst arm INNER JOIN QL_conap con ON con.reftype = 'QL_trnapitemmst' AND con.refoid = arm.apitemmstoid WHERE arm.rabmstoid = rab.rabmstoid),0.0) bayarap, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT refno FROM(SELECT DISTINCT c.cashbankno refno FROM QL_trnapitemmst cbx INNER JOIN QL_trnpayap con ON con.reftype = 'QL_trnapitemmst' AND con.refoid = cbx.apitemmstoid INNER JOIN QL_trncashbankmst c ON c.cashbankoid = con.cashbankoid WHERE cbx.rabmstoid = rab.rabmstoid UNION ALL  SELECT DISTINCT c.cashbankno refno FROM QL_trnapitemmst cbx INNER JOIN QL_trnpaydp con ON con.reftype = 'QL_trnapitemmst' AND con.refoid = cbx.apitemmstoid INNER JOIN QL_trncashbankmst c ON c.cashbankoid = con.cashbankoid WHERE cbx.rabmstoid = rab.rabmstoid) AS t) ft FOR XML PATH('')), 1, 1, ''),'') payapno FROM QL_trnrabmst rab INNER JOIN QL_mstcust c ON c.custoid = rab.custoid WHERE ISNULL(rab.rabmststatus, '') IN('Approved') AND ISNULL(rab.rabmstres1, '')= '' "+ sWhere +") AS tb ORDER BY rabdate DESC";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, "statusPengerjaan");
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintUserName", Session["UserName"].ToString());

            ReportDocument report = new ReportDocument();

            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptStatusPengerjaanXls.rpt"));
            report.SetDataSource(dvRpt.ToTable());
            if (rptparam.Count > 0)
                foreach (var item in rptparam)
                    report.SetParameterValue(item.Key, item.Value);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "StatusPengerjaan.xls");
        }
    }
}