﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class MarketingClosingSOController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public MarketingClosingSOController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class prabmst
        {
            public int reqrabmstoid { get; set; }
            public int custoid { get; set; }
            public string reqrabno { get; set; }
            public string reqrabdate { get; set; }
            public string transstatus { get; set; }
            public string reqrabtype { get; set; }
            public string projectname { get; set; }
            public string transnote { get; set; }
            public string custname { get; set; }
            public decimal reqrabgrandtotalamt { get; set; }
        }

        public class rabmst
        {
            public int rabmstoid { get; set; }
            public int custoid { get; set; }
            public string rabno { get; set; }
            public string rabdate { get; set; }
            public string transstatus { get; set; }
            public string rabtype { get; set; }
            public string projectname { get; set; }
            public string transnote { get; set; }
            public string custname { get; set; }
            public string soitem { get; set; }
            public decimal rabgrandtotaljualamt { get; set; }
            public decimal rabgrandtotalbeliamt { get; set; }
            public decimal rabgrandtotaljualjasaamt { get; set; }
            public decimal rabgrandtotalbelijasaamt { get; set; }
            public decimal rabgrandtotalcostamt { get; set; }
        }

        public class reqrabdtl
        {
            public int reqrabmstoid { get; set; }
            public int reqrabdtlseq { get; set; }
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public decimal reqrabqty { get; set; }
            public decimal reqrabdtlnetto { get; set; }
            public string reqrabunit { get; set; }
            public string reqrabdtlnote { get; set; }
            public string reqrabdtlstatus { get; set; }
            public decimal reqrabprice { get; set; }
        }

        public class rabdtl
        {
            public int seq { get; set; }
            public int rabmstoid { get; set; }
            public int transoid { get; set; }
            public string name { get; set; }
            public string transno { get; set; }
            public string transdate { get; set; }
            public decimal transgrandtotal { get; set; }
            public string transnote { get; set; }
            public string transtype { get; set; }
            public string flag { get; set; }
            public string flagrevisi { get; set; }
        }


        [HttpPost]
        public ActionResult GetPRABData()
        {
            List<prabmst> tbl = new List<prabmst>();
            sSql = "SELECT reqrabmstoid, reqrabno, CONVERT(VARCHAR(10), reqrabdate, 103) AS reqrabdate, reqrabtype, projectname, reqrabmststatus AS transstatus, reqrabmstnote AS transnote, c.custname, reqrabgrandtotalamt FROM QL_trnreqrabmst s INNER JOIN QL_mstcust C ON s.custoid=c.custoid AND c.activeflag='ACTIVE' WHERE s.cmpcode='" + CompnyCode + "' AND (reqrabmststatus='Approved')";

            sSql += " ORDER BY reqrabdate DESC, reqrabmstoid DESC";

            tbl = db.Database.SqlQuery<prabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<rabmst> tbl = new List<rabmst>();
            sSql = "SELECT rabmstoid, rabno, CONVERT(VARCHAR(10), rabdate, 103) AS rabdate, rabtype, projectname, rabmststatus AS transstatus, rabmstnote AS transnote, c.custoid, c.custname, rabgrandtotaljualamt, rabgrandtotalbeliamt, ISNULL(rabgrandtotaljualjasaamt,0.0) rabgrandtotaljualjasaamt, rabgrandtotalbelijasaamt, rabgrandtotalcostamt, ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.rabmstoid=s.rabmstoid AND som.soitemtype NOT IN('Jasa')),'') soitemno FROM QL_trnrabmst s INNER JOIN QL_mstcust C ON s.custoid=c.custoid WHERE s.cmpcode='" + CompnyCode + "' AND rabmststatus IN ('Approved') AND ISNULL((SELECT COUNT(*) FROM QL_trnpoitemmst pom WHERE pom.rabmstoid=s.rabmstoid),0)<=0 AND ISNULL((SELECT COUNT(*) FROM QL_trnshipmentitemmst pom WHERE pom.rabmstoid=s.rabmstoid),0)<=0 ";

            sSql += " ORDER BY rabdate DESC, rabmstoid DESC";

            tbl = db.Database.SqlQuery<rabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailData(string sType, int iOid)
        {

            if (sType.ToUpper() == "PRAB")
            {
                List<reqrabdtl> tbl = new List<reqrabdtl>();
                sSql = "SELECT rd.reqrabdtloid , CAST(ROW_NUMBER() OVER(ORDER BY rd.reqrabseq) AS INT) AS reqrabdtlseq, rd.itemoid AS matoid, m.itemcode AS matcode, m.itemdesc AS matlongdesc, reqrabqty, g.gndesc AS reqrabunit, '' AS reqrabdtlnote, rd.reqrabdtlstatus, reqrabprice, reqrabprice * reqrabqty AS reqrabdtlnetto FROM QL_trnreqrabdtl rd INNER JOIN QL_mstitem m ON rd.itemoid=m.itemoid INNER JOIN QL_m05gn g ON rd.reqrabunitoid=g.gnoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.reqrabdtlstatus='' AND rd.reqrabmstoid=" + iOid;
                tbl = db.Database.SqlQuery<reqrabdtl>(sSql).ToList();

                JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
                return js;
            }
            else
            {
                List<rabdtl> tbl = new List<rabdtl>();
                sSql = "SELECT 0 AS seq, rabmstoid, soitemmstoid AS transoid, soitemno AS transno, CONVERT(varchar(10), soitemdate, 103) transdate, c.custname AS name, soitemgrandtotalamt AS transgrandtotal, soitemmstnote AS transnote, 'SO' transtype FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid = som.custoid where rabmstoid = " + iOid;
                //sSql += " UNION ALL";
                //sSql += " SELECT 0 AS seq, rabmstoid, poitemmstoid AS transoid, poitemno AS transno, CONVERT(varchar(10), poitemdate, 103) transdate, s.suppname AS name, poitemgrandtotalamt AS transgrandtotal, poitemmstnote AS transnote, 'PO' transtype FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid where rabmstoid = " + iOid;
                tbl = db.Database.SqlQuery<rabdtl>(sSql).ToList();

                JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
                js.MaxJsonLength = Int32.MaxValue;
                return js;
            }

        }

        [HttpPost]
        public JsonResult SetDataDetailsReq(List<reqrabdtl> dtDtl)
        {
            Session["QL_trnreqrabdtl_closing_so"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsRab(List<rabdtl> dtDtl)
        {
            Session["QL_trnrabdtl_closing_so"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string marktype, string transnote)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<reqrabdtl> dtDtlReq = null;
            if (Session["QL_trnreqrabdtl_closing_so"] != null)
            {
                dtDtlReq = (List<reqrabdtl>)Session["QL_trnreqrabdtl_closing_so"];
            }
            List<rabdtl> dtDtlRab = null;
            if (Session["QL_trnrabdtl_closing_so"] != null)
            {
                dtDtlRab = (List<rabdtl>)Session["QL_trnrabdtl_closing_so"];
            }

            if (marktype == "PRAB")
            {
                if (dtDtlReq != null)
                {
                    if (dtDtlReq[0].reqrabmstoid == 0)
                    {
                        //ModelState.AddModelError("", "Please select SO Data first!");
                    }
                    else
                    {
                        if (dtDtlReq == null)
                            ModelState.AddModelError("", "Please Fill Detail Data!");
                        else if (dtDtlReq.Count() <= 0)
                            ModelState.AddModelError("", "Please Fill Detail Data!");

                        sSql = "SELECT COUNT(*) FROM QL_trnreqrabmst where reqrabmststatus = 'Closed' AND reqrabmstoid = " + dtDtlReq[0].reqrabmstoid + "";
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "Data ini sudah di Closed oleh user yang lain. Silahkan tutup transaksi ini dan ulang kembali!");
                        }

                        //for (var i = 0; i < dtDtlReq.Count(); i++)
                        //{

                        //}
                    }
                    if (ModelState.IsValid)
                    {
                        var servertime = ClassFunction.GetServerTime();
                        using (var objTrans = db.Database.BeginTransaction())
                        {
                            try
                            {
                                sSql = "UPDATE QL_trnreqrabmst set reqrabmststatus = 'Closed', reqrabmstres1 = 'Force Closed', closereason='" + transnote + "', closeuser='" + Session["UserID"].ToString() + "', closetime=CURRENT_TIMESTAMP WHERE reqrabmstoid = " + dtDtlReq[0].reqrabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnreqrabdtl set reqrabdtlstatus = 'Complete' where reqrabmstoid = " + dtDtlReq[0].reqrabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                objTrans.Commit();
                                Session["QL_trnreqrabdtl_closing_so"] = null;
                                return RedirectToAction("Index");
                            }
                            catch (Exception ex)
                            {
                                objTrans.Rollback();
                                ModelState.AddModelError("Error", ex.ToString());
                            }
                        }
                    }
                }
            }
            else
            {
                if (dtDtlRab != null)
                {
                    if (dtDtlRab[0].rabmstoid == 0)
                    {
                        //ModelState.AddModelError("", "Please select SO Data first!");
                    }
                    else
                    {
                        if (dtDtlRab == null)
                            ModelState.AddModelError("", "Please Fill Detail Data!");
                        else if (dtDtlRab.Count() <= 0)
                            ModelState.AddModelError("", "Please Fill Detail Data!");

                        if (dtDtlRab.Count() > 0)
                        {
                            for (var i = 0; i < dtDtlRab.Count(); i++)
                            {
                                ////PO
                                //var cPO = dtDtlRab.Where(m => m.transtype == "PO").Count();
                                //if (cPO > 0)
                                //{
                                //    if (dtDtlRab[i].transtype == "PO" && dtDtlRab[i].flagrevisi == "Belum")
                                //    {
                                //        sSql = "SELECT COUNT(*) FROM QL_trnmritemmst where mritemmststatus='In Process' AND pomstoid = " + dtDtlRab[i].transoid + "";
                                //        var iCheckMR = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //        if (iCheckMR > 0)
                                //        {
                                //            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data PB Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                //        }
                                //        else
                                //        {
                                //            sSql = "SELECT COUNT(*) FROM QL_trnmritemmst where mritemmststatus IN('Post','Closed') AND pomstoid = " + dtDtlRab[i].transoid + "";
                                //            var iCheckMR2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //            if (iCheckMR2 > 0)
                                //            {
                                //                sSql = "SELECT COUNT(*) FROM QL_trnapitemmst where poitemmstoid = " + dtDtlRab[i].transoid + "";
                                //                var iCheckAP2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //                if (iCheckAP2 > 0) //Cek Sudah Di AP Return
                                //                {
                                //                    sSql = "SELECT COUNT(*) FROM QL_trnapretitemmst rt INNER JOIN QL_trnapitemmst ap ON ap.apitemmstoid=rt.apitemmstoid where rt.apretitemmststatus IN('Post','Closed') AND ap.poitemmstoid = " + dtDtlRab[i].transoid + "";
                                //                    var iCheckAPRet2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //                    if (iCheckAPRet2 <= 0)
                                //                    {
                                //                        ModelState.AddModelError("", dtDtlRab[i].transno + " Belum Dibuatkan FB Return!");
                                //                    }
                                //                }
                                //                else //Cek Sudah Di MR Return
                                //                {
                                //                    sSql = "SELECT COUNT(*) FROM QL_trnpretitemmst rt INNER JOIN QL_trnmritemmst mr ON mr.mritemmstoid=rt.mritemmstoid WHERE rt.pretitemmststatus IN('Post','Closed') AND mr.pomstoid = " + dtDtlRab[i].transoid + "";
                                //                    var iCheckMRRet2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //                    if (iCheckMRRet2 <= 0)
                                //                    {
                                //                        ModelState.AddModelError("", dtDtlRab[i].transno + " Belum Dibuatkan PB Return!");
                                //                    }
                                //                }
                                //            }
                                //        }
                                //        sSql = "SELECT COUNT(*) FROM QL_trnpretitemmst rt INNER JOIN QL_trnmritemmst mr ON mr.mritemmstoid=rt.mritemmstoid WHERE rt.pretitemmststatus='In Process' AND mr.pomstoid = " + dtDtlRab[i].transoid + "";
                                //        var iCheckMRRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //        if (iCheckMRRet > 0)
                                //        {
                                //            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data PB Return Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                //        }
                                //        sSql = "SELECT COUNT(*) FROM QL_trnapitemmst where apitemmststatus='In Process' AND poitemmstoid = " + dtDtlRab[i].transoid + "";
                                //        var iCheckAPc = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //        if (iCheckAPc > 0)
                                //        {
                                //            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data FB Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                //        }
                                //        sSql = "SELECT COUNT(*) FROM QL_trnapretitemmst rt INNER JOIN QL_trnapitemmst ap ON ap.apitemmstoid=rt.apitemmstoid where rt.apretitemmststatus='In Process' AND ap.poitemmstoid = " + dtDtlRab[i].transoid + "";
                                //        var iCheckAPRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                //        if (iCheckAPRet > 0)
                                //        {
                                //            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data FB Return Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                //        }
                                //    }
                                //}
                                //SO
                                var cSO = dtDtlRab.Where(m => m.transtype == "SO").Count();
                                if (cSO > 0)
                                {
                                    if (dtDtlRab[i].transtype == "SO")
                                    {
                                        sSql = "SELECT COUNT(*) FROM QL_trnshipmentitemmst where shipmentitemmststatus='In Process' AND soitemmstoid = " + dtDtlRab[i].transoid + "";
                                        var iCheckSJ = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                        if (iCheckSJ > 0)
                                        {
                                            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data SJ Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                        }
                                        else
                                        {
                                            sSql = "SELECT COUNT(*) FROM QL_trnshipmentitemmst where shipmentitemmststatus IN('Post','Closed') AND soitemmstoid = " + dtDtlRab[i].transoid + "";
                                            var iCheckSJ2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                            if (iCheckSJ2 > 0)
                                            {
                                                sSql = "SELECT COUNT(*) FROM QL_trnaritemmst where somstoid = " + dtDtlRab[i].transoid + "";
                                                var iCheckAR2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                                if (iCheckAR2 > 0) //Cek Sudah Di AR Return
                                                {
                                                    sSql = "SELECT COUNT(*) FROM QL_trnarretitemmst rt INNER JOIN QL_trnaritemmst ar ON ar.aritemmstoid=rt.aritemmstoid where rt.arretitemmststatus IN('Post','Closed') AND ar.somstoid = " + dtDtlRab[i].transoid + "";
                                                    var iCheckARRet2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                                    if (iCheckARRet2 <= 0)
                                                    {
                                                        ModelState.AddModelError("", dtDtlRab[i].transno + " Belum Dibuatkan FJ Return!");
                                                    }
                                                }
                                                else //Cek Sudah Di SJ Return
                                                {
                                                    sSql = "SELECT COUNT(*) FROM QL_trnsretitemmst rt INNER JOIN QL_trnshipmentitemmst sj ON sj.shipmentitemmstoid=rt.shipmentitemmstoid where rt.sretitemmststatus IN('Post','Closed') AND sj.soitemmstoid = " + dtDtlRab[i].transoid + "";
                                                    var iCheckSJRet2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                                    if (iCheckSJRet2 <= 0)
                                                    {
                                                        ModelState.AddModelError("", dtDtlRab[i].transno + " Belum Dibuatkan SJ Return!");
                                                    }
                                                }
                                            }
                                        }
                                        sSql = "SELECT COUNT(*) FROM QL_trnsretitemmst rt INNER JOIN QL_trnshipmentitemmst sj ON sj.shipmentitemmstoid=rt.shipmentitemmstoid where rt.sretitemmststatus='In Process' AND sj.soitemmstoid = " + dtDtlRab[i].transoid + "";
                                        var iCheckSJRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                        if (iCheckSJRet > 0)
                                        {
                                            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data SJ Return Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                        }
                                        sSql = "SELECT COUNT(*) FROM QL_trnaritemmst where aritemmststatus='In Process' AND somstoid = " + dtDtlRab[i].transoid + "";
                                        var iCheckAR = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                        if (iCheckAR > 0)
                                        {
                                            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data FJ Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                        }
                                        sSql = "SELECT COUNT(*) FROM QL_trnarretitemmst rt INNER JOIN QL_trnaritemmst ar ON ar.aritemmstoid=rt.aritemmstoid where rt.arretitemmststatus='In Process' AND ar.somstoid = " + dtDtlRab[i].transoid + "";
                                        var iCheckARRet = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                                        if (iCheckARRet > 0)
                                        {
                                            ModelState.AddModelError("", dtDtlRab[i].transno + ", Data FJ Return Masih In Process, Silahkan Hapus Transaksinya Dahulu!");
                                        }
                                    }
                                }
                            }
                        }

                        sSql = "SELECT COUNT(*) FROM QL_trnrabmst where rabmststatus = 'Closed' AND rabmstoid = " + dtDtlRab[0].rabmstoid + "";
                        var iCheck2 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck2 > 0)
                        {
                            ModelState.AddModelError("", "Data ini sudah di Closed oleh user yang lain. Silahkan tutup transaksi ini dan ulang kembali!");
                        }
                        sSql = "SELECT COUNT(*) FROM QL_trnsoitemmst where soitemmststatus = 'In Process' AND rabmstoid = " + dtDtlRab[0].rabmstoid + "";
                        var iCheck3 = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        if (iCheck3 > 0)
                        {
                            ModelState.AddModelError("", "Data SO Masih In Process, Silahkan Posting SO Terlebih Dahulu!");
                        }
                    }
                    if (ModelState.IsValid)
                    {
                        var servertime = ClassFunction.GetServerTime();
                        using (var objTrans = db.Database.BeginTransaction())
                        {
                            try
                            {
                                sSql = "UPDATE QL_trnrabmst set rabmststatus = 'Closed', rabmstres1 = 'Force Closed', closereason='" + transnote + "', closeuser='" + Session["UserID"].ToString() + "', closetime=CURRENT_TIMESTAMP WHERE rabmstoid = " + dtDtlRab[0].rabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_matbooking SET flag='Closed' WHERE cmpcode='" + CompnyCode + "' AND formoid=" + dtDtlRab[0].rabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnsoitemmst set soitemmststatus = 'Closed', soitemmstres1 = 'Force Closed', closereason='" + transnote + "', closeuser='" + Session["UserID"].ToString() + "', closetime=CURRENT_TIMESTAMP where rabmstoid = " + dtDtlRab[0].rabmstoid + "";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                //for (var i = 0; i < dtDtlRab.Count(); i++)
                                //{
                                //    if (dtDtlRab[i].transtype == "PO")
                                //    {
                                //        sSql = "UPDATE QL_trnrabdtl2 SET rabdtl2flagrevisi = '" + dtDtlRab[i].flagrevisi + "' WHERE rabdtl2oid IN(SELECT rabdtl2oid FROM QL_trnpoitemdtl WHERE poitemmstoid=" + dtDtlRab[i].transoid + ")";
                                //        db.Database.ExecuteSqlCommand(sSql);
                                //        db.SaveChanges();

                                //        if (dtDtlRab[i].flagrevisi == "Belum")
                                //        {
                                //            sSql = "UPDATE QL_trnpoitemmst set poitemmststatus = 'Closed', poitemmstres1 = 'Force Closed', closereason='" + transnote + "', closeuser='" + Session["UserID"].ToString() + "', closetime=CURRENT_TIMESTAMP WHERE poitemmstoid=" + dtDtlRab[i].transoid + "";
                                //            db.Database.ExecuteSqlCommand(sSql);
                                //            db.SaveChanges();
                                //        }
                                //    }
                                //}

                                objTrans.Commit();
                                Session["QL_trnrabdtl_closing_so"] = null;
                                return RedirectToAction("Index");
                            }
                            catch (Exception ex)
                            {
                                objTrans.Rollback();
                                ModelState.AddModelError("Error", ex.ToString());
                            }
                        }
                    }
                }
            }
            if (marktype == "PRAB")
            {
                return View(dtDtlReq);
            }
            else
            {
                return View(dtDtlRab);
            }
        }
    }
}