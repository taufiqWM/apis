﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class RABWapuController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public RABWapuController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listDDLsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
        }

        private void InitDDL(QL_trnrabmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS int)";
            var rabpaytermoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabpaytermoid);
            ViewBag.rabpaytermoid = rabpaytermoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PROVINSI' AND gnflag='ACTIVE'";
            var rabprovoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabprovoid);
            ViewBag.rabprovoid = rabprovoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' AND gnflag='ACTIVE'";
            var rabcityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabcityoid);
            ViewBag.rabcityoid = rabcityoid;

            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>("SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'").ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND ISNULL(gnother1,'')='Komersil' AND gnflag='ACTIVE'";
            var whdtl2oid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.whdtl2oid);
            ViewBag.whdtl2oid = whdtl2oid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesadminoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesadminoid);
            ViewBag.salesadminoid = salesadminoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            var m_location_id = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.markup = "";
            foreach (var item in m_location_id)
            {
                ViewBag.markup += "<option value=\'" + item.Value + "\'>" + item.Text + "</option>";
            }
        }

        private void FillAdditionalField(QL_trnrabmst tblmst)
        {
            ViewBag.partnername = db.Database.SqlQuery<string>("SELECT partnername FROM QL_mstpartner a WHERE a.partneroid ='" + tblmst.partneroid + "'").FirstOrDefault();
            ViewBag.salesname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tblmst.salesoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.ToEmail = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 a WHERE a.custdtl2oid ='" + tblmst.alamatoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT reqrabno FROM QL_trnreqrabmst a WHERE a.reqrabmstoid ='" + tblmst.reqrabmstoid + "'").FirstOrDefault();
            ViewBag.rabno_draft = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst_draft a WHERE a.rabmstoid ='" + tblmst.rabmstoid_draft + "'").FirstOrDefault();
            ViewBag.mrserviceno = db.Database.SqlQuery<string>("SELECT mrserviceno FROM QL_trnmrservicemst a WHERE a.mrservicemstoid ='" + tblmst.mrservicemstoid + "'").FirstOrDefault();
            ViewBag.revrabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst a WHERE a.rabmstoid ='" + tblmst.rabmstoid_revisi + "'").FirstOrDefault();
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "RAB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(rabno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnrabmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND rabno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public class rabdtl : QL_trnrabdtl
        {
            public string itemcode { get; set; }
            public string itemspec { get; set; }
            public string pidtype { get; set; }
            public string rabunit { get; set; }
            public decimal stockqty { get; set; }
            public decimal itempricesrp { get; set; }
            public decimal itempricedealer { get; set; }
            public decimal itempricemd { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<rabdtl> tbl = new List<rabdtl>();

            sSql = "SELECT 0 rabseq, 'Master' rabdtlflag_m, i.itemoid, i.itemtype itemdtltype, i.itemtype+'/'+i.pidtype pidtype, i.itemcode, i.itemdesc itemdtldesc, 0.0 rabqty, i.itemunitoid rabunitoid, g.gndesc rabunit, 0.0 rabprice, 0.0 rabdtlamt, 10.0 rabtaxvalue, 0.0 rabtaxamt, 0.0 rabdtlnetto, 1.5 rabpphvalue, 0.0 rabpphamt, '' rabdtlnote, 0.0 rabongkiramt, GETDATE() rabetd, 16 rablocoid, itemspec, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=i.itemoid),0.0) stockqty, itempricesrp, itempricedealer, itempricemd FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' AND i.itemtype IN ('Barang', 'Rakitan', 'Ongkir', 'Jasa') ORDER BY itemcode";
            tbl = db.Database.SqlQuery<rabdtl>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatManualDetail(string itemtype)
        {
            List<rabdtl> tbl = new List<rabdtl>();

            sSql = $"SELECT 0 rabseq, 'Manual' rabdtlflag_m, 0 itemoid, '{itemtype}' itemdtltype, '' itemcode, '' itemdtldesc, 0.0 rabqty, 0 rabunitoid, '' rabunit, 0.0 rabprice, 0.0 rabdtlamt, 10.0 rabtaxvalue, 0.0 rabtaxamt, 0.0 rabdtlnetto, 1.5 rabpphvalue, 0.0 rabpphamt, '' rabdtlnote, 0.0 rabongkiramt, GETDATE() rabetd, 16 rablocoid, '' itemspec, 0.0 stockqty, 0.0 itempricesrp, 0.0 itempricedealer, 0.0 itempricemd";
            tbl = db.Database.SqlQuery<rabdtl>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailDataReq(int reqrabmstoid)
        {
            List<rabdtl> tbl = new List<rabdtl>();

            sSql = $"SELECT rd.reqrabseq rabseq, rd.itemtype itemdtltype, reqrabflag_m rabdtlflag_m, rd.itemoid, ISNULL(i.itemcode,'') itemcode, rd.itemdesc itemdtldesc, rd.reqrabqty rabqty, rd.reqrabunitoid rabunitoid, ISNULL(g.gndesc,'') rabunit, rd.reqrabprice rabprice, rd.reqrabdtlamt rabdtlamt, rd.reqrabtaxvalue rabtaxvalue, rd.reqrabtaxamt rabtaxamt, rd.reqrabdtlnetto rabdtlnetto, rd.reqrabpphvalue rabpphvalue, rd.reqrabpphamt rabpphamt, '' rabdtlnote, 0.0 rabongkiramt, GETDATE() rabetd, 16 rablocoid, ISNULL(itemspec,'') itemspec, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=rd.itemoid),0.0) stockqty, ISNULL(itempricesrp,0.0) itempricesrp, ISNULL(itempricedealer,0.0) itempricedealer, ISNULL(itempricemd,0.0) itempricemd FROM QL_trnreqrabdtl rd LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemoid LEFT JOIN QL_m05GN g ON g.gnoid=rd.reqrabunitoid WHERE rd.cmpcode='{CompnyCode}' AND rd.reqrabmstoid={reqrabmstoid} ORDER BY rd.reqrabseq";
            tbl = db.Database.SqlQuery<rabdtl>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabdtl2 : QL_trnrabdtl2
        {
            public string suppdtl2name { get; set; }
            public string suppdtl2taxtype { get; set; }
            public string itemrefdesc { get; set; }
            public string itemcode { get; set; }
            public string rabdtl2unit { get; set; }
            public string rabdtl2locdesc { get; set; }
            public string poassetno { get; set; }
            public string poassetsuppref { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail2(string filtermat)
        {
            List<rabdtl2> tbl = new List<rabdtl2>();

            var sPlus = "";
            if (!string.IsNullOrEmpty(filtermat))
            {
                sPlus = " AND i.itemoid IN(" + filtermat + ")";
            }

            sSql = "SELECT 0 rabdtl2seq, 'Master' rabdtl2flag_m, 0 suppdtl2oid, '' suppdtl2name, '' suppdtl2taxtype, i.itemoid itemdtl2oid, i.itemcode, i.itemdesc itemdtl2desc, 0.0 rabdtl2qty, i.itemunitoid rabdtl2unitoid, g.gndesc rabdtl2unit, 0.0 rabdtl2price, 0.0 rabdtl2qtybooking, 0.0 rabdtl2amt, 0.0 rabdtl2discvalue, 0.0 rabdtl2discamt, 0.0 rabdtl2taxvalue, 0.0 rabdtl2taxamt, 0.0 rabdtl2netto, '' rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, 16 rabdtl2locoid, '' rabdtl2locdesc, 0 itemdtl2refoid, '' itemrefdesc, 0.0 rabdtl2amtbooking, 'Belum' rabdtl2flagrevisi, 0 poassetmstoid, 0 poassetdtloid, '' poassetno FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' " + sPlus + " AND i.itemtype IN ('Barang', 'Ongkir')  UNION ALL  SELECT 0 rabdtl2seq, 'Master' rabdtl2flag_m, 0 suppdtl2oid, '' suppdtl2name, '' suppdtl2taxtype, itm.itemoid itemdtl2oid, itm.itemcode, itm.itemdesc, 0.0 rabdtl2qty, itm.itemunitoid rabdtl2unitoid, g.gndesc rabdtl2unit, 0.0 rabdtl2price, 0.0 rabdtl2qtybooking, 0.0 rabdtl2amt, 0.0 rabdtl2discvalue, 0.0 rabdtl2discamt, 0.0 rabdtl2taxvalue, 0.0 rabdtl2taxamt, 0.0 rabdtl2netto, '' rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, 16 rabdtl2locoid, '' rabdtl2locdesc, i.itemoid itemdtl2refoid, i.itemdesc itemrefdesc, 0.0 rabdtl2amtbooking, 'Belum' rabdtl2flagrevisi, 0 poassetmstoid, 0 poassetdtloid, '' poassetno FROM QL_mstitem i INNER JOIN QL_mstitemdtl2 itd ON itd.itemoid=i.itemoid INNER JOIN QL_mstitem itm ON itm.itemoid=itd.itemrefoid INNER JOIN QL_m05GN g ON g.gnoid=itd.itemrefunitoid WHERE itm.activeflag='ACTIVE' " + sPlus + " AND i.itemtype IN ('Rakitan')";
            tbl = db.Database.SqlQuery<rabdtl2>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatManualDetail2(string itemtype)
        {
            List<rabdtl2> tbl = new List<rabdtl2>();

            sSql = $"SELECT 0 rabdtl2seq, 'Manual' rabdtl2flag_m, 0 suppdtl2oid, '' suppdtl2name, '' suppdtl2taxtype, 0 itemdtl2oid, '' itemcode, '' itemdtl2desc, 0.0 rabdtl2qty, 0 rabdtl2unitoid, '' rabdtl2unit, 0.0 rabdtl2price, 0.0 rabdtl2qtybooking, 0.0 rabdtl2amt, 0.0 rabdtl2discvalue, 0.0 rabdtl2discamt, 0.0 rabdtl2taxvalue, 0.0 rabdtl2taxamt, 0.0 rabdtl2netto, '' rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, 16 rabdtl2locoid, '' rabdtl2locdesc, 0 itemdtl2refoid, '' itemrefdesc, 0.0 rabdtl2amtbooking, 'Belum' rabdtl2flagrevisi, 0 poassetmstoid, 0 poassetdtloid, '' poassetno";
            tbl = db.Database.SqlQuery<rabdtl2>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailPOAsset(string filtermat)
        {
            List<rabdtl2> tbl = new List<rabdtl2>();
            string sPlus = "";
            if (filtermat != "")
            {
                sPlus += " AND i.itemoid IN(SELECT DISTINCT itemoid FROM( SELECT i.itemoid itemoid FROM QL_mstitem i WHERE i.itemoid IN(" + filtermat + ") AND i.itemtype IN('Barang') UNION ALL  SELECT i2.itemrefoid itemoid FROM QL_mstitemdtl2 i2 INNER JOIN QL_mstitem i ON i.itemoid=i2.itemrefoid WHERE i2.itemoid IN(" + filtermat + ") /*AND i.itemtype IN('Rakitan')*/)AS t)";
            }
            sSql = "SELECT 0 rabdtl2seq, 'Master' rabdtl2flag_m, s.suppoid suppdtl2oid, s.suppname suppdtl2name, case when supppajak=1 then 'TAX' else 'NON TAX' end suppdtl2taxtype, i.itemoid itemdtl2oid, i.itemcode, i.itemdesc itemdtl2desc, (poassetqty - ISNULL((SELECT SUM(rx.rabdtl2qty) FROM QL_trnrabdtl2 rx INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=rx.rabmstoid WHERE rm.rabmststatus IN('In Approval', 'Approved') AND ISNULL(rx.poassetdtloid,0)=pod.poassetdtloid AND ISNULL(rx.poassetmstoid,0)=pod.poassetmstoid),0.0)) rabdtl2qty, poassetunitoid rabdtl2unitoid, g.gndesc rabdtl2unit, poassetprice rabdtl2price, 0.0 rabdtl2qtybooking, poassetdtlamt rabdtl2amt, poassetdtldiscvalue rabdtl2discvalue, poassetdtldiscamt rabdtl2discamt, pod.poassetdtltaxvalue rabdtl2taxvalue, pod.poassetdtltaxamt rabdtl2taxamt, pod.poassetdtlnetto rabdtl2netto, pod.poassetdtlnote rabdtl2note, 0.0 rabdtl2ongkiramt, GETDATE() rabdtl2eta, CONVERT(CHAR(10),GETDATE(),103) rabdtl2etastr, 0 rabdtl2locoid, '' rabdtl2locdesc, ISNULL((SELECT TOP 1 i2.itemoid FROM QL_mstitemdtl2 i2 WHERE i2.itemrefoid=i.itemoid AND i2.itemoid IN(" + filtermat + ")),0) itemdtl2refoid, ISNULL((SELECT TOP 1 ix.itemdesc FROM QL_mstitemdtl2 i2 INNER JOIN QL_mstitem ix ON ix.itemoid=i2.itemoid WHERE i2.itemrefoid=i.itemoid AND i2.itemoid IN(" + filtermat + ")),'') itemrefdesc, 0.0 stockqty, 0.0 rabdtl2amtbooking, 'Terpenuhi' rabdtl2flagrevisi, pom.poassetmstoid, pod.poassetdtloid, pom.poassetno, pom.poassetsuppref FROM QL_trnpoassetmst pom INNER JOIN QL_trnpoassetdtl pod ON pod.poassetmstoid=pom.poassetmstoid INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid INNER JOIN QL_mstitem i ON i.itemoid=pod.poassetrefoid INNER JOIN QL_m05GN g ON g.gnoid=pod.poassetunitoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poassettype IN('FPB','Stock') AND pom.poassetmststatus IN('Approved','Closed') AND ISNULL(pom.poassetmstres1,'')='' AND (poassetqty - ISNULL((SELECT SUM(rx.rabdtl2qty) FROM QL_trnrabdtl2 rx INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=rx.rabmstoid WHERE rm.rabmststatus IN('In Approval', 'Approved') AND ISNULL(rx.poassetdtloid,0)=pod.poassetdtloid AND ISNULL(rx.poassetmstoid,0)=pod.poassetmstoid),0.0))>0  /*AND pod.poassetdtloid NOT IN(SELECT DISTINCT ISNULL(poassetdtloid,0) FROM QL_trnrabdtl2)*/ " + sPlus + " ORDER BY pod.poassetdtlseq";

            tbl = db.Database.SqlQuery<rabdtl2>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class matbooking : QL_matbooking
        {
            public int matbookingseq { get; set; }
            public string refcode { get; set; }
            public string refdesc { get; set; }
            public string mtrwhdesc { get; set; }
            public int unitoid { get; set; }
            public string unitdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatBooking(string filtermat, int mtrwhoid)
        {
            List<matbooking> tbl = new List<matbooking>();

            sSql = "SELECT *, (tbl.saldo - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.refoid AND mb.mtrwhoid=tbl.mtrwhoid AND ISNULL(mb.refno,'')=ISNULL(tbl.refno,'') AND ISNULL(mb.serialnumber,'')=ISNULL(tbl.serialnumber,'') AND mb.flag='Post'),0.0)) AS qtyout, CASE WHEN tbl.saldo>0 AND tbl.valueidrmat>0 THEN CAST((tbl.valueidrmat) AS DECIMAL(18,4)) ELSE 0 END valueidr, 10.00 taxvalue, CASE WHEN tbl.saldo>0 AND tbl.valueidrmat>0 THEN CAST(((tbl.valueidrmat) * 0.1) AS DECIMAL(18,4)) ELSE 0 END taxamt, CASE WHEN tbl.saldo>0 AND tbl.valueidrmat>0 THEN CAST((tbl.valueidrmat) + ((tbl.valueidrmat) * 0.1) AS DECIMAL(18,4)) ELSE 0 END amtnetto FROM( SELECT 0 matbookingseq, itemoid refoid, itemcode refcode, itemdesc refdesc, itemunitoid unitoid, g.gndesc unitdesc, c.mtrwhoid, g2.gndesc mtrwhdesc, SUM(qtyin - qtyout) saldo, ISNULL(c.refno,'') refno, ISNULL(c.serialnumber,'') serialnumber, 0 rabmstoid, CAST(ISNULL((SELECT TOP 1 (con.valueidr/(con.qtyin+con.qtyout)) FROM QL_conmat con WHERE con.refoid=c.refoid ORDER BY con.updtime DESC),0.0) AS DECIMAL(18,4)) valueidrmat FROM QL_mstitem i INNER JOIN QL_conmat c ON c.refoid = i.itemoid INNER JOIN QL_m05GN g ON g.gnoid = i.itemunitoid INNER JOIN QL_m05GN g2 ON g2.gnoid = c.mtrwhoid WHERE i.activeflag = 'ACTIVE' AND c.refoid IN(" + filtermat + ") AND c.mtrwhoid=" + mtrwhoid + " AND i.itemtype IN('Barang') AND ISNULL(rabmstoid,0)=0 GROUP BY itemoid, itemcode, itemdesc, itemunitoid, g.gndesc, c.mtrwhoid, g2.gndesc, ISNULL(c.refno, ''), ISNULL(c.serialnumber, ''), c.refoid ) AS tbl WHERE (tbl.saldo - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.refoid AND mb.mtrwhoid=tbl.mtrwhoid AND ISNULL(mb.refno,'')=ISNULL(tbl.refno,'') AND ISNULL(mb.serialnumber,'')=ISNULL(tbl.serialnumber,'') AND mb.flag='Post'),0.0))>0 ORDER BY tbl.refcode";
            tbl = db.Database.SqlQuery<matbooking>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabdtl4 : QL_trnrabdtl4
        {
            public string suppdtl4name { get; set; }
            public string suppdtl4taxtype { get; set; }
            public string jasacode { get; set; }
            public string rabdtl4unit { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail4()
        {
            List<rabdtl4> tbl = new List<rabdtl4>();

            sSql = "SELECT 0 rabdtl4seq, 'Master' rabdtl4flag_m, 0 suppdtl4oid, '' suppdtl4name, '' suppdtl4taxtype, i.jasaoid itemdtl4oid, i.jasacode, i.jasadesc itemdtl4desc, 0.0 rabdtl4qty, i.jasaunitoid rabdtl4unitoid, g.gndesc rabdtl4unit, 0.0 rabdtl4price, 0.0 rabdtl4discvalue, 0.0 rabdtl4discamt, 0.0 rabdtl4amt, 2.0 rabdtl4pphvalue, 0.0 rabdtl4pphamt, 0.0 rabdtl4taxvalue, 0.0 rabdtl4taxamt, 0.0 rabdtl4netto, '' rabdtl4note, GETDATE() rabdtl4eta FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE activeflag='ACTIVE' ORDER BY jasacode";
            tbl = db.Database.SqlQuery<rabdtl4>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatManualDetail4()
        {
            List<rabdtl4> tbl = new List<rabdtl4>();

            sSql = $"SELECT 0 rabdtl4seq, 'Manual' rabdtl4flag_m, 0 suppdtl4oid, '' suppdtl4name, '' suppdtl4taxtype, 0 itemdtl4oid, '' jasacode, '' itemdtl4desc, 0.0 rabdtl4qty, 0 rabdtl4unitoid, '' rabdtl4unit, 0.0 rabdtl4price, 0.0 rabdtl4discvalue, 0.0 rabdtl4discamt, 0.0 rabdtl4amt, 2.0 rabdtl4pphvalue, 0.0 rabdtl4pphamt, 0.0 rabdtl4taxvalue, 0.0 rabdtl4taxamt, 0.0 rabdtl4netto, '' rabdtl4note, GETDATE() rabdtl4eta";
            tbl = db.Database.SqlQuery<rabdtl4>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabdtl5 : QL_trnrabdtl5
        {
            public string acctgcode { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataMatDetail5(string sVar)
        {
            List<rabdtl5> tbl = new List<rabdtl5>();

            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT 0 rabdtl5seq, 'Master' rabdtl5flag_m, a.acctgoid, a.acctgcode, a.acctgdesc itemdtl5desc, 0.0 rabdtl5price, '' rabdtl5note FROM QL_mstacctg a WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<rabdtl5>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetDataMatManualDetail5()
        {
            List<rabdtl5> tbl = new List<rabdtl5>();

            sSql = $"SELECT 0 rabdtl5seq, 'Manual' rabdtl5flag_m, 0 acctgoid, '' acctgcode, '' itemdtl5desc, 0.0 rabdtl5price, '' rabdtl5note";
            tbl = db.Database.SqlQuery<rabdtl5>(sSql).ToList();

            JsonResult js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult FillDetailData(int id, string rabflag)
        {
            JsonResult js = null;
            var rabflag_tbl = "";
            if (rabflag == "draft")
            {
                rabflag_tbl = "_draft";
            }
            try
            {
                var tbl = db.Database.SqlQuery<rabdtl>("SELECT rd.rabseq, ISNULL(rd.itemdtltype,ISNULL(i.itemtype,'')) itemdtltype, ISNULL(rabdtlflag_m,'Master') rabdtlflag_m, rd.itemoid, ISNULL(i.itemcode,'') itemcode, ISNULL(itemdtldesc,ISNULL(i.itemdesc,'')) itemdtldesc, rd.rabqty, rd.rabunitoid, ISNULL(g.gndesc,'') rabunit, rd.rabprice, rd.rabdtlamt, rd.rabtaxvalue, rd.rabtaxamt, rd.rabdtlnetto, rd.rabpphvalue, rd.rabpphamt, rd.rabdtlnote, rd.rabongkiramt, rd.rabetd, rd.rablocoid, ISNULL(itemspec,'') itemspec, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat con WHERE con.refoid=rd.itemoid),0.0) stockqty, ISNULL(itempricesrp,0.0) itempricesrp, ISNULL(itempricedealer,0.0) itempricedealer, ISNULL(itempricemd,0.0) itempricemd, rd.rabdtloid_draft FROM QL_trnrabdtl" + rabflag_tbl + " rd LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemoid LEFT JOIN QL_m05GN g ON g.gnoid=rd.rabunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabseq").ToList();
                if (tbl != null)
                {
                    if (tbl.Count > 0)
                    {
                        for (int i = 0; i < tbl.Count(); i++)
                        {
                            if (tbl[i].itemoid == 0) 
                            {
                                tbl[i].itemoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 itemoid FROM QL_mstitem WHERE ISNULL(rabdtloid_draft,0)="+ (tbl[i].rabdtloid_draft ?? 0) + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                tbl[i].itemcode = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 itemcode FROM QL_mstitem WHERE ISNULL(rabdtloid_draft,0)=" + (tbl[i].rabdtloid_draft ?? 0) + " ORDER BY updtime DESC),'') AS t").FirstOrDefault();
                                if (tbl[i].itemoid != 0)
                                {
                                    tbl[i].itemdtldesc = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 itemdesc FROM QL_mstitem WHERE ISNULL(rabdtloid_draft,0)=" + (tbl[i].rabdtloid_draft ?? 0) + " ORDER BY updtime DESC),'') AS t").FirstOrDefault();
                                }
                                tbl[i].rabunitoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 itemunitoid FROM QL_mstitem WHERE ISNULL(rabdtloid_draft,0)=" + (tbl[i].rabdtloid_draft ?? 0) + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                tbl[i].rabunit = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 g.gndesc FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE ISNULL(rabdtloid_draft,0)=" + (tbl[i].rabdtloid_draft ?? 0) + " ORDER BY i.updtime DESC),'') AS t").FirstOrDefault();
                            }
                        }
                    }
                }

                var tbl2 = db.Database.SqlQuery<rabdtl2>("SELECT rd.rabdtl2seq, ISNULL(rabdtl2flag_m,'Master') rabdtl2flag_m, rd.suppdtl2oid, ISNULL(s.suppname,'') suppdtl2name, case when supppajak=1 then 'TAX' else 'NON TAX' end suppdtl2taxtype, rd.itemdtl2oid, ISNULL(i.itemcode,'') itemcode, ISNULL(itemdtl2desc,ISNULL(i.itemdesc,'')) itemdtl2desc, rd.rabdtl2qty, rd.rabdtl2unitoid, ISNULL(g.gndesc,'') rabdtl2unit, rd.rabdtl2price, rd.rabdtl2qtybooking, rd.rabdtl2amt, rd.rabdtl2discvalue, rd.rabdtl2discamt, rd.rabdtl2taxvalue, rd.rabdtl2taxamt, rd.rabdtl2netto, rd.rabdtl2note, rd.rabdtl2ongkiramt, rd.rabdtl2eta, rd.rabdtl2locoid, ISNULL(g2.gndesc,'') rabdtl2locdesc, ISNULL(rd.itemdtl2refoid,0) itemdtl2refoid, ISNULL((SELECT ix.itemdesc FROM QL_mstitem ix WHERE ix.itemoid=ISNULL(rd.itemdtl2refoid,0)),'') itemrefdesc, rabdtl2amtbooking, rabdtl2flagrevisi, ISNULL(poassetmstoid,0) poassetmstoid, ISNULL(poassetdtloid,0) poassetdtloid, ISNULL((SELECT pom.poassetno FROM QL_trnpoassetmst pom WHERE pom.poassetmstoid=ISNULL(rd.poassetmstoid,0)),'') poassetno, rd.rabdtloid2_draft FROM QL_trnrabdtl2" + rabflag_tbl + " rd LEFT JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl2oid LEFT JOIN QL_mstitem i ON i.itemoid=rd.itemdtl2oid LEFT JOIN QL_m05GN g ON g.gnoid=rd.rabdtl2unitoid LEFT JOIN QL_m05GN g2 ON g2.gnoid=rd.rabdtl2locoid  WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl2seq").ToList();
                if (tbl2 != null)
                {
                    if (tbl2.Count > 0)
                    {
                        for (int i = 0; i < tbl2.Count(); i++)
                        {
                            if (tbl2[i].itemdtl2oid == 0)
                            {
                                tbl2[i].itemdtl2oid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 itemoid FROM QL_mstitem WHERE ISNULL(rabdtloid2_draft,0)=" + (tbl2[i].rabdtloid2_draft ?? 0) + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                tbl2[i].itemcode = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 itemcode FROM QL_mstitem WHERE ISNULL(rabdtloid2_draft,0)=" + (tbl2[i].rabdtloid2_draft ?? 0) + " ORDER BY updtime DESC),'') AS t").FirstOrDefault();
                                if (tbl2[i].itemdtl2oid != 0)
                                {
                                    tbl2[i].itemdtl2desc = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 itemdesc FROM QL_mstitem WHERE ISNULL(rabdtloid2_draft,0)=" + (tbl2[i].rabdtloid2_draft ?? 0) + " ORDER BY updtime DESC),'') AS t").FirstOrDefault();

                                    int cekRakitan = db.Database.SqlQuery<int>("SELECT COUNT(*) AS hitung FROM QL_mstitemdtl2 WHERE ISNULL(itemrefoid,0)=" + tbl2[i].itemdtl2oid + "").FirstOrDefault();
                                    if (cekRakitan > 0)
                                    {
                                        tbl2[i].itemdtl2refoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 itemoid FROM QL_mstitemdtl2 WHERE ISNULL(itemrefoid,0)=" + tbl2[i].itemdtl2oid + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                    }
                                }
                                tbl2[i].rabdtl2unitoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 itemunitoid FROM QL_mstitem WHERE ISNULL(rabdtloid2_draft,0)=" + (tbl2[i].rabdtloid2_draft ?? 0) + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                tbl2[i].rabdtl2unit = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 g.gndesc FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE ISNULL(rabdtloid2_draft,0)=" + (tbl2[i].rabdtloid2_draft ?? 0) + " ORDER BY i.updtime DESC),'') AS t").FirstOrDefault();
                            }
                        }
                    }
                }

                var tblmb = new List<matbooking>();
                if (rabflag != "draft")
                {
                    tblmb = db.Database.SqlQuery<matbooking>("SELECT 0 AS matbookingseq, c.refoid, i.itemcode refcode, i.itemdesc refdesc, i.itemunitoid unitoid, g.gndesc unitdesc, c.mtrwhoid, g2.gndesc mtrwhdesc, c.qtyout, c.refno, ISNULL(c.serialnumber,'') serialnumber, ISNULL(c.rabmstoid,0) rabmstoid, c.valueidr, c.taxvalue, c.taxamt, c.amtnetto FROM QL_mstitem i INNER JOIN QL_matbooking c ON c.refoid=i.itemoid INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid INNER JOIN QL_m05GN g2 ON g2.gnoid=c.mtrwhoid WHERE c.formoid=" + id + " ORDER BY c.matbookingoid").ToList();
                    if (tblmb != null)
                    {
                        if (tblmb.Count > 0)
                        {
                            for (int i = 0; i < tblmb.Count(); i++)
                            {
                                tblmb[i].matbookingseq = i + 1;
                            }
                        }
                    }
                }  

                var tbl4 = db.Database.SqlQuery<rabdtl4>("SELECT rd.rabdtl4seq, ISNULL(rabdtl4flag_m,'Master') rabdtl4flag_m, rd.suppdtl4oid, ISNULL(s.suppname,'') suppdtl4name, case when supppajak=1 then 'TAX' else 'NON TAX' end suppdtl4taxtype, rd.itemdtl4oid, ISNULL(i.jasacode,'') jasacode, ISNULL(itemdtl4desc,ISNULL(i.jasadesc,'')) itemdtl4desc, rd.rabdtl4qty, rd.rabdtl4unitoid, ISNULL(g.gndesc,'') rabdtl4unit, rd.rabdtl4price, rd.rabdtl4discvalue, rd.rabdtl4discamt, rd.rabdtl4amt, rd.rabdtl4pphvalue, rd.rabdtl4pphamt, rd.rabdtl4taxvalue, rd.rabdtl4taxamt, rd.rabdtl4netto, rd.rabdtl4note, rd.rabdtl4eta, rd.rabdtloid4_draft FROM QL_trnrabdtl4" + rabflag_tbl + " rd LEFT JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl4oid LEFT JOIN QL_mstjasa i ON i.jasaoid=rd.itemdtl4oid LEFT JOIN QL_m05GN g ON g.gnoid=rd.rabdtl4unitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl4seq").ToList();
                if (tbl4 != null)
                {
                    if (tbl4.Count > 0)
                    {
                        for (int i = 0; i < tbl4.Count(); i++)
                        {
                            if (tbl4[i].itemdtl4oid == 0)
                            {
                                tbl4[i].itemdtl4oid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 jasaoid FROM QL_mstjasa WHERE ISNULL(rabdtloid4_draft,0)=" + (tbl4[i].rabdtloid4_draft ?? 0) + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                tbl4[i].jasacode = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 jasacode FROM QL_mstjasa WHERE ISNULL(rabdtloid4_draft,0)=" + (tbl4[i].rabdtloid4_draft ?? 0) + " ORDER BY updtime DESC),'') AS t").FirstOrDefault();
                                if (tbl4[i].itemdtl4oid != 0)
                                {
                                    tbl4[i].itemdtl4desc = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 jasadesc FROM QL_mstjasa WHERE ISNULL(rabdtloid4_draft,0)=" + (tbl4[i].rabdtloid4_draft ?? 0) + " ORDER BY updtime DESC),'') AS t").FirstOrDefault();
                                }
                                tbl4[i].rabdtl4unitoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 jasaunitoid FROM QL_mstjasa WHERE ISNULL(rabdtloid4_draft,0)=" + (tbl4[i].rabdtloid4_draft ?? 0) + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                tbl4[i].rabdtl4unit = db.Database.SqlQuery<string>("SELECT ISNULL((SELECT TOP 1 g.gndesc FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE ISNULL(rabdtloid4_draft,0)=" + (tbl4[i].rabdtloid4_draft ?? 0) + " ORDER BY i.updtime DESC),'') AS t").FirstOrDefault();
                            }
                        }
                    }
                }

                var tbl5 = db.Database.SqlQuery<rabdtl5>("SELECT rd.rabdtl5seq, ISNULL(rabdtl5flag_m,'Master') rabdtl5flag_m, rd.acctgoid, ISNULL(a.acctgcode,'') acctgcode, ISNULL(itemdtl5desc,ISNULL(a.acctgdesc,'')) itemdtl5desc, rd.rabdtl5price, rd.rabdtl5note FROM QL_trnrabdtl5" + rabflag_tbl + " rd LEFT JOIN QL_mstacctg a ON a.acctgoid=rd.acctgoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl5seq").ToList();

                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {
                    js = Json(new { result = "", tbl, tbl2, tblmb, tbl4, tbl5 }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public class rabmst : QL_trnrabmst
        {
            public string rabnonew { get; set; }
            public string reqrabno { get; set; }
            public string mrserviceno { get; set; }
            public string custname { get; set; }
            public int custpaymentoid { get; set; }
            public string custemail { get; set; }
        }

        [HttpPost]
        public ActionResult GetRevRABData()
        {
            List<rabmst> tbl = new List<rabmst>();

            sSql = "SELECT req.rabmstoid, req.revrabmstoid, req.rabno, '' rabnonew, req.rabtype, req.rabtype2, rabtypereq, reqrabmstoid, ISNULL((SELECT q.reqrabno FROM QL_trnreqrabmst q WHERE q.reqrabmstoid=req.reqrabmstoid),'') reqrabno, rabtypesrv, mrservicemstoid, ISNULL((SELECT q.mrserviceno FROM QL_trnmrservicemst q WHERE q.mrservicemstoid=req.mrservicemstoid),'') mrserviceno, req.rabestdate, req.rabdate, req.deptoid, req.salesoid, req.salesadminoid, c.custname, c.custoid, c.custemail, c.custpaymentoid, c.custprovinceoid rabprovoid, c.custcityoid rabcityoid, req.projectname, ISNULL(req.rabmstnote, '') rabmstnote, req.curroid, rabtotaljualamt, rabtotaljualtaxamt, rabtotaljualpphamt, rabgrandtotaljualamt, rabtotalbeliamt, rabtotalbelitaxamt, rabgrandtotalbeliamt, rabtotalbelijasaamt, rabtotalbelijasataxamt, rabtotalbelijasapphamt, rabgrandtotalbelijasaamt, rabgrandtotalcostamt, rabtotalmargin, rabpersenmargin, req.alamatoid, c2.custdtl2addr alamat, req.partneroid, ISNULL((SELECT p.partnername FROM QL_mstpartner p WHERE p.partneroid=req.partneroid),'') partnername, ISNULL(req.partnerpersen,0.0) partnerpersen, ISNULL(req.partneramt,0.0) partneramt FROM QL_trnrabmst req INNER JOIN QL_mstcust c ON c.custoid=req.custoid INNER JOIN QL_mstcustdtl2 c2 ON custdtl2oid=req.alamatoid AND c2.custoid=req.custoid WHERE rabmststatus IN('Closed') AND req.rabtype='WAPU' ORDER BY rabdate DESC";
            tbl = db.Database.SqlQuery<rabmst>(sSql).ToList();
            for (int i = 0; i < tbl.Count; i++)
            {
                string sNo = ClassFunction.Left(tbl[i].rabno, 15) + "-R";
                sSql = "SELECT ISNULL(MAX(CAST(REPLACE(rabno, '" + sNo + "', '') AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnrabmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND rabno LIKE '" + sNo + "%'";
                string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), 0);
                sNo = sNo + sCounter;

                tbl[i].rabnonew = sNo;
            }

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class rabmst_draft : QL_trnrabmst_draft
        {
            public string reqrabno { get; set; }
            public string mrserviceno { get; set; }
            public string custname { get; set; }
            public int custpaymentoid { get; set; }
            public string custemail { get; set; }
        }

        [HttpPost]
        public ActionResult GetRABDraftData()
        {
            List<rabmst_draft> tbl = new List<rabmst_draft>();

            sSql = "SELECT req.rabmstoid, req.revrabmstoid, req.rabno, req.rabtype, req.rabtype2, rabtypereq, reqrabmstoid, ISNULL((SELECT q.reqrabno FROM QL_trnreqrabmst q WHERE q.reqrabmstoid=req.reqrabmstoid),'') reqrabno, rabtypesrv, mrservicemstoid, '' mrserviceno, req.rabestdate, req.rabdate, req.deptoid, req.salesoid, req.salesadminoid, c.custname, c.custoid, c.custemail, c.custpaymentoid, c.custprovinceoid rabprovoid, c.custcityoid rabcityoid, req.projectname, ISNULL(req.rabmstnote, '') rabmstnote, req.curroid, rabtotaljualamt, rabtotaljualtaxamt, rabtotaljualpphamt, rabgrandtotaljualamt, rabtotalbeliamt, rabtotalbelitaxamt, rabgrandtotalbeliamt, rabtotalbelijasaamt, rabtotalbelijasataxamt, rabtotalbelijasapphamt, rabgrandtotalbelijasaamt, rabgrandtotalcostamt, rabtotalmargin, rabpersenmargin, req.alamatoid, c2.custdtl2addr alamat, req.partneroid, ISNULL((SELECT p.partnername FROM QL_mstpartner p WHERE p.partneroid=req.partneroid),'') partnername, ISNULL(req.partnerpersen,0.0) partnerpersen, ISNULL(req.partneramt,0.0) partneramt FROM QL_trnrabmst_draft req INNER JOIN QL_mstcust c ON c.custoid=req.custoid INNER JOIN QL_mstcustdtl2 c2 ON custdtl2oid=req.alamatoid AND c2.custoid=req.custoid WHERE rabmststatus IN('Post','Approved') AND req.rabmstoid NOT IN(SELECT ISNULL(rabmstoid_draft,0) mstoid FROM QL_trnrabmst) ORDER BY req.rabdate DESC";
            tbl = db.Database.SqlQuery<rabmst_draft>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class reqrabmst : QL_trnreqrabmst
        {
            public string custname { get; set; }
        }

        [HttpPost]
        public ActionResult GetPRABData()
        {
            List<reqrabmst> tbl = new List<reqrabmst>();

            sSql = "SELECT req.*, c.custname FROM QL_trnreqrabmst req INNER JOIN QL_mstcust c ON c.custoid=req.custoid WHERE reqrabmststatus IN('Post','Approved') ORDER BY reqrabdate DESC";
            tbl = db.Database.SqlQuery<reqrabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listmrservice
        {
            public int mrservicemstoid { get; set; }
            public string mrserviceno { get; set; }
            public DateTime mrservicedate { get; set; }
            public string itemdesc { get; set; }
            public string custname { get; set; }
            public string mrservicemstnote { get; set; }
        }

        [HttpPost]
        public ActionResult GetServiceData()
        {
            List<listmrservice> tbl = new List<listmrservice>();

            sSql = "SELECT mr.mrservicemstoid, mr.mrserviceno, mr.mrservicedate, c.custname, i.itemdesc, mr.mrservicemstnote FROM QL_trnmrservicemst mr INNER JOIN QL_mstcust c ON c.custoid=mr.custoid INNER JOIN QL_mstitem i ON i.itemoid=mr.itemoid WHERE mr.mrservicemststatus='Post' ORDER BY mr.mrservicedate DESC";
            tbl = db.Database.SqlQuery<listmrservice>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetPartnerData()
        {
            List<QL_mstpartner> tbl = new List<QL_mstpartner>();

            sSql = "SELECT * FROM QL_mstpartner WHERE activeflag='ACTIVE' ORDER BY partnername";
            tbl = db.Database.SqlQuery<QL_mstpartner>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierData()
        {
            List<QL_mstsupp> tbl = new List<QL_mstsupp>();

            sSql = "SELECT * FROM QL_mstsupp WHERE activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: RAB
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            return View();
        }

        [HttpPost]
        public ActionResult getListDataTable(string ddlstatus, string periodfrom, string periodto)
        {

            sSql = "SELECT * FROM( Select pr.rabmstoid, pr.rabno, pr.rabdate, pr.rabtype, ISNULL(pr.projectname,'') projectname, c.custname, pr.rabgrandtotaljualamt, pr.rabmststatus, CASE WHEN pr.rabmststatus = 'Revised' THEN pr.revisereason WHEN pr.rabmststatus = 'Rejected' THEN pr.rejectreason ELSE '' END reason FROM QL_trnrabmst pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid WHERE pr.rabtype2='PROJECT' ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(periodfrom) && !string.IsNullOrEmpty(periodto))
            {
                sSql += " AND t.rabdate >='" + periodfrom + " 00:00:00' AND t.rabdate <='" + periodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(ddlstatus))
            {
                sSql += " AND t.rabmststatus " + ddlstatus;
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnrabmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnrabmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.rabno = ""; //generateNo(ClassFunction.GetServerTime());
                tblmst.rabgrandtotaljualamt = 0; tblmst.rabtotaljualamt = 0;
                tblmst.rabtotaljualtaxamt = 0; tblmst.rabtotaljualpphamt = 0;
                tblmst.rabgrandtotalbeliamt = 0; tblmst.rabtotalbeliamt = 0; tblmst.rabtotalbelitaxamt = 0;
                tblmst.rabgrandtotaljualjasaamt = 0; tblmst.rabtotaljualjasaamt = 0;
                tblmst.rabtotaljualjasataxamt = 0; tblmst.rabtotaljualjasapphamt = 0;
                tblmst.rabgrandtotalbelijasaamt = 0; tblmst.rabtotalbelijasaamt = 0;
                tblmst.rabtotalbelijasataxamt = 0; tblmst.rabtotalbelijasapphamt = 0;
                tblmst.rabgrandtotalcostamt = 0; tblmst.rabtotalcostamt = 0;
                tblmst.rabmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                ViewBag.rabdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.rabestdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnrabmst.Find(Session["CompnyCode"].ToString(), id);

                ViewBag.rabdate = tblmst.rabdate.Value.ToString("dd/MM/yyyy");
                ViewBag.rabestdate = tblmst.rabestdate.Value.ToString("dd/MM/yyyy");
                ViewBag.updtime = tblmst.updtime.ToString("dd/MM/yyyy");
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Form(QL_trnrabmst tblmst, string action, string tglmst, string tglest, string custname, SendEmailModel tblse)
        public ActionResult Form(QL_trnrabmst tblmst, List<rabdtl> dtDtl, List<rabdtl2> dtDtl2, List<matbooking> dtDtlMatBooking, List<rabdtl4> dtDtl4, List<rabdtl5> dtDtl5, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var msg = ""; var result = "failed";
            var servertime = ClassFunction.GetServerTime();

            //is Input Valid
            if (tblmst.custoid == null)
                msg += "- Pilih Customer Terlebih Dahulu!!<br>";
            if (tblmst.revrabtype == "Baru")
            {
                if (tblmst.rabmstoid_draft == null)
                    msg += "- Pilih RAB Draft Terlebih Dahulu!!<br>";
            }
            if (tblmst.alamatoid == null)
                msg += "- Pilih Alamat Terlebih Dahulu!!<br>";
            if (tblmst.rabtypereq == "Permintaan RAB")
            {
                if (tblmst.reqrabmstoid == 0)
                    msg += "- Pilih Permintaan RAB Terlebih Dahulu!!<br>";
            }
            if (tblmst.rabtype2 != "PARTNER")
            {
                tblmst.partneroid = 0;
                tblmst.partnerpersen = 0;
                tblmst.partneramt = 0;
            }
            if (string.IsNullOrEmpty(tblmst.projectname))
                msg += "- Silahkan isi Project!<br>";
            if (string.IsNullOrEmpty(tblmst.rabno))
                tblmst.rabno = "";
            if (string.IsNullOrEmpty(tblmst.rabmstnote))
                tblmst.rabmstnote = "";
            if (string.IsNullOrEmpty(tblmst.projectno))
                tblmst.projectno = "";
            if (string.IsNullOrEmpty(tblmst.projectname))
                tblmst.projectname = "";
            if (string.IsNullOrEmpty(tblmst.rabasaljual))
                tblmst.rabasaljual = "";
            if (string.IsNullOrEmpty(tblmst.rabtotalbeliamt.ToString()))
                tblmst.rabtotalbeliamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelitaxamt.ToString()))
                tblmst.rabtotalbelitaxamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotaljualjasaamt.ToString()))
                tblmst.rabtotaljualjasaamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotaljualjasataxamt.ToString()))
                tblmst.rabtotaljualjasataxamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotaljualjasapphamt.ToString()))
                tblmst.rabtotaljualjasapphamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelijasaamt.ToString()))
                tblmst.rabtotalbelijasaamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelijasataxamt.ToString()))
                tblmst.rabtotalbelijasataxamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalbelijasapphamt.ToString()))
                tblmst.rabtotalbelijasapphamt = 0;
            if (string.IsNullOrEmpty(tblmst.rabtotalcostamt.ToString()))
                tblmst.rabtotalcostamt = 0;
            if (tblmst.rabmststatus.ToUpper() == "REVISED")
            {
                tblmst.rabmststatus = "In Process";
            }
            tblmst.rate2oid = 0;
            if (tblmst.revrabtype == "Baru")
                tblmst.rabno = "";

            if (dtDtl == null)
                msg += "- Silahkan isi Detail Penjualan!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Silahkan isi Detail Penjualan!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].rabdtlnote))
                            dtDtl[i].rabdtlnote = "";
                        if (dtDtl[i].rabqty == 0)
                        {
                            msg += "- Qty " + dtDtl[i].itemdtldesc + " Belum Diisi !!<br>";
                        }
                        if (dtDtl[i].rabprice == 0)
                        {
                            msg += "- Harga " + dtDtl[i].itemdtldesc + " Belum Diisi !!<br>";
                        }

                        //Cek data item beli
                        if (tblmst.rabmststatus == "In Approval")
                        {
                            if (dtDtl[i].itemoid == 0)
                            {
                                msg += "- Item " + dtDtl[i].itemdtldesc + " Belum Di Buatkan Di Master barang!!<br>";
                            }
                            if (dtDtl2 != null)
                            {
                                if (dtDtl2.Count > 0)
                                {
                                    var itmjual = dtDtl[i].itemoid;
                                    var itmjualtype = db.Database.SqlQuery<string>("SELECT itemtype FROM QL_mstitem WHERE itemoid=" + itmjual + "").FirstOrDefault();
                                    if (itmjualtype == "Barang")
                                    {
                                        var cItmBeli = dtDtl2.Where(x => x.itemdtl2oid == itmjual).Count();
                                        if (cItmBeli == 0)
                                        {
                                            msg += "- Item " + dtDtl[i].itemdtldesc + " Belum Di Tarik Di Pembelian!!<br>";
                                        }
                                    }
                                }
                            }
                        }
                        //End Cek data item beli
                    }
                }
            }

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl2[i].rabdtl2note))
                            dtDtl2[i].rabdtl2note = "";

                        if (dtDtl2[i].suppdtl2oid == 0)
                        {
                            msg += "- Supplier " + dtDtl2[i].itemdtl2desc + "  Belum Di Pilih!!<br>";
                        }

                        if (tblmst.rabmststatus == "In Approval")
                        {
                            if (dtDtl2[i].itemdtl2oid == 0)
                            {
                                msg += "- Item " + dtDtl2[i].itemdtl2desc + "  Belum Di Buatkan Di Master barang!!<br>";
                            }
                        }

                        //Cek data Rakitan
                        if (tblmst.revrabtype == "Revisi")
                        {
                            if (dtDtl2[i].itemdtl2oid.Value != 0)
                            {
                                int cekRakitan = db.Database.SqlQuery<int>("SELECT COUNT(*) AS hitung FROM QL_mstitemdtl2 WHERE ISNULL(itemrefoid,0)=" + dtDtl2[i].itemdtl2oid.Value + "").FirstOrDefault();
                                if (cekRakitan > 0)
                                {
                                    dtDtl2[i].itemdtl2refoid = db.Database.SqlQuery<int>("SELECT ISNULL((SELECT TOP 1 itemoid FROM QL_mstitemdtl2 WHERE ISNULL(itemrefoid,0)=" + dtDtl2[i].itemdtl2oid.Value + " ORDER BY updtime DESC),0) AS t").FirstOrDefault();
                                }
                            }
                        } //End Cek data Rakitan

                        //Validasi Booking Stock
                        if (dtDtl2[i].rabdtl2amtbooking > 0)
                        {
                            if (dtDtl2[i].rabdtl2qtybooking <= 0)
                            {
                                msg += "- Qty Booking Stock " + dtDtl2[i].itemdtl2desc + "  Tidak Boleh 0 !<br>";
                            }
                        }
                        if (dtDtl2[i].rabdtl2qtybooking > 0)
                        {
                            decimal qtyBooking = 0;
                            if (dtDtlMatBooking != null)
                            {
                                if (dtDtlMatBooking.Count() > 0)
                                {
                                    var dtQtyBooking = dtDtlMatBooking.Where(x => x.refoid == dtDtl2[i].itemdtl2oid).GroupBy(x => x.refoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.qtyout) }).ToList();
                                    for (int j = 0; j < dtQtyBooking.Count(); j++)
                                    {
                                        qtyBooking = dtQtyBooking[j].Qty.Value;

                                    }
                                }
                            }
                            if (qtyBooking != dtDtl2[i].rabdtl2qtybooking)
                            {
                                msg += "- Qty Booking Stock " + dtDtl2[i].itemdtl2desc + "  Harus Sama Dengan Qty Booking!<br>";
                            }
                        }
                        //End Validasi Booking Stock
                        if (tblmst.rabmststatus == "In Approval" && dtDtl2[i].poassetmstoid > 0 && dtDtl2[i].poassetdtloid > 0)
                        {
                            //Validasi PO Asset
                            sSql = "SELECT (poassetqty - ISNULL((SELECT SUM(rx.rabdtl2qty) FROM QL_trnrabdtl2 rx INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=rx.rabmstoid WHERE rm.rabmststatus IN('In Approval', 'Approved') AND ISNULL(rx.poassetdtloid,0)=pod.poassetdtloid AND ISNULL(rx.poassetmstoid,0)=pod.poassetmstoid),0.0)) rabdtl2qty FROM QL_trnpoassetmst pom INNER JOIN QL_trnpoassetdtl pod ON pod.poassetmstoid=pom.poassetmstoid INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid INNER JOIN QL_mstitem i ON i.itemoid=pod.poassetrefoid INNER JOIN QL_m05GN g ON g.gnoid=pod.poassetunitoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poassettype IN('FPB','Stock') AND pod.poassetdtloid=" + dtDtl2[i].poassetdtloid + " AND pod.poassetmstoid=" + dtDtl2[i].poassetmstoid + "";
                            var OSQty_POA = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (dtDtl2[i].rabdtl2qty > OSQty_POA)
                            {
                                msg += "- Qty " + dtDtl2[i].itemdtl2desc + " Dari " + dtDtl2[i].poassetno + " Tidak Boleh Melebihi Sisa Qty PO Asset Sebesar " + Math.Round(OSQty_POA, 0) + "!<br>";
                            }
                        }
                    }
                }
            }

            if (dtDtl4 != null)
            {
                if (dtDtl4.Count > 0)
                {
                    for (int i = 0; i < dtDtl4.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl4[i].rabdtl4note))
                            dtDtl4[i].rabdtl4note = "";

                        if (dtDtl4[i].suppdtl4oid == 0)
                        {
                            msg += "- Supplier " + dtDtl4[i].itemdtl4desc + "  Belum Di Pilih!!<br>";
                        }

                        if (dtDtl4[i].rabdtl4qty == 0)
                        {
                            msg += "- Qty " + dtDtl4[i].itemdtl4desc + " Belum Diisi !!<br>";
                        }
                        if (dtDtl4[i].rabdtl4price == 0)
                        {
                            msg += "- Harga " + dtDtl4[i].itemdtl4desc + " Belum Diisi !!<br>";
                        }

                        if (tblmst.rabmststatus == "In Approval")
                        {
                            if (dtDtl4[i].itemdtl4oid == 0)
                            {
                                msg += "- Item " + dtDtl4[i].itemdtl4desc + "  Belum Di Buatkan Di Master barang!!<br>";
                            }
                        }
                    }
                }
            }

            if (dtDtl5 != null)
            {
                if (dtDtl5.Count > 0)
                {
                    for (int i = 0; i < dtDtl5.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl5[i].rabdtl5note))
                            dtDtl5[i].rabdtl5note = "";
                        if (dtDtl5[i].rabdtl5price == 0)
                        {
                            msg += "- Amount " + dtDtl5[i].itemdtl5desc + " Belum Diisi !!<br>";
                        }

                        if (tblmst.rabmststatus == "In Approval")
                        {
                            if (dtDtl5[i].acctgoid == 0)
                            {
                                msg += "- Akun " + dtDtl5[i].itemdtl5desc + "  Belum Di Pilih!!<br>";
                            }
                        }
                    }
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();

            if (tblmst.rabmststatus == "In Approval")
            {
                if (stoid_app <= 0)
                {
                    msg += "- Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tblmst.rabdate.Value;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                tblmst.rabmststatus = "In Process";
            }
            if (tblmst.rabmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                    tblmst.rabmststatus = "In Process";
                }
            }

            if (msg == "")
            {
                var mstoid = ClassFunction.GenerateID("QL_trnrabmst");
                var dtloid = ClassFunction.GenerateID("QL_trnrabdtl");
                var dtloid2 = ClassFunction.GenerateID("QL_trnrabdtl2");
                var dtloid4 = ClassFunction.GenerateID("QL_trnrabdtl4");
                var dtloid5 = ClassFunction.GenerateID("QL_trnrabdtl5");
                var matbookingoid = ClassFunction.GenerateID("QL_matbooking");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.rabmstoid = mstoid;
                            if (tblmst.revrabtype == "Baru")
                            {
                                tblmst.revrabmstoid = mstoid;
                                tblmst.rabmstoid_revisi = 0;
                            }
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnrabmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnrabmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnrabdtl.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl.RemoveRange(trndtl);
                            db.SaveChanges();

                            var trndtl2 = db.QL_trnrabdtl2.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl2.RemoveRange(trndtl2);
                            db.SaveChanges();

                            var trndtl4 = db.QL_trnrabdtl4.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl4.RemoveRange(trndtl4);
                            db.SaveChanges();

                            var trndtl5 = db.QL_trnrabdtl5.Where(a => a.rabmstoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnrabdtl5.RemoveRange(trndtl5);
                            db.SaveChanges();

                            var trndtl_matbooking = db.QL_matbooking.Where(a => a.formoid == tblmst.rabmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_matbooking.RemoveRange(trndtl_matbooking);
                            db.SaveChanges();
                        }

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl>();
                                foreach (var item in dtDtl)
                                {
                                    var new_dtl = (QL_trnrabdtl)ClassFunction.MappingTable(new QL_trnrabdtl(), item);
                                    if (string.IsNullOrEmpty(item.rabdtlstatus)) new_dtl.rabdtlstatus = "";
                                    if (string.IsNullOrEmpty(item.rabdtlnote)) new_dtl.rabdtlnote = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtloid = dtloid++;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnrabdtl'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl2 != null)
                        {
                            if (dtDtl2.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl2>();
                                foreach (var item in dtDtl2)
                                {
                                    var new_dtl = (QL_trnrabdtl2)ClassFunction.MappingTable(new QL_trnrabdtl2(), item);
                                    if (string.IsNullOrEmpty(item.rabdtl2status)) new_dtl.rabdtl2status = "";
                                    if (string.IsNullOrEmpty(item.rabdtl2note)) new_dtl.rabdtl2note = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtl2oid = dtloid2++;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl2.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid2 - 1) + " WHERE tablename='QL_trnrabdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtlMatBooking != null)
                        {
                            if (dtDtlMatBooking.Count > 0)
                            {
                                var tbldtl = new List<QL_matbooking>();
                                foreach (var item in dtDtlMatBooking)
                                {
                                    var new_dtl = (QL_matbooking)ClassFunction.MappingTable(new QL_matbooking(), item);
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.matbookingoid = matbookingoid++;
                                    new_dtl.trndate = tblmst.rabdate;
                                    new_dtl.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                                    new_dtl.trndate = tblmst.rabdate;
                                    new_dtl.formaction = "QL_trnrabmst";
                                    new_dtl.formoid = tblmst.rabmstoid;
                                    new_dtl.formdtloid = 0;
                                    new_dtl.qtyin = 0;
                                    new_dtl.refname = "FINISH GOOD";
                                    new_dtl.flag = "Post";
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_matbooking.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (matbookingoid - 1) + " WHERE tablename='QL_matbooking'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl4 != null)
                        {
                            if (dtDtl4.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl4>();
                                foreach (var item in dtDtl4)
                                {
                                    var new_dtl = (QL_trnrabdtl4)ClassFunction.MappingTable(new QL_trnrabdtl4(), item);
                                    if (string.IsNullOrEmpty(item.rabdtl4status)) new_dtl.rabdtl4status = "";
                                    if (string.IsNullOrEmpty(item.rabdtl4note)) new_dtl.rabdtl4note = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtl4oid = dtloid4++;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl4.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid4 - 1) + " WHERE tablename='QL_trnrabdtl4'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dtDtl5 != null)
                        {
                            if (dtDtl5.Count > 0)
                            {
                                var tbldtl = new List<QL_trnrabdtl5>();
                                foreach (var item in dtDtl5)
                                {
                                    var new_dtl = (QL_trnrabdtl5)ClassFunction.MappingTable(new QL_trnrabdtl5(), item);
                                    if (string.IsNullOrEmpty(item.rabdtl5status)) new_dtl.rabdtl5status = "";
                                    if (string.IsNullOrEmpty(item.rabdtl5note)) new_dtl.rabdtl5note = "";
                                    new_dtl.cmpcode = tblmst.cmpcode;
                                    new_dtl.rabdtl5oid = dtloid5++;
                                    new_dtl.rabmstoid = tblmst.rabmstoid;
                                    new_dtl.rabdtl5qty = 1;
                                    new_dtl.rabdtl5netto = item.rabdtl5price;
                                    new_dtl.upduser = tblmst.upduser;
                                    new_dtl.updtime = tblmst.updtime;
                                    tbldtl.Add(new_dtl);
                                }
                                db.QL_trnrabdtl5.AddRange(tbldtl);

                                sSql = "UPDATE QL_ID SET lastoid=" + (dtloid5 - 1) + " WHERE tablename='QL_trnrabdtl5'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Deklarasi data utk approval
                        QL_APP tblapp = new QL_APP();
                        if (tblmst.rabmststatus == "In Approval")
                        {
                            tblapp.cmpcode = tblmst.cmpcode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tblmst.rabmstoid;
                            tblapp.requser = tblmst.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appstoid = stoid_app;
                            tblapp.tablename = "QL_trnrabmst";
                            db.QL_APP.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        msg = "Data has been Saved <br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnrabmst tblmst = db.QL_trnrabmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnrabdtl.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trnrabdtl2.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl4 = db.QL_trnrabdtl4.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl4.RemoveRange(trndtl4);
                        db.SaveChanges();

                        var trndtl5 = db.QL_trnrabdtl5.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl5.RemoveRange(trndtl5);
                        db.SaveChanges();

                        var trndtl_matbooking = db.QL_matbooking.Where(a => a.formoid == id);
                        db.QL_matbooking.RemoveRange(trndtl_matbooking);
                        db.SaveChanges();

                        db.QL_trnrabmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT rabtype FROM QL_trnrabmst WHERE rabmstoid=" + id + "";
            string rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            string rptname = "rptRAB.rpt";

            ReportDocument report = new ReportDocument();
            if (rabtype == "WAPU")
            {
                rptname = "rptRABWapu.rpt";
            }
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE rm.cmpcode='" + CompnyCode + "' AND rm.rabmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "RABReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}