﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class SupplierController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string suppktppath = "~/Images/SuppKTP";
        private string suppnpwppath = "~/Images/SuppNPWP";
        private string imgtemppath = "~/Images/ImagesTemps";

        public SupplierController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class suppmst
        {            
            public string cmpcode { get; set; }
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppaddr { get; set; }
            public string suppphone1 { get; set; }

            //[RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "silahkan masukan e-mail secara valid")]
            //[Required]

            //[DataType(DataType.EmailAddress)]
            //[Display(Name = "Email address*")]
            public string suppemail { get; set; }
            public string payterm { get; set; }
            public string suppname { get; set; }
            public string pajak { get; set; }
            public string activeflag { get; set; }            
        }

        public class suppdtl1
        {
            public int suppdtl1oid { get; set; }
            public int suppdtl1seq { get; set; }
            public string suppdtl1addr { get; set; }
            public int suppcityoid { get; set; }            
            public string suppdtl1phone { get; set; }
            public string suppdtl1status { get; set; }
        }

        public class suppdtl2
        {
            public int suppdtl2oid { get; set; }
            public int suppdtl2seq { get; set; }
            public string suppdtl2picname { get; set; }
            public string suppdtl2phone1 { get; set; }
            public string suppdtl2phone2 { get; set; }
            public string suppdtl2email { get; set; }
            public string suppdtl2jabatan { get; set; }
        }

        public class suppdtl3
        {
            public int suppdtl3oid { get; set; }
            public int suppdtl3seq { get; set; }
            public string suppdtl3picname { get; set; }
            public string suppdtl3phone1 { get; set; }
            public string suppdtl3phone2 { get; set; }
            public string suppdtl3email { get; set; }
            public string suppdtl3jabatan { get; set; }
            public string suppdtl3picproduk { get; set; }
        }

        private void InitDDL(QL_mstsupp tbl)
        {
            sSql = "select * from ql_m05gn where gngroup='PROVINSI' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var suppprovinceoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.suppprovinceoid);
            ViewBag.suppprovinceoid = suppprovinceoid;

            sSql = "select * from ql_m05gn where gngroup='KATEGORI SUPPLIER' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var catid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.catid = catid;

            sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.suppcityOid);
            ViewBag.suppcityOid = cityoid;

            sSql = "select * from ql_m05gn where gngroup='PAYMENT TERM' AND cmpcode='" + tbl.cmpcode + "' AND gnflag='ACTIVE'";
            var supppaymentoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.supppaymentoid);
            ViewBag.supppaymentoid = supppaymentoid;
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }

        [HttpPost]
        public ActionResult InitDDLCity()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05gn WHERE gngroup = 'KOTA' and gnflag = 'ACTIVE' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }                

        public ActionResult FillDetailData1()
        {
            if (Session["QL_mstsuppdtl1"] == null)
            {
                Session["QL_mstsuppdtl1"] = new List<suppdtl1>();
            }

            List<suppdtl1> dataDtl = (List<suppdtl1>)Session["QL_mstsuppdtl1"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData2()
        {
            if (Session["QL_mstsuppdtl2"] == null)
            {
                Session["QL_mstsuppdtl2"] = new List<suppdtl2>();
            }

            List<suppdtl2> dataDtl = (List<suppdtl2>)Session["QL_mstsuppdtl2"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData3()
        {
            if (Session["QL_mstsuppdtl3"] == null)
            {
                Session["QL_mstsuppdtl3"] = new List<suppdtl3>();
            }

            List<suppdtl3> dataDtl = (List<suppdtl3>)Session["QL_mstsuppdtl3"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchValue(string search)
        {
           List<suppmst> allsearch = db.QL_mstsupp.Where(x => x.suppname.Contains(search)).Select(x => new suppmst
            {
                suppoid = x.suppoid,
                suppname = x.suppname
            }).ToList();
            return new JsonResult { Data = allsearch, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        // GET: Supplier
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");            
            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND gnflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT *, ISNULL((SELECT g.gndesc FROM QL_m05GN g WHERE g.gnoid=s.supppaymentoid),'') payterm, case when supppajak=1 then 'Ya' else 'Tidak' end pajak FROM QL_mstsupp s WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY suppoid DESC";
            List<suppmst> vbag = db.Database.SqlQuery<suppmst>(sSql).ToList();
            return View(vbag);
        }

        // GET: Supplier/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstsupp tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstsupp();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.suppcode = generateCode();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                Session["QL_mstsuppdtl1"] = null;               
                Session["QL_mstsuppdtl2"] = null;
                Session["QL_mstsuppdtl3"] = null;

            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstsupp.Find(Session["CompnyCode"].ToString(), id);

                string sSql = "SELECT sd1.suppdtl1oid, sd1.suppdtl1seq, sd1.suppdtl1addr, sd1.suppcityoid, ISNULL((SELECT g.gndesc FROM QL_m05GN g WHERE g.gnoid=sd1.suppcityoid and gngroup = 'KOTA' and gnflag = 'ACTIVE'),'') suppcitydesc, sd1.suppdtl1phone, sd1.suppdtl1status FROM QL_mstsuppdtl1 sd1 where suppoid =" + id;
                Session["QL_mstsuppdtl1"] = db.Database.SqlQuery<suppdtl1>(sSql).ToList();

                sSql = "SELECT sd2.suppdtl2oid, sd2.suppdtl2seq, sd2.suppdtl2picname, sd2.suppdtl2phone1, sd2.suppdtl2phone2, sd2.suppdtl2email, sd2.suppdtl2jabatan FROM QL_mstsuppdtl2 sd2 where suppoid = " + id + "";
                Session["QL_mstsuppdtl2"] = db.Database.SqlQuery<suppdtl2>(sSql).ToList();

                sSql = "SELECT sd3.suppdtl3oid, sd3.suppdtl3seq, sd3.suppdtl3picname, sd3.suppdtl3phone1, sd3.suppdtl3phone2, sd3.suppdtl3email, sd3.suppdtl3jabatan, sd3.suppdtl3picproduk FROM QL_mstsuppdtl3 sd3 where suppoid = " + id + "";
                Session["QL_mstsuppdtl3"] = db.Database.SqlQuery<suppdtl3>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);           
            return View(tbl);
        }

        // POST: Supplier/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstsupp tbl, string action, string[] catid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<suppdtl1> dt1dtl = (List<suppdtl1>)Session["QL_mstsuppdtl1"];
            if (dt1dtl == null)
                ModelState.AddModelError("", "Silahkan isi data detail kantor");
            else if (dt1dtl.Count <= 0)
                ModelState.AddModelError("", "Silahkan isi data detail kantor");

            List<suppdtl2> dt2dtl = (List<suppdtl2>)Session["QL_mstsuppdtl2"];
            //if (dt2dtl == null)
            //    ModelState.AddModelError("", "Silahkan isi data detail pic management");
            //else if (dt2dtl.Count <= 0)
            //    ModelState.AddModelError("", "Silahkan isi data detail pic management");

            List<suppdtl3> dt3dtl = (List<suppdtl3>)Session["QL_mstsuppdtl3"];
            //if (dt3dtl == null)
            //    ModelState.AddModelError("", "Silahkan isi data detail pic produk");
            //else if (dt3dtl.Count <= 0)
            //    ModelState.AddModelError("", "Silahkan isi data detail pic produk");

            //Is Input Valid?
            //if (string.IsNullOrEmpty(tbl.suppcode))
            //    ModelState.AddModelError("suppcode", "Silahkan isi Kode!");
            //else if (db.QL_mstsupp.Where(w => w.suppcode == tbl.suppname & w.suppoid != tbl.suppoid).Count() > 0)
            //    ModelState.AddModelError("suppcode", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
            if (action == "New Data")
            {
                tbl.suppcode = generateCode();
            }
            if (string.IsNullOrEmpty(tbl.suppname))
                ModelState.AddModelError("suppname", "Silahkan isi Nama!");
            else if (db.QL_mstsupp.Where(w => w.suppname == tbl.suppname & w.suppoid != tbl.suppoid).Count() > 0)
                ModelState.AddModelError("suppname", "Nama yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Nama lainnya!");
            if (string.IsNullOrEmpty(tbl.suppaddr))
                tbl.suppaddr = "";
            if (string.IsNullOrEmpty(tbl.supppostcode))
                tbl.supppostcode = "";
            if (string.IsNullOrEmpty(tbl.suppphone1))
                tbl.suppphone1 = "";
            if (string.IsNullOrEmpty(tbl.suppphone2))
                tbl.suppphone2 = "";
            if (string.IsNullOrEmpty(tbl.suppfax1))
                tbl.suppfax1 = "";
            if (!string.IsNullOrEmpty(tbl.suppemail))
            {
                string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(emailRegex);
                if (!re.IsMatch(tbl.suppemail))
                {
                    ModelState.AddModelError("", "Email is not valid");
                }
            }
            else
            {
                ModelState.AddModelError("", "Email is required");
            }
            if (string.IsNullOrEmpty(tbl.suppgaransi))
                tbl.suppgaransi = "";
            if (string.IsNullOrEmpty(tbl.suppnpwpno))
                tbl.suppnpwpno = "";
            if (string.IsNullOrEmpty(tbl.suppnpwp))
                tbl.suppnpwp = "";
            if (string.IsNullOrEmpty(tbl.suppnpwpaddr))
                tbl.suppnpwpaddr = "";
            if (string.IsNullOrEmpty(tbl.suppktpimg))
                tbl.suppktpimg = "";
            if (string.IsNullOrEmpty(tbl.suppnpwpimg))
                tbl.suppnpwpimg = "";
            if (string.IsNullOrEmpty(tbl.suppnote))
                tbl.suppnote = "";
            if (string.IsNullOrEmpty(tbl.suppktpimg))
                tbl.suppktpimg = "~/Images/";
            if (string.IsNullOrEmpty(tbl.suppnpwpimg))
                tbl.suppnpwpimg = "~/Images/";

            if (catid != null)
            {
                if (catid.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < catid.Count(); i++)
                    {
                        stsval += "" + catid[i] + ",";
                    }
                    tbl.suppcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                }
                else
                {
                    tbl.suppcategoryid = "";
                }
            }
            else
            {
                tbl.suppcategoryid = "";
            }

            if (dt1dtl != null)
            {
                if (dt1dtl.Count > 0)
                {
                    for (int i = 0; i < dt1dtl.Count(); i++)
                    {
                        if (dt1dtl[i].suppdtl1addr == "" | string.IsNullOrEmpty(dt1dtl[i].suppdtl1addr))
                        {
                            ModelState.AddModelError("", "Detail Kantor No. " + dt1dtl[i].suppdtl1seq +" - Alamat tidak boleh kosong");
                        }
                        if (dt1dtl[i].suppdtl1phone == "" | string.IsNullOrEmpty(dt1dtl[i].suppdtl1phone))
                        {
                            ModelState.AddModelError("", "Detail Kantor No. " + dt1dtl[i].suppdtl1seq + " - No. Telp tidak boleh kosong");
                        }
                        if (string.IsNullOrEmpty(dt1dtl[i].suppdtl1addr))
                            dt1dtl[i].suppdtl1addr = "";
                        if (string.IsNullOrEmpty(dt1dtl[i].suppdtl1phone))
                            dt1dtl[i].suppdtl1phone = "";
                        if (string.IsNullOrEmpty(dt1dtl[i].suppdtl1status))
                            dt1dtl[i].suppdtl1status = "";
                    }
                }
            }

            if (dt2dtl != null)
            {
                if (dt2dtl.Count > 0)
                {
                    for (int i = 0; i < dt2dtl.Count(); i++)
                    {
                        if (dt2dtl[i].suppdtl2picname == "" | string.IsNullOrEmpty(dt2dtl[i].suppdtl2picname))
                        {
                            ModelState.AddModelError("", "Detail PIC Management No. " + dt2dtl[i].suppdtl2seq + " - Nama PIC tidak boleh kosong");
                        }
                        if (dt2dtl[i].suppdtl2phone1 == "" | string.IsNullOrEmpty(dt2dtl[i].suppdtl2phone1))
                        {
                            ModelState.AddModelError("", "Detail PIC Management No. " + dt2dtl[i].suppdtl2seq + " - No. HP 1 tidak boleh kosong");
                        }
                        if (string.IsNullOrEmpty(dt2dtl[i].suppdtl2picname))
                            dt2dtl[i].suppdtl2picname = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].suppdtl2phone1))
                            dt2dtl[i].suppdtl2phone1 = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].suppdtl2phone2))
                            dt2dtl[i].suppdtl2phone2 = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].suppdtl2email))
                            dt2dtl[i].suppdtl2email = "";
                        if (string.IsNullOrEmpty(dt2dtl[i].suppdtl2jabatan))
                            dt2dtl[i].suppdtl2jabatan = "";
                    }
                }
            }

            if (dt3dtl != null)
            {
                if (dt3dtl.Count > 0)
                {
                    for (int i = 0; i < dt3dtl.Count(); i++)
                    {
                        if (dt3dtl[i].suppdtl3picname == "" | string.IsNullOrEmpty(dt3dtl[i].suppdtl3picname))
                        {
                            ModelState.AddModelError("", "Detail PIC Produk No. " + dt3dtl[i].suppdtl3seq + " - Nama PIC tidak boleh kosong");
                        }
                        if (dt3dtl[i].suppdtl3phone1 == "" | string.IsNullOrEmpty(dt3dtl[i].suppdtl3phone1))
                        {
                            ModelState.AddModelError("", "Detail PIC Produk No. " + dt3dtl[i].suppdtl3seq + " - No. HP 1 tidak boleh kosong");
                        }
                        if (dt3dtl[i].suppdtl3jabatan == "" | string.IsNullOrEmpty(dt3dtl[i].suppdtl3jabatan))
                        {
                            ModelState.AddModelError("", "Detail PIC Produk No. " + dt3dtl[i].suppdtl3seq + " - Jabatan tidak boleh kosong");
                        }
                        if (dt3dtl[i].suppdtl3picproduk == "" | string.IsNullOrEmpty(dt3dtl[i].suppdtl3picproduk))
                        {
                            ModelState.AddModelError("", "Detail PIC Produk No. " + dt3dtl[i].suppdtl3seq + " - Produk yg dihandel tidak boleh kosong");
                        }
                        if (string.IsNullOrEmpty(dt3dtl[i].suppdtl3picname))
                            dt3dtl[i].suppdtl3picname = "";
                        if (string.IsNullOrEmpty(dt3dtl[i].suppdtl3phone1))
                            dt3dtl[i].suppdtl3phone1 = "";
                        if (string.IsNullOrEmpty(dt3dtl[i].suppdtl3phone2))
                            dt3dtl[i].suppdtl3phone2 = "";
                        if (string.IsNullOrEmpty(dt3dtl[i].suppdtl3email))
                            dt3dtl[i].suppdtl3email = "";
                        if (string.IsNullOrEmpty(dt3dtl[i].suppdtl3jabatan))
                            dt3dtl[i].suppdtl3jabatan = "";
                        if (string.IsNullOrEmpty(dt3dtl[i].suppdtl3picproduk))
                            dt3dtl[i].suppdtl3picproduk = "";
                    }
                }
            }


            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstsupp");

            if (ModelState.IsValid)
            {
                var suppdtl1oid = ClassFunction.GenerateID("QL_mstsuppdtl1");
                var suppdtl2oid = ClassFunction.GenerateID("QL_mstsuppdtl2");
                var suppdtl3oid = ClassFunction.GenerateID("QL_mstsuppdtl3");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(tbl.suppktpimg)))
                        {
                            var sExt = Path.GetExtension(tbl.suppktpimg);
                            var sdir = Server.MapPath(suppktppath);
                            var sfilename = tbl.suppoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.suppktpimg.ToLower() != (suppktppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.suppktpimg), sdir + "/" + sfilename);
                            }
                            tbl.suppktpimg = suppktppath + "/" + sfilename;
                        }
                        if (System.IO.File.Exists(Server.MapPath(tbl.suppnpwpimg)))
                        {
                            var sExt = Path.GetExtension(tbl.suppnpwpimg);
                            var sdir = Server.MapPath(suppnpwppath);
                            var sfilename = tbl.suppoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (tbl.suppnpwpimg.ToLower() != (suppnpwppath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(tbl.suppnpwpimg), sdir + "/" + sfilename);
                            }
                            tbl.suppnpwpimg = suppnpwppath + "/" + sfilename;
                        }

                        if (action == "New Data")
                        {
                            //Insert
                            tbl.suppoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;                            
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstsupp.Add(tbl);
                            db.SaveChanges();                            

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstsupp'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Update Data")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            //Update Cat Detail
                            sSql = "DELETE FROM QL_mstcatdtl WHERE catdtltype='QL_mstsupp' AND catdtlrefoid=" + tbl.suppoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tbl.suppoid && a.catdtltype == "QL_mstsupp");
                            db.QL_mstcatdtl.RemoveRange(trncat);
                            db.SaveChanges();

                            ////delete detail
                            //var kntrdtl = db.QL_mstsuppdtl1.Where(a => a.suppoid == tbl.suppoid);
                            //var picMdtl = db.QL_mstsuppdtl2.Where(a => a.suppoid == tbl.suppoid);
                            //var picPdtl = db.QL_mstsuppdtl3.Where(a => a.suppoid == tbl.suppoid);
                            //db.QL_mstsuppdtl1.RemoveRange(kntrdtl);
                            //db.SaveChanges();

                            //db.QL_mstsuppdtl2.RemoveRange(picMdtl);
                            //db.SaveChanges();

                            //db.QL_mstsuppdtl3.RemoveRange(picPdtl);
                            //db.SaveChanges();
                        }

                        //insert cat detail
                        if (catid != null)
                        {
                            if (catid.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < catid.Count(); i++)
                                {
                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstsupp";
                                    tblcatdtl.catdtlrefoid = tbl.suppoid;
                                    tblcatdtl.catoid = int.Parse(catid[i]);
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        //insert detail
                        if (dt1dtl != null)
                        {
                            if (dt1dtl.Count > 0)
                            {
                                QL_mstsuppdtl1 tbldtl1;
                                for (int i = 0; i < dt1dtl.Count(); i++)
                                {
                                    if (dt1dtl[i].suppdtl1oid == 0)
                                    {
                                        tbldtl1 = new QL_mstsuppdtl1();
                                        tbldtl1.suppdtl1oid = suppdtl1oid++;
                                    }
                                    else
                                    {
                                        tbldtl1 = db.QL_mstsuppdtl1.Find(CompnyCode, dt1dtl[i].suppdtl1oid);
                                    }
                                    tbldtl1.cmpcode = CompnyCode;
                                    tbldtl1.suppdtl1seq = i + 1;
                                    tbldtl1.suppoid = tbl.suppoid;
                                    tbldtl1.suppdtl1addr = dt1dtl[i].suppdtl1addr;
                                    tbldtl1.suppcityoid = dt1dtl[i].suppcityoid;
                                    tbldtl1.suppdtl1phone = dt1dtl[i].suppdtl1phone;
                                    tbldtl1.suppdtl1status = dt1dtl[i].suppdtl1status;
                                    tbldtl1.updtime = tbl.updtime;
                                    tbldtl1.upduser = tbl.upduser;
                                    if (dt1dtl[i].suppdtl1oid == 0)
                                    {
                                        db.QL_mstsuppdtl1.Add(tbldtl1);
                                    }
                                    else
                                    {
                                        db.Entry(tbldtl1).State = EntityState.Modified;
                                    }
                                }
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (suppdtl1oid - 1) + " Where tablename = 'QL_mstsuppdtl1'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dt2dtl != null)
                        {
                            if (dt2dtl.Count > 0)
                            {
                                QL_mstsuppdtl2 tbldtl2;
                                for (int i = 0; i < dt2dtl.Count(); i++)
                                {
                                    if (dt2dtl[i].suppdtl2oid == 0)
                                    {
                                        tbldtl2 = new QL_mstsuppdtl2();
                                        tbldtl2.suppdtl2oid = suppdtl2oid++;
                                    }
                                    else
                                    {
                                        tbldtl2 = db.QL_mstsuppdtl2.Find(CompnyCode, dt2dtl[i].suppdtl2oid);
                                    }
                                    tbldtl2.cmpcode = CompnyCode;
                                    tbldtl2.suppdtl2seq = i + 1;
                                    tbldtl2.suppoid = tbl.suppoid;
                                    tbldtl2.suppdtl2picname = dt2dtl[i].suppdtl2picname;
                                    tbldtl2.suppdtl2phone1 = dt2dtl[i].suppdtl2phone1;
                                    tbldtl2.suppdtl2phone2 = dt2dtl[i].suppdtl2phone2;
                                    tbldtl2.suppdtl2email = dt2dtl[i].suppdtl2email;
                                    tbldtl2.suppdtl2jabatan = dt2dtl[i].suppdtl2jabatan;
                                    tbldtl2.updtime = tbl.updtime;
                                    tbldtl2.upduser = tbl.upduser;
                                    if (dt2dtl[i].suppdtl2oid == 0)
                                    {
                                        db.QL_mstsuppdtl2.Add(tbldtl2);
                                    }
                                    else
                                    {
                                        db.Entry(tbldtl2).State = EntityState.Modified;
                                    }
                                }                                
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (suppdtl2oid - 1) + " Where tablename = 'QL_mstsuppdtl2'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        if (dt3dtl != null)
                        {
                            if (dt3dtl.Count > 0)
                            {
                                QL_mstsuppdtl3 tbldtl3;
                                for (int i = 0; i < dt3dtl.Count(); i++)
                                {
                                    if (dt3dtl[i].suppdtl3oid == 0)
                                    {
                                        tbldtl3 = new QL_mstsuppdtl3();
                                        tbldtl3.suppdtl3oid = suppdtl3oid++;
                                    }
                                    else
                                    {
                                        tbldtl3 = db.QL_mstsuppdtl3.Find(CompnyCode, dt3dtl[i].suppdtl3oid);
                                    }
                                    tbldtl3.cmpcode = CompnyCode;
                                    tbldtl3.suppdtl3seq = i + 1;
                                    tbldtl3.suppoid = tbl.suppoid;
                                    tbldtl3.suppdtl3picname = dt3dtl[i].suppdtl3picname;
                                    tbldtl3.suppdtl3phone1 = dt3dtl[i].suppdtl3phone1;
                                    tbldtl3.suppdtl3phone2 = dt3dtl[i].suppdtl3phone2;
                                    tbldtl3.suppdtl3email = dt3dtl[i].suppdtl3email;
                                    tbldtl3.suppdtl3jabatan = dt3dtl[i].suppdtl3jabatan;
                                    tbldtl3.suppdtl3picproduk = dt3dtl[i].suppdtl3picproduk;
                                    tbldtl3.updtime = tbl.updtime;
                                    tbldtl3.upduser = tbl.upduser;
                                    if (dt3dtl[i].suppdtl3oid == 0)
                                    {
                                        db.QL_mstsuppdtl3.Add(tbldtl3);
                                    }
                                    else
                                    {
                                        db.Entry(tbldtl3).State = EntityState.Modified;
                                    }
                                }                                
                                //Update lastoid
                                sSql = "Update QL_ID set lastoid = " + (suppdtl3oid - 1) + " Where tablename = 'QL_mstsuppdtl3'";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("Error", ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
            //ViewBag(QL_mstsuppdtl1);
        }

        // POST: Supplier/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstsupp tbl = db.QL_mstsupp.Find(Session["CompnyCode"].ToString(),id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl != null)
                        {
                            System.IO.File.Delete(Server.MapPath(tbl.suppktpimg));
                            System.IO.File.Delete(Server.MapPath(tbl.suppnpwpimg));
                        }

                        var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tbl.suppoid && a.catdtltype == "QL_mstsupp");
                        db.QL_mstcatdtl.RemoveRange(trncat);
                        db.SaveChanges();

                        var trndtl = db.QL_mstsuppdtl1.Where(a => a.suppoid == id);
                        db.QL_mstsuppdtl1.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_mstsuppdtl2.Where(a => a.suppoid == id);
                        db.QL_mstsuppdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl3 = db.QL_mstsuppdtl3.Where(a => a.suppoid == id);
                        db.QL_mstsuppdtl3.RemoveRange(trndtl3);
                        db.SaveChanges();

                        db.QL_mstsupp.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult getQL_mstsuppdtl1()
        {
            if (Session["QL_mstsuppdtl1"] == null)
            {
                Session["QL_mstsuppdtl1"] = new List<suppdtl1>();
            }

            List<suppdtl1> dataDtl = (List<suppdtl1>)Session["QL_mstsuppdtl1"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetailsAll(List<suppdtl1> dtDtl, List<suppdtl2> dtDtl2, List<suppdtl3> dtDtl3)
        {
            Session["QL_mstsuppdtl1"] = dtDtl;
            Session["QL_mstsuppdtl2"] = dtDtl2;
            Session["QL_mstsuppdtl3"] = dtDtl3;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setDataDtl(List<suppdtl1> mdataDtl)
        {
            Session["QL_mstsuppdtl1"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setDataDtl2(List<suppdtl2> mdataDtl)
        {
            Session["QL_mstsuppdtl2"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setDataDtl3(List<suppdtl3> mdataDtl)
        {
            Session["QL_mstsuppdtl3"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string generateCode()
        {
            string sCode = "SUPP-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(SuppCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstSupp WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND SuppCode LIKE '" + sCode + "%'";
            
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintXlsReport(string typeexp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            if (typeexp == "Supplier")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSuppXls.rpt"));
            }
            else if (typeexp == "PIC")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSuppPICXls.rpt"));
            }
            else if (typeexp == "PICProd")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSuppPICProdXls.rpt"));
            }
            else if (typeexp == "Alamat")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSuppAlamatXls.rpt"));
            }
            report.SetParameterValue("PrintUserID", Session["UserID"].ToString());
            report.SetParameterValue("PrintUserName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/excel", "ExportSupplier_" + typeexp + ".xls");
        }
    }
}
