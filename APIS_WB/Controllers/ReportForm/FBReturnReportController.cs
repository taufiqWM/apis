﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FBReturnReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";
        private string sWhere = "";

        public FBReturnReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetSuppData(string[] status, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, s.suppcode [Kode], s.suppname [Nama], s.suppaddr [Alamat] FROM QL_mstsupp s WHERE s.suppoid IN (SELECT pretm.suppoid FROM QL_trnapretitemmst pretm WHERE pretm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND pretm.apretitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY s.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetReturData(string[] status, string suppcode, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, pretm.apretitemmstoid [Draft No.], pretm.apretitemno [No. Retur], CONVERT(varchar(10),pretm.apretitemdate,103) [Tgl Retur], s.suppname [Supplier], pretm.apretitemmstnote FROM QL_trnapretitemmst pretm INNER JOIN QL_mstsupp s ON s.suppoid=pretm.suppoid WHERE pretm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND pretm.apretitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (suppcode != "")
                {
                    string[] arr = suppcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY pretm.apretitemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] status, string suppcode, string ddlnomor, string rabno, string startdate, string enddate, string fdate)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode Barang], m.itemdesc [Nama Barang], g.gndesc [Satuan] FROM QL_mstitem m INNER JOIN QL_trnapretitemdtl pretd ON pretd.itemoid=m.itemoid INNER JOIN QL_trnapretitemmst pretm ON pretm.cmpcode=pretd.cmpcode AND pretm.apretitemmstoid=pretd.apretitemmstoid INNER JOIN QL_mstsupp s ON s.suppoid=pretm.suppoid INNER JOIN QL_m05GN g ON g.gnoid=pretd.apretitemunitoid WHERE pretd.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }
                if (status != null)
                {
                    if (status.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < status.Count(); i++)
                        {
                            stsval += "'" + status[i] + "',";
                        }
                        sSql += " AND pretm.apretitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (suppcode != "")
                {
                    string[] arr = suppcode.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (rabno != "")
                {
                    string[] arr = rabno.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string DDLGroup)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            //rptPBReturnDtlPdfGudang
            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                rptfile = "rptFBReturnSumPdf.rpt";
                rptname = "FBReturn_SUMMARY";
            }
            else
            {
                //if (DDLGroup == "Gudang")
                //{
                //    rptfile = "rptPBReturnDtlPdfGudang.rpt";
                //}
                //else if (DDLGroup == "Gudang")
                //{
                //    rptfile = "rptPBReturnDtlPdfBarang.rpt";
                //}
                //else
                //{
                //    rptfile = "rptPBReturnDtlPdf.rpt";
                //}
                rptfile = "rptFBReturnDtlPdf.rpt";
                rptname = "FBReturn_DETAIL";
            }

            var Dtl = "";
            var Join = "";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sWhere += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }
            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sWhere += " AND pretm.apretitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }
            if (TextSupp != "")
            {
                string[] arr = TextSupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sWhere += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }
            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sWhere += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (DDLType == "Detail")
            {
                //SELECT DETAIL
                Dtl += " , apretitemdtlseq [No.], apitemno [MR No.] , (apitemqty - ISNULL((SELECT SUM(x.apretitemqty) FROM QL_trnapretitemdtl x WHERE x.cmpcode=pretd.cmpcode AND x.apitemdtloid=pretd.apitemdtloid AND x.apretitemmstoid<>pretd.apretitemmstoid), 0)) AS MRQty, ISNULL((SELECT poitemno FROM QL_trnpoitemmst pom  WHERE pom.cmpcode=mrm.cmpcode AND pom.poitemmstoid=mrm.poitemmstoid), '') [PO No.], '' [PR No.], itemcode [Code], itemdesc [Material], apretitemqty [Qty], g1.gndesc [Unit], CASE WHEN apretitemvalueidr=0 THEN 0 ELSE (apretitemvalueidr/apretitemqty) END [HPP], '' [PID], apretitemdtlnote [Detail Note], '' AS Reason, gu.gndesc Gudang, gu.gnoid ";
                //JOIN DTL
                Join += " INNER JOIN QL_trnapretitemdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretd.apretitemmstoid=pretm.apretitemmstoid INNER JOIN QL_mstitem m ON m.itemoid=pretd.itemoid INNER JOIN QL_m05GN gu ON gu.gnoid=pretd.apretitemunitoid INNER JOIN QL_m05gn g1 ON g1.gnoid=apretitemunitoid INNER JOIN QL_trnapitemdtl mrd ON mrd.cmpcode=pretd.cmpcode AND mrd.apitemdtloid=pretd.apitemdtloid ";
            }

            sSql = " SELECT '" + CompnyName + "' [Business Unit], CONVERT(VARCHAR(20), pretm.apretitemmstoid) [Draft No.], apretitemno [Return No.], apretitemdate [Return Date], suppcode [Supplier Code], suppname [Supplier], mrm.apitemno [Reg. No.], mrm.apitemdate registerdate, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ISNULL(mrm.rabmstoid,0)),'') [Project], ISNULL((SELECT rm.poitemno FROM QL_trnpoitemmst rm WHERE rm.poitemmstoid=ISNULL(mrm.poitemmstoid,0)),'') [PO No.], mrm.upduser [Reg Post Date], '' AS [Reg UserPost], GETDATE() [Tgl Surat Jalan], apretitemmststatus [Status], apretitemmstnote [Header Note], UPPER(pretm.createuser) [Create User], pretm.createtime [Create Datetime], ''[Approval User], getdate() [Approval Datetime], ISNULL(apretitemmstres2, '') [Return Type], ISNULL((SELECT SUM(x.apretitemvalueidr) FROM QL_trnapretitemdtl x WHERE x.apretitemmstoid=pretm.apretitemmstoid),0.0) [Total HPP] " + Dtl + " FROM QL_trnapretitemmst pretm INNER JOIN QL_mstsupp s ON s.suppoid = pretm.suppoid INNER JOIN QL_trnapitemmst mrm ON mrm.cmpcode = pretm.cmpcode AND mrm.apitemmstoid = pretm.apitemmstoid " + Join + " " + sWhere + "";

            if (DDLType == "Detail")
            {
                sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLetter;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}