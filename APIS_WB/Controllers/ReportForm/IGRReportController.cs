﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class IGRReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public IGRReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            // Isi DropDownList Business Unit
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;
            var cmp = DDLBusinessUnit.First().Value;
            //if (DDLBusinessUnit.Count() > 0)
            //{
            sSql = "SELECT * FROM QL_mstcurr WHERE currcode IN ('IDR', 'USD') order by currcode";
            var DDLCurrency = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "currcode", "currcode");
            ViewBag.DDLCurrency = DDLCurrency;
            //}

            var DDLCOA = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(CompnyCode, "VAR_BANK")).ToList(), "acctgoid", "acctgdesc");
            ViewBag.DDLCOA = DDLCOA;
        }

        [HttpPost]
        public ActionResult InitDDLCOA(string DDLBusinessUnit)
        {
            var result = "";
            JsonResult js = null;
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            try
            {
                sSql = GetQueryBindListCOA(CompnyCode, "VAR_BANK");
                tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tbl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        private string GetQueryBindListCOA(string cmp, string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: PRReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("IGRReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLType, string StartDate, string EndDate, string DDLPeriod, string DDLCurrency, string DDLOrderBy, string ReportType, string TextNomor, string DDLNomor, string DDLCOA)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "rptGiroInReal.rpt";
            var rptname = "InGoingGiroRealizationReport";
            if (DDLType == "Detail")
            {
                rptfile = "rptGiroInRealStatus.rpt";
                rptname = "InGoingGiroRealizationReportStatusReport";
            }

            var sWhere = " AND cb.cmpcode='" + CompnyCode + "'";
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                sWhere += " AND cb.cashbankdate>=CAST('" + StartDate + " 00:00:00' AS DATETIME) AND cb.cashbankdate<=CAST('" + EndDate + " 23:59:59' AS DATETIME)";
            if (!string.IsNullOrEmpty(DDLCOA))
                sWhere += " AND cb.acctgoid='" + DDLCOA + "'";
            if (!string.IsNullOrEmpty(TextNomor))
            {
                if (DDLType == "Detail")
                {
                    if (DDLNomor == "Cash/Bank No.")
                        sWhere += " AND cb.cashbankno IN ('" + TextNomor.Replace(";", "','") + "')";
                    else
                        sWhere += " AND ISNULL(gp.realno,'') IN ('" + TextNomor.Replace(";", "','") + "')";
                }
                else
                {
                    if (DDLNomor == "Cash/Bank No.")
                        sWhere += " AND cb.cashbankno IN ('" + TextNomor.Replace(";", "','") + "')";
                    else
                        sWhere += " AND cbref.cashbankno IN ('" + TextNomor.Replace(";", "','") + "')";
                }
            }
            
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("sWhere", sWhere);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("PrintUserName", Session["UserName"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = "";
                this.HttpContext.Session["rptpaper"] = null;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                ClassProcedure.SetDBLogonForReport(report);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}