﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class POStatusFBReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public POStatusFBReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetSuppData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, s.suppcode [Kode], s.suppname [Nama], s.suppaddr [Alamat] FROM QL_mstsupp s WHERE s.suppoid IN (SELECT pom.suppoid FROM QL_trnpoitemmst pom WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY s.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult GetPOData(string[] DDLStatus, string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomorRAB)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, pom.poitemmstoid [No. Draft],pom.poitemno [No. Purchase Order], CONVERT(varchar(10),pom.poitemdate,103) [Tgl. PO],s.suppname [Supplier], pom.poitemmststatus [Status], pom.poitemmstnote [Note] FROM QL_trnpoitemmst pom INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = pom.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid=pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " ORDER BY pom.poitemdate DESC, pom.poitemmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetRABData(string[] DDLStatus, string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, rm.rabmstoid [No. Draft], rm.rabno [No. RAB], CONVERT(VARCHAR(10), rm.rabdate, 101) AS [Tgl. RAB], rm.projectname [Nama Project], CONVERT(VARCHAR(10), rm.approvaldatetime, 101) AS [Tgl Approval RAB], rm.rabmststatus [Status], rm.rabmstnote [Note] /*, CONVERT(VARCHAR(10), prd.pritemarrdatereq, 101) AS pritemarrdatereq*/ FROM QL_trnrabmst rm WHERE rm.rabmstoid IN (SELECT pom.rabmstoid from QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON pom.suppoid=s.suppoid WHERE pom.poitemtype<>'Jasa' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                sSql += " )";
                sSql += " ORDER BY rm.rabdate DESC, rm.rabmstoid DESC";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextSupp, string DDLPoNo, string TextNomorPO, string TextNomorRAB, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnpoitemdtl pod ON pod.itemoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid=pod.poitemunitoid INNER JOIN QL_trnpoitemmst pom ON pom.poitemmstoid=pod.poitemmstoid INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=pom.rabmstoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid WHERE pom.cmpcode='" + CompnyCode + "' AND pom.poitemtype<>'Jasa' ";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }
                if (TextNomorPO != "")
                {
                    string[] arr = TextNomorPO.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLPoNo + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono, string TextNomorRAB)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptPOStatusFBSumPdf.rpt";
                else
                    rptfile = "rptPOStatusFBSumPdf.rpt";
                rptname = "POStatusFB_SUMMARY";
            }
            else if (DDLType == "Summary2")
            {
                rptfile = "rptPOStatusFBAmtSumPdf.rpt";
                //if (reporttype == "XLS")
                //    rptfile = "rptPOStatusFBAmtSumXls.rpt";
                //else
                //    rptfile = "rptPOStatusFBAmtSumPdf.rpt";
                rptname = "POStatusFBAmt_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptPOStatusFBDtlPdf.rpt";
                else
                {
                    rptfile = "rptPOStatusFBDtlPdf.rpt";
                }
                rptname = "POStatusFB_DETAIL";
            }

            var Dtl = "";
            var Join = "";
            if (DDLType == "Summary2")
            {
                sSql = "SELECT * FROM( SELECT pom.cmpcode, pom.poitemmstoid, pom.poitemno, pom.poitemdate, pom.updtime, pom.poitemmststatus, CASE WHEN ISNULL(pom.poitemmstres1,'')='' THEN pom.poitemgrandtotalamt ELSE 0.0 END poitemgrandtotalamt, s.suppcode, s.suppname, gndesc tempo, ISNULL((SELECT som.soitemno FROM QL_trnsoitemmst som WHERE som.soitemmstoid = pom.somstoid),'') sono, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.mritemno refno FROM QL_trnmritemmst cbx WHERE cbx.pomstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') mrno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.mritemdate, 103) refno FROM QL_trnmritemmst cbx WHERE cbx.pomstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') mrdate, ISNULL((SELECT SUM((((pd.poitemqty * pd.poitemprice) - pd.poitemdtldiscamt) / pd.poitemqty) * mrd.mritemqty) refno FROM QL_trnmritemmst cbx INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemmstoid = cbx.mritemmstoid INNER JOIN QL_trnpoitemdtl pd ON pd.poitemdtloid = mrd.podtloid WHERE cbx.pomstoid = pom.poitemmstoid),0.0) mramt, ISNULL((SELECT SUM(pd.poitemdtlnetto) refno FROM QL_trnpoitemdtl pd WHERE pd.poitemmstoid = pom.poitemmstoid AND pd.poitemmstoid IN(SELECT mrm.pomstoid FROM QL_trnmritemmst mrm WHERE mrm.mritemmststatus IN('Approved','Post','Closed'))),0.0) hutangsusamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.apitemno refno FROM QL_trnapitemmst cbx WHERE cbx.poitemmstoid = pom.poitemmstoid AND cbx.apitemmststatus IN('Approved','Post','Closed')) ft FOR XML PATH('')), 1, 1, ''),'') apno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.apitemdate, 103) refno FROM QL_trnapitemmst cbx WHERE cbx.poitemmstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') apdate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.apitemduedate, 103) refno FROM QL_trnapitemmst cbx WHERE cbx.poitemmstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') apduedate, ISNULL((SELECT SUM(cbx.apitemgrandtotal) refno FROM QL_trnapitemmst cbx WHERE cbx.poitemmstoid = pom.poitemmstoid),0.0) apgrandtotal, ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con INNER JOIN QL_trnapitemmst ap ON con.reftype = 'QL_trnapitemmst' AND con.refoid = ap.apitemmstoid WHERE ap.poitemmstoid = pom.poitemmstoid AND con.trnaptype IN('CNAP', 'DNAP')),0.0) rebate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT aprm.pretitemno refno FROM QL_trnpretitemmst aprm INNER JOIN QL_trnmritemmst apm ON apm.mritemmstoid = aprm.mritemmstoid WHERE apm.pomstoid = pom.poitemmstoid AND ISNULL(aprm.pretitemtype, '') <> 'Asset' UNION ALL SELECT DISTINCT aprm.apretitemno refno FROM QL_trnapretitemmst aprm INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = aprm.apitemmstoid WHERE apm.poitemmstoid = pom.poitemmstoid AND ISNULL(aprm.apretitemmstres1, '') <> 'Asset') ft FOR XML PATH('')), 1, 1, ''),'') returno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), aprm.pretitemdate, 103) refno FROM QL_trnpretitemmst aprm INNER JOIN QL_trnmritemmst apm ON apm.mritemmstoid = aprm.mritemmstoid WHERE apm.pomstoid = pom.poitemmstoid AND ISNULL(aprm.pretitemtype, '') <> 'Asset' UNION ALL SELECT DISTINCT CONVERT(CHAR(10), aprm.apretitemdate, 103) refno FROM QL_trnapretitemmst aprm INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = aprm.apitemmstoid WHERE apm.poitemmstoid = pom.poitemmstoid AND ISNULL(aprm.apretitemmstres1, '') <> 'Asset') ft FOR XML PATH('')), 1, 1, ''),'') returdate, ISNULL((SELECT SUM(mrd.pretitemqty * mrd.pretitemvalueidr) refno FROM QL_trnpretitemmst aprm INNER JOIN QL_trnmritemmst apm ON apm.mritemmstoid = aprm.mritemmstoid INNER JOIN QL_trnpretitemdtl mrd ON mrd.pretitemmstoid = aprm.pretitemmstoid WHERE apm.pomstoid = pom.poitemmstoid AND ISNULL(aprm.pretitemtype, '') <> 'Asset'),0.0) +ISNULL((SELECT SUM(mrd.apretitemdtlnetto) refno FROM QL_trnapretitemmst aprm INNER JOIN QL_trnapitemmst apm ON apm.apitemmstoid = aprm.apitemmstoid INNER JOIN QL_trnapretitemdtl mrd ON mrd.apretitemmstoid = aprm.apretitemmstoid WHERE apm.poitemmstoid = pom.poitemmstoid AND ISNULL(aprm.apretitemmstres1, '') <> 'Asset'),0.0) returamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.dpapno refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poitemmstoid AND cbx.poreftype='QL_trnpoitemmst' AND cbx.dpappaytype<>'DPFM') ft FOR XML PATH('')), 1, 1, ''),'') cashbankdpno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.dpapdate, 103) refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poitemmstoid AND cbx.poreftype='QL_trnpoitemmst' AND cbx.dpappaytype<>'DPFM') ft FOR XML PATH('')), 1, 1, ''),'') cashbankdpdate, ISNULL((SELECT SUM(cbx.dpapamt) refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poitemmstoid AND cbx.poreftype='QL_trnpoitemmst' AND cbx.dpappaytype<>'DPFM'),0.0) - ISNULL((SELECT SUM(cbx.dpapamt) refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poitemmstoid AND cbx.poreftype='QL_trnpoitemmst' AND cbx.dpappaytype<>'DPFM' AND ISNULL(dpapres1,'')='Batal'),0.0) - ISNULL((SELECT SUM(dp.dprefundamt) refno FROM QL_trndpap cbx INNER JOIN QL_trndprefund dp ON dp.dpreftype='QL_trndpap' AND dp.dpoid=cbx.dpapoid WHERE cbx.pomstoid = pom.poitemmstoid AND cbx.poreftype='QL_trnpoitemmst' AND cbx.dpappaytype<>'DPFM'),0.0) paydpamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.cashbankno refno FROM QL_trnapitemmst apx INNER JOIN QL_trnpayap pyx ON pyx.refoid = apx.apitemmstoid AND pyx.reftype = 'QL_trnapitemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.poitemmstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.cashbankdate, 103) refno FROM QL_trnapitemmst apx INNER JOIN QL_trnpayap pyx ON pyx.refoid = apx.apitemmstoid AND pyx.reftype = 'QL_trnapitemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.poitemmstoid = pom.poitemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankdate, ISNULL((SELECT SUM(pyx.payapamt) refno FROM QL_trnapitemmst apx INNER JOIN QL_trnpayap pyx ON pyx.refoid = apx.apitemmstoid AND pyx.reftype = 'QL_trnapitemmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.poitemmstoid = pom.poitemmstoid),0.0) payapamt FROM QL_trnpoitemmst pom INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid INNER JOIN QL_m05GN g ON g.gnoid = pom.poitempaytypeoid  UNION ALL  SELECT pom.cmpcode, pom.poassetmstoid poitemmstoid, pom.poassetno poitemno, pom.poassetdate poitemdate, pom.updtime, pom.poassetmststatus poitemmststatus,  CASE WHEN ISNULL(pom.poassetmstres1,'')='' THEN pom.poassetgrandtotalamt ELSE 0.0 END poitemgrandtotalamt, s.suppcode, s.suppname, gndesc tempo, '' sono, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.mrassetno refno FROM QL_trnmrassetmst cbx WHERE cbx.poassetmstoid = pom.poassetmstoid) ft FOR XML PATH('')), 1, 1, ''),'') mrno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.mrassetdate, 103) refno FROM QL_trnmrassetmst cbx WHERE cbx.poassetmstoid = pom.poassetmstoid) ft FOR XML PATH('')), 1, 1, ''),'') mrdate, ISNULL((SELECT SUM((((pd.poassetqty * pd.poassetprice) - pd.poassetdtldiscamt) / pd.poassetqty) * mrd.mrassetqty) refno FROM QL_trnmrassetmst cbx INNER JOIN QL_trnmrassetdtl mrd ON mrd.mrassetmstoid = cbx.mrassetmstoid INNER JOIN QL_trnpoassetdtl pd ON pd.poassetdtloid = mrd.poassetdtloid WHERE cbx.poassetmstoid = pom.poassetmstoid),0.0) mramt, ISNULL((SELECT SUM(pd.poassetdtlnetto) refno FROM QL_trnpoassetdtl pd WHERE pd.poassetmstoid = pom.poassetmstoid AND pd.poassetmstoid IN(SELECT mrm.poassetmstoid FROM QL_trnmrassetmst mrm WHERE mrm.mrassetmststatus IN('Approved','Post','Closed'))),0.0) hutangsusamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.apassetno refno FROM QL_trnapassetmst cbx WHERE cbx.poassetmstoid = pom.poassetmstoid AND cbx.apassetmststatus IN('Approved','Post','Closed')) ft FOR XML PATH('')), 1, 1, ''),'') apno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.apassetdate, 103) refno FROM QL_trnapassetmst cbx WHERE cbx.poassetmstoid = pom.poassetmstoid) ft FOR XML PATH('')), 1, 1, ''),'') apdate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.apassetduedate, 103) refno FROM QL_trnapassetmst cbx WHERE cbx.poassetmstoid = pom.poassetmstoid) ft FOR XML PATH('')), 1, 1, ''),'') apduedate, ISNULL((SELECT SUM(cbx.apassetgrandtotal) refno FROM QL_trnapassetmst cbx WHERE cbx.poassetmstoid = pom.poassetmstoid),0.0) apgrandtotal, ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con INNER JOIN QL_trnapassetmst ap ON con.reftype = 'QL_trnapassetmst' AND con.refoid = ap.apassetmstoid WHERE ap.poassetmstoid = pom.poassetmstoid AND con.trnaptype IN('CNAP', 'DNAP')),0.0) rebate, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT aprm.pretitemno refno FROM QL_trnpretitemmst aprm INNER JOIN QL_trnmrassetmst apm ON apm.mrassetmstoid = aprm.mritemmstoid WHERE apm.poassetmstoid = pom.poassetmstoid AND ISNULL(aprm.pretitemtype, '') = 'Asset' UNION ALL SELECT DISTINCT aprm.apretitemno refno FROM QL_trnapretitemmst aprm INNER JOIN QL_trnapassetmst apm ON apm.apassetmstoid = aprm.apitemmstoid WHERE apm.poassetmstoid = pom.poassetmstoid AND ISNULL(aprm.apretitemmstres1, '') = 'Asset') ft FOR XML PATH('')), 1, 1, ''),'') returno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), aprm.pretitemdate, 103) refno FROM QL_trnpretitemmst aprm INNER JOIN QL_trnmrassetmst apm ON apm.mrassetmstoid = aprm.mritemmstoid WHERE apm.poassetmstoid = pom.poassetmstoid AND ISNULL(aprm.pretitemtype, '') = 'Asset' UNION ALL SELECT DISTINCT CONVERT(CHAR(10), aprm.apretitemdate, 103) refno FROM QL_trnapretitemmst aprm INNER JOIN QL_trnapassetmst apm ON apm.apassetmstoid = aprm.apitemmstoid WHERE apm.poassetmstoid = pom.poassetmstoid AND ISNULL(aprm.apretitemmstres1, '') = 'Asset') ft FOR XML PATH('')), 1, 1, ''),'') returdate, ISNULL((SELECT SUM(mrd.pretitemqty * mrd.pretitemvalueidr) refno FROM QL_trnpretitemmst aprm INNER JOIN QL_trnmrassetmst apm ON apm.mrassetmstoid = aprm.mritemmstoid INNER JOIN QL_trnpretitemdtl mrd ON mrd.pretitemmstoid = aprm.pretitemmstoid WHERE apm.poassetmstoid = pom.poassetmstoid AND ISNULL(aprm.pretitemtype, '') = 'Asset'),0.0) +ISNULL((SELECT SUM(mrd.apretitemdtlnetto) refno FROM QL_trnapretitemmst aprm INNER JOIN QL_trnapassetmst apm ON apm.apassetmstoid = aprm.apitemmstoid INNER JOIN QL_trnapretitemdtl mrd ON mrd.apretitemmstoid = aprm.apretitemmstoid WHERE apm.poassetmstoid = pom.poassetmstoid AND ISNULL(aprm.apretitemmstres1, '') = 'Asset'),0.0) returamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.dpapno refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poassetmstoid AND cbx.poreftype='QL_trnpoassetmst' AND cbx.dpappaytype<>'DPFM') ft FOR XML PATH('')), 1, 1, ''),'') cashbankdpno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.dpapdate, 103) refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poassetmstoid AND cbx.poreftype='QL_trnpoassetmst' AND cbx.dpappaytype<>'DPFM') ft FOR XML PATH('')), 1, 1, ''),'') cashbankdpdate, ISNULL((SELECT SUM(cbx.dpapamt) refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poassetmstoid AND cbx.poreftype='QL_trnpoassetmst' AND cbx.dpappaytype<>'DPFM'),0.0) - ISNULL((SELECT SUM(cbx.dpapamt) refno FROM QL_trndpap cbx WHERE cbx.pomstoid = pom.poassetmstoid AND cbx.poreftype='QL_trnpoassetmst' AND cbx.dpappaytype<>'DPFM' AND ISNULL(dpapres1,'')='Batal'),0.0) - ISNULL((SELECT SUM(dp.dprefundamt) refno FROM QL_trndpap cbx INNER JOIN QL_trndprefund dp ON dp.dpreftype='QL_trndpap' AND dp.dpoid=cbx.dpapoid WHERE cbx.pomstoid = pom.poassetmstoid AND cbx.poreftype='QL_trnpoassetmst' AND cbx.dpappaytype<>'DPFM'),0.0) paydpamt, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT cbx.cashbankno refno FROM QL_trnapassetmst apx INNER JOIN QL_trnpayap pyx ON pyx.refoid = apx.apassetmstoid AND pyx.reftype = 'QL_trnapassetmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.poassetmstoid = pom.poassetmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankno, ISNULL(STUFF((SELECT ', ' + ft.refno FROM(SELECT DISTINCT CONVERT(CHAR(10), cbx.cashbankdate, 103) refno FROM QL_trnapassetmst apx INNER JOIN QL_trnpayap pyx ON pyx.refoid = apx.apassetmstoid AND pyx.reftype = 'QL_trnapassetmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.poassetmstoid = pom.poassetmstoid) ft FOR XML PATH('')), 1, 1, ''),'') cashbankdate, ISNULL((SELECT SUM(pyx.payapamt) refno FROM QL_trnapassetmst apx INNER JOIN QL_trnpayap pyx ON pyx.refoid = apx.apassetmstoid AND pyx.reftype = 'QL_trnapassetmst' INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid = pyx.cashbankoid WHERE apx.poassetmstoid = pom.poassetmstoid),0.0) payapamt FROM QL_trnpoassetmst pom INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid INNER JOIN QL_m05GN g ON g.gnoid = pom.poassetpaytypeoid )AS pom ";
            }
            else
            {
                if (DDLType == "Detail")
                {
                    //Detail Receive
                    Dtl = ", ISNULL(mrd.mritemqty, 0) AS registerqty, ISNULL(mrd.refno, 0) AS refno, ISNULL((CASE mrm.mritemno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), mrm.mritemmstoid) ELSE mrm.mritemno END),'') AS registerno, ISNULL (mrm.mritemdate, CAST('01/01/1900' AS datetime))  AS registerdate, ISNULL(mrm.nosjsupp, '') AS registerdocrefno, ISNULL (mrm.tglsjsupp , CAST('01/01/1900' AS datetime))  AS registerdocrefdate, ISNULL(mrm.createuser, '') AS Regcreateuser, ISNULL (mrm.createtime, CAST('01/01/1900' AS datetime)) AS RegCreateDate";
                    //Detail Purchase Return
                    Dtl += ",(SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnpretitemmst pretm INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode = pretm.cmpcode AND pretm.pretitemmstoid = pretd.pretitemmstoid  AND pretd.itemoid = mrd.itemoid AND pretd.mritemdtloid IN(SELECT mritemdtloid FROM QL_trnmritemdtl mr WHERE mr.mritemdtloid = mrd.mritemdtloid) WHERE pretm.cmpcode = mrm.cmpcode AND pretm.mritemmstoid = mrm.mritemmstoid AND mrm.mritemtype NOT IN('Jasa') AND mrm.mritemmststatus IN('Post', 'Closed') AND pretm.pretitemmststatus IN('Post')) AS preturnqty";
                    Join = " LEFT JOIN QL_trnmritemdtl mrd ON mrd.cmpcode = pod.cmpcode AND mrd.itemoid = pod.itemoid AND mrd.podtloid = pod.poitemdtloid AND mrd.mritemmstoid IN (SELECT x.mritemmstoid FROM QL_trnmritemmst x WHERE x.mritemmststatus IN('Post', 'Closed') AND x.mritemtype NOT IN('Jasa')) LEFT JOIN QL_trnmritemmst mrm ON mrm.cmpcode = pom.cmpcode AND mrm.mritemmstoid = mrd.mritemmstoid AND mrm.mritemtype NOT IN('Jasa') ";
                    //Detail FB
                    Dtl += " , ISNULL(apd.apitemqty, 0) AS apqty, ISNULL((CASE apm.apitemno WHEN '' THEN 'Draft No. ' + CONVERT(VARCHAR(10), apm.apitemmstoid) ELSE apm.apitemno END),'') AS apitemno, ISNULL (apm.apitemdate, CAST('01/01/1900' AS datetime)) AS apdate";
                    Join += " LEFT JOIN QL_trnapitemdtl apd ON apd.cmpcode = pod.cmpcode AND apd.itemoid = mrd.itemoid  AND mrd.mritemmstoid=apd.mritemmstoid AND mrd.mritemdtloid=apd.mritemdtloid AND apd.apitemmstoid IN (SELECT x.apitemmstoid FROM QL_trnapitemmst x WHERE x.apitemmststatus IN('Post', 'Closed') AND x.apitemtype NOT IN('Jasa')) LEFT JOIN QL_trnapitemmst apm ON apm.cmpcode = pom.cmpcode AND apm.apitemmstoid = apd.apitemmstoid AND apm.poitemmstoid=pom.poitemmstoid AND apm.apitemtype NOT IN('Jasa') ";
                }
                else
                {
                    //Detail Receive
                    Dtl = " , (SELECT ISNULL(SUM(mrd.mritemqty), 0) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid AND mrd.itemoid=pod.itemoid AND mrd.podtloid=pod.poitemdtloid WHERE mrd.cmpcode=pod.cmpcode AND mrd.itemoid=pod.itemoid AND mrd.podtloid=pod.poitemdtloid AND mrm.mritemtype NOT IN ('Jasa') AND mrm.mritemmststatus IN ('Post', 'Closed')) AS registerqty";
                    //Detail Purchase Return
                    Dtl += " , (SELECT ISNULL(SUM(pretd.pretitemqty), 0) FROM QL_trnmritemdtl mrd INNER JOIN QL_trnmritemmst mrm ON mrm.cmpcode=mrd.cmpcode AND mrm.mritemmstoid=mrd.mritemmstoid AND mrm.pomstoid=pom.poitemmstoid AND mrd.podtloid=pod.poitemdtloid INNER JOIN QL_trnpretitemmst pretm ON pretm.cmpcode=mrm.cmpcode AND pretm.mritemmstoid=mrm.mritemmstoid INNER JOIN QL_trnpretitemdtl pretd ON pretd.cmpcode=pretm.cmpcode AND pretm.pretitemmstoid=pretd.pretitemmstoid AND pretd.itemoid=mrd.itemoid AND pretd.mritemdtloid IN (SELECT mritemdtloid FROM QL_trnmritemdtl mr WHERE mr.mritemdtloid=mrd.mritemdtloid) WHERE mrm.mritemtype NOT IN ('Jasa') AND mrm.mritemmststatus IN ('Post','Closed') AND pretm.pretitemmststatus IN ('Post')) AS preturnqty";
                    //Detail FB
                    Dtl += " , (SELECT ISNULL(SUM(apd.apitemqty), 0) FROM QL_trnapitemdtl apd INNER JOIN QL_trnapitemmst apm ON apm.cmpcode=apd.cmpcode AND apm.apitemmstoid=apd.apitemmstoid AND apd.itemoid=pod.itemoid INNER JOIN QL_trnmritemdtl mrd ON mrd.mritemmstoid = apd.mritemmstoid AND mrd.mritemdtloid = apd.mritemdtloid WHERE apd.cmpcode=pod.cmpcode AND apd.itemoid=pod.itemoid AND mrd.podtloid=pod.poitemdtloid AND apm.apitemtype NOT IN ('Jasa') AND apm.apitemmststatus IN ('Post','Closed')) AS apqty";
                }
                sSql = "SELECT pom.poitemmstoid AS pomstoid, pod.poitemdtloid AS podtloid, ('Draft No. ' + CONVERT(VARCHAR(10), pom.poitemmstoid)) AS draftno, pom.poitemno AS pono, pom.poitemdate AS podate, pom.poitemmststatus AS pomststatus, m.itemcode AS matcode, m.itemdesc AS matlongdesc, /*(CASE WHEN ISNULL(CONVERT(decimal(18,4), pod.poitemdtlres1),0)=0 then*/ ISNULL(pod.poitemqty, 0) /*ELSE CONVERT(decimal(18,4), pod.poitemdtlres1) END)*/ AS poqty, pod.closeqty AS POClosingQty, ISNULL(pod.poitemqty, 0) AS POQtyAwal, pom.cmpcode, pom.suppoid, s.suppname " + Dtl + ", 0.0 AS balanceqty, 0.0 AS sumregqty, 0.0 AS sumretqty, 0.0 AS sumpretqty, g.gndesc AS pounit, '' AS divname, pod.poitemdtlnote AS podtlnote, pom.poitemmstnote AS pomstnote, pom.approvaluser, pom.approvaldatetime, pom.createuser, ISNULL(rm.projectname, '') AS [PR No.], ISNULL(rm.rabdate, '') AS [PR Date], s.suppcode AS[Code Supplier], '' AS[PR Dept], '' AS[PR Exp Date], ISNULL(rm.rabmstnote, '') AS[PR Header Note], /* ISNULL((SELECT ISNULL(prd.pritemarrdatereq,'') FROM QL_pritemdtl prd WHERE prd.cmpcode=prm.cmpcode AND prd.pritemmstoid=prm.pritemmstoid AND prd.pritemdtloid = pod.pritemdtloid),'')*/ GETDATE() AS[PR Date Req], pom.closereason, pom.closeuser, pom.closetime FROM QL_trnpoitemmst pom INNER JOIN QL_trnpoitemdtl pod ON pod.cmpcode = pom.cmpcode AND pod.poitemmstoid = pom.poitemmstoid INNER JOIN QL_trnrabmst rm ON pom.cmpcode = rm.cmpcode AND rm.rabmstoid = pom.rabmstoid LEFT JOIN QL_trnrabdtl2 rd2 ON pod.cmpcode = rd2.cmpcode AND rd2.rabdtl2oid = pod.rabdtl2oid " + Join + " INNER JOIN QL_mstitem m ON m.itemoid = pod.itemoid INNER JOIN QL_m05GN g ON g.gnoid = pod.poitemunitoid INNER JOIN QL_mstsupp s ON s.suppoid = pom.suppoid";
            }

            sSql += " WHERE pom.cmpcode = '" + CompnyCode + "' ";
            if (DDLType != "Summary2")
                sSql += " AND pom.poitemtype NOT IN('Jasa')";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND pom.poitemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextSupp != "")
            {
                string[] arr = TextSupp.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                if (DDLType != "Summary2")
                    sSql += " AND s.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                else
                    sSql += " AND pom.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (TextNomorPO != "")
            {
                string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType != "Summary2")
            {
                if (TextNomorRAB != "")
                {
                    string[] arr = TextNomorRAB.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND rm.rabno IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (DDLType == "Detail")
                {
                    if (TextMaterial != "")
                    {
                        string[] arr = TextMaterial.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
            }    
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            if (DDLType != "Summary2")
            {
                
                for (int C1 = 0; C1 < dtRpt.Rows.Count; C1++)
                {
                    dtRpt.Rows[C1]["balanceqty"] = (decimal)dtRpt.Rows[C1]["POQtyAwal"] - (decimal)dtRpt.Rows[C1]["POClosingQty"] - (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"]) + (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"]);
                    dtRpt.Rows[C1]["sumregqty"] = (decimal)dtRpt.Compute("SUM(registerqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"]);
                    dtRpt.Rows[C1]["sumpretqty"] = (decimal)dtRpt.Compute("SUM(preturnqty)", "podtloid=" + dtRpt.Rows[C1]["podtloid"]);
                }

                //For Status PO Closed (In Process Register)
                decimal SumBalance = 0;
                for (int C2 = 0; C2 < dtRpt.Rows.Count; C2++)
                {
                    SumBalance = (decimal)dtRpt.Compute("SUM(balanceqty)", "pomstoid=" + dtRpt.Rows[C2]["pomstoid"]);
                    if (dtRpt.Rows[C2]["pomststatus"].ToString() == "Closed" && SumBalance > 0)
                    {
                        dtRpt.Rows[C2]["pomststatus"] = "Approved";
                    }
                }
            }   

            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Detail")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }
                else if (DDLType == "Summary2")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Detail")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                }
                else if (DDLType == "Summary2")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}