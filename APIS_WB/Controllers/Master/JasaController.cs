﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class JasaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public JasaController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class jasa
        {
            public string cmpcode { get; set; }
            public int jasaoid { get; set; }            
            public string jasacode { get; set; }
            public string jasaunit { get; set; }
            public string jasadesc { get; set; }         
            public string activeflag { get; set; }

        }

        private string generateNo(string tipe)
        {
            string sNo = "ITL/";
            if (tipe == "Jasa")
            {
                sNo = "JASA/";
            }
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(jasacode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstjasa WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND jasacode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;

            return sNo;
        }

        [HttpPost]

        public ActionResult Getcode(string tipe)
        {
            string sNo = "ITL/";
            if (tipe == "Jasa")
            {
                sNo = "JASA/";
            }
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(jasacode, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstjasa WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND jasacode LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sNo = sNo + sCounter;

            return Json(sNo);
        }

        private void InitDDL(QL_mstjasa tbl)
        {      
            sSql = "select * from QL_m05GN g where g.gngroup = 'SATUAN' AND gnflag='ACTIVE'";
            var jasaunitoid= new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.jasaunitoid);
            ViewBag.jasaunitoid = jasaunitoid;

            var sVar = "VAR_EXPENSE";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(GetQueryBindListCOA(sVar)).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
        }

        public class rabmst
        {
            public int rabmstoid_draft { get; set; }
            public int rabdtloid4_draft { get; set; }
            public string rabno { get; set; }
            public string rabtype { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string itemdesc { get; set; }

        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<rabmst> tbl = new List<rabmst>();

            sSql = "SELECT rd.rabmstoid rabmstoid_draft, rabdtloid4_draft, rabno, rabtype, projectname, custname, rd.itemdtl4desc itemdesc FROM QL_trnrabmst_draft rm INNER JOIN QL_trnrabdtl4_draft rd ON rd.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=rm.custoid WHERE rm.rabmststatus IN('Post','Approved') AND ISNULL(rd.itemdtl4oid,0)=0 AND ISNULL(rabdtl4flag_m,'')='Manual'";
            tbl = db.Database.SqlQuery<rabmst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string GetQueryBindListCOA(string sVar)
        {
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            var cols = db.Database.SqlQuery<string>("SELECT (STUFF((SELECT DISTINCT ',' + name FROM sys.syscolumns WHERE id=OBJECT_ID('QL_mstacctg') AND name<>'acctgdesc' FOR XML PATH('')), 1, 1, '')) cols").FirstOrDefault();
            var result = "SELECT " + cols + ", ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            return result;
        }

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

    
            sSql = "SELECT i.* FROM QL_mstjasa i WHERE i.cmpcode='" + Session["CompnyCode"].ToString() + "' ORDER BY jasaoid DESC";
            List<jasa> vbag = db.Database.SqlQuery<jasa>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstjasa tbl;
            string action = "New Data";
            if (id == null)
            {
                tbl = new QL_mstjasa();
                tbl.jasacode = generateNo("Jasa");
                tbl.jasaoid = ClassFunction.GenerateID("QL_mstjasa");
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cmpcode = Session["CompnyCode"].ToString();           
            }
            else
            {
                action = "Update Data";
                tbl = db.QL_mstjasa.Find(Session["CompnyCode"].ToString(), id);       
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstjasa tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            if (string.IsNullOrEmpty(tbl.jasanote))
                tbl.jasanote = "";

            if (action == "New Data")
            {
                tbl.jasacode = generateNo(tbl.jasatype);
            }
                
            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_mstjasa");
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "New Data")
                        {
                            //Insert
                            tbl.jasaoid = mstoid;
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstjasa.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstjasa'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        if (tbl.rabdtloid4_draft != null)
                        {
                            sSql = "UPDATE QL_trnrabdtl4_draft SET itemdtl4oid=" + tbl.jasaoid + " WHERE ISNULL(rabdtloid4_draft,0)=" + (tbl.rabdtloid4_draft ?? 0) + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }

            ViewBag.action = action;
            InitDDL(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat4 tbl = db.QL_mstcat4.Find(Session["CompnyCode"].ToString(), id);
                        db.QL_mstcat4.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
