//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIS_WB.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QL_trnbackservicemst
    {
        public string cmpcode { get; set; }
        public int backservicemstoid { get; set; }
        public int mrservicemstoid { get; set; }
        public string backserviceno { get; set; }
        public Nullable<System.DateTime> backservicedate { get; set; }
        public string periodacctg { get; set; }
        public Nullable<int> itemoid { get; set; }
        public Nullable<int> custoid { get; set; }
        public string backservicetype { get; set; }
        public string backservicereq { get; set; }
        public string backservicecomplaint { get; set; }
        public string backserviceresult { get; set; }
        public string backservicemstnote { get; set; }
        public string backservicemststatus { get; set; }
        public string createuser { get; set; }
        public Nullable<System.DateTime> createtime { get; set; }
        public string upduser { get; set; }
        public Nullable<System.DateTime> updtime { get; set; }
        public string backserviceres1 { get; set; }
        public string backserviceres2 { get; set; }
        public string backserviceres3 { get; set; }
        public string teknisioid { get; set; }
    }
}
