﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class ItemReportController : Controller
    {
        // GET: ItemReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public ItemReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_mstitem tbl)
        {
            sSql = "SELECT * FROM QL_mstcat1 WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>(sSql).ToList(), "cat1code", "cat1shortdesc", tbl.cat1code);
            ViewBag.cat1code = cat1code;

            sSql = "SELECT * FROM QL_mstcat2 WHERE cmpcode='" + CompnyCode + "' AND cat1code='" + tbl.cat1code + "' AND activeflag='ACTIVE'";
            var cat2code = new SelectList(db.Database.SqlQuery<QL_mstcat2>(sSql).ToList(), "cat2code", "cat2shortdesc", tbl.cat2code);
            ViewBag.cat2code = cat2code;

            sSql = "SELECT * FROM QL_mstcat3 WHERE cmpcode='" + CompnyCode + "' AND cat1code= '" + tbl.cat1code + "'  AND cat2code= '" + tbl.cat2code + "' AND  activeflag='ACTIVE'";
            var cat3code = new SelectList(db.Database.SqlQuery<QL_mstcat3>(sSql).ToList(), "cat3code", "cat3shortdesc", tbl.cat3code);
            ViewBag.cat3code = cat3code;

            sSql = "SELECT * FROM QL_mstcat4 WHERE cmpcode='" + CompnyCode + "' AND cat1code= '" + tbl.cat1code + "'  AND cat2code= '" + tbl.cat2code + "'   AND cat3code= '" + tbl.cat3code + "' AND activeflag ='ACTIVE'";
            var cat4code = new SelectList(db.Database.SqlQuery<QL_mstcat4>(sSql).ToList(), "cat4code", "cat4shortdesc", tbl.cat4code);
            ViewBag.cat4code = cat4code;
        }

        [HttpPost]
        public ActionResult GetSuppData(string DDLType)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, s.suppcode [Kode], s.suppname [Nama], s.suppaddr [Alamat] FROM QL_mstsupp s Left Join QL_mstitemdtl d ON d.suppoid=s.suppoid Where s.cmpcode='" + CompnyCode + "' AND itemoid IN (Select itemoid from ql_mstitem)";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult Getcat2(string[] cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat3(string[] cat1, string[] cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            sSql = "SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 != null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat3code";
            tbl = db.Database.SqlQuery<QL_mstcat3>(sSql).ToList();
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat4(string[] cat1, string[] cat2, string[] cat3)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat4> tbl = new List<QL_mstcat4>();
            sSql = "SELECT * FROM QL_mstcat4 WHERE activeflag='ACTIVE'";

            if (cat1 != null)
            {
                if (cat1.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1.Count(); i++)
                    {
                        stsval1 += "'" + cat1[i] + "',";
                    }
                    sSql += " AND cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2 != null)
            {
                if (cat2.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2.Count(); i++)
                    {
                        stsval2 += "'" + cat2[i] + "',";
                    }
                    sSql += " AND cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            if (cat3 != null)
            {
                if (cat3.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3.Count(); i++)
                    {
                        stsval3 += "'" + cat3[i] + "',";
                    }
                    sSql += " AND cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }

            sSql += " ORDER BY cat4code";
            tbl = db.Database.SqlQuery<QL_mstcat4>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] cat1, string[] cat2, string[] cat3, string[] cat4)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();
            try
            {
                sSql = "SELECT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_m05GN g ON g.gnoid=m.itemunitoid WHERE m.cmpcode='" + CompnyCode + "' ";

                if (cat1 != null)
                {
                    if (cat1.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < cat1.Count(); i++)
                        {
                            stsval += "'" + cat1[i] + "',";
                        }
                        sSql += " AND cat1code IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (cat2 != null)
                {
                    if (cat2.Count() > 0)
                    {
                        string stsval1 = "";
                        for (int i = 0; i < cat2.Count(); i++)
                        {
                            stsval1 += "'" + cat2[i] + "',";
                        }
                        sSql += " AND cat2code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                    }
                }

                if (cat3 != null)
                {
                    if (cat3.Count() > 0)
                    {
                        string stsval2 = "";
                        for (int i = 0; i < cat3.Count(); i++)
                        {
                            stsval2 += "'" + cat3[i] + "',";
                        }
                        sSql += " AND cat3code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                    }
                }

                if (cat4 != null)
                {
                    if (cat4.Count() > 0)
                    {
                        string stsval3 = "";
                        for (int i = 0; i < cat4.Count(); i++)
                        {
                            stsval3 += "'" + cat4[i] + "',";
                        }
                        sSql += " AND cat4code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");            

            QL_mstitem tbl; tbl = new QL_mstitem();
            if (tbl == null)
                return HttpNotFound();
            InitDDL(tbl);
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLStatus, string TextMaterial, string reporttype, string[] cat1code, string[] cat2code, string[] cat3code, string[] cat4code, string DDLType, string TextSupp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Kategori")
            {
                rptfile = "rptItemKategori.rpt";
                rptname = "ITEM_KATEGORI";
                sSql = "Select m.itemcode, c1.cat1longdesc, c2.cat2longdesc, c3.cat3longdesc, c4.cat4longdesc, itemdesc, m.itemtype, ISNULL( m.itemnote,'') itemnote, m.activeflag, gndesc unit From QL_mstitem m INNER JOIN QL_m05GN u ON u.gnoid=m.itemunitoid AND gngroup='SATUAN' INNER JOIN QL_mstcat1 c1 ON c1.cat1code=m.cat1code INNER JOIN QL_mstcat2 c2 ON c1.cat1code=c2.cat1code INNER JOIN QL_mstcat3 c3 ON c3.cat2code=c2.cat2code INNER JOIN QL_mstcat4 c4 ON c4.cat4code=m.cat4code AND c4.cat1code=m.cat1code AND c4.cat2code=m.cat2code AND c4.cat3code=m.cat3code And c4.cat1code=c1.cat1code AND c4.cat2code=c2.cat2code AND c4.cat3code=c3.cat3code Where m.cmpcode='" + CompnyCode + "'";
            }
            else
            {
                rptfile = "rptItemSupplier.rpt";
                rptname = "ITEM_SUPPLIER";
                sSql = "Select suppcode, suppname, ISNULL(m .cat1longdesc,'') cat1longdesc, ISNULL(m.cat2longdesc,'') cat2longdesc, ISNULL(m.cat3longdesc,'') cat3longdesc, ISNULL(m.cat4longdesc,'') cat4longdesc, Isnull(m.itemcode,'') itemcode, ISNULL(m.itemdesc,'') itemdesc, Isnull(m.itemtype, '') itemtype, Isnull(m.itemnote, '') itemnote, Isnull(m.activeflag, '') activeflag, Isnull(unit, '') unit From QL_mstsupp s LEFT JOIN QL_mstitemdtl d ON d.suppoid = s.suppoid LEFT JOIN (Select m.cmpcode, m.itemoid, m.itemcode, c1.cat1longdesc, c2.cat2longdesc, c3.cat3longdesc, c4.cat4longdesc, itemdesc, m.itemtype, ISNULL(m.itemnote, '') itemnote, m.activeflag, gndesc unit, m.cat1code, m.cat2code, m.cat3code, m.cat4code From QL_mstitem m INNER JOIN QL_m05GN u ON u.gnoid = m.itemunitoid AND gngroup = 'SATUAN' INNER JOIN QL_mstcat1 c1 ON c1.cat1code = m.cat1code INNER JOIN QL_mstcat2 c2 ON c1.cat1code = c2.cat1code INNER JOIN QL_mstcat3 c3 ON c3.cat2code = c2.cat2code INNER JOIN QL_mstcat4 c4 ON c4.cat4code = m.cat4code AND c4.cat1code = m.cat1code AND c4.cat2code = m.cat2code AND c4.cat3code = m.cat3code And c4.cat1code = c1.cat1code AND c4.cat2code = c2.cat2code AND c4.cat3code = c3.cat3code) m ON m.itemoid = d.itemoid Where m.cmpcode='" + CompnyCode + "'";

                if (TextSupp != "")
                {
                    string[] arr = TextSupp.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            
            if (TextMaterial != "")
            {
                string[] arr = TextMaterial.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (cat1code != null)
            {
                if (cat1code.Count() > 0)
                {
                    string stsval1 = "";
                    for (int i = 0; i < cat1code.Count(); i++)
                    {
                        stsval1 += "'" + cat1code[i] + "',";
                    }
                    sSql += " AND m.cat1code IN (" + ClassFunction.Left(stsval1, stsval1.Length - 1) + ")";
                }
            }

            if (cat2code != null)
            {
                if (cat2code.Count() > 0)
                {
                    string stsval2 = "";
                    for (int i = 0; i < cat2code.Count(); i++)
                    {
                        stsval2 += "'" + cat2code[i] + "',";
                    }
                    sSql += " AND m.cat2code IN (" + ClassFunction.Left(stsval2, stsval2.Length - 1) + ")";
                }
            }

            if (cat3code != null)
            {
                if (cat3code.Count() > 0)
                {
                    string stsval3 = "";
                    for (int i = 0; i < cat3code.Count(); i++)
                    {
                        stsval3 += "'" + cat3code[i] + "',";
                    }
                    sSql += " AND m.cat3code IN (" + ClassFunction.Left(stsval3, stsval3.Length - 1) + ")";
                }
            }

            if (cat4code != null)
            {
                if (cat4code.Count() > 0)
                {
                    string stsval4 = "";
                    for (int i = 0; i < cat4code.Count(); i++)
                    {
                        stsval4 += "'" + cat4code[i] + "',";
                    }
                    sSql += " AND m.cat4code IN (" + ClassFunction.Left(stsval4, stsval4.Length - 1) + ")";
                }
            }

            if (DDLStatus != "ALL")
            {
                sSql += " AND  m.activeflag='"+ DDLStatus + "'";
            }
           
            if (DDLType == "Kategori")
            {
                sSql += " Group By m.itemcode, c1.cat1longdesc, c2.cat2longdesc, c3.cat3longdesc, c4.cat4longdesc, itemdesc, m.itemnote, m.activeflag, gndesc, m.itemtype Order By m.itemcode ASC";
            }
            else
            {
                sSql += " Group By suppcode, suppname, Isnull(m.itemcode, ''), ISNULL(m.itemdesc, ''), Isnull(m.itemtype, ''), Isnull(m.itemnote, ''), Isnull(m.activeflag, ''), Isnull(unit, ''),ISNULL(m .cat1longdesc,'') , ISNULL(m.cat2longdesc,'') , ISNULL(m.cat3longdesc,'') , ISNULL(m.cat4longdesc,'') Order By suppcode";
            }

                DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            //rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}