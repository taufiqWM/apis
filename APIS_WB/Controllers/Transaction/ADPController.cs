﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class ADPController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public ADPController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class trnasset
        {
            public string cmpcode { get; set; }
            public int assetmstoid { get; set; }
            public string assetno { get; set; }
            public DateTime assetdate { get; set; }
            public string reftype { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
            public string assetmststatus { get; set; }
            public string assetmstnote { get; set; }
            public string divname { get; set; }
        }

        public class mstmat
        {
            public int matoid { get; set; }
            public string matcode { get; set; }
            public string matlongdesc { get; set; }
        }

        private void InitDDL(QL_assetmst tbl)
        {
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var assetacctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.assetacctgoid);
            ViewBag.assetacctgoid = assetacctgoid;
        }

        [HttpPost]
        public ActionResult BindListMat()
        {
            List<mstmat> tbl = new List<mstmat>();
            
            sSql = "SELECT itemoid AS matoid, itemcode AS matcode, itemdesc AS matlongdesc FROM QL_mstitem m WHERE m.cmpcode='" + CompnyCode + "' AND m.activeflag='ACTIVE' and itemtype = 'Asset' ORDER BY matcode";
            
            tbl = db.Database.SqlQuery<mstmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListCOA(string cmp, string sVar)
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + ClassFunction.GetDataAcctgOid(sVar, CompnyCode) + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string assetno, string cmpcode, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(assetno, cmpcode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_assetmst tbl)
        {
            ViewBag.reflongdesc = db.Database.SqlQuery<string>("SELECT sparepartlongdesc FROM QL_mstsparepart m WHERE sparepartoid=" + tbl.refoid).FirstOrDefault();

            ViewBag.reflongdesc = db.Database.SqlQuery<string>("SELECT itemdesc FROM QL_mstitem m WHERE itemoid=" + tbl.refoid).FirstOrDefault();
            
            tbl.assetmststatus = (tbl.assetmstres1 == "Asset In Progress Closed" ? "Closed" : tbl.assetmststatus);
        }

        [HttpPost]
        public ActionResult GenerateAssetsNo(string cmp)
        {
            var assetno = "";
            if (cmp != "")
            {
                string sNo = "ASSETS-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(assetno, " + int.Parse(DefaultCounter) + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_assetmst WHERE cmpcode='" + CompnyCode + "' AND assetno LIKE '%" + sNo + "%' ";
                assetno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultCounter));
            }
            return Json(assetno, JsonRequestBehavior.AllowGet);
        }

        private string GenerateNo(string cmp)
        {
            var assetno = "";
            if (cmp != "")
            {
                string sNo = "ASSETS-" + ClassFunction.GetServerTime().ToString("yyyy.MM") + "-";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(assetno, " + int.Parse(DefaultCounter) + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_assetmst WHERE cmpcode='" + CompnyCode + "' AND assetno LIKE '%" + sNo + "%' ";
                assetno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), int.Parse(DefaultCounter));
            }
            return assetno;
        }

        // GET: assetMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT am.cmpcode, assetmstoid, assetno, assetdate, reftype, (SELECT itemcode FROM QL_mstitem m WHERE itemoid=refoid) AS matcode, (SELECT itemdesc FROM QL_mstitem m WHERE itemoid=refoid) AS matlongdesc, (CASE WHEN ISNULL(assetmstres1, '')='Asset In Progress Closed' THEN 'Closed' ELSE assetmststatus END) assetmststatus, assetmstnote, 'False' AS checkvalue FROM QL_assetmst am WHERE assetmstres1 like 'Asset In Progress%' ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "AND am.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "AND am.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND assetdate>=CAST('" + modfil.filterperiodfrom + " 00:00:00' AS DATETIME) AND assetdate<=CAST('" + modfil.filterperiodto + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND (CASE WHEN ISNULL(assetmstres1, '')='Asset In Progress Closed' THEN 'Closed' ELSE assetmststatus END) IN ('In Process')";
            }

            List<trnasset> dt = db.Database.SqlQuery<trnasset>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: assetMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_assetmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_assetmst();
                tbl.assetmstoid = ClassFunction.GenerateID("QL_assetmst");
                tbl.assetdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.assetmststatus = "In Process";
            }
            else
            {
                action = "Edit";
                tbl = db.QL_assetmst.Find(CompnyCode, id);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: assetMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_assetmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.assetdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("assetdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            if (tbl.cmpcode == null)
                tbl.cmpcode = CompnyCode;
            if (tbl.assetno == null)
                tbl.assetno = "";

            tbl.assetpurchase = 0;
            tbl.assetpurchaseidr = 0;
            tbl.assetpurchaseusd = 0;
            tbl.assetvalue = 0;
            tbl.assetvalueidr = 0;
            tbl.assetvalueusd = 0;
            tbl.assetaccumdep = 0;
            tbl.assetaccumdepidr = 0;
            tbl.assetaccumdepusd = 0;
            tbl.accumdepacctgoid = 0;
            tbl.assetdepmonth = 0;
            tbl.assetdepvalue = 0;
            tbl.assetdepvalueidr = 0;
            tbl.assetdepvalueusd = 0;
            tbl.depacctgoid = 0;
            tbl.assetmstres1 = "Asset In Progress";
            tbl.assetmstres2 = "";
            tbl.assetmstres3 = "";
            tbl.deptoid = 0;
            tbl.curroid = 0;
            tbl.rateoid = 0;
            tbl.rate2oid = 0;
            tbl.assetdepdate = new DateTime(1900, 01, 01);
            tbl.closeuser = "";
            tbl.closetime = new DateTime(1900, 01, 01);
            tbl.assetmstflag = "";
            tbl.solduser = "";
            tbl.soldtime = new DateTime(1900, 01, 01);
            tbl.shipmentuser = "";
            tbl.shipmenttime = new DateTime(1900, 01, 01);
            tbl.disposeuser = "";
            tbl.disposetime = new DateTime(1900, 01, 01);
            tbl.shipmentamt = 0;
            tbl.shipmentamtidr = 0;
            tbl.shipmentamtusd = 0;
            tbl.revaluser = "";
            tbl.revaltime = new DateTime(1900, 01, 01);
            tbl.disposeacctgoid = 0;            

            if (tbl.assetacctgoid == 0)
                ModelState.AddModelError("", "Please select ASSETS ACCOUNT field!");
            tbl.inprogacctgoid = tbl.assetacctgoid;

            if (tbl.refoid == 0)
                ModelState.AddModelError("", "Please select MATERIAL field!");

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.assetdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.assetmststatus = "In Process";
            }
            if (tbl.assetmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.assetmststatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.assetmststatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_assetmst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_assetmst WHERE assetmstoid=" + tbl.assetmstoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_assetmst");
                        tbl.assetno = GenerateNo(CompnyCode);
                    }
                }
                var servertime = ClassFunction.GetServerTime();
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_assetmst.Find(tbl.cmpcode, tbl.assetmstoid) != null)
                                tbl.assetmstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.assetdate);
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            db.QL_assetmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_id SET lastoid=" + tbl.assetmstoid + " WHERE tablename='QL_assetmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: assetMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_assetmst tbl = db.QL_assetmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "DELETE FROM QL_assetmst WHERE assetmststatus='In Process' AND assetmstres1='Asset In Progress' AND assetmstoid=" + id;
                        db.Database.ExecuteSqlCommand(sSql);
                        if (db.SaveChanges() > 0)
                        {
                            sSql = "DELETE FROM QL_assetdtl WHERE assetmstoid=" + id;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_assetmst.Find(CompnyCode, id);
            if (tbl == null)
                return null;
            
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPrintasset.rpt"));
            
            report.SetParameterValue("sWhere", " WHERE am.cmpcode='" + CompnyCode + "' AND am.assetmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "ADPPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}