﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class SalesCustController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public SalesCustController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET/POST: UserRole
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            string sSql = "SELECT ur.cmpcode, ur.usoid, usname, COUNT(*) total from QL_mstsalescust ur INNER JOIN QL_m01US us ON us.usoid=ur.usoid INNER JOIN QL_m05GN g ON g.gnoid=us.Jabatanoid WHERE ur.cmpcode='" + Session["CompnyCode"].ToString() + "' AND g.gndesc='SALES' " + sfilter + " group by ur.cmpcode, ur.usoid, usname";
            var QL_mstsalescust = db.Database.SqlQuery<listsalescust>(sSql).ToList();
            return View(QL_mstsalescust);
        }

        // GET: UserRole/Form/5
        public ActionResult Form(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstsalescust QL_mstsalescust;
            string action = "Create";
            if (id == null)
            {
                QL_mstsalescust = new QL_mstsalescust();
                QL_mstsalescust.cmpcode = Session["CompnyCode"].ToString();
                Session["QL_mstsalescust"] = null;
            }
            else
            {
                action = "Edit";
                QL_mstsalescust = db.QL_mstsalescust.Where(w => w.usoid == id).FirstOrDefault();
                string sSql = "select ur.custoid, rl.custname, ur.salescustflag activeflag from QL_mstsalescust ur inner join QL_mstcust rl ON rl.custoid=ur.custoid where ur.usoid='" + id + "'";
                Session["QL_mstsalescust"] = db.Database.SqlQuery<salescust>(sSql).ToList();
            }

            if (QL_mstsalescust == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(QL_mstsalescust);
            return View(QL_mstsalescust);
        }

        // POST: UserRole/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstsalescust tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            List<salescust> dataDtl = (List<salescust>)Session["QL_mstsalescust"];
            if (dataDtl == null)
                ModelState.AddModelError("", "Please fill Customer Detail");
            else if (dataDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill Customer Detail");

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstsalescust");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                        }
                        else if (action == "Edit")
                        {
                            //delete old
                            var trndtl = db.QL_mstsalescust.Where(a => a.usoid == tbl.usoid);
                            db.QL_mstsalescust.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        //insert
                        QL_mstsalescust tbldtl;
                        for (int i = 0; i < dataDtl.Count(); i++)
                        {
                            //sSql = "INSERT INTO QL_mstsalescust (cmpcode, uroid, usoid, rloid, urflag, upduser, updtime) VALUES ('"+ tbl.cmpcode + "', "+ (mstoid++) + ", '"+ tbl.usoid + "', "+ dataDtl[i].rloid + ", '"+ dataDtl[i].urflag + "', '" + Session["UserID"].ToString() + "', '" + servertime + "')";
                            //db.Database.ExecuteSqlCommand(sSql);
                            tbldtl = new QL_mstsalescust();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.usoid = tbl.usoid;
                            tbldtl.salescustoid = mstoid++;
                            tbldtl.custoid = dataDtl[i].custoid;
                            tbldtl.salescustflag = dataDtl[i].activeflag;
                            tbldtl.upduser = Session["UserID"].ToString();
                            tbldtl.updtime = servertime;
                            db.QL_mstsalescust.Add(tbldtl);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstsalescust'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tbl);
            return View(tbl);
        }

        private void setViewBag(QL_mstsalescust QL_mstsalescust)
        {

            sSql = " SELECT * FROM QL_m01US us INNER JOIN QL_m05GN g ON g.gnoid=us.Jabatanoid WHERE ( gndesc='SALES' AND usflag = 'ACTIVE' AND usoid<>'admin' AND usoid not in (select x.usoid from QL_mstsalescust x)) OR usoid = '" + QL_mstsalescust.usoid + "'";
            ViewBag.usoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql), "usoid", "usname", QL_mstsalescust.usoid);
            ViewBag.custoid = new SelectList(db.QL_mstcust, "custoid", "custname");
        }

        public ActionResult getQL_mstsalescust()
        {
            if (Session["QL_mstsalescust"] == null)
            {
                Session["QL_mstsalescust"] = new List<salescust>();
            }

            List<salescust> dataDtl = (List<salescust>)Session["QL_mstsalescust"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult setQL_mstsalescust(List<salescust> mdataDtl)
        {
            Session["QL_mstsalescust"] = mdataDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        // POST: UserRole/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            using (var objTrans = db.Database.BeginTransaction())
            {
                try
                {
                    var userroles = db.QL_mstsalescust.Where(a => a.usoid == id);
                    db.QL_mstsalescust.RemoveRange(userroles);
                    db.SaveChanges();
                    // Oh we are here, looks like everything is fine - save all the data permanently
                    objTrans.Commit();
                }
                catch (Exception ex)
                {
                    // roll back all database operations, if any thing goes wrong
                    objTrans.Rollback();

                    result = "failed";
                    msg += ex.ToString();
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public class salescust
        {
            public int custoid { get; set; }
            public string custname { get; set; }
            public string activeflag { get; set; }
        }

        public class listsalescust
        {
            public string cmpcode { get; set; }
            public string usoid { get; set; }
            public string usname { get; set; }
            public int total { get; set; }
        }
    }
}