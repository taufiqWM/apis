﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class UBController : Controller
    {
        // GET: UB
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public UBController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class matusagemst
        {
            public string cmpcode { get; set; }
            public string divname { get; set; }
            public int matusagemstoid { get; set; }
            public string matusageno { get; set; }
            public DateTime matusagedate { get; set; }
            public string deptname { get; set; }
            public string matusagewh { get; set; }
            public string matusagemststatus { get; set; }
            public string matusagemstnote { get; set; }
            public string createuser { get; set; }
        }

        public class matusagedtl
        {
            public int matusagedtlseq { get; set; }
            public int matusagewhoid { get; set; }
            public string matusagewh { get; set; }
            public string matusagereftype { get; set; }
            public int matusagerefoid { get; set; }
            public string matusagerefcode { get; set; }
            public string matusagereflongdesc { get; set; }
            public decimal matusageqty { get; set; }
            public decimal stockqty { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int matusageunitoid { get; set; }
            public string matusageunit { get; set; }
            public string matusagedtlnote { get; set; }
            public decimal matusagevalueidr { get; set; }
            public decimal matusagevalueusd { get; set; }
            public int rabmstoid { get; set; }
            public string projectname { get; set; }
        }

        public class matreqmst
        {
            public int matreqmstoid { get; set; }
            public string matreqno { get; set; }
            public DateTime matreqdate { get; set; }
            public int matreqwhoid { get; set; }
            public string matreqwh { get; set; }
            public string matreqmstnote { get; set; }
        }

        public class mrassetmst
        {
            public int mrassetmstoid { get; set; }
            public string mrassetno { get; set; }
            public DateTime mrassetdate { get; set; }
            public string suppname { get; set; }
            public int mrassetwhoid { get; set; }
            public string mrassetwh { get; set; }
            public string mrassetmstnote { get; set; }
        }

        private string generateNo(string cmp, DateTime tgl)
        {
            string sCode = "USG/" + tgl.ToString("yy") + "/" + tgl.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(matusageno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnmatusagemst WHERE cmpcode='" + cmp + "' AND matusageno LIKE '" + sCode + "%'";
            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);
            sCode += sCounter;
            return sCode;
        }

        private void InitDDL(QL_trnmatusagemst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";
            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY groupdesc";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gnflag='ACTIVE' AND gngroup='GUDANG' ORDER BY gndesc";
            var whoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.whoid);
            ViewBag.whoid = whoid;

            string acctgoid = ClassFunction.GetDataAcctgOid("VAR_USAGE_NON_KIK", CompnyCode);
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            var reasonoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.reasonoid);
            ViewBag.reasonoid = reasonoid;
        }

        [HttpPost]
        public ActionResult InitDDLDepartment(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstdeptgroup> tbl = new List<QL_mstdeptgroup>();
            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + cmpcode + "' AND activeflag='ACTIVE' ORDER BY groupdesc";
            tbl = db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWH(string cmpcode)
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE gnother1='" + cmpcode + "' AND activeflag='ACTIVE' AND gngroup='GUDANG' ORDER BY gndesc";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataDetails(string cmpcode, string mattype, int whoid, int oid)
        {
            List<matusagedtl> tbl = new List<matusagedtl>();
            string sSql = ""; string sJoin = "";
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

            if (oid != 0)
            {
                sJoin = " INNER JOIN QL_trnmrassetdtl mrd ON mrd.mrassetrefoid=tbl.matusagerefoid AND ISNULL(mrd.refno,'')=tbl.refno AND ISNULL(mrd.serialnumber,'')=tbl.serialnumber AND mrd.mrassetmstoid=" + oid + " INNER JOIN QL_trnmrassetmst mrm ON mrm.mrassetmstoid=mrd.mrassetmstoid AND mrm.mrassetwhoid=" + whoid +" ";
            }

            sSql = "SELECT matusagedtlseq, matusagerefoid, matusagerefcode, matusagereflongdesc, matusageunitoid, matusageunit, (stockqty - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.matusagerefoid AND mb.mtrwhoid=" + whoid + " AND ISNULL(mb.refno,'')=tbl.refno AND ISNULL(mb.serialnumber,'')=tbl.serialnumber AND mb.flag='Post'),0.0)) stockqty, tbl.refno, tbl.serialnumber, matusageqty, matusagedtlnote, rabmstoid, projectname FROM( ";
            sSql += "SELECT 0 matusagedtlseq, itemoid matusagerefoid, itemcode AS matusagerefcode, itemdesc AS matusagereflongdesc, itemunitoid matusageunitoid, gndesc AS matusageunit, ISNULL(SUM(qtyin-qtyout),0.0) AS stockqty, ISNULL(con.refno,'') refno, ISNULL(con.serialnumber,'') serialnumber, 0.0 AS matusageqty, '' AS matusagedtlnote, ISNULL(con.rabmstoid,0) rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ISNULL(con.rabmstoid,0)),'') projectname FROM QL_mstitem m INNER JOIN QL_m05GN g ON gnoid=itemunitoid INNER JOIN QL_conmat con ON con.refoid = m.itemoid WHERE m.itemtype='" + mattype + "' AND con.mtrwhoid="+ whoid + " GROUP BY m.itemoid, itemcode, ISNULL(con.refno,''), ISNULL(con.serialnumber,''), ISNULL(con.rabmstoid,0), itemdesc, itemunitoid, itemtype, g.gndesc";
            sSql += " )AS tbl " + sJoin + " WHERE (stockqty - ISNULL((SELECT SUM(mb.qtyout) FROM QL_matbooking mb WHERE mb.refoid=tbl.matusagerefoid AND mb.mtrwhoid=" + whoid + " AND ISNULL(mb.refno,'')=tbl.refno AND ISNULL(mb.serialnumber,'')=tbl.serialnumber AND mb.flag='Post'),0.0))>0 ORDER BY tbl.matusagerefcode";

            tbl = db.Database.SqlQuery<matusagedtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<matusagedtl> dtDtl)
        {
            Session["QL_trnmatusagedtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnmatusagedtl"] == null)
            {
                Session["QL_trnmatusagedtl"] = new List<matusagedtl>();
            }

            List<matusagedtl> dataDtl = (List<matusagedtl>)Session["QL_trnmatusagedtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnmatusagemst tbl)
        {
            ViewBag.mrassetno = db.Database.SqlQuery<string>("SELECT mrassetno FROM QL_trnmrassetmst WHERE mrassetmstoid='" + tbl.mrassetmstoid + "'").FirstOrDefault();

            //try
            //{
            //    ViewBag.personname = db.Database.SqlQuery<string>("SELECT personname FROM " + HRISServer + "tv_allperson WHERE cmpcode='" + tbl.personcmpcode + "' AND personoid='" + tbl.personoid + "'").FirstOrDefault();
            //}
            //catch
            //{
            //    ViewBag.personname = db.Database.SqlQuery<string>("SELECT personname FROM QL_mstperson WHERE cmpcode='" + tbl.cmpcode + "' AND personoid='" + tbl.personoid + "'").FirstOrDefault();
            //}
        }

        [HttpPost]
        public ActionResult BindListMatReq(string cmp, int deptoid)
        {
            List<matreqmst> tbl = new List<matreqmst>();
            sSql = "SELECT matreqmstoid, matreqno, matreqdate, matreqwhoid, gendesc AS matreqwh, matreqmstnote FROM QL_trnmatreqmst req INNER JOIN QL_mstgen g ON genoid=matreqwhoid WHERE req.cmpcode='" + cmp + "' AND matreqmststatus='Post' AND deptoid=" + deptoid + " ORDER BY matreqmstoid";
            tbl = db.Database.SqlQuery<matreqmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BindListMRAsset()
        {
            List<mrassetmst> tbl = new List<mrassetmst>();
            sSql = "SELECT mrassetmstoid, mrassetno, mrassetdate, suppname, mrassetwhoid, gndesc AS matreqwh, mrassetmstnote FROM QL_trnmrassetmst req INNER JOIN QL_m05GN g ON gnoid=mrassetwhoid INNER JOIN QL_mstsupp s ON s.suppoid=req.suppoid WHERE tipebarang NOT IN('Asset')  ORDER BY mrassetno";
            tbl = db.Database.SqlQuery<mrassetmst>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET: MatUsageNonKIK
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT reqm.cmpcode,div.divname, reqm.matusagemstoid, reqm.matusageno, reqm.matusagedate, de.groupdesc deptname, reqm.matusagemststatus, reqm.matusagemstnote, reqm.createuser FROM QL_trnmatusagemst reqm INNER JOIN QL_mstdeptgroup de ON de.cmpcode=reqm.cmpcode AND de.groupoid=reqm.deptoid INNER JOIN QL_mstdivision div ON div.cmpcode=reqm.cmpcode WHERE ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "reqm.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "reqm.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND matusagedate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND matusagedate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND matusagemststatus IN ('In Process', 'Revised')";
            }

            sSql += " AND reqm.assetmstoid=0";

            //if (!ClassFunction.isSpecialAccess(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<RoleSpecial>)Session["SpecialAccess"]))
            //    sSql += " AND reqm.createuser='" + Session["UserID"].ToString() + "'";

            List<matusagemst> dt = db.Database.SqlQuery<matusagemst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: MatUsageNonKIK/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmatusagemst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnmatusagemst();
                tbl.cmpcode = CompnyCode;
                tbl.matusagemstoid = ClassFunction.GenerateID("QL_trnmatusagemst");
                tbl.matusagedate = ClassFunction.GetServerTime();
                tbl.mrassetmstoid = 0;
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.matusagemststatus = "In Process";

                Session["QL_trnmatusagedtl"] = null;
            }
            else
            {
                action = "Edit";
                string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
                tbl = db.QL_trnmatusagemst.Find(CompnyCode, id);

                sSql = "SELECT DISTINCT matusagedtlseq, matusagewhoid, g1.gndesc AS matusagewh, matusagereftype, matusagerefoid, itemcode AS matusagerefcode, itemdesc AS matusagereflongdesc, matusageqty, ISNULL((SELECT SUM(crd.qtyin-qtyout) FROM QL_conmat crd WHERE crd.cmpcode=mud.cmpcode AND refoid=matusagerefoid AND mtrwhoid=matusagewhoid), 0.0) - ISNULL((SELECT SUM(qtyout) FROM QL_matbooking mb WHERE mb.refoid=mud.matusagerefoid AND mb.mtrwhoid=mud.matusagewhoid AND ISNULL(mb.refno,'')=mud.refno AND ISNULL(mb.serialnumber,'')=mud.serialnumber),0.0) AS stockqty, ISNULL(mud.refno,'') refno, ISNULL(mud.serialnumber,'') serialnumber, matusageunitoid, g2.gndesc AS matusageunit, matusagedtlnote, 0.0000 AS matusagevalueidr, 0.0000 AS matusagevalueusd, ISNULL(mud.rabmstoid,0) rabmstoid, ISNULL((SELECT rm.projectname FROM QL_trnrabmst rm WHERE rm.rabmstoid=ISNULL(mud.rabmstoid,0)),'') projectname FROM QL_trnmatusagedtl mud INNER JOIN QL_m05GN g1 ON g1.gnoid=mud.matusagewhoid INNER JOIN QL_m05GN g2 ON g2.gnoid=mud.matusageunitoid INNER JOIN QL_mstitem i ON i.itemoid=mud.matusagerefoid WHERE mud.cmpcode='" + tbl.cmpcode + "' AND matusagemstoid=" + id + " ORDER BY matusagedtlseq";
                Session["QL_trnmatusagedtl"] = db.Database.SqlQuery<matusagedtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MatUsageNonKIK/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnmatusagemst tbl, string action, string closing, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            try
            {
                tbl.matusagedate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("matusagedate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            var servertime = ClassFunction.GetServerTime();
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(servertime);
            int iStockAcctgoid = 0; decimal dTotalAmt = 0;
            tbl.cmpcode = CompnyCode;

            if (tbl.matusagemststatus == "Post")
            {
                tbl.matusageno = generateNo(tbl.cmpcode, ClassFunction.GetServerTime());
            }
            else
            {
                if (tbl.matusageno == null)
                    tbl.matusageno = "";
            }
            tbl.assetmstoid = 0;

            List<matusagedtl> dtDtl = (List<matusagedtl>)Session["QL_trnmatusagedtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].matusageqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY Usage Qty must be more than 0!");
                        }
                        dtDtl[i].matusagevalueidr = 0;
                        dtDtl[i].matusagevalueusd = 0;
                        if (tbl.matusagemststatus == "Post")
                        {
                            if (!ClassFunction.IsStockAvailable(tbl.cmpcode, servertime, dtDtl[i].matusagerefoid, dtDtl[i].matusagewhoid, dtDtl[i].matusageqty, dtDtl[i].refno, dtDtl[i].rabmstoid, dtDtl[i].serialnumber))
                            {
                                ModelState.AddModelError("", "USAGE QTY field Code " + dtDtl[i].matusagerefcode + " must be less than STOCK QTY!");
                            }
                            //Stock Value
                            decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].matusagerefoid);
                            decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].matusagerefoid, dtDtl[i].matusageqty, sValue, "OUT");
                            dtDtl[i].matusagevalueidr = sAvgValue;
                        }
                    }
                }
            }

            if (tbl.matusagemststatus == "Post")
            {
                //Ger Var Interface
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", tbl.cmpcode))
                {
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));
                }
                iStockAcctgoid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", tbl.cmpcode));
                if (tbl.reasonoid == 0)
                {
                    ModelState.AddModelError("", "Please select default COA Debet!");
                }
                dTotalAmt = dtDtl.Sum(m => m.matusagevalueidr);
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.matusagedate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.matusagemststatus = "In Process";
            }
            if (tbl.matusagemststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.matusagemststatus = "In Process";
                }
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnmatusagemst");
                var dtloid = ClassFunction.GenerateID("QL_trnmatusagedtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trnmatusagemst.Find(tbl.cmpcode, tbl.matusagemstoid) != null)
                                tbl.matusagemstoid = mstoid;

                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.matusagedate);
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnmatusagemst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.matusagemstoid + " WHERE tablename='QL_trnmatusagemst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnmatusagedtl.Where(a => a.matusagemstoid == tbl.matusagemstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnmatusagedtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnmatusagedtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnmatusagedtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.matusagedtloid = dtloid++;
                            tbldtl.matusagemstoid = tbl.matusagemstoid;
                            tbldtl.matusagedtlseq = i + 1;
                            tbldtl.matreqmstoid = 0;
                            tbldtl.matreqdtloid = 0;
                            tbldtl.matusagewhoid = dtDtl[i].matusagewhoid;
                            tbldtl.matusagereftype = dtDtl[i].matusagereftype;
                            tbldtl.matusagerefoid = dtDtl[i].matusagerefoid;
                            tbldtl.matusageqty = dtDtl[i].matusageqty;
                            tbldtl.matusageunitoid = dtDtl[i].matusageunitoid;
                            tbldtl.matusagedtlstatus = "";
                            tbldtl.refno = (dtDtl[i].refno == null ? "" : dtDtl[i].refno);
                            tbldtl.matusagedtlnote = (dtDtl[i].matusagedtlnote == null ? "" : dtDtl[i].matusagedtlnote);
                            tbldtl.matusagevalueidr = dtDtl[i].matusagevalueidr;
                            tbldtl.matusagevalueusd = 0;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.rabmstoid = dtDtl[i].rabmstoid;
                            tbldtl.serialnumber = (dtDtl[i].serialnumber == null ? "" : dtDtl[i].serialnumber);
                            db.QL_trnmatusagedtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.matusagemststatus == "Post")
                            {
                                var refname = "FINISH GOOD";
                                if (dtDtl[i].matusagereftype == "Asset")
                                {
                                    refname = "FIXED ASSET";
                                }
                                // Insert QL_conmat
                                db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "USG", "QL_trnmatusagedtl", tbl.matusagemstoid, dtDtl[i].matusagerefoid, refname, dtDtl[i].matusagewhoid, dtDtl[i].matusageqty * -1, "Mat Usage", "Mat Usage No. " + tbl.matusageno + "", Session["UserID"].ToString(), tbldtl.refno, dtDtl[i].matusagevalueidr, 0, 0, null, tbldtl.matusagedtloid, dtDtl[i].rabmstoid, dtDtl[i].matusagevalueidr, tbldtl.serialnumber));
                                db.SaveChanges();
                            }
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnmatusagedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.matusagemststatus == "Post")
                        {
                            sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Mat Usage|No. " + tbl.matusageno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            // Insert QL_trngldtl
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, tbl.reasonoid, "D", dTotalAmt, tbl.matusageno, "Mat Usage|No. " + tbl.matusageno, "Post", Session["UserID"].ToString(), servertime, dTotalAmt, 0, "QL_trnmatusagemst " + tbl.matusagemstoid, null, null, null, 0));
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iStockAcctgoid, "C", dTotalAmt, tbl.matusageno, "Mat Usage|No. " + tbl.matusageno, "Post", Session["UserID"].ToString(), servertime, dTotalAmt, 0, "QL_trnmatusagemst " + tbl.matusagemstoid, null, null, null, 0));

                            sSql = "UPDATE QL_ID SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        //if (string.IsNullOrEmpty(closing))
                        //    return RedirectToAction("Form/" + tbl.matusagemstoid + "/" + tbl.cmpcode);
                        //else
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        tbl.matusagemststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.matusagemststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: MatUsageNonKIK/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string cmp)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmatusagemst tbl = db.QL_trnmatusagemst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnmatusagedtl.Where(a => a.matusagemstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnmatusagedtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnmatusagemst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            var tbl = db.QL_trnmatusagemst.Find(CompnyCode, id);
            if (tbl == null)
                return null;

            var rptname = "rptMatUsage";
            report.Load(Path.Combine(Server.MapPath("~/Report"), rptname + ".rpt"));

            sSql = "SELECT DISTINCT mum.matusagemstoid, CONVERT(VARCHAR(20), mum.matusagemstoid) AS [Draft No.], mum.matusageno AS [Usage No.], mum.matusagedate AS [Usage Date], '' [Division], de.groupdesc AS [Department], mum.matusagemststatus AS [Status], mum.matusagemstnote AS [Header Note], mud.matusagedtloid, mud.matusagedtlseq AS [No.], '' AS [Request No.], g1.gndesc AS [Warehouse], mud.matusagereftype AS [Type], mud.matusagerefoid, itemcode AS [Code], itemdesc AS [Description], mud.matusageqty AS [Qty], g2.gndesc AS [Unit], mud.matusagedtlnote AS [Detail Note], di.divname AS [Business Unit], '' AS [Detail Location], ''  [Person Name] , ''  [NIP], ISNULL(mud.refno,'') refno, ISNULL(mud.serialnumber,'') serialnumber  FROM QL_trnmatusagemst mum INNER JOIN QL_mstdeptgroup de ON de.cmpcode=mum.cmpcode AND de.groupoid=mum.deptoid INNER JOIN QL_trnmatusagedtl mud ON mud.cmpcode=mum.cmpcode AND mud.matusagemstoid=mum.matusagemstoid INNER JOIN QL_m05GN g1 ON g1.gnoid=mud.matusagewhoid INNER JOIN QL_m05GN g2 ON g2.gnoid=mud.matusageunitoid INNER JOIN QL_mstdivision di ON di.cmpcode=mum.cmpcode INNER JOIN QL_mstitem i ON i.itemoid=mud.matusagerefoid WHERE mud.cmpcode='" + CompnyCode + "' AND mud.matusagemstoid=" + id + " ORDER BY mum.matusagemstoid, mud.matusagedtlseq ";

            ClassConnection cConn = new ClassConnection();
            DataTable dtRpt = cConn.GetDataTable(sSql, rptname);

            report.SetDataSource(dtRpt);
            //report.SetParameterValue("diPrint", Session["UserID"].ToString());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "MatUsagePrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}