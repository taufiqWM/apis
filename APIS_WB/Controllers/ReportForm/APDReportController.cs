﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class APDReportController : Controller
    {
        // GET: APDReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public APDReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetSupptData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.suppcode [Kode], c.suppname [Nama], c.suppaddr [Alamat] FROM QL_mstsupp c WHERE c.suppoid IN (SELECT apm.suppoid FROM QL_trnapdirmst apm Where apm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                } 
                sSql += ") ORDER BY c.suppcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblSupp");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
        
        [HttpPost]
        public ActionResult GetDataAPD(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, apm.apdirmstoid, apm.apdirno [No. AP Direct], CONVERT(CHAR(10),apm.apdirdate,103) Tanggal, sup.suppname Supplier, apm.apdirmststatus [Status] From QL_trnapdirmst apm INNER JOIN QL_mstsupp sup ON sup.suppoid=apm.suppoid WHERE apm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    if (TextSupp != null)
                    {
                        string[] arr = TextSupp.Split(';'); string datafilter = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            datafilter += "'" + arr[i] + "',";
                        }
                        sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                    }                  
                } 

                sSql += " ORDER BY apm.apdirno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        //[HttpPost]
        //public ActionResult GetRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string apdirno, string TextSupp)
        //{
        //    var result = ""; JsonResult js = null;
        //    List<string> tblcols = new List<string>();
        //    List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

        //    try
        //    {
        //        sSql = "SELECT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnapdirmst apm ON apm.rabmstoid=rm.rabmstoid INNER JOIN QL_mstsupp c ON c.suppoid=apm.suppoid WHERE apm.cmpcode='" + CompnyCode + "'";
        //        if (StartPeriod != "" && EndPeriod != "")
        //        {
        //            sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
        //        }

        //        if (DDLStatus != null)
        //        {
        //            if (DDLStatus.Count() > 0)
        //            {
        //                string stsval = "";
        //                for (int i = 0; i < DDLStatus.Count(); i++)
        //                {
        //                    stsval += "'" + DDLStatus[i] + "',";
        //                }
        //                sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
        //            }
        //        }

        //        if (TextSupp != "")
        //        {
        //            if (TextSupp != null)
        //            {
        //                string[] arr = TextSupp.Split(';'); string datafilter = "";
        //                for (int i = 0; i < arr.Count(); i++)
        //                {
        //                    datafilter += "'" + arr[i] + "',";
        //                }
        //                sSql += " AND c.suppcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
        //            }                    
        //        }

        //        if (apdirno != "")
        //        {
        //            if (apdirno != null)
        //            {
        //                string[] arr = apdirno.Split(';'); string datafilter = "";
        //                for (int i = 0; i < arr.Count(); i++)
        //                {
        //                    datafilter += "'" + arr[i] + "',";
        //                }
        //                sSql += " AND apm.apdirno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
        //            }                    
        //        }

        //        sSql += " ORDER BY apm.apdirno";

        //        DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
        //        if (tbl.Rows.Count > 0)
        //        {
        //            int i = 1;
        //            Dictionary<string, object> row;
        //            foreach (DataRow dr in tbl.Rows)
        //            {
        //                row = new Dictionary<string, object>();
        //                foreach (DataColumn col in tbl.Columns)
        //                {
        //                    var item = dr[col].ToString();
        //                    if (col.ColumnName == "seq")
        //                        item = (i++).ToString();
        //                    row.Add(col.ColumnName, item);
        //                    if (!tblcols.Contains(col.ColumnName))
        //                        tblcols.Add(col.ColumnName);
        //                }
        //                tblrows.Add(row);
        //            }
        //        }
        //        else
        //            result = "Data Not Found.";
        //    }
        //    catch (Exception e)
        //    {
        //        result = e.ToString();
        //    }

        //    js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
        //    js.MaxJsonLength = Int32.MaxValue;
        //    return js;
        //}

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextSupp, string StartPeriod, string EndPeriod, string DDLPeriod, string apdirno)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select DISTINCT 0 seq, acc.acctgcode [Kode], acctgdesc [Deskripsi] From QL_trnapdirmst apm INNER JOIN QL_mstsupp sup ON sup.suppoid=apm.suppoid INNER JOIN QL_trnapdirdtl apd ON apm.apdirmstoid=apd.apdirmstoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=apd.acctgoid LEFT JOIN (Select rabmstoid, rabno, projectname from QL_trnrabmst rab ) rab ON rab.rabmstoid=apm.rabmstoid Where apm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextSupp != "")
                {
                    if (TextSupp != null) {
                        string[] arr = TextSupp.Split(';'); string datafilter = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            datafilter += "'" + arr[i] + "',";
                        }
                        sSql += " AND acc.acctgcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                    }                    
                }

                if (apdirno != "")
                {
                    if (apdirno != null) {
                        string[] arr = apdirno.Split(';'); string datafilter = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            datafilter += "'" + arr[i] + "',";
                        }
                        sSql += " AND apm.apdirno IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                    }                    
                }
                sSql += " ORDER BY acctgcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listddl
        {
            public int ddlvalue { get; set; }
            public string ddltext { get; set; }
        }

        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            sSql = "SELECT * FROM( SELECT 0 ddlvalue, 'ALL' ddltext  UNION ALL  SELECT groupoid ddlvalue, groupdesc ddltext FROM QL_mstdeptgroup WHERE activeflag='ACTIVE' )AS tb";
            var deptoid = new SelectList(db.Database.SqlQuery<listddl>(sSql).ToList(), "ddlvalue", "ddltext", null);
            ViewBag.DDLDept = deptoid;

            return View();
        }
        

        [HttpPost]
        public ActionResult PrintReport(string  DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextSupp, string apdirno, string DDLNomorAP, string TextMaterial, string DDLDept, string DDLSorting, string DDLSortDir, string reporttype)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptname = ""; var rptfile = ""; var Dtl = ""; var join = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptAPDirectSumXls.rpt";
                }
                else
                {
                    rptfile = "rptAPDirectSumPdf.rpt";
                } 
                rptname = "APDIRECT_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptAPDirectDtlXls.rpt";
                }
                else
                {
                    rptfile = "rptAPDirectDtlPdf.rpt";
                }  
                rptname = "APDIRECT_DETAIL";
            }
            if (DDLType == "Detail")
            {
                Dtl = ", acc.acctgcode, acctgdesc, apd.apdirprice, apm.apdirtaxvalue, 0.0 TaxDtlAmt, apd.apdirprice amtnettodtl, apd.apdirdtlnote, acc.acctgoid ";
                join = " INNER JOIN QL_trnapdirdtl apd ON apm.apdirmstoid=apd.apdirmstoid INNER JOIN QL_mstacctg acc ON acc.acctgoid=apd.acctgoid ";
            }

            sSql = "Select apm.cmpcode, apm.apdirmstoid, apm.apdirno, apm.apdirdate, sup.suppname, apm.apdirmststatus, apm.apdirmstnote, apm.apdirtaxamt, apm.apdirtotalamt, apm.apdirtotalnetto, apm.apdirgrandtotal, ISNULL((SELECT ax.acctgdesc FROM QL_mstacctg ax WHERE ax.acctgoid=apm.acctgpphoid),'') akunpph, apm.pphamt, ISNULL(rab.rabmstoid,0) rabmstoid, ISNULL(rab.rabno, '-') rabno, ISNULL(rab.projectname, '') projectname, ISNULL(apm.apdirfaktur, '') apdirfaktur, apm.updtime, apm.apdirduedate, apm.apdirsuppfjdate, g.gndesc [top], ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype='QL_trnapdirmst' AND con.refoid=apm.apdirmstoid AND con.payrefoid<>0),0.0)  [Paid Amt], apm.apdirgrandtotal - ISNULL((SELECT SUM(con.amtbayar) FROM QL_conap con WHERE con.reftype='QL_trnapdirmst' AND con.refoid=apm.apdirmstoid AND con.payrefoid<>0),0.0)  [Balance Amt] " + Dtl + " From QL_trnapdirmst apm INNER JOIN QL_mstsupp sup ON sup.suppoid=apm.suppoid INNER JOIN QL_m05GN g ON g.gnoid=apm.apdirpaytypeoid " + join + " LEFT JOIN (Select rabmstoid, rabno, projectname from QL_trnrabmst rab) rab ON rab.rabmstoid=apm.rabmstoid Where apm.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND apm.apdirmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextSupp != "")
            {
                if (TextSupp != null)
                {
                    string[] arr = TextSupp.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND sup.suppcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }                
            }

            if (apdirno != "")
            {
                if (apdirno != "")
                {
                    string[] arr = apdirno.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomorAP + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }                
            }

            if (DDLDept != "0")
            {
                sSql += " AND apm.deptoid = " + DDLDept + "";
            }
           sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "" + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);
            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperFolio;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperFolio;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }


        }      

    }
}