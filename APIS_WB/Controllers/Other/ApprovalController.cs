﻿using APIS_WB.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.ViewModel;
using System.Data.Entity;

namespace APIS_WB.Controllers.Other
{
    public class ApprovalController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public ApprovalController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET: UP/Form/5
        public ActionResult Form(int? id, string refname)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            List<dtAppHeader> dtHeader = new List<dtAppHeader>();
            ViewBag.id = id;
            ViewBag.refname = refname;
            return View(dtHeader);
        }

        // GET: UP/Form/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(List<dtAppHeader> dtHeader, int id, string refname, string status)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var servertime = ClassFunction.GetServerTime();
            //var msg ="";
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + refname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            var appoid = ClassFunction.GenerateID("QL_APP");

            //List<dtAppHeader> dtHeader = new List<dtAppHeader>();
            // Deklarasi data utk approval
            QL_APP tblapp = new QL_APP();
            if (stoid_app != 0 & status == "Approved")
            {
                tblapp.cmpcode = Session["CompnyCode"].ToString();
                tblapp.appoid = appoid;
                tblapp.appform = refname;
                tblapp.appformoid = id;
                tblapp.requser = Session["UserID"].ToString();
                tblapp.reqdate = servertime;
                tblapp.appstoid = stoid_app;
                db.QL_APP.Add(tblapp);
                db.SaveChanges();

            }

            ViewBag.id = id;
            ViewBag.refname = refname;
            return View(dtHeader);
        }
        public ActionResult getData(int? id, string refname)
        {
            var msg = "No data Found";
            return Json(new { msg }, JsonRequestBehavior.AllowGet);
        }

    }
}