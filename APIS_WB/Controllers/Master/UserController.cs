﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;

namespace APIS_WB.Controllers.Master
{
    public class UserController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string userimgpath = "~/Images/UserImage";
        private string usersignpath = "~/Images/UserSign";
        private string imgtemppath = "~/Images/ImagesTemps";

        public UserController()
        {
            db.Database.CommandTimeout = 0;
        }

        // GET/POST: User
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "Select * FROM QL_m01US us WHERE us.cmpcode='" + Session["CompnyCode"].ToString() + "' AND usoid<>'admin' AND ustype IN('admin','user') " + sfilter;
            List<QL_m01US> mstuser = db.Database.SqlQuery<QL_m01US>(sSql).ToList();

            return View(mstuser);
        }


        // GET: User/Form/5
        public ActionResult Form(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_m01US qL_m01US;
            string action = "Create";
            if (id == null)
            {
                qL_m01US = new QL_m01US();
                qL_m01US.cmpcode = Session["CompnyCode"].ToString();
                qL_m01US.crtuser = Session["UserID"].ToString();
                qL_m01US.crttime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                qL_m01US = db.QL_m01US.Find(Session["CompnyCode"].ToString(), id);
            }
            
            if (qL_m01US == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(qL_m01US);
            return View(qL_m01US);
        }

        // POST: User/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_m01US qL_m01US, string action, string tglmst, string tglmsk, string tglkluar)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(qL_m01US.usoid))
                ModelState.AddModelError("usoid", "Please Fill User ID!!");
            else if (db.QL_m01US.Where(w => w.usoid == qL_m01US.usoid).Count() > 0 & action == "Create")
                ModelState.AddModelError("usoid", "This User ID already use, Please Fill Another User ID!!");
            if (string.IsNullOrEmpty(qL_m01US.usname))
                ModelState.AddModelError("usname", "Please Fill User Name");
            if (string.IsNullOrEmpty(qL_m01US.uspassword))
                ModelState.AddModelError("uspassword", "Please Fill Password");
            if (string.IsNullOrEmpty(qL_m01US.ustype))
                ModelState.AddModelError("ustype", "Please Fill User Type");
            if (string.IsNullOrEmpty(qL_m01US.usemail))
                ModelState.AddModelError("stoid", "Please Fill Email");
            //if (string.IsNullOrEmpty(qL_m01US.stoid))
            //    ModelState.AddModelError("usemail", "Please Fill Email");
            if (string.IsNullOrEmpty(qL_m01US.usphone))
                ModelState.AddModelError("usphone", "Please Fill Phone");
            //if (string.IsNullOrEmpty(qL_m01US.usaddress))
            //    ModelState.AddModelError("usaddress", "Please Fill Address");
            if (string.IsNullOrEmpty(qL_m01US.usimgfile))
                qL_m01US.usimgfile = "~/Images/";
                //ModelState.AddModelError("usimgfile", "Please Upload Foto");
            if (string.IsNullOrEmpty(qL_m01US.ussignfile))
                qL_m01US.ussignfile = "~/Images/";
            if (string.IsNullOrEmpty(qL_m01US.usphone2))
                qL_m01US.usphone2 = "";
            //ModelState.AddModelError("usphone", "Please Upload Sign");

            try
            {
                qL_m01US.tgllahir = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("tgllahir", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }
            try
            {
                qL_m01US.tglmasuk = DateTime.Parse(ClassFunction.toDate(tglmsk));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("tglmasuk", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }
            try
            {
                qL_m01US.tglkeluar = DateTime.Parse(ClassFunction.toDate(tglkluar));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("tglkeluar", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }

            var servertime = ClassFunction.GetServerTime();

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(qL_m01US.usimgfile)))
                        {
                            var sExt = Path.GetExtension(qL_m01US.usimgfile);
                            var sdir = Server.MapPath(userimgpath);
                            var sfilename = qL_m01US.usoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_m01US.usimgfile.ToLower() != (userimgpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_m01US.usimgfile), sdir + "/" + sfilename);
                            }
                            qL_m01US.usimgfile = userimgpath + "/" + sfilename;
                        }
                        if (System.IO.File.Exists(Server.MapPath(qL_m01US.ussignfile)))
                        {
                            var sExt = Path.GetExtension(qL_m01US.ussignfile);
                            var sdir = Server.MapPath(usersignpath);
                            var sfilename = qL_m01US.usoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_m01US.ussignfile.ToLower() != (usersignpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_m01US.ussignfile), sdir + "/" + sfilename);
                            }
                            qL_m01US.ussignfile = usersignpath + "/" + sfilename;
                        }

                        if (action == "Create")
                        {
                            //Insert
                            qL_m01US.crttime = servertime;
                            qL_m01US.crtuser = Session["UserID"].ToString();
                            qL_m01US.updtime = servertime;
                            qL_m01US.upduser = Session["UserID"].ToString();
                            db.QL_m01US.Add(qL_m01US);
                            db.SaveChanges();
                            
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            qL_m01US.updtime = servertime;
                            qL_m01US.upduser = Session["UserID"].ToString();
                            db.Entry(qL_m01US).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;

            setViewBag(qL_m01US);
            return View(qL_m01US);
        }

        // GET: User/Form/5
        public ActionResult EditForm(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            //if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
            //    return RedirectToAction("NotAuthorize", "Account");

            QL_m01US qL_m01US;
            string action = "Create";
            if (id == null)
            {
                qL_m01US = new QL_m01US();
                qL_m01US.cmpcode = Session["CompnyCode"].ToString();
                qL_m01US.crtuser = Session["UserID"].ToString();
                qL_m01US.crttime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                qL_m01US = db.QL_m01US.Find(Session["CompnyCode"].ToString(), id);
            }

            if (qL_m01US == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(qL_m01US);
            return View(qL_m01US);
        }

        // POST: User/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditForm(QL_m01US qL_m01US, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            //if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
            //    return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (string.IsNullOrEmpty(qL_m01US.usoid))
                ModelState.AddModelError("usoid", "Please Fill User ID!!");
            else if (db.QL_m01US.Where(w => w.usoid == qL_m01US.usoid).Count() > 0 & action == "Create")
                ModelState.AddModelError("usoid", "This User ID already use, Please Fill Another User ID!!");
            if (string.IsNullOrEmpty(qL_m01US.usname))
                ModelState.AddModelError("usname", "Please Fill User Name");
            if (string.IsNullOrEmpty(qL_m01US.uspassword))
                ModelState.AddModelError("uspassword", "Please Fill Password");
            if (string.IsNullOrEmpty(qL_m01US.ustype))
                ModelState.AddModelError("ustype", "Please Fill User Type");
            if (string.IsNullOrEmpty(qL_m01US.usemail))
                ModelState.AddModelError("stoid", "Please Fill Email");
            //if (string.IsNullOrEmpty(qL_m01US.stoid))
            //    ModelState.AddModelError("usemail", "Please Fill Email");
            if (string.IsNullOrEmpty(qL_m01US.usphone))
                ModelState.AddModelError("usphone", "Please Fill Phone");
            //if (string.IsNullOrEmpty(qL_m01US.usaddress))
            //    ModelState.AddModelError("usaddress", "Please Fill Address");
            if (string.IsNullOrEmpty(qL_m01US.usimgfile))
                qL_m01US.usimgfile = "~/Images/";
            //ModelState.AddModelError("usimgfile", "Please Upload Foto");
            if (string.IsNullOrEmpty(qL_m01US.ussignfile))
                qL_m01US.ussignfile = "~/Images/";
            if (string.IsNullOrEmpty(qL_m01US.usphone2))
                qL_m01US.usphone2 = "";
            //ModelState.AddModelError("usphone", "Please Upload Sign");

            try
            {
                qL_m01US.tgllahir = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("tgllahir", "Format Tanggal Tidak Valid!!" + ex.ToString());
            }

            var servertime = ClassFunction.GetServerTime();

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Move File from temp to real Path
                        if (System.IO.File.Exists(Server.MapPath(qL_m01US.usimgfile)))
                        {
                            var sExt = Path.GetExtension(qL_m01US.usimgfile);
                            var sdir = Server.MapPath(userimgpath);
                            var sfilename = qL_m01US.usoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_m01US.usimgfile.ToLower() != (userimgpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_m01US.usimgfile), sdir + "/" + sfilename);
                            }
                            qL_m01US.usimgfile = userimgpath + "/" + sfilename;
                        }
                        if (System.IO.File.Exists(Server.MapPath(qL_m01US.ussignfile)))
                        {
                            var sExt = Path.GetExtension(qL_m01US.ussignfile);
                            var sdir = Server.MapPath(usersignpath);
                            var sfilename = qL_m01US.usoid + sExt;
                            if (!Directory.Exists(sdir))
                            {
                                DirectorySecurity securityRules = new DirectorySecurity();
                                securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                Directory.CreateDirectory(sdir, securityRules);
                            }
                            if (qL_m01US.ussignfile.ToLower() != (usersignpath + "/" + sfilename).ToLower())
                            {
                                System.IO.File.Delete(sdir + "/" + sfilename);
                                System.IO.File.Move(Server.MapPath(qL_m01US.ussignfile), sdir + "/" + sfilename);
                            }
                            qL_m01US.ussignfile = usersignpath + "/" + sfilename;
                        }

                        if (action == "Create")
                        {
                            //Insert
                            qL_m01US.crttime = servertime;
                            qL_m01US.crtuser = Session["UserID"].ToString();
                            qL_m01US.updtime = servertime;
                            qL_m01US.upduser = Session["UserID"].ToString();
                            db.QL_m01US.Add(qL_m01US);
                            db.SaveChanges();

                        }
                        else if (action == "Edit")
                        {
                            //Update
                            qL_m01US.updtime = servertime;
                            qL_m01US.upduser = Session["UserID"].ToString();
                            db.Entry(qL_m01US).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index", "Dashboard");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;

            setViewBag(qL_m01US);
            return View(qL_m01US);
        }

        private void setViewBag(QL_m01US qL_m01US)
        {
            ViewBag.stoid = new SelectList(db.QL_m07ST.Where(w => w.stflag == "ACTIVE" | w.stoid == qL_m01US.stoid).ToList(), "stoid", "stname", qL_m01US.stoid);

            sSql = "SELECT * FROM QL_m05gn WHERE cmpcode='" + qL_m01US.cmpcode + "' AND gngroup='JABATAN' AND gnflag='ACTIVE' ";
            var jabatanoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.Jabatanoid);
            ViewBag.jabatanoid = jabatanoid;

            sSql = "SELECT * FROM QL_mstdept WHERE cmpcode='" + qL_m01US.cmpcode + "' AND activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", qL_m01US.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + qL_m01US.cmpcode + "' AND activeflag='ACTIVE'";
            var deptgroup = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", qL_m01US.deptgroupoid);
            ViewBag.deptgroupoid = deptgroup;

            sSql = "select * from ql_m05gn where gngroup='AGAMA' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnflag='ACTIVE'";
            var agama = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.agamaoid);
            ViewBag.agamaoid = agama;

            sSql = "select * from ql_m05gn where gngroup='PROVINSI' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnflag='ACTIVE'";
            var provoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.provoid);
            ViewBag.provoid = provoid;

            if (qL_m01US.provoid > 0)
            {
                sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnother2=" + qL_m01US.provoid + " AND gnflag='ACTIVE'";
                var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.cityoid);
                ViewBag.cityoid = cityoid;
            }
            else
            {
                sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnother2=" + provoid.First().Value + " AND gnflag='ACTIVE'";
                var cityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.cityoid);
                ViewBag.cityoid = cityoid;
            }
    
            sSql = "select * from ql_m05gn where gngroup='PROVINSI' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnflag='ACTIVE'";
            var provdomoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.provoid);
            ViewBag.provdomoid = provdomoid;

            if (qL_m01US.provdomoid > 0)
            {
                sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnother2=" + qL_m01US.provdomoid + " AND gnflag='ACTIVE'";
                var citydomoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.citydomoid);
                ViewBag.citydomoid = citydomoid;
            }
            else
            {
                sSql = "select * from ql_m05gn where gngroup='KOTA' AND cmpcode='" + qL_m01US.cmpcode + "' AND gnother2=" + provdomoid.First().Value + " AND gnflag='ACTIVE'";
                var citydomoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", qL_m01US.citydomoid);
                ViewBag.citydomoid = citydomoid;
            }
        }

        [HttpPost]
        public ActionResult GetCity(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }
        public ActionResult GetCitydom(int provinceoid)
        {
            List<QL_m05GN> objcity = new List<QL_m05GN>();
            objcity = db.QL_m05GN.Where(g => g.gngroup == "KOTA" && g.gnflag.ToUpper() == "ACTIVE" && g.gnother2 == provinceoid.ToString()).ToList();
            SelectList obgcity = new SelectList(objcity, "gnoid", "gndesc", 0);
            return Json(obgcity);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (id.ToLower() == "admin")
            {
                result = "failed";
                msg += "this User Can't be Deleted";
            }
            else if(db.QL_m03UR.Where(w => w.usoid == id).Count() > 0)
            {
                result = "failed";
                msg += "this User already used in Another Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {

                        QL_m01US qL_m01US = db.QL_m01US.Find(Session["CompnyCode"].ToString(), id);

                        if (qL_m01US != null)
                        {
                            System.IO.File.Delete(Server.MapPath(qL_m01US.usimgfile));
                            System.IO.File.Delete(Server.MapPath(qL_m01US.ussignfile));
                        }
                        db.QL_m01US.Remove(qL_m01US);
                        db.SaveChanges();
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        //return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }
            
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public async Task<JsonResult> UploadFile()
        {
            var result = "";
            var idimgpath = "";
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file] as HttpPostedFileBase;
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        //var fileName = Path.GetFileName(fileContent.FileName);
                        var sfilename = Path.GetRandomFileName().Replace(".", "");
                        var sext = Path.GetExtension(fileContent.FileName);
                        var sdir = Server.MapPath(imgtemppath);
                        var path = Path.Combine(sdir, sfilename + sext);

                        idimgpath = imgtemppath + "/" + sfilename + sext;
                        if (!Directory.Exists(sdir))
                        {
                            DirectorySecurity securityRules = new DirectorySecurity();
                            securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                            Directory.CreateDirectory(sdir, securityRules);
                        }
                        using (var stream2 = new FileStream(path, FileMode.Create))
                        {
                            await stream.CopyToAsync(stream2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                result = "Upload failed" + ex.Message;
                return Json(result, idimgpath);
            }
            result = "Sukses";

            return Json(new { result, idimgpath }, JsonRequestBehavior.AllowGet);
        }
    }
}
