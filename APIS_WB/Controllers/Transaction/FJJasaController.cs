﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;

namespace APIS_WB.Controllers.Transaction
{
    public class FJJasaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public FJJasaController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listaritem
        {
            public int aritemmstoid { get; set; }
            public string aritemno { get; set; }
            public DateTime aritemdate { get; set; }
            public string custname { get; set; }
            public string rabno { get; set; }
            public string soitemno { get; set; }
            [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
            public decimal grandtotal { get; set; }
            public string aritemmststatus { get; set; }
        }

        public class listaritemdtl
        {
            public int aritemdtlseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public decimal aritemqty { get; set; }
            public int aritemunitoid { get; set; }
            public string aritemunit { get; set; }
            public decimal aritemprice { get; set; }
            public decimal aritemdtlamt { get; set; }
            public decimal aritemdtltaxvalue { get; set; }
            public decimal aritemdtltaxamt { get; set; }
            public decimal aritemdtlnetto { get; set; }
            public string aritemdtlnote { get; set; }
            public string shipmentitemno { get; set; }
            public int shipmentitemmstoid { get; set; }
            public int shipmentitemdtloid { get; set; }
            public int jasaacctgoid { get; set; }
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabdatestr { get; set; }
            public string projectname { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public int reqrabmstoid { get; set; }
            public string reqrabno { get; set; }
            public string custname { get; set; }
            public int custoid { get; set; }
            public string rabmstnote { get; set; }
            public int alamatoid { get; set; }
            public string alamat { get; set; }
            public string salesoid { get; set; }
            public int deptoid { get; set; }
            public string departement { get; set; }
            public int rabpaytermoid { get; set; }
            public int gnother1 { get; set; }
            public int curroid { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        public class shipmentitem
        {
            public int shipmentitemmstoid { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }
            public string shipmentitemdatestr { get; set; }
            public string shipmentitemmstnote { get; set; }
            public string soitemno { get; set; }
            public string soitemcustpo { get; set; }
        }

        public class listduedate
        {
            public DateTime duedate { get; set; }
            public string duedatestr { get; set; }
        }

        public class listfa
        {
            public int faoid { get; set; }
            public string fakturno { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listaritemdtl> dtDtl)
        {
            Session["QL_trnaritemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData()
        {
            if (Session["QL_trnaritemdtl"] == null)
            {
                Session["QL_trnaritemdtl"] = new List<listaritemdtl>();
            }

            List<listaritemdtl> dataDtl = (List<listaritemdtl>)Session["QL_trnaritemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FJJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(aritemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnaritemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND aritemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        [HttpPost]
        public ActionResult GetShipmentData(int soitemmstoid)
        {
            List<shipmentitem> tbl = new List<shipmentitem>();
            sSql = "SELECT DISTINCT sm.shipmentitemmstoid, shipmentitemno, shipmentitemdate,CONVERT(char(20),shipmentitemdate,103) shipmentitemdatestr, shipmentitemmstnote, soitemno, soitemcustpo FROM QL_trnshipmentitemmst sm INNER JOIN QL_trnshipmentitemdtl sd ON sd.cmpcode=sm.cmpcode AND sd.shipmentitemmstoid=sm.shipmentitemmstoid INNER JOIN QL_trnsoitemmst dom ON dom.cmpcode=sd.cmpcode AND dom.soitemmstoid=sm.soitemmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.soitemmstoid = " + soitemmstoid + " AND shipmentitemmststatus='Post' AND ISNULL(shipmentitemmstres1,'')<>'Closed' ORDER BY shipmentitemdate DESC, sm.shipmentitemmstoid DESC";

            tbl = db.Database.SqlQuery<shipmentitem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnaritemmst tbl, string action)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS integer)";
            var aritempaytypeoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.aritempaytypeoid);
            ViewBag.aritempaytypeoid = aritempaytypeoid;

            sSql = "SELECT * FROM QL_mstcurr WHERE activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;
            string sPlus = "";
            if (action == "Create")
            {
                sPlus = " WHERE faoid NOT IN (SELECT faoid FROM QL_trnaritemmst) AND status = 'ACTIVE'";
            }
            sSql = "SELECT * FROM( select 0 faoid, 'NONE' fakturno UNION ALL select faoid, fakturno from QL_mstFA  " + sPlus + ") AS t ORDER BY t.faoid";
            var faoid = new SelectList(db.Database.SqlQuery<listfa>(sSql).ToList(), "faoid", "fakturno", tbl.faoid);
            ViewBag.faoid = faoid;
        }

        private void FillAdditionalField(QL_trnaritemmst tblmst)
        {
            ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("select (SELECT reqrabno FROM QL_trnreqrabmst where reqrabmstoid = rb.reqrabmstoid) from QL_trnrabmst rb where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.projectname = db.Database.SqlQuery<string>("select projectname from QL_trnrabmst where rabmstoid = " + tblmst.rabmstoid + "").FirstOrDefault();
            ViewBag.soitemno = db.Database.SqlQuery<string>("select soitemno from QL_trnsoitemmst rb where soitemmstoid = " + tblmst.somstoid + "").FirstOrDefault();
            ViewBag.salesoid = db.Database.SqlQuery<string>("SELECT (SELECT usname FROM QL_m01US a WHERE a.usoid = salesoid) FROM QL_trnsoitemmst where soitemmstoid = " + tblmst.somstoid + "").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.gnother1 = db.Database.SqlQuery<int>("SELECT CAST(gnother1 AS int) FROM QL_m05gn a WHERE gngroup ='PAYMENT TERM' AND a.gnoid ='" + tblmst.aritempaytypeoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFAData(string action)
        {
            var result = "sukses";
            var msg = "";
            string sPlus = "";
            List<listfa> tbl = new List<listfa>();
            if (action == "Create")
            {
                sPlus = " WHERE faoid NOT IN (SELECT faoid FROM QL_trnaritemmst) AND status = 'ACTIVE'";
            }
            sSql = "SELECT * FROM( select 0 faoid, 'NONE' fakturno UNION ALL select faoid, fakturno from QL_mstFA  " + sPlus + ") AS t ORDER BY t.faoid";
            tbl = db.Database.SqlQuery<listfa>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDueDate(int paytype, string tgldue)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select dateadd(D, " + paytype + ",'" + DateTime.Parse(ClassFunction.toDate(tgldue)) + "') duedate,CONVERT(char(20),dateadd(D, " + paytype + ",'" + DateTime.Parse(ClassFunction.toDate(tgldue)) + "'),103) duedatestr ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRABData()
        {
            List<listrab> tbl = new List<listrab>();

            sSql = "SELECT rm.rabmstoid, rm.rabpaytermoid, (SELECT CAST(gnother1 AS integer) FROM QL_m05GN where gnoid = rm.rabpaytermoid) gnother1, rm.rabno, rm.rabdate, CONVERT(CHAR(10),rm.rabdate,103) rabdatestr, soitemmstoid, soitemno, rm.reqrabmstoid, rm.projectname, rm.salesoid, c.custname, c.custoid, rm.rabmstnote, rm.alamatoid, c2.custdtl2addr alamat, rm.salesoid, rm.deptoid, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=rm.deptoid),'') departement, rm.curroid FROM QL_trnrabmst rm INNER JOIN QL_mstcust c ON c.custoid=rm.custoid INNER JOIN QL_mstcustdtl2 c2 ON c2.custdtl2oid = rm.alamatoid INNER JOIN QL_trnsoitemmst sm ON sm.rabmstoid = rm.rabmstoid WHERE rm.cmpcode = '" + CompnyCode + "' AND sm.soitemmstoid IN (SELECT soitemmstoid FROM QL_trnshipmentitemmst where shipmentitemmststatus = 'Post' AND ISNULL(shipmentitemmstres1,'')<>'Closed' AND shipmentitemtype IN ('Jasa')) /*rabmststatus='Approved'*/ ORDER BY rabdate DESC";
            tbl = db.Database.SqlQuery<listrab>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetailData(int shipmentitemmstoid)
        {
            List<listaritemdtl> tbl = new List<listaritemdtl>();
            sSql = "SELECT sod.soitemdtlseq aritemdtlseq, sm.shipmentitemno, rabdtloid, m.jasaoid AS itemoid, m.jasacode AS itemcode, m.jasadesc AS itemdesc, sd.refno, (shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.sretitemmstoid=sretd.sretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.shipmentitemdtloid=sd.shipmentitemdtloid AND sretitemmststatus<>'Rejected'), 0.0)) AS aritemqty, sod.soitemunitoid aritemunitoid, gn.gndesc aritemunit, sod.soitemprice aritemprice,sod.soitemdtltaxvalue aritemdtltaxvalue, '' aritemdtlnote, sd.shipmentitemmstoid, sd.shipmentitemdtloid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid INNER JOIN QL_mstjasa m ON m.jasaoid = sd.itemoid INNER JOIN QL_m05gn gn ON gnoid = shipmentitemunitoid INNER JOIN QL_trnsoitemmst som ON som.cmpcode = sm.cmpcode AND som.soitemmstoid = sm.soitemmstoid INNER JOIN QL_trnsoitemdtl sod ON sod.cmpcode = sd.cmpcode AND sod.soitemmstoid = sm.soitemmstoid AND sod.soitemdtloid = sd.soitemdtloid WHERE sd.cmpcode = '" + CompnyCode + "' AND sm.shipmentitemmstoid = " + shipmentitemmstoid + " AND shipmentitemdtlstatus = '' AND(shipmentitemqty - ISNULL((SELECT SUM(sretitemqty) FROM QL_trnsretitemdtl sretd INNER JOIN QL_trnsretitemmst sretm ON sretm.cmpcode = sretd.cmpcode AND sretm.sretitemmstoid = sretd.sretitemmstoid WHERE sretd.cmpcode = sd.cmpcode AND sretd.shipmentitemdtloid = sd.shipmentitemdtloid AND sretitemmststatus <> 'Rejected'), 0.0)) > 0 ORDER BY shipmentitemdtlseq";

            tbl = db.Database.SqlQuery<listaritemdtl>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listduedate> tbl = new List<listduedate>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listduedate>(sSql).ToList();
            //tblmst.aritemduedate = ClassFunction.GetServerTime().AddDays(7);
            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: aritem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            string sfilter = "";
            sSql = "Select pr.aritemmstoid, pr.aritemno, pr.aritemdate, c.custname, ISNULL((SELECT rm.rabno FROM QL_trnrabmst rm WHERE rm.rabmstoid=pr.rabmstoid),'') rabno, ISNULL((SELECT rm.soitemno FROM QL_trnsoitemmst rm WHERE rm.soitemmstoid=pr.somstoid),'') soitemno, pr.aritemgrandtotal grandtotal, pr.aritemmststatus FROM QL_trnaritemmst pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid WHERE pr.cmpcode='" + Session["CompnyCode"].ToString() + "' AND pr.aritemtype IN ('JASA') " + sfilter + "  ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND pr.aritemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND pr.aritemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND pr.aritemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY pr.aritemdate";
            List<listaritem> tblmst = db.Database.SqlQuery<listaritem>(sSql).ToList();
            return View(tblmst);
        }


        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnaritemmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnaritemmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.aritemdate = ClassFunction.GetServerTime();
                tblmst.aritemmststatus = "In Process";
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_trnaritemdtl"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnaritemmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT rd.aritemdtlseq, sm.shipmentitemno, i.jasaoid AS itemoid, i.jasacode AS itemcode, i.jasadesc AS itemdesc, (SELECT refno FROM QL_trnshipmentitemdtl where shipmentitemdtloid = rd.shipmentitemdtloid) refno, rd.aritemqty, rd.aritemunitoid, g.gndesc aritemunit, rd.aritemprice, rd.aritemdtlamt, rd.aritemdtltaxvalue, rd.aritemdtltaxamt, rd.aritemdtlnetto, rd.aritemdtlnote, rd.shipmentitemdtloid, rd.shipmentitemmstoid FROM QL_trnaritemdtl rd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = rd.shipmentitemmstoid INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.aritemunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.aritemmstoid=" + id + " ORDER BY rd.aritemdtlseq";
                Session["QL_trnaritemdtl"] = db.Database.SqlQuery<listaritemdtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst, action);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnaritemmst tblmst, string action, string tglmst, string tgldue)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            //is Input Valid
            if (tblmst.aritemmststatus == "Post")
            {
                tblmst.aritemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tblmst.aritemno = "";
            }
            try
            {
                tblmst.aritemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("aritemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            try
            {
                tblmst.aritemduedate = DateTime.Parse(ClassFunction.toDate(tgldue));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("aritemduedate", "Format Tanggal Jatuh Tempo Tidak Valid!!" + ex.ToString());
            }
            if (string.IsNullOrEmpty(tblmst.aritemmstnote))
                tblmst.aritemmstnote = "";
            if (string.IsNullOrEmpty(tblmst.aritemmstres1))
                tblmst.aritemmstres1 = "";
            if (string.IsNullOrEmpty(tblmst.aritemmstres2))
                tblmst.aritemmstres2 = "";
            if (string.IsNullOrEmpty(tblmst.aritemmstres3))
                tblmst.aritemmstres3 = "";
            if (string.IsNullOrEmpty(tblmst.aritemtype))
                tblmst.aritemtype = "Jasa";


            //is Input Detail Valid
            List<listaritemdtl> dtDtl = (List<listaritemdtl>)Session["QL_trnaritemdtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl != null)
                {
                    if (dtDtl.Count > 0)
                    {
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            if (string.IsNullOrEmpty(dtDtl[i].aritemdtlnote))
                                dtDtl[i].aritemdtlnote = "";
                            if (dtDtl[i].aritemqty == 0)
                            {
                                ModelState.AddModelError("", "Qty " + dtDtl[i].itemdesc + " Belum Diisi !!");
                            }
                            if (dtDtl[i].aritemprice == 0)
                            {
                                ModelState.AddModelError("", "Harga " + dtDtl[i].itemdesc + " Belum Diisi !!");
                            }
                            sSql = "SELECT acctgoid FROM QL_mstjasa j WHERE j.cmpcode='" + CompnyCode + "' AND j.jasaoid=" + dtDtl[i].itemoid + "";
                            dtDtl[i].jasaacctgoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                        }
                        if (tblmst.aritemmststatus == "Post")
                        {
                            if (string.IsNullOrEmpty(tblmst.aritemno))
                                ModelState.AddModelError("aritemno", "Silahkan isi No. Faktur!");
                            else if (db.QL_trnaritemmst.Where(w => w.aritemno == tblmst.aritemno & w.aritemmstoid != tblmst.aritemmstoid).Count() > 0)
                                ModelState.AddModelError("aritemno", "No. Faktur yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");
                        }
                    }
                }
            }

            if (tblmst.aritemmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_AR_JASA", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_AR_JASA"));
                if (!ClassFunction.IsInterfaceExists("VAR_JUAL_JASA", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_JUAL_JASA"));
                if (!ClassFunction.IsInterfaceExists("VAR_PPN_OUT", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PPN_OUT"));
                if (!ClassFunction.IsInterfaceExists("VAR_PENDAPATAN_JASA", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PENDAPATAN_JASA"));
            }            

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tblmst.aritemmststatus == "Post")
            {
                cRate.SetRateValue(tblmst.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            string fakturno = "";
            if (tblmst.faoid > 0)
            {
                sSql = "SELECT fakturno FROM QL_mstfa WHERE faoid=" + tblmst.faoid + "";
                fakturno = db.Database.SqlQuery<string>(sSql).FirstOrDefault();
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnaritemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnaritemdtl");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var conaroid = ClassFunction.GenerateID("QL_conar");


                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PENDAPATAN_JASA", CompnyCode));
                var iAcctgOidAR = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_AR_JASA", CompnyCode));
                var iAcctgOidJual = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_JUAL_JASA", CompnyCode));
                var iAcctgOidPPN = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PPN_OUT", CompnyCode));

                var glseq = 1;

                tblmst.periodacctg = ClassFunction.GetDateToPeriodAcctg(servertime);
                tblmst.rateoid = 1;
                tblmst.rate2oid = rate2oid;
                tblmst.aritemmstdiscamt = 0;
                tblmst.aritemmstdisctype = "";
                tblmst.aritemmstdiscvalue = 0;
                tblmst.aritemothercost = 0;
                tblmst.aritemratetoidr = "1";
                tblmst.aritemratetousd = "0";
                tblmst.aritemrate2toidr = rate2toidr;
                tblmst.aritemrate2tousd = "0";
                tblmst.aritemtaxtype = "TAX";
                tblmst.aritemtaxvalue = 10;
                tblmst.aritemtotaldisc = 0;
                tblmst.aritemtotaldiscdtl = 0;
                tblmst.aritemtype = "Jasa";

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tblmst.aritemmstoid = mstoid;
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.QL_trnaritemmst.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_trnaritemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            //update status fa
                            sSql = "Update QL_mstFA set status = 'INACTIVE' Where faoid = " + tblmst.faoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus = 'Post' WHERE shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl where aritemmstoid = " + tblmst.aritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus = '' WHERE shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl where aritemmstoid = " + tblmst.aritemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnaritemdtl.Where(a => a.aritemmstoid == tblmst.aritemmstoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_trnaritemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnaritemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnaritemdtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.aritemdtloid = dtloid++;
                            tbldtl.aritemmstoid = tblmst.aritemmstoid;
                            tbldtl.aritemdtlseq = i + 1;
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.aritemqty = dtDtl[i].aritemqty;
                            tbldtl.aritemprice = dtDtl[i].aritemprice;
                            tbldtl.aritemunitoid = dtDtl[i].aritemunitoid;
                            tbldtl.aritemdtlamt = dtDtl[i].aritemdtlamt;
                            tbldtl.aritemdtlnetto = dtDtl[i].aritemdtlnetto;
                            tbldtl.aritemdtlstatus = "";
                            tbldtl.aritemdtlnote = dtDtl[i].aritemdtlnote;
                            tbldtl.upduser = tblmst.upduser;
                            tbldtl.updtime = tblmst.updtime;
                            tbldtl.aritemdtldisctype = "";
                            tbldtl.aritemdtldiscamt = 0;
                            tbldtl.aritemdtldiscvalue = 0;
                            tbldtl.aritemdtlres1 = "";
                            tbldtl.aritemdtlres2 = "";
                            tbldtl.aritemdtlres3 = "";
                            tbldtl.shipmentitemmstoid = dtDtl[i].shipmentitemmstoid;
                            tbldtl.shipmentitemdtloid = dtDtl[i].shipmentitemdtloid;
                            tbldtl.aritemdtltaxvalue = dtDtl[i].aritemdtltaxvalue;
                            tbldtl.aritemdtltaxamt = dtDtl[i].aritemdtltaxamt;

                            db.QL_trnaritemdtl.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus = 'Complete' WHERE shipmentitemdtloid = " + dtDtl[i].shipmentitemdtloid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusjualjasa='FJ JASA PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid = " + tblmst.rabmstoid + "";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + dtDtl[i].shipmentitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemdtlstatus='' AND shipmentitemmstoid=" + dtDtl[i].shipmentitemmstoid + " AND shipmentitemdtloid<>" + dtDtl[i].shipmentitemdtloid + ")=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnrabmst SET statusjualjasa='FJ JASA' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid = " + tblmst.rabmstoid + " AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl md INNER JOIN QL_trnshipmentitemmst mm ON mm.shipmentitemmstoid = md.shipmentitemmstoid WHERE mm.cmpcode='" + CompnyCode + "' AND shipmentitemdtlstatus='' AND mm.soitemmstoid IN (SELECT soitemmstoid from ql_trnsoitemmst where rabmstoid = " + tblmst.rabmstoid + ") AND md.shipmentitemdtloid<>" + dtDtl[i].shipmentitemdtloid + ")=0";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        if (tblmst.aritemmststatus == "Post")
                        {
                            decimal argrandtotal = Convert.ToDecimal(tblmst.aritemgrandtotal);
                            decimal artotalamt = Convert.ToDecimal(tblmst.aritemtotalamt - tblmst.aritemtotaldiscdtl);
                            decimal artaxamt = Convert.ToDecimal(tblmst.aritemtaxamt);
                            var shipmentdtl = (from sd in db.QL_trnshipmentitemdtl join ard in db.QL_trnaritemdtl on new { cmpcode = sd.cmpcode, shipmentitemdtloid = sd.shipmentitemdtloid } equals new { cmpcode = ard.cmpcode, shipmentitemdtloid = ard.shipmentitemdtloid } where ard.cmpcode == CompnyCode && ard.aritemmstoid == tblmst.aritemmstoid select new { sd.shipmentitemvalueidr, sd.shipmentitemvalueusd, ard.aritemqty }).ToList();
                            decimal shipmentamtidr = shipmentdtl.Sum(x => x.shipmentitemvalueidr * x.aritemqty);

                            //Insert QL_conar
                            db.QL_conar.Add(ClassFunction.InsertConAR(CompnyCode, conaroid++, "QL_trnaritemmst", tblmst.aritemmstoid, 0, tblmst.custoid, iAcctgOidAR, "Post", "ARFG", tblmst.aritemdate, ClassFunction.GetDateToPeriodAcctg(tblmst.aritemdate), 0, DateTime.Parse("01/01/1900"), "", 0, DateTime.Parse("01/01/1900"), argrandtotal, 0, "", "", "", "", Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, argrandtotal * cRate.GetRateMonthlyIDRValue, 0, 0, 0, 0, ""));

                            var BiayaAmt = dtDtl.Sum(x => (x.aritemqty * x.aritemprice));
                            //// Insert QL_trnglmst
                            //db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jasa|No. " + tblmst.aritemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            //db.SaveChanges();

                            glseq = 1;

                            //// Insert QL_trngldtl
                            //// D : pendapatan lain Penjualan
                            //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "D", BiayaAmt, tblmst.aritemno, "Faktur Jasa|No. " + tblmst.aritemno, "Post", Session["UserID"].ToString(), servertime, BiayaAmt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                            //db.SaveChanges();

                            //for (int i = 0; i < dtDtl.Count(); i++)
                            //{
                            //    // Insert QL_trngldtl
                            //    // D : Biaya Lain - Lain
                            //    var glamt = (dtDtl[i].aritemqty * dtDtl[i].aritemprice);
                            //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].jasaacctgoid, "C", glamt, tblmst.aritemno, "Faktur Jasa|No. " + tblmst.aritemno, "Post", Session["UserID"].ToString(), servertime, glamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                            //    db.SaveChanges();
                            //}
                            
                            if (argrandtotal > 0)
                            {
                                //glmstoid += 1;
                                // Insert QL_trnglmst
                                db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Faktur Jasa|No. " + tblmst.aritemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                                db.SaveChanges();

                                glseq = 1;

                                // D : Piutang Dagang
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidAR, "D", tblmst.aritemgrandtotal, tblmst.aritemno, "Faktur Jasa|No. " + tblmst.aritemno, "Post", Session["UserID"].ToString(), servertime, tblmst.aritemgrandtotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                db.SaveChanges();

                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    // Insert QL_trngldtl
                                    // D : Biaya Lain - Lain
                                    var glamt = (dtDtl[i].aritemqty * dtDtl[i].aritemprice);
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].jasaacctgoid, "C", glamt, tblmst.aritemno, "Faktur Jasa|No. " + tblmst.aritemno, "Post", Session["UserID"].ToString(), servertime, glamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                    db.SaveChanges();
                                }
                                //// C : Penjualan Jasa
                                //db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidJual, "C", artotalamt, tblmst.aritemno, "Faktur Jasa|No. " + tblmst.aritemno, "Post", Session["UserID"].ToString(), servertime, artotalamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0));
                                //db.SaveChanges();

                                if (artaxamt > 0)
                                {
                                    // C : PPN KELUARAN
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidPPN, "C", artaxamt, tblmst.aritemno, "Faktur Jasa|No. " + tblmst.aritemno, "Post", Session["UserID"].ToString(), servertime, artaxamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnaritemmst " + tblmst.aritemmstoid, null, null, null, 0, fakturno));
                                    db.SaveChanges();
                                }
                            }


                            sSql = "UPDATE QL_ID SET lastoid=" + (conaroid - 1) + " WHERE tablename='QL_conar'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }



                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_trnaritemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        tblmst.aritemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tblmst.aritemmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst, action);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnaritemmst tblmst = db.QL_trnaritemmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnshipmentitemmst SET shipmentitemmststatus = 'Post' WHERE shipmentitemmstoid IN (SELECT shipmentitemmstoid FROM QL_trnaritemdtl where aritemmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnshipmentitemdtl SET shipmentitemdtlstatus = '' WHERE shipmentitemdtloid IN (SELECT shipmentitemdtloid FROM QL_trnaritemdtl where aritemmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='FJ PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnaritemmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.aritemmstoid=" + id + " )";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnaritemmst pm WHERE pm.cmpcode='" + CompnyCode + "' AND pm.aritemmstoid=" + id + ") AND (SELECT COUNT(*) FROM QL_trnshipmentitemdtl md INNER JOIN QL_trnshipmentitemmst mm ON mm.shipmentitemmstoid = md.shipmentitemmstoid INNER JOIN QL_trnsoitemmst pom ON pom.soitemmstoid = mm.soitemmstoid WHERE mm.cmpcode='" + CompnyCode + "' AND shipmentitemdtlstatus='' AND pom.rabmstoid IN (SELECT rabmstoid from QL_trnaritemmst where aritemmstoid <> " + id + "))=0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_mstFA SET status = 'ACTIVE' WHERE faoid IN (SELECT faoid FROM QL_trnaritemmst where aritemmstoid = " + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();


                        var trndtl = db.QL_trnaritemdtl.Where(a => a.aritemmstoid == id);
                        db.QL_trnaritemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnaritemmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        public ActionResult PrintReport(int id, string sRekening, string sBank, string sAtasNama)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var rabtype = "";
            sSql = "SELECT ISNULL((SELECT rm.rabtype2 FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid WHERE arm.aritemmstoid=" + id + "),'') dt";
            rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            if (rabtype != "RETAIL")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARJasaTrn2.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptARJasaTrn.rpt"));
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");
            report.SetParameterValue("sRekening", sRekening);
            report.SetParameterValue("sBank", sBank);
            report.SetParameterValue("sAtasNama", sAtasNama);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "FJJasaReport_" + sNo + ".pdf");
        }

        public ActionResult PrintReportSPP(int id, string sRekening, string sBank, string sAtasNama)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSPPJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");
            report.SetParameterValue("sRekening", sRekening);
            report.SetParameterValue("sBank", sBank);
            report.SetParameterValue("sAtasNama", sAtasNama);

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SPPJasa_" + sNo + ".pdf");
        }

        public ActionResult PrintReportKWT(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var rabtype = "";
            sSql = "SELECT ISNULL((SELECT rm.rabtype2 FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid WHERE arm.aritemmstoid=" + id + "),'') dt";
            rabtype = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            if (rabtype != "RETAIL")
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKWTJasa2.rpt"));
            }
            else
            {
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rptKWTJasa.rpt"));
            }

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "KWTJasa_" + sNo + ".pdf");
        }

        public ActionResult PrintReportBASTB(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptBASTBJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "BASTBJasa_" + sNo + ".pdf");
        }

        public ActionResult PrintReportSKPBN(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT ISNULL((SELECT arm.aritemno FROM QL_trnaritemmst arm WHERE arm.aritemmstoid=" + id + "),'') dt";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSKPBNJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE arm.cmpcode='" + CompnyCode + "' AND arm.aritemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "SKPBNJasa_" + sNo + ".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}