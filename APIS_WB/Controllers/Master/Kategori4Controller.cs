﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class Kategori4Controller : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public Kategori4Controller()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ListCat4
        {
            public string cat4code { get; set; }
            public string cat3code { get; set; }
            public string cat2code { get; set; }
            public string cat1code { get; set; }
            public string cat4shortdesc { get; set; }
            public string cat4note { get; set; }
            public string activeflag { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat3shortdesc { get; set; }

        }

        [HttpPost]
        public ActionResult Getcat2(string cat1)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat2> tbl = new List<QL_mstcat2>();
            sSql = "SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE' AND cat1code='" + cat1 + "' ORDER BY cat2code";
            tbl = db.Database.SqlQuery<QL_mstcat2>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Getcat3(string cat1, string cat2)
        {
            var result = "sukses";
            var msg = "";
            List<QL_mstcat3> tbl = new List<QL_mstcat3>();
            sSql = "SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE' AND cat1code='" + cat1 + "' AND cat2code='" + cat2 + "' ORDER BY cat3code";
            tbl = db.Database.SqlQuery<QL_mstcat3>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void setViewBag(QL_mstcat4 qL_mstcat4, string action)
        {
            string sPlus = ""; string sPlus2 = "";
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>("SELECT * FROM QL_mstcat1 WHERE activeflag='ACTIVE'").ToList(), "cat1code", "cat1shortdesc", qL_mstcat4.cat1code);
            ViewBag.cat1code = cat1code;
            if (action == "Edit")
            {
                sPlus = " AND cat1code='"+ qL_mstcat4.cat1code + "'";
            }
            var cat2code = new SelectList(db.Database.SqlQuery<QL_mstcat2>("SELECT * FROM QL_mstcat2 WHERE activeflag='ACTIVE' "+ sPlus +"").ToList(), "cat2code", "cat2shortdesc", qL_mstcat4.cat2code);
            ViewBag.cat2code = cat2code;
            if (action == "Edit")
            {
                sPlus2 = " AND cat1code='" + qL_mstcat4.cat1code + "' AND cat2code='" + qL_mstcat4.cat2code + "'";
            }
            var cat3code = new SelectList(db.Database.SqlQuery<QL_mstcat3>("SELECT * FROM QL_mstcat3 WHERE activeflag='ACTIVE' "+ sPlus2 +"").ToList(), "cat3code", "cat3shortdesc", qL_mstcat4.cat3code);
            ViewBag.cat3code = cat3code;
        }

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND c4.activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT c4.*, c1.cat1shortdesc, c2.cat2shortdesc, c3.cat3shortdesc FROM QL_mstcat4 c4 Inner Join QL_mstcat3 c3 on c4.cat3code=c3.cat3code and c3.cat2code = c4.cat2code and c3.cat1code = c4.cat1code inner join ql_mstcat2 c2 on c4.cat2code=c2.cat2code and c4.cat1code = c2.cat1code inner join ql_mstcat1 c1 on c4.cat1code=c1.cat1code WHERE c4.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and c4.cat4code<>'0' ORDER BY cat4shortdesc";
            List<ListCat4> vbag = db.Database.SqlQuery<ListCat4>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(string c4, string c3, string c2, string c1)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat4 tbl;
            string action = "Create";
            if (c4 == null)
            {
                tbl = new QL_mstcat4();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tbl = db.QL_mstcat4.Find(c4, c3, c2, c1);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tbl, action);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat4 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (tbl.cat1code == "")
                ModelState.AddModelError("cat1code", "Kategori 1 Tidak Boleh Kosong !!!");
            if (tbl.cat2code == "")
                ModelState.AddModelError("cat2code", "Kategori 2 Tidak Boleh Kosong !!!");
            if (tbl.cat3code == "")
                ModelState.AddModelError("cat3code", "Kategori 3 Tidak Boleh Kosong !!!");
            if (string.IsNullOrEmpty(tbl.cat4code))
                ModelState.AddModelError("cat4code", "Silahkan isi Kode!");
            else
            {
                if (action == "Create")
                {
                    if (db.QL_mstcat4.Where(w => w.cat4code == tbl.cat4code && w.cat3code == tbl.cat3code && w.cat2code == tbl.cat2code && w.cat1code == tbl.cat1code).Count() > 0)
                        ModelState.AddModelError("cat4code", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
                }
            }
            if (string.IsNullOrEmpty(tbl.cat4shortdesc))
                ModelState.AddModelError("cat4shortdesc", "Silahkan isi Deskripsi!");
            else if (db.QL_mstcat4.Where(w => w.cat4shortdesc == tbl.cat4shortdesc && w.cat4code != tbl.cat4code && w.cat3code == tbl.cat3code && w.cat2code == tbl.cat2code && w.cat1code == tbl.cat1code).Count() > 0)
                ModelState.AddModelError("cat4shortdesc", "Deskripsi yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Deskripsi lainnya!");
            if (string.IsNullOrEmpty(tbl.cat4note))
                tbl.cat4note = "";
            if (tbl.cat4code.Length !=2)
                ModelState.AddModelError("cat4code", "Kode Maximum 2 Karakter!");

            tbl.cat4longdesc = tbl.cat4shortdesc;

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcat4");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcat4.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Terjadi Error, data tidak jadi disimpan. Keterangan Error: " + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tbl, action);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string c4, string c3, string c2, string c1)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat4 tbl = db.QL_mstcat4.Find(c4, c3, c2, c1);
                        db.QL_mstcat4.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
