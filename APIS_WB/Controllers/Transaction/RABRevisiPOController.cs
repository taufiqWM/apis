﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class RABRevisiPOController : Controller
    {
        // GET: RABRevisiPO
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public RABRevisiPOController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listrab
        {
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public DateTime rabdate { get; set; }
            public string rabtype { get; set; }
            public string projectno { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string rabmststatus { get; set; }
            public string reason { get; set; }
        }

        public class listrabdtl
        {
            public int rabseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public decimal rabqty { get; set; }
            public int rabunitoid { get; set; }
            public string rabunit { get; set; }
            public decimal rabprice { get; set; }
            public decimal rabdtlamt { get; set; }
            public decimal rabtaxvalue { get; set; }
            public decimal rabtaxamt { get; set; }
            public decimal rabpphvalue { get; set; }
            public decimal rabpphamt { get; set; }
            public decimal rabdtlnetto { get; set; }
            public string rabdtlstatus { get; set; }
            public string rabdtlnote { get; set; }
            public decimal rabongkiramt { get; set; }
            public DateTime rabetd { get; set; }
            public string rabetdstr { get; set; }
            public int rablocoid { get; set; }
        }

        public class listrabdtl2
        {
            public int rabdtl2seq { get; set; }
            public int rabdtl2oid { get; set; }
            public int itemdtl2oid { get; set; }
            public string itemcode { get; set; }
            public int suppdtl2oid { get; set; }
            public string suppdtl2name { get; set; }
            public string itemdesc { get; set; }
            public decimal rabdtl2qty { get; set; }
            public int rabdtl2unitoid { get; set; }
            public string rabdtl2unit { get; set; }
            public decimal rabdtl2price { get; set; }
            public decimal rabdtl2discvalue { get; set; }
            public decimal rabdtl2discamt { get; set; }
            public decimal rabdtl2amt { get; set; }
            public decimal rabdtl2taxvalue { get; set; }
            public decimal rabdtl2taxamt { get; set; }
            public decimal rabdtl2netto { get; set; }
            public string rabdtl2status { get; set; }
            public string rabdtl2note { get; set; }
            public decimal rabdtl2ongkiramt { get; set; }
            public DateTime rabdtl2eta { get; set; }
            public string rabdtl2etastr { get; set; }
            public int rabdtl2locoid { get; set; }
        }

        public class listrabdtl3
        {
            public int rabdtl3seq { get; set; }
            public int itemdtl3oid { get; set; }
            public string jasacode { get; set; }
            public string jasadesc { get; set; }
            public decimal rabdtl3qty { get; set; }
            public int rabdtl3unitoid { get; set; }
            public string rabdtl3unit { get; set; }
            public decimal rabdtl3price { get; set; }
            public decimal rabdtl3amt { get; set; }
            public decimal rabdtl3taxvalue { get; set; }
            public decimal rabdtl3taxamt { get; set; }
            public decimal rabdtl3pphvalue { get; set; }
            public decimal rabdtl3pphamt { get; set; }
            public decimal rabdtl3netto { get; set; }
            public string rabdtl3status { get; set; }
            public string rabdtl3note { get; set; }
            public DateTime rabdtl3etd { get; set; }
            public string rabdtl3etdstr { get; set; }
            public int rabdtl3loc { get; set; }
        }

        public class listrabdtl4
        {
            public int rabdtl4seq { get; set; }
            public int itemdtl4oid { get; set; }
            public string jasacode { get; set; }
            public int suppdtl4oid { get; set; }
            public string suppdtl4name { get; set; }
            public string jasadesc { get; set; }
            public decimal rabdtl4qty { get; set; }
            public int rabdtl4unitoid { get; set; }
            public string rabdtl4unit { get; set; }
            public decimal rabdtl4price { get; set; }
            public decimal rabdtl4amt { get; set; }
            public decimal rabdtl4taxvalue { get; set; }
            public decimal rabdtl4taxamt { get; set; }
            public decimal rabdtl4netto { get; set; }
            public string rabdtl4status { get; set; }
            public string rabdtl4note { get; set; }
            public DateTime rabdtl4eta { get; set; }
            public string rabdtl4etastr { get; set; }
        }

        public class listrabdtl5
        {
            public int rabdtl5seq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
            public decimal rabdtl5price { get; set; }
            public string rabdtl5note { get; set; }
        }

        public class listprab
        {
            public int reqrabmstoid { get; set; }
            public string reqrabno { get; set; }
            public DateTime reqrabdate { get; set; }
            public string projectname { get; set; }
            public string reqrabtype { get; set; }
            public string reqrabtype2 { get; set; }
            public string reqrabestdate { get; set; }
            public string custname { get; set; }
            public int custoid { get; set; }
            public string custemail { get; set; }
            public int custpaymentoid { get; set; }
            public int reqrabprovoid { get; set; }
            public int reqrabcityoid { get; set; }
            public string salesoid { get; set; }
            public string note { get; set; }
        }


        public class listmrservice
        {
            public int mrservicemstoid { get; set; }
            public string mrserviceno { get; set; }
            public DateTime mrservicedate { get; set; }
            public string itemdesc { get; set; }
            public string custname { get; set; }
            public string mrservicemstnote { get; set; }
        }

        public class listsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
            public int deptoid { get; set; }
            public string departemen { get; set; }
            public string alamat { get; set; }
        }

        public class listmat
        {
            public int itemseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
        }

        public class listjasa
        {
            public int jasaseq { get; set; }
            public int jasaoid { get; set; }
            public string jasacode { get; set; }
            public string jasadesc { get; set; }
            public int jasaunitoid { get; set; }
            public string jasaunit { get; set; }
        }

        public class listalamat
        {
            public int alamatoid { get; set; }
            public string lokasi { get; set; }
            public string alamat { get; set; }
            public int cityoid { get; set; }
            public string kota { get; set; }
            public int provoid { get; set; }
            public string provinsi { get; set; }
        }

        public class acctg
        {
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }

        public class listDDLsales
        {
            public string salesoid { get; set; }
            public string salesname { get; set; }
        }

        public class listsoitem
        {
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public string soitemdate { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public string custphone1 { get; set; }
            public string custemail { get; set; }
            public decimal soitemtotalamt { get; set; }
            public decimal soitemtotaltaxamt { get; set; }
            public decimal soitemtotalnetto { get; set; }
        }

        public class listsoitemdtl
        {
            public int soitemdtlseq { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public decimal soitemqty { get; set; }
            public decimal soitemprice { get; set; }
            public decimal soitemdtlamt { get; set; }
            public decimal soitemdtltaxamt { get; set; }
            public decimal soitemdtlpphamt { get; set; }
            public decimal soitemdtlnetto { get; set; }
            public string soitemunit { get; set; }
            public string eta { get; set; }
        }

        private string mailbodyso(int id)
        {
            string mailbody = "";

            sSql = "Select sm.soitemno, convert(varchar(10),sm.soitemdate,103) soitemdate, c.custname, c.custaddr, c.custphone1, c.custemail, sm.soitemtotalamt, sm.soitemtotaltaxamt, sm.soitemtotalnetto from ql_trnsoitemmst sm inner join ql_mstcust c on sm.custoid=c.custoid where sm.soitemmstoid=" + id + "";
            List<listsoitem> somst = db.Database.SqlQuery<listsoitem>(sSql).ToList();
            sSql = "SELECT rd.soitemdtlseq, i.itemcode, i.itemdesc, rd.soitemqty, rd.soitemprice, rd.soitemdtlamt, rd.soitemdtltaxamt, rd.soitemdtlpphamt, rd.soitemdtlnetto, convert(varchar(10),soitemdtletd,103) as eta FROM QL_trnsoitemdtl, g.gndesc soitemunit rd INNER JOIN QL_mstitem i ON i.itemoid = rd.itemoid INNER JOIN QL_m05gn g ON g.gnoid = i.itemunitoid WHERE rd.soitemmstoid = " + id + "";
            List<listsoitemdtl> sodtl = db.Database.SqlQuery<listsoitemdtl>(sSql).ToList();

            if (somst.Count > 0)
            {
                //mailbody = "<html><head></head><body>";
                mailbody = "<div align=\"center\"><div style = \"margin: 0px auto; max-width: 600px;\"><h6 style = \"font-size: 20px; text-align: center;\"><img src = \"http://www.budi-jaya.com/assets/Uploads/for-website.png\" width = \"80px\" /><br> Anugrah Pratama</h6><div style = \"padding: 30px; border: 1px solid #ededed; max-width: 420px; margin-top: 10px; font-family: Helvetica, Arial, sans-serif; background-color: #ffffff; overflow-x: auto; text-align: left;\"><h5 style = \"font-family: Helvetica, 'Arial', sans-serif; -webkit-text-size-adjust: none; color: #891c2e; font-size: 20px; line-height: 24px; font-weight: bold; text-align: center;\"> Sales Order </h5> ";
                mailbody += "<span style=\"font - size: 8pt; color: #000;\"><strong>No. :" + somst[0].soitemno + "<br>Tanggal : " + somst[0].soitemdate + "<br></strong></span><br><span style=\"color: #000; font-size: 8pt;\">Nama Konsumen : " + somst[0].custname + " </span><br><span style=\"font-size: 8pt; color: #000;\">No. Telp.&nbsp;: " + somst[0].custphone1 + "</span><br><span style=\"font-size: 8pt; color: #000;\">Alamat : " + somst[0].custaddr + "</span><br><br><br>";
                mailbody += "<table style=\"width: 100 %; text - size - adjust: none; font - weight: normal; font - size: 8px; color: #000000; line-height: 20px; border-collapse: collapse; border: 1px solid #ededed;\"><thead style = \"line-height: 25px; font-weight: bold;\"> ";
                mailbody += "<tr><td style =\"border: 1px solid #ededed; width: 5%; max-width: 25px; text-align: center;\"><span style=\"font-size: 8pt;\">No.</span></td><td style =\"border: 1px solid #ededed; width: 27.0286%; max-width: 125px; text-align: center;\"><span style=\"font-size: 8pt;\">Item</span></td><td style =\"border: 1px solid #ededed; width: 10%; text-align: center;\"><span style=\"font-size: 8pt;\">QTY</span></td><td style =\"border: 1px solid #ededed; width: 5%; max-width: 25px; text-align: center;\"><span style=\"font-size: 8pt;\">Unit</span></td><td style =\"border: 1px solid #ededed; width: 12%; max-width: 50px; text-align: center;\"><span style=\"font-size: 8pt;\">Harga</span></td><td style =\"border: 1px solid #ededed; width: 12%; max-width: 50px; text-align: center;\"><span style=\"font-size: 8pt;\">Total</span></td><td style =\"border: 1px solid #ededed; width: 15%; max-width: 50px; text-align: center;\"><span style=\"font-size: 8pt;\">ETD</span></td></tr></thead><tbody>";
                if (sodtl != null)
                {
                    if (sodtl.Count > 0)
                    {
                        for (int i = 0; i < sodtl.Count; i++)
                        {
                            mailbody += "<tr><td style =\"border: 1px solid #ededed; width: 5%; max-width: 25px; text-align: center;\"><span style=\"font-size: 7pt;\">" + (i + 1) + "</span></td><td style=\"border: 1px solid #ededed; width: 27.0286%; max-width: 125px; text-align: left;\"><span style=\"font-size: 7pt;\">" + sodtl[i].itemdesc + "</span></td><td style=\"border: 1px solid #ededed; width: 10%; max-width: 25px; text-align: center;\"><span style=\"font-size: 7pt;\">" + sodtl[i].soitemqty + "</span></td><td style=\"border: 1px solid #ededed; width: 5%; max-width: 25px; text-align: center;\"><span style=\"font-size: 7pt;\">" + sodtl[i].soitemunit + "</span></td><td style=\"border: 1px solid #ededed; width: 12%; max-width: 50px; text-align: right;\"><span style=\"font-size: 7pt;\">" + sodtl[i].soitemprice + "</span></td><td style=\"border: 1px solid #ededed; width: 12%; max-width: 50px; text-align: right;\"><span style=\"font-size: 7pt;\">" + sodtl[i].soitemdtlnetto + "</span></td><td style=\"border: 1px solid #ededed; width: 15%; max-width: 50px; text-align: center;\"><span style=\"font-size: 7pt;\">" + sodtl[i].eta + "</span></td></tr>";
                            //mailbody += "<tr><td>" + (i + 1) + ".</td><td> " + sodtl[i].itemdesc + "</td> <td> " + sodtl[i].soitemqty + "</td><td>" + sodtl[i].soitemunit + "</td><td> " + sodtl[i].soitemprice + "</td> <td> " + sodtl[i].soitemdtlnetto + "</td><td>" + sodtl[i].eta + "</td><tr>";
                        }
                    }
                }
                mailbody += "<tr style=\"text-align: right; font-weight: bold;\"><td style=\"border: 1px solid #ededed; width: 71.0286%;\" colspan=\"6\"><span style=\"font-size: 7pt;\">Sub Total</span></td><td style=\"border: 1px solid #ededed; width: 15%;\" colspan=\"1\"><span style=\"font-size: 7pt;\">" + somst[0].soitemtotalamt + "</span></td></tr><tr style =\"text-align: right; font-weight: bold;\"><td style=\"border: 1px solid #ededed; width: 71.0286%;\" colspan=\"6\"><span style=\"font-size: 7pt;\">PPN</span></td><td style=\"border: 1px solid #ededed; width: 15%;\" colspan=\"1\"><span style=\"font-size: 7pt;\">" + somst[0].soitemtotaltaxamt + "</span></td></tr><tr style =\"text-align: right; font-weight: bold;\"><td style=\"border: 1px solid #ededed; width: 71.0286%;\" colspan=\"6\"><span style=\"font-size: 7pt;\">Grand Total</span></td><td style=\"border: 1px solid #ededed; width: 15%;\" colspan=\"1\"><span style=\"font-size: 7pt;\">" + somst[0].soitemtotalnetto + "</span></td></tr></tbody></table></div> ";
                mailbody += "<div style=\"padding-left: 0px; max-width: 420px; margin-top: 25px; font-family: 'Helvetica', Arial, sans-serif; text-align: center;\"><p style=\"font-size: 11px; padding: 15px 0px; color: #000;\"><a href=\"http://localhost:23487/WaitingAction2/Form/" + id + "?tblname=QL_trnsoitemmst\" target = \"_blank\"> Please click this link to approve this Sales Order</a></p><span style=\"font-size: 8pt;\"><br><br><br></span></div><div style=\"max-width: 600px; padding: 10px; text-align: center; display: block; background-color: #891c2e; color: #fff !important; margin-top: 50px; font-size: 11px;\"><span style=\"font-family: helvetica, arial, sans-serif;\">PT. Budi Jaya Amenities<br><span style=\"color: #fff; text-decoration: none;\">Jl. Raya Trosobo No.19B | E-mail : office@budi-jaya.com</span><br><span>Telp. 031 7873888 | Fax : 031 7871999</span></span></div></div></div>";
                //mailbody += "<a href=\"http://localhost:23487/WaitingAction2/Form/" + id + "?tblname=QL_trnsoitemmst\" target = \"_blank\" > Please click this link to approve this Sales Order</a>";
                //mailbody += "</body></html>";
            }

            return mailbody;
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listrabdtl> dtDtl)
        {
            Session["QL_trnrabdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData()
        {
            if (Session["QL_trnrabdtl"] == null)
            {
                Session["QL_trnrabdtl"] = new List<listrabdtl>();
            }

            List<listrabdtl> dataDtl = (List<listrabdtl>)Session["QL_trnrabdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails2(List<listrabdtl2> dtDtl)
        {
            Session["QL_trnrabdtl2"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData2()
        {
            if (Session["QL_trnrabdtl2"] == null)
            {
                Session["QL_trnrabdtl2"] = new List<listrabdtl2>();
            }

            List<listrabdtl2> dataDtl = (List<listrabdtl2>)Session["QL_trnrabdtl2"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails3(List<listrabdtl3> dtDtl)
        {
            Session["QL_trnrabdtl3"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData3()
        {
            if (Session["QL_trnrabdtl3"] == null)
            {
                Session["QL_trnrabdtl3"] = new List<listrabdtl3>();
            }

            List<listrabdtl3> dataDtl = (List<listrabdtl3>)Session["QL_trnrabdtl3"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails4(List<listrabdtl4> dtDtl)
        {
            Session["QL_trnrabdtl4"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData4()
        {
            if (Session["QL_trnrabdtl4"] == null)
            {
                Session["QL_trnrabdtl4"] = new List<listrabdtl4>();
            }

            List<listrabdtl4> dataDtl = (List<listrabdtl4>)Session["QL_trnrabdtl4"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails5(List<listrabdtl5> dtDtl)
        {
            Session["QL_trnrabdtl5"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData5()
        {
            if (Session["QL_trnrabdtl5"] == null)
            {
                Session["QL_trnrabdtl5"] = new List<listrabdtl5>();
            }

            List<listrabdtl5> dataDtl = (List<listrabdtl5>)Session["QL_trnrabdtl5"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "RAB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(rabno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnrabmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND rabno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        private void InitDDL(QL_trnrabmst tbl)
        {
            sSql = "SELECT * FROM QL_mstdept WHERE activeflag='ACTIVE'";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnflag='ACTIVE' ORDER BY CAST(gnother1 AS int)";
            var rabpaytermoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabpaytermoid);
            ViewBag.rabpaytermoid = rabpaytermoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='PROVINSI' AND gnflag='ACTIVE'";
            var rabprovoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabprovoid);
            ViewBag.rabprovoid = rabprovoid;

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' AND gnflag='ACTIVE'";
            var rabcityoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", tbl.rabcityoid);
            ViewBag.rabcityoid = rabcityoid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesoid);
            ViewBag.salesoid = salesoid;

            sSql = "SELECT u.usoid salesoid, u.usname salesname FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            var salesadminoid = new SelectList(db.Database.SqlQuery<listDDLsales>(sSql).ToList(), "salesoid", "salesname", tbl.salesadminoid);
            ViewBag.salesadminoid = salesadminoid;
        }

        private void FillAdditionalField(QL_trnrabmst tblmst)
        {
            ViewBag.partnername = db.Database.SqlQuery<string>("SELECT partnername FROM QL_mstpartner a WHERE a.partneroid ='" + tblmst.partneroid + "'").FirstOrDefault();
            ViewBag.salesname = db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US a WHERE a.usoid ='" + tblmst.salesoid + "'").FirstOrDefault();
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.ToEmail = db.Database.SqlQuery<string>("SELECT custemail FROM QL_mstcust a WHERE a.custoid ='" + tblmst.custoid + "'").FirstOrDefault();
            ViewBag.alamat = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 a WHERE a.custdtl2oid ='" + tblmst.alamatoid + "'").FirstOrDefault();
            ViewBag.reqrabno = db.Database.SqlQuery<string>("SELECT reqrabno FROM QL_trnreqrabmst a WHERE a.reqrabmstoid ='" + tblmst.reqrabmstoid + "'").FirstOrDefault();
            ViewBag.mrserviceno = db.Database.SqlQuery<string>("SELECT mrserviceno FROM QL_trnmrservicemst a WHERE a.mrservicemstoid ='" + tblmst.mrservicemstoid + "'").FirstOrDefault();
        }

        [HttpPost]
        public ActionResult InitDDLGudang()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLGudangBeli()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();

            sSql = "SELECT * FROM QL_m05GN WHERE gngroup='GUDANG' AND gnflag='ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPRABData()
        {
            List<listprab> tbl = new List<listprab>();

            sSql = "SELECT req.reqrabmstoid, req.reqrabno, req.reqrabtype, req.reqrabtype2, CONVERT(varchar(10),req.reqrabestdate,103) reqrabestdate, req.reqrabdate, req.salesoid, c.custname, c.custoid, c.custemail, c.custpaymentoid, c.custprovinceoid reqrabprovoid, c.custcityoid reqrabcityoid, req.projectname, ISNULL(req.reqrabmstnote, '') note FROM QL_trnreqrabmst req INNER JOIN QL_mstcust c ON c.custoid=req.custoid WHERE reqrabmststatus='Approved' ORDER BY reqrabdate DESC";
            tbl = db.Database.SqlQuery<listprab>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetServiceData()
        {
            List<listmrservice> tbl = new List<listmrservice>();

            sSql = "SELECT mr.mrservicemstoid, mr.mrserviceno, mr.mrservicedate, c.custname, i.itemdesc, mr.mrservicemstnote FROM QL_trnmrservicemst mr INNER JOIN QL_mstcust c ON c.custoid=mr.custoid INNER JOIN QL_mstitem i ON i.itemoid=mr.itemoid WHERE mr.mrservicemststatus='Post' ORDER BY mr.mrservicedate DESC";
            tbl = db.Database.SqlQuery<listmrservice>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPartnerData()
        {
            List<QL_mstpartner> tbl = new List<QL_mstpartner>();

            sSql = "SELECT * FROM QL_mstpartner WHERE activeflag='ACTIVE' ORDER BY partnername";
            tbl = db.Database.SqlQuery<QL_mstpartner>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSalesData()
        {
            List<listsales> tbl = new List<listsales>();

            sSql = "SELECT u.usoid salesoid, u.usname salesname, ISNULL((SELECT d.deptname FROM QL_mstdept d WHERE d.deptoid=u.deptoid),'') departemen, ISNULL(u.deptoid,0) deptoid, ISNULL(u.usaddress,'') alamat  FROM QL_m01US u INNER JOIN QL_m05GN g ON g.gnoid=u.Jabatanoid WHERE g.gndesc='SALES' AND u.usflag='ACTIVE' ORDER BY u.usname";
            tbl = db.Database.SqlQuery<listsales>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataSupp()
        {
            List<QL_mstsupp> tbl = new List<QL_mstsupp>();

            sSql = "SELECT * FROM QL_mstsupp WHERE activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataSuppJasa()
        {
            List<QL_mstsupp> tbl = new List<QL_mstsupp>();

            sSql = "SELECT * FROM QL_mstsupp WHERE activeflag='ACTIVE' ORDER BY suppname";
            tbl = db.Database.SqlQuery<QL_mstsupp>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatData(int custoid)
        {
            List<listalamat> tbl = new List<listalamat>();

            sSql = "SELECT custdtl2oid alamatoid, custdtl2loc lokasi, custdtl2addr alamat, custdtl2cityoid cityoid, g.gndesc kota, ISNULL((SELECT g2.gnoid FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),0) provoid, ISNULL((SELECT g2.gndesc FROM QL_m05GN g2 WHERE CAST(g2.gnoid AS VARCHAR(30))=g.gnother2),'') provinsi FROM QL_mstcustdtl2 c INNER JOIN QL_m05GN g ON g.gnoid=c.custdtl2cityoid WHERE c.custoid = " + custoid + " ORDER BY alamatoid";
            tbl = db.Database.SqlQuery<listalamat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' AND i.itemtype IN ('Barang', 'Rakitan', 'Ongkir') ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail2()
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY itemcode) AS INT) itemseq, itemoid, itemcode, itemdesc, itemunitoid, g.gndesc itemunit FROM QL_mstitem i INNER JOIN QL_m05GN g ON g.gnoid=i.itemunitoid WHERE activeflag='ACTIVE' ORDER BY itemcode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataJasaDetail()
        {
            List<listjasa> tbl = new List<listjasa>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY jasacode) AS INT) jasaseq, jasaoid, jasacode, jasadesc, jasaunitoid, g.gndesc jasaunit FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE activeflag='ACTIVE' ORDER BY jasacode";
            tbl = db.Database.SqlQuery<listjasa>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataJasaDetail2()
        {
            List<listjasa> tbl = new List<listjasa>();

            sSql = "SELECT CAST(ROW_NUMBER() OVER(ORDER BY jasacode) AS INT) jasaseq, jasaoid, jasacode, jasadesc, jasaunitoid, g.gndesc jasaunit FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid=i.jasaunitoid WHERE activeflag='ACTIVE' ORDER BY jasacode";
            tbl = db.Database.SqlQuery<listjasa>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListBiaya(string sVar)
        {
            List<acctg> tbl = new List<acctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<acctg>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDetailData(int reqrabmstoid)
        {
            List<listrabdtl> tbl = new List<listrabdtl>();
            sSql = "SELECT rd.reqrabseq AS rabseq, rd.reqrabdtloid AS rabdtloid, i.itemoid, i.itemcode, i.itemdesc, rd.reqrabqty AS rabqty, rd.reqrabunitoid AS rabunitoid, g.gndesc rabunit, reqrabprice AS rabprice, reqrabdtlamt AS rabdtlamt, reqrabtaxvalue AS rabtaxvalue, reqrabtaxamt AS rabtaxamt, reqrabpphvalue AS rabpphvalue, reqrabpphamt AS rabpphamt, reqrabdtlnetto AS rabdtlnetto FROM QL_trnreqrabdtl rd INNER JOIN QL_mstitem i ON i.itemoid = rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid = rd.reqrabunitoid WHERE rd.cmpcode = '" + CompnyCode + "' AND rd.reqrabmstoid = " + reqrabmstoid + "AND rd.reqrabdtlstatus = ''";

            tbl = db.Database.SqlQuery<listrabdtl>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: RAB
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data RAB Detail PO";
            ViewBag.DisplayCol = "none";

            string sfilter = "";
            sSql = "Select pr.rabmstoid, pr.rabno, pr.rabdate, pr.rabtype, ISNULL(pr.projectname,'') projectname, c.custname, pr.rabmststatus, CASE WHEN pr.rabmststatus = 'Revised' THEN pr.revisereason WHEN pr.rabmststatus = 'Rejected' THEN pr.rejectreason ELSE '' END reason FROM QL_trnrabmst pr INNER JOIN QL_mstcust c ON c.custoid=pr.custoid LEFT JOIN QL_trnreqrabmst req ON req.reqrabmstoid = pr.reqrabmstoid WHERE pr.cmpcode='" + Session["CompnyCode"].ToString() + "' " + sfilter + " AND pr.rabmststatus NOT IN ('In Process', 'Revised', 'Rejected', 'Closed') AND pr.rabmstoid IN(SELECT DISTINCT a.rabmstoid FROM QL_trnrabdtl2 a) AND pr.rabmstoid NOT IN(SELECT DISTINCT rabmstoid FROM( SELECT DISTINCT rh.rabmstoid FROM QL_trnrabmst_hist rh INNER JOIN QL_trnrabdtl2_hist rd ON rd.rabmstoid_hist=rh.rabmstoid_hist WHERE rh.rabmststatus='In Approval'  UNION ALL  SELECT DISTINCT rh.rabmstoid FROM QL_trnpoitemmst rh WHERE rh.poitemmststatus='Approved' AND ISNULL(rh.rabmstoid,0)>0 AND rh.poitemmstoid NOT IN(SELECT mrm.pomstoid FROM QL_trnmritemmst mrm))AS tb) ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND pr.rabdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND pr.rabdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                //sSql += " AND pr.rabmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY pr.rabdate";
            List<listrab> tblmst = db.Database.SqlQuery<listrab>(sSql).ToList();
            return View(tblmst);
        }


        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnrabmst tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_trnrabmst();
                tblmst.cmpcode = Session["CompnyCode"].ToString();
                tblmst.rabno = ""; //generateNo(ClassFunction.GetServerTime());
                tblmst.rabdate = ClassFunction.GetServerTime();
                tblmst.rabestdate = ClassFunction.GetServerTime();
                tblmst.rabmststatus = "In Process";
                tblmst.rabtotaljualamt = 0;
                tblmst.rabtotalbeliamt = 0;
                tblmst.rabtotaljualjasaamt = 0;
                tblmst.rabtotalbelijasaamt = 0;
                tblmst.rabtotaljualtaxamt = 0;
                tblmst.rabtotalbelitaxamt = 0;
                tblmst.rabtotaljualjasataxamt = 0;
                tblmst.rabtotalbelijasataxamt = 0;
                tblmst.rabgrandtotaljualamt = 0;
                tblmst.rabgrandtotalbeliamt = 0;
                tblmst.rabtotaljualpphamt = 0;
                tblmst.rabgrandtotalcostamt = 0;
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_trnrabdtl"] = null; Session["QL_trnrabdtl2"] = null; Session["QL_trnrabdtl3"] = null; Session["QL_trnrabdtl4"] = null; Session["QL_trnrabdtl5"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_trnrabmst.Find(Session["CompnyCode"].ToString(), id);

                sSql = "SELECT rd.rabseq, rd.itemoid, i.itemcode, i.itemdesc, rd.rabqty, rd.rabunitoid, g.gndesc rabunit, rd.rabprice, rd.rabdtlamt, rd.rabtaxvalue, rd.rabtaxamt, rd.rabdtlnetto, rd.rabpphvalue, rd.rabpphamt, rd.rabdtlnote, rd.rabongkiramt, rd.rabetd, CONVERT(CHAR(10),rd.rabetd,103) rabetdstr, rd.rablocoid FROM QL_trnrabdtl rd INNER JOIN QL_mstitem i ON i.itemoid=rd.itemoid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabunitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabseq";
                Session["QL_trnrabdtl"] = db.Database.SqlQuery<listrabdtl>(sSql).ToList();

                sSql = "SELECT rd.rabdtl2seq, rd.rabdtl2oid, rd.suppdtl2oid, s.suppname suppdtl2name, rd.itemdtl2oid, i.itemcode, i.itemdesc, rd.rabdtl2qty, rd.rabdtl2unitoid, g.gndesc rabdtl2unit, rd.rabdtl2price, rd.rabdtl2amt, rd.rabdtl2discvalue, rd.rabdtl2discamt, rd.rabdtl2taxvalue, rd.rabdtl2taxamt, rd.rabdtl2netto, rd.rabdtl2note, rd.rabdtl2ongkiramt, rd.rabdtl2eta, CONVERT(CHAR(10),rd.rabdtl2eta,103) rabdtl2etastr, rd.rabdtl2locoid FROM QL_trnrabdtl2 rd INNER JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl2oid INNER JOIN QL_mstitem i ON i.itemoid=rd.itemdtl2oid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabdtl2unitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " AND ISNULL(rabdtl2status,'')='' ORDER BY rd.rabdtl2seq";
                Session["QL_trnrabdtl2"] = db.Database.SqlQuery<listrabdtl2>(sSql).ToList();

                sSql = "SELECT rd.rabdtl3seq, rd.itemdtl3oid, i.jasacode, i.jasadesc, rd.rabdtl3qty, rd.rabdtl3unitoid, g.gndesc rabdtl3unit, rd.rabdtl3price, rd.rabdtl3amt, rd.rabdtl3taxvalue, rd.rabdtl3taxamt, rd.rabdtl3netto, rd.rabdtl3pphvalue, rd.rabdtl3pphamt, rd.rabdtl3note, rd.rabdtl3etd, CONVERT(CHAR(10),rd.rabdtl3etd,103) rabdtl3etdstr, rd.rabdtl3locoid rabdtl3loc FROM QL_trnrabdtl3 rd INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemdtl3oid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabdtl3unitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl3seq";
                Session["QL_trnrabdtl3"] = db.Database.SqlQuery<listrabdtl3>(sSql).ToList();

                sSql = "SELECT rd.rabdtl4seq, rd.suppdtl4oid, s.suppname suppdtl4name, rd.itemdtl4oid, i.jasacode, i.jasadesc, rd.rabdtl4qty, rd.rabdtl4unitoid, g.gndesc rabdtl4unit, rd.rabdtl4price, rd.rabdtl4amt, rd.rabdtl4taxvalue, rd.rabdtl4taxamt, rd.rabdtl4netto, rd.rabdtl4note, rd.rabdtl4eta, CONVERT(CHAR(10),rd.rabdtl4eta,103) rabdtl4etastr FROM QL_trnrabdtl4 rd INNER JOIN QL_mstsupp s ON s.suppoid=rd.suppdtl4oid INNER JOIN QL_mstjasa i ON i.jasaoid=rd.itemdtl4oid INNER JOIN QL_m05GN g ON g.gnoid=rd.rabdtl4unitoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl4seq";
                Session["QL_trnrabdtl4"] = db.Database.SqlQuery<listrabdtl4>(sSql).ToList();

                sSql = "SELECT rd.rabdtl5seq, rd.acctgoid, a.acctgcode, a.acctgdesc, rd.rabdtl5price, rd.rabdtl5note FROM QL_trnrabdtl5 rd INNER JOIN QL_mstacctg a ON a.acctgoid=rd.acctgoid WHERE rd.cmpcode='" + CompnyCode + "' AND rd.rabmstoid=" + id + " ORDER BY rd.rabdtl5seq";
                Session["QL_trnrabdtl5"] = db.Database.SqlQuery<listrabdtl5>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Form(QL_trnrabmst tblmst, string action, string tglmst, string tglest, string custname, SendEmailModel tblse)
        public ActionResult Form(QL_trnrabmst tblmst, string action, string tglmst, string tglest, string custname)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            
            //is Input Detail Valid
            List<listrabdtl2> dtDtl2 = (List<listrabdtl2>)Session["QL_trnrabdtl2"];

            if (dtDtl2 != null)
            {
                if (dtDtl2.Count > 0)
                {
                    for (int i = 0; i < dtDtl2.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl2[i].rabdtl2note))
                            dtDtl2[i].rabdtl2note = "";
                        if (dtDtl2[i].rabdtl2qty == 0)
                        {
                            ModelState.AddModelError("", "Qty " + dtDtl2[i].itemdesc + " Belum Diisi !!");
                        }
                        if (dtDtl2[i].rabdtl2price == 0)
                        {
                            ModelState.AddModelError("", "Harga " + dtDtl2[i].itemdesc + " Belum Diisi !!");
                        }
                        try
                        {
                            dtDtl2[i].rabdtl2eta = DateTime.Parse(ClassFunction.toDate(dtDtl2[i].rabdtl2etastr));
                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("", "Format Tanggal etd Tidak Valid!!" + ex.ToString());
                        }
                    }
                }
            }

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP");
            sSql = "SELECT stoid_app FROM QL_m08AS a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.stoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "'";
            var stoid_app = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
            if (stoid_app <= 0)
            {
                ModelState.AddModelError("", "Approval Structure untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!");
            }

            if (ModelState.IsValid)
            {
                var servertime = ClassFunction.GetServerTime();
                var mstoid_hist = ClassFunction.GenerateID("QL_trnrabmst_hist");
                var dtloid2_hist = ClassFunction.GenerateID("QL_trnrabdtl2_hist");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Edit")
                        {
                            if (dtDtl2 != null)
                            {
                                if (dtDtl2.Count > 0)
                                {
                                    //sSql = "UPDATE QL_trnrabmst SET rabtotalbeliamt="+ tblmst.rabtotalbeliamt + ", rabtotalbelitaxamt=" + tblmst.rabtotalbelitaxamt + ", rabgrandtotalbeliamt=" + tblmst.rabgrandtotalbeliamt + ", rabpersenmargin=" + tblmst.rabpersenmargin + ", rabtotalmargin=" + tblmst.rabtotalmargin + ", upduser='" + Session["UserID"].ToString() + "', updtime='" + servertime + "' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid=" + tblmst.rabmstoid + "";
                                    //db.Database.ExecuteSqlCommand(sSql);
                                    //db.SaveChanges();

                                    QL_trnrabmst_hist tblmst_hist;
                                    tblmst_hist = new QL_trnrabmst_hist();
                                    tblmst_hist.cmpcode = tblmst.cmpcode;
                                    tblmst_hist.rabmstoid_hist = mstoid_hist++;
                                    tblmst_hist.rabmstoid = tblmst.rabmstoid;
                                    tblmst_hist.rabno = tblmst.rabno;
                                    tblmst_hist.rabdate = tblmst.rabdate;
                                    tblmst_hist.rabtypereq = tblmst.rabtypereq;
                                    tblmst_hist.reqrabmstoid = tblmst.reqrabmstoid;
                                    tblmst_hist.rabtypesrv = tblmst.rabtypesrv;
                                    tblmst_hist.mrservicemstoid = tblmst.mrservicemstoid;
                                    tblmst_hist.rabtype = tblmst.rabtype;
                                    tblmst_hist.rabtype2 = tblmst.rabtype2;
                                    tblmst_hist.rabestdate = tblmst.rabestdate;
                                    tblmst_hist.partneroid = tblmst.partneroid;
                                    tblmst_hist.partnerpersen = tblmst.partnerpersen;
                                    tblmst_hist.partneramt = tblmst.partneramt;
                                    tblmst_hist.projectno = tblmst.projectno;
                                    tblmst_hist.projectname = tblmst.projectname;
                                    tblmst_hist.custoid = tblmst.custoid;
                                    tblmst_hist.rabpaytermoid = tblmst.rabpaytermoid;
                                    tblmst_hist.alamatoid = tblmst.alamatoid;
                                    tblmst_hist.rabprovoid = tblmst.rabprovoid;
                                    tblmst_hist.rabcityoid = tblmst.rabcityoid;
                                    tblmst_hist.salesoid = tblmst.salesoid;
                                    tblmst_hist.deptoid = tblmst.deptoid;
                                    tblmst_hist.rabasaljual = tblmst.rabasaljual;
                                    tblmst_hist.rabmststatus = "In Approval";
                                    tblmst_hist.rabmstnote = tblmst.rabmstnote;
                                    tblmst_hist.rabtotaljualamt = tblmst.rabtotaljualamt;
                                    tblmst_hist.rabtotaljualtaxamt = tblmst.rabtotalbelitaxamt;
                                    tblmst_hist.rabtotaljualpphamt = tblmst.rabtotaljualjasapphamt;
                                    tblmst_hist.rabgrandtotaljualamt = tblmst.rabgrandtotaljualamt;
                                    tblmst_hist.rabtotalbeliamt = tblmst.rabtotalbeliamt;
                                    tblmst_hist.rabtotalbelitaxamt = tblmst.rabtotalbelitaxamt;
                                    tblmst_hist.rabgrandtotalbeliamt = tblmst.rabgrandtotalbeliamt;
                                    tblmst_hist.rabtotaljualjasaamt = tblmst.rabtotaljualjasaamt;
                                    tblmst_hist.rabtotaljualjasataxamt = tblmst.rabtotaljualjasataxamt;
                                    tblmst_hist.rabtotaljualjasapphamt = tblmst.rabtotaljualjasapphamt;
                                    tblmst_hist.rabgrandtotaljualjasaamt = tblmst.rabgrandtotaljualjasaamt;
                                    tblmst_hist.rabtotalbelijasaamt = tblmst.rabtotalbelijasaamt;
                                    tblmst_hist.rabtotalbelijasataxamt = tblmst.rabtotalbelijasataxamt;
                                    tblmst_hist.rabtotalbelijasapphamt = tblmst.rabtotalbelijasapphamt;
                                    tblmst_hist.rabgrandtotalbelijasaamt = tblmst.rabgrandtotalbelijasaamt;
                                    tblmst_hist.rabtotalcostamt = tblmst.rabtotalcostamt;
                                    tblmst_hist.rabgrandtotalcostamt = tblmst.rabgrandtotalcostamt;
                                    tblmst_hist.rabpersenmargin = tblmst.rabpersenmargin;
                                    tblmst_hist.rabtotalmargin = tblmst.rabtotalmargin;
                                    tblmst_hist.createuser = Session["UserID"].ToString();
                                    tblmst_hist.createtime = servertime;
                                    tblmst_hist.upduser = Session["UserID"].ToString();
                                    tblmst_hist.updtime = servertime;
                                    tblmst_hist.approvaluser = "";
                                    tblmst_hist.approvaldatetime = DateTime.Parse("1900-01-01");
                                    tblmst_hist.statusjual = "";
                                    tblmst_hist.statusbeli = "";
                                    tblmst_hist.reviseuser = "";
                                    tblmst_hist.revisedatetime = DateTime.Parse("1900-01-01");
                                    tblmst_hist.revisereason = "";
                                    tblmst_hist.rejectreason = "";
                                    tblmst_hist.rejectuser = "";
                                    tblmst_hist.rejectdatetime = DateTime.Parse("1900-01-01");
                                    tblmst_hist.statusjualjasa = "";
                                    tblmst_hist.statusbelijasa = "";
                                    tblmst_hist.curroid = tblmst.curroid;
                                    tblmst_hist.rate2oid = tblmst.rate2oid;
                                    tblmst_hist.revrabtype = tblmst.revrabtype;
                                    tblmst_hist.revrabmstoid = tblmst.revrabmstoid;
                                    tblmst_hist.salesadminoid = tblmst.salesadminoid;

                                    db.QL_trnrabmst_hist.Add(tblmst_hist);

                                    sSql = "UPDATE QL_ID SET lastoid=" + mstoid_hist + " WHERE tablename='QL_trnrabmst_hist'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    QL_trnrabdtl2_hist tbldtl2_hist;
                                    for (int i = 0; i < dtDtl2.Count(); i++)
                                    {
                                        //sSql = "UPDATE QL_trnrabdtl2 SET suppdtl2oid=" + dtDtl2[i].suppdtl2oid + ", rabdtl2price=" + dtDtl2[i].rabdtl2price + ", rabdtl2note='" + dtDtl2[i].rabdtl2note + "', upduser='"+ Session["UserID"].ToString() + "', updtime='"+ servertime +"' WHERE cmpcode='" + CompnyCode +"' AND rabdtl2oid="+ dtDtl2[i].rabdtl2oid + "";
                                        //db.Database.ExecuteSqlCommand(sSql);
                                        //db.SaveChanges();

                                        tbldtl2_hist = new QL_trnrabdtl2_hist();
                                        tbldtl2_hist.cmpcode = tblmst.cmpcode;
                                        tbldtl2_hist.rabdtl2oid_hist = dtloid2_hist++;
                                        tbldtl2_hist.rabmstoid = tblmst.rabmstoid;
                                        tbldtl2_hist.rabdtl2oid = dtDtl2[i].rabdtl2oid;
                                        tbldtl2_hist.rabdtl2seq = i + 1;
                                        tbldtl2_hist.suppdtl2oid = dtDtl2[i].suppdtl2oid;
                                        tbldtl2_hist.itemdtl2oid = dtDtl2[i].itemdtl2oid;
                                        tbldtl2_hist.rabdtl2qty = dtDtl2[i].rabdtl2qty;
                                        tbldtl2_hist.rabdtl2price = dtDtl2[i].rabdtl2price;
                                        tbldtl2_hist.rabdtl2discvalue = dtDtl2[i].rabdtl2discvalue;
                                        tbldtl2_hist.rabdtl2discamt = dtDtl2[i].rabdtl2discamt;
                                        tbldtl2_hist.rabdtl2unitoid = dtDtl2[i].rabdtl2unitoid;
                                        tbldtl2_hist.rabdtl2amt = dtDtl2[i].rabdtl2amt;
                                        tbldtl2_hist.rabdtl2taxvalue = dtDtl2[i].rabdtl2taxvalue;
                                        tbldtl2_hist.rabdtl2taxamt = dtDtl2[i].rabdtl2taxamt;
                                        tbldtl2_hist.rabdtl2netto = dtDtl2[i].rabdtl2netto;
                                        tbldtl2_hist.rabdtl2status = "";
                                        tbldtl2_hist.rabdtl2note = dtDtl2[i].rabdtl2note;
                                        tbldtl2_hist.rabdtl2ongkiramt = dtDtl2[i].rabdtl2ongkiramt;
                                        tbldtl2_hist.rabdtl2eta = dtDtl2[i].rabdtl2eta;
                                        tbldtl2_hist.rabdtl2locoid = dtDtl2[i].rabdtl2locoid;
                                        tbldtl2_hist.upduser = Session["UserID"].ToString();
                                        tbldtl2_hist.updtime = servertime;
                                        tbldtl2_hist.rabmstoid_hist = tblmst_hist.rabmstoid_hist;
                                        tbldtl2_hist.rabdtl2amtbooking = 0;
                                        tbldtl2_hist.rabdtl2flagrevisi = "Belum";

                                        db.QL_trnrabdtl2_hist.Add(tbldtl2_hist);
                                        
                                    }
                                    sSql = "UPDATE QL_ID SET lastoid=" + (dtloid2_hist - 1) + " WHERE tablename='QL_trnrabdtl2_hist'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    // Deklarasi data utk approval
                                    QL_APP tblapp = new QL_APP();
                                    tblapp.cmpcode = tblmst_hist.cmpcode;
                                    tblapp.appoid = appoid;
                                    tblapp.appform = ctrlname;
                                    tblapp.appformoid = tblmst_hist.rabmstoid_hist;
                                    tblapp.requser = tblmst_hist.upduser;
                                    tblapp.reqdate = servertime;
                                    tblapp.appstoid = stoid_app;
                                    tblapp.tablename = "QL_trnrabmst_hist";
                                    db.QL_APP.Add(tblapp);
                                    db.SaveChanges();

                                    //Update lastoid
                                    sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP'";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                        }
                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();

                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        //tblmst.rabmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                //tblmst.rabmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnrabmst tblmst = db.QL_trnrabmst.Find(Session["CompnyCode"].ToString(), id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnrabdtl.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        var trndtl2 = db.QL_trnrabdtl2.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl2.RemoveRange(trndtl2);
                        db.SaveChanges();

                        var trndtl3 = db.QL_trnrabdtl3.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl3.RemoveRange(trndtl3);
                        db.SaveChanges();

                        var trndtl4 = db.QL_trnrabdtl4.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl4.RemoveRange(trndtl4);
                        db.SaveChanges();

                        var trndtl5 = db.QL_trnrabdtl5.Where(a => a.rabmstoid == id);
                        db.QL_trnrabdtl5.RemoveRange(trndtl5);
                        db.SaveChanges();

                        db.QL_trnrabmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptRAB.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usoid FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE rm.cmpcode='" + CompnyCode + "' AND rm.rabmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "RABReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}