﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PGMController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private int DefaultCounter = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"]);
        private string sSql = "";

        public PGMController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class cashbankmst
        {
            public string cmpcode { get; set; }
            public int cashbankoid { get; set; }
            public string cashbankno { get; set; }
            public DateTime cashbankdate { get; set; }
            public string acctgdesc { get; set; }
            public string cashbankstatus { get; set; }
            public string cashbanknote { get; set; }
            public string divname { get; set; }
        }

        public class trnpayargiro
        {
            public int seq { get; set; }
            public int refoid { get; set; }
            public string payargirorefno { get; set; }
            public int refacctgoid { get; set; }
            public string refno { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; }
            public string payargiroduedate { get; set; }
            public decimal payargiroamt { get; set; }
            public string payargironote { get; set; }
            public decimal payargiroamtidr { get; set; }
            public decimal payargiroamtusd { get; set; }
            public string tblname { get; set; }
            public string fieldoid { get; set; }
            public string fieldstatus { get; set; }
            public string fieldtakegiro { get; set; }
            public int acctgoid { get; set; }
            public decimal acctgamt { get; set; }
            public decimal acctgamtidr { get; set; }
            public decimal acctgamtusd { get; set; }
        }

        public class trngiromst
        {
            public int cashbankoid { get; set; }
            public string cashbankno { get; set; }
            public string cashbankrefno { get; set; }
            public string cashbankduedate { get; set; }
            public decimal cashbankamt { get; set; }
            public string payargironote { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; }
            public int acctgoid { get; set; }
            public string acctgdesc { get; set; }
            public decimal cashbankamtidr { get; set; }
            public decimal cashbankamtusd { get; set; }
            public string tblname { get; set; }
            public string fieldoid { get; set; }
            public string fieldstatus { get; set; }
            public string fieldtakegiro { get; set; }
            public int curroid { get; set; }
        }

        public class mstacctg
        {
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }

        private void InitDDL(QL_trncashbankmst tbl)
        {
            sSql = "SELECT * FROM QL_mstcurr WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var curroid = new SelectList(db.Database.SqlQuery<QL_mstcurr>(sSql).ToList(), "curroid", "currcode", tbl.curroid);
            ViewBag.curroid = curroid;

            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE'";
            var acctgoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.acctgoid);
            ViewBag.acctgoid = acctgoid;
        }

        [HttpPost]
        public ActionResult BindDataGiro(string cashbankdate, int curroid, int acctgoid)
        {
            DateTime cbdate = ClassFunction.GetServerTime();
            try
            {
                cbdate = DateTime.Parse(ClassFunction.toDate(cashbankdate));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            List<trngiromst> tbl = new List<trngiromst>();
            sSql = "SELECT * FROM (";
            sSql += "SELECT 'False' AS checkvalue, cashbankoid, cashbankno, cashbankrefno, CONVERT(VARCHAR(10), cashbankduedate, 101) cashbankduedate, cashbankamt, cb.refsuppoid custoid, ISNULL((SELECT s.custname FROM QL_mstcust s WHERE s.custoid=cb.refsuppoid),'') AS custname, cb.giroacctgoid AS acctgoid, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc, cashbankamtidr, cashbankamtusd, 'QL_trncashbankmst' AS tblname, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbanktakegiroreal' AS fieldtakegiro, '' payargironote FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.giroacctgoid WHERE cb.cmpcode='" + CompnyCode + "' AND cashbanktype='BGM' AND cashbankgroup NOT IN ('RECEIPT GIRO', 'AR') AND cashbankstatus='Post' AND ISNULL(cashbankres1, '')='' AND cb.curroid=" + curroid + " AND cashbankduedate<='" + cbdate + "' AND cb.acctgoid=" + acctgoid + " AND cb.cashbankoid NOT IN(SELECT cashbankoid FROM QL_trndpar WHERE ISNULL(dparres1,'')='Batal') ";
            sSql += " UNION ALL ";
            sSql += "SELECT 'False' AS checkvalue, cashbankgloid AS cashbankoid, cashbankno, cashbankglrefno AS cashbankrefno, CONVERT(VARCHAR(10), cashbankglduedate, 101) AS cashbankduedate, cashbankglamt AS cashbankamt, s.custoid, custname, gl.giroacctgoid AS acctgoid, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc, cashbankglamtidr AS cashbankamtidr, cashbankglamtusd AS cashbankamtusd, 'QL_trncashbankgl' AS tblname, 'cashbankgloid' AS fieldoid, 'cashbankglgiroflag' AS fieldstatus, 'cashbankgltakegiroreal' AS fieldtakegiro, '' payargironote FROM QL_trncashbankgl gl INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=gl.cmpcode AND cb.cashbankoid=gl.cashbankoid INNER JOIN QL_mstcust s ON s.custoid=cb.personoid INNER JOIN QL_mstacctg a ON a.acctgoid=gl.giroacctgoid WHERE gl.cmpcode='" + CompnyCode + "' AND cashbanktype='BGM' AND cashbankgroup='RECEIPT GIRO' AND cashbankstatus='Post' AND cashbankglgiroflag='' AND cb.curroid=" + curroid + " AND cashbankglduedate<='" + cbdate + "' AND cb.acctgoid=" + acctgoid + "";
            sSql += " UNION ALL ";
            sSql += "SELECT 'False' AS checkvalue, payaroid AS cashbankoid, cashbankno, payarrefno AS cashbankrefno, CONVERT(VARCHAR(10), payarduedate, 101) AS cashbankduedate, payaramt AS cashbankamt, s.custoid, custname, cb.giroacctgoid AS acctgoid, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc, payaramtidr AS cashbankamtidr, payaramtusd AS cashbankamtusd, 'QL_trnpayar' AS tblname, 'payaroid' AS fieldoid, 'payargiroflag' AS fieldstatus, 'payartakegiroreal' AS fieldtakegiro, '' payargironote FROM QL_trnpayar pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstcust s ON s.custoid=cb.refsuppoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.giroacctgoid WHERE pay.cmpcode='" + CompnyCode + "' AND cashbanktype='BGM' AND cashbankgroup='AR' AND cashbankstatus='Post' AND payargiroflag='' AND cb.curroid=" + curroid + " AND payarduedate<='" + cbdate + "' AND cb.acctgoid=" + acctgoid + "";
            sSql += ") AS tbl_GiroIn ORDER BY CAST (cashbankduedate AS DATETIME)";
            tbl = db.Database.SqlQuery<trngiromst>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult BindListCOA(string sVar, string sFilter)
        {
            List<mstacctg> tbl = new List<mstacctg>();
            string acctgoid = ClassFunction.GetDataAcctgOid(sVar, CompnyCode);
            sSql = "SELECT acctgoid, acctgcode, ('(' + acctgcode + ') ' + acctgdesc) acctgdesc FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") " + sFilter + " ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowCOAPosting(string cashbankno, string ratetype, string glother1)
        {
            return Json(ClassFunction.ShowCOAPosting(cashbankno, CompnyCode, ratetype, glother1), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<trnpayargiro> dtDtl)
        {
            Session["QL_trnpayargiro"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpayargiro"] == null)
            {
                Session["QL_trnpayargiro"] = new List<trnpayargiro>();
            }

            List<trnpayargiro> dataDtl = (List<trnpayargiro>)Session["QL_trnpayargiro"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trncashbankmst tbl)
        {

        }

        private string GenerateReceiptNo2(string cmpcode, DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + cashbankdate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return cashbankno;
        }

        [HttpPost]
        public ActionResult GenerateReceiptNo(DateTime cashbankdate, string cashbanktype, int acctgoid)
        {
            var cashbankno = "";
            if (CompnyCode != "")
            {
                string sNo = cashbanktype + "/" + cashbankdate.ToString("yy/MM") + "/";
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, "+ DefaultCounter +") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" + CompnyCode + "' AND cashbankno LIKE '%" + sNo + "%' /*AND acctgoid=" + acctgoid + "*/";
                cashbankno = sNo + ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), DefaultCounter);
            }
            return Json(cashbankno, JsonRequestBehavior.AllowGet);
        }

        // GET: cashbankMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT cb.cmpcode, cashbankoid, cashbankno, cashbankdate, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, cashbankstatus, cashbanknote, 'False' AS checkvalue,'CV. ANUGRAH PRATAMA' divname FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid WHERE cashbankgroup='GIRO IN' ";

            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += "AND cb.cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            else
                sSql += "AND cb.cmpcode LIKE '%'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND cashbankdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND cashbankdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else

            {
                sSql += " AND cashbankstatus IN ('In Process')";
            }

            sSql += " ORDER BY CONVERT(DATETIME, cb.cashbankdate) DESC, cb.cashbankoid DESC";

            List<cashbankmst> dt = db.Database.SqlQuery<cashbankmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: cashbankMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl;
            string action = "Create";
            if (id == null | CompnyCode == null)
            {
                tbl = new QL_trncashbankmst();
                tbl.cashbankoid = ClassFunction.GenerateID("QL_trncashbankmst");
                tbl.cashbankdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.cashbankstatus = "In Process";
                tbl.cashbanktaxtype = "NON TAX";
                tbl.cashbanktakegiro = ClassFunction.GetServerTime();

                Session["QL_trnpayargiro"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trncashbankmst.Find(CompnyCode, id);

                sSql = "SELECT 0 AS seq, pay.refoid, payargirorefno, refacctgoid, cashbankno AS refno, pay.custoid, (CASE cashbankgroup WHEN 'MUTATIONTO' THEN '' ELSE ISNULL(custname, '') END) custname, CONVERT(VARCHAR(10), payargiroduedate, 101) AS payargiroduedate, payargiroamt, payargironote, payargiroamtidr, payargiroamtusd, (CASE pay.reftype WHEN 'Pay A/R Giro In' THEN 'QL_trncashbankmst' ELSE pay.reftype END) AS tblname, (CASE pay.reftype WHEN 'QL_trnpayar' THEN 'payaroid' WHEN 'QL_trncashbankgl' THEN 'cashbankgloid' ELSE 'cashbankoid' END) AS fieldoid, (CASE pay.reftype WHEN 'QL_trnpayar' THEN 'payargiroflag' WHEN 'QL_trncashbankgl' THEN 'cashbankglgiroflag' ELSE 'cashbankres1' END) AS fieldstatus, (CASE pay.reftype WHEN 'QL_trnpayar' THEN 'payartakegiroreal' WHEN 'QL_trncashbankgl' THEN 'cashbankgltakegiroreal' ELSE 'cashbanktakegiroreal' END) AS fieldtakegiro FROM QL_trnpayargiro pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.refoid LEFT JOIN QL_mstcust s ON s.custoid=pay.custoid WHERE pay.cmpcode='" + CompnyCode + "' AND pay.cashbankoid=" + id;
                var tbldtl = db.Database.SqlQuery<trnpayargiro>(sSql).ToList();
                if (tbldtl != null)
                {
                    if (tbldtl.Count() > 0)
                    {
                        for (var i = 0; i < tbldtl.Count(); i++)
                        {
                            tbldtl[i].seq = i + 1;
                        }
                    }
                }
                Session["QL_trnpayargiro"] = tbldtl;
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trncashbankmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            if (tbl.cashbankno == null)
                tbl.cashbankno = "";
            tbl.cmpcode = CompnyCode;
            List<trnpayargiro> dtDtl = (List<trnpayargiro>)Session["QL_trnpayargiro"];
            if (tbl.curroid == 0)
                ModelState.AddModelError("", "Please select CURRENCY field!");
            if (tbl.acctgoid == 0)
                ModelState.AddModelError("", "Please select BANK ACCOUNT field!");

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].refoid == 0)
                            ModelState.AddModelError("", "Please select BGM NO. field!");
                    }
                }
            }

            try
            {
                tbl.cashbankdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("cashbankdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }

            var cRate = new ClassRate();
            DateTime sDate = tbl.cashbankdate;
            string sPeriod = ClassFunction.GetDateToPeriodAcctg(sDate);

            if (tbl.cashbankstatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, tbl.cashbankdate.ToString("MM/dd/yyyy"));
                if (cRate.GetRateDailyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateDailyLastError); tbl.cashbankstatus = "In Process";
                }
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError); tbl.cashbankstatus = "In Process";
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.cashbankdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.cashbankstatus = "In Process";
            }
            if (tbl.cashbankstatus == "Post")
            {
                cekClosingDate = sDate;//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.cashbankstatus = "In Process";
                }
            }

            if (!ModelState.IsValid)
                tbl.cashbankstatus = "In Process";

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                if (action == "Create")
                {
                    sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" + tbl.cashbankoid;
                    if (db.Database.SqlQuery<int>(sSql).FirstOrDefault() > 0)
                    {
                        mstoid = ClassFunction.GenerateID("QL_trncashbankmst");
                        tbl.cashbankno = GenerateReceiptNo2(CompnyCode, tbl.cashbankdate, tbl.cashbanktype, tbl.acctgoid);
                    }
                }
                var dtloid = ClassFunction.GenerateID("QL_trnpayargiro");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                var servertime = ClassFunction.GetServerTime();

                tbl.cashbankgroup = "GIRO IN";
                tbl.cashbanktype = "BBM";
                tbl.cashbanktakegiro = tbl.cashbankdate;
                tbl.cashbankduedate = tbl.cashbankdate;
                tbl.cashbanktakegiroreal = Convert.ToDateTime("1/1/1900");
                tbl.cashbankgiroreal = Convert.ToDateTime("1/1/1900");
                tbl.cashbankaptype = (tbl.cashbankaptype == null ? "" : tbl.cashbankaptype);
                tbl.cashbanknote = (tbl.cashbanknote == null ? "" : ClassFunction.Tchar(tbl.cashbanknote));
                if (tbl.refsuppoid != 0)
                    ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE custoid=" + tbl.refsuppoid).FirstOrDefault();
                tbl.cashbanktaxtype = "NON TAX";
                tbl.giroacctgoid = 0;
                tbl.cashbankdpp = tbl.cashbankamt;
                tbl.cashbankamtidr = tbl.cashbankamt * cRate.GetRateMonthlyIDRValue;
                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.cashbankdate);

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            if (db.QL_trncashbankmst.Find(tbl.cmpcode, tbl.cashbankoid) != null)
                                tbl.cashbankoid = mstoid;

                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trncashbankmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.cashbankoid + " WHERE tablename='QL_trncashbankmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trncashbankmst SET cashbankres1='', cashbanktakegiroreal='01/01/1900' WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + " AND reftype IN ('QL_trncashbankmst', 'Pay A/R Giro In'))";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trncashbankgl SET cashbankglgiroflag='', cashbankgltakegiroreal='01/01/1900' WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankgloid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + " AND reftype IN ('QL_trncashbankgl'))";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                            sSql = "UPDATE QL_trnpayar SET payargiroflag='', payartakegiroreal='01/01/1900' WHERE cmpcode='" + tbl.cmpcode + "' AND payaroid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + " AND reftype IN ('QL_trnpayar'))";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnpayargiro.Where(a => a.cashbankoid == tbl.cashbankoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpayargiro.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnpayargiro tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpayargiro();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.payargirooid = dtloid++;
                            tbldtl.cashbankoid = tbl.cashbankoid;
                            tbldtl.custoid = dtDtl[i].custoid;
                            tbldtl.reftype = (dtDtl[i].tblname == "QL_trncashbankmst" ? "Pay A/R Giro In" : dtDtl[i].tblname);
                            tbldtl.refoid = dtDtl[i].refoid;
                            tbldtl.refacctgoid = dtDtl[i].refacctgoid;
                            tbldtl.payargirorefno = dtDtl[i].payargirorefno;
                            tbldtl.payargiroduedate = Convert.ToDateTime(dtDtl[i].payargiroduedate);
                            tbldtl.payargiroamt = dtDtl[i].payargiroamt;
                            tbldtl.payargiroamtidr = dtDtl[i].payargiroamt * cRate.GetRateMonthlyIDRValue;
                            tbldtl.payargiroamtusd = 0;
                            tbldtl.payargironote = (dtDtl[i].payargironote == null ? "" : dtDtl[i].payargironote);
                            tbldtl.payargirostatus = tbl.cashbankstatus;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnpayargiro.Add(tbldtl);
                            db.SaveChanges();

                            sSql = "UPDATE " + dtDtl[i].tblname + " SET " + dtDtl[i].fieldstatus + "='Closed', " + dtDtl[i].fieldtakegiro + "='" + tbl.cashbankdate + "' WHERE cmpcode='" + tbl.cmpcode + "' AND " + dtDtl[i].fieldoid + "=" + dtDtl[i].refoid;
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpayargiro'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.cashbankstatus == "Post")
                        {
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(tbl.cmpcode, glmstoid, sDate, sPeriod, "Giro In|No=" + tbl.cashbankno + "", tbl.cashbankstatus, tbl.updtime, tbl.createuser, tbl.createtime, tbl.upduser, tbl.updtime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            int iSeq = 1;
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, tbl.acctgoid, "D", tbl.cashbankamt, tbl.cashbankno, tbl.cashbankno + " | " + (tbl.cashbanknote == null ? "" : tbl.cashbanknote), tbl.cashbankstatus, tbl.upduser, tbl.updtime, tbl.cashbankamtidr, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                            db.SaveChanges();
                            iSeq += 1;
                            gldtloid += 1;

                            for (int i = 0; i < dtDtl.Count(); i++)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(tbl.cmpcode, gldtloid, iSeq, glmstoid, dtDtl[i].refacctgoid, "C", dtDtl[i].payargiroamt, tbl.cashbankno, tbl.cashbankno + " | " + (tbl.cashbanknote == null ? "" : tbl.cashbanknote), tbl.cashbankstatus, tbl.upduser, tbl.updtime, dtDtl[i].payargiroamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trncashbankmst " + tbl.cashbankoid + "", "", "", "", 0));
                                db.SaveChanges();
                                iSeq += 1;
                                gldtloid += 1;
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: cashbankMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trncashbankmst tbl = db.QL_trncashbankmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trncashbankmst SET cashbankres1='', cashbanktakegiroreal='01/01/1900' WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + " AND reftype IN ('QL_trncashbankmst', 'Pay A/R Giro In'))";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trncashbankgl SET cashbankglgiroflag='', cashbankgltakegiroreal='01/01/1900' WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankgloid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + " AND reftype IN ('QL_trncashbankgl'))";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();
                        sSql = "UPDATE QL_trnpayar SET payargiroflag='', payartakegiroreal='01/01/1900' WHERE cmpcode='" + tbl.cmpcode + "' AND payaroid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" + tbl.cmpcode + "' AND cashbankoid=" + tbl.cashbankoid + " AND reftype IN ('QL_trnpayar'))";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnpayargiro.Where(a => a.cashbankoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnpayargiro.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trncashbankmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptGiroIn.rpt"));
            report.SetParameterValue("sWhere", " WHERE cb.cashbankgroup='GIRO IN' AND cb.cmpcode='" + CompnyCode + "' AND cb.cashbankoid IN (" + id + ")");
            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "IncomingGiroRealizationPrintOut.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}