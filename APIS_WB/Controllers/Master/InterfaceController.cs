﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Master
{
    public class InterfaceController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public InterfaceController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listinterface
        {
            public int interfaceoid { get; set; }
            public string interfacevar { get; set; }
            public string interfacevalue { get; set; }
            public string activeflag { get; set; }
            public string interfacenote { get; set; }
            public string interfaceres2 { get; set; }
        }

        public class listinterfacedtl
        {
            public int interfaceseq { get; set; }
            public int acctgoid { get; set; }
            public string acctgcode { get; set; }
            public string acctgdesc { get; set; }
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<listinterfacedtl> dtDtl)
        {
            Session["QL_mstinterfacedtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillDetailData()
        {
            if (Session["QL_mstinterfacedtl"] == null)
            {
                Session["QL_mstinterfacedtl"] = new List<listinterfacedtl>();
            }

            List<listinterfacedtl> dataDtl = (List<listinterfacedtl>)Session["QL_mstinterfacedtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }


        private void InitDDL(QL_mstinterface tbl)
        {
            //sSql = "SELECT * FROM QL_mstdept WHERE activeflag='ACTIVE'";
            //var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdept>(sSql).ToList(), "deptoid", "deptname", "deptoid");
            //ViewBag.deptoid = deptoid;
        }
     

        private void FillAdditionalField(QL_mstinterface tblmst)
        {
            //ViewBag.rabno = db.Database.SqlQuery<string>("SELECT rabno FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tblmst.rabmstoid + "'").FirstOrDefault();           
        }

        [HttpPost]
        public ActionResult GetCOAData()
        {
            List<QL_mstacctg> tbl = new List<QL_mstacctg>();

            sSql = "SELECT * FROM QL_mstacctg WHERE activeflag='ACTIVE' ORDER BY acctgcode";
            tbl = db.Database.SqlQuery<QL_mstacctg>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

       

        // GET/POST: SOItem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            sSql = "SELECT * FROM QL_mstinterface WHERE cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " ORDER BY interfaceoid DESC";
            List<listinterface> vbag = db.Database.SqlQuery<listinterface>(sSql).ToList();
            return View(vbag);
        }


        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstinterface tblmst;
            string action = "Create";
            if (id == null)
            {
                tblmst = new QL_mstinterface();
                tblmst.cmpcode = CompnyCode;                
                tblmst.createuser = Session["UserID"].ToString();
                tblmst.createtime = ClassFunction.GetServerTime();

                Session["QL_mstinterfacedtl"] = null;
            }
            else
            {
                action = "Edit";
                tblmst = db.QL_mstinterface.Find(CompnyCode, id);

                sSql = "SELECT d.interfaceseq, a.acctgoid, a.acctgcode, a.acctgdesc FROM QL_mstinterfacedtl d INNER JOIN QL_mstacctg a ON a.acctgoid=d.acctgoid WHERE d.cmpcode='" + CompnyCode + "' AND d.interfaceoid=" + id + " ORDER BY d.interfaceseq";
                Session["QL_mstinterfacedtl"] = db.Database.SqlQuery<listinterfacedtl>(sSql).ToList();
            }

            if (tblmst == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstinterface tblmst, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //is Input Valid
            
            //is Input Detail Valid
            List<listinterfacedtl> dtDtl = (List<listinterfacedtl>)Session["QL_mstinterfacedtl"];
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");

            var acctgcodeSum = "";
            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        acctgcodeSum += dtDtl[i].acctgcode + ",";
                    }
                    tblmst.interfacevalue = ClassFunction.Left(acctgcodeSum, acctgcodeSum.Length - 1);
                }
            }

            if (ModelState.IsValid)
            {
                tblmst.cmpcode = CompnyCode;
                tblmst.interfacegroup = "ACCTG";
                tblmst.interfaceres1 = CompnyCode;
                tblmst.interfaceres3 = "";
                var servertime = ClassFunction.GetServerTime();
                var mstoid = ClassFunction.GenerateID("QL_mstinterface");
                var dtloid = ClassFunction.GenerateID("QL_mstinterfacedtl");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert                          
                            tblmst.createtime = servertime;
                            tblmst.createuser = Session["UserID"].ToString();
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();                            
                            db.QL_mstinterface.Add(tblmst);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + mstoid + " Where tablename = 'QL_mstinterface'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tblmst.createtime = tblmst.createtime;
                            tblmst.createuser = tblmst.createuser;
                            tblmst.updtime = servertime;
                            tblmst.upduser = Session["UserID"].ToString();
                            db.Entry(tblmst).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_mstinterfacedtl.Where(a => a.interfaceoid == tblmst.interfaceoid && a.cmpcode == tblmst.cmpcode);
                            db.QL_mstinterfacedtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_mstinterfacedtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_mstinterfacedtl();
                            tbldtl.cmpcode = tblmst.cmpcode;
                            tbldtl.interfacedtloid = dtloid++;
                            tbldtl.interfaceoid = tblmst.interfaceoid;
                            tbldtl.interfaceseq = i + 1;
                            tbldtl.acctgoid = dtDtl[i].acctgoid;
                            tbldtl.acctgcode = dtDtl[i].acctgcode;
                            tbldtl.upduser = Session["UserID"].ToString();
                            tbldtl.updtime = tblmst.updtime;
                            db.QL_mstinterfacedtl.Add(tbldtl);
                        }

                        sSql = "UPDATE QL_ID SET lastoid=" + dtloid + " WHERE tablename='QL_mstinterfacedtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        //tblmst.soitemmststatus = "In Process";
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                //tblmst.soitemmststatus = "In Process";
            }
            ViewBag.action = action;
            InitDDL(tblmst);
            FillAdditionalField(tblmst);
            return View(tblmst);
        }       
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}