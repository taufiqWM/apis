﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Drawing;

namespace APIS_WB.Controllers
{
    public class APIWebInfoController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string urlimgsrv = "https://apis.anugrahpratama.com/";
        private string itempath = "~/Images/UploadTagihan";

        public object SAWHelpers { get; private set; }

        public APIWebInfoController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listddl
        {
            public int ddlid { get; set; }
            public string ddltxt { get; set; }
        }

        [HttpPost]
        public ActionResult PostLoginUser(string UserID, string Password)
        {
            var result = "sukses"; var msg = ""; var usertype = "";

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (string.IsNullOrEmpty(Password))
                msg += "- Password harap di isi";
            if (!string.IsNullOrEmpty(UserID) && !string.IsNullOrEmpty(Password))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.uspassword == Password && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.uspassword == Password && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {
                    usertype = db.QL_m01US.Where(x => x.usoid == UserID && x.uspassword == Password).Select(x => x.ustype).FirstOrDefault();
                    UserID = db.QL_m01US.Where(x => x.usoid == UserID && x.uspassword == Password).Select(x => x.usoid).FirstOrDefault();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, UserID, usertype }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetContry(string UserID = "")
        {
            var result = "sukses"; var msg = "";
            List<listddl> data = new List<listddl>();

            //if (string.IsNullOrEmpty(UserID))
            //    msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {

                    sSql = "select gnoid ddlid, gndesc ddltxt from ql_m05gn where gngroup='NEGARA' AND gnflag='ACTIVE'";
                    data = db.Database.SqlQuery<listddl>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetProvince(string UserID = "", string countryid = "")
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listddl> data = new List<listddl>();

            //if (string.IsNullOrEmpty(UserID))
            //    msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {
                    if (countryid != "")
                    {
                        sPlus = " AND gnother1 = '" + countryid + "'";
                    }

                    sSql = "select gnoid ddlid, gndesc ddltxt from ql_m05gn where gngroup='PROVINSI' AND gnflag='ACTIVE' " + sPlus + "";
                    data = db.Database.SqlQuery<listddl>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCity(string UserID = "", string countryid = "", string provid = "")
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listddl> data = new List<listddl>();

            //if (string.IsNullOrEmpty(UserID))
            //    msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                if (countryid != "")
                {
                    sPlus = " AND gnother1 = '" + countryid + "'";
                }
                if (provid != "")
                {
                    sPlus = " AND gnother2 = '" + provid + "'";
                }

                try
                {
                    sSql = "select gnoid ddlid, gndesc ddltxt from ql_m05gn where gngroup='KOTA' AND gnflag='ACTIVE' " + sPlus + "";
                    data = db.Database.SqlQuery<listddl>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetKategoriSupp(string UserID = "")
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listddl> data = new List<listddl>();

            //if (string.IsNullOrEmpty(UserID))
            //    msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {
                    sSql = "select gnoid ddlid, gndesc ddltxt from ql_m05gn where gngroup='KATEGORI SUPPLIER' AND gnflag='ACTIVE' " + sPlus + "";
                    data = db.Database.SqlQuery<listddl>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetKategoriCust(string UserID = "")
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listddl> data = new List<listddl>();

            //if (string.IsNullOrEmpty(UserID))
            //    msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {
                    sSql = "select gnoid ddlid, gndesc ddltxt from ql_m05gn where gngroup='KATEGORI CUSTOMER' AND gnflag='ACTIVE' " + sPlus + "";
                    data = db.Database.SqlQuery<listddl>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPayterm(string UserID = "")
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listddl> data = new List<listddl>();

            //if (string.IsNullOrEmpty(UserID))
            //    msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0 && db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {
                    sSql = "select gnoid ddlid, gndesc ddltxt from ql_m05gn where gngroup='PAYMENT TERM' AND gnflag='ACTIVE' " + sPlus + "";
                    data = db.Database.SqlQuery<listddl>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string generateCodeCust()
        {
            string sCode = "CUST-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(custCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custCode LIKE '" + sCode + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        public class listcustomer
        {
            public string usoid { get; set; }
            public string uspassword { get; set; }
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public int custprovinceoid { get; set; }
            public int custcityoid { get; set; }
            public string custpostcode { get; set; }
            public int custpaymentoid { get; set; }
            public int custtaxable { get; set; }
            public string custnpwp { get; set; }
            public string custnpwpno { get; set; }
            public string custnpwpaddr { get; set; }
            public string custphone1 { get; set; }
            public string custemail { get; set; }
            public string custcategoryid { get; set; }
        }

        [HttpPost]
        public ActionResult PostCustomerAll(string dt, string tagkategori, string dtalamat, string dtpic)
        {
            var result = "sukses"; var msg = "";
            var mstoid = 0; var alamatdtloid = 0; var picdtloid = 0;
            var dtlist = new List<listcustomer>();
            var taglist = new List<string>();
            var dtlistalamat = new List<listalamatcust>();
            var dtlistpic = new List<listpiccust>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listcustomer>>(dt);
                taglist = JsonConvert.DeserializeObject<List<string>>(tagkategori);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    var usoidlist = dtlist[0].usoid;

                    if (string.IsNullOrEmpty(dtlist[0].usoid))
                        msg += "- User ID harap di isi";
                    else if (db.QL_m01US.Where(w => w.usoid == usoidlist).Count() > 0)
                        msg += "- This User ID already use, Please Fill Another User ID!!";
                    if (string.IsNullOrEmpty(dtlist[0].uspassword))
                        msg += "- Password harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custname))
                        msg += "- Nama Customer harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custaddr))
                        msg += "- Alamat harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custphone1))
                        msg += "- No Telp harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custemail))
                        msg += "- Email harap di isi";
                    if (tagkategori == null)
                        msg += "- Kategori harap di isi";
                }
            }

            //Alamat
            if (string.IsNullOrEmpty(dtalamat))
            {
                msg += "- Data Alamat Kosong!";
            }
            else
            {
                dtlistalamat = JsonConvert.DeserializeObject<List<listalamatcust>>(dtalamat);

                if (dtlistalamat.Count <= 0)
                {
                    msg += "- Data Alamat Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlistalamat.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlistalamat[i].custdtl2loc))
                            msg += "- Lokasi harap di isi";
                        if (string.IsNullOrEmpty(dtlistalamat[i].custdtl2addr))
                            msg += "- Alamat harap di isi";
                    }
                }
            }

            //pic
            if (string.IsNullOrEmpty(dtpic))
            {
                msg += "- Data PIC Kosong!";
            }
            else
            {
                dtlistpic = JsonConvert.DeserializeObject<List<listpiccust>>(dtpic);

                if (dtlistpic.Count <= 0)
                {
                    msg += "- Data PIC Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlistpic.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlistpic[i].custdtl1picname))
                            msg += "- Nama PIC harap di isi";
                        if (string.IsNullOrEmpty(dtlistpic[i].custdtl1phone))
                            msg += "- No Telp harap di isi";
                    }
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                mstoid = ClassFunction.GenerateID("QL_mstcust");
                alamatdtloid = ClassFunction.GenerateID("QL_mstcustdtl2");
                picdtloid = ClassFunction.GenerateID("QL_mstcustdtl1");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcust tbl = new QL_mstcust();

                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                string stsval = "";
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    stsval += "" + taglist[i] + ",";
                                }
                                tbl.custcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                            }
                            else
                            {
                                tbl.custcategoryid = "";
                            }
                        }
                        else
                        {
                            tbl.custcategoryid = "";
                        }

                        tbl.cmpcode = CompnyCode;
                        tbl.custoid = mstoid;
                        tbl.custcode = generateCodeCust();
                        tbl.custcategory = "";
                        tbl.custname = dtlist[0].custname;
                        tbl.custaddr = dtlist[0].custaddr;
                        tbl.custprovinceoid = dtlist[0].custprovinceoid;
                        tbl.custcityoid = dtlist[0].custcityoid;
                        tbl.custpaymentoid = dtlist[0].custpaymentoid;
                        tbl.custtaxable = dtlist[0].custtaxable;
                        tbl.custnpwp = dtlist[0].custnpwp ?? "";
                        tbl.custnpwpno = dtlist[0].custnpwpno ?? "";
                        tbl.custnpwpaddr = dtlist[0].custnpwpaddr ?? "";
                        tbl.custphone1 = dtlist[0].custphone1 ?? "";
                        tbl.custphone2 = "";
                        tbl.custfax1 = "";
                        tbl.custemail = dtlist[0].custemail ?? "";
                        tbl.custnote = "";
                        tbl.createtime = servertime;
                        tbl.createuser = dtlist[0].usoid;
                        tbl.custpostcode = dtlist[0].custpostcode ?? "";
                        tbl.custktpimg = "";
                        tbl.custnpwpimg = "";
                        tbl.custcreditlimit = 0;
                        tbl.custcreditusage = 0;
                        tbl.activeflag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = dtlist[0].usoid;
                        tbl.custcategory2 = "Corporate";
                        db.QL_mstcust.Add(tbl);
                        db.SaveChanges();

                        //Update lastoid
                        sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcust'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        QL_m01US tblus = new QL_m01US();
                        tblus.cmpcode = CompnyCode;
                        tblus.usoid = dtlist[0].usoid;
                        tblus.usname = dtlist[0].custname;
                        tblus.uspassword = dtlist[0].uspassword;
                        tblus.ustype = "cust";
                        tblus.usflag = "ACTIVE";
                        tblus.usemail = "";
                        tblus.stoid = 0;
                        tblus.usphone = "";
                        tblus.crttime = servertime;
                        tblus.crtuser = dtlist[0].usoid;
                        tblus.updtime = servertime;
                        tblus.upduser = dtlist[0].usoid;
                        tblus.expmastertype = "No";
                        tblus.suppcustoid = tbl.custoid;
                        db.QL_m01US.Add(tblus);
                        db.SaveChanges();

                        //insert cat detail
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    int catid = int.Parse(taglist[i]);

                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstcust";
                                    tblcatdtl.catdtlrefoid = tbl.custoid;
                                    tblcatdtl.catoid = catid;
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        mstoid = tbl.custoid;

                        //Alamat
                        QL_mstcustdtl2 tbl2;
                        for (int i = 0; i < dtlistalamat.Count(); i++)
                        {
                            tbl2 = new QL_mstcustdtl2();
                            tbl2.cmpcode = CompnyCode;
                            tbl2.custdtl2oid = alamatdtloid++;
                            tbl2.custdtl2seq = i + 1;
                            tbl2.custoid = mstoid;
                            tbl2.custdtl2loc = dtlistalamat[i].custdtl2loc;
                            tbl2.custdtl2addr = dtlistalamat[i].custdtl2addr;
                            tbl2.custdtl2picitem = dtlistalamat[i].custdtl2picitem ?? "";
                            tbl2.custdtl2phone1 = dtlistalamat[i].custdtl2phone ?? "";
                            tbl2.custdtl2phone2 = "";
                            tbl2.custdtl2email = dtlistalamat[i].custdtl2email ?? "";
                            tbl2.updtime = servertime;
                            tbl2.upduser = dtlist[0].usoid;
                            db.QL_mstcustdtl2.Add(tbl2);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (alamatdtloid - 1) + " Where tablename = 'QL_mstcustdtl2'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //pic
                        QL_mstcustdtl1 tbl3;
                        for (int i = 0; i < dtlistpic.Count(); i++)
                        {
                            tbl3 = new QL_mstcustdtl1();
                            tbl3.cmpcode = CompnyCode;
                            tbl3.custdtl1oid = picdtloid++;
                            tbl3.custdtl1seq = i + 1;
                            tbl3.custoid = mstoid;
                            tbl3.custdtl1picname = dtlistpic[i].custdtl1picname;
                            tbl3.custdtl1phone1 = dtlistpic[i].custdtl1phone;
                            tbl3.custdtl1phone2 = "";
                            tbl3.custdtl1jabatan = dtlistpic[i].custdtl1jabatan ?? "";
                            tbl3.custdtl1email = dtlistpic[i].custdtl1email ?? "";
                            tbl3.custdtl1flag = "ACTIVE";
                            tbl3.updtime = servertime;
                            tbl3.upduser = dtlist[0].usoid;
                            db.QL_mstcustdtl1.Add(tbl3);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (picdtloid - 1) + " Where tablename = 'QL_mstcustdtl1'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostCustomer(string dt, string tagkategori)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listcustomer>();
            var taglist = new List<string>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listcustomer>>(dt);
                taglist = JsonConvert.DeserializeObject<List<string>>(tagkategori);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    var usoidlist = dtlist[0].usoid;

                    if (string.IsNullOrEmpty(dtlist[0].usoid))
                        msg += "- User ID harap di isi";
                    else if (db.QL_m01US.Where(w => w.usoid == usoidlist).Count() > 0)
                        msg += "- This User ID already use, Please Fill Another User ID!!";
                    if (string.IsNullOrEmpty(dtlist[0].uspassword))
                        msg += "- Password harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custname))
                        msg += "- Nama Customer harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custaddr))
                        msg += "- Alamat harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custphone1))
                        msg += "- No Telp harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custemail))
                        msg += "- Email harap di isi";
                    if (tagkategori == null)
                        msg += "- Kategori harap di isi";
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                mstoid = ClassFunction.GenerateID("QL_mstcust");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcust tbl = new QL_mstcust();

                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                string stsval = "";
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    stsval += "" + taglist[i] + ",";
                                }
                                tbl.custcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                            }
                            else
                            {
                                tbl.custcategoryid = "";
                            }
                        }
                        else
                        {
                            tbl.custcategoryid = "";
                        }

                        tbl.cmpcode = CompnyCode;
                        tbl.custoid = mstoid;
                        tbl.custcode = generateCodeCust();
                        tbl.custcategory = "";
                        tbl.custname = dtlist[0].custname;
                        tbl.custaddr = dtlist[0].custaddr;
                        tbl.custprovinceoid = dtlist[0].custprovinceoid;
                        tbl.custcityoid = dtlist[0].custcityoid;
                        tbl.custpaymentoid = dtlist[0].custpaymentoid;
                        tbl.custtaxable = dtlist[0].custtaxable;
                        tbl.custnpwp = dtlist[0].custnpwp ?? "";
                        tbl.custnpwpno = dtlist[0].custnpwpno ?? "";
                        tbl.custnpwpaddr = dtlist[0].custnpwpaddr ?? "";
                        tbl.custphone1 = dtlist[0].custphone1 ?? "";
                        tbl.custphone2 = "";
                        tbl.custfax1 = "";
                        tbl.custemail = dtlist[0].custemail ?? "";
                        tbl.custnote = "";
                        tbl.createtime = servertime;
                        tbl.createuser = dtlist[0].usoid;
                        tbl.custpostcode = dtlist[0].custpostcode ?? "";
                        tbl.custktpimg = "";
                        tbl.custnpwpimg = "";
                        tbl.custcreditlimit = 0;
                        tbl.custcreditusage = 0;
                        tbl.activeflag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = dtlist[0].usoid;
                        tbl.custcategory2 = "Corporate";
                        db.QL_mstcust.Add(tbl);
                        db.SaveChanges();

                        //Update lastoid
                        sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcust'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        QL_m01US tblus = new QL_m01US();
                        tblus.cmpcode = CompnyCode;
                        tblus.usoid = dtlist[0].usoid;
                        tblus.usname = dtlist[0].custname;
                        tblus.uspassword = dtlist[0].uspassword;
                        tblus.ustype = "cust";
                        tblus.usflag = "ACTIVE";
                        tblus.usemail = "";
                        tblus.stoid = 0;
                        tblus.usphone = "";
                        tblus.crttime = servertime;
                        tblus.crtuser = dtlist[0].usoid;
                        tblus.updtime = servertime;
                        tblus.upduser = dtlist[0].usoid;
                        tblus.expmastertype = "No";
                        tblus.suppcustoid = tbl.custoid;
                        db.QL_m01US.Add(tblus);
                        db.SaveChanges();

                        //insert cat detail
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    int catid = int.Parse(taglist[i]);

                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstcust";
                                    tblcatdtl.catdtlrefoid = tbl.custoid;
                                    tblcatdtl.catoid = catid;
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        mstoid = tbl.custoid;

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }  
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustomerByID(string UserID)
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listcustomer> data = new List<listcustomer>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (msg == "")
            {
                try
                {
                    sSql = "SELECT usoid, uspassword, custoid, custcode, custname, custaddr, custprovinceoid, custcityoid, custpaymentoid, custpostcode, custtaxable, custnpwp, custnpwpno, custnpwpaddr, custphone1, custemail, custcategoryid FROM QL_m01US us INNER JOIN QL_mstcust c ON c.custoid=us.suppcustoid WHERE usflag='ACTIVE' and c.activeflag='ACTIVE' and ustype='cust' and usoid = '"+ UserID + "' " + sPlus + "";
                    data = db.Database.SqlQuery<listcustomer>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostCustomerByID(string UserID, string dt, string tagkategori)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listcustomer>();
            var taglist = new List<string>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listcustomer>>(dt);
                taglist = JsonConvert.DeserializeObject<List<string>>(tagkategori);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    var usoidlist = dtlist[0].usoid;

                    if (string.IsNullOrEmpty(dtlist[0].usoid))
                        msg += "- User ID harap di isi";
                    else if (db.QL_m01US.Where(w => w.usoid == usoidlist).Count() > 0)
                        msg += "- This User ID already use, Please Fill Another User ID!!";
                    if (string.IsNullOrEmpty(dtlist[0].uspassword))
                        msg += "- Password harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custname))
                        msg += "- Nama Customer harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custaddr))
                        msg += "- Alamat harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custphone1))
                        msg += "- No Telp harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custemail))
                        msg += "- Email harap di isi";
                    if (tagkategori == null)
                        msg += "- Kategori harap di isi";
                }
            }

            if (msg == "")
            {
                var custID = db.QL_m01US.Where(x => x.usoid == UserID).Select(y => y.suppcustoid).FirstOrDefault();
                var tbl = db.QL_mstcust.Where(x => x.custoid == custID).FirstOrDefault();
                var tblus = db.QL_m01US.Where(x => x.usoid == UserID).FirstOrDefault();

                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                string stsval = "";
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    stsval += "" + taglist[i] + ",";
                                }
                                tbl.custcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                            }
                            else
                            {
                                tbl.custcategoryid = "";
                            }
                        }
                        else
                        {
                            tbl.custcategoryid = "";
                        }

                        tbl.cmpcode = CompnyCode;
                        tbl.custoid = mstoid;
                        tbl.custcode = generateCodeCust();
                        tbl.custcategory = "";
                        tbl.custname = dtlist[0].custname;
                        tbl.custaddr = dtlist[0].custaddr;
                        tbl.custprovinceoid = dtlist[0].custprovinceoid;
                        tbl.custcityoid = dtlist[0].custcityoid;
                        tbl.custpaymentoid = dtlist[0].custpaymentoid;
                        tbl.custtaxable = dtlist[0].custtaxable;
                        tbl.custnpwp = dtlist[0].custnpwp ?? "";
                        tbl.custnpwpno = dtlist[0].custnpwpno ?? "";
                        tbl.custnpwpaddr = dtlist[0].custnpwpaddr ?? "";
                        tbl.custphone1 = dtlist[0].custphone1 ?? "";
                        tbl.custphone2 = "";
                        tbl.custfax1 = "";
                        tbl.custemail = dtlist[0].custemail ?? "";
                        tbl.custnote = "";
                        tbl.createtime = servertime;
                        tbl.createuser = dtlist[0].usoid;
                        tbl.custpostcode = dtlist[0].custpostcode ?? "";
                        tbl.custktpimg = "";
                        tbl.custnpwpimg = "";
                        tbl.custcreditlimit = 0;
                        tbl.custcreditusage = 0;
                        tbl.activeflag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = dtlist[0].usoid;
                        tbl.custcategory2 = "Corporate";
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();
                       
                        tblus.cmpcode = CompnyCode;
                        tblus.usoid = dtlist[0].usoid;
                        tblus.usname = dtlist[0].custname;
                        tblus.uspassword = dtlist[0].uspassword;
                        tblus.ustype = "cust";
                        tblus.usflag = "ACTIVE";
                        tblus.usemail = "";
                        tblus.stoid = 0;
                        tblus.usphone = "";
                        tblus.crttime = servertime;
                        tblus.crtuser = dtlist[0].usoid;
                        tblus.updtime = servertime;
                        tblus.upduser = dtlist[0].usoid;
                        tblus.expmastertype = "No";
                        tblus.suppcustoid = tbl.custoid;
                        db.Entry(tblus).State = EntityState.Modified;
                        db.SaveChanges();

                        var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tbl.custoid && a.catdtltype == "QL_mstcust");
                        db.QL_mstcatdtl.RemoveRange(trncat);
                        db.SaveChanges();

                        //insert cat detail
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    int catid = int.Parse(taglist[i]);

                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstcust";
                                    tblcatdtl.catdtlrefoid = tbl.custoid;
                                    tblcatdtl.catoid = catid;
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        mstoid = tbl.custoid;

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, UserID, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listalamatcust
        {
            public int custdtl2oid { get; set; }
            public int custoid { get; set; }
            public string custdtl2loc { get; set; }
            public string custdtl2addr { get; set; }
            public int custdtl2cityoid { get; set; }
            public string custdtl2postcode { get; set; }
            public string custdtl2picitem { get; set; }
            public string custdtl2phone { get; set; }
            public string custdtl2email { get; set; }
        }

        [HttpPost]
        public ActionResult PostAlamatCustomer(string custID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listalamatcust>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listalamatcust>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlist.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlist[i].custdtl2loc))
                            msg += "- Lokasi harap di isi";
                        if (string.IsNullOrEmpty(dtlist[i].custdtl2addr))
                            msg += "- Alamat harap di isi";
                    }
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                int id = int.Parse(custID);
                var UserID = db.QL_m01US.Where(x => x.suppcustoid == id).Select(y => y.usoid).FirstOrDefault();
                mstoid = ClassFunction.GenerateID("QL_mstcustdtl2");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcustdtl2 tbl;

                        for (int i = 0; i < dtlist.Count(); i++)
                        {
                            tbl = new QL_mstcustdtl2();
                            tbl.cmpcode = CompnyCode;
                            tbl.custdtl2oid = mstoid++;
                            tbl.custdtl2seq = i + 1;
                            tbl.custoid = int.Parse(custID);
                            tbl.custdtl2loc = dtlist[i].custdtl2loc;
                            tbl.custdtl2addr = dtlist[i].custdtl2addr;
                            tbl.custdtl2picitem = dtlist[i].custdtl2picitem ?? "";
                            tbl.custdtl2phone1 = dtlist[i].custdtl2phone ?? "";
                            tbl.custdtl2phone2 = "";
                            tbl.custdtl2email = dtlist[i].custdtl2email ?? "";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.QL_mstcustdtl2.Add(tbl);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (mstoid - 1) + " Where tablename = 'QL_mstcustdtl2'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        mstoid = int.Parse(custID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatCustomerByID(string UserID)
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listalamatcust> data = new List<listalamatcust>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (msg == "")
            {
                try
                {
                    sSql = "SELECT custdtl2oid, c.custoid, custdtl2loc, custdtl2addr, custdtl2cityoid, custdtl2postcode, custdtl2picitem, custdtl2phone1 custdtl2phone, custdtl2email FROM QL_m01US us INNER JOIN QL_mstcust c ON c.custoid=us.suppcustoid  INNER JOIN QL_mstcustdtl2 cd ON cd.custoid=c.custoid WHERE usflag='ACTIVE' and c.activeflag='ACTIVE' and ustype='cust' and usoid = '" + UserID + "' " + sPlus + "";
                    data = db.Database.SqlQuery<listalamatcust>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostAlamatCustomerByID(string UserID, string custDtlID, string custID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listalamatcust>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (string.IsNullOrEmpty(custDtlID))
                msg += "- ID Detail harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listalamatcust>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    if (string.IsNullOrEmpty(dtlist[0].custdtl2loc))
                        msg += "- Lokasi harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custdtl2addr))
                        msg += "- Alamat harap di isi";
                }
            }

            if (msg == "")
            {
                int id = int.Parse(custDtlID);
                var tbl = db.QL_mstcustdtl2.Where(x => x.custdtl2oid == id).FirstOrDefault();

                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        tbl.custdtl2oid = mstoid++;
                        tbl.custoid = int.Parse(custID);
                        tbl.custdtl2loc = dtlist[0].custdtl2loc;
                        tbl.custdtl2addr = dtlist[0].custdtl2addr;
                        tbl.custdtl2picitem = dtlist[0].custdtl2picitem ?? "";
                        tbl.custdtl2phone1 = dtlist[0].custdtl2phone ?? "";
                        tbl.custdtl2phone2 = "";
                        tbl.custdtl2email = dtlist[0].custdtl2email ?? "";
                        tbl.updtime = servertime;
                        tbl.upduser = UserID;
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                        mstoid = int.Parse(custID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, UserID, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listpiccust
        {
            public int custdtl1oid { get; set; }
            public int custoid { get; set; }
            public string custdtl1picname { get; set; }
            public string custdtl1phone { get; set; }
            public string custdtl1email { get; set; }
            public string custdtl1jabatan { get; set; }
        }

        [HttpPost]
        public ActionResult PostPICCustomer(string custID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listpiccust>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listpiccust>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlist.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlist[i].custdtl1picname))
                            msg += "- Nama PIC harap di isi";
                        if (string.IsNullOrEmpty(dtlist[i].custdtl1phone))
                            msg += "- No Telp harap di isi";
                    }
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                int id = int.Parse(custID);
                var UserID = db.QL_m01US.Where(x => x.suppcustoid == id).Select(y => y.usoid).FirstOrDefault();
                mstoid = ClassFunction.GenerateID("QL_mstcustdtl1");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcustdtl1 tbl;

                        for (int i = 0; i < dtlist.Count(); i++)
                        {
                            tbl = new QL_mstcustdtl1();
                            tbl.cmpcode = CompnyCode;
                            tbl.custdtl1oid = mstoid++;
                            tbl.custdtl1seq = i + 1;
                            tbl.custoid = int.Parse(custID);
                            tbl.custdtl1picname = dtlist[i].custdtl1picname;
                            tbl.custdtl1phone1 = dtlist[i].custdtl1phone;
                            tbl.custdtl1phone2 = "";
                            tbl.custdtl1jabatan = dtlist[i].custdtl1jabatan ?? "";
                            tbl.custdtl1email = dtlist[i].custdtl1email ?? "";
                            tbl.custdtl1flag = "ACTIVE";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.QL_mstcustdtl1.Add(tbl);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (mstoid - 1) + " Where tablename = 'QL_mstcustdtl1'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        mstoid = int.Parse(custID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPICCustomerByID(string UserID)
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listpiccust> data = new List<listpiccust>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (msg == "")
            {
                try
                {
                    sSql = "SELECT custdtl1oid, c.custoid, custdtl1picname, custdtl1phone1 custdtl1phone, custdtl1email, custdtl1jabatan FROM QL_m01US us INNER JOIN QL_mstcust c ON c.custoid=us.suppcustoid  INNER JOIN QL_mstcustdtl1 cd ON cd.custoid=c.custoid WHERE usflag='ACTIVE' and c.activeflag='ACTIVE' and ustype='cust' and usoid = '" + UserID + "' " + sPlus + "";
                    data = db.Database.SqlQuery<listpiccust>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostPICCustomerByID(string UserID, string custDtlID, string custID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listpiccust>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (string.IsNullOrEmpty(custDtlID))
                msg += "- ID Detail harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listpiccust>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    if (string.IsNullOrEmpty(dtlist[0].custdtl1picname))
                        msg += "- Nama PIC harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].custdtl1phone))
                        msg += "- No Telp harap di isi";
                }
            }

            if (msg == "")
            {
                int id = int.Parse(custDtlID);
                var tbl = db.QL_mstcustdtl1.Where(x => x.custdtl1oid == id).FirstOrDefault();

                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        tbl.cmpcode = CompnyCode;
                        tbl.custdtl1oid = mstoid++;
                        tbl.custoid = int.Parse(custID);
                        tbl.custdtl1picname = dtlist[0].custdtl1picname;
                        tbl.custdtl1phone1 = dtlist[0].custdtl1phone;
                        tbl.custdtl1phone2 = "";
                        tbl.custdtl1jabatan = dtlist[0].custdtl1jabatan ?? "";
                        tbl.custdtl1email = dtlist[0].custdtl1email ?? "";
                        tbl.custdtl1flag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = UserID;
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                        mstoid = int.Parse(custID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, UserID, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private string generateCodeSupp()
        {
            string sCode = "SUPP-";
            int formatCounter = 5;
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(SuppCode, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_mstSupp WHERE cmpcode='" + CompnyCode + "' AND SuppCode LIKE '" + sCode + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sCode += sCounter;
            return sCode;
        }

        public class listsupplier
        {
            public string usoid { get; set; }
            public string uspassword { get; set; }
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public int suppprovinceoid { get; set; }
            public int suppcityoid { get; set; }
            public string supppostcode { get; set; }
            public int supppaymentoid { get; set; }
            public int supppajak { get; set; }
            public string suppnpwp { get; set; }
            public string suppnpwpno { get; set; }
            public string suppnpwpaddr { get; set; }
            public string suppphone1 { get; set; }
            public string suppemail { get; set; }
            public string supppengakuan { get; set; }
            public string suppsurgan { get; set; }
            public string suppgaransi { get; set; }
            public string suppcategoryid { get; set; }
        }

        [HttpPost]
        public ActionResult PostSupplierAll(string dt, string tagkategori, string dtalamat, string dtpic)
        {
            var result = "sukses"; var msg = "";
            var mstoid = 0; var alamatdtloid = 0; var picdtloid = 0;
            var dtlist = new List<listsupplier>();
            var taglist = new List<string>();
            var dtlistalamat = new List<listalamatsupp>();
            var dtlistpic = new List<listpicsupp>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listsupplier>>(dt);
                taglist = JsonConvert.DeserializeObject<List<string>>(tagkategori);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    var usoidlist = dtlist[0].usoid;

                    if (string.IsNullOrEmpty(dtlist[0].usoid))
                        msg += "- User ID harap di isi";
                    else if (db.QL_m01US.Where(w => w.usoid == usoidlist).Count() > 0)
                        msg += "- This User ID already use, Please Fill Another User ID!!";
                    if (string.IsNullOrEmpty(dtlist[0].uspassword))
                        msg += "- Password harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppname))
                        msg += "- Nama Supplier harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppaddr))
                        msg += "- Alamat harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppphone1))
                        msg += "- No Telp harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppemail))
                        msg += "- Email harap di isi";
                    if (tagkategori == null)
                        msg += "- Kategori harap di isi";
                }
            }

            //alamat
            if (string.IsNullOrEmpty(dtalamat))
            {
                msg += "- Data Alamat Kosong!";
            }
            else
            {
                dtlistalamat = JsonConvert.DeserializeObject<List<listalamatsupp>>(dtalamat);

                if (dtlistalamat.Count <= 0)
                {
                    msg += "- Data Alamat Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlistalamat.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlistalamat[i].suppdtl1addr))
                            msg += "- Nama PIC harap di isi";
                        if (string.IsNullOrEmpty(dtlistalamat[i].suppdtl1phone))
                            msg += "- No Telp harap di isi";
                    }
                }
            }

            //pic
            if (string.IsNullOrEmpty(dtpic))
            {
                msg += "- Data PIC Kosong!";
            }
            else
            {
                dtlistpic = JsonConvert.DeserializeObject<List<listpicsupp>>(dtpic);

                if (dtlistpic.Count <= 0)
                {
                    msg += "- Data PIC Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlistpic.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlistpic[i].suppdtl3picname))
                            msg += "- Nama PIC harap di isi";
                        if (string.IsNullOrEmpty(dtlistpic[i].suppdtl3phone))
                            msg += "- No Telp harap di isi";
                    }
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                mstoid = ClassFunction.GenerateID("QL_mstsupp");
                alamatdtloid = ClassFunction.GenerateID("QL_mstsuppdtl1");
                picdtloid = ClassFunction.GenerateID("QL_mstsuppdtl3");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstsupp tbl = new QL_mstsupp();

                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                string stsval = "";
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    stsval += "" + taglist[i] + ",";
                                }
                                tbl.suppcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                            }
                            else
                            {
                                tbl.suppcategoryid = "";
                            }
                        }
                        else
                        {
                            tbl.suppcategoryid = "";
                        }

                        tbl.cmpcode = CompnyCode;
                        tbl.suppoid = mstoid;
                        tbl.suppcode = generateCodeSupp();
                        tbl.suppname = dtlist[0].suppname;
                        tbl.suppaddr = dtlist[0].suppaddr;
                        tbl.suppprovinceoid = dtlist[0].suppprovinceoid;
                        tbl.suppcityOid = dtlist[0].suppcityoid;
                        tbl.supppaymentoid = dtlist[0].supppaymentoid;
                        tbl.supppajak = dtlist[0].supppajak.ToString();
                        tbl.suppnpwp = dtlist[0].suppnpwp ?? "";
                        tbl.suppnpwpno = dtlist[0].suppnpwpno ?? "";
                        tbl.suppnpwpaddr = dtlist[0].suppnpwpaddr ?? "";
                        tbl.suppphone1 = dtlist[0].suppphone1 ?? "";
                        tbl.suppphone2 = "";
                        tbl.suppfax1 = "";
                        tbl.suppemail = dtlist[0].suppemail ?? "";
                        tbl.suppnote = "";
                        tbl.createtime = servertime;
                        tbl.createuser = dtlist[0].usoid;
                        tbl.supppostcode = dtlist[0].supppostcode ?? "";
                        tbl.suppcategory = "";
                        tbl.suppktpimg = "";
                        tbl.suppnpwpimg = "";
                        tbl.activeflag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = dtlist[0].usoid;
                        tbl.supppengakuan = dtlist[0].supppengakuan;
                        tbl.suppsurgan = dtlist[0].suppsurgan;
                        tbl.suppgaransi = dtlist[0].suppgaransi ?? "";
                        tbl.supptype = "Hutang";
                        db.QL_mstsupp.Add(tbl);
                        db.SaveChanges();

                        //Update lastoid
                        sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstsupp'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        QL_m01US tblus = new QL_m01US();
                        tblus.cmpcode = CompnyCode;
                        tblus.usoid = dtlist[0].usoid;
                        tblus.usname = dtlist[0].suppname;
                        tblus.uspassword = dtlist[0].uspassword;
                        tblus.ustype = "supp";
                        tblus.usflag = "ACTIVE";
                        tblus.usemail = "";
                        tblus.stoid = 0;
                        tblus.usphone = "";
                        tblus.crttime = servertime;
                        tblus.crtuser = dtlist[0].usoid;
                        tblus.updtime = servertime;
                        tblus.upduser = dtlist[0].usoid;
                        tblus.expmastertype = "No";
                        tblus.suppcustoid = tbl.suppoid;
                        db.QL_m01US.Add(tblus);
                        db.SaveChanges();

                        //insert cat detail
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    int catid = int.Parse(taglist[i]);

                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstsupp";
                                    tblcatdtl.catdtlrefoid = tbl.suppoid;
                                    tblcatdtl.catoid = catid;
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        mstoid = tbl.suppoid;

                        //alamat
                        QL_mstsuppdtl1 tbl2;
                        for (int i = 0; i < dtlistalamat.Count(); i++)
                        {
                            tbl2 = new QL_mstsuppdtl1();
                            tbl2.cmpcode = CompnyCode;
                            tbl2.suppdtl1oid = alamatdtloid++;
                            tbl2.suppdtl1seq = i + 1;
                            tbl2.suppoid = mstoid;
                            tbl2.suppdtl1addr = dtlistalamat[i].suppdtl1addr;
                            tbl2.suppdtl1phone = dtlistalamat[i].suppdtl1phone;
                            tbl2.suppcityoid = dtlistalamat[i].suppdtl1cityoid;
                            tbl2.suppdtl1status = "ACTIVE";
                            tbl2.updtime = servertime;
                            tbl2.upduser = dtlist[0].usoid;
                            db.QL_mstsuppdtl1.Add(tbl2);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (alamatdtloid - 1) + " Where tablename = 'QL_mstsuppdtl1'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        //pic
                        QL_mstsuppdtl3 tbl3;
                        for (int i = 0; i < dtlistpic.Count(); i++)
                        {
                            tbl3 = new QL_mstsuppdtl3();
                            tbl3.cmpcode = CompnyCode;
                            tbl3.suppdtl3oid = picdtloid++;
                            tbl3.suppdtl3seq = i + 1;
                            tbl3.suppoid = mstoid;
                            tbl3.suppdtl3picname = dtlistpic[i].suppdtl3picname;
                            tbl3.suppdtl3phone1 = dtlistpic[i].suppdtl3phone;
                            tbl3.suppdtl3phone2 = "";
                            tbl3.suppdtl3email = dtlistpic[i].suppdtl3email ?? "";
                            tbl3.suppdtl3jabatan = dtlistpic[i].suppdtl3jabatan ?? "";
                            tbl3.suppdtl3picproduk = "";
                            tbl3.updtime = servertime;
                            tbl3.upduser = dtlist[0].usoid;
                            db.QL_mstsuppdtl3.Add(tbl3);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (picdtloid - 1) + " Where tablename = 'QL_mstsuppdtl3'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostSupplier(string dt, string tagkategori)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listsupplier>();
            var taglist = new List<string>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listsupplier>>(dt);
                taglist = JsonConvert.DeserializeObject<List<string>>(tagkategori);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    var usoidlist = dtlist[0].usoid;

                    if (string.IsNullOrEmpty(dtlist[0].usoid))
                        msg += "- User ID harap di isi";
                    else if (db.QL_m01US.Where(w => w.usoid == usoidlist).Count() > 0)
                        msg += "- This User ID already use, Please Fill Another User ID!!";
                    if (string.IsNullOrEmpty(dtlist[0].uspassword))
                        msg += "- Password harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppname))
                        msg += "- Nama Supplier harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppaddr))
                        msg += "- Alamat harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppphone1))
                        msg += "- No Telp harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppemail))
                        msg += "- Email harap di isi";
                    if (tagkategori == null)
                        msg += "- Kategori harap di isi";
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                mstoid = ClassFunction.GenerateID("QL_mstsupp");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstsupp tbl = new QL_mstsupp();

                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                string stsval = "";
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    stsval += "" + taglist[i] + ",";
                                }
                                tbl.suppcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                            }
                            else
                            {
                                tbl.suppcategoryid = "";
                            }
                        }
                        else
                        {
                            tbl.suppcategoryid = "";
                        }

                        tbl.cmpcode = CompnyCode;
                        tbl.suppoid = mstoid;
                        tbl.suppcode = generateCodeSupp();
                        tbl.suppname = dtlist[0].suppname;
                        tbl.suppaddr = dtlist[0].suppaddr;
                        tbl.suppprovinceoid = dtlist[0].suppprovinceoid;
                        tbl.suppcityOid = dtlist[0].suppcityoid;
                        tbl.supppaymentoid = dtlist[0].supppaymentoid;
                        tbl.supppajak = dtlist[0].supppajak.ToString();
                        tbl.suppnpwp = dtlist[0].suppnpwp ?? "";
                        tbl.suppnpwpno = dtlist[0].suppnpwpno ?? "";
                        tbl.suppnpwpaddr = dtlist[0].suppnpwpaddr ?? "";
                        tbl.suppphone1 = dtlist[0].suppphone1 ?? "";
                        tbl.suppphone2 = "";
                        tbl.suppfax1 = "";
                        tbl.suppemail = dtlist[0].suppemail ?? "";
                        tbl.suppnote = "";
                        tbl.createtime = servertime;
                        tbl.createuser = dtlist[0].usoid;
                        tbl.supppostcode = dtlist[0].supppostcode ?? "";
                        tbl.suppcategory = "";
                        tbl.suppktpimg = "";
                        tbl.suppnpwpimg = "";
                        tbl.activeflag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = dtlist[0].usoid;
                        tbl.supppengakuan = dtlist[0].supppengakuan;
                        tbl.suppsurgan = dtlist[0].suppsurgan;
                        tbl.suppgaransi = dtlist[0].suppgaransi ?? "";
                        tbl.supptype = "Hutang";
                        db.QL_mstsupp.Add(tbl);
                        db.SaveChanges();

                        //Update lastoid
                        sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstsupp'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        QL_m01US tblus = new QL_m01US();
                        tblus.cmpcode = CompnyCode;
                        tblus.usoid = dtlist[0].usoid;
                        tblus.usname = dtlist[0].suppname;
                        tblus.uspassword = dtlist[0].uspassword;
                        tblus.ustype = "supp";
                        tblus.usflag = "ACTIVE";
                        tblus.usemail = "";
                        tblus.stoid = 0;
                        tblus.usphone = "";
                        tblus.crttime = servertime;
                        tblus.crtuser = dtlist[0].usoid;
                        tblus.updtime = servertime;
                        tblus.upduser = dtlist[0].usoid;
                        tblus.expmastertype = "No";
                        tblus.suppcustoid = tbl.suppoid;
                        db.QL_m01US.Add(tblus);
                        db.SaveChanges();

                        //insert cat detail
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    int catid = int.Parse(taglist[i]);

                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstsupp";
                                    tblcatdtl.catdtlrefoid = tbl.suppoid;
                                    tblcatdtl.catoid = catid;
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        mstoid = tbl.suppoid;

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetSupplierByID(string UserID)
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listsupplier> data = new List<listsupplier>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (msg == "")
            {
                try
                {
                    sSql = "SELECT usoid, uspassword, suppoid, suppcode, suppname, suppaddr, suppprovinceoid, suppcityoid, supppaymentoid, supppostcode, CAST(supppajak AS INTEGER) supppajak, suppnpwp, suppnpwpno, suppnpwpaddr, suppphone1, suppemail, suppcategoryid, supppengakuan, suppsurgan, suppgaransi FROM QL_m01US us INNER JOIN QL_mstsupp c ON c.suppoid=us.suppcustoid WHERE usflag='ACTIVE' and c.activeflag='ACTIVE' and ustype='supp' and usoid = '" + UserID + "' " + sPlus + "";
                    data = db.Database.SqlQuery<listsupplier>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostSupplierByID(string UserID, string dt, string tagkategori)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listsupplier>();
            var taglist = new List<string>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listsupplier>>(dt);
                taglist = JsonConvert.DeserializeObject<List<string>>(tagkategori);
                
                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    var usoidlist = dtlist[0].usoid;

                    if (string.IsNullOrEmpty(dtlist[0].usoid))
                        msg += "- User ID harap di isi";
                    //else if (db.QL_m01US.Where(w => w.usoid == usoidlist).Count() > 0)
                    //    msg += "- This User ID already use, Please Fill Another User ID!!";
                    if (string.IsNullOrEmpty(dtlist[0].uspassword))
                        msg += "- Password harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppname))
                        msg += "- Nama Supplier harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppaddr))
                        msg += "- Alamat harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppphone1))
                        msg += "- No Telp harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppemail))
                        msg += "- Email harap di isi";
                    if (tagkategori == null)
                        msg += "- Kategori harap di isi";
                }
            }

            if (msg == "")
            {
                var suppID = db.QL_m01US.Where(x => x.usoid == UserID).Select(y => y.suppcustoid).FirstOrDefault();
                var tbl = db.QL_mstsupp.Where(x => x.suppoid == suppID).FirstOrDefault();
                var tblus = db.QL_m01US.Where(x => x.usoid == UserID).FirstOrDefault();

                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                string stsval = "";
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    stsval += "" + taglist[i] + ",";
                                }
                                tbl.suppcategoryid = ClassFunction.Left(stsval, stsval.Length - 1);
                            }
                            else
                            {
                                tbl.suppcategoryid = "";
                            }
                        }
                        else
                        {
                            tbl.suppcategoryid = "";
                        }

                        //tbl.cmpcode = CompnyCode;
                        //tbl.suppoid = mstoid;
                        //tbl.suppcode = generateCodeSupp();
                        tbl.suppcategory = "";
                        tbl.suppname = dtlist[0].suppname;
                        tbl.suppaddr = dtlist[0].suppaddr;
                        tbl.suppprovinceoid = dtlist[0].suppprovinceoid;
                        tbl.suppcityOid = dtlist[0].suppcityoid;
                        tbl.supppaymentoid = dtlist[0].supppaymentoid;
                        tbl.supppajak = dtlist[0].supppajak.ToString();
                        tbl.suppnpwp = dtlist[0].suppnpwp ?? "";
                        tbl.suppnpwpno = dtlist[0].suppnpwpno ?? "";
                        tbl.suppnpwpaddr = dtlist[0].suppnpwpaddr ?? "";
                        tbl.suppphone1 = dtlist[0].suppphone1 ?? "";
                        tbl.suppphone2 = "";
                        tbl.suppfax1 = "";
                        tbl.suppemail = dtlist[0].suppemail ?? "";
                        tbl.suppnote = "";
                        tbl.createtime = servertime;
                        tbl.createuser = dtlist[0].usoid;
                        tbl.supppostcode = dtlist[0].supppostcode ?? "";
                        tbl.suppktpimg = "";
                        tbl.suppnpwpimg = "";
                        tbl.activeflag = "ACTIVE";
                        tbl.updtime = servertime;
                        tbl.upduser = dtlist[0].usoid;
                        tbl.supppengakuan = dtlist[0].supppengakuan;
                        tbl.suppsurgan = dtlist[0].suppsurgan;
                        tbl.suppgaransi = dtlist[0].suppgaransi ?? "";
                        tbl.supptype = "Hutang";
                        db.Entry(tbl).State = EntityState.Modified;
                        db.SaveChanges();

                        //tblus.cmpcode = CompnyCode;
                        //tblus.usoid = dtlist[0].usoid;
                        tblus.usname = dtlist[0].suppname;
                        tblus.uspassword = dtlist[0].uspassword;
                        tblus.ustype = "supp";
                        tblus.usflag = "ACTIVE";
                        tblus.usemail = "";
                        tblus.stoid = 0;
                        tblus.usphone = "";
                        tblus.crttime = servertime;
                        tblus.crtuser = dtlist[0].usoid;
                        tblus.updtime = servertime;
                        tblus.upduser = dtlist[0].usoid;
                        tblus.expmastertype = "No";
                        tblus.suppcustoid = tbl.suppoid;
                        db.Entry(tblus).State = EntityState.Modified;
                        db.SaveChanges();

                        var trncat = db.QL_mstcatdtl.Where(a => a.catdtlrefoid == tbl.suppoid && a.catdtltype == "QL_mstsupp");
                        db.QL_mstcatdtl.RemoveRange(trncat);
                        db.SaveChanges();

                        //insert cat detail
                        if (taglist != null)
                        {
                            if (taglist.Count() > 0)
                            {
                                QL_mstcatdtl tblcatdtl;
                                for (int i = 0; i < taglist.Count(); i++)
                                {
                                    int catid = int.Parse(taglist[i]);

                                    tblcatdtl = new QL_mstcatdtl();
                                    tblcatdtl.catdtltype = "QL_mstsupp";
                                    tblcatdtl.catdtlrefoid = tbl.suppoid;
                                    tblcatdtl.catoid = catid;
                                    db.QL_mstcatdtl.Add(tblcatdtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        mstoid = tbl.suppoid;

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, UserID, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listalamatsupp
        {
            public int suppdtl1oid { get; set; }
            public int suppoid { get; set; }
            public string suppdtl1addr { get; set; }
            public int suppdtl1cityoid { get; set; }
            public string citydesc { get; set; }
            public string suppdtl1phone { get; set; }
        }

        [HttpPost]
        public ActionResult PostAlamatSupplier(string suppID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listalamatsupp>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listalamatsupp>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlist.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlist[i].suppdtl1addr))
                            msg += "- Nama PIC harap di isi";
                        if (string.IsNullOrEmpty(dtlist[i].suppdtl1phone))
                            msg += "- No Telp harap di isi";
                    }
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                int id = int.Parse(suppID);
                var UserID = db.QL_m01US.Where(x => x.suppcustoid == id).Select(y => y.usoid).FirstOrDefault();
                mstoid = ClassFunction.GenerateID("QL_mstsuppdtl1");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstsuppdtl1 tbl;

                        for (int i = 0; i < dtlist.Count(); i++)
                        {
                            tbl = new QL_mstsuppdtl1();
                            tbl.cmpcode = CompnyCode;
                            tbl.suppdtl1oid = mstoid++;
                            tbl.suppdtl1seq = i + 1;
                            tbl.suppoid = int.Parse(suppID);
                            tbl.suppdtl1addr = dtlist[i].suppdtl1addr;
                            tbl.suppdtl1phone = dtlist[i].suppdtl1phone;
                            tbl.suppcityoid = dtlist[i].suppdtl1cityoid;
                            tbl.suppdtl1status = "ACTIVE";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.QL_mstsuppdtl1.Add(tbl);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (mstoid - 1) + " Where tablename = 'QL_mstsuppdtl1'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        mstoid = int.Parse(suppID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetAlamatSupplierByID(string UserID)
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listalamatsupp> data = new List<listalamatsupp>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (msg == "")
            {
                try
                {
                    sSql = "SELECT suppdtl1oid, c.suppoid, suppdtl1addr, cd.suppcityoid suppdtl1cityoid, ISNULL((SELECT g.gndesc FROM QL_m05GN g WHERE g.gnoid=cd.suppcityoid),'') citydesc, suppdtl1phone FROM QL_m01US us INNER JOIN QL_mstsupp c ON c.suppoid=us.suppcustoid  INNER JOIN QL_mstsuppdtl1 cd ON cd.suppoid=c.suppoid WHERE usflag='ACTIVE' and c.activeflag='ACTIVE' and ustype='supp' and usoid = '" + UserID + "' " + sPlus + "";
                    data = db.Database.SqlQuery<listalamatsupp>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostAlamatSupplierByID(string UserID, string suppDtlID, string suppID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listalamatsupp>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (string.IsNullOrEmpty(suppDtlID))
                msg += "- ID Detail harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listalamatsupp>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    if (string.IsNullOrEmpty(dtlist[0].suppdtl1addr))
                        msg += "- Nama PIC harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppdtl1phone))
                        msg += "- No Telp harap di isi";
                }
            }

            if (msg == "")
            {
                int id = int.Parse(suppDtlID);
                var servertime = ClassFunction.GetServerTime();
                var dtloid = ClassFunction.GenerateID("QL_mstsuppdtl1");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (id > 0)
                        {
                            var tbl = db.QL_mstsuppdtl1.Where(x => x.suppdtl1oid == id).FirstOrDefault();

                            //tbl.cmpcode = CompnyCode;
                            //tbl.suppdtl1oid = mstoid++;
                            //tbl.suppoid = int.Parse(suppID);
                            tbl.suppdtl1addr = dtlist[0].suppdtl1addr;
                            tbl.suppdtl1phone = dtlist[0].suppdtl1phone;
                            tbl.suppcityoid = dtlist[0].suppdtl1cityoid;
                            tbl.suppdtl1status = "ACTIVE";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            QL_mstsuppdtl1 tbl;
                            tbl = new QL_mstsuppdtl1();
                            tbl.cmpcode = CompnyCode;
                            tbl.suppdtl1oid = dtloid++;
                            tbl.suppoid = int.Parse(suppID);
                            tbl.suppdtl1addr = dtlist[0].suppdtl1addr;
                            tbl.suppdtl1phone = dtlist[0].suppdtl1phone;
                            tbl.suppcityoid = dtlist[0].suppdtl1cityoid;
                            tbl.suppdtl1status = "ACTIVE";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.QL_mstsuppdtl1.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (dtloid - 1) + " Where tablename = 'QL_mstsuppdtl1'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        mstoid = int.Parse(suppID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listpicsupp
        {
            public int suppdtl3oid { get; set; }
            public string suppdtl3picname { get; set; }
            public string suppdtl3phone { get; set; }
            public string suppdtl3email { get; set; }
            public string suppdtl3jabatan { get; set; }
        }

        [HttpPost]
        public ActionResult PostPICSupplier(string suppID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listpicsupp>();

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listpicsupp>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    for (int i = 0; i < dtlist.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtlist[i].suppdtl3picname))
                            msg += "- Nama PIC harap di isi";
                        if (string.IsNullOrEmpty(dtlist[i].suppdtl3phone))
                            msg += "- No Telp harap di isi";
                    }
                }
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();
                int id = int.Parse(suppID);
                var UserID = db.QL_m01US.Where(x => x.suppcustoid == id).Select(y => y.usoid).FirstOrDefault();
                mstoid = ClassFunction.GenerateID("QL_mstsuppdtl3");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstsuppdtl3 tbl;

                        for (int i = 0; i < dtlist.Count(); i++)
                        {
                            tbl = new QL_mstsuppdtl3();
                            tbl.cmpcode = CompnyCode;
                            tbl.suppdtl3oid = mstoid++;
                            tbl.suppdtl3seq = i + 1;
                            tbl.suppoid = int.Parse(suppID);
                            tbl.suppdtl3picname = dtlist[i].suppdtl3picname;
                            tbl.suppdtl3phone1 = dtlist[i].suppdtl3phone;
                            tbl.suppdtl3phone2 = "";
                            tbl.suppdtl3email = dtlist[i].suppdtl3email ?? "";
                            tbl.suppdtl3jabatan = dtlist[i].suppdtl3jabatan ?? "";
                            tbl.suppdtl3picproduk = "";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.QL_mstsuppdtl3.Add(tbl);
                            db.SaveChanges();
                        }

                        //Update lastoid
                        sSql = "Update QL_ID set lastoid = " + (mstoid - 1) + " Where tablename = 'QL_mstsuppdtl3'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        mstoid = int.Parse(suppID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPICSupplierByID(string UserID)
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listpicsupp> data = new List<listpicsupp>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (msg == "")
            {
                try
                {
                    sSql = "SELECT suppdtl3oid, c.suppoid, suppdtl3picname, suppdtl3phone1 suppdtl3phone, suppdtl3email, suppdtl3jabatan FROM QL_m01US us INNER JOIN QL_mstsupp c ON c.suppoid=us.suppcustoid  INNER JOIN QL_mstsuppdtl3 cd ON cd.suppoid=c.suppoid WHERE usflag='ACTIVE' and c.activeflag='ACTIVE' and ustype='supp' and usoid = '" + UserID + "' " + sPlus + "";
                    data = db.Database.SqlQuery<listpicsupp>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult PostPICSupplierByID(string UserID, string suppDtlID, string suppID, string dt)
        {
            var result = "sukses"; var msg = ""; var mstoid = 0;
            var dtlist = new List<listpicsupp>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (string.IsNullOrEmpty(suppDtlID))
                msg += "- ID Detail harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listpicsupp>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    if (string.IsNullOrEmpty(dtlist[0].suppdtl3picname))
                        msg += "- Nama PIC harap di isi";
                    if (string.IsNullOrEmpty(dtlist[0].suppdtl3phone))
                        msg += "- No Telp harap di isi";
                }
            }

            if (msg == "")
            {
                int id = int.Parse(suppDtlID);
                var servertime = ClassFunction.GetServerTime();
                var dtloid = ClassFunction.GenerateID("QL_mstsuppdtl3");

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (id > 0)
                        {
                            var tbl = db.QL_mstsuppdtl3.Where(x => x.suppdtl3oid == id).FirstOrDefault();
                            //tbl.cmpcode = CompnyCode;
                            //tbl.suppdtl3oid = mstoid++;
                            //tbl.suppoid = int.Parse(suppID);
                            tbl.suppdtl3picname = dtlist[0].suppdtl3picname;
                            tbl.suppdtl3phone1 = dtlist[0].suppdtl3phone;
                            tbl.suppdtl3phone2 = "";
                            tbl.suppdtl3email = dtlist[0].suppdtl3email ?? "";
                            tbl.suppdtl3jabatan = dtlist[0].suppdtl3jabatan ?? "";
                            tbl.suppdtl3picproduk = "";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            QL_mstsuppdtl3 tbl;
                            tbl = new QL_mstsuppdtl3();
                            tbl.cmpcode = CompnyCode;
                            tbl.suppdtl3oid = dtloid++;
                            tbl.suppoid = int.Parse(suppID);
                            tbl.suppdtl3picname = dtlist[0].suppdtl3picname;
                            tbl.suppdtl3phone1 = dtlist[0].suppdtl3phone;
                            tbl.suppdtl3phone2 = "";
                            tbl.suppdtl3email = dtlist[0].suppdtl3email ?? "";
                            tbl.suppdtl3jabatan = dtlist[0].suppdtl3jabatan ?? "";
                            tbl.suppdtl3picproduk = "";
                            tbl.updtime = servertime;
                            tbl.upduser = UserID;
                            db.QL_mstsuppdtl3.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + (dtloid - 1) + " Where tablename = 'QL_mstsuppdtl3'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        mstoid = int.Parse(suppID);

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, mstoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class liststock
        {
            public string itemdesc { get; set; }
            public string itemimgfile { get; set; }
            public string tampilharga { get; set; }
            public decimal stock { get; set; }
            public decimal hargajual { get; set; }
        }

        [HttpPost]
        public ActionResult GetDisplayStock(string UserID = "", string sFilter = "")
        {
            var result = "sukses"; var msg = ""; string sPlus = "";
            int custoid = 0;  int cekDis = 0; string sType = ""; string tampilharga = "Tidak";
            List<liststock> data = new List<liststock>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }

                custoid = db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "cust").Select(y => y.suppcustoid.Value).FirstOrDefault();
                cekDis = db.Database.SqlQuery<int>("select ds.custoid from ql_mstcatds ds inner join QL_displaystmst dm ON dm.catoid=ds.catoid WHERE ds.custoid = " + custoid + "").Count();
                tampilharga = db.Database.SqlQuery<string>("select dm.displaystlistprice from ql_mstcatds ds inner join QL_displaystmst dm ON dm.catoid=ds.catoid WHERE ds.custoid = " + custoid + "").FirstOrDefault();
                sType = db.Database.SqlQuery<string>("select dm.displaysttype from ql_mstcatds ds inner join QL_displaystmst dm ON dm.catoid=ds.catoid WHERE ds.custoid = " + custoid + "").FirstOrDefault();
            }
            if (!string.IsNullOrEmpty(sFilter))
            {
                sPlus += " AND i.itemdesc LIKE'%"+ sFilter + "%' ";
            }
            if (!string.IsNullOrEmpty(sType))
            {
                if (sType == "Pilih")
                {
                    sPlus += " AND i.itemoid IN(SELECT dd.itemoid FROM QL_displaystdtl dd INNER JOIN QL_displaystmst dm ON dm.displaystoid=dd.displaystoid INNER JOIN QL_mstcatds ds ON ds.catoid=dm.catoid WHERE ds.custoid=" + custoid +") ";
                }
            }

            if (msg == "")
            {
                try
                {
                    if (cekDis > 0)
                    {
                        sSql = "SELECT i.itemdesc, CASE WHEN ISNULL(i.itemimgfile,'') <> '' THEN '" + urlimgsrv + "' + REPLACE(ISNULL(i.itemimgfile,''),'~/','') ELSE ISNULL(i.itemimgfile,'') END itemimgfile, '"+ tampilharga + "' tampilharga, ISNULL((SELECT SUM(qtyin - qtyout) FROM QL_conmat con WHERE con.refoid=i.itemoid),0.0) stock, ISNULL((SELECT TOP 1 sod.soitemprice FROM QL_trnsoitemdtl sod WHERE sod.itemoid=i.itemoid ORDER BY updtime DESC),0.0) hargajual FROM QL_mstitem i WHERE i.activeflag='ACTIVE' " + sPlus + "";
                        data = db.Database.SqlQuery<liststock>(sSql).ToList();
                    }

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listpo
        {
            public int pomstoid { get; set; }
            public string poreftype { get; set; }
            public string pono { get; set; }
            public decimal pototalamt { get; set; }
        }

        [HttpPost]
        public ActionResult GetPO(string UserID = "", string sFilter = "")
        {
            var result = "sukses"; var msg = ""; string sPlus = ""; string sPlus2 = "";
            List<listpo> data = new List<listpo>();
            int suppoid = 0;

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                suppoid = db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Select(y => y.suppcustoid.Value).FirstOrDefault();

                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }
            if (!string.IsNullOrEmpty(sFilter))
            {
                sPlus = " AND poitemno LIKE'%" + sFilter + "%' ";
                sPlus = " AND poassetno LIKE'%" + sFilter + "%' ";
            }

            if (msg == "")
            {
                try
                {

                    sSql = "SELECT poitemmstoid pomstoid, 'QL_trnpoitemmst' poreftype, poitemno pono, poitemgrandtotalamt pototalamt FROM QL_trnpoitemmst WHERE poitemmststatus IN('Approved','Post','Closed') AND ISNULL(poitemmstres1,'')='' AND poitemmstoid NOT IN(SELECT tg.pomstoid FROM QL_trnsupptg tg WHERE tg.poreftype='QL_trnpoitemmst') AND suppoid=" + suppoid + " " + sPlus + "  UNION ALL SELECT poassetmstoid pomstoid, 'QL_trnpoassetmst' poreftype, poassetno pono, poassetgrandtotalamt pototalamt FROM QL_trnpoassetmst WHERE poassetmststatus IN('Approved','Post','Closed') AND ISNULL(poassetmstres1,'')='' AND poassetmstoid NOT IN(SELECT tg.pomstoid FROM QL_trnsupptg tg WHERE tg.poreftype='QL_trnpoassetmst') AND suppoid=" + suppoid +" " + sPlus2 + "";
                    data = db.Database.SqlQuery<listpo>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public class listuploadtagihan
        {
            public int supptgoid { get; set; }
            public int suppoid { get; set; }
            public int pomstoid { get; set; }
            public string poreftype { get; set; }
            public string pono { get; set; }
            public decimal pototalamt { get; set; }
            public string imgpo { get; set; }
            public string imgsj { get; set; }
            public string imginv { get; set; }
            public string imgfp { get; set; }
            public DateTime createtime { get; set; }
        }

        public class listgambartagihan
        {
            public string imgtg { get; set; }
        }

        public static Image Base64ToImage(byte[] imageBytes)
        {
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        [HttpPost]
        public ActionResult PostUploadTagihan(string UserID, string dt, byte[] imgpox, byte[] imgsjx, byte[] imginvx, byte[] imgfpx)
        {
            var result = "sukses"; var msg = "";
            var mstoid = 0; var suppoid = 0;
            var dtlist = new List<listuploadtagihan>();

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";

            if (string.IsNullOrEmpty(dt))
            {
                msg += "- Data Kosong!";
            }
            else
            {
                dtlist = JsonConvert.DeserializeObject<List<listuploadtagihan>>(dt);

                if (dtlist.Count <= 0)
                {
                    msg += "- Data Kosong!";
                }
                else
                {
                    //if (string.IsNullOrEmpty(dtlist[0].imgpo))
                    //    msg += "- Image PO harap di isi";
                    //if (string.IsNullOrEmpty(dtlist[0].imgsj))
                    //    msg += "- Image SJ harap di isi";
                    //if (string.IsNullOrEmpty(dtlist[0].imginv))
                    //    msg += "- Image Invoice harap di isi";
                    //if (string.IsNullOrEmpty(dtlist[0].imgfp))
                    //    msg += "- Image Faktur Pajak harap di isi";
                }
            }

            if (!string.IsNullOrEmpty(UserID))
            {
                suppoid = db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Select(y => y.suppcustoid.Value).FirstOrDefault();
            }

            if (msg == "")
            {
                var servertime = ClassFunction.GetServerTime();

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var path = Server.MapPath(itempath);

                        //Image imagepo = Base64ToImage(imgpo);
                        string imageNamepo = "imgpo_"+ dtlist[0].pomstoid + "_"+ dtlist[0].poreftype + ".jpg";
                        string imgPathpo = itempath + "/" + imageNamepo;
                        //imagepo.Save(imgPathpo, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //byte[] pox = HttpUtility.UrlDecodeToBytes(imgpox);
                        System.IO.File.WriteAllBytes(Path.Combine(path, imageNamepo), imgpox);

                        //Image imagesj = Base64ToImage(imgsj);
                        string imageNamesj = "imgsj_" + dtlist[0].pomstoid + "_" + dtlist[0].poreftype + ".jpg";
                        string imgPathsj = itempath + "/" + imageNamesj;
                        //imagesj.Save(imgPathsj, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //byte[] sjx = HttpUtility.UrlDecodeToBytes(imgsjx);
                        System.IO.File.WriteAllBytes(Path.Combine(path, imageNamesj), imgsjx);

                        //Image imageinv = Base64ToImage(imginv);
                        string imageNameinv = "imginv_" + dtlist[0].pomstoid + "_" + dtlist[0].poreftype + ".jpg";
                        string imgPathinv = itempath + "/" + imageNameinv;
                        //imageinv.Save(imgPathinv, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //byte[] invx = HttpUtility.UrlDecodeToBytes(imginvx);
                        System.IO.File.WriteAllBytes(Path.Combine(path, imageNameinv), imginvx);

                        //Image imagefp = Base64ToImage(imgfp);
                        string imageNamefp = "imgfp_" + dtlist[0].pomstoid + "_" + dtlist[0].poreftype + ".jpg";
                        string imgPathfp = itempath + "/" + imageNamefp;
                        //imagefp.Save(imgPathfp, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //byte[] fpx = HttpUtility.UrlDecodeToBytes(imgfpx);
                        System.IO.File.WriteAllBytes(Path.Combine(path, imageNamefp), imgfpx);

                        QL_trnsupptg tbl = new QL_trnsupptg();

                        tbl.cmpcode = CompnyCode;
                        tbl.suppoid = suppoid;
                        tbl.pomstoid = dtlist[0].pomstoid;
                        tbl.poreftype = dtlist[0].poreftype;
                        tbl.pototalamt = dtlist[0].pototalamt;
                        //tbl.imgpo = dtlist[0].imgpo;
                        //tbl.imgsj = dtlist[0].imgsj;
                        //tbl.imginv = dtlist[0].imginv;
                        //tbl.imgfp = dtlist[0].imgfp;
                        tbl.imgpo = imgPathpo;
                        tbl.imgsj = imgPathsj;
                        tbl.imginv = imgPathinv;
                        tbl.imgfp = imgPathfp;
                        tbl.createtime = servertime;
                        tbl.createuser = UserID;
                        tbl.updtime = servertime;
                        tbl.upduser = UserID;
                        db.QL_trnsupptg.Add(tbl);
                        db.SaveChanges();

                        mstoid = tbl.supptgoid;

                        objTrans.Commit();
                        result = "sukses";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed"; msg += ex.ToString();
                    }
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, suppoid }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetUploadTagihan(string UserID = "")
        {
            var result = "sukses"; var msg = "";
            List<listuploadtagihan> data = new List<listuploadtagihan>();
            int suppoid = 0;

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (!string.IsNullOrEmpty(UserID))
            {
                suppoid = db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Select(y => y.suppcustoid.Value).FirstOrDefault();

                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {

                    //sSql = "SELECT tg.suppoid, tg.pomstoid, tg.poreftype, '' pono, tg.pototalamt, /*CAST(CAST(tg.imgpo AS VARBINARY(MAX))  AS VARCHAR(MAX))*/  tg.imgpo imgpo, /*CAST(CAST(tg.imgsj AS VARBINARY(MAX))  AS VARCHAR(MAX))*/  tg.imgsj imgsj, /*CAST(CAST(tg.imginv AS VARBINARY(MAX))  AS VARCHAR(MAX))*/ tg.imginv imginv, /*CAST(CAST(tg.imgfp AS VARBINARY(MAX))  AS VARCHAR(MAX))*/ tg.imgfp imgfp, tg.createtime FROM QL_trnsupptg tg WHERE suppoid=" + suppoid + "";
                    sSql = "SELECT tg.supptgoid, tg.suppoid, tg.pomstoid, tg.poreftype, CASE WHEN tg.poreftype='QL_trnpoassetmst' THEN (SELECT pom.poassetno FROM QL_trnpoassetmst pom WHERE pom.poassetmstoid=tg.pomstoid) ELSE (SELECT pom.poitemno FROM QL_trnpoitemmst pom WHERE pom.poitemmstoid=tg.pomstoid) END pono, tg.pototalamt, CASE WHEN ISNULL(imgpo,'') <> '' THEN '" + urlimgsrv + "' + REPLACE(ISNULL(imgpo,''),'~/','') ELSE ISNULL(imgpo,'') END imgpo, CASE WHEN ISNULL(imgsj,'') <> '' THEN '" + urlimgsrv + "' + REPLACE(ISNULL(imgsj,''),'~/','') ELSE ISNULL(imgsj,'') END imgsj, CASE WHEN ISNULL(imginv,'') <> '' THEN '" + urlimgsrv + "' + REPLACE(ISNULL(imginv,''),'~/','') ELSE ISNULL(imginv,'') END imginv, CASE WHEN ISNULL(imgfp,'') <> '' THEN '" + urlimgsrv + "' + REPLACE(ISNULL(imgfp,''),'~/','') ELSE ISNULL(imgfp,'') END imgfp, tg.createtime FROM QL_trnsupptg tg WHERE suppoid=" + suppoid + "";
                    data = db.Database.SqlQuery<listuploadtagihan>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetGambarTagihan(string UserID = "", string idtg = "", string tipe = "")
        {
            var result = "sukses"; var msg = ""; var sPlus = "";
            List<listgambartagihan> data = new List<listgambartagihan>();
            int suppoid = 0;

            if (string.IsNullOrEmpty(UserID))
                msg += "- User ID harap di isi";
            if (string.IsNullOrEmpty(idtg))
                msg += "- ID Tagihan harap di isi";
            if (string.IsNullOrEmpty(tipe))
                msg += "- tipe harap di isi";

            if (!string.IsNullOrEmpty(UserID))
            {
                suppoid = db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Select(y => y.suppcustoid.Value).FirstOrDefault();

                if (db.QL_m01US.Where(x => x.usoid == UserID && x.ustype == "supp").Count() <= 0)
                {
                    msg += "- User ID & Password Belum Terdaftar!";
                }
            }

            if (msg == "")
            {
                try
                {
                    if (tipe == "po")
                    {
                        sPlus = "imgpo";
                    }
                    else if (tipe == "sj")
                    {
                        sPlus = "imgsj";
                    }
                    else if (tipe == "inv")
                    {
                        sPlus = "imginv";
                    }
                    else if (tipe == "fp")
                    {
                        sPlus = "imgfp";
                    }
                    sSql = "SELECT CASE WHEN ISNULL("+ sPlus +",'') <> '' THEN '" + urlimgsrv + "' + REPLACE(ISNULL(" + sPlus + ",''),'~/','') ELSE ISNULL(" + sPlus + ",'') END imgtg FROM QL_trnsupptg tg WHERE suppoid=" + suppoid + " AND supptgoid="+ idtg +" ";
                    data = db.Database.SqlQuery<listgambartagihan>(sSql).ToList();

                    result = "sukses";
                }
                catch (Exception ex)
                {
                    result = "failed"; msg += ex.ToString();
                }
            }
            else
            {
                result = "failed";
            }

            JsonResult js = Json(new { result, msg, data }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }
    }
}