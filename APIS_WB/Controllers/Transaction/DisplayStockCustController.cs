﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class DisplayStockCustController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";

        public DisplayStockCustController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class listdisplaystmst
        {
            public int catoid { get; set; }
            public string catdesc { get; set; }
            public int jml { get; set; }
        }

        public class listdisplayds
        {
            public int seq { get; set; }
            public int catoid { get; set; }
            public int custoid { get; set; }
            public string custcode { get; set; }
            public string custname { get; set; }
        }

        [HttpPost]
        public ActionResult GetCustomerData()
        {
            List<QL_mstcust> tbl = new List<QL_mstcust>();

            sSql = "SELECT * FROM QL_mstcust WHERE activeflag='ACTIVE' AND custoid NOT IN(SELECT custoid FROM QL_displaystmst) ORDER BY custname";
            tbl = db.Database.SqlQuery<QL_mstcust>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail()
        {
            List<listdisplayds> tbl = new List<listdisplayds>();

            sSql = "SELECT 0 seq, 0 catoid, custoid, custcode, custname FROM QL_mstcust i WHERE activeflag='ACTIVE' AND custoid NOT IN(SELECT custoid FROM QL_mstcatds) ORDER BY custcode";
            tbl = db.Database.SqlQuery<listdisplayds>(sSql).ToList();

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        public JsonResult SetDataDetails(List<listdisplayds> dtDtl)
        {
            Session["QL_mstcatds"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_mstcatds"] == null)
            {
                Session["QL_mstcatds"] = new List<listdisplayds>();
            }

            List<listdisplayds> dataDtl = (List<listdisplayds>)Session["QL_mstcatds"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        // GET/POST: Driver
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            sSql = "SELECT gnoid catoid, gndesc catdesc, ISNULL((SELECT COUNT(*) FROM QL_mstcatds ds WHERE ds.catoid=ex.gnoid),0) jml FROM QL_m05GN ex WHERE ex.cmpcode='" + Session["CompnyCode"].ToString() + "' AND gngroup='KATEGORI DISPLAY STOCK' order by ex.gnoid ASC";
            List<listdisplaystmst> ExpList = db.Database.SqlQuery<listdisplaystmst>(sSql).ToList();
            return View(ExpList);
        }

        // GET: Driver/Form/5
        //pageload
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //QL_displaystmst tbl;
            string action = "Create";
            if (id == null)
            {
                //tbl = new QL_displaystmst();
                //tbl.cmpcode = Session["CompnyCode"].ToString();
                //tbl.createuser = Session["UserID"].ToString();
                //tbl.createtime = ClassFunction.GetServerTime();

                Session["QL_mstcatds"] = null;
            }
            else
            {
                action = "Edit";
                //tbl = db.QL_displaystmst.Find(id);
                List<listdisplayds> dtl = new List<listdisplayds>();
                sSql = "SELECT 0 seq, dd.catoid, dd.custoid, custcode, custname FROM QL_mstcatds dd INNER JOIN QL_mstcust i ON i.custoid=dd.custoid WHERE dd.catoid=" + id + " ORDER BY dd.catdsoid";

                dtl = db.Database.SqlQuery<listdisplayds>(sSql).ToList();
                if (dtl.Count > 0)
                {
                    for (int i = 0; i < dtl.Count; i++)
                    {
                        dtl[i].seq = i + 1;
                    }
                }
                Session["QL_mstcatds"] = dtl;
                ViewBag.catoid = id;

                sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KATEGORI DISPLAY STOCK' ORDER BY gnoid";
                var gnoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", id);
                ViewBag.gnoid = gnoid;
            }

            //if (tbl == null)
            //{
            //    return HttpNotFound();
            //}
            ViewBag.action = action;
            //InitDDL(tbl);
            //FillAdditionalField(tbl);
            return View();
        }

        // POST: COA/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(string action, int catoid)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();

            List<listdisplayds> dtDtl = (List<listdisplayds>)Session["QL_mstcatds"];
            //if (dtDtl == null)
            //    ModelState.AddModelError("", "Maaf Tolong isi data detail,,!!");
            //else if (dtDtl.Count <= 0)
            //    ModelState.AddModelError("", "Maaf Tolong isi data detail,,!!");

            if (catoid == 0)
            {
                ModelState.AddModelError("", "Silahkan Isi Kategori!");
            }

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].custoid == 0)
                        {
                            ModelState.AddModelError("", "Silahkan Isi Customer!!");
                        }
                        if (dtDtl[i].catoid == 0)
                        {
                            ModelState.AddModelError("", "Silahkan Isi Kategori!!");
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                //var mstoid = ClassFunction.GenerateID("QL_displaystmst");
                //var dtloid = ClassFunction.GenerateID("QL_mstcatds");
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {

                        }
                        else if (action == "Edit")
                        {

                            var trndtl = db.QL_mstcatds.Where(a => a.catoid == catoid);
                            db.QL_mstcatds.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        if (dtDtl != null)
                        {
                            if (dtDtl.Count > 0)
                            {
                                QL_mstcatds tbldtl;
                                for (int i = 0; i < dtDtl.Count(); i++)
                                {
                                    tbldtl = new QL_mstcatds();
                                    tbldtl.cmpcode = CompnyCode;
                                    tbldtl.catoid = catoid;
                                    tbldtl.custoid = dtDtl[i].custoid;
                                    tbldtl.upduser = Session["UserID"].ToString();
                                    tbldtl.updtime = servertime;
                                    db.QL_mstcatds.Add(tbldtl);
                                    db.SaveChanges();
                                }
                            }
                        }

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        ModelState.AddModelError("", "Error occured, records rolledback." + ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            //InitDDL(tbl);
            //FillAdditionalField(tbl);
            return View();
        }

        private void InitDDL(QL_displaystmst tbl)
        {
            //sSql = "SELECT * FROM QL_m05GN WHERE gngroup='KOTA' ORDER BY gnoid";
            //var gnoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc");
            //ViewBag.expcityoid = gnoid;
        }

        private void FillAdditionalField(QL_displaystmst tbl)
        {
            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust a WHERE a.custoid ='" + tbl.custoid + "'").FirstOrDefault();
        }


        // POST: MasterCoa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //QL_displaystmst tblmst = db.QL_displaystmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_mstcatds.Where(a => a.catoid == id);
                        db.QL_mstcatds.RemoveRange(trndtl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}