﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;
namespace APIS_WB.Controllers.ReportForm
{
    public class UBReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public UBReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        public ActionResult GetUBData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "Select 0 seq, arm.matusageno, arm.matusagedate, d.deptname From QL_trnmatusagemst arm INNER JOIN QL_mstdept d ON d.deptoid=arm.deptoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.matusagemststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                 
                sSql += " ORDER BY arm.matusageno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblAR");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string ddlnomor, string TextNomor, string startdate, string enddate, string fdate, string whoid, string deptoid, string reasonoid)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnmatusagedtl ard ON ard.matusagerefoid = m.itemoid INNER JOIN QL_m05GN g ON g.gnoid = ard.matusageunitoid INNER JOIN QL_trnmatusagemst arm ON arm.matusagemstoid = ard.matusagemstoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (startdate != "" && enddate != "")
                {
                    sSql += " AND " + fdate + ">=CAST('" + startdate + " 00:00:00' AS DATETIME) AND " + fdate + "<=CAST('" + enddate + " 23:59:59' AS DATETIME)";
                }

                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.matusagemststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                 

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + ddlnomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (whoid != null)
                {
                    if (whoid.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < whoid.Count(); i++)
                        {
                            stsval += "'" + whoid[i] + "',";
                        }
                        sSql += " AND ard.matusagewhoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (deptoid != null)
                {
                    if (deptoid.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < deptoid.Count(); i++)
                        {
                            stsval += "'" + deptoid[i] + "',";
                        }
                        sSql += " AND arm.deptoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (reasonoid != null)
                {
                    if (reasonoid.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < reasonoid.Count(); i++)
                        {
                            stsval += "'" + reasonoid[i] + "',";
                        }
                        sSql += " AND arm.reasonoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        private void InitDDL(QL_trnmatusagemst tbl)
        {
            sSql = "SELECT * FROM QL_mstdivision WHERE activeflag='ACTIVE'";
            if (Session["CompnyCode"].ToString() != CompnyCode)
                sSql += " AND cmpcode IN ('" + Session["CompnyCode"].ToString() + "')";
            sSql += " ORDER BY divname";

            var cmpcode = new SelectList(db.Database.SqlQuery<QL_mstdivision>(sSql).ToList(), "cmpcode", "divname", tbl.cmpcode);
            ViewBag.cmpcode = cmpcode;

            sSql = "SELECT * FROM QL_mstdeptgroup WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' ORDER BY groupdesc";
            var deptoid = new SelectList(db.Database.SqlQuery<QL_mstdeptgroup>(sSql).ToList(), "groupoid", "groupdesc", tbl.deptoid);
            ViewBag.deptoid = deptoid;

            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gnflag='ACTIVE' AND gngroup='GUDANG' ORDER BY gndesc";
            var whoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", ViewBag.whoid);
            ViewBag.whoid = whoid;

            string acctgoid = ClassFunction.GetDataAcctgOid("VAR_USAGE_NON_KIK", CompnyCode);
            sSql = "SELECT * FROM QL_mstacctg WHERE cmpcode='" + CompnyCode + "' AND activeflag='ACTIVE' AND acctgoid IN (" + acctgoid + ") ORDER BY acctgcode";
            var reasonoid = new SelectList(db.Database.SqlQuery<QL_mstacctg>(sSql).ToList(), "acctgoid", "acctgdesc", tbl.reasonoid);
            ViewBag.reasonoid = reasonoid;
        }
        // GET: UBReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnmatusagemst tbl;
            tbl = new QL_trnmatusagemst();
            if (tbl == null)
                return HttpNotFound();
            InitDDL(tbl);
            return View();
        }
               

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string whoid, string deptoid, string reasonoid)
        {
            var Slct = ""; var Join = "";

            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = ""; var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                    rptfile = "rptUBSumPdf.rpt";
                else
                    rptfile = "rptUBSumPdf.rpt";
                rptname = "PemkaianBarang_SUMMARY";
            }
            else
            {
                if (reporttype == "XLS")
                    rptfile = "rptUBDtlPdf.rpt";
                else
                {
                    rptfile = "rptUBDtlPdf.rpt";
                }
                rptname = "PemkaianBarang_DETAIL";
            }         

            if (DDLType == "Detail")
            {
                Slct += ", m.itemoid, m.itemcode, m.itemdesc, m.itemtype, g.gndesc, ard.matusageqty QtyUsage, refno, ISNULL(ard.serialnumber,'') [serialnumber], CASE WHEN ard.matusagevalueidr = 0 THEN 0 ELSE (ard.matusagevalueidr/ard.matusageqty) END ValueUsage, ard.matusagedtlnote ";
                Join += " INNER JOIN QL_trnmatusagedtl ard ON ard.matusagemstoid=arm.matusagemstoid INNER JOIN QL_mstitem m ON m.itemoid = ard.matusagerefoid INNER JOIN QL_m05GN g ON g.gnoid = ard.matusageunitoid AND g.gngroup = 'SATUAN' INNER JOIN QL_m05GN g2 ON g2.gnoid = ard.matusagewhoid AND g2.gngroup = 'GUDANG' ";
            }
            else
            {
                Slct = ",(SELECT SUM(ard.matusageqty) FROM QL_trnmatusagedtl ard WHERE ard.matusagemstoid=arm.matusagemstoid) QtyUsage";
            }

            sSql = "SELECT arm.matusagemstoid, (CASE arm.matusageno WHEN '' THEN CONVERT(VARCHAR(10), arm.matusagemstoid) ELSE arm.matusageno END) AS [Draft No.],arm.matusagedate, arm.matusageno, arm.deptoid, dep.groupdesc deptname, ac.acctgdesc, arm.reasonoid, arm.matusagemstnote, arm.matusagemststatus, arm.updtime, arm.createuser, arm.upduser " + Slct + " From QL_trnmatusagemst arm INNER JOIN QL_mstdeptgroup dep ON dep.groupoid=arm.deptoid INNER JOIN QL_mstacctg ac ON ac.acctgoid=arm.reasonoid "+ Join + " WHERE arm.cmpcode='" + CompnyCode + "'";

            if (StartPeriod != "" && EndPeriod != "")
            {
                sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
            }

            if (DDLStatus != null)
            {
                if (DDLStatus.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < DDLStatus.Count(); i++)
                    {
                        stsval += "'" + DDLStatus[i] + "',";
                    }
                    sSql += " AND arm.matusagemststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (deptoid != null)
            {
                if (deptoid.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < deptoid.Count(); i++)
                    {
                        stsval += "'" + deptoid[i] + "',";
                    }
                    sSql += " AND arm.deptoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (reasonoid != null)
            {
                if (reasonoid.Count() > 0)
                {
                    string stsval = "";
                    for (int i = 0; i < reasonoid.Count(); i++)
                    {
                        stsval += "'" + reasonoid[i] + "',";
                    }
                    sSql += " AND arm.reasonoid IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                }
            }

            if (TextNomor != "")
            {
                string[] arr = TextNomor.Split(';'); string filterdata = "";
                for (int i = 0; i < arr.Count(); i++)
                {
                    filterdata += "'" + arr[i] + "',";
                }
                sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            sSql += " ORDER BY " + DDLSorting + " " + DDLSortDir + "";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperLegal;
                } 
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperLegal;
                } 
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}