﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;

namespace APIS_WB.Controllers.Master
{
    public class Kategori2Controller : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public Kategori2Controller()
        {
            db.Database.CommandTimeout = 0;
        }

        public class ListCat2
        {
            public string cat1code { get; set; }
            public string cat2code { get; set; }
            public string cat2shortdesc { get; set; }
            public string cat2note { get; set; }
            public string activeflag { get; set; }
            public string cat1shortdesc { get; set; }
            public string cat2longdesc { get; set; }

        }

        private void setViewBag(QL_mstcat2 qL_mstcat2)
        {
            var cat1code = new SelectList(db.Database.SqlQuery<QL_mstcat1>("SELECT * FROM QL_mstcat1 WHERE activeflag='ACTIVE'").ToList(), "cat1code", "cat1shortdesc", qL_mstcat2.cat1code);
            ViewBag.cat1code = cat1code;
        }

        // GET: Kategory
        public ActionResult Index(mdFilter filter)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string sfilter = "";
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (!String.IsNullOrEmpty(filter.txtfilter))
                    sfilter += " AND " + filter.ddlfilter + " LIKE '%" + filter.txtfilter + "%'";
                if (filter.ddlstatus != "ALL")
                    sfilter += " AND c2.activeflag='" + filter.ddlstatus + "'";
                ViewBag.ddlfilter = filter.ddlfilter;
                ViewBag.txtfilter = filter.txtfilter;
                ViewBag.ddlstatus = filter.ddlstatus;
            }
            sSql = "SELECT c2.*, c1.cat1shortdesc FROM QL_mstcat2 c2 inner join ql_mstcat1 c1 on c2.cat1code=c1.cat1code WHERE c2.cmpcode='" + Session["CompnyCode"].ToString() + "'" + sfilter + " and c2.cat2code<>'0' ORDER BY cat2shortdesc";
            List<ListCat2> vbag = db.Database.SqlQuery<ListCat2>(sSql).ToList();
            return View(vbag);
        }

        // GET: Kategori1/Form/5
        public ActionResult Form(string c2, string c1)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_mstcat2 tbl;
            string action = "Create";
            if (c2 == null)
            {
                tbl = new QL_mstcat2();
                tbl.cmpcode = Session["CompnyCode"].ToString();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
            }
            else
            {
                action = "Edit";
                tbl = db.QL_mstcat2.Find(c2, c1);
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Categori1/Form/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_mstcat2 tbl, string action)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            //Is Input Valid?
            if (tbl.cat2code == "")
                ModelState.AddModelError("cat1code", "Pilih Kategori 1 Dahulu !!");
            if (string.IsNullOrEmpty(tbl.cat2code))
                ModelState.AddModelError("cat2code", "Silahkan isi Kode!");
            else
            {
                if (action == "Create")
                {
                    if (db.QL_mstcat2.Where(w => w.cat2code == tbl.cat2code && w.cat1code == tbl.cat1code).Count() > 0)
                        ModelState.AddModelError("cat2code", "Kode yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Kode lainnya!");
                }
            }
            if (string.IsNullOrEmpty(tbl.cat2shortdesc))
                ModelState.AddModelError("cat2shortdesc", "Silahkan isi Deskripsi!");
            else if (db.QL_mstcat2.Where(w => w.cat2shortdesc == tbl.cat2shortdesc && w.cat2code != tbl.cat2code && w.cat1code == tbl.cat1code).Count() > 0)
                ModelState.AddModelError("cat2shortdesc", "Deskripsi yang Anda masukkan sudah digunakan oleh data lainnya. Silahkan masukkan Deskripsi lainnya!");
            if (string.IsNullOrEmpty(tbl.cat2note))
                tbl.cat2note = "";
            if (tbl.cat2code.Length != 2)
                ModelState.AddModelError("cat2code", "Kode Maximum 2 Karakter!");

            tbl.cat2longdesc = tbl.cat2shortdesc;

            var servertime = ClassFunction.GetServerTime();
            var mstoid = ClassFunction.GenerateID("QL_mstcat2");

            if (ModelState.IsValid)
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_mstcat2.Add(tbl);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "UPDATE QL_ID SET lastoid=" + mstoid + " WHERE tablename='QL_mstcat2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        return View(ex.ToString());
                    }
                }
            }
            ViewBag.action = action;
            setViewBag(tbl);
            return View(tbl);
        }

        // POST: Kategori1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string c2, string c1)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        QL_mstcat2 tbl = db.QL_mstcat2.Find(c2, c1);
                        db.QL_mstcat2.Remove(tbl);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();
                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
