﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;


namespace APIS_WB.Controllers.Transaction
{
    public class SJJasaController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public SJJasaController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class shipmentitemmst
        {
            public string cmpcode { get; set; }
            public int shipmentitemmstoid { get; set; }
            public string shipmentitemno { get; set; }
            public DateTime shipmentitemdate { get; set; }
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public string projectname { get; set; }
            public string custname { get; set; }
            public string shipmentitemmststatus { get; set; }
            public string shipmentitemmstnote { get; set; }
        }

        public class shipmentitemdtl
        {
            public int shipmentitemdtlseq { get; set; }
            public int soitemdtloid { get; set; }
            public int rabdtloid { get; set; }
            public int jasaoid { get; set; }
            public string jasacode { get; set; }
            public string jasadesc { get; set; }
            public int shipmentitemunitoid { get; set; }
            public string shipmentitemunit { get; set; }
            public decimal soitemqty { get; set; }
            public decimal shipmentitemqty { get; set; }
            public string shipmentitemdtlnote { get; set; }
            public decimal shipmentitemvalueidr { get; set; }
            public decimal shipmentitemvalue { get; set; }
            public int jasaacctgoid { get; set; }
        }

        public class soitem
        {
            public int soitemmstoid { get; set; }
            public string soitemno { get; set; }
            public DateTime soitemdate { get; set; }
            public string soitemdatestr { get; set; }
            public int custoid { get; set; }
            public string custname { get; set; }
            public string custaddr { get; set; }
            public int rabmstoid { get; set; }
            public string rabno { get; set; }
            public int curroid { get; set; }
        }

        public class warehouse
        {
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
        }

        public class listmat
        {
            public int soitemdtloid { get; set; }
            public int itemseq { get; set; }
            public int jasaoid { get; set; }
            public string jasacode { get; set; }
            public string refno { get; set; }
            public string jasadesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal qtystok { get; set; }
            public Decimal soitemqty { get; set; }
            public int shipmentitemwhoid { get; set; }
            public string shipmentitemwh { get; set; }
            public Decimal soitemprice { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        public class listnopol
        {
            public string nopol { get; set; }
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int jasaoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + jasaoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listnopol> tbl = new List<listnopol>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listnopol>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnshipmentitemmst tbl)
        {
            sSql = "SELECT * FROM QL_mstexpedisi where activeflag = 'ACTIVE'";
            var ekspedisioid = new SelectList(db.Database.SqlQuery<QL_mstexpedisi>(sSql).ToList(), "expoid", "expname", tbl.ekspedisioid);
            ViewBag.ekspedisioid = ekspedisioid;

            sSql = "SELECT us.* FROM QL_m01us us INNER JOIN QL_m05gn gn ON gn.gnoid = us.jabatanoid where usflag = 'ACTIVE' AND gn.gndesc = 'DRIVER'";
            var driveroid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", tbl.driveroid);
            ViewBag.driveroid = driveroid;

            sSql = "select * from QL_mstvehicle where activeflag = 'ACTIVE'";
            var vhcoid = new SelectList(db.Database.SqlQuery<QL_mstvehicle>(sSql).ToList(), "vhcoid", "vhcdesc", tbl.armadaoid);
            ViewBag.armadaoid = vhcoid;

        }

        [HttpPost]
        public ActionResult GetDataMatDetail(int soitemmstoid, int shipmentitemmstoid)
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT DISTINCT sod.soitemdtloid,0 itemseq, i.jasaoid, jasacode,jasadesc, jasaunitoid itemunitoid, g.gndesc itemunit, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=sod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=sod.soitemdtloid AND sd.shipmentitemmstoid<>"+ shipmentitemmstoid +"), 0.0)) AS soitemqty, sod.soitemprice FROM QL_mstjasa i INNER JOIN QL_m05GN g ON g.gnoid = i.jasaunitoid AND gngroup = 'SATUAN' INNER JOIN QL_trnsoitemdtl sod ON sod.itemoid = i.jasaoid WHERE soitemmstoid = "+ soitemmstoid +" AND sod.soitemdtlstatus = '' ORDER BY jasacode";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSOData()
        {
            List<soitem> tbl = new List<soitem>();
            sSql = "SELECT soitemmstoid, soitemno, soitemdate,CONVERT(CHAR(10),som.soitemdate,103) soitemdatestr, c.custoid, c.custname, (SELECT custdtl2addr FROM QL_mstcustdtl2 where custdtl2oid = som.alamatoid) custaddr, rm.rabmstoid, rm.projectname rabno, som.curroid FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = som.custoid and som.soitemmststatus = 'Post' AND som.soitemtype IN ('Jasa') order by soitemno";

            tbl = db.Database.SqlQuery<soitem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<shipmentitemdtl> dtDtl)
        {
            Session["QL_trnshipmentitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnshipmentitemdtl"] == null)
            {
                Session["QL_trnshipmentitemdtl"] = new List<shipmentitemdtl>();
            }

            List<shipmentitemdtl> dataDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }

        private void FillAdditionalField(QL_trnshipmentitemmst tbl)
        {
            ViewBag.soitemno = db.Database.SqlQuery<string>("SELECT soitemno FROM QL_trnsoitemmst WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid=" + tbl.soitemmstoid + "").FirstOrDefault();

            ViewBag.soitemdate = db.Database.SqlQuery<string>("SELECT CONVERT(CHAR(10),soitemdate,103) FROM QL_trnsoitemmst WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid=" + tbl.soitemmstoid + "").FirstOrDefault();

            ViewBag.custname = db.Database.SqlQuery<string>("SELECT custname FROM QL_mstcust WHERE cmpcode='" + CompnyCode + "' AND custoid=" + tbl.custoid + "").FirstOrDefault();

            ViewBag.custaddr = db.Database.SqlQuery<string>("SELECT custdtl2addr FROM QL_mstcustdtl2 cd INNER JOIN QL_trnsoitemmst som ON som.alamatoid=cd.custdtl2oid WHERE som.cmpcode='" + CompnyCode + "' AND som.soitemmstoid=" + tbl.soitemmstoid + "").FirstOrDefault();
        }

        // GET: shipmentitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT shm.cmpcode, shm.shipmentitemmstoid, shm.shipmentitemno, shm.shipmentitemdate, shm.soitemmstoid, ISNULL((SELECT projectname FROM QL_trnrabmst rm INNER JOIN QL_trnsoitemmst sm ON sm.rabmstoid = rm.rabmstoid WHERE sm.soitemmstoid = shm.soitemmstoid),'') projectname, (SELECT soitemno FROM QL_trnsoitemmst where soitemmstoid = shm.soitemmstoid) soitemno, c.custname, shm.shipmentitemmststatus, shm.shipmentitemmstnote FROM QL_trnshipmentitemmst shm INNER JOIN QL_mstcust c ON c.custoid=shm.custoid WHERE shm.shipmentitemtype IN ('Jasa') AND shm.cmpcode = '"+ CompnyCode +"'";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND shm.shipmentitemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND shm.shipmentitemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND shm.shipmentitemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY shm.shipmentitemdate";

            List<shipmentitemmst> dt = db.Database.SqlQuery<shipmentitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: shipmentitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnshipmentitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.shipmentitemdate = ClassFunction.GetServerTime();
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.shipmentitemmststatus = "In Process";

                Session["QL_trnshipmentitemdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnshipmentitemmst.Find(CompnyCode, id);
                sSql = "SELECT sd.soitemdtloid, shipmentitemdtlseq, i.jasaoid, i.jasacode, jasadesc, (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=shd.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=shd.soitemdtloid AND sd.shipmentitemmstoid<>"+ tbl.shipmentitemmstoid + "), 0.0)) AS soitemqty, shipmentitemqty, shipmentitemvalue, shipmentitemunitoid, gn.gndesc shipmentitemunit, shipmentitemdtlnote from QL_trnshipmentitemdtl shd INNER JOIN QL_trnsoitemdtl sd ON sd.soitemdtloid = shd.soitemdtloid INNER JOIN QL_m05GN gn ON gn.gnoid = shd.shipmentitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_mstjasa i ON i.jasaoid = shd.itemoid WHERE shd.cmpcode='" + CompnyCode +"' AND shd.shipmentitemmstoid='"+ id +"' ORDER BY shd.shipmentitemdtlseq";
                Session["QL_trnshipmentitemdtl"] = db.Database.SqlQuery<shipmentitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: shipmentitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnshipmentitemmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.shipmentitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("shipmentitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.shipmentitemmststatus == "Post")
            {
                tbl.shipmentitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.shipmentitemno = "";
            }
            if (string.IsNullOrEmpty(tbl.shipmentitemmstres1))
                tbl.shipmentitemmstres1 = "";
            if (string.IsNullOrEmpty(tbl.shipmentitemmstres2))
                tbl.shipmentitemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.shipmentitemtype))
                tbl.shipmentitemtype = "Jasa";
            if (string.IsNullOrEmpty(tbl.shipmentitemmstnote))
                tbl.shipmentitemmstnote = "";
            if (string.IsNullOrEmpty(tbl.ekspedisinoresi))
                tbl.ekspedisinoresi = "";

            var servertime = ClassFunction.GetServerTime();
            var rate2oid = 0; var rate2toidr = "0";
            var cRate = new ClassRate();
            if (tbl.shipmentitemmststatus == "Post")
            {
                cRate.SetRateValue(tbl.curroid, servertime.ToString("MM/dd/yyyy"));
                if (cRate.GetRateMonthlyLastError != "")
                {
                    ModelState.AddModelError("", cRate.GetRateMonthlyLastError);
                }
                else
                {
                    rate2oid = cRate.GetRateMonthlyOid;
                    rate2toidr = cRate.GetRateMonthlyIDRValue.ToString();
                }
            }

            List<shipmentitemdtl> dtDtl = (List<shipmentitemdtl>)Session["QL_trnshipmentitemdtl"];

            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        var dSHQty = dtDtl.GroupBy(x => x.jasaoid).Select(x => new { Nama = x.Key, Amt = x.Sum(y => y.shipmentitemqty) });
                        if (dtDtl[i].shipmentitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY SJ item " + dtDtl[i].jasadesc + "  tidak boleh 0!");
                        }
                        else
                        {
                            if (dtDtl[i].shipmentitemqty > dtDtl[i].soitemqty)
                            {
                                ModelState.AddModelError("", "QTY SJ item " + dtDtl[i].jasadesc + " Harus kurang dari SO QTY!");
                            }
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].shipmentitemdtlnote))
                            dtDtl[i].shipmentitemdtlnote = "";
                        sSql = "SELECT (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=dod.soitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0)) AS soitemqty FROM QL_trnsoitemdtl dod WHERE dod.cmpcode='" + CompnyCode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid;
                        var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                        if (dQty != dtDtl[i].soitemqty)
                            dtDtl[i].soitemqty = dQty;
                        if (dQty < dtDtl[i].soitemqty)
                            ModelState.AddModelError("", "Qty item " + dtDtl[i].jasadesc + " SJ telah diupdate oleh user yg lain. Silahkan cek setiap Qty Harus kurang dari SO QTY!");
                        if (dtDtl[i].shipmentitemqty > dQty)
                            ModelState.AddModelError("", "Qty item " + dtDtl[i].jasadesc + " SO tidak boleh lebih dari Qty SJ (" + dQty + ")");
                        
                        dtDtl[i].shipmentitemvalueidr = dtDtl[i].shipmentitemvalue;

                        sSql = "SELECT acctgoid FROM QL_mstjasa j WHERE j.cmpcode='" + CompnyCode + "' AND j.jasaoid=" + dtDtl[i].jasaoid + "";
                        dtDtl[i].jasaacctgoid = db.Database.SqlQuery<int>(sSql).FirstOrDefault();
                    }
                    if (tbl.shipmentitemmststatus == "Post")
                    {
                        if (string.IsNullOrEmpty(tbl.shipmentitemno))
                            ModelState.AddModelError("shipmentitemno", "Silahkan isi No. SJ!");
                        else if (db.QL_trnshipmentitemmst.Where(w => w.shipmentitemno == tbl.shipmentitemno & w.shipmentitemmstoid != tbl.shipmentitemmstoid).Count() > 0)
                            ModelState.AddModelError("shipmentitemno", "No. SJ yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");
                    }
                }
            }

            //if (tbl.shipmentitemmststatus == "Post")
            //{
            //    // Interface Validation
            //    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_FG", CompnyCode))
            //        ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_FG"));
            //    if (!ClassFunction.IsInterfaceExists("VAR_STOCK_TRANSIT", CompnyCode))
            //        ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK_TRANSIT"));
            //}

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnshipmentitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnshipmentitemdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");
                
                //var iAcctgOidTrans = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK_TRANSIT", CompnyCode));

                tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.shipmentitemdate);
                tbl.rate2oid = rate2oid;

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            tbl.shipmentitemmstoid = mstoid;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            db.QL_trnshipmentitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.shipmentitemmstoid + " WHERE tablename='QL_trnshipmentitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid IN (SELECT sm.soitemmstoid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid=" + tbl.shipmentitemmstoid + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == tbl.shipmentitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        var z = dtDtl.GroupBy(x => x.jasaoid).Select(x => new { Nama = x.Key, Qty = x.Sum(y => y.shipmentitemqty) }).ToList();
                        for (int i = 0; i < z.Count(); i++)
                        {
                            sSql = "SELECT (soitemqty - ISNULL((SELECT SUM(shipmentitemqty) FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.cmpcode=sd.cmpcode AND sm.shipmentitemmstoid=sd.shipmentitemmstoid WHERE sd.cmpcode=dod.cmpcode AND shipmentitemmststatus<>'Rejected' AND sd.soitemdtloid=dod.soitemdtloid AND sd.shipmentitemmstoid<>" + tbl.shipmentitemmstoid + "), 0.0)) AS soitemqty FROM QL_trnsoitemdtl dod WHERE dod.cmpcode='" + CompnyCode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid;
                            var qtyOs = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (z[i].Qty >= qtyOs)
                            {
                                sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='Complete' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid=" + dtDtl[i].soitemdtloid;
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET statusjualjasa='SJ JASA PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst mr ON mr.soitemmstoid = pm.soitemmstoid where mr.shipmentitemmstoid = " + tbl.shipmentitemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Closed' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid=" + tbl.soitemmstoid + " AND (SELECT COUNT(*) FROM QL_trnsoitemdtl WHERE cmpcode='" + CompnyCode + "' AND soitemdtlstatus='' AND soitemmstoid=" + tbl.soitemmstoid + " AND soitemdtloid<>" + dtDtl[i].soitemdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnrabmst SET statusjualjasa='SJ JASA' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst mr ON mr.soitemmstoid = pm.soitemmstoid where mr.shipmentitemmstoid = " + tbl.shipmentitemmstoid + ") AND (SELECT COUNT(*) FROM QL_trnsoitemdtl pod INNER JOIN QL_trnsoitemmst pom ON pom.soitemmstoid = pod.soitemmstoid WHERE pom.cmpcode='" + CompnyCode + "' AND soitemdtlstatus='' AND pom.rabmstoid IN (SELECT rabmstoid FROM QL_trnsoitemmst where soitemmstoid = " + tbl.soitemmstoid + ") AND pod.soitemdtloid<>" + dtDtl[i].soitemdtloid + ")=0";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                        }

                        QL_trnshipmentitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnshipmentitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.shipmentitemdtloid = dtloid++;
                            tbldtl.shipmentitemmstoid = tbl.shipmentitemmstoid;
                            tbldtl.shipmentitemdtlseq = i + 1;
                            tbldtl.soitemdtloid = dtDtl[i].soitemdtloid;
                            tbldtl.refno = "";
                            tbldtl.itemoid = dtDtl[i].jasaoid;
                            tbldtl.shipmentitemqty = dtDtl[i].shipmentitemqty;
                            tbldtl.shipmentitemunitoid = dtDtl[i].shipmentitemunitoid;
                            tbldtl.shipmentitemwhoid = 0;
                            tbldtl.shipmentitemdtlstatus = "";
                            tbldtl.shipmentitemdtlnote = (dtDtl[i].shipmentitemdtlnote == null ? "" : dtDtl[i].shipmentitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.shipmentitemdtlres1 = "";
                            tbldtl.shipmentitemdtlres2 = "";
                            tbldtl.shipmentitemdtlres3 = "";
                            tbldtl.shipmentitemvalueidr = dtDtl[i].shipmentitemvalue * cRate.GetRateMonthlyIDRValue;
                            tbldtl.shipmentitemvalueusd = 0;
                            tbldtl.shipmentitemvalue = dtDtl[i].shipmentitemvalue;

                            db.QL_trnshipmentitemdtl.Add(tbldtl);
                            db.SaveChanges();                            
                        }

                        //if (tbl.shipmentitemmststatus == "Post")
                        //{

                        //    decimal gltotal = dtDtl.Sum(x => x.shipmentitemqty * x.shipmentitemvalue);
                        //    // Insert QL_trnglmst
                        //    db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, "Surat Jalan Jasa|No. " + tbl.shipmentitemno, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                        //    db.SaveChanges();

                        //    var glseq = 1;
                        //    // Insert QL_trngldtl
                        //    // D : Persediaan Barang Dlm Perjalanan
                        //    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidTrans, "D", gltotal, tbl.shipmentitemno, "Surat Jalan Jasa|No. " + tbl.shipmentitemno, "Post", Session["UserID"].ToString(), servertime, gltotal * cRate.GetRateMonthlyIDRValue, 0, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0));
                        //    // C : Biaya Lain-Lain
                        //    for (int i = 0; i < dtDtl.Count(); i++)
                        //    {
                        //        decimal glamt = dtDtl[i].shipmentitemqty * dtDtl[i].shipmentitemvalue;
                        //        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, dtDtl[i].jasaacctgoid, "C", glamt, tbl.shipmentitemno, "Surat Jalan Jasa|No. " + tbl.shipmentitemno, "Post", Session["UserID"].ToString(), servertime, glamt * cRate.GetRateMonthlyIDRValue, 0, "QL_trnshipmentitemmst " + tbl.shipmentitemmstoid, null, null, null, 0));
                        //        db.SaveChanges();
                        //    }

                        //    sSql = "UPDATE QL_ID SET lastoid=" + (glmstoid) + " WHERE tablename='QL_trnglmst'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();

                        //    sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                        //    db.Database.ExecuteSqlCommand(sSql);
                        //    db.SaveChanges();
                        //}                        

                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnshipmentitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        objTrans.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.shipmentitemmststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.shipmentitemmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);

        }


        // POST: shipmentitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnshipmentitemmst tbl = db.QL_trnshipmentitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        sSql = "UPDATE QL_trnsoitemdtl SET soitemdtlstatus='' WHERE cmpcode='" + CompnyCode + "' AND soitemdtloid IN (SELECT soitemdtloid FROM QL_trnshipmentitemdtl WHERE cmpcode='" + CompnyCode + "' AND shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnsoitemmst SET soitemmststatus='Post' WHERE cmpcode='" + CompnyCode + "' AND soitemmstoid IN (SELECT sm.soitemmstoid FROM QL_trnshipmentitemdtl sd INNER JOIN QL_trnshipmentitemmst sm ON sm.shipmentitemmstoid = sd.shipmentitemmstoid WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='SJ PARTIAL' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst rm ON rm.soitemmstoid = pm.soitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND rm.shipmentitemmstoid=" + id + ")";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        sSql = "UPDATE QL_trnrabmst SET statusjual='' WHERE cmpcode='" + CompnyCode + "' AND rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnsoitemmst pm INNER JOIN QL_trnshipmentitemmst rm ON rm.soitemmstoid = pm.soitemmstoid WHERE pm.cmpcode='" + CompnyCode + "' AND rm.shipmentitemmstoid=" + id + ") AND (SELECT COUNT(*) FROM QL_trnsoitemdtl pod INNER JOIN QL_trnsoitemmst pom ON pom.soitemmstoid = pod.soitemmstoid WHERE pom.cmpcode='" + CompnyCode + "' AND soitemdtlstatus='' AND pom.rabmstoid IN (SELECT pm.rabmstoid FROM QL_trnshipmentitemmst mm INNER JOIN QL_trnsoitemmst pm ON pm.soitemmstoid = mm.soitemmstoid where mm.shipmentitemmstoid <> " + id + "))=0";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        var trndtl = db.QL_trnshipmentitemdtl.Where(a => a.shipmentitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnshipmentitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnshipmentitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "SJJ/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(shipmentitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnshipmentitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND shipmentitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Profile");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptSJJasa.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.shipmentitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptSJJasa.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}