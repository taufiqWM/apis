﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PBReturnController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];

        public PBReturnController()
        {
            db.Database.CommandTimeout = 0;
        }

        public class pretitemmst
        {
            public string cmpcode { get; set; }
            public int pretitemmstoid { get; set; }
            public string pretitemno { get; set; }
            public DateTime pretitemdate { get; set; }
            public string mritemno { get; set; }            
            public string suppname { get; set; }
            public string pretitemmststatus { get; set; }
            public string pretitemmstnote { get; set; }           
        }

        public class pretitemdtl
        {
            public int pretitemdtlseq { get; set; }
            public int mritemdtloid { get; set; }            
            public int pretitemwhoid { get; set; }
            public string pretitemwh { get; set; }            
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public int pretitemunitoid { get; set; }
            public string pretitemunit { get; set; }
            public decimal qtystok { get; set; }
            public decimal mritemqty { get; set; }
            public decimal pretitemqty { get; set; }            
            public string pretitemdtlnote { get; set; }
            public decimal mritemvalue { get; set; }          
            public decimal pretitemamt { get; set; }
            public int assetacctgoid { get; set; }
        }        

        public class mritem
        {
            public int mritemmstoid { get; set; }
            public string mritemno { get; set; }
            public DateTime mritemdate { get; set; }
            public string mritemdatestr { get; set; }
            public int soitemmstoid { get; set; }
            public string poitemno { get; set; }
            public int rabmstoid { get; set; }
            public int rabmstoid_awal { get; set; }
            public string projectname { get; set; }
            public string departemen { get; set; }
            public string mrflag { get; set; }
        }

        public class Supp
        {
            public int suppoid { get; set; }
            public string suppcode { get; set; }
            public string suppname { get; set; }
            public string suppaddr { get; set; }
            public string suppphone { get; set; }
        }

        public class warehouse
        {
            public int pretitemwhoid { get; set; }
            public string pretitemwh { get; set; }
        }

        public class listmat
        {
            public int mritemdtloid { get; set; }
            public int pretitemdtlseq { get; set; }
            public int itemoid { get; set; }
            public string itemcode { get; set; }
            public string refno { get; set; }
            public string serialnumber { get; set; }
            public string itemdesc { get; set; }
            public int itemunitoid { get; set; }
            public string itemunit { get; set; }
            public Decimal qtystok { get; set; }
            public Decimal mritemqty { get; set; }
            public int pretitemwhoid { get; set; }
            public string pretitemwh { get; set; }
        }

        public class liststock
        {
            public Decimal stokakhir { get; set; }
        }

        public class listnopol
        {
            public string nopol { get; set; }
        }

        [HttpPost]
        public ActionResult GetDetailData(int mritemmstoid, int pretitemmstoid, string pretitemtype)
        {
            List<pretitemdtl> tbl = new List<pretitemdtl>();
            if (pretitemtype == "Asset")
            {
                sSql = "SELECT DISTINCT mrassetdtlseq mritemdtlseq, mrassetdtloid mritemdtloid, m.itemoid, itemcode, itemdesc, refno, ISNULL(serialnumber,'') serialnumber, mrassetunitoid pretitemunitoid, g.gndesc AS pretitemunit, mrm.mrassetwhoid pretitemwhoid, (mrassetqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=mrd.cmpcode AND sretd.mritemdtloid=mrd.mrassetdtloid AND pretitemmststatus<>'Rejected' AND ISNULL(pretitemtype,'')='Asset' AND sretd.pretitemmstoid<>" + pretitemmstoid + "), 0)) AS mritemqty, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat c where c.refoid = m.itemoid and c.mtrwhoid = mrm.mrassetwhoid and c.refno = mrd.refno and ISNULL(c.serialnumber,'') = ISNULL(mrd.serialnumber,'')),0.0) qtystok, (mrassetqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=mrd.cmpcode AND sretd.mritemdtloid=mrd.mrassetdtloid AND pretitemmststatus<>'Rejected' AND ISNULL(pretitemtype,'')='Asset' AND sretd.pretitemmstoid<>" + pretitemmstoid + "), 0)) AS pretitemqty, '' AS pretitemdtlnote FROM QL_trnmrassetdtl mrd INNER JOIN QL_trnmrassetmst mrm ON mrm.mrassetmstoid=mrd.mrassetmstoid INNER JOIN QL_mstitem m ON m.itemoid=mrd.mrassetrefoid INNER JOIN QL_m05GN g ON gnoid=mrassetunitoid INNER JOIN QL_m05GN g2 ON g2.gnoid=mrm.mrassetwhoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mrm.mrassetmstoid= " + mritemmstoid + " AND ISNULL(mrassetdtlres1, '')<>'Complete' ORDER BY mrassetdtlseq";
            }
            else
            {
                sSql = "SELECT DISTINCT mritemdtlseq, mritemdtloid, mrd.itemoid, itemcode, itemdesc, refno, ISNULL(serialnumber,'') serialnumber, mritemunitoid pretitemunitoid, g.gndesc AS pretitemunit, mritemwhoid pretitemwhoid, (mritemqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=mrd.cmpcode AND sretd.mritemdtloid=mrd.mritemdtloid AND pretitemmststatus<>'Rejected' AND ISNULL(pretitemtype,'')='' AND sretd.pretitemmstoid<>" + pretitemmstoid + "), 0)) AS mritemqty, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat c where c.refoid = m.itemoid and c.mtrwhoid = mrd.mritemwhoid and c.refno = mrd.refno and ISNULL(c.serialnumber,'') = ISNULL(mrd.serialnumber,'')),0.0) qtystok, (mritemqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=mrd.cmpcode AND sretd.mritemdtloid=mrd.mritemdtloid AND pretitemmststatus<>'Rejected' AND ISNULL(pretitemtype,'')='' AND sretd.pretitemmstoid<>" + pretitemmstoid + "), 0)) AS pretitemqty, '' AS pretitemdtlnote FROM QL_trnmritemdtl mrd INNER JOIN QL_mstitem m ON m.itemoid=mrd.itemoid INNER JOIN QL_m05GN g ON gnoid=mritemunitoid INNER JOIN QL_m05GN g2 ON g2.gnoid=mritemwhoid WHERE mrd.cmpcode='" + CompnyCode + "' AND mritemmstoid= " + mritemmstoid + " AND ISNULL(mritemdtlres1, '')<>'Complete' ORDER BY mritemdtlseq";
            }
            tbl = db.Database.SqlQuery<pretitemdtl>(sSql).ToList();
            if (tbl != null)
            {
                if (tbl.Count() > 0)
                {
                    for (var i = 0; i < tbl.Count(); i++)
                    {
                        tbl[i].pretitemqty = tbl[i].mritemqty;
                    }
                }
            }

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitStockWH(int whfrom, int itemoid, string refno)
        {
            var result = "sukses";
            var msg = "";
            List<liststock> tbl = new List<liststock>();
            sSql = "select ISNULL(SUM(qtyin-qtyout),0) as stokakhir from QL_conmat where mtrwhoid = " + whfrom + " and refoid = " + itemoid + " and refno = '" + refno + "' ";
            tbl = db.Database.SqlQuery<liststock>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitNopol(int vhcoid)
        {
            var result = "sukses";
            var msg = "";
            List<listnopol> tbl = new List<listnopol>();
            sSql = "select vhcno nopol from QL_mstvehicle where vhcoid = " + vhcoid + " ";
            tbl = db.Database.SqlQuery<listnopol>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }

        private void InitDDL(QL_trnpretitemmst tbl)
        {
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            var pretitemwhoid = new SelectList(db.Database.SqlQuery<QL_m05GN>(sSql).ToList(), "gnoid", "gndesc", null);
            ViewBag.pretitemwhoid = pretitemwhoid;
        }

        [HttpPost]
        public ActionResult GetDataMatDetail(int whoid, int mritemmstoid)
        {
            List<listmat> tbl = new List<listmat>();

            sSql = "SELECT mritemdtloid, 0 pretitemdtlseq, shd.itemoid, itemcode, ISNULL(refno,'') refno, ISNULL(serialnumber,'') serialnumber, m.itemdesc, mritemunitoid itemunitoid, g.gndesc AS itemunit, ISNULL((SELECT SUM(qtyin-qtyout) FROM QL_conmat where refoid = m.itemoid AND isnull(refno, '') = shd.refno and mtrwhoid = " + whoid + "),0.0) - ISNULL((SELECT SUM(qtyout) FROM QL_matbooking where refoid = m.itemoid AND isnull(refno, '') = shd.refno and mtrwhoid = " + whoid + " AND flag='Post'),0.0) qtystok, (SELECT gndesc FROM QL_m05gn where gnoid = " + whoid + " and gngroup = 'GUDANG') pretitemwh,"+ whoid + " pretitemwhoid, (mritemqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=shd.cmpcode AND sretd.mritemdtloid=shd.mritemdtloid AND pretitemmststatus<>'Rejected' ), 0)) AS mritemqty FROM QL_trnmritemdtl shd INNER JOIN QL_mstitem m ON m.itemoid=shd.itemoid INNER JOIN QL_m05gn g ON gnoid=mritemunitoid INNER JOIN QL_m05GN g2 ON g2.gnoid=mritemwhoid WHERE shd.cmpcode='"+ CompnyCode +"' AND mritemmstoid=" + mritemmstoid + " AND ISNULL(mritemdtlres1, '')<>'Complete' ORDER BY mritemdtlseq";
            tbl = db.Database.SqlQuery<listmat>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InitDDLWarehouse()
        {
            var result = "sukses";
            var msg = "";
            List<QL_m05GN> tbl = new List<QL_m05GN>();
            sSql = "SELECT * FROM QL_m05GN WHERE cmpcode='" + CompnyCode + "' AND gngroup='GUDANG' AND gnoid>0 AND gnflag = 'ACTIVE'";
            tbl = db.Database.SqlQuery<QL_m05GN>(sSql).ToList();

            return Json(new { result, msg, tbl }, JsonRequestBehavior.AllowGet);
        }        

        [HttpPost]
        public ActionResult GetPBData(int suppoid)
        {            
            
            List <mritem> tbl = new List<mritem>();
            sSql = "SELECT mritemmstoid, mritemno, mritemdate, (SELECT poitemno FROM QL_trnpoitemmst where poitemmstoid = sm.pomstoid) poitemno, sm.rabmstoid, sm.rabmstoid_awal, rm.projectname, de.groupdesc departemen, '' mrflag FROM QL_trnmritemmst sm INNER JOIN QL_trnrabmst rm ON rm.rabmstoid=ISNULL(sm.rabmstoid,0) INNER JOIN QL_mstdeptgroup de ON de.groupoid=rm.deptoid where mritemmststatus = 'Post' and suppoid = " + suppoid + " AND ISNULL(mritemtype, '')='' AND ISNULL(mritemmstres1, '')<>'Closed' AND mritemmstoid NOT IN (SELECT mritemmstoid FROM QL_trnapitemdtl ard INNER JOIN QL_trnapitemmst arm ON arm.cmpcode=ard.cmpcode AND arm.apitemmstoid=ard.apitemmstoid WHERE ard.cmpcode='" + CompnyCode + "' AND apitemmststatus<>'Rejected') UNION ALL  SELECT mrassetmstoid mritemmstoid, mrassetno mritemno, mrassetdate mritemdate, (SELECT poassetno FROM QL_trnpoassetmst where poassetmstoid = sm.poassetmstoid) poitemno, 0 rabmstoid, 0 rabmstoid_awal, '' projectname, '' departemen, 'Asset' mrflag FROM QL_trnmrassetmst sm where mrassetmststatus = 'Post' and suppoid = " + suppoid + " AND ISNULL(mrassetmstres1, '')<>'Closed' AND mrassetmstoid NOT IN (SELECT mrassetmstoid FROM QL_trnapassetdtl ard INNER JOIN QL_trnapassetmst arm ON arm.cmpcode=ard.cmpcode AND arm.apassetmstoid=ard.apassetmstoid WHERE ard.cmpcode='" + CompnyCode + "' AND apassetmststatus<>'Rejected') ";

            tbl = db.Database.SqlQuery<mritem>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSuppData()
        {
            List<Supp> tbl = new List<Supp>();
            sSql = "SELECT DISTINCT suppoid, suppname, Suppcode, Suppaddr, Suppphone1 Suppphone FROM QL_mstsupp WHERE cmpcode='"+ CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnmritemmst WHERE mritemmstoid IN (SELECT mritemmstoid FROM QL_trnmritemmst WHERE mritemmststatus='Post' AND ISNULL(mritemtype, '')='' AND ISNULL(mritemmstres1, '')<>'Closed') AND mritemmstoid NOT IN (SELECT mritemmstoid FROM QL_trnapitemdtl apd INNER JOIN QL_trnapitemmst apm ON apm.cmpcode=apd.cmpcode AND apm.apitemmstoid=apd.apitemmstoid WHERE apitemmststatus<>'Rejected')) UNION ALL  SELECT DISTINCT suppoid, suppname, Suppcode, Suppaddr, Suppphone1 Suppphone FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid IN (SELECT suppoid FROM QL_trnmrassetmst WHERE mrassetmstoid IN (SELECT mrassetmstoid FROM QL_trnmrassetmst WHERE mrassetmststatus='Post' AND ISNULL(mrassetmstres1, '')<>'Closed') AND mrassetmstoid NOT IN (SELECT mrassetmstoid FROM QL_trnapassetdtl apd INNER JOIN QL_trnapassetmst apm ON apm.cmpcode=apd.cmpcode AND apm.apassetmstoid=apd.apassetmstoid WHERE apassetmststatus<>'Rejected')) ";

            tbl = db.Database.SqlQuery<Supp>(sSql).ToList();

            return Json(tbl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDataDetails(List<pretitemdtl> dtDtl)
        {
            Session["QL_trnpretitemdtl"] = dtDtl;
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult FillDetailData()
        {
            if (Session["QL_trnpretitemdtl"] == null)
            {
                Session["QL_trnpretitemdtl"] = new List<pretitemdtl>();
            }

            List<pretitemdtl> dataDtl = (List<pretitemdtl>)Session["QL_trnpretitemdtl"];
            return Json(dataDtl, JsonRequestBehavior.AllowGet);
        }        

        private void FillAdditionalField(QL_trnpretitemmst tbl)
        {
            ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname FROM QL_mstsupp WHERE cmpcode='" + CompnyCode + "' AND suppoid=" + tbl.suppoid + "").FirstOrDefault();

            if (tbl.pretitemtype == "Asset")
            {
                ViewBag.mritemno = db.Database.SqlQuery<string>("SELECT mrassetno FROM QL_trnmrassetmst WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid='" + tbl.mritemmstoid + "'").FirstOrDefault();

                ViewBag.projectname = "";
                ViewBag.departemen = "";
            }
            else
            {
                ViewBag.mritemno = db.Database.SqlQuery<string>("SELECT mritemno FROM QL_trnmritemmst WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid='" + tbl.mritemmstoid + "'").FirstOrDefault();

                ViewBag.projectname = db.Database.SqlQuery<string>("SELECT projectname FROM QL_trnrabmst r WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
                ViewBag.departemen = db.Database.SqlQuery<string>("SELECT groupdesc FROM QL_trnrabmst r INNER JOIN QL_mstdeptgroup de ON de.groupoid=r.deptoid WHERE r.rabmstoid ='" + tbl.rabmstoid + "'").FirstOrDefault();
            }   
        }

        // GET: pretitemMaterial
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index(string filter, ModelFilter modfil)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var DDTitle = "Semua Data In Process";
            var DisplayCol = "none";

            sSql = "SELECT som.cmpcode, som.pretitemmstoid, som.pretitemno, som.pretitemdate, som.mritemmstoid, CASE ISNULL(som.pretitemtype,'') WHEN 'Asset' THEN (SELECT mrassetno FROM QL_trnmrassetmst where mrassetmstoid = som.mritemmstoid) ELSE (SELECT mritemno FROM QL_trnmritemmst where mritemmstoid = som.mritemmstoid) END mritemno, c.suppname, som.pretitemmststatus, som.pretitemmstnote FROM QL_trnpretitemmst som INNER JOIN QL_mstsupp c ON c.suppoid=som.suppoid WHERE som.cmpcode = '" + CompnyCode + "' AND ISNULL(som.pretitemmstres1,'')='' ";

            if (!string.IsNullOrEmpty(filter))
            {
                if (filter == "CF" & modfil.filterperiodfrom != null & modfil.filterperiodto != null)
                {
                    sSql += " AND som.pretitemdate>=CAST('" + ClassFunction.toDate(modfil.filterperiodfrom) + " 00:00:00' AS DATETIME) AND som.pretitemdate<=CAST('" + ClassFunction.toDate(modfil.filterperiodto) + " 23:59:59' AS DATETIME)";
                    ViewBag.DDTitle = "Custom Filter";
                }
            }
            else
            {
                sSql += " AND som.pretitemmststatus IN ('In Process', 'Revised')";
            }
            sSql += " ORDER BY som.pretitemdate";

            List<pretitemmst> dt = db.Database.SqlQuery<pretitemmst>(sSql).ToList();

            ViewBag.DDTitle = DDTitle;
            ViewBag.DisplayCol = DisplayCol;
            return View(dt);
        }

        // GET: pretitemMaterial/Form/5/11
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpretitemmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnpretitemmst();
                tbl.cmpcode = CompnyCode;
                tbl.pretitemmstoid = 0;
                tbl.pretitemdate = ClassFunction.GetServerTime();               
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();
                tbl.pretitemmststatus = "In Process";
                tbl.rabmstoid = 0;
                
                Session["QL_trnpretitemdtl"] = null;
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnpretitemmst.Find(CompnyCode, id);                
                sSql = "select mritemdtloid, pretitemdtlseq, i.itemoid, i.itemcode, ISNULL(refno,'') refno, ISNULL(serialnumber,'') serialnumber, itemdesc, pretitemwhoid, gn2.gndesc pretitemwh, CASE WHEN ISNULL(pretitemtype,'')='Asset' THEN (SELECT mrassetqty FROM QL_trnmrassetdtl where mrassetdtloid = sd.mritemdtloid) ELSE (SELECT mritemqty FROM QL_trnmritemdtl where mritemdtloid = sd.mritemdtloid) END - CASE WHEN ISNULL(pretitemtype,'')='Asset' THEN ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.mritemdtloid=sd.mritemdtloid AND ISNULL(pretitemtype,'')='Asset' AND pretitemmststatus<>'Rejected' AND sretd.pretitemmstoid<>sd.pretitemmstoid), 0) ELSE ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=sd.cmpcode AND sretd.mritemdtloid=sd.mritemdtloid AND ISNULL(pretitemtype,'')='' AND pretitemmststatus<>'Rejected' AND sretd.pretitemmstoid<>sd.pretitemmstoid), 0) END mritemqty, (SELECT SUM(qtyin-qtyout) FROM QL_conmat where refoid = i.itemoid and mtrwhoid = sd.pretitemwhoid and refno = sd.refno) AS qtystok, pretitemqty, pretitemunitoid, gn.gndesc pretitemunit, pretitemdtlnote from QL_trnpretitemdtl sd INNER JOIN QL_trnpretitemmst sm ON sm.pretitemmstoid=sd.pretitemmstoid INNER JOIN QL_m05GN gn ON gn.gnoid = sd.pretitemunitoid and gn.gngroup = 'SATUAN' INNER JOIN QL_m05GN gn2 ON gn2.gnoid = sd.pretitemwhoid and gn2.gngroup = 'GUDANG' INNER JOIN QL_mstitem i ON i.itemoid = sd.itemoid WHERE sd.cmpcode='" + CompnyCode + "' AND sd.pretitemmstoid='" + id + "' ORDER BY sd.pretitemdtlseq";
                Session["QL_trnpretitemdtl"] = db.Database.SqlQuery<pretitemdtl>(sSql).ToList();
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: pretitemMaterial/Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnpretitemmst tbl, string action, string tglmst)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            try
            {
                tbl.pretitemdate = DateTime.Parse(ClassFunction.toDate(tglmst));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("pretitemdate", "Format Tanggal Dokumen Tidak Valid!!" + ex.ToString());
            }
            if (tbl.pretitemmststatus == "Post")
            {
                tbl.pretitemno = generateNo(ClassFunction.GetServerTime());
            }
            else
            {
                tbl.pretitemno = "";
            }
            if (string.IsNullOrEmpty(tbl.pretitemmstres1))
                tbl.pretitemmstres1 = "";
            if (string.IsNullOrEmpty(tbl.pretitemmstres2))
                tbl.pretitemmstres2 = "";
            if (string.IsNullOrEmpty(tbl.pretitemmstres3))
                tbl.pretitemmstres3 = "";
            if (string.IsNullOrEmpty(tbl.pretitemmstnote))
                tbl.pretitemmstnote = "";
            

            List<pretitemdtl> dtDtl = (List<pretitemdtl>)Session["QL_trnpretitemdtl"];
            var sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());
            if (dtDtl == null)
                ModelState.AddModelError("", "Please fill detail data!");
            else if (dtDtl.Count <= 0)
                ModelState.AddModelError("", "Please fill detail data!");
            else
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (dtDtl[i].pretitemqty <= 0)
                        {
                            ModelState.AddModelError("", "QTY PB item "+ dtDtl[i].itemdesc +"  tidak boleh 0!");
                        }
                        else
                        {
                            if (dtDtl[i].pretitemqty > dtDtl[i].mritemqty)
                            {
                                ModelState.AddModelError("", "QTY PB item " + dtDtl[i].itemdesc + " Harus kurang dari PO QTY!");
                            }                            
                        }
                        if (string.IsNullOrEmpty(dtDtl[i].pretitemdtlnote))
                            dtDtl[i].pretitemdtlnote = "";
                        if (string.IsNullOrEmpty(dtDtl[i].refno))
                            dtDtl[i].refno = "";
                        if (tbl.pretitemtype == "Asset")
                        {
                            sSql = "SELECT (mrassetqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=shd.cmpcode AND sretd.mritemdtloid=shd.mrassetdtloid AND ISNULL(pretitemtype,'')='Asset' AND pretitemmststatus<>'Rejected' AND sretd.pretitemmstoid<>" + tbl.pretitemmstoid + "), 0)) AS mritemqty FROM QL_trnmrassetdtl shd WHERE shd.cmpcode='" + CompnyCode + "' AND shd.mrassetdtloid=" + dtDtl[i].mritemdtloid;
                            var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (dtDtl[i].pretitemqty > dQty)
                                ModelState.AddModelError("", "QTY PB item " + dtDtl[i].itemdesc + " sebagian detail data telah diupdate oleh user yg lain. Silahkan cek setiap detail QTY PB harus lebih dari retur PB QTY");
                        }
                        else
                        {
                            sSql = "SELECT (mritemqty - ISNULL((SELECT SUM(pretitemqty) FROM QL_trnpretitemdtl sretd INNER JOIN QL_trnpretitemmst sretm ON sretm.cmpcode=sretd.cmpcode AND sretm.pretitemmstoid=sretd.pretitemmstoid WHERE sretd.cmpcode=shd.cmpcode AND sretd.mritemdtloid=shd.mritemdtloid AND pretitemmststatus<>'Rejected' AND ISNULL(pretitemtype,'')='' AND sretd.pretitemmstoid<>" + tbl.pretitemmstoid + "), 0)) AS mritemqty FROM QL_trnmritemdtl shd WHERE shd.cmpcode='" + CompnyCode + "' AND shd.mritemdtloid=" + dtDtl[i].mritemdtloid;
                            var dQty = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                            if (dtDtl[i].pretitemqty > dQty)
                                ModelState.AddModelError("", "QTY PB item " + dtDtl[i].itemdesc + " sebagian detail data telah diupdate oleh user yg lain. Silahkan cek setiap detail QTY PB harus lebih dari retur PB QTY");
                        }
                        

                        if (tbl.pretitemmststatus == "Post")
                        {
                            if (string.IsNullOrEmpty(tbl.pretitemno))
                                ModelState.AddModelError("pretitemno", "Silahkan isi No. PB!");
                            else if (db.QL_trnpretitemmst.Where(w => w.pretitemno == tbl.pretitemno & w.pretitemmstoid != tbl.pretitemmstoid).Count() > 0)
                                ModelState.AddModelError("pretitemno", "No. PB yang Anda gunakan sudah digunakan oleh data lainnya. Silahkan refresh terlebih dahulu!");

                            sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                            var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                            if (type == "Barang" || type == "Rakitan")
                            {
                                if (!ClassFunction.IsStockAvailable(Session["CompnyCode"].ToString(), ClassFunction.GetServerTime(), dtDtl[i].itemoid, dtDtl[i].pretitemwhoid, dtDtl[i].pretitemqty, dtDtl[i].refno, tbl.rabmstoid_awal, dtDtl[i].serialnumber))
                                {
                                    ModelState.AddModelError("", "Item " + dtDtl[i].itemdesc + " Qty PB tidak boleh lebih dari Stock Qty");
                                }

                                //Stock Value
                                decimal sValue = ClassFunction.GetStockValue(CompnyCode, dtDtl[i].itemoid);
                                decimal sAvgValue = ClassFunction.GetAvgStockValue(CompnyCode, dtDtl[i].itemoid, dtDtl[i].pretitemqty, sValue, "OUT");
                                dtDtl[i].pretitemamt = sAvgValue;
                            }
                            else if (type == "Asset")
                            {
                                var itemoid = dtDtl[i].itemoid;
                                dtDtl[i].assetacctgoid = db.QL_mstitem.FirstOrDefault(x => x.itemoid == itemoid).assetacctgoid;
                                var mrdtloid = dtDtl[i].mritemdtloid;
                                var price = (from mr in db.QL_trnmrassetdtl join po in db.QL_trnpoassetdtl on mr.poassetdtloid equals po.poassetdtloid where mr.mrassetdtloid == mrdtloid select po.poassetprice).FirstOrDefault();
                                var value = Math.Round((dtDtl[i].pretitemqty * price),4);
                                dtDtl[i].pretitemamt = value;
                            }
                            else
                            {
                                dtDtl[i].pretitemamt = 0;
                            }   
                        }

                    }
                }
            }

            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.pretitemdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                ModelState.AddModelError("", "Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                tbl.pretitemmststatus = "In Process";
            }
            if (tbl.pretitemmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    ModelState.AddModelError("", "Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!");
                    tbl.pretitemmststatus = "In Process";
                }
            }

            sSql = "SELECT suppname FROM QL_mstsupp WHERE suppoid=" + tbl.suppoid + "";
            string suppname = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (tbl.pretitemmststatus == "Post")
            {
                // Interface Validation
                if (!ClassFunction.IsInterfaceExists("VAR_STOCK", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_STOCK"));
                if (!ClassFunction.IsInterfaceExists("VAR_PURC_RECEIVED", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_PURC_RECEIVED"));
                if (!ClassFunction.IsInterfaceExists("VAR_SELISIH_STOCK", CompnyCode))
                    ModelState.AddModelError("", ClassFunction.GetInterfaceWarning("VAR_SELISIH_STOCK"));
            }

            if (ModelState.IsValid)
            {
                var mstoid = ClassFunction.GenerateID("QL_trnpretitemmst");
                var dtloid = ClassFunction.GenerateID("QL_trnpretitemdtl");
                var conmatoid = ClassFunction.GenerateID("QL_conmat");
                var servertime = ClassFunction.GetServerTime();

                var glmstoid = ClassFunction.GenerateID("QL_trnglmst");
                var gldtloid = ClassFunction.GenerateID("QL_trngldtl");

                var iAcctgOidStock = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_STOCK", CompnyCode));
                var iAcctgOidRec = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_PURC_RECEIVED", CompnyCode));
                var iSelisihAcctgOid = ClassFunction.GetAcctgOID(ClassFunction.GetVarInterface("VAR_SELISIH_STOCK", CompnyCode));

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {                                         

                        if (action == "Create")
                        {                            
                            tbl.pretitemmstoid = mstoid;
                            tbl.periodacctg = ClassFunction.GetDateToPeriodAcctg(tbl.pretitemdate);
                            //tbl.cmpcode = CompnyCode;
                            tbl.updtime = tbl.createtime;
                            tbl.upduser = tbl.createuser;
                            
                            db.QL_trnpretitemmst.Add(tbl);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + tbl.pretitemmstoid + " WHERE tablename='QL_trnpretitemmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            //tbl.cmpcode = CompnyCode;
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            if (tbl.pretitemtype == "Asset")
                            {
                                sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mrassetdtloid IN (SELECT mritemdtloid FROM QL_trnpretitemdtl WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + tbl.pretitemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnmrassetmst SET mrassetmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid IN (SELECT mritemmstoid FROM QL_trnpretitemmst WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + tbl.pretitemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                            else
                            {
                                sSql = "UPDATE QL_trnmritemdtl SET mritemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mritemdtloid IN (SELECT mritemdtloid FROM QL_trnpretitemdtl WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + tbl.pretitemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();

                                sSql = "UPDATE QL_trnmritemmst SET mritemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid IN (SELECT mritemmstoid FROM QL_trnpretitemmst WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + tbl.pretitemmstoid + ")";
                                db.Database.ExecuteSqlCommand(sSql);
                                db.SaveChanges();
                            }
                                

                            var trndtl = db.QL_trnpretitemdtl.Where(a => a.pretitemmstoid == tbl.pretitemmstoid && a.cmpcode == tbl.cmpcode);
                            db.QL_trnpretitemdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }
                        
                        QL_trnpretitemdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = new QL_trnpretitemdtl();
                            tbldtl.cmpcode = tbl.cmpcode;
                            tbldtl.pretitemdtloid = dtloid++;
                            tbldtl.pretitemmstoid = tbl.pretitemmstoid;
                            tbldtl.pretitemdtlseq = i + 1;
                            tbldtl.mritemmstoid = tbl.mritemmstoid;
                            tbldtl.mritemdtloid = dtDtl[i].mritemdtloid;
                            tbldtl.refno = (dtDtl[i].refno == null ? "" : dtDtl[i].refno);
                            tbldtl.itemoid = dtDtl[i].itemoid;
                            tbldtl.pretitemqty = dtDtl[i].pretitemqty;
                            tbldtl.pretitemunitoid = dtDtl[i].pretitemunitoid;
                            tbldtl.pretitemwhoid = dtDtl[i].pretitemwhoid;
                            tbldtl.pretitemdtlstatus = "";
                            tbldtl.pretitemdtlnote = (dtDtl[i].pretitemdtlnote == null ? "" : dtDtl[i].pretitemdtlnote);
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;
                            tbldtl.pretitemdtlres1 = "";
                            tbldtl.pretitemdtlres2 = "";
                            tbldtl.pretitemdtlres3 = "";
                            tbldtl.pretitemvalueidr = dtDtl[i].pretitemamt;
                            tbldtl.pretitemvalueusd = 0;
                            tbldtl.serialnumber = (dtDtl[i].serialnumber == null ? "" : dtDtl[i].serialnumber);

                            db.QL_trnpretitemdtl.Add(tbldtl);
                            db.SaveChanges();

                            if (tbl.pretitemtype == "Asset")
                            {
                                if (dtDtl[i].pretitemqty >= dtDtl[i].mritemqty)
                                {
                                    sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlres1='Complete' WHERE cmpcode='" + CompnyCode + "' AND mrassetdtloid=" + dtDtl[i].mritemdtloid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnmrassetmst SET mrassetmstres1='Closed' WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid=" + tbl.mritemmstoid + " AND (SELECT COUNT(*) FROM QL_trnmrassetdtl WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid=" + tbl.mritemmstoid + " AND mrassetdtloid<>" + dtDtl[i].mritemdtloid + " AND ISNULL(mrassetdtlres1, '')<>'Complete')=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                if (dtDtl[i].pretitemqty >= dtDtl[i].mritemqty)
                                {
                                    sSql = "UPDATE QL_trnmritemdtl SET mritemdtlres1='Complete' WHERE cmpcode='" + CompnyCode + "' AND mritemdtloid=" + dtDtl[i].mritemdtloid + "";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();

                                    sSql = "UPDATE QL_trnmritemmst SET mritemmstres1='Closed' WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + tbl.mritemmstoid + " AND (SELECT COUNT(*) FROM QL_trnmritemdtl WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid=" + tbl.mritemmstoid + " AND mritemdtloid<>" + dtDtl[i].mritemdtloid + " AND ISNULL(mritemdtlres1, '')<>'Complete')=0";
                                    db.Database.ExecuteSqlCommand(sSql);
                                    db.SaveChanges();
                                }
                            } 

                            if (tbl.pretitemmststatus == "Post")
                            {
                                sSql = "SELECT itemtype FROM QL_mstitem where itemoid = " + dtDtl[i].itemoid + "";
                                var type = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

                                if (type == "Barang" || type == "Rakitan")
                                {
                                    db.QL_conmat.Add(ClassFunction.InsertConMat(CompnyCode, conmatoid++, "PRETFG", "QL_trnpretitemdtl", tbl.pretitemmstoid, tbldtl.itemoid, (tbl.pretitemtype == "Asset" ? "FIXED ASSET" : "FINISH GOOD"), tbldtl.pretitemwhoid, tbldtl.pretitemqty * -1, "PB Retur", tbl.pretitemno + " | " + suppname, Session["UserID"].ToString(), tbldtl.refno, dtDtl[i].pretitemamt, 0, 0, null, tbldtl.pretitemdtloid, tbl.rabmstoid_awal, dtDtl[i].pretitemamt, tbldtl.serialnumber));
                                    db.SaveChanges();
                                }
                            }
                        }
                        sSql = "UPDATE QL_ID SET lastoid=" + (dtloid - 1) + " WHERE tablename='QL_trnpretitemdtl'";
                        db.Database.ExecuteSqlCommand(sSql);
                        db.SaveChanges();

                        if (tbl.pretitemmststatus == "Post")
                        {
                            decimal dSumMR = 0;
                            dSumMR = dtDtl.Sum(x => x.pretitemamt);
                            decimal shipmentamt = 0;
                            if (tbl.pretitemtype == "Asset")
                            {
                                var shipmentdtl = (from prd in dtDtl join sd in db.QL_trnmrassetdtl on prd.mritemdtloid equals sd.mrassetdtloid join ard in db.QL_trnpoassetdtl on new { cmpcode = sd.cmpcode, poassetdtloid = sd.poassetdtloid } equals new { cmpcode = ard.cmpcode, poassetdtloid = ard.poassetdtloid } where ard.cmpcode == CompnyCode && sd.mrassetmstoid == tbl.mritemmstoid select new { ard.poassetprice, ard.poassetqty, ard.poassetdtldiscamt, prd.pretitemqty }).ToList();
                                shipmentamt = Math.Round(shipmentdtl.Sum(x => (((x.poassetprice * x.poassetqty) - x.poassetdtldiscamt.Value) / x.poassetqty) * x.pretitemqty), 4);
                            }
                            else
                            {
                                var shipmentdtl = (from prd in dtDtl join sd in db.QL_trnmritemdtl on prd.mritemdtloid equals sd.mritemdtloid join ard in db.QL_trnpoitemdtl on new { cmpcode = sd.cmpcode, poitemdtloid = sd.podtloid } equals new { cmpcode = ard.cmpcode, poitemdtloid = ard.poitemdtloid } where ard.cmpcode == CompnyCode && sd.mritemmstoid == tbl.mritemmstoid select new { ard.poitemprice, ard.poitemqty, ard.poitemdtldiscamt, prd.pretitemqty }).ToList();
                                shipmentamt = Math.Round(shipmentdtl.Sum(x => (((x.poitemprice.Value * x.poitemqty.Value) - x.poitemdtldiscamt.Value) / x.poitemqty.Value) * x.pretitemqty), 4);
                            }  
                            decimal selisiamt = 0;
                            selisiamt = (dSumMR - shipmentamt);

                            // Insert QL_trnglmst
                            db.QL_trnglmst.Add(ClassFunction.InsertGLMst(CompnyCode, glmstoid, DateTime.Parse(servertime.ToString("MM/dd/yyyy")), sPeriod, tbl.pretitemno + " | " + suppname, "Post", servertime, Session["UserID"].ToString(), servertime, Session["UserID"].ToString(), servertime, 0, 0, 1, 1, 1, 1));
                            db.SaveChanges();

                            var glseq = 1;
                            // Insert QL_trngldtl                            
                            // D : Penerimaan Barang
                            db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidRec, "D", shipmentamt, tbl.pretitemno, tbl.pretitemno + " | " + suppname + " | " + tbl.pretitemmstnote, "Post", Session["UserID"].ToString(), servertime, shipmentamt, 0, "QL_trnpretitemmst " + tbl.pretitemmstoid, null, null, null, 0));
                            db.SaveChanges();

                            if (selisiamt > 0)
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iSelisihAcctgOid, "D", selisiamt, tbl.pretitemno, tbl.pretitemno + " | " + suppname + " | " + tbl.pretitemmstnote, "Post", Session["UserID"].ToString(), servertime, selisiamt, 0, "QL_trnpretitemmst " + tbl.pretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }
                            else if (selisiamt < 0)
                            {
                                selisiamt = selisiamt * (-1);
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iSelisihAcctgOid, "C", selisiamt, tbl.pretitemno, tbl.pretitemno + " | " + suppname + " | " + tbl.pretitemmstnote, "Post", Session["UserID"].ToString(), servertime, selisiamt, 0, "QL_trnpretitemmst " + tbl.pretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            // C : Persediaan Barang 
                            if (tbl.pretitemtype == "Asset")
                            {
                                var assettype = db.QL_trnmrassetmst.FirstOrDefault(x => x.mrassetmstoid == tbl.mritemmstoid).tipebarang;
                                if (assettype == "Asset")
                                {
                                    var listGLAsset = dtDtl.GroupBy(x => x.assetacctgoid).Select(x => new { acctgoid = x.Key, amtIDR = x.Sum(y => y.pretitemamt) }).ToList();
                                    foreach (var item in listGLAsset)
                                    {
                                        db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, item.acctgoid, "C", item.amtIDR, tbl.pretitemno, tbl.pretitemno + " | " + suppname + " | " + tbl.pretitemmstnote, "Post", Session["UserID"].ToString(), servertime, item.amtIDR, 0, "QL_trnpretitemmst " + tbl.pretitemmstoid, null, null, null, 0));
                                    }
                                }
                                else//tipebarang non asset
                                {
                                    db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidStock, "C", dSumMR, tbl.pretitemno, tbl.pretitemno + " | " + suppname + " | " + tbl.pretitemmstnote, "Post", Session["UserID"].ToString(), servertime, dSumMR, 0, "QL_trnpretitemmst " + tbl.pretitemmstoid, null, null, null, 0));
                                }
                                db.SaveChanges();
                            }
                            else //selain tipe asset
                            {
                                db.QL_trngldtl.Add(ClassFunction.InsertGLDtl(CompnyCode, gldtloid++, glseq++, glmstoid, iAcctgOidStock, "C", dSumMR, tbl.pretitemno, tbl.pretitemno + " | " + suppname + " | " + tbl.pretitemmstnote, "Post", Session["UserID"].ToString(), servertime, dSumMR, 0, "QL_trnpretitemmst " + tbl.pretitemmstoid, null, null, null, 0));
                                db.SaveChanges();
                            }

                            sSql = "UPDATE QL_ID SET lastoid=" + glmstoid + " WHERE tablename='QL_trnglmst'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (gldtloid - 1) + " WHERE tablename='QL_trngldtl'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_ID SET lastoid=" + (conmatoid - 1) + " WHERE tablename='QL_conmat'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();                       
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        tbl.pretitemmststatus = "In Process";
                        return View(ex.ToString());
                    }
                }
            }
            else
            {
                tbl.pretitemmststatus = "In Process";
            }

            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        
        }
        

        // POST: pretitemMaterial/Delete/5/11
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnpretitemmst tbl = db.QL_trnpretitemmst.Find(CompnyCode, id);
            var servertime = ClassFunction.GetServerTime();

            string result = "success";
            string msg = "";
            if (tbl == null)
            {
                result = "failed";
                msg = "Data can't be found!";
            }

            if (result == "success")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (tbl.pretitemtype == "Asset")
                        {
                            sSql = "UPDATE QL_trnmrassetdtl SET mrassetdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mrassetdtloid IN (SELECT mritemdtloid FROM QL_trnpretitemdtl WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmrassetmst SET mrassetmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mrassetmstoid IN (SELECT mritemmstoid FROM QL_trnpretitemmst WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }
                        else
                        {
                            sSql = "UPDATE QL_trnmritemdtl SET mritemdtlres1='' WHERE cmpcode='" + CompnyCode + "' AND mritemdtloid IN (SELECT mritemdtloid FROM QL_trnpretitemdtl WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();

                            sSql = "UPDATE QL_trnmritemmst SET mritemmstres1='' WHERE cmpcode='" + CompnyCode + "' AND mritemmstoid IN (SELECT mritemmstoid FROM QL_trnpretitemmst WHERE cmpcode='" + CompnyCode + "' AND pretitemmstoid=" + id + ")";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }   

                        var trndtl = db.QL_trnpretitemdtl.Where(a => a.pretitemmstoid == id && a.cmpcode == CompnyCode);
                        db.QL_trnpretitemdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnpretitemmst.Remove(tbl);
                        db.SaveChanges();

                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        result = "failed";
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "PBR/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(pretitemno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnpretitemmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND pretitemno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var sNo = "";
            sSql = "SELECT pretitemno FROM QL_trnpretitemmst WHERE pretitemmstoid=" + id + "";
            sNo = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPBReturnItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE prm.cmpcode='" + CompnyCode + "' AND prm.pretitemmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "rptPBReturnItem_" + sNo + ".pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}