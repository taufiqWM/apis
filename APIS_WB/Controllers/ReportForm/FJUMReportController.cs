﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class FJUMReportController : Controller
    {
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string CompnyName = ClassFunction.GetCompnyName(System.Configuration.ConfigurationManager.AppSettings["CompnyCode"]);
        private string sSql = "";

        public FJUMReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        [HttpPost]
        public ActionResult GetPRABData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, rm.rabmstoid [ID], rm.rabno [RAB No], rm.rabdate [Tgl Dokumen], rm.projectname [Project], rm.rabmstnote [Keterangan] FROM QL_trnrabmst rm INNER JOIN QL_trnaritemmst arm ON arm.rabmstoid=rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid=arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY rm.rabno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblRAB");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetPOData(string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextNomor, string DDLNomor, string TextCust)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, arm.cmpcode, arm.aritemno [FJ No], CONVERT(CHAR(10),arm.aritemdate,103) [FJ Date], c.custname [Customer], arm.aritemmstnote [FJ Note] FROM QL_trnaritemmst arm INNER JOIN QL_trnrabmst rm ON arm.rabmstoid = rm.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE arm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                sSql += " ORDER BY arm.aritemno";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblPO");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetCustData(string[] DDLstatus, string StartPeriod, string EndPeriod, string DDLPeriod)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT 0 seq, c.custcode [Kode], c.custname [Nama], c.custaddr [Alamat] FROM QL_mstcust c WHERE c.custoid IN (SELECT dp.custoid FROM QL_trndpar dp LEFT JOIN QL_trnaritemmst arm ON arm.somstoid=dp.somstoid LEFT JOIN QL_trnrabmst rm ON rm.rabmstoid=arm.rabmstoid WHERE dp.cmpcode='" + CompnyCode + "'";
                //if (StartPeriod != "" && EndPeriod != "")
                //{
                //    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                //}
                if (DDLstatus != null)
                {
                    if (DDLstatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLstatus.Count(); i++)
                        {
                            stsval += "'" + DDLstatus[i] + "',";
                        }
                        sSql += " AND dp.dparstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }
                sSql += ") ORDER BY c.custcode";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblCust");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        [HttpPost]
        public ActionResult GetMaterialData(string[] DDLStatus, string TextCust, string DDLNomor, string TextNomor, string StartPeriod, string EndPeriod, string DDLPeriod, string DDLPono, string TextNomorPO)
        {
            var result = ""; JsonResult js = null;
            List<string> tblcols = new List<string>();
            List<Dictionary<string, object>> tblrows = new List<Dictionary<string, object>>();

            try
            {
                sSql = "SELECT DISTINCT 0 seq, m.itemcode [Kode], m.itemdesc [Deskripsi], m.itemtype [Tipe], g.gndesc [Unit] FROM QL_mstitem m INNER JOIN QL_trnaritemdtl ard ON ard.itemoid=m.itemoid INNER JOIN QL_trnaritemmst arm ON ard.aritemmstoid=arm.aritemmstoid INNER JOIN QL_trnrabmst rm ON rm.cmpcode=rd.cmpcode AND rm.rabmstoid=arm.rabmstoid INNER JOIN QL_m05GN g ON g.gnoid=ard.aritemunitunitoid INNER JOIN QL_mstcust c ON c.custoid = arm.custoid WHERE rm.cmpcode='" + CompnyCode + "'";
                if (StartPeriod != "" && EndPeriod != "")
                {
                    sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND arm.aritemmststatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string datafilter = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        datafilter += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(datafilter, datafilter.Length - 1) + ")";
                }

                if (TextNomorPO != "")
                {
                    string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
                sSql += " ORDER BY m.itemdesc";

                DataTable tbl = new ClassConnection().GetDataTable(sSql, "tblMat");
                if (tbl.Rows.Count > 0)
                {
                    int i = 1;
                    Dictionary<string, object> row;
                    foreach (DataRow dr in tbl.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in tbl.Columns)
                        {
                            var item = dr[col].ToString();
                            if (col.ColumnName == "seq")
                                item = (i++).ToString();
                            row.Add(col.ColumnName, item);
                            if (!tblcols.Contains(col.ColumnName))
                                tblcols.Add(col.ColumnName);
                        }
                        tblrows.Add(row);
                    }
                }
                else
                    result = "Data Not Found.";
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, tblrows, tblcols }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: PRReport/Report/id
        // id must be RawMaterial, GeneralMaterial, SparePart, FinishGood
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString() + "/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLType, string[] DDLStatus, string StartPeriod, string EndPeriod, string DDLPeriod, string TextCust, string TextNomor, string DDLNomor, string TextMaterial, string DDLSorting, string DDLSortDir, string reporttype, string TextNomorPO, string DDLPono)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");

            var rptfile = "";
            var rptname = "";
            if (DDLType == "Summary")
            {
                if (reporttype == "XLS")
                {
                    rptfile = "rptARSumXls.rpt";
                }
                else
                {
                    rptfile = "rptARSumPdf.rpt";
                }
                rptname = "FJ_SUMMARY";
            }
            else if (DDLType == "Detail2")
            {
                rptfile = "rptARDtl2Pdf.rpt";
                //if (reporttype == "XLS")
                //{
                //    rptfile = "rptARSumXls.rpt";
                //}
                //else
                //{
                //    rptfile = "rptARSumPdf.rpt";
                //}
                rptname = "FJ_DETAIL";
            }
            else if (DDLType == "Summary Status Amount")
            {
                rptfile = "rptARStatusAmtSumPdf.rpt";
                //if (reporttype == "XLS")
                //    rptfile = "rptPRABSumXls.rpt";
                //else
                //    rptfile = "rptPRABSumPdf.rpt";
                rptname = "FJStatusAmt_SUMMARY";
            }
            else if (DDLType == "Summary Outstanding FJ")
            {
                rptfile = "rptAROSSumPdf.rpt";
                //if (reporttype == "XLS")
                //    rptfile = "rptPRABSumXls.rpt";
                //else
                //    rptfile = "rptPRABSumPdf.rpt";
                rptname = "FJOS_SUMMARY";
            }
            else
            {
                rptfile = "rptARDtlPdf.rpt";
                //if (reporttype == "XLS")
                //    rptfile = "rptPRABDtlXls.rpt";
                //else
                //{
                //    rptfile = "rptPRABDtlPdf.rpt";
                //}
                rptname = "FJ_DETAIL";
            }
            var Slct = ""; var Join = "";
            if (DDLType == "Summary Status Amount" || DDLType == "Summary Outstanding FJ")
            {
                if (DDLType == "Summary Status Amount")
                {
                    sSql = "SELECT som.cmpcode, som.soitemmstoid, som.soitemno, som.soitemdate, rm.rabmstoid, rm.rabno, rm.projectname, rm.rabdate, rm.rabtype, ISNULL((SELECT us.usname FROM QL_m01US us WHERE us.usoid=rm.salesoid),'') sales, ISNULL((SELECT arm.aritemfaktur + '.' + us.fakturno FROM QL_mstfa us WHERE us.faoid=ISNULL(arm.faoid,'')),'') fakturno, c.custname, som.soitemgrandtotalamt, som.soitemtotaltaxamt , ISNULL(arm.aritemno, '') aritemno, ISNULL(arm.aritemdate, '') aritemdate, ISNULL(arm.aritemgrandtotal, 0.0)aritemgrandtotal , ISNULL(shm.shipmentitemno, '') shipmentitemno, ISNULL(shm.shipmentitemdate, '') shipmentitemdate, ISNULL(cb.cashbankno, '') cashbankno, ISNULL(cb.cashbankdate, '') cashbankdate FROM QL_trnsoitemmst som INNER JOIN QL_trnrabmst rm ON rm.rabmstoid = som.rabmstoid INNER JOIN QL_mstcust c ON c.custoid = som.custoid LEFT JOIN( SELECT arm.cmpcode, arm.aritemmstoid, arm.aritemno, CONVERT(CHAR(10), arm.aritemdate, 103) aritemdate, arm.aritemgrandtotal, arm.somstoid, arm.rabmstoid, arm.faoid, arm.aritemmststatus, arm.aritemfaktur FROM QL_trnaritemmst arm ) AS arm ON arm.somstoid = som.soitemmstoid AND arm.rabmstoid = rm.rabmstoid LEFT JOIN( SELECT shm.cmpcode, shm.shipmentitemmstoid, shm.shipmentitemno, CONVERT(CHAR(10), shm.shipmentitemdate, 103) shipmentitemdate, shm.soitemmstoid, shm.rabmstoid FROM QL_trnshipmentitemmst shm ) AS shm ON shm.soitemmstoid = som.soitemmstoid AND shm.rabmstoid = rm.rabmstoid LEFT JOIN( SELECT cb.cmpcode, cb.cashbankno, CONVERT(CHAR(10), cb.cashbankdate, 103) cashbankdate, arm.somstoid, arm.rabmstoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar py ON py.cashbankoid = cb.cashbankoid INNER JOIN QL_trnaritemmst arm ON arm.aritemmstoid = py.refoid WHERE py.reftype = 'QL_trnaritemmst' ) AS cb ON cb.somstoid = som.soitemmstoid AND cb.rabmstoid = rm.rabmstoid WHERE som.cmpcode='" + CompnyCode + "'";
                }
                if (DDLType == "Summary Outstanding FJ")
                {
                    sSql = "SELECT arm.aritemmstoid AS [armstoid], (CASE arm.aritemno WHEN '' THEN CONVERT(VARCHAR(10), arm.aritemmstoid) ELSE arm.aritemno END) AS [Draft No.], arm.aritemno AS [AR No.], arm.aritemdate AS [AR Date], CURRENT_TIMESTAMP [AR Date Take Giro], arm.aritemmststatus AS [Status], arm.cmpcode AS [CmpCode], arm.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], arm.aritemratetoidr [Rate IDR], cast(arm.aritemratetousd as Float) [Rate USD], arm.aritemtotalamt [AR Total Amt], arm.aritemtotaldisc [AR Total Disc], arm.aritemtotalnetto [Total Netto], arm.aritemtaxtype [Tax Type], arm.aritemtaxvalue [AR Tax Value], arm.aritemtaxamt [AR Tax Amount], arm.aritemgrandtotal [AR Grand Total], ISNULL((SELECT divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode),'') AS [BU Name], arm.aritemmstnote AS [AR Header Note], ISNULL(arm.aritemmstres3,'') AS [Invoice No], arm.createuser [AR Create User], arm.createtime [AR Create Time], '' AS [AR User Approval] , CURRENT_TIMESTAMP AS [AR ApprovalDate], aritemfaktur [No. Faktur], som.soitemdate [Date SO], som.soitemno [No SO], som.soitemgrandtotalamt [Total SO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemno FROM (SELECT DISTINCT CAST (cbx.shipmentitemno AS VARCHAR) shipmentitemno FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ NO], ISNULL(STUFF((SELECT ', '+ft.shipmentitemdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.shipmentitemdate,103) shipmentitemdate FROM QL_trnshipmentitemmst cbx INNER JOIN QL_trnaritemdtl ardx ON ardx.shipmentitemmstoid=cbx.shipmentitemmstoid WHERE ardx.aritemmstoid=arm.aritemmstoid) ft FOR XML PATH('')), 1, 1, ''),'') [SJ Date], ISNULL(STUFF((SELECT ', '+ft.cashbankno FROM (SELECT DISTINCT CAST (cbx.cashbankno AS VARCHAR) cashbankno FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype<>'BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar NO], ISNULL(STUFF((SELECT ', '+ft.cashbankdate FROM (SELECT DISTINCT CONVERT(CHAR(10),cbx.cashbankdate,103) cashbankdate FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype<>'BLM') ft FOR XML PATH('')), 1, 1, ''),'') [Bayar Date], ISNULL((SELECT SUM(ardx.payaramt) FROM QL_trncashbankmst cbx INNER JOIN QL_trnpayar ardx ON ardx.cashbankoid=cbx.cashbankoid WHERE ardx.refoid=arm.aritemmstoid AND ardx.reftype='QL_trnaritemmst' AND cbx.cashbanktype<>'BLM'),0.0) [Bayar Amt] " + Slct + ", rabno [No. RAB], rm.rabdate [RAB Date], rm.projectname [Project], us.usname [Sales] FROM QL_trnaritemmst arm " + Join + " INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=arm.aritempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid INNER JOIN QL_trnrabmst rm ON arm.rabmstoid=rm.rabmstoid INNER JOIN QL_m01US us ON us.usoid=rm.salesoid INNER JOIN QL_trnsoitemmst som ON som.soitemmstoid=arm.somstoid WHERE arm.cmpcode='" + CompnyCode + "'";
                }
            }
            else if (DDLType == "Detail2")
            {
                sSql = "SELECT * FROM( SELECT arm.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode) [BU Name], c.custoid [Cust Oid], c.custcode [Custcode], c.custname [Customer], arm.dparoid AS [armstoid], (CASE ISNULL(arm.dparno,'') WHEN '' THEN CONVERT(VARCHAR(10), ISNULL(arm.dparno,0)) ELSE ISNULL(arm.dparno,'') END) AS [Draft No.], arm.dparno AS [AR No.], ISNULL(som.soitemno,'') [SO No.], arm.dpardate AS [AR Date], arm.dpardate AS [AR Due Date], arm.updtime AS [AR Post Date], 0.0 [AR Grand Total], arm.dparamt [Balance Amt] FROM QL_trndpar arm  INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=c.custpaymentoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid LEFT JOIN QL_trnsoitemmst som ON arm.somstoid=som.soitemmstoid LEFT JOIN QL_trnrabmst rm ON som.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid WHERE arm.dparstatus IN('Post','Closed') UNION ALL  SELECT som.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=som.cmpcode) [BU Name], c.custoid [Cust Oid], c.custcode [Custcode], c.custname [Customer], ISNULL(arm.aritemmstoid,0) AS [armstoid], (CASE ISNULL(arm.aritemno,'') WHEN '' THEN CONVERT(VARCHAR(10), ISNULL(arm.aritemmstoid,0)) ELSE ISNULL(arm.aritemno,'') END) AS [Draft No.], ISNULL(arm.aritemno,'') AS [AR No.], som.soitemno [SO No.], ISNULL(arm.aritemdate,'') AS [AR Date], dp.dpardate AS [AR Due Date], dp.updtime AS [AR Post Date], dp.dparamt [AR Grand Total], 0.0 [Balance Amt] FROM QL_trnsoitemmst som INNER JOIN QL_mstcust c ON c.custoid=som.custoid INNER JOIN QL_m05GN g ON g.gnoid=som.soitempaytypeoid INNER JOIN QL_mstcurr curr ON curr.curroid=som.curroid INNER JOIN QL_trnaritemmst arm ON arm.somstoid=som.soitemmstoid INNER JOIN QL_trndpar dp ON ISNULL(dp.somstoid,0)=som.soitemmstoid INNER JOIN QL_trnpaydp pay ON pay.dpreftype='QL_trndpar' AND pay.dprefoid=dp.dparoid AND reftype='QL_trnaritemmst' AND refoid=arm.aritemmstoid LEFT JOIN QL_trnrabmst rm ON som.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid WHERE arm.aritemmststatus IN('Post','Closed') UNION ALL  SELECT arm.cmpcode, (SELECT div.divname FROM QL_mstdivision div WHERE div.divcode=arm.cmpcode) [BU Name], c.custoid [Cust Oid], c.custcode [Custcode], c.custname [Customer], ISNULL(am.aritemmstoid,0) AS [armstoid], (CASE ISNULL(am.aritemno,'') WHEN '' THEN CONVERT(VARCHAR(10), ISNULL(am.aritemmstoid,0)) ELSE ISNULL(am.aritemno,'') END) AS [Draft No.], ISNULL(am.aritemno,'') AS [AR No.], ISNULL(som.soitemno,'') [SO No.], ISNULL(am.aritemdate,'') AS [AR Date], arm.dpardate AS [AR Due Date], arm.updtime AS [AR Post Date], pdp.paydpamt [AR Grand Total], 0.0 [Balance Amt] FROM QL_trndpar arm  INNER JOIN QL_mstcust c ON c.custoid=arm.custoid INNER JOIN QL_m05GN g ON g.gnoid=c.custpaymentoid INNER JOIN QL_mstcurr curr ON curr.curroid=arm.curroid INNER JOIN QL_trnpaydp pdp ON pdp.dpreftype='QL_trndpar' AND pdp.dprefoid=arm.dparoid INNER JOIN QL_trnaritemmst am ON am.aritemmstoid=pdp.refoid AND pdp.reftype='QL_trnaritemmst' LEFT JOIN QL_trnsoitemmst som ON arm.somstoid=som.soitemmstoid LEFT JOIN QL_trnrabmst rm ON som.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid WHERE arm.dparoid<0 AND ISNULL(arm.somstoid,0)=0 AND arm.dparstatus IN('Post','Closed') ) AS dt WHERE dt.cmpcode='" + CompnyCode + "'";
            }
            else
            {
                if (DDLType == "Detail")
                {
                    Slct += " ,ard.aritemdtloid [AR Dtl Oid], ard.aritemdtlseq [Seq], ard.itemoid [Mat Oid], m.itemcode [Mat Code] , m.itemdesc [Mat Longdesc], aritemqty [Qty] , g2.gndesc [Unit], (aritemdtlamt / (aritemqty + 0.00)) [Price], aritemdtlamt [Detail Amount], aritemdtldisctype [Disc Type], aritemdtldiscvalue [Disc Value], aritemdtldiscamt [Disc Amount], '' [Disc Type 2], 0.00 [Disc Value 2], 0.00 [Disc Amount 2], aritemdtlnetto [Dtl Netto], aritemdtlnote [AR Detail Note], 1 AS [Convert Qty], ((aritemqty + 0.00) * 1) AS [Qty Dtl], '' AS [Unit Dtl] ";
                    Join += " INNER JOIN QL_trnaritemdtl ard ON arm.cmpcode=ard.cmpcode AND arm.aritemmstoid=ard.aritemmstoid INNER JOIN QL_mstitem m ON m.itemoid=ard.itemoid INNER JOIN QL_m05GN g2 ON g2.gnoid=ard.aritemunitoid ";

                    Slct += ", shm.shipmentitemmstoid [Shipment Oid],shipmentitemno [Shipment No.], shipmentitemdate [Shipment Date], shipmentitemvalueidr [PriceSJIDR], shipmentitemvalueusd [PriceSJUSD], '' [Shipment Container No.], ISNULL((SELECT usname FROM QL_m01US dr WHERE dr.usoid=shm.driveroid),'') [Shipment Driver], (SELECT vhcdesc FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Vehicle], (SELECT vhcno FROM QL_mstvehicle v WHERE shm.armadaoid=v.vhcoid) [Shipment Police No.], shm.shipmentitemmststatus [Shipment Status], arm.aritemfaktur [Shipment SJ Pajak], '' [Shipment Seal No.], shipmentitemmstnote[Shipment Header Note], shm.createuser[Shipment Create User], shm.createtime[Shipment Create Datetime], '' AS[Shipment User Approval] , CURRENT_TIMESTAMP AS[Shipment ApprovalDate] ";

                    Join += " INNER JOIN QL_trnshipmentitemmst shm ON shm.cmpcode=ard.cmpcode AND shm.shipmentitemmstoid=ard.shipmentitemmstoid INNER JOIN QL_trnshipmentitemdtl shd ON shd.cmpcode=ard.cmpcode AND shd.shipmentitemdtloid=ard.shipmentitemdtloid ";

                    Slct += " , '' AS [Divisi] ,som.soitemmstoid [SO ID], soitemno [SO No.], soitemdate [SO Date], ((SELECT gndesc FROM QL_m05GN WHERE gngroup='PAYMENT TERM' AND gnoid=soitempaytypeoid)) [SO Payment Type] , '' [SO Price Type], som.soitemtype [SO Local/Exp], '' AS [SO Direct Cust PO No.], 0 AS [Payment Method], CURRENT_TIMESTAMP [SO ETD], '' [Invoice Value],'' [SO Account No.], '' [Port Of Ship], '' [Port Of Dicharge], soitemmstnote [SO Header Note], soitemmststatus [SO Status], som.createuser [SO Create User], som.createtime [SO Create Date], CURRENT_TIMESTAMP AS [App User SO], CURRENT_TIMESTAMP AS [App Date SO]";

                    //Join += " INNER JOIN QL_trnsoitemmst som ON som.cmpcode=arm.cmpcode AND som.soitemmstoid=arm.somstoid ";
                }

                sSql = "SELECT CASE WHEN dp.dparoid>0 THEN ISNULL((SELECT top 1 arm.aritemmstoid FROM QL_trnaritemmst arm WHERE arm.somstoid=ISNULL(dp.somstoid,0)),0) WHEN dp.dparoid<0 THEN ISNULL((SELECT top 1 arm.aritemmstoid FROM QL_trnaritemmst arm INNER JOIN QL_trnpaydp py ON py.reftype='QL_trnaritemmst' AND py.refoid=arm.aritemmstoid WHERE py.dpreftype='QL_trndpar' AND py.dprefoid=dp.dparoid),0) ELSE 0  END AS [armstoid], CONVERT(VARCHAR(30), CASE WHEN dp.dparoid>0 THEN ISNULL((SELECT top 1 arm.aritemmstoid FROM QL_trnaritemmst arm WHERE arm.somstoid=ISNULL(dp.somstoid,0)),0) WHEN dp.dparoid<0 THEN ISNULL((SELECT top 1 arm.aritemmstoid FROM QL_trnaritemmst arm INNER JOIN QL_trnpaydp py ON py.reftype='QL_trnaritemmst' AND py.refoid=arm.aritemmstoid WHERE py.dpreftype='QL_trndpar' AND py.dprefoid=dp.dparoid),0) ELSE ''  END) AS [Draft No.], CASE WHEN dp.dparoid>0 THEN ISNULL((SELECT top 1 arm.aritemno FROM QL_trnaritemmst arm WHERE arm.somstoid=ISNULL(dp.somstoid,0)),'') WHEN dp.dparoid<0 THEN ISNULL((SELECT top 1 arm.aritemno FROM QL_trnaritemmst arm INNER JOIN QL_trnpaydp py ON py.reftype='QL_trnaritemmst' AND py.refoid=arm.aritemmstoid WHERE py.dpreftype='QL_trndpar' AND py.dprefoid=dp.dparoid),0) ELSE ''  END AS [AR No.], CASE WHEN dp.dparoid>0 THEN ISNULL((SELECT top 1 arm.aritemdate FROM QL_trnaritemmst arm WHERE arm.somstoid=ISNULL(dp.somstoid,0)),'') WHEN dp.dparoid<0 THEN ISNULL((SELECT top 1 arm.aritemdate FROM QL_trnaritemmst arm INNER JOIN QL_trnpaydp py ON py.reftype='QL_trnaritemmst' AND py.refoid=arm.aritemmstoid WHERE py.dpreftype='QL_trndpar' AND py.dprefoid=dp.dparoid),0) ELSE ''  END AS [AR Date], CURRENT_TIMESTAMP [AR Date Take Giro], '' AS [Status], dp.cmpcode AS [CmpCode], c.custoid [Custoid], c.custcode [Custcode], c.custname [Customer], g.gndesc [Payment Type], curr.currcode [Currency], '' [Rate IDR], '' [Rate USD], 0.0 [AR Total Amt], 0.0 [AR Total Disc], 0.0 [Total Netto], '' [Tax Type], 10 [AR Tax Value], 0.0 [AR Tax Amount], CASE WHEN dp.dparoid>0 THEN ISNULL((SELECT top 1 CASE WHEN arm.aritemgrandtotal > 0 THEN dp.dparamt ELSE 0.0 END FROM QL_trnaritemmst arm WHERE arm.somstoid=ISNULL(dp.somstoid,0)),0.0) WHEN dp.dparoid<0 THEN ISNULL((SELECT top 1 py.paydpamt FROM QL_trnaritemmst arm INNER JOIN QL_trnpaydp py ON py.reftype='QL_trnaritemmst' AND py.refoid=arm.aritemmstoid WHERE py.dpreftype='QL_trndpar' AND py.dprefoid=dp.dparoid),0.0) ELSE 0.0  END [AR Grand Total], ISNULL((SELECT divname FROM QL_mstdivision div WHERE div.divcode=dp.cmpcode),'') AS [BU Name], '' AS [AR Header Note], '' AS [Invoice No], som.createuser [AR Create User], som.createtime [AR Create Time], '' AS [AR User Approval] , CURRENT_TIMESTAMP AS [AR ApprovalDate], '' [No. Faktur], ISNULL(som.soitemdate,'') [Date SO], ISNULL(som.soitemno,'') [No SO], ISNULL(som.soitemgrandtotalamt,0.0) [Total SO], '' [SJ NO], '' [SJ Date], dp.dparno [Bayar NO], CONVERT(CHAR(10),dp.dpardate,103) [Bayar Date], dp.dparamt [Bayar Amt], CASE ISNULL(dp.dparres1,'') WHEN 'Batal' THEN (dp.dparamt - dp.dparaccumamt) ELSE (dp.dparamt - ISNULL((SELECT SUM(payx.paydpamt) FROM QL_trnpaydp payx INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid=payx.cashbankoid WHERE payx.dpreftype='QL_trndpar' AND payx.dprefoid=dp.dparoid AND cbx.cashbankstatus IN('Post','Approved','Closed') AND cbx.cashbankdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)),0.0) - ISNULL((SELECT SUM(payx.dprefundamt) FROM QL_trndprefund payx INNER JOIN QL_trncashbankmst cbx ON cbx.cashbankoid=payx.cashbankoid WHERE payx.dpreftype='QL_trndpar' AND payx.dpoid=dp.dparoid AND cbx.cashbankstatus IN('Post','Approved','Closed') AND cbx.cashbankdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)),0.0)) END [Balance Amt], ISNULL(rabno,'') [No. RAB], ISNULL(rm.rabdate,'') [RAB Date], ISNULL(rm.projectname,'') [Project], ISNULL(us.usname,'') [Sales] FROM QL_trndpar dp INNER JOIN QL_mstcust c ON c.custoid=dp.custoid INNER JOIN QL_m05GN g ON g.gnoid=c.custpaymentoid INNER JOIN QL_mstcurr curr ON curr.curroid=dp.curroid LEFT JOIN QL_trnsoitemmst som ON dp.somstoid=som.soitemmstoid LEFT JOIN QL_trnrabmst rm ON som.rabmstoid=rm.rabmstoid LEFT JOIN QL_m01US us ON us.usoid=rm.salesoid WHERE dp.cmpcode='" + CompnyCode + "' AND dp.soreftype='QL_trnsoitemmst' AND dp.dparpaytype<>'DPFK' ";
            }

            if (StartPeriod != "" && EndPeriod != "")
            {
                if (DDLType == "Summary Status Amount")
                {
                    sSql += " AND som.soitemdate>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND som.soitemdate<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
                else if (DDLType == "Summary Outstanding FJ")
                {
                    sSql += " AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME) AND ISNULL((SELECT SUM(con.amttrans - amtbayar) AS amt FROM QL_conar con WHERE con.reftype='QL_trnaritemmst' AND con.refoid=arm.aritemmstoid),0.0) > 0 ";
                }
                else if (DDLType == "Detail2")
                {
                    //sSql += " AND dt.[AR Date]>=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND dt.[AR Date]<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    var filter = "dt.[AR Due Date]";
                    if (DDLPeriod == "arm.updtime")
                    {
                        filter = "dt.[AR Post Date]";
                    }
                    sSql += " AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";

                    if (TextCust != "")
                    {
                        string[] arr = TextCust.Split(';'); string filterdata = "";
                        for (int i = 0; i < arr.Count(); i++)
                        {
                            filterdata += "'" + arr[i] + "',";
                        }
                        sSql += " AND [Custcode] IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                    }
                }
                else
                {
                    //sSql += " AND " + DDLPeriod + ">=CAST('" + StartPeriod + " 00:00:00' AS DATETIME) AND " + DDLPeriod + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                    var filter = "dp.dpardate";
                    if (DDLPeriod == "arm.updtime")
                    {
                        filter = "dp.updtime";
                    }
                    sSql += " AND " + filter + "<=CAST('" + EndPeriod + " 23:59:59' AS DATETIME)";
                }
            }

            if (DDLType == "Summary")
            {
                if (DDLStatus != null)
                {
                    if (DDLStatus.Count() > 0)
                    {
                        string stsval = "";
                        for (int i = 0; i < DDLStatus.Count(); i++)
                        {
                            stsval += "'" + DDLStatus[i] + "',";
                        }
                        sSql += " AND dp.dparstatus IN (" + ClassFunction.Left(stsval, stsval.Length - 1) + ")";
                    }
                }

                if (TextCust != "")
                {
                    string[] arr = TextCust.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND c.custcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomor != "")
                {
                    string[] arr = TextNomor.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLNomor + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }

                if (TextNomorPO != "")
                {
                    string[] arr = TextNomorPO.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND " + DDLPono + " IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }

            if (DDLType == "Detail")
            {
                if (TextMaterial != "")
                {
                    string[] arr = TextMaterial.Split(';'); string filterdata = "";
                    for (int i = 0; i < arr.Count(); i++)
                    {
                        filterdata += "'" + arr[i] + "',";
                    }
                    sSql += " AND m.itemcode IN (" + ClassFunction.Left(filterdata, filterdata.Length - 1) + ")";
                }
            }
            if (DDLType == "Detail2")
            {
                sSql += " ORDER BY dt.[SO No.]";
            }

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", "Periode : " + ClassFunction.toDate(StartPeriod) + " - " + ClassFunction.toDate(EndPeriod));
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (reporttype == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                if (DDLType == "Summary Status Amount")
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else
                {
                    this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                report.SetDataSource(dvRpt.ToTable());
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                if (DDLType == "Summary Status Amount")
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3;
                }
                else
                {
                    report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                }
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (reporttype == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}