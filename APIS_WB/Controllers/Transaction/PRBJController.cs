﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.Transaction
{
    public class PRBJController : Controller
    {
        // GET: PRBJ
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string DefaultCounter = System.Configuration.ConfigurationManager.AppSettings["DefaultCounter"];
        private string sSql = "";
        private string sPeriod = ClassFunction.GetDateToPeriodAcctg(ClassFunction.GetServerTime());

        public PRBJController()
        {
            db.Database.CommandTimeout = 0;
        }

        private void InitDDL(QL_trnprmst tbl)
        {
            sSql = "SELECT * FROM QL_m01US WHERE cmpcode='" + CompnyCode + "' AND usflag = 'ACTIVE'";
            var usoid = new SelectList(db.Database.SqlQuery<QL_m01US>(sSql).ToList(), "usoid", "usname", null);
            ViewBag.usoid = usoid;
        }

        private void FillAdditionalField(QL_trnprmst tbl)
        {
            //ViewBag.suppname = db.Database.SqlQuery<string>("SELECT suppname from QL_mstsupp where suppoid = '" + tblmst.suppoid + "'").FirstOrDefault();
        }

        private string generateNo(DateTime tanggal)
        {
            string sNo = "FPB/" + tanggal.ToString("yy") + "/" + tanggal.ToString("MM") + "/";
            int formatCounter = Convert.ToInt32(DefaultCounter);
            sSql = "SELECT ISNULL(MAX(CAST(RIGHT(prno, " + formatCounter + ") AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnprmst WHERE cmpcode='" + Session["CompnyCode"].ToString() + "' AND prno LIKE '" + sNo + "%'";

            string sCounter = ClassFunction.GenNumberString(db.Database.SqlQuery<int>(sSql).FirstOrDefault(), formatCounter);

            sNo = sNo + sCounter;
            return sNo;
        }

        public class listpbdtl : QL_trnprdtl
        {
            public string itemcode { get; set; }
            public string itemdesc { get; set; }
        }

        [HttpPost]
        public ActionResult GetDataDetails(string prtype)
        {
            JsonResult js = null;
            try
            {
                if(prtype == "Barang")
                {
                    sSql = $"select 0 prdtlseq, 'QL_mstitem' prreftype, i.itemoid prrefoid, i.itemcode, i.itemdesc, 0.0 prqty, 0.0 prprice, '' prdtlnote from QL_mstitem i WHERE i.activeflag='ACTIVE' ";
                }
                else
                {
                    sSql = $"select 0 prdtlseq, 'QL_mstjasa' prreftype, i.jasaoid prrefoid, i.jasacode itemcode, i.jasadesc itemdesc, 0.0 prqty, 0.0 prprice, '' prdtlnote from QL_mstjasa i WHERE i.activeflag='ACTIVE' ";
                }
                var tbl = db.Database.SqlQuery<listpbdtl>(sSql).ToList();
                if (tbl == null || tbl.Count <= 0)
                    js = Json(new { result = "Data not found!" }, JsonRequestBehavior.AllowGet);
                else
                {

                    js = Json(new { result = "", tbl }, JsonRequestBehavior.AllowGet);
                    js.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                js = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return js;
        }

        public ActionResult FillDetailData(int id)
        {
            var result = "";
            JsonResult js = null;
            var dtl = new List<listpbdtl>();

            try
            {
                sSql = $"SELECT prdtlseq, prreftype, prrefoid, CASE prreftype WHEN 'QL_mstitem' THEN ISNULL((select x.itemcode from QL_mstitem x where x.itemoid=d.prrefoid),'') ELSE ISNULL((select x.jasacode from QL_mstjasa x where x.jasaoid=d.prrefoid),'') END itemcode, CASE prreftype WHEN 'QL_mstitem' THEN ISNULL((select x.itemdesc from QL_mstitem x where x.itemoid=d.prrefoid),'') ELSE ISNULL((select x.jasadesc from QL_mstjasa x where x.jasaoid=d.prrefoid),'') END itemdesc, prqty, prprice, prdtlamt, prdtlnote from QL_trnprdtl d WHERE d.prmstoid={id} ORDER BY d.prdtlseq";
                dtl = db.Database.SqlQuery<listpbdtl>(sSql).ToList();
                if (dtl.Count == 0)
                {
                    result = "Data Not Found!";
                }
            }
            catch (Exception e)
            {
                result = e.ToString();
            }

            js = Json(new { result, dtl }, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET/POST: poitem
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ViewBag.DDTitle = "Semua Data In Process";
            ViewBag.DisplayCol = "none";

            return View();
        }

        [HttpPost]
        public ActionResult getListDataTable(string ddlstatus, string periodfrom, string periodto)
        {

            sSql = "SELECT * FROM( Select pr.prmstoid, pr.prno, pr.prtype, pr.prdate, us.usname, pr.prmstnote, pr.prmststatus FROM QL_trnprmst pr INNER JOIN QL_m01US us ON us.usoid=pr.usoid ) AS t WHERE 1=1 ";

            if (!string.IsNullOrEmpty(periodfrom) && !string.IsNullOrEmpty(periodto))
            {
                sSql += " AND t.prdate >='" + periodfrom + " 00:00:00' AND t.prdate <='" + periodto + " 23:00:00'";
            }
            if (!string.IsNullOrEmpty(ddlstatus))
            {
                sSql += " AND t.prmststatus " + ddlstatus;
            }

            var tbl = ClassFunction.toObject(new ClassConnection().GetDataTable(sSql, "tbl"));

            JsonResult js = Json(tbl, JsonRequestBehavior.AllowGet);
            js.MaxJsonLength = Int32.MaxValue;
            return js;
        }

        // GET: RAB/Form
        public ActionResult Form(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnprmst tbl;
            string action = "Create";
            if (id == null)
            {
                tbl = new QL_trnprmst();
                tbl.pruid = Guid.NewGuid();
                tbl.cmpcode = CompnyCode;
                tbl.prmststatus = "In Process";
                tbl.createuser = Session["UserID"].ToString();
                tbl.createtime = ClassFunction.GetServerTime();

                ViewBag.prdate = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
                ViewBag.updtime = ClassFunction.GetServerTime().ToString("dd/MM/yyyy");
            }
            else
            {
                action = "Edit";
                tbl = db.QL_trnprmst.Find(id);
                ViewBag.prdate = tbl.prdate.ToString("dd/MM/yyyy");
                ViewBag.updtime = tbl.updtime.ToString("dd/MM/yyyy");
            }

            if (tbl == null)
            {
                return HttpNotFound();
            }
            ViewBag.action = action;
            InitDDL(tbl);
            FillAdditionalField(tbl);
            return View(tbl);
        }

        // POST: RAB/Form
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(QL_trnprmst tbl, List<listpbdtl> dtDtl, string action, string tglmst, string tglsj)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";
            var sReturnNo = ""; var sReturnState = "";

            tbl.cmpcode = CompnyCode;
            if (string.IsNullOrEmpty(tbl.prno))
                tbl.prno = "";
            if (string.IsNullOrEmpty(tbl.usoid))
                msg += "- Silahkan Pilih Pemohon!<br>";
            if (string.IsNullOrEmpty(tbl.prmstnote))
                tbl.prmstnote = "";

            if (dtDtl == null)
                msg += "- Please fill detail data!<br>";
            else if (dtDtl.Count <= 0)
                msg += "- Please fill detail data!<br>";

            if (dtDtl != null)
            {
                if (dtDtl.Count > 0)
                {
                    for (int i = 0; i < dtDtl.Count(); i++)
                    {
                        if (string.IsNullOrEmpty(dtDtl[i].prdtlnote))
                            dtDtl[i].prdtlnote = "";

                        if (dtDtl[i].prqty <= 0)
                            msg += "- QTY " + dtDtl[i].itemdesc + "  tidak boleh 0!<br>";
                        if (dtDtl[i].prprice <= 0)
                            msg += "- Harga " + dtDtl[i].itemdesc + "  tidak boleh 0!<br>";
                        if (dtDtl[i].prdtlamt <= 0)
                            msg += "- Jumlah " + dtDtl[i].itemdesc + "  tidak boleh 0!<br>";
                    }
                }
            }
            
            //Cek Tanggal Closing
            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
            DateTime cekClosingDate = tbl.prdate;//Tanggal Dokumen
            if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
            {
                msg += "- Cannot save data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
            }
            if (tbl.prmststatus == "Post")
            {
                cekClosingDate = ClassFunction.GetServerTime();//Tanggal Posting/Approved
                if (ClassFunction.isPeriodAcctgClosed(CompnyCode, cekClosingDate))
                {
                    msg += "- Cannot posting/aprroved data to period " + mfi.GetMonthName(cekClosingDate.Month).ToString() + " " + cekClosingDate.Year.ToString() + " anymore because the period has been closed. Please select another period!<br>";
                }
            }

            if (tbl.prmststatus.ToUpper() == "REVISED")
                tbl.prmststatus = "In Process";

            //Variable Send Approval
            string ctrlname = this.ControllerContext.RouteData.Values["controller"].ToString();
            var appoid = ClassFunction.GenerateID("QL_APP2");
            sSql = "SELECT stoid_app FROM QL_m08AS2 a INNER JOIN QL_m01US u ON u.cmpcode=a.cmpcode AND u.usoid=stoid_req INNER JOIN QL_m04MN m ON m.cmpcode=a.cmpcode AND m.mnoid=a.mnoid  WHERE a.cmpcode='" + Session["CompnyCode"].ToString() + "' AND mnfileloc='" + ctrlname + "' AND u.usoid='" + Session["UserID"].ToString() + "' AND a.asflag='Level 1'";
            var stoid_app = db.Database.SqlQuery<string>(sSql).FirstOrDefault();

            if (tbl.prmststatus == "In Approval")
            {
                if (string.IsNullOrEmpty(stoid_app))
                {
                    msg += "Approval User Level 1 untuk data Anda masih belum diatur. Silahkan hubungi Admin untuk mengaturnya!<br>";
                }
            }

            if (msg == "")
            {
                if (tbl.prmststatus == "Post")
                    tbl.prno = generateNo(tbl.prdate);

                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (action == "Create")
                        {
                            //Insert
                            tbl.createtime = servertime;
                            tbl.createuser = Session["UserID"].ToString();
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.QL_trnprmst.Add(tbl);
                            db.SaveChanges();
                        }
                        else if (action == "Edit")
                        {
                            //Update
                            tbl.updtime = servertime;
                            tbl.upduser = Session["UserID"].ToString();
                            db.Entry(tbl).State = EntityState.Modified;
                            db.SaveChanges();

                            var trndtl = db.QL_trnprdtl.Where(b => b.prmstoid == tbl.prmstoid);
                            db.QL_trnprdtl.RemoveRange(trndtl);
                            db.SaveChanges();
                        }

                        QL_trnprdtl tbldtl;
                        for (int i = 0; i < dtDtl.Count(); i++)
                        {
                            tbldtl = (QL_trnprdtl)ClassFunction.MappingTable(new QL_trnprdtl(), dtDtl[i]);
                            tbldtl.prmstoid = tbl.prmstoid;
                            tbldtl.prdtlseq = i + 1;
                            tbldtl.upduser = tbl.upduser;
                            tbldtl.updtime = tbl.updtime;

                            db.QL_trnprdtl.Add(tbldtl);
                            db.SaveChanges();
                        }

                        // Deklarasi data utk approval
                        QL_APP2 tblapp = new QL_APP2();
                        if (tbl.prmststatus == "In Approval")
                        {
                            tblapp.cmpcode = CompnyCode;
                            tblapp.appoid = appoid;
                            tblapp.appform = ctrlname;
                            tblapp.appformoid = tbl.prmstoid;
                            tblapp.requser = tbl.upduser;
                            tblapp.reqdate = servertime;
                            tblapp.appuser = stoid_app;
                            tblapp.tablename = "QL_trnprmst";
                            db.QL_APP2.Add(tblapp);
                            db.SaveChanges();

                            //Update lastoid
                            sSql = "Update QL_ID set lastoid = " + appoid + " Where tablename = 'QL_APP2'";
                            db.Database.ExecuteSqlCommand(sSql);
                            db.SaveChanges();
                        }

                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.prmstoid.ToString();
                        sReturnNo = "No. " + tbl.prno;
                        if (tbl.prmststatus == "Post") sReturnState = "Posted";
                        else sReturnState = "Saved";
                        msg = "Data already " + sReturnState + " with " + sReturnNo + "<br />";
                        result = "success";
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        var err = "";
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                            foreach (var ve in eve.ValidationErrors)
                            {
                                err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                            }
                        }
                        msg = err;
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // POST: PRAB/Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            QL_trnprmst tblmst = db.QL_trnprmst.Find(id);
            var servertime = ClassFunction.GetServerTime();

            string result = "sukses";
            string msg = "";
            if (msg != "")
            {
                result = "failed";
                msg += "this Data already used by other Data";
            }
            else
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.QL_trnprdtl.Where(a => a.prmstoid == id);
                        db.QL_trnprdtl.RemoveRange(trndtl);
                        db.SaveChanges();

                        db.QL_trnprmst.Remove(tblmst);
                        db.SaveChanges();

                        // Oh we are here, looks like everything is fine - save all the data permanently
                        objTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        // roll back all database operations, if any thing goes wrong
                        objTrans.Rollback();

                        result = "failed";
                        msg += ex.ToString();
                    }
                }
            }

            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index");
        }
        public ActionResult PrintReport(int id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");
            if (ClassFunction.checkPagePermission(this.ControllerContext.RouteData.Values["controller"].ToString(), (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            ReportDocument report = new ReportDocument();
            report.Load(Path.Combine(Server.MapPath("~/Report"), "rptPRItem.rpt"));

            report.SetParameterValue("userID", Session["UserID"].ToString());
            report.SetParameterValue("userName", db.Database.SqlQuery<string>("SELECT usname FROM QL_m01US WHERE usoid='" + Session["UserID"].ToString() + "'").FirstOrDefault());
            report.SetParameterValue("sWhere", " WHERE sm.cmpcode='" + CompnyCode + "' AND sm.prmstoid IN (" + id + ")");

            ClassProcedure.SetDBLogonForReport(report);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            report.Close(); report.Dispose();
            return File(stream, "application/pdf", "PRReport.pdf");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}