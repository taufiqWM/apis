﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APIS_WB.Models.DB;
using APIS_WB.Models;
using APIS_WB.Models.ViewModel;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using CrystalDecisions.CrystalReports.Engine;

namespace APIS_WB.Controllers.ReportForm
{
    public class EkuitasReportController : Controller
    {
        // GET: ISReport
        private QL_APISEntities db = new QL_APISEntities();
        private string CompnyCode = System.Configuration.ConfigurationManager.AppSettings["CompnyCode"];
        private string sSql = "";

        public EkuitasReportController()
        {
            db.Database.CommandTimeout = 0;
        }

        // Digunakan untuk mengisi data pada semua 'Dynamic DropDownList' pada saat halaman View pertama kali dibuka
        private void InitDDL(string rpttype = "")
        {
            sSql = "SELECT 'APIS' divcode,'ANUGRAH PRATAMA' divname";
            var DDLBusinessUnit = new SelectList(db.Database.SqlQuery<ReportModels.DDLBusinessUnitModel>(sSql).ToList(), "divcode", "divname");
            ViewBag.DDLBusinessUnit = DDLBusinessUnit;

            List<SelectListItem> DDLMonth = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                var item = new SelectListItem();
                item.Text = new System.Globalization.DateTimeFormatInfo().GetMonthName(i).ToString().ToUpper();
                item.Value = i.ToString();
                DDLMonth.Add(item);
            }
            ViewBag.DDLMonth = DDLMonth;

            List<SelectListItem> DDLYear = new List<SelectListItem>();
            int start = 2019;
            int end = DateTime.Today.Year;
            for (int i = start; i <= end; i++)
            {
                var item = new SelectListItem();
                item.Text = i.ToString().ToUpper();
                item.Value = i.ToString();
                DDLYear.Add(item);
            }
            ViewBag.DDLYear = DDLYear;
        }


        // GET: PReturnReport
        public ActionResult Report(string id)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("LogIn", "Account");
            if (ClassFunction.checkPagePermission("ISReport/Report", (List<QL_m04MN>)Session["Role"]))
                return RedirectToAction("NotAuthorize", "Account");

            switch (id)
            {
                case "Index":
                    ViewBag.FormType = "Finish Good";
                    break;
                default:
                    ViewBag.FormType = "";
                    break;
            }

            InitDDL(); // Memanggil prosedur InitDDL
            return View();
        }

        [HttpPost]
        public ActionResult PrintReport(string DDLBusinessUnit, string DDLCurrency, string ReportType, string DDLType, string DDLMonth, string DDLYear)
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Login", "Account");

            var rptfile = "crEkuitas.rpt"; var rptname = "Ekuitas";

            decimal sa_modal = 0; decimal modal = 0; decimal laba_ditahan = 0;
            decimal laba_tahun = 0; decimal laba_bulan = 0; decimal laba_ta = 0; decimal prive = 0;
            decimal laba_1 = 0; decimal laba_2 = 0; decimal laba_3 = 0; decimal laba_4 = 0;
            decimal laba_5 = 0; decimal laba_6 = 0; decimal laba_7 = 0; decimal laba_8 = 0;
            decimal laba_9 = 0; decimal laba_10 = 0; decimal laba_11 = 0; decimal laba_12 = 0;
            string acctgoidModal = ClassFunction.GetDataAcctgOid("VAR_MODAL", CompnyCode);
            string acctgoidLabaDitahan = ClassFunction.GetDataAcctgOid("VAR_LR_ONHAND", CompnyCode);
            string acctgoidLabaTahun = ClassFunction.GetDataAcctgOid("VAR_LR_YEAR", CompnyCode);
            string acctgoidLabaBulan = ClassFunction.GetDataAcctgOid("VAR_LR_MONTH", CompnyCode);
            string acctgoidLabaTA = ClassFunction.GetDataAcctgOid("VAR_LR_TA", CompnyCode);
            string acctgoidPrive = ClassFunction.GetDataAcctgOid("VAR_PRIVE", CompnyCode);
            string acctgoidLaba = ClassFunction.GetDataAcctgOid("VAR_LABA_BERSIH", CompnyCode);
            sSql = "SELECT ISNULL((SELECT c.amtopen FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidModal + ")";
            sa_modal = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidModal + ")";
            modal = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidLabaDitahan + ")";
            laba_ditahan = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidLabaTahun + ")";
            laba_tahun = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidLabaBulan + ")";
            laba_bulan = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidLabaTA + ")";
            laba_ta = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
            sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidPrive + ")";
            prive = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();

            int start = 1;
            int end = 12;
            for (int i = start; i <= end; i++)
            {
                sSql = "SELECT ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), i, 1)) + "'),0.0) saldo FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoidLaba + ")";
                if (i == 1)
                {
                    laba_1 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 2)
                {
                    laba_2 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 3)
                {
                    laba_3 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 4)
                {
                    laba_4 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 5)
                {
                    laba_5 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 1)
                {
                    laba_6 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 7)
                {
                    laba_7 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 8)
                {
                    laba_8 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 9)
                {
                    laba_9 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 10)
                {
                    laba_10 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 11)
                {
                    laba_11 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
                else if (i == 12)
                {
                    laba_12 = db.Database.SqlQuery<decimal>(sSql).FirstOrDefault();
                }
            }

            string bulandesc = "";
            if(int.Parse(DDLMonth) == 1)
            {
                bulandesc = "Januari";
            }
            else if (int.Parse(DDLMonth) == 2)
            {
                bulandesc = "Februari";
            }else if (int.Parse(DDLMonth) == 3)
            {
                bulandesc = "Maret";
            }else if (int.Parse(DDLMonth) == 4)
            {
                bulandesc = "April";
            }else if (int.Parse(DDLMonth) == 5)
            {
                bulandesc = "Mei";
            }else if (int.Parse(DDLMonth) == 6)
            {
                bulandesc = "Juni";
            }else if (int.Parse(DDLMonth) == 7)
            {
                bulandesc = "Juli";
            }else if (int.Parse(DDLMonth) == 8)
            {
                bulandesc = "Agustus";
            }else if (int.Parse(DDLMonth) == 9)
            {
                bulandesc = "September";
            }else if (int.Parse(DDLMonth) == 10)
            {
                bulandesc = "Oktober";
            }
            else if (int.Parse(DDLMonth) == 11)
            {
                bulandesc = "Nopember";
            }
            else if (int.Parse(DDLMonth) == 12)
            {
                bulandesc = "Desember";
            }
            bulandesc = bulandesc + " " + DDLYear;

            string acctgoid1 = ClassFunction.GetDataAcctgOid("VAR_CASH", CompnyCode);

            sSql = "SELECT * FROM( SELECT a.acctgoid, a.acctgcode, a.acctgdesc, ISNULL((SELECT c.amtbalance FROM QL_crdgl c WHERE c.acctgoid=a.acctgoid AND c.periodacctg='" + ClassFunction.GetDateToPeriodAcctg(new DateTime(int.Parse(DDLYear), int.Parse(DDLMonth), 1)) + "'),0.0) saldo, 'ARUS KAS DARI KEGIATAN USAHA ' utama, '' subutama, 'a' gb, 1 seq FROM QL_mstacctg a WHERE  a.acctgoid IN (" + acctgoid1 + "))AS dt ORDER BY dt.seq";

            DataTable dtRpt = new ClassConnection().GetDataTable(sSql, rptname);
            DataView dvRpt = dtRpt.DefaultView;
            Dictionary<string, object> rptparam = new Dictionary<string, object>();
            rptparam.Add("Periode", bulandesc);
            rptparam.Add("sa_modal", sa_modal);
            rptparam.Add("modal", modal);
            rptparam.Add("laba_ditahan", (laba_ditahan != 0 ? (laba_ditahan * (-1)) : laba_ditahan));
            rptparam.Add("laba_tahun", (laba_tahun != 0 ? (laba_tahun * (-1)) : laba_tahun));
            rptparam.Add("laba_bulan", (laba_bulan != 0 ? (laba_bulan * (-1)) : laba_bulan));
            rptparam.Add("laba_ta", (laba_ta != 0 ? (laba_ta * (-1)) : laba_ta));
            rptparam.Add("prive", (prive != 0 ? (prive * (-1)) : prive));
            rptparam.Add("laba_1", laba_1);
            rptparam.Add("laba_2", laba_2);
            rptparam.Add("laba_3", laba_3);
            rptparam.Add("laba_4", laba_4);
            rptparam.Add("laba_5", laba_5);
            rptparam.Add("laba_6", laba_6);
            rptparam.Add("laba_7", laba_7);
            rptparam.Add("laba_8", laba_8);
            rptparam.Add("laba_9", laba_9);
            rptparam.Add("laba_10", laba_10);
            rptparam.Add("laba_11", laba_11);
            rptparam.Add("laba_12", laba_12);
            rptparam.Add("PrintUserID", Session["UserID"].ToString());
            rptparam.Add("RptFile", rptfile);

            if (ReportType == "")
            {
                this.HttpContext.Session["rptsource"] = dvRpt.ToTable();
                this.HttpContext.Session["rptpaper"] = CrystalDecisions.Shared.PaperSize.PaperA4;
                this.HttpContext.Session["rptparam"] = rptparam;
                return Json(new { rptfile, rptname }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), rptfile));
                if (rptparam.Count > 0)
                    foreach (var item in rptparam)
                        report.SetParameterValue(item.Key, item.Value);
                ClassProcedure.SetDBLogonForReport(report);
                report.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4;
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                if (ReportType == "XLS")
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/excel", rptname + ".xls");
                }
                else
                {
                    Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    report.Close(); report.Dispose();
                    return File(stream, "application/pdf", rptname + ".pdf");
                }
            }
        }
    }
}